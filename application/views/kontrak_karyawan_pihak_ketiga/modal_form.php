<!-- Modal Logout System -->
<div class="modal fade" id="modal_karyawan_pihak_ketiga">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-warning"></i> Create Penawaran</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-karyawan-pihak-ketiga">
                    <input type="hidden" name="id" value="" />
                    
                    <div class="form-group">
                        <label for="namalengkap" class="col-sm-4 control-label">Karyawan</label>
                        <div class="col-sm-8">
                            <select name="karyawan_id" class="form-control">
                                <option value="">-- Select Karyawan --</option>
                                <?php foreach($karyawan as $item):?>
                                    <option value="<?php print($item->id); ?>"><?php print($item->nama_lengkap); ?></option>
                                <?php endforeach;?>
                            </select>
                            <span class="help-block"></span>
                        </div>
                    </div>
                                   
            </div>
            <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                    <button type="submit" name="simpan" class="btn btn-primary" id="btnSaveKaryawanPihakKetiga">Simpan</button>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

</div>
</div>
<!-- /Box Form Pengunjung -->