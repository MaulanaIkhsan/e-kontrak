<?php
$this->load->view('template/header');?>

<?php $this->load->view('template/asset_header');?>
<link rel="stylesheet" type="text/css" href="<?php print(base_url('assets/bower_components/datatables.net-bs/css/custom_button_export.css'))?>" />

<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <?php print($this->session->flashdata('alert')); ?>
        <?php print($this->session->flashdata('success')); ?>
        <?php $this->session->unset_userdata('pengunjung_id'); ?>
        <div class="col-md-12">
            <!-- Horizontal Form -->
          <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Data Kunjungan</h3>
                <a href="<?php print(base_url('kunjungan/add')) ?>" class="btn btn-primary pull-right">Tambah Data</a>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <div class="box-body">
                <table id="kunjungan" class="table table-bordered table-striped table-responsive">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Tanggal</th>
                      <th>Waktu</th>
                      <th>Nama Pengunjung</th>
                      <th>Asal</th>
                      <th>Tujuan</th>
                      <th>Keperluan</th>
                      <th>Penilaian</th>
                      <th><i class="fa fa-gear"></i></th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </div> 

            </div>
          </div>

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>

<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/dataTables.buttons.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.flash.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/jszip.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/pdfmake.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/vfs_fonts.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.html5.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.print.min.js'))?>"></script>

<script>
  $(function(){
    var data = $('#kunjungan').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
            "ajax": {
                "url": "<?php echo site_url('kunjungan/get_data');?>",
                "type": "POST"
            },
            dom: 'lBfrtip',
            button:[{
              extend:'pdf',
              oriented:'potrait',
              pageSize: 'A4',
              title : 'Data Kunjungan',
              download : 'open'
            }, 'copy', 'csv', 'excel', 'pdf', 'print'],
            "columnDefs": [{ 
                "targets": [ 0 ], 
                "orderable": false, 
            }]
        });
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>
<?php $this->load->view('template/footer');?>