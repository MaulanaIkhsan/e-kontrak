<?php
$this->load->view('template/header');?>
<?php $this->load->view('template/asset_header');?>

<link rel="stylesheet" type="text/css" href="<?php print(base_url('assets/bower_components/datatables.net-bs/css/custom_button_export.css'))?>" />

<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <?php
        print($this->session->flashdata('alert'));
        print($this->session->flashdata('success'));

        $sess_data          = $this->session->userdata('session_data');
        $user_as            = $sess_data['as'];
        $user_role          = $sess_data['role'];
        $user_id            = $sess_data['id'];
        ?>
        <div class="col-md-12">
            <!-- Horizontal Form -->
          <div class="box box-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Data Pekerjaan</h3>
                <span class="pull-right">
                  
                  <?php if($user_as=='pegawai' and $user_role=='Super Admin') { ?>
                    <a href="<?php print(base_url('aktivitas/template_import')); ?>" target="blank" class="btn btn-primary">Template Import</a>
                    <a href="javascript:void(0)" onclick="modal_import()" class="btn btn-primary">Import Data</a>
                  <?php } ?>

                </span>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <div class="box-body">
                <table id="aktivitas" class="table table-bordered table-striped table-responsive">
                    <thead>
                        <tr class="headings" align="center">
                            <th class="column-title" align="center">No</th>
                            <th class="column-title" align="center">Nama Pekerjaan</th>
                            <th class="column-title" align="center">Tahun</th>
                            <th class="column-title" align="center">PPKOM</th>
                            <th class="column-title" align="center">PPTK</th>
                            
                            <th class="column-title" align="center">Jenis Pekerjaan</th>
                            <th class="column-title" align="center">Metode Pengadaan</th>
                            <th class="column-title" align="center">Anggaran</th>
                            <th class="column-title" align="center">Aksi</th>
                        </tr>
                    </thead>
                  <tbody></tbody>
                </table>
              </div> 

            </div>
          </div>

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/dataTables.buttons.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.flash.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/jszip.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/pdfmake.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/vfs_fonts.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.html5.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.print.min.js'))?>"></script>

<script>
  $(function(){
      
      $.fn.dataTable.ext.errMode = 'none';
      
    var data = $('#aktivitas').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
            "ajax": {
                "url": "<?php echo site_url('aktivitas/get_data');?>",
                "type": "POST"
            },
            dom: 'lBfrtip',
            button:[{
              extend:'pdf',
              oriented:'potrait',
              pageSize: 'A4',
              title : 'Data Kegiatan',
              download : 'open'
            }, 'copy', 'csv', 'excel', 'pdf', 'print'],
            "columnDefs": [{ 
                "targets": [ 0 ], 
                "orderable": false, 
            }]
        });
    $('[data-toggle="tooltip"]').tooltip();

     // import data pekerjaan
     $('#form-import-csv').submit(function(obj){
            obj.preventDefault();
            
            // console.log('data form : '+new FormData(this));
            
            $.ajax({
                url: "<?php echo site_url('aktivitas/import_csv/')?>",
                type: 'POST',
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(datas){                  
                    // Parsing to object
                    console.log(datas);
                    
                    datas=JSON.parse(datas);                  
                    
                    //if success close modal and reload ajax table dokumen
                    if(datas.hasOwnProperty('status') && datas.status) 
                    {
                        alert('Data berhasil tersimpan');
                        $('#modal-import-data').modal('hide');
                        $('#form-import-csv')[0].reset(); // reset form on modals
                        reload_data();
                    }
                    else
                    {
                        if(datas.hasOwnProperty('inputerror')) {
                            for (var i = 0; i < datas.inputerror.length; i++) 
                            {
                                $('[name="'+datas.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                $('[name="'+datas.inputerror[i]+'"]').next().text(datas.error_string[i]); //select span help-block class set text error string
                            }
                        }
                    }
                    $('#btnImport').text('save'); //change button text
                    $('#btnImport').attr('disabled',false); //set button enable 
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error calling save data. Please check again');
                    $('#modal-import-data').modal('hide');
                }
            });
        });
  });

  function modal_import() {
    $('#modal-import-data').modal('show');
  }

  function reload_data() {
    data.ajax.reload();
  }
</script>

<?php 
  $this->load->view('aktivitas/modal_form');

  $this->load->view('template/footer');
?>