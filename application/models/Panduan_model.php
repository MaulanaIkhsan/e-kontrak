<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Panduan_model extends CI_Model {

    /*--- panduan_user ---*/
    // id
    // nama
    // jenis (internal, umum)
    // file
    // created_at
    // updated_at

    var $id, $nama, $jenis;

    public function set_id($id)
    {
        $this->id = $id;
    }

    public function set_nama($nama)
    {
        $this->nama = $nama;
    }

    public function set_jenis($jenis)
    {
        $this->jenis = $jenis;
    }

    public function fetch_data()
    {
        $sql = $this->db->select('panduan_user.id as id,
                                panduan_user.nama as nama,
                                panduan_user.jenis as jenis,
                                panduan_user.file as file')
                        ->from('panduan_user');
        
        if(!empty($this->file)) {
            $sql->where('panduan_user.file', $this->file);
        }
        
        if(!empty($this->jenis)) {
            $sql->where('panduan_user.jenis', $this->jenis);
        }

        if(!empty($this->nama)) {
            $sql->where('panduan_user.nama', $this->nama);
        }

        if(!empty($this->id)) {
            return $sql->where('panduan_user.id', $this->id)
                        ->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }
}

/* End of file Panduan_model.php */
