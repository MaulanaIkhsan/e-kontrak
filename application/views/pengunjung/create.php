<?php
$this->load->view('template/header');?>
<!-- Select2 -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/select2/dist/css/select2.min.css')); ?>">
<?php $this->load->view('template/asset_header');?>
<!-- Star Rating -->
<link rel="stylesheet" href="<?php print(base_url('assets/plugins/rating/dist/starrr.css')); ?>">


<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <!-- Box Form Pengunjung -->
        <?php print($this->session->flashdata('alert')); ?>
        <?php print($this->session->flashdata('success')); ?>
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-primary" id="form-pengunjung">
              <div class="box-header with-border">
                <h3 class="box-title">Formulir Pengunjung</h3>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <form class="form-horizontal" method="post" action="<?php print(base_url('pengunjung/add'))?>">
                <div class="box-body">
                  <div class="form-group">
                    <label for="nik" class="col-sm-2 control-label">NIK</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name="nik" placeholder="Input NIK" maxlength="15" required="required" id="nik" />
                      <span class="text-danger col-md-8"><?php echo form_error('NIK'); ?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Nama Lengkap</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name="namalengkap" placeholder="Input nama lengkap" maxlength="24" required="required">
                      <span class="text-danger col-md-8"><?php echo form_error('NamaLengkap'); ?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="keperluan" class="col-sm-2 control-label">Alamat</label>
                    <div class="col-sm-9">
                    <textarea class="form-control" rows="3" placeholder="Alamat Anda" maxlength="500" name="alamat" required="required"></textarea>
                    <span class="text-danger col-md-8"><?php echo form_error('Alamat'); ?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Jenis Kelamin</label>
                    <div class="col-sm-9">
                      <input type="radio" name="jk" value="Pria" class="minimal" required="required" /> Pria<br/>
                      <input type="radio" name="jk" value="Wanita" class="minimal" required="required" /> Wanita<br/>
                      <span class="text-danger col-md-8"><?php echo form_error('jk'); ?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Asal</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" placeholder="Asal instansi/perusahaan" maxlength="15" required="required" name="asal">
                      <span class="text-danger col-md-8"><?php echo form_error('Asal'); ?></span>
                    </div>
                  </div>
                  <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Nomor Handphone</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" placeholder="Nomor handphone anda" maxlength="15" required="required" name="nohp">
                        <span class="text-danger col-md-8"><?php echo form_error('Nohp'); ?></span>
                      </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="reset" class="btn btn-default">Batal</button> | 
                  <button type="submit" name="save" class="btn btn-success" value="simpan">Simpan</button>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
          </div>
          <!-- /Box Form Pengunjung -->

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<!-- Select2 -->
<script src="<?php print(base_url('assets/bower_components/select2/dist/js/select2.full.min.js'));?>"></script>
<!-- Star Rating -->
<script type="text/javascript" src="<?php print(base_url('assets/plugins/rating/dist/starrr.js')); ?>"></script>
<script>
  $(function(){
    $('#nohp,#nik').bind('keyup paste', function(){
      this.value = this.value.replace(/[^0-9]/g, '');
    });

    $('input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    });

  });
</script>
<?php $this->load->view('template/footer');?>