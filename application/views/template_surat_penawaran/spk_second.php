<!DOCTYPE html>
<HtmL>

<HeAd>
    <MeTa HTTP-EquIV="CONTENT-TYPE" CONTent="text/html; charset=utf-8">
    <TItlE>Semarang, 29 Maret 2004</tITLE>
    <meta name=GENERATOR coNTeNT="LibreOffice 4.1.6.2 (Linux)">
    <Meta nAMe="AUTHOR" CONTeNt="DPU">
    <MEtA NaMe=CREATED cONtENt="Tahun_Anggaran0817;20400000000000">
    <MeTA NAmE="CHANGEDBY" CoNteNt="isan">
    <META NAme="CHANGED" coNTent="20190320;163019000000000">
    <MEtA nAmE=KSOProductBuildVer CONTEnt=1033-10.1.0.6757>
    <sTyLE>
        @page {
            size: 8.47in 13.98in;
            margin-right: 0.88in;
            margin-top: 0.59in;
            margin-bottom: 0.69in
        }
        P {
            margin-bottom: 0.08in;
            direction: ltr;
            color: #000000
        }
        P.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
        H1 {
            margin-left: 3.5in;
            margin-top: 0in;
            margin-bottom: 0in;
            direction: ltr;
            color: #000000;
            text-align: justify;
            text-decoration: underline
        }
        H1.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
    </sTYLE>
</heAd>

<bOdY LANg="en-US" tExT=#000000 dIR="LTR">
    <table CELLPADDING="7" cellspacing="0">
        <tr>
            <td valign="center" align="justify" colspan="2" width="400" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
            <strong>INSTRUKSI KEPADA PENYEDIA :</strong> Penagihan hanya dapat dilakukan setelah penyelesaian pekerjaan yang diperintahkan dalam SPK ini dan dibuktikan dengan Berita Acara Serah Terima. Jika pekerjaan tidak dapat diselesaikan dalam jangka waktu pelaksanaan pekerjaan karena kesalahan atau kelalaian Penyedia maka Penyedia berkewajiban untuk membayar denda kepada PPK sebesar 1/1000 (satu per seribu) dari bagian tertentu nilai SPK sebelum PPN setiap hari kalender keterlambatan.
            </td>
        </tr>
        <tr>
            <td align="center" valign="middle" width="500" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                A.n. Kepala Dinas Pekerjaan Umum<br/>
                Kota Semarang<br/>
                Kuasa Pengguna Anggaran<br/>
                Selaku Pejabat Pembuat Komitmen<br/>
                Tahun Anggaran <?php print(date("Y"));?><br/>
                <br/><br/><br/><br/><br/>
                <u><?php print((!empty($pejabat_pembuat_komitmen->pegawai_nama))?$pejabat_pembuat_komitmen->pegawai_nama:''); ?></u><br/>
                NIP. <?php print((!empty($pejabat_pembuat_komitmen->pegawai_nip))?$pejabat_pembuat_komitmen->pegawai_nip:''); ?>
            </td>
            <td align="center" width="500" valign="middle" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                Penyedia Barang/Jasa <?php print(ucwords(strtolower($kontrak_pekerjaan->aktivitas_nama)));?><br/>
                <br/><br/><br/><br/><br/><br/><br/><br/><br/>
                <u><?php print($direktur_perusahaan);?></u><br/>
                Direktur
            </td>
        </tr>
        
    </table>
    <p></p>
    <p></p>

    
    
</BODY>

</HTML>