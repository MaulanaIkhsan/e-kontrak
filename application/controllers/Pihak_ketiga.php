<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pihak_ketiga extends CI_Controller {

    //----- pihak_ketiga -----
    // id
	// nama_perusahaan
	// npwp
    // alamat_lengkap
    // no_telepon
    // email
    // status ('Cabang', 'Pusat')
    // alamat_pusat
    // no_telepon_pusat
    // email_pusat
    // created_at
    // updated_at

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $sess_data = $this->session->userdata('session_data');
        if($sess_data===NULL) {
            $this->session->set_flashdata('warning', '<div class="alert alert-warning fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a> 
                Harap login terlebih dahulu untuk mengakses sistem</div>');
			redirect('auth');
        }

        $this->load->model('Pihak_ketiga_model', 'pihak_ketiga');
		$this->load->model('Generic_model', 'generic');
		
		$this->load->helper('generic');
    }
    
    public function index()
    {
		$this->session->unset_userdata('pihak_ketiga_id');
		$this->session->unset_userdata('is_profil');
        $this->load->view('pihak_ketiga/show');
	}
	
	public function redirect_menu()
	{
		$this->load->model('Karyawan_model', 'karyawan');

		$sess_data = $this->session->userdata('session_data');
		$karyawan = $this->karyawan->fetch_data($sess_data['id']);
		
		redirect('pihak_ketiga/detail/'.$karyawan->pihak_ketiga_id);
	}
	
	public function detail($id)
	{
		$data['pihak_ketiga_id'] = $id;
		$this->session->set_userdata('pihak_ketiga_id', $id);
		$this->session->unset_userdata('is_profil');
		
		$this->load->model('Jenis_dokumen_model', 'jenis_dokumen');
		$this->load->model('Jenjang_pendidikan_model', 'jenjang_pendidikan');
		$this->load->model('Jabatan_perusahaan_model', 'jabatan');
		$this->load->model('Dokumen_pihak_ketiga_model', 'dokumen_pihak_ketiga');
		$this->load->model('Karyawan_model', 'karyawan');

		$data['jenis_dokumen'] 		= $this->jenis_dokumen->fetch_data();
		$data['jenjang_pendidikan'] = $this->jenjang_pendidikan->fetch_data();
		$data['jabatan_perusahaan'] = $this->jabatan->fetch_data();

		$data['flag_dokumen'] 		 = FALSE;
		$data['flag_karyawan'] 		 = FALSE;
		$data['flag_karyawan_admin'] = FALSE;

		$dokumen 	= $this->dokumen_pihak_ketiga->fetch_data(NULL, $id);
		$karyawan	= $this->karyawan->fetch_data(NULL, $id);

		if(!empty($dokumen)) {
			$data['flag_dokumen'] = TRUE;
		}

		if(!empty($karyawan)) {
			$data['flag_karyawan'] = TRUE;

			foreach($karyawan as $item):
				if($item->is_user=='y') {
					$data['flag_karyawan_admin'] = TRUE;
					break;
				}
			endforeach;
		}

		
		
		$this->load->view('pihak_ketiga/detail', $data);
	}

    public function ajax_add()
	{
		$this->_validate();
		$data = array(
				'nama_perusahaan'   => $this->input->post('nama_perusahaan'),
				'npwp'				=> $this->input->post('npwp'),
				'alamat_lengkap' 	=> $this->input->post('alamat_lengkap'),
				'no_telepon' 	    => $this->input->post('no_telepon'),
				'email' 	        => $this->input->post('email'),
				'status' 		    => $this->input->post('status'),
                'alamat_pusat' 	    => $this->input->post('alamat_pusat'),
                'no_telepon_pusat' 	=> $this->input->post('no_telepon_pusat'),
                'email_pusat' 	    => $this->input->post('email_pusat'),
			);
		
		
		$insert = $this->generic->insert('pihak_ketiga',$data);
		
        
        if($insert=='ok') {
			//Autogenerate admin user
			if(!empty($this->input->post('admin-user'))) {
				// id
				// nama_lengkap
				// jabatan_id
				// jenjang_pendidikan_id
				// pihak_ketiga_id
				// jenis_kelamin (Pria, Wanita)
				// alamat_lengkap
				// tgl_lahir
				// no_telepon
				// email
				// dokumen_profil
				// dokumen_pendidikan
				// dokumen_lainnya
				// username
				// password
				// is_user (y, n)
				$options = ['cost' => 13];
				$default_password = 'dpusmg123';
				$pihak_ketiga = $this->db->select('id')
										->from('pihak_ketiga')
										->order_by('id', 'desc')
										->limit(1)->get()->row();
				$data_user = array(
								'nama_lengkap'          => 'admin@'.normalize_str($this->input->post('nama_perusahaan')),
								'jabatan_id'            => '',
								'jenjang_pendidikan_id' => '',
								'pihak_ketiga_id'       => (!empty($pihak_ketiga->id) ? $pihak_ketiga->id:FALSE),
								'jenis_kelamin' 	    => 'Pria',
								'alamat_lengkap'        => 'semarang',
								'tgl_lahir'             => '01-01-2000',
								'no_telepon'            => '123',
								'email'                 => 'mail@'.normalize_str($this->input->post('nama_perusahaan')),
								'username'              => 'admin@'.normalize_str($this->input->post('nama_perusahaan')),
								'password'              => password_hash($default_password, PASSWORD_BCRYPT, $options),
								'is_user'               => 'y',
							);
				
				$this->db->query('set FOREIGN_KEY_CHECKS = 0;');
				$admin_user = $this->generic->insert('karyawan',$data_user);
				$this->db->query('set FOREIGN_KEY_CHECKS = 1;');
			}

            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => FALSE));
        }
	}

    public function ajax_edit($id)
	{
		$data = $this->pihak_ketiga->fetch_data($id);
		echo json_encode($data);
	}

	public function ajax_update()
	{
		$this->_validate();
		$data = array(
				'nama_perusahaan'   => $this->input->post('nama_perusahaan'),
				'npwp'				=> $this->input->post('npwp'),
                'alamat_lengkap' 	=> $this->input->post('alamat_lengkap'),
                'no_telepon' 	    => $this->input->post('no_telepon'),
                'email' 	        => $this->input->post('email'),
                'status' 		    => $this->input->post('status'),
                'alamat_pusat' 	    => $this->input->post('alamat_pusat'),
                'no_telepon_pusat' 	=> $this->input->post('no_telepon_pusat'),
                'email_pusat' 	    => $this->input->post('email_pusat'),
            );
        
        $key = array('id'=>$this->input->post('id'));
		$update = $this->generic->update('pihak_ketiga', $data, $key);
        
        if($update=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => FALSE));
        }
    }
    
    public function ajax_delete($id)
	{
        $key = array('id'=>$id);
		$delete = $this->generic->delete('pihak_ketiga', $key);
        
        if($delete=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => FALSE));
        }
	}

    private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('nama_perusahaan') == '')
		{
			$data['inputerror'][] = 'nama_perusahaan';
			$data['error_string'][] = 'Nama Perusahaan name is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('alamat_lengkap') == '')
		{
			$data['inputerror'][] = 'alamat_lengkap';
			$data['error_string'][] = 'Alamat Lengkap is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('no_telepon') == '')
		{
			$data['inputerror'][] = 'no_telepon';
			$data['error_string'][] = 'Nomor Telepon is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('email') == '')
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Email is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('email')!='')
		{
			$email = $this->input->post('email');
			$check_email = filter_var(filter_var($email, FILTER_SANITIZE_EMAIL), FILTER_VALIDATE_EMAIL);

			if(!$check_email) {
				$data['inputerror'][] = 'email';
				$data['error_string'][] = 'Invalid email address';
				$data['status'] = FALSE;
			}
		}

		if($this->input->post('status') == '')
		{
			$data['inputerror'][] = 'status';
			$data['error_string'][] = 'Status is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('status') == 'Cabang') 
		{
			if($this->input->post('alamat_pusat') == '')
			{
				$data['inputerror'][] = 'alamat_pusat';
				$data['error_string'][] = 'Alamat Pusat is required';
				$data['status'] = FALSE;
			}
			
			if($this->input->post('no_telepon_pusat') == '')
			{
				$data['inputerror'][] = 'no_telepon_pusat';
				$data['error_string'][] = 'Nomor Telepon Pusat is required';
				$data['status'] = FALSE;
			}

			if($this->input->post('email_pusat') == '')
			{
				$data['inputerror'][] = 'email_pusat';
				$data['error_string'][] = 'Email Pusat is required';
				$data['status'] = FALSE;
			}
		}

		
        
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

    public function get_data()
	{        
		$list = $this->pihak_ketiga->get_datatables();

		$data = array();
        $no = $_POST['start'];
        
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->nama_perusahaan;
			$row[] = $field->alamat_perusahaan;
            $row[] = $field->no_telepon;
            $row[] = $field->email;
            $row[] = $field->status;
			// $row[] = '<a href="'.base_url('pihak_ketiga/detail/'.$field->id).'" class="btn btn-sm btn-primary">Detail</a>
			// <a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_pihak_ketiga('."'".$field->id."'".')"> Edit</a>
			// <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_pihak_ketiga('."'".$field->id."'".')">Delete</a>';
			$row[] = "<a class='btn btn-primary' href=".base_url('pihak_ketiga/detail/'.$field->id).">Detail</a>";
			$row[] = "";

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->pihak_ketiga->count_all(),
			"recordsFiltered" => $this->pihak_ketiga->count_filtered(),
			"data" => $data,
		);
		// output dalam format JSON
		echo json_encode($output);
	}

}

/* End of file Pihak_ketiga.php */
