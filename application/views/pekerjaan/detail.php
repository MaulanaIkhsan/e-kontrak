<?php
$this->load->view('template/header');?>
<?php $this->load->view('template/asset_header');?>
<link rel="stylesheet" type="text/css" href="<?php print(base_url('assets/bower_components/datatables.net-bs/css/custom_button_export.css'))?>" />
<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <!-- Box Form Pengunjung -->
        <?php print($this->session->flashdata('alert')); ?>
        <?php print($this->session->flashdata('success')); ?>
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-warning" id="form-pengunjung">
              <div class="box-header with-border">
                <h3 class="box-title">Detail Pekerjaan</h3>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <form class="form-horizontal" method="post" action="<?php print(base_url('pengunjung/add'))?>">
                <div class="box-body">
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Kode Program</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->kode_program); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Nama Program</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->nama_program); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Kode Pekerjaan</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->kode_pekerjaan); ?></div>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Nama Pekerjaan</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->nama_pekerjaan); ?></div>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Tahun</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->tahun); ?></div>
                    </div>
                  </div>  
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Anggaran</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print('Rp '.number_format($detail->anggaran_awal, 0, '.','.').',-'); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Tahap</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->tahap_pekerjaan); ?></div>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Keterangan</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->keterangan); ?></div>
                    </div>
                  </div>            
                </div>
                 <div class="box-footer">
                  <a href="<?php print(base_url('program/detail/'.$detail->program_id));?>" class="btn btn-primary">Kembali</a>
                </div>
                <!-- /.box-footer -->
              </form>

                <!-- Custom tabs (Charts with tabs)-->
                <div class="nav-tabs-custom">
                    <!-- Tabs within a box -->
                    <ul class="nav nav-tabs pull-left">
                        <li class="active"><a href="#data-kegiatan" data-toggle="tab">Data Kegiatan</a></li>
                        <li><a href="#data-riwayat-anggaran" data-toggle="tab">Riwayat Anggaran</a></li>
                    </ul>
                    <div class="tab-content no-padding">
                        <div class="chart tab-pane active" id="data-kegiatan" style="position: relative;">
                            <p></p>
                            <a href="<?php print(base_url('aktivitas/add/')); ?>" class="btn btn-primary">Input Kegiatan</a>
                            <p></p>
                            <table id="kegiatan" class="table table-bordered table-striped table-responsive">
                              <thead>
                                  <tr class="headings" align="center">
                                      <th class="column-title" align="center">No</th>
                                      <th class="column-title" align="center">Nama Kegiatan</th>
                                      <th class="column-title" align="center">Tahun</th>
                                      <th class="column-title" align="center">PPKOM</th>
                                      <th class="column-title" align="center">PPTK</th>
                                      <th class="column-title" align="center">Jenis Pekerjaan</th>
                                      <th class="column-title" align="center">Metode Pengadaan</th>
                                      <th class="column-title" align="center">Anggaran</th>
                                      <th class="column-title" align="center">Aksi</th>
                                  </tr>
                              </thead>
                              <tbody></tbody>
                            </table>
                        </div>
                        <div class="chart tab-pane" id="data-riwayat-anggaran" style="position: relative;">
                          <div>
                            <table id="riwayat-anggaran" class="table table-bordered table-striped table-responsive">
                              <thead>
                                    <tr class="headings" align="center">
                                        <th class="column-title" align="center">Tahap</th>
                                        <th class="column-title" align="center">Anggaran</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php foreach($anggaran as $item): ?>
                                  <tr>
                                      <th><?php print($item->tahap_pekerjaan); ?></th>
                                      <th><?php print('Rp '.number_format($item->anggaran, 0, '.','.').',-'); ?></th>
                                  </tr>
                                  <?php endforeach;?>
                                </tbody>
                            </table>
                          </div>
                            
                        </div>
                    </div>
                </div>
                <!-- /.nav-tabs-custom -->

            </div>
          </div>
          <!-- /Box Form Pengunjung -->

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/dataTables.buttons.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.flash.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/jszip.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/pdfmake.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/vfs_fonts.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.html5.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.print.min.js'))?>"></script>
<script>
  $(function(){
    $.fn.dataTable.ext.errMode = 'none';
    
    var data = $('#kegiatan').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
            "ajax": {
                "url": "<?php echo site_url('aktivitas/get_data/'.$detail->id);?>",
                "type": "POST"
            },
            dom: 'lBfrtip',
            button:[{
              extend:'pdf',
              oriented:'potrait',
              pageSize: 'A4',
              title : 'Data Pekerjaan',
              download : 'open'
            }, 'copy', 'csv', 'excel', 'pdf', 'print'],
            "columnDefs": [{ 
                "targets": [ 0 ], 
                "orderable": false, 
            }]
        });
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>
<?php $this->load->view('template/footer');?>