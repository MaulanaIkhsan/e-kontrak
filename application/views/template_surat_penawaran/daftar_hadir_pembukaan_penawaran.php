<!DOCTYPE html>
<HtmL>

<HeAd>
    <MeTa HTTP-EquIV="CONTENT-TYPE" CONTent="text/html; charset=utf-8">
    <TItlE>Semarang, 29 Maret 2004</tITLE>
    <meta name=GENERATOR coNTeNT="LibreOffice 4.1.6.2 (Linux)">
    <Meta nAMe="AUTHOR" CONTeNt="DPU">
    <MEtA NaMe=CREATED cONtENt="Tahun_Anggaran0817;20400000000000">
    <MeTA NAmE="CHANGEDBY" CoNteNt="isan">
    <META NAme="CHANGED" coNTent="20190320;163019000000000">
    <MEtA nAmE=KSOProductBuildVer CONTEnt=1033-10.1.0.6757>
    <sTyLE>
        <!-- @page {
            size: 8.47in 13.98in;
            margin-right: 0.88in;
            margin-top: 0.59in;
            margin-bottom: 0.69in
        }
        P {
            margin-bottom: 0.08in;
            direction: ltr;
            color: #000000
        }
        P.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
        H1 {
            margin-left: 3.5in;
            margin-top: 0in;
            margin-bottom: 0in;
            direction: ltr;
            color: #000000;
            text-align: justify;
            text-decoration: underline
        }
        H1.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
        -->
    </sTYLE>
</heAd>

<bOdY LANg="en-US" tExT=#000000 dIR="LTR">
    <b>DAFTAR</b>
    <fOnt SIZE=2 sTYle="font-size: 11pt"><B> HADIR</b></FONt>
    </p>
    <p clAsS="western" STyLE="margin-bottom: 0in">
        <FONt SIZE=2 stYLE="font-size: 11pt"><SPAn LanG=pt-BR>Program		:
	Judul_Program  </sPAN></font>
    </p>
    <p ClASs=western stYLE="margin-bottom: 0in">
        <FoNT SIZE=2 StYle="font-size: 11pt"><sPaN lanG="pt-BR">Kegiatan		:
	Judul_Kegiatan</spaN></fONT>
    </p>
    <p CLAsS="western" StYle="margin-left: 1.5in; text-indent: -1.5in; margin-bottom: 0in">
        <fONT SIZE=2 STYle="font-size: 11pt"><SPAN LaNg=pt-BR>Pekerjaan 		:
	</spAN></Font>
        <fOnt SIZE=2 StyLE="font-size: 11pt"><SPan laNg="pt-BR"><b>Judul_Pekerjaan</B></spaN></FONt>
    </p>
    <P LAnG="en-US" CLass="western" ALIGN=JUSTIFY sTYLE="margin-bottom: 0in">
        <Br>
    </p>
    <P CLaSS="western" sTyLE="margin-bottom: 0in">
        <FOnT SIZE=2 stylE="font-size: 11pt"><SpaN lanG="pt-BR">Hari		:	Hari_PmsknPenawaran
</SpaN></foNt>
    </p>
    <p Class="western" sTYlE="margin-bottom: 0in">
        <fOnT SIZE=2 STYlE="font-size: 11pt"><SPaN lAnG="pt-BR">Tanggal		:	Tanggal_PmsknPenawaran</sPAn></font>
    </p>
    <p CLASS="western" StYLE="margin-bottom: 0in">
        <FonT SIZE=2 STYLe="font-size: 11pt"><SpAn Lang="pt-BR">Tempat		:	Ruang
Rapat Dinas </SpaN></Font>
        <fOnt SIZE=2 STyLe="font-size: 11pt"><spAn lANg="id-ID">Pekerjaan
Umum</sPan></fOnT>
        <FOnt SIZE=2 stylE="font-size: 11pt"><sPaN lang=pt-BR>
Kota Semarang</SpAN></FOnT>
    </P>

    <p LANg="pt-BR" CLASS=western sTylE="margin-bottom: 0in">
        <Br>
    </P>
    <P clAsS="western" StYle="margin-bottom: 0in">
        <FoNt SIZE=2 Style="font-size: 11pt"><SPAn LANg="pt-BR">Acara		:	</SPAn></FOnt>
        <FONt SIZE=2 sTyle="font-size: 11pt"><spAn LANg=pt-BR><B>PEMBUKAAN
PENAWARAN</B></SpAn></fOnt>
    </P>
    <p LaNg=pt-BR ClASS=western ALIGN=JUSTIFY StyLe="margin-left: 0.49in; margin-bottom: 0in">
        <bR>
    </P>

    <p LaNG=en-US CLass="western" ALIGN=JUSTIFY sTyLe="margin-bottom: 0in">
        <FOnt SIZE=2 STyle="font-size: 11pt"><B>PEJABAT PENGADAAN</b></fonT>
    </P>
    <p LANG="en-US" CLASs="western" ALIGN=JUSTIFY style="margin-bottom: 0in">
        <bR>
    </P>
    <cENteR>
        <tAblE WIDTH=603 CELLPADDING=7 CELLSPACING=0>
            <Col WIDTH=23>
                <coL WIDTH=244>
                    <Col WIDTH=140>
                        <coL WIDTH=139>
                            <TR VALIGN=TOP>
                                <Td WIDTH=23 StYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p lAnG="en-US" class=western ALIGN=CENTER>
                                        <fONT SIZE=2 STYle="font-size: 11pt">No</fONt>
                                    </p>
                                </tD>
                                <td WIDTH=244 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p laNG=en-US cLasS="western" ALIGN=CENTER>
                                        <FonT SIZE=2 STYLE="font-size: 11pt">Nama</Font>
                                    </P>
                                </tD>
                                <Td WIDTH=140 stYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LANg=en-US cLaSS="western" ALIGN=CENTER>
                                        <FoNT SIZE=2 sTYle="font-size: 11pt">Jabatan</FOnT>
                                    </p>
                                </tD>
                                <TD WIDTH=139 sTYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p LaNg=en-US cLASs=western ALIGN=CENTER>
                                        <foNT SIZE=2 styLE="font-size: 11pt">Tanda
				Tangan</fONT>
                                    </P>
                                </Td>
                            </TR>
                            <tR VALIGN=TOP>
                                <TD WIDTH=23 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p lAng=en-US CLaSS=western ALIGN=CENTER StYle="margin-bottom: 0in">
                                        <BR>
                                    </p>
                                    <P LANG="en-US" cLaSS=western ALIGN=CENTER STYLe="margin-bottom: 0in">
                                        <FOnt SIZE=2 STyLE="font-size: 11pt">1.</font>
                                    </P>
                                    <P LANg="en-US" CLaSs="western" ALIGN=CENTER>
                                        <bR>
                                    </p>
                                </td>
                                <Td WIDTH=244 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P laNG=en-US Class=western ALIGN=JUSTIFY sTylE="margin-bottom: 0in">
                                        <Br>
                                    </p>
                                    <P laNG="en-US" cLasS=western ALIGN=JUSTIFY><font SIZE=2 sTYle="font-size: 11pt">NamaPejabat_Pengadaan</fONt>
                                    </p>
                                </TD>
                                <Td WIDTH=140 sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p lang=en-US ClASS=western ALIGN=CENTER STyle="margin-bottom: 0in">
                                        <Br>
                                    </P>
                                    <P lANg=en-US clASS="western" ALIGN=CENTER>
                                        <foNt SIZE=2 StylE="font-size: 11pt">Pejabat
				Pengadaan</font>
                                    </P>
                                </TD>
                                <Td WIDTH=139 StYle="border: 1px solid #000000; padding: 0in 0.08in">
                                    <P lANg=en-US cLASS=western ALIGN=CENTER STYle="margin-bottom: 0in">
                                        <br>
                                    </P>
                                    <p lang="en-US" ClASs=western ALIGN=CENTER>………………<font SIZE=2 styLE="font-size: 11pt">..</fOnt>
                                    </p>
                                </td>
                            </TR>
        </TABle>
    </ceNter>
    <p lAnG=en-US ClAss="western" ALIGN=JUSTIFY STYLe="margin-bottom: 0in">
        <bR>
    </P>
    <p laNg=en-US cLASS=western ALIGN=JUSTIFY sTYLE="margin-bottom: 0in">
        <Br>
    </p>
    <p LAnG=en-US clASs=western ALIGN=JUSTIFY stylE="margin-bottom: 0in">
        <FoNT SIZE=2 STyLe="font-size: 11pt"><b>PENYEDIA BARANG</b></font>
    </p>
    <p LANg=en-US ClASS=western ALIGN=JUSTIFY sTYlE="margin-bottom: 0in">
        <br>
    </P>
    <ceNTEr>
        <TaBLe WIDTH=625 CELLPADDING=7 CELLSPACING=0>
            <COl WIDTH=22>
                <COl WIDTH=230>
                    <Col WIDTH=175>
                        <COL WIDTH=140>
                            <TR VALIGN=TOP>
                                <tD WIDTH=22 sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LAnG=en-US ClASs=western ALIGN=CENTER>
                                        <FOnT SIZE=2 StylE="font-size: 11pt">No
				</fOnT>
                                    </p>
                                </Td>
                                <td WIDTH=230 stYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LANg="en-US" ClASS="western" ALIGN=CENTER>
                                        <FOnT SIZE=2 StylE="font-size: 11pt">Nama
				Perusahaan</fONT>
                                    </P>
                                </tD>
                                <td WIDTH=175 StYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LAng=en-US ClASS="western" ALIGN=CENTER><font SIZE=2 stYle="font-size: 11pt">Nama
				Peserta</FONT>
                                    </p>
                                </td>
                                <td WIDTH=140 styLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p lANg="en-US" ClAss="western" ALIGN=CENTER>
                                        <fONT SIZE=2 StylE="font-size: 11pt">Tanda
				Tangan</FOnt>
                                    </P>
                                </tD>
                            </TR>
                            <Tr VALIGN=TOP>
                                <Td WIDTH=22 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LAnG="en-US" Class=western ALIGN=CENTER sTYLe="margin-bottom: 0in">
                                        <br>
                                    </p>
                                    <p lang="en-US" class="western" ALIGN=CENTER sTylE="margin-bottom: 0in">
                                        <FONT SIZE=2 STYLE="font-size: 11pt">1.</FonT>
                                    </p>
                                    <p laNG="en-US" ClASs=western ALIGN=CENTER sTYLe="margin-bottom: 0in">
                                        <bR>
                                    </P>
                                    <P lang="en-US" CLaSs=western ALIGN=CENTER StYLE="margin-bottom: 0in">
                                        <foNt COLOr=#ffffff><fOnT SIZE=2 sTyle="font-size: 11pt">2.</FOnT>
                                        </FoNt>
                                    </p>
                                    <P LAnG="en-US" cLAsS=western ALIGN=CENTER>
                                        <br>
                                    </p>
                                </Td>
                                <Td WIDTH=230 STyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P lAng="en-US" ClASS="western" ALIGN=JUSTIFY StyLe="margin-bottom: 0in">
                                        <Br>
                                    </p>
                                    <P LaNG=en-US cLASs=western>nama_perusahaan</p>
                                </TD>
                                <tD WIDTH=175 sTyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LaNG="en-US" ClASS=western ALIGN=CENTER sTyle="margin-bottom: 0in">
                                        <br>
                                    </p>
                                    <p LaNG="en-US" cLass=western ALIGN=CENTER Style="margin-bottom: 0in">
                                        Dir_Perusahaan</P>
                                    <p LANg=en-US CLasS=western ALIGN=CENTER>
                                        <Br>
                                    </p>
                                </tD>
                                <td WIDTH=140 styLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p LAng=en-US cLass="western" ALIGN=CENTER stYlE="margin-bottom: 0in">
                                        <bR>
                                    </p>
                                    <p lANg="en-US" claSs="western" ALIGN=JUSTIFY sTYlE="margin-left: 0.32in; margin-bottom: 0in">
                                        ………………
                                        <fonT SIZE=2 STyLE="font-size: 11pt">..</FOnt>
                                    </P>
                                </tD>
                            </tR>
        </taBLE>
    </cENTER>
    <P LaNg=id-ID ClAsS="western" ALIGN=CENTER stYle="margin-bottom: 0in">
        <bR>
    </P>
</BODY>

</HTML>