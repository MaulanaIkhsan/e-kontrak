<!DOCTYPE html>
<html>

<HeaD>
    <METa HTTP-EQUiV="CONTENT-TYPE" coNteNT="text/html; charset=utf-8">
    <title>Semarang, 29 Maret 2004</tiTle>
    <MeTa naMe="GENERATOR" CONteNt="LibreOffice 4.1.6.2 (Linux)">
    <mEta nAMe=AUTHOR content=DPU>
    <Meta nAmE="CREATED" ConTeNT=Tahun_Anggaran0817;20400000000000>
    <MeTA naMe=CHANGEDBY cOntENt="isan">
    <MEta NamE="CHANGED" cOntent="20190320;163019000000000">
    <MeTa NAMe="KSOProductBuildVer" COnTeNt=1033-10.1.0.6757>
    <StylE>
        @page {
            size: 8.47in 13.98in;
            margin-right: 0.88in;
            margin-top: 0.59in;
            margin-bottom: 0.69in
        }
        P {
            margin-bottom: 0.08in;
            direction: ltr;
            color: #000000
        }
        P.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
        H1 {
            margin-left: 3.5in;
            margin-top: 0in;
            margin-bottom: 0in;
            direction: ltr;
            color: #000000;
            text-align: justify;
            text-decoration: underline
        }
        H1.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }

        * {
            margin: 0;
            padding: 0;
        }

        #textbox {
            width: 90%;
            margin: 90px auto;
            padding: 10px;
            border: 1px solid #cccccc;
        }

        .alignleft {
            float: left;
        }

        .alignright {
            float: right;
        }

        .text {
            font-family: "Times New Roman", serif;
            font-size: 14pt;
        }

        .border-table {
            border: 1px solid black;
        }
    </styLe>
</hEAd>

<boDy laNG=en-US tEXt="#000000" dIr=LTR>
    <table>
        <tr>
            <td width="300" valign="top">
                <table cellpadding="2">
                    <tr>
                        <td class="text">Nomor</td>
                        <td class="text">:</td>
                        <td class="text"><?php print($kontrak_surat->no_surat);?></td>
                    </tr>
                    <tr>
                        <td class="text">Lampiran</td>
                        <td class="text">:</td>
                        <td class="text">-</td>
                    </tr>
                    <tr>
                        <td class="text">Perihal</td>
                        <td class="text">:</td>
                        <td class="text"><strong>Undangan</strong></td>
                    </tr>
                </table>
            </td>
            <td width="400"></td>
            <td width="300" align="left" class="text" valign="top">
                Semarang, <?php print(tgl_indo($kontrak_surat->tgl_surat));?><br/>
                Yth. <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama);?><br/>
                <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_alamat);?><br/>
                di - <br/>
                Tempat
            </td>
        </tr>
    </table>
    
    
    <ol type="1">
        <li>
            Dasar
            <ol type="a">
                <li>Peraturan Presiden Nomor 4 tahun 2015 tentang Perubahan Keempat atas dasar Peraturan Presiden Nomor
54 Tahun 2010 Tentang Pengadaan barang/ Jasa Pemerintah.</li>
                <li>Peraturan Daerah Kota Semarang Nomor 16 Tahun 2016 tentang Anggaran Pendapatan dan Belanja Daerah
Kota Semarang Tahun Anggaran Tahun_Anggaran</li>
                <li>Peraturan Walikota Semarang Nomor 129 Tahun 2016 tentang Penjabaran Anggaran Pendapatan dan
Belanja Daerah Kota Semarang Tahun Anggaran Tahun_Anggaran</li>
            </ol>
        </li>
        <li>
            Dengan ini kami mengharap kesediaan Saudara untuk ikut serta dalam Pengadaan Langsung untuk : <br/>
            <br/><table>
                <tr>
                    <td width="250">Program</td>
                    <td>:</td>
                    <td><?php print($kontrak_pekerjaan->program_nama);?></td>
                </tr>
                <tr>
                    <td width="250">Pekerjaaan</td>
                    <td>:</td>
                    <td><?php print($kontrak_pekerjaan->pekerjaan_nama);?></td>
                </tr>
                <tr>
                    <td width="250">Kegiatan</td>
                    <td>:</td>
                    <td><?php print($kontrak_pekerjaan->aktivitas_nama);?></td>
                </tr>
                <tr>
                    <td width="250">Sumber Dana</td>
                    <td>:</td>
                    <td><?php print($kontrak_pekerjaan->aktivitas_sumber_dana);?></td>
                </tr>
                <tr>
                    <td width="250">Kualifikasi</td>
                    <td>:</td>
                    <td>SIUP Kecil</td>
                </tr>
                <tr>
                    <td width="250">HPS</td>
                    <td>:</td>
                    <td><?php print('Rp '.format_money($kontrak_pekerjaan->hps).',-');?></td>
                </tr>
                <tr>
                    <td width="250">Tempat dan Alamat</td>
                    <td>:</td>
                    <td>
                        <?php if(!empty($kontrak_surat->tempat_rapat_nama) and !empty($kontrak_surat->tempat_rapat_alamat)) {
                            
                        }
                        else {
                            print('Ruang Rapat Dinas Pekerjaan Umum Kota Semarang<br/>');
                            print('Jl. Madukoro Raya No. 7 Semarang');
                        } ?>
                    </td>
                </tr>
            </table><br/>
            dengan jadwal sebagai berikut:<br/><br/>
            <table CELLPADDING="7" cellspacing="0">
                <tr>
                    <th width="50" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">NO</th>
                    <th width="550" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">JADWAL KEGIATAN</th>
                    <th width="200" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">TANGGAL</th>
                    <th width="200" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">WAKTU</th>
                </tr>
                <?php 
                if(!empty($jadwal_pengadaan)) {
                    print_r($jadwal_pengadaan);
                    $no=1;
                    foreach($jadwal_pengadaan as $item):
                        print('<tr>');
                        print('<td STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in" align="center">'.$no.'</td>');
                        print('<td STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">'.$item->jenis_kegiatan_nama.'</td>');
                        print('<td STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in" align="center">'.tgl_indo($item->tgl).'</td>');
                        print('<td STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in" align="center">'.$item->jam.'</td>');
                        print('</tr>');
                    endforeach;
                } else {
                    print('<tr>');
                    print('<td colspan="4" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in" align="center">-- Kosong --</td>');
                    print('</tr>');
                }
                ?>
                
            </table>
            <br/>
        </li>
        <li>Apabila Saudara butuh keterangan dan penjelasan lebih lanjut, dapat menghubungi Pejabat Pengadaan sesuai
alamat tersebut di atas sampai dengan batas akhir pemasukan Dokumen Penawaran.</li>
    </ol>
    <br/>
    Demikian disampaikan untuk diketahui.
    <p></p>
    <table cellpadding="2">
        <tr>
            <td width="600" class="text">&nbsp;</td>
            <td align="center" width="400" class="text">
                <?php print($pejabat_pengadaan->jenis_pejabat_pengadaan_nama); ?> <?php print($pejabat_pengadaan->seri_pejabat_pengadaan_nama); ?><br/>
                Pada Dinas Pekerjaan Umum Kota Semarang<br/>
                Tahun Anggaran <?php print(date("Y"));?><br/>
                <br/><br/><br/><br/><br/>
                <u><?php print($pejabat_pengadaan->pegawai_nama); ?></u><br/>
                NIP. <?php print($pejabat_pengadaan->pegawai_nip); ?>
            </td>
        </tr>
    </table>
    
</BOdy>

</htmL>