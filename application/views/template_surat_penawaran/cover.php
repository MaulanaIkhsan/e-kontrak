<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>

<HEAD>
	<META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
	<TITLE><?php print((!empty($filename))?$filename:'Dokumen Kontrak Pekerjaan'); ?></TITLE>
	<META NAME="GENERATOR" CONTENT="LibreOffice 4.1.6.2 (Linux)">
	<META NAME="AUTHOR" CONTENT="DPU">
	<META NAME="CREATED" CONTENT="Tahun_Anggaran0817;20400000000000">
	<META NAME="CHANGEDBY" CONTENT="isan">
	<META NAME="CHANGED" CONTENT="20190320;163019000000000">
	<META NAME="KSOProductBuildVer" CONTENT="1033-10.1.0.6757">
	<STYLE TYPE="text/css">
		 @page {
			size: 8.47in 13.98in;
			margin-right: 0.88in;
			margin-top: 0.59in;
			margin-bottom: 0.69in
		}

		P {
			margin-bottom: 0.08in;
			direction: ltr;
			color: #000000
		}

		P.western {
			font-family: "Times New Roman", serif;
			font-size: 12pt;
			so-language: en-US
		}

		P.cjk {
			font-family: "Times New Roman", serif;
			font-size: 12pt;
			so-language: en-US
		}

		P.ctl {
			font-family: "Times New Roman", serif;
			font-size: 12pt;
			so-language: ar-SA
		}

		H1 {
			margin-left: 3.5in;
			margin-top: 0in;
			margin-bottom: 0in;
			direction: ltr;
			color: #000000;
			text-align: justify;
			text-decoration: underline
		}

		H1.western {
			font-family: "Times New Roman", serif;
			font-size: 12pt;
			so-language: en-US
		}

		H1.cjk {
			font-family: "Times New Roman", serif;
			font-size: 12pt;
			so-language: en-US
		}

		H1.ctl {
			font-family: "Times New Roman", serif;
			font-size: 12pt;
			so-language: ar-SA
		}

		
	</STYLE>
</HEAD>

<BODY LANG="en-US" TEXT="#000000" DIR="LTR" >
	<div style="border:3px solid black; padding:30px;">
	<P LANG="id-ID" CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<BR>
	</P>
	<P LANG="id-ID" CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<BR>
	</P>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<IMG SRC="<?php print(base_url('assets/template_surat/img/logo_pemkot_smg_color.png')) ?>" NAME="Picture 8" ALIGN=BOTTOM WIDTH=111 HEIGHT=130 BORDER=0></P>
	<P LANG="sv-SE" CLASS="western" STYLE="margin-bottom: 0in">
		<FONT SIZE=5><B> </B></FONT>
	</P>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4 STYLE="font-size: 16pt">PEMERINTAH
				KOTA SEMARANG</FONT>
		</FONT>
	</P>
	<BR>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=5><B>SURAT PERINTAH KERJA </B></FONT><br/>
			<FONT SIZE=5><B>(SPK)</B></FONT>
		</FONT>
	</P>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			
		</FONT>
	</P>
	<BR>
	<P CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4><SPAN LANG="sv-SE"><B>Nomor
						: <?php print((!empty($spk[0]->no_surat))?$spk[0]->no_surat:''); ?> Tanggal : <?php print((!empty($spk[0]->tgl_surat))?tgl_indo($spk[0]->tgl_surat):''); ?> </B></SPAN></FONT>
			
		</FONT>
	</P>
	<BR>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER>
		<FONT FACE="Tahoma, Ubuntu, sans-serif">PROGRAM <?php print($kontrak_pekerjaan->program_nama); ?> </FONT>
	</P>
	<BR>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER>
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4><B>KEGIATAN</B></FONT><br/>
			<FONT SIZE=4><?php print($kontrak_pekerjaan->pekerjaan_nama); ?></FONT>
		</FONT>
	</P>
	
	<BR>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER>
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4><B>PEKERJAAN </B></FONT><br/>
			<FONT SIZE=4><?php print($kontrak_pekerjaan->aktivitas_nama); ?></FONT>
		</FONT>
	</P>
	<BR>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4><B>ANTARA</B></FONT>
			<br/>
			<FONT SIZE=4>Kuasa Pengguna Anggaran</FONT>
			<br/>
			<FONT SIZE=4>Selaku Pejabat Pembuat Komitmen</FONT>
			<br/>
			<FONT SIZE=4><SPAN LANG="sv-SE">Pekerjaan <?php print($kontrak_pekerjaan->aktivitas_nama); ?></SPAN></FONT>
		</FONT>
	</P>
	
	<BR>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER>
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4><B>DENGAN</B></FONT>
		</FONT>
	</P>
	<div LANG="sv-SE" CLASS="western" ALIGN=CENTER>
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4><?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama); ?></FONT>
		</FONT>
	</div>
	<div CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=2 STYLE="font-size: 11pt"><SPAN LANG="sv-SE"><?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_alamat); ?></SPAN></FONT>
		</FONT>
	</div><BR />
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4 STYLE="font-size: 16pt"><B>Nilai Kontrak</B></FONT><br/>
			<FONT SIZE=4><?php print('Rp '.format_money($kontrak_pihak_ketiga[0]->nilai_kontrak).',-'); ?></FONT><br/>
			<FONT SIZE=2 STYLE="font-size: 11pt"><SPAN LANG="sv-SE"><?php print(ucwords(strtolower(terbilang($kontrak_pihak_ketiga[0]->nilai_kontrak))).' Rupiah'); ?></SPAN></FONT>
		</FONT>
	</P>
	<P CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			
		</FONT>
	</P>
	<BR>
	<P CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif"><SPAN LANG="sv-SE">Kode Rekening : </SPAN></FONT>
		<FONT FACE="Tahoma, Ubuntu, sans-serif"><SPAN LANG="sv-SE"><B><?php print($kontrak_pekerjaan->aktivitas_no_rekening);?></FONT>
	</P>
	<P CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4 STYLE="font-size: 16pt"><SPAN LANG="sv-SE">Sumber Dana : <?php print($kontrak_pekerjaan->aktivitas_sumber_dana); ?></SPAN></FONT>
		</FONT>
		
	</P>
	</div>
</BODY>

</HTML>