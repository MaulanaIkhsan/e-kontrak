<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_model extends CI_Model {

	var $main_table = 'api';

	public function show()
	{
		return $this->db->select('id, nama')
						->from($this->main_table)
						->get()->result();
	}

	public function get_api_url($nama_api)
	{
		return $this->db->select('nama, url')
						->from($this->main_table)
						->where('nama', $nama_api)
						->get()->row();
	}

}

/* End of file Api_model.php */
/* Location: ./application/models/Api_model.php */