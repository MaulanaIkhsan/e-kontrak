<?php
$this->load->view('template/header');?>
<!-- Select2 -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/select2/dist/css/select2.min.css')); ?>">
<?php $this->load->view('template/asset_header');?>
<link rel="stylesheet" type="text/css" href="<?php print(base_url('assets/bower_components/datatables.net-bs/css/custom_button_export.css'))?>" />
<link href="<?php echo base_url('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css')?>" rel="stylesheet">

<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <!-- Box Form Pengunjung -->
        <?php 
            print($this->session->flashdata('alert')); 
            print($this->session->flashdata('success')); 
        ?>
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-warning" id="form-pengunjung">
              <div class="box-header with-border">
                <h3 class="box-title">Research Content</h3>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <form class="form-horizontal" >
                <div class="box-body">
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Unit Kerja</label>
                    <div class="col-sm-9">
                      <select name="unit_kerja" id="unit_kerja">
                        <option value="">--Select Unit Kerja--</option>
                        <?php foreach($unit_kerja as $item):?>
                            <option value="<?php print($item->id); ?>"><?php print($item->nama); ?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Alamat Lengkap</label>
                    <div class="col-sm-9" id="combobox-pegawai">
                        
                    </div>
                  </div>
                                
                </div>
                <div class="box-footer">
                   
                </div>
                <!-- /.box-footer -->
              </form>

            </div>
          </div>
          <!-- /Box Form Pengunjung -->

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/dataTables.buttons.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.flash.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/jszip.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/pdfmake.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/vfs_fonts.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.html5.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.print.min.js'))?>"></script>
<script src="<?php echo base_url('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')?>"></script>

<script>
    $(function(){
        $("#unit_kerja").change(function(){
            var unit_kerja = $("#unit_kerja").val();
            if(unit_kerja){
                $.ajax({
                    type:'POST',
                    url: "<?php print(base_url('research/get_pegawai')) ?>",
                    data: "unit_kerja="+unit_kerja,
                    success: function(data){
                        
                        data = JSON.parse(data);

                        // Create combobox pegawai
                        $("#combobox-pegawai").html('<select id="pegawai" name="pegawai"></select>');
                        
                        if(data.status!=false) {
                          $("#pegawai").append('<option value="">--Select Pegawai--</option>');
                          
                          $(data).each(function(){
                            $("#pegawai").append('<option value="'+this.id+'">'+this.nama_lengkap+'</option>');
                          })
                        }
                        else {
                          $("#pegawai").html('<option value="">--Data Kosong--</option>');
                        }
                        
                    }
                });
            }
        });
    });
</script>


<?php $this->load->view('template/footer');?>