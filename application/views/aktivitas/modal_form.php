<?php 
$sess_data  = $this->session->userdata('session_data');
$user_role  = $sess_data['role'];
$user_id    = $sess_data['id'];
$user_as    = $sess_data['as'];
?>
<div class="modal fade" id="modal-import-data">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-warning"></i> Import CSV Data</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-import-csv" enctype="multipart/form-data">                  
                    <div class="form-group">
                        <label for="namalengkap" class="col-sm-2 control-label">File CSV</label>
                        <div class="col-sm-10">
                            <input type="file" name="csvfile" />
                            <span class="help-block"></span>
                        </div>
                    </div>
                
            </div>
            <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                    <button type="submit" name="simpan" class="btn btn-primary" id="btnImport">Import</button>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

</div>
</div>
<!-- /Box Form Pengunjung -->