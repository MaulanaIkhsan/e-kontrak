<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class kontrak_dokumen_lainnya_pihak_ketiga extends CI_Controller {

    /*--- kontrak_dokumen_lainnya_pihak_ketiga_pihak_ketiga ---*/
    // id
    // kontrak_pihak_ketiga_id
    // nama
    // file
    // created_at
    // updated_at

    // Configuration upload file 
    var $conf_upload = array(
        'upload_path'   => "./assets/uploads/documents_kontrak_lainnya_pihak_ketiga/",
        'allowed_types' => "pdf",
        'max_size'      => '2000',
        'encrypt_name'  => FALSE
    );

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $sess_data = $this->session->userdata('session_data');
        if($sess_data===NULL) {
            $this->session->set_flashdata('warning', '<div class="alert alert-warning fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a> 
                Harap login terlebih dahulu untuk mengakses sistem</div>');
			redirect('auth');
        }

        $this->load->model('Kontrak_dokumen_lainnya_pihak_ketiga_model', 'kontrak_dokumen_lainnya_pihak_ketiga');
        $this->load->model('Generic_model', 'generic');
    }
    
    private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('nama_dokumen_lainnya') == '')
		{
			$data['inputerror'][] = 'nama_dokumen_lainnya';
			$data['error_string'][] = 'Nama Dokumen is required';
			$data['status'] = FALSE;
        }

        if(empty($_FILES))
        {
            // Re-initialize data
            $data = array();
		    $data['error_string'] = array();
            $data['inputerror'] = array();
            
            $data['inputerror'][] = 'dokumen_lainnya';
            $data['error_string'][] = 'Dokumen Harap berformat PDF*';
            $data['status'] = FALSE;
        }
        elseif($_FILES['dokumen_lainnya']['name']!='') {
            $nama_dok = $_FILES['dokumen_lainnya']['name'];
            $split = explode('.', $nama_dok);
            $ekstensi = strtolower($split[1]);

            //'2000000' as same as 2MB
            if($_FILES['dokumen_lainnya']['size']>2000000 or $_FILES['dokumen_lainnya']['size']<=0) {
                $data['inputerror'][] = 'dokumen_lainnya';
                $data['error_string'][] = 'Size dokumen melebihi 2MB';
                $data['status'] = FALSE;
            }
            else if(strpos($this->conf_upload['allowed_types'], $ekstensi)===FALSE) {
                $data['inputerror'][] = 'dokumen_lainnya';
                $data['error_string'][] = 'Format dokumen harus PDF';
                $data['status'] = FALSE;
            }
        }
        
		if($data['status'] === FALSE)
		{
			echo json_encode((object)$data);
			exit();
		}
        
    }

    public function get_data($kontrak_pihak_ketiga_id)
	{        
        $this->load->model('Kontrak_pihak_ketiga_model', 'kontrak_pihak_ketiga');
        $this->load->model('Kontrak_pekerjaan_model', 'kontrak_pekerjaan');
        $this->load->model('Karyawan_model', 'karyawan');
		$this->load->model('Kontrak_pihak_ketiga_model', 'kontrak_pihak_ketiga');

        $kontrak_pihak_ketiga   = $this->kontrak_pihak_ketiga->fetch_data($kontrak_pihak_ketiga_id);
        $kontrak_pekerjaan      = $this->kontrak_pekerjaan->fetch_data($kontrak_pihak_ketiga->kontrak_pekerjaan_id);
        
        $sess_data  = $this->session->userdata('session_data');
        $user_id    = $sess_data['id'];
        $user_role  = $sess_data['role'];
        $user_as    = $sess_data['as'];

        $karyawan 			= NULL;
		$pihak_ketiga_id 	= NULL;
        $karyawan			= $this->karyawan->fetch_data($user_id);

        if(!empty($karyawan)) {
            $pihak_ketiga_id = $karyawan->pihak_ketiga_id;
		}
		
		$kontrak_pihak_ketiga = $this->kontrak_pihak_ketiga->fetch_data($kontrak_pihak_ketiga_id);

        // // Check data karyawan
        // if($user_as=='pihak_ketiga') {
        //     $karyawan = $this->karyawan->fetch_data(NULL, $kontrak_pihak_ketiga->pihak_ketiga_id);
        //     $contained_karyawan = FALSE;
        //     if(!empty($karyawan)) {
        //         foreach($karyawan as $item):
        //             if($karyawan->id==$user_id) {
        //                 $contained_karyawan=TRUE;
        //                 break;
        //             }
        //         endforeach;
        //     }
        // }
        
		$list = $this->kontrak_dokumen_lainnya_pihak_ketiga->get_datatables($kontrak_pihak_ketiga_id);

		$data = array();
        $no = $_POST['start'];
        
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->nama;
            if(strlen($field->file)>0) {
                $row[] = "<a href=".base_url('assets/uploads/documents_kontrak_lainnya_pihak_ketiga/'.$field->file)." target='_blank'>Dokumen</a>";
            }
            else {
                $row[] = 'Kosong';
            }

            // if($user_as=='pegawai' and 
            // ($kontrak_pekerjaan->pejabat_pengadaan_pegawai_id==$user_id or $kontrak_pekerjaan->ppkom==$user_id or
            // $user_role=='Super Admin')) {            
			//     $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_dokumen_lainnya_pihak_ketiga('."'".$field->id."'".')"> Edit</a>
			//             <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_dokumen_lainnya_pihak_ketiga('."'".$field->id."'".')">Delete</a>';
            // }
            // elseif ($user_as=='pihak_ketiga' and $contained_karyawan==TRUE) {
            //     $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_dokumen_lainnya_pihak_ketiga('."'".$field->id."'".')"> Edit</a>
			//             <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_dokumen_lainnya_pihak_ketiga('."'".$field->id."'".')">Delete</a>';
            // }


            if(($user_as=='pegawai' and $user_role=='Super Admin') or ($user_as=='pihak_ketiga' and $kontrak_pihak_ketiga->pihak_ketiga_id==$pihak_ketiga_id)) {
                $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_dokumen_lainnya_pihak_ketiga('."'".$field->id."'".')"> Edit</a>
			            <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_dokumen_lainnya_pihak_ketiga('."'".$field->id."'".')">Delete</a>';
            }

			$row[] = "";

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->kontrak_dokumen_lainnya_pihak_ketiga->count_all($kontrak_pihak_ketiga_id),
			"recordsFiltered" => $this->kontrak_dokumen_lainnya_pihak_ketiga->count_filtered($kontrak_pihak_ketiga_id),
			"data" => $data,
		);
		// output dalam format JSON
		echo json_encode($output);
	}

    
    public function ajax_add($kontrak_pihak_ketiga_id)
	{
        $this->_validate();
        $this->load->helper('generic');

        $random_name = rename_file();
        $new_name = $random_name.'.pdf';
        $this->conf_upload['file_name'] = $new_name;
        $this->load->library('upload', $this->conf_upload);
        $this->upload->initialize($this->conf_upload);

		$data = array(
                'kontrak_pihak_ketiga_id'   => $kontrak_pihak_ketiga_id,
                'nama'                      => $this->input->post('nama_dokumen_lainnya')
            );
        
        //'file' is name of textbox_upload in view
        $filename = str_replace(' ','', $_FILES['dokumen_lainnya']['name']);
        if(strlen($filename)>0) {
            $uploading = $this->upload->do_upload("dokumen_lainnya");
            if($uploading==TRUE){
                $data['file'] = $new_name;
            }
            else {
                echo json_encode(array('status' => FALSE));
                die;
            }
        }

        $insert = $this->generic->insert('kontrak_dokumen_lainnya_pihak_ketiga',$data);
        if($insert=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => FALSE));
        }
        
    }

    public function ajax_edit($id)
	{
		$data = $this->kontrak_dokumen_lainnya_pihak_ketiga->fetch_data($id);
		echo json_encode($data);
	}

	public function ajax_update()
	{
        $this->_validate();
        
        $this->load->helper('generic');

        $random_name = rename_file();
        $new_name = $random_name.'.pdf';
        $this->conf_upload['file_name'] = $new_name;
        $this->load->library('upload', $this->conf_upload);
        $this->upload->initialize($this->conf_upload);

        $id = $this->input->post('id');

		$data = array(
            'nama' => $this->input->post('nama_dokumen_lainnya')
        );
        
        //'file' is name of textbox_upload in view
        $filename = str_replace(' ','', $_FILES['dokumen_lainnya']['name']);

        if(strlen($filename)>0) {
            $uploading = $this->upload->do_upload("dokumen_lainnya");

            //Deleting old file
            $dokumen        = $this->kontrak_dokumen_lainnya_pihak_ketiga->fetch_data($id);
            if(!empty($dokumen->file)) {
                $path_dokumen   = $this->conf_upload['upload_path'].$dokumen->file;
                if($path_dokumen!=NULL and file_exists($path_dokumen)){
                    unlink($path_dokumen);
                }
            }
            
            if($uploading==TRUE){
                $data['file'] = $new_name;
            }
            else {
                echo json_encode(array('status' => 'Error upload new lampiran'));
            }
        }

        $key    = array('id'=>$id);
        $update = $this->generic->update('kontrak_dokumen_lainnya_pihak_ketiga', $data, $key);
        
        if($update=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => 'Error edit data'));
        }
        
    }

    public function ajax_delete($id)
	{
        $key = array('id'=>$id);

        //Deleting file
        $dokumen = $this->kontrak_dokumen_lainnya_pihak_ketiga->fetch_data($id);
        $path_dokumen = $this->conf_upload['upload_path'].$dokumen->file;
        if($path_dokumen!=NULL and file_exists($path_dokumen)){
            unlink($path_dokumen);
        }

		$delete = $this->generic->delete('kontrak_dokumen_lainnya_pihak_ketiga', $key);
        
        if($delete=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => FALSE));
        }
	}

}

/* End of file kontrak_dokumen_lainnya_pihak_ketiga.php */
