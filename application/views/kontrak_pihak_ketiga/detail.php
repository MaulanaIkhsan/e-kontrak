<?php
$this->load->view('template/header');?>
<!-- Select2 -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/select2/dist/css/select2.min.css')); ?>">
<?php $this->load->view('template/asset_header');?>
<link rel="stylesheet" type="text/css" href="<?php print(base_url('assets/bower_components/datatables.net-bs/css/custom_button_export.css'))?>" />
<link href="<?php echo base_url('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css')?>" rel="stylesheet">

<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <!-- Box Form Pengunjung -->
        <?php
            print($this->session->flashdata('alert'));
            print($this->session->flashdata('success'));

            $sess_kontrak_pekerjaan_id = $this->session->userdata('kontrak_pekerjaan_id');
            $sess_data          = $this->session->userdata('session_data');
            $user_as            = $sess_data['as'];
            $user_role          = $sess_data['role'];
            $user_id            = $sess_data['id'];
        ?>

        <div class="col-md-12">
            <?php if($is_dokumen_kontrak == FALSE) { ?>
                <div class="alert alert-warning fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a> 
                    Mohon lengkapi / upload dokumen kelengkapan (di master data > Penyedia Barang/Jasa > Dokumen Pelengkap) pengadaan sesuai dengan arahan pejabat pengadaan
                </div>
            <?php } ?>
        
            <?php if($is_penugasan_karyawan == FALSE) { ?>
                <div class="alert alert-warning fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a> 
                    Silahkan input karyawan perusahaan anda yang bertugas pada pekerjaan ini dan tertera pada dokumen penawaran
                </div>
            <?php } ?>
        </div>

        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-warning" id="form-pengunjung">
              <div class="box-header with-border">
                <h3 class="box-title">Detail Kontrak Penyedia Barang/Jasa</h3>
              </div>
              <!-- /.box-header -->

              <!-- form start -->
              <form class="form-horizontal" method="post">
                <div class="box-body">
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Nama Pekerjaan</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_nama_pekerjaan"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Tgl Awal Kontrak</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_tgl_awal_kontrak"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Tgl Akhir Kontrak</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_tgl_akhir_kontrak"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Pejabat Pembuat Komitmen</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_pejabat_pembuat_komitmen"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Pejabat Pengadaan</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_pejabat_pengadaan"></div>
                    </div>
                  </div>
                  <div class="form-group data-pusat">
                    <label for="namalengkap" class="col-sm-2 control-label">HPS</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_hps"></div>
                    </div>
                  </div>
                  <div class="form-group data-pusat">
                    <label for="namalengkap" class="col-sm-2 control-label">Penyedia Barang/Jasa</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_pihak_ketiga"></div>
                    </div>
                  </div>
                  <div class="form-group data-pusat">
                    <label for="namalengkap" class="col-sm-2 control-label">NPWP</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_npwp"></div>
                    </div>
                  </div>
                  <div class="form-group data-pusat">
                    <label for="namalengkap" class="col-sm-2 control-label">Rekening Bank</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_rekening_bank"></div>
                    </div>
                  </div>
                  <div class="form-group data-pusat">
                    <label for="namalengkap" class="col-sm-2 control-label">Nomor Rekening</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_no_rekening"></div>
                    </div>
                  </div>
                  <div class="form-group data-pusat">
                    <label for="namalengkap" class="col-sm-2 control-label">Nilai Penawaran</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_nilai_kontrak" class="money"></div>
                    </div>
                  </div>

                </div>
                <div class="box-footer">
                    <?php if(!empty($is_pihak_ketiga) and $is_pihak_ketiga==TRUE) { ?>
                        <a href="<?php print(base_url('kontrak_pekerjaan/resume/'));?>" class="btn btn-primary">Kembali</a>
                    <?php } elseif(!empty($sess_kontrak_pekerjaan_id)) {?>
                        <a href="<?php print(base_url('kontrak_pekerjaan/detail/'.$sess_kontrak_pekerjaan_id));?>" class="btn btn-primary">Kembali</a>
                    <?php } ?>
                    

                    <?php if(($user_as=='pegawai' and $user_role=='Super Admin') or ($user_as=='pihak_ketiga' and $kontrak_pihak_ketiga->pihak_ketiga_id==$pihak_ketiga_id)) { ?>
                        <a href="javascript:void(0)" class="btn btn-primary" onclick="edit_data()">Edit</a>
                        <a href="javascript:void(0)" class="btn btn-danger" onclick="delete_data()">Delete</a>

                        <a href="<?php print(base_url('kontrak_pihak_ketiga/download_dokumen/'.$id));?>" class="btn btn-primary pull-right" data-toggle="tooltip" title="Download semua dokumen kontrak pihak ketiga" target="_blank"><i class="fa fa-cloud-download"></i> Download Dokumen</a>
                    <?php }
                    elseif($user_as=='pegawai' and $user_role!='Super Admin' and $is_pejabat_pengadaan==TRUE) { ?>
                        <a href="javascript:void(0)" class="btn btn-danger" onclick="delete_data()">Delete</a>

                        <a href="<?php print(base_url('kontrak_pihak_ketiga/download_dokumen/'.$id));?>" class="btn btn-primary pull-right" data-toggle="tooltip" title="Download semua dokumen kontrak pihak ketiga" target="_blank"><i class="fa fa-cloud-download"></i> Download Dokumen</a>
                    <?php } ?>
                </div>
                <!-- /.box-footer -->
              </form>

              <!-- Tabs Content -->
              <div class="nav-tabs-custom">
                    <!-- Tabs within a box -->
                    <ul class="nav nav-tabs pull-left">
                        <li class="active">
                            <a href="#data-dokumen" data-toggle="tab">
                                Dokumen Profil
                                <span class="badge bg-red" id="notif-dokumen"><?php print($belum_terkoreksi); ?></span>
                            </a>
                        </li>
                        <li><a href="#data-karyawan" data-toggle="tab">Penugasan Karyawan</a></li>
                        <li><a href="#data-dokumen-lainnya" data-toggle="tab">Dokumen Penawaran</a></li>
                    </ul>
                    <div class="tab-content no-padding">
                        <div class="chart tab-pane active" id="data-dokumen" style="position: relative;">
                            <p>
                                <?php if($is_opened==TRUE and ($user_as=='pegawai' and $user_role=='Super Admin') or ($user_as=='pihak_ketiga' and $kontrak_pihak_ketiga->pihak_ketiga_id==$pihak_ketiga_id)){ ?>
                                    <a href="javascript:void(0)" class="btn btn-primary" onclick="add_dokumen_pihak_ketiga()">Insert Dokumen</a>
                                <?php } ?>
                                <?php 
                                if($user_as=='pegawai' and ($user_role=='Super Admin' or $is_pejabat_pengadaan==TRUE)) { ?>
                                    <button type="button" type="submit" id="terkoreksi" class="btn btn-primary">Terkoreksi</button> 
                                <?php } ?>    
                            </p>
                            <table id="tabel_dokumen_pihak_ketiga" class="table table-bordered table-striped table-responsive">
                                <thead>
                                    <tr class="headings" align="center">
                                        <th class="column-title" align="center"><input type="checkbox" id="master" /></th>
                                        <th class="column-title" align="center">No</th>
                                        <th class="column-title" align="center">Nama Dokumen</th>
                                        <th class="column-title" align="center">Urutan</th>
                                        <th class="column-title" align="center">Terkoreksi</th>
                                        <th class="column-title" align="center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <div class="chart tab-pane" id="data-karyawan" style="position: relative;">
                            <p>
                                <?php if($is_opened==TRUE and ($user_as=='pegawai' and $user_role=='Super Admin') or ($user_as=='pihak_ketiga' and $kontrak_pihak_ketiga->pihak_ketiga_id==$pihak_ketiga_id)){ ?>
                                    <a href="javascript:void(0)" class="btn btn-primary" onclick="add_karyawan_pihak_ketiga()">Insert Data</a>
                                <?php } ?>
                            </p>
                            <table id="tabel_karyawan_pihak_ketiga" class="table table-bordered table-striped table-responsive">
                                <thead>
                                    <tr class="headings" align="center">
                                        <th class="column-title" align="center">No</th>
                                        <th class="column-title" align="center">Nama Karyawan</th>
                                        <th class="column-title" align="center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>

                        <div class="chart tab-pane" id="data-dokumen-lainnya" style="position: relative;">
                            <p>
                                <?php if($is_opened==TRUE and ($user_as=='pegawai' and $user_role=='Super Admin') or ($user_as=='pihak_ketiga' and $kontrak_pihak_ketiga->pihak_ketiga_id==$pihak_ketiga_id)){ ?>
                                    <a href="javascript:void(0)" class="btn btn-primary" onclick="add_dokumen_lainnya_pihak_ketiga()">Insert Data</a>
                                <?php } ?>
                            </p>
                            <table id="tabel_dokumen_lainnya_pihak_ketiga" class="table table-bordered table-striped table-responsive">
                                <thead>
                                    <tr class="headings" align="center">
                                        <th class="column-title" align="center">No</th>
                                        <th class="column-title" align="center">Nama</th>
                                        <th class="column-title" align="center">File</th>
                                        <th class="column-title" align="center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!-- /Tabs Content -->

            </div>
          </div>
          <!-- /Box Form Pengunjung -->

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/dataTables.buttons.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.flash.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/jszip.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/pdfmake.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/vfs_fonts.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.html5.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.print.min.js'))?>"></script>
<script src="<?php echo base_url('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')?>"></script>
<!-- Select2 -->
<script src="<?php print(base_url('assets/bower_components/select2/dist/js/select2.full.min.js'));?>"></script>
<!-- Number divider -->
<script src="<?php print(base_url('assets/my_custom/js/number-divider.min.js')); ?>"></script>
<script>
    var table_dokumen_pihak_ketiga,
        save_method_dokumen_pihak_ketiga,
        url_dokumen_pihak_ketiga,
        save_method_pihak_ketiga,
        url_pihak_ketiga,
        table_karyawan,
        url_karyawan,
        save_method_karyawan,
        table_dokumen_lainnya_pihak_ketiga,
        url_dokumen_lainnya_pihak_ketiga,
        save_method_dokumen_lainnya_pihak_ketiga;

    $(function(){
        detail_data();

        $('.data-akun').hide();

        //Just number input
        $(".money").on("keypress keyup",function (event) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        $("input").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $("textarea").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $("select").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $('.datepicker').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
            todayHighlight: true,
            todayBtn: true,
            todayHighlight: true,
        });

        //Just number input
        $(".number").on("keypress keyup",function (event) {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        table_dokumen_pihak_ketiga = $('#tabel_dokumen_pihak_ketiga').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('kontrak_dokumen_pihak_ketiga/get_data/'.$id);?>",
                "type": "POST"
            },
            "autoWidth": false,
            dom: 'lBfrtip',
            button:[{
              extend:'pdf',
              oriented:'potrait',
              pageSize: 'A4',
              title : 'Data Kontrak Dokumen Pihak Ketiga',
              download : 'open'
            }, 'copy', 'csv', 'excel', 'pdf', 'print'],
            "columnDefs": [{
                "targets": [ 0 ],
                "orderable": false,
            }]
        });

        table_karyawan = $('#tabel_karyawan_pihak_ketiga').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('kontrak_karyawan_pihak_ketiga/get_data/'.$id);?>",
                "type": "POST"
            },
            "autoWidth": false,
            dom: 'lBfrtip',
            button:[{
              extend:'pdf',
              oriented:'potrait',
              pageSize: 'A4',
              title : 'Data Kontrak Karyawan Pihak Ketiga',
              download : 'open'
            }, 'copy', 'csv', 'excel', 'pdf', 'print'],
            "columnDefs": [{
                "targets": [ 0 ],
                "orderable": false,
            }]
        });

        table_dokumen_lainnya_pihak_ketiga = $('#tabel_dokumen_lainnya_pihak_ketiga').DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": "<?php echo site_url('kontrak_dokumen_lainnya_pihak_ketiga/get_data/'.$id);?>",
                "type": "POST"
            },
            "autoWidth": false,
            dom: 'lBfrtip',
            button:[{
              extend:'pdf',
              oriented:'potrait',
              pageSize: 'A4',
              title : 'Data Dokumen Lainnya Pihak Ketiga',
              download : 'open'
            }, 'copy', 'csv', 'excel', 'pdf', 'print'],
            "columnDefs": [{
                "targets": [ 0 ],
                "orderable": false,
            }]
        });

        // Save and Update kontrak pihak ketiga
        $('#form-kontrak-pihak-ketiga').submit(function(obj){
            obj.preventDefault();
            if(save_method_pihak_ketiga=='add') {
                url_pihak_ketiga = "<?php echo site_url('kontrak_pihak_ketiga/ajax_add/'.$id)?>";
            } else {
                url_pihak_ketiga = "<?php echo site_url('kontrak_pihak_ketiga/ajax_update/')?>";
            }
            $.ajax({
                url: url_pihak_ketiga,
                type: 'POST',
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(datas){
                    // Parsing to object
                    datas=JSON.parse(datas);

                    //if success close modal and reload ajax table dokumen
                    if(datas.hasOwnProperty('status') && datas.status)
                    {
                        alert('Data berhasil tersimpan');
                        $('#modal_form_kontrak_pihak_ketiga').modal('hide');
                        $('#form-kontrak-pihak-ketiga')[0].reset(); // reset form on modals
                        detail_data();
                    }
                    else
                    {
                        if(datas.hasOwnProperty('inputerror')) {
                            for (var i = 0; i < datas.inputerror.length; i++)
                            {
                                $('[name="'+datas.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                $('[name="'+datas.inputerror[i]+'"]').next().text(datas.error_string[i]); //select span help-block class set text error string
                            }
                        }
                    }
                    $('#btnSaveKontrakPihakKetiga').text('save'); //change button text
                    $('#btnSaveKontrakPihakKetiga').attr('disabled',false); //set button enable
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error calling save data. Please check again');
                    $('#modal_form_kontrak_pihak_ketiga').modal('hide');
                }
            });
        });

        // Save and Update kontrak dokumen pihak ketiga
        $('#form-dokumen-pihak-ketiga').submit(function(obj){
            obj.preventDefault();

            if(save_method_dokumen_pihak_ketiga == 'add') {
                url_dokumen_pihak_ketiga = "<?php echo site_url('kontrak_dokumen_pihak_ketiga/ajax_add/'.$id)?>";
            } else {
                url_dokumen_pihak_ketiga = "<?php echo site_url('kontrak_dokumen_pihak_ketiga/ajax_update')?>";
            }

            $.ajax({
                url : url_dokumen_pihak_ketiga,
                type: "POST",
                data: $('#form-dokumen-pihak-ketiga').serialize(),
                dataType: "JSON",
                success: function(data)
                {
                    console.log('data value : '+data);
                    if(data.status) //if success close modal and reload ajax table
                    {
                        alert('Data berhasil tersimpan');
                        $('#modal_dokumen_pihak_ketiga').modal('hide');
                        reload_data_dokumen_pihak_ketiga();
                    }
                    else
                    {
                        for (var i = 0; i < data.inputerror.length; i++)
                        {
                            $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                            $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                        }
                    }
                    $('#btnSaveDokumenPihakKetiga').text('save'); //change button text
                    $('#btnSaveDokumenPihakKetiga').attr('disabled',false); //set button enable

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error adding / update data');
                    $('#form-dokumen-pihak-ketiga')[0].reset();
                    $('#modal_dokumen_pihak_ketiga').modal('hide');
                }
            });
        });

        // Save and Update kontrak karyawan pihak ketiga
        $('#form-karyawan-pihak-ketiga').submit(function(obj){
            obj.preventDefault();

            if(save_method_karyawan == 'add') {
                url_karyawan = "<?php echo site_url('kontrak_karyawan_pihak_ketiga/ajax_add/'.$id)?>";
            } else {
                url_karyawan = "<?php echo site_url('kontrak_karyawan_pihak_ketiga/ajax_update')?>";
            }

            // console.log($('#form-karyawan-pihak-ketiga').serialize());
            console.log(url_karyawan);
            $.ajax({
                url : url_karyawan,
                type: "POST",
                data: $('#form-karyawan-pihak-ketiga').serialize(),
                dataType: "JSON",
                success: function(data)
                {
                    console.log('data value : '+data);
                    if(data.status) //if success close modal and reload ajax table
                    {
                        alert('Data berhasil tersimpan');
                        $('#modal_karyawan_pihak_ketiga').modal('hide');
                        reload_data_karyawan_pihak_ketiga();
                    }
                    else
                    {
                        for (var i = 0; i < data.inputerror.length; i++)
                        {
                            $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                            $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                        }
                    }
                    $('#btnSaveKaryawanPihakKetiga').text('save'); //change button text
                    $('#btnSaveKaryawanPihakKetiga').attr('disabled',false); //set button enable

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error adding / update data karyawan pihak ketiga');
                    $('#form-karyawan-pihak-ketiga')[0].reset();
                    $('#modal_karyawan_pihak_ketiga').modal('hide');
                }
            });
        });

        // Save and Update kontrak dokumen pihak ketiga
        $('#form-dokumen-lainnya-pihak-ketiga').submit(function(obj){
            obj.preventDefault();

            if(save_method_dokumen_lainnya_pihak_ketiga == 'add') {
                url_dokumen_lainnya_pihak_ketiga = "<?php echo site_url('kontrak_dokumen_lainnya_pihak_ketiga/ajax_add/'.$id)?>";
            } else {
                url_dokumen_lainnya_pihak_ketiga = "<?php echo site_url('kontrak_dokumen_lainnya_pihak_ketiga/ajax_update')?>";
            }

            $.ajax({
                url: url_dokumen_lainnya_pihak_ketiga,
                type: 'POST',
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(datas){
                    // Parsing to object
                    datas=JSON.parse(datas);

                    //if success close modal and reload ajax table dokumen
                    if(datas.hasOwnProperty('status') && datas.status)
                    {
                        alert('Data berhasil tersimpan');
                        $('#modal_form_dokumen_lainnya_pihak_ketiga').modal('hide');
                        $('#form-dokumen-lainnya-pihak-ketiga')[0].reset(); // reset form on modals
                        reload_data_dokumen_lainnya();
                    }
                    else
                    {
                        if(datas.hasOwnProperty('inputerror')) {
                            for (var i = 0; i < datas.inputerror.length; i++)
                            {
                                $('[name="'+datas.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                $('[name="'+datas.inputerror[i]+'"]').next().text(datas.error_string[i]); //select span help-block class set text error string
                            }
                        }
                    }
                    $('#btnSaveDokumenLainnyaPihakKetiga').text('save'); //change button text
                    $('#btnSaveDokumenLainnyaPihakKetiga').attr('disabled',false); //set button enable
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error calling save data. Please check again');
                    $('#modal_form_dokumen_lainnya_pihak_ketiga').modal('hide');
                }
            });
        });

        // Checkbox to checking all
        $('#master').on('click', function(event){
            if($(this).is(':checked', true)) {
                $('.sub_chk').prop('checked', true);
            } else {
                $('.sub_chk').prop('checked', false);
            }
        });

        $('#terkoreksi').on('click', function(event){
            var allVals     = [],
                url_vals    = "<?php echo site_url('kontrak_dokumen_pihak_ketiga/update_terkoreksi/')?>";;

            $(".sub_chk:checked").each(function(){
                allVals.push($(this).attr('data-id'));
            });

            if(allVals.length<=0) {
                alert('Harap pilih dokumen profil yang akan diubah status koreksinya !!!');
            } else {
                if(confirm('Apakah anda yakin akan mengubah status dokumen menjadi terkoreksi ?'))
                {
                    var join_vals = allVals.join(",");

                    $.ajax({
                        url : url_vals,
                        type : 'POST',
                        data : 'ids='+join_vals,
                        success: function(data) {
                            $('.sub_chk').prop('checked', false);
                            $('#master').prop('checked', false);

                            alert('status dokumen berhasil terkoreksi');
                            $('#notif-dokumen').text('');
                            reload_data_dokumen_pihak_ketiga();
                        },
                        error: function(data){
                            $('.sub_chk').prop('checked', false);
                            alert(data.responseText);
                            reload_data_dokumen_pihak_ketiga();
                        }
                    });

                    allVals = [];
                }
            }
        });

    });

    // Detail data kontrak pihak ketiga
    function detail_data() {
        $.ajax({
            url: "<?php echo site_url('kontrak_pihak_ketiga/ajax_edit/'.$id)?>",
            method: 'GET',
            type: 'JSON',
            success: function(data) {
                // Parsing to object
                data=JSON.parse(data);

                $('#detail_nama_pekerjaan').html(data.nama_aktivitas);
                $('#detail_tgl_awal_kontrak').text(data.tgl_awal_kontrak);
                $('#detail_tgl_akhir_kontrak').text(data.tgl_akhir_kontrak);
                $('#detail_pejabat_pembuat_komitmen').text(data.pejabat_pembuat_komitmen_nama);
                $('#detail_pejabat_pengadaan').text(data.pejabat_pengadaan_nama);
                $('#detail_hps').text(data.hps);
                $('#detail_pihak_ketiga').text(data.pihak_ketiga_nama);
                $('#detail_npwp').text(data.pihak_ketiga_npwp);
                $('#detail_rekening_bank').text(data.bank_nama);
                $('#detail_no_rekening').text(data.no_rekening);
                $('#detail_nilai_kontrak').text(data.nilai_kontrak);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error while load detail data detail kontrak kegiatan !');
            }
        });
    }

    function delete_data() {
        if(confirm('Apakah anda yakin akan menghapus data ini ?')){
            $.ajax({
                url: "<?php echo site_url('kontrak_pihak_ketiga/ajax_delete/'.$id)?>",
                method: 'POST',
                type: 'JSON',
                success: function(){
                  alert('Data berhasil dihapus');
                  window.location.replace("<?php print(base_url('kontrak_pekerjaan/detail/'.$sess_kontrak_pekerjaan_id)); ?>");

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error while delete data kontrak pihak ketiga !');
                }
            });
        }
    }

    function edit_data() {
        save_method_pihak_ketiga = 'update';
        $('#form-kontrak-pihak-ketiga')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('kontrak_pihak_ketiga/ajax_edit/'.$id)?>",
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id);
                <?php if($user_as=='pihak_ketiga' and $pihak_ketiga_id!=NULL and $pihak_ketiga_nama!==NULL) { ?>
                    $('[name="pihak_ketiga_id"]').val('<?php print($pihak_ketiga_id); ?>');
                    $('[name="pihak_ketiga_nama"]').val('<?php print($pihak_ketiga_nama); ?>');
                <?php } else { ?>
                    $('[name="pihak_ketiga_id"]').val(data.pihak_ketiga_id);
                <?php } ?>
                $('[name="nama_bank"]').val(data.bank_id);
                $('[name="no_rekening"]').val(data.no_rekening);
                $('[name="nilai_kontrak"]').val(data.nilai_kontrak);
                $('#modal_form_kontrak_pihak_ketiga').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Update Data'); // Set title to Bootstrap modal title
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    // Manage dokumen pihak ketiga
    function reload_data_dokumen_pihak_ketiga()
    {
        table_dokumen_pihak_ketiga.ajax.reload();
    }

    function add_dokumen_pihak_ketiga() {
        save_method_dokumen_pihak_ketiga = 'add';
        $('#form-dokumen-pihak-ketiga')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_dokumen_pihak_ketiga').modal('show'); // show bootstrap modal
        $('.modal-title').text('Insert Dokumen Penyedia Barang/Jasa'); // Set Title to Bootstrap modal title
    }

    function edit_dokumen_pihak_ketiga(id) {
        save_method_dokumen_pihak_ketiga = 'update';
        $('#form-dokumen-pihak-ketiga')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('kontrak_dokumen_pihak_ketiga/ajax_edit/')?>/"+id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id);
                $('[name="dokumen_pihak_ketiga_id"]').val(data.dokumen_pihak_ketiga_id);
                $('[name="urutan"]').val(data.urutan);

                $('#modal_dokumen_pihak_ketiga').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Update Dokumen Pihak Ketiga'); // Set title to Bootstrap modal title

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_dokumen_pihak_ketiga(id)
    {
        if(confirm('Are you sure delete this data?'))
        {

            // ajax delete data to database
            $.ajax({
                url : "<?php echo site_url('kontrak_dokumen_pihak_ketiga/ajax_delete')?>/"+id,
                method: 'POST',
                type: 'JSON',
                success: function(datas)
                {
                    alert('Data berhasil dihapus');
                    reload_data_dokumen_pihak_ketiga();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });

        }
    }

    function edit_pihak_ketiga()
    {
        save_method_pihak_ketiga = 'update';
        $('#form-kontrak-pihak-ketiga')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('kontrak_pihak_ketiga/ajax_edit/')?>/"+id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id);
                $('[name="pihak_ketiga_id"]').val(data.pihak_ketiga_id);
                $('[name="nama_bank"]').val(data.bank_id);
                $('[name="no_rekening"]').val(data.no_rekening);
                $('[name="nilai_kontrak"]').val(data.nilai_kontrak);
                $('#modal_form_kontrak_pihak_ketiga').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Update Data'); // Set title to Bootstrap modal title
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    //Kontrak karyawan pihak ketiga
    function reload_data_karyawan_pihak_ketiga()
    {
        table_karyawan.ajax.reload();
    }

    function add_karyawan_pihak_ketiga() {
        save_method_karyawan = 'add';
        $('#form-karyawan-pihak-ketiga')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_karyawan_pihak_ketiga').modal('show'); // show bootstrap modal
        $('.modal-title').text('Insert Data Penugasan Karyawan'); // Set Title to Bootstrap modal title
    }

    function edit_karyawan_pihak_ketiga(id) {
        save_method_karyawan = 'update';
        $('#form-karyawan-pihak-ketiga')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('kontrak_karyawan_pihak_ketiga/ajax_edit/')?>/"+id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id);
                $('[name="karyawan_id"]').val(data.karyawan_id);

                $('#modal_karyawan_pihak_ketiga').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Update Data Penugasan Karyawan'); // Set title to Bootstrap modal title

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_karyawan_pihak_ketiga(id)
    {
        if(confirm('Are you sure delete this data?'))
        {

            // ajax delete data to database
            $.ajax({
                url : "<?php echo site_url('kontrak_karyawan_pihak_ketiga/ajax_delete')?>/"+id,
                method: 'POST',
                type: 'JSON',
                success: function(datas)
                {
                    alert('Data berhasil dihapus');
                    reload_data_karyawan_pihak_ketiga();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });

        }
    }

    //Kontrak karyawan pihak ketiga
    function reload_data_dokumen_lainnya()
    {
        table_dokumen_lainnya_pihak_ketiga.ajax.reload();
    }

    function add_dokumen_lainnya_pihak_ketiga() {
        save_method_dokumen_lainnya_pihak_ketiga = 'add';
        $('#form-dokumen-lainnya-pihak-ketiga')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form_dokumen_lainnya_pihak_ketiga').modal('show'); // show bootstrap modal
        $('.modal-title').text('Insert Data Dokumen Lainnya'); // Set Title to Bootstrap modal title
    }

    function edit_dokumen_lainnya_pihak_ketiga(id) {
        save_method_dokumen_lainnya_pihak_ketiga = 'update';
        $('#form-dokumen-lainnya-pihak-ketiga')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('kontrak_dokumen_lainnya_pihak_ketiga/ajax_edit/')?>/"+id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id);
                $('[name="nama_dokumen_lainnya"]').val(data.nama);

                $('#modal_form_dokumen_lainnya_pihak_ketiga').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Update Data Dokumen Lainnya'); // Set title to Bootstrap modal title

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_dokumen_lainnya_pihak_ketiga(id)
    {
        if(confirm('Are you sure delete this data?'))
        {

            // ajax delete data to database
            $.ajax({
                url : "<?php echo site_url('kontrak_dokumen_lainnya_pihak_ketiga/ajax_delete')?>/"+id,
                method: 'POST',
                type: 'JSON',
                success: function(datas)
                {
                    alert('Data berhasil dihapus');
                    reload_data_dokumen_lainnya();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });

        }
    }

</script>

<?php
    // Load Modal Form
    $this->load->view('kontrak_pihak_ketiga/modal_form');
    $this->load->view('kontrak_dokumen_pihak_ketiga/modal_form');
    $this->load->view('kontrak_karyawan_pihak_ketiga/modal_form');
    $this->load->view('kontrak_dokumen_lainnya_pihak_ketiga/modal_form');

?>

<?php $this->load->view('template/footer');?>
