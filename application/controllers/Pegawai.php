<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller {

    public function __construct()
	{
        parent::__construct();

        $sess_data = $this->session->userdata('session_data');
        if($sess_data===NULL) {
            $this->session->set_flashdata('warning', '<div class="alert alert-warning fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a> 
                Harap login terlebih dahulu untuk mengakses sistem</div>');
			redirect('auth');
        }

        $this->load->library('grocery_CRUD');
        $this->load->model('Pegawai_model', 'pegawai');
        $this->load->model('Generic_model', 'generic');
    }
    
    public function index()
    {
        // ----- Pegawai -----
        // id
        // nip
        // nama_lengkap
        // jenis_kelamin ('Pria', 'Wanita')
        // alamat
        // no_telepon
        // is_pegawai ('y','n')
        // unit_id
        // jabatan_id
        // role_id
        // username
        // password
        // created_at
        // updated_at
        
        $sess_data = $this->session->userdata('session_data');
        
        $title = 'Data Pegawai';
		$table = 'pegawai';

        $crud = new grocery_CRUD();

        $crud->set_table($table);

        // Set Relation
        $crud->set_relation('unit_id', 'unit_kerja', 'nama');
        $crud->set_relation('role_id', 'role', 'nama');
        $crud->set_relation('jabatan_id', 'jabatan_fungsional', 'nama');

        //list of columns that displayed to user
        $crud->columns('nip','nama_lengkap', 'jenis_kelamin', 'alamat', 
                    'no_telepon','unit_id', 'jabatan_id', 'role_id');
        
        $crud->display_as('nip', 'NIP');
        $crud->display_as('nama_lengkap', 'Nama Lengkap');
        $crud->display_as('alamat', 'Alamat');
        $crud->display_as('no_telepon', 'Telepon');
        $crud->display_as('is_pegawai', 'Pegawai ?');
        $crud->display_as('jabatan_id', 'Jabatan');
        $crud->display_as('unit_id', 'Unit Kerja');
        $crud->display_as('role_id', 'Role');

        $crud->unique_fields(array('username'));

        // Set columns that edit and insert operation
        $crud->fields('nip','nama_lengkap', 'jenis_kelamin', 'alamat', 
                'no_telepon', 'is_pegawai', 'unit_id', 'jabatan_id' ,'role_id', 
                'username', 'password');
        
        // Field type beside textbox
        $crud->field_type('is_pegawai', 'dropdown', array('y'=>'Ya', 'n'=>'Tidak'));
        $crud->field_type('password', 'password');

        // Action to encrypt & decrypt password
        $crud->callback_before_insert(array($this, 'encrypt_password_callback'));
        $crud->callback_before_update(array($this, 'encrypt_password_callback'));
        $crud->callback_edit_field('password', array($this, 'show_password_callback'));

        $crud->unset_columns(array('created_at', 'updated_at', 'id'));

        // Disable button clone
        $crud->unset_clone();
        // $crud->unset_read();
        if(($sess_data['role']=='Staff') or ($sess_data['role']=='Admin')) {
            $crud->unset_edit();
            $crud->unset_delete();
            $crud->unset_add();
        }

		// Required field
        $crud->required_fields('nip','nama_lengkap', 'jenis_kelamin', 'alamat'
                ,'is_pegawai', 'unit_id', 'jabatan_id' ,'role_id', 'username');

        // Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);		
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
    }

    function encrypt_password_callback($post_array, $primary_key=null) 
    {
        // if(strlen($post_array['password'])>0) {
        //     $options = ['cost' => 13];
        //     $post_array['password'] = password_hash($post_array['password'], PASSWORD_BCRYPT, $options);
        // }

        if(empty($post_array['password'])) {
            unset($post_array['password'], $post_array['konfirmpass']);
        }
        else {
            $options = ['cost' => 13];
            $post_array['password'] = password_hash($post_array['password'], PASSWORD_BCRYPT, $options);
        }

        return $post_array;
    }

    function show_password_callback($value)
    {
        return "<input type='password' name='password' value='' />";
    }

    public function pejabat_pengadaan()
    {
        // ----- pejabat_pengadaan -----
        // id
        // pegawai_id
        // jenis_pejabat_pengadaan_id
        // tingkat_id
        // sk_id
        // tahun
        // created_at
        // updated_at

        $sess_data = $this->session->userdata('session_data');

        $title = 'Data Pejabat Pengadaan';
		$table = 'pejabat_pengadaan';

        $crud = new grocery_CRUD();

        $crud->set_table($table);

        // Set Relation
        $crud->set_relation('pegawai_id', 'pegawai', 'nama_lengkap');
        $crud->set_relation('jenis_pejabat_pengadaan_id', 'jenis_pejabat_pengadaan', 'nama');
        $crud->set_relation('tingkat_id', 'seri_pejabat_pengadaan', 'nama');
        $crud->set_relation('sk_id', 'sk_pejabat_pengadaan', 'no_sk');

        $crud->columns('pegawai_id','jenis_pejabat_pengadaan_id', 'tingkat_id', 'tahun');
        
        $crud->display_as('pegawai_id', 'Nama Pegawai');
        $crud->display_as('jenis_pejabat_pengadaan_id', 'Jenis Pejabat Pengadaan');
        $crud->display_as('tingkat_id', 'Tingkat');
        $crud->display_as('tahun', 'Tahun');
        $crud->display_as('sk_id', 'Nomor SK');

        // $crud->unique_fields(array('pegawai_id','jenis_pejabat_pengadaan_id', 'tingkat_id', 'tahun'));

        // Set columns that show, edit and insert operation
        $crud->fields('pegawai_id','jenis_pejabat_pengadaan_id', 'tingkat_id', 'sk_id', 'tahun');
        
        $tahun_value = [];
        $curr_year = (int)date("Y");
        $limit_year = (int)4;
        $item=$curr_year-$limit_year;

        while ($curr_year>=$item) {
            $tahun_value[$curr_year] = $curr_year;
            
            $curr_year--;
        }

        // Field type beside textbox
        $crud->field_type('tahun', 'dropdown', $tahun_value);

        $crud->unset_columns(array('created_at', 'updated_at', 'id'));

        // Disable button clone
        $crud->unset_clone();
        if($sess_data['role']=='Staff') {
            $crud->unset_edit();
            $crud->unset_delete();
            $crud->unset_add();
        }

		// Required field
        $crud->required_fields('pegawai_id','jenis_pejabat_pengadaan_id', 'tingkat_id', 'sk_id','tahun');

        // Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);		
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
    }

    public function sk_pejabat_pengadaan()
	{
		/*--- sk_pejabat_pengadaan ---*/
		// id
		// no_sk
		// tahun
		// tgl_penetapan
		// created_at
		// updated_at

		$title = 'SK Pejabat Pengadaan';
		$table = 'sk_pejabat_pengadaan';

		$crud = new grocery_CRUD();

		$crud->set_table($table);
		$crud->columns('no_sk', 'tahun', 'tgl_penetapan');

		$crud->set_subject($title);
		$crud->display_as('no_sk', 'Nomor SK');
		$crud->display_as('tahun', 'Tahun');
		$crud->display_as('tgl_penetapan', 'Tgl Penetapan');

        // Set columns that show, edit and insert operation
		$crud->fields('no_sk', 'tahun', 'tgl_penetapan');

		// Disable button clone
		$crud->unset_clone();

		// Required field
		$crud->required_fields('no_sk', 'tahun', 'tgl_penetapan');

		// Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);		
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
    }
    
    public function pejabat_pembuat_komitmen()
    {
        // ----- pejabat_pembuat_komitmen -----
        // id
        // pegawai_id
        // sk_id
        // tahun
        // created_at
        // updated_at

        $sess_data = $this->session->userdata('session_data');

        $title = 'Data Pejabat Pembuat Komitmen';
		$table = 'pejabat_pembuat_komitmen';

        $crud = new grocery_CRUD();

        $crud->set_table($table);

        // Set Relation
        $crud->set_relation('pegawai_id', 'pegawai', 'nama_lengkap');
        $crud->set_relation('sk_id', 'sk_pejabat_pembuat_komitmen', 'no_sk');

        $crud->columns('pegawai_id', 'sk_id', 'tahun');
        
        $crud->display_as('pegawai_id', 'Nama Pegawai');
        $crud->display_as('sk_id', 'Nomor SK');
        $crud->display_as('tahun', 'Tahun');

        // $crud->unique_fields(array('pegawai_id','jenis_pejabat_pengadaan_id', 'tingkat_id', 'tahun'));

        // Set columns that show, edit and insert operation
        $crud->fields('pegawai_id', 'sk_id', 'tahun');
        
        $tahun_value = [];
        $curr_year = (int)date("Y");
        $limit_year = (int)4;
        $item=$curr_year-$limit_year;

        while ($curr_year>=$item) {
            $tahun_value[$curr_year] = $curr_year;
            
            $curr_year--;
        }

        // Field type beside textbox
        $crud->field_type('tahun', 'dropdown', $tahun_value);

        $crud->unset_columns(array('created_at', 'updated_at', 'id'));

        // Disable button clone
        $crud->unset_clone();
        if($sess_data['role']=='Staff') {
            $crud->unset_edit();
            $crud->unset_delete();
            $crud->unset_add();
        }

		// Required field
        $crud->required_fields('pegawai_id', 'sk_id','tahun');

        // Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);		
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
    }

    public function sk_pejabat_pembuat_komitmen()
    {
        /*--- sk_pejabat_pengadaan ---*/
		// id
		// no_sk
		// tahun
		// tgl_penetapan
		// created_at
		// updated_at

		$title = 'SK Pejabat Pembuat Komitmen';
		$table = 'sk_pejabat_pembuat_komitmen';

		$crud = new grocery_CRUD();

		$crud->set_table($table);
		$crud->columns('no_sk', 'tahun', 'tgl_penetapan');

		$crud->set_subject($title);
		$crud->display_as('no_sk', 'Nomor SK');
		$crud->display_as('tahun', 'Tahun');
		$crud->display_as('tgl_penetapan', 'Tgl Penetapan');

        // Set columns that show, edit and insert operation
		$crud->fields('no_sk', 'tahun', 'tgl_penetapan');

		// Disable button clone
		$crud->unset_clone();

		// Required field
		$crud->required_fields('no_sk', 'tahun', 'tgl_penetapan');

		// Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);		
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
    }

    private function _validate($is_update=NULL, $id=NULL)
	{
        $this->load->helper('generic');
                
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('nip') == '')
		{
			$data['inputerror'][] = 'nip';
			$data['error_string'][] = 'NIP name is required';
			$data['status'] = FALSE;
		}        
        
		if($this->input->post('nama_lengkap') == '')
		{
			$data['inputerror'][] = 'nama_lengkap';
			$data['error_string'][] = 'Nama Lengkap is required';
			$data['status'] = FALSE;
        }
        
        if($this->input->post('jenis_kelamin') == '')
		{
			$data['inputerror'][] = 'jenis_kelamin';
			$data['error_string'][] = 'Jenis Kelamin is required';
			$data['status'] = FALSE;
		}

        if($this->input->post('alamat') == '')
		{
			$data['inputerror'][] = 'alamat';
			$data['error_string'][] = 'Alamat is required';
			$data['status'] = FALSE;
        }
        
        if($this->input->post('no_telepon') == '')
		{
			$data['inputerror'][] = 'no_telepon';
			$data['error_string'][] = 'Jabatan is required';
			$data['status'] = FALSE;
        }
        
        if($this->input->post('unit_kerja') == '')
		{
			$data['inputerror'][] = 'unit_kerja';
			$data['error_string'][] = 'Alamat is required';
			$data['status'] = FALSE;
        }

        if($is_update == 'update')
		{
			if($this->input->post('username') == '')
            {
                $data['inputerror'][] = 'username';
                $data['error_string'][] = 'Username is required';
                $data['status'] = FALSE;
            }
            else if($this->input->post('username') != '')
            {
                $username = $this->input->post('username');
                
                // get 1 data include $id value
                $include = $this->pegawai->check_username($username, $id);
                // With data exclude $id value
                $exclude = (array)$this->pegawai->check_username($username, $id, 'y');
                $index_exclude = index_by_column($exclude, 'username', $username);

                if(count((array)$include)>0) {
                    if( ($include->username!=$username) and (($index_exclude!==FALSE) or ($index_exclude!="0")) ) {
                        $data['inputerror'][] = 'username';
                        $data['error_string'][] = 'Username sudah digunakan, harap cari yang lain';
                        $data['status'] = FALSE;
                    }
                } 
                else {
                    if (($index_exclude!==FALSE) or ($index_exclude!="0")) {
                        $data['inputerror'][] = 'username';
                        $data['error_string'][] = 'Username sudah digunakan, harap cari yang lain';
                        $data['status'] = FALSE;
                    }
                }
            }

            

        }
        if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
        }
    }
    
    public function ajax_edit($id)
	{
		$data = $this->pegawai->fetch_data($id);
		echo json_encode($data);
	}

	public function ajax_update()
	{      
        $this->_validate('update', $this->input->post('id'));
        
        $sess_data = $this->session->userdata('session_data');
		$data = array(
                'nip'           => $this->input->post('nip'),
                'nama_lengkap' 	=> $this->input->post('nama_lengkap'),
                'jenis_kelamin' => $this->input->post('jenis_kelamin'),
                'alamat' 	    => $this->input->post('alamat'),
                'no_telepon' 	=> $this->input->post('no_telepon'),
                'unit_id' 	    => $this->input->post('unit_kerja'),
                'username' 	    => $this->input->post('username')
            );

        if(strlen($this->input->post('password'))>0) {
            $options = ['cost' => 13];
            $data['password'] = password_hash($this->input->post('password'), PASSWORD_BCRYPT, $options);
        }

        $key = array('id'=>$this->input->post('id'));
		$update = $this->generic->update('pegawai', $data, $key);
        
        if($update=='ok') {
            echo json_encode(array("status" => TRUE));
            exit();
        }
        else {
            echo json_encode(array("status" => FALSE));
            exit();
        }
    }

    public function profil()
    {
        $this->load->model('Unit_kerja_model', 'unit_kerja');
        $this->load->model('Role_model', 'role');
        
        $data = array(
            'unit_kerja'    => $this->unit_kerja->fetch_data(),
            'role'          => $this->role->fetch_data()
        );
        
        $this->load->view('pegawai/profil', $data);
    }

}

/* End of file Pegawai.php */
