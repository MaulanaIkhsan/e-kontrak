<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_upload_dokumen" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Upload</h4>
            </div>
            <div class="modal-body form">
                <div class="callout callout-danger">
                    <h4>Perhatian!</h4>

                    <p>Harap upload dokumen dengan format <strong><u>PDF</u></strong> dan size <strong><u>kurang dari 2MB</u></strong> (atau compress terlebih dahulu)</p>
                </div>
                <form id="form-upload-dokumen" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Dokumen</label>
                            <div class="col-md-9">
                                <input name="nama_dokumen" placeholder="Nama Dokumen" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Jenis Dokumen</label>
                            <div class="col-md-9">
                                <select name="jenis_dokumen" class="form-control">
                                    <option value="">--Select Jenis Dokumen--</option>
                                    <?php foreach($jenis_dokumen as $item):?>
                                        <option value="<?php print($item->id); ?>"><?php print($item->nama); ?></option>
                                    <?php endforeach;?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">File</label>
                            <div class="col-md-9">
                                <input type="file" name="files" />
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tanggal Awal Aktif</label>
                            <div class="col-md-9">
                                <input name="tgl_awal_aktif" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tanggal Akhir Aktif</label>
                            <div class="col-md-9">
                                <input name="tgl_akhir_aktif" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Aktif</label>
                            <div class="col-md-9">
                                <select name="is_aktif" class="form-control">
                                    <option value="">--Select Keaktifan--</option>
                                    <option value="y">Ya</option>
                                    <option value="n">Tidak</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                    </div>
                
            </div>
            <div class="modal-footer">
                    <button type="submit" id="btnSaveDokumen" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->