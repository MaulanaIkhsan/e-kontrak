<?php
$this->load->view('template/header');?>
<!-- Select2 -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/select2/dist/css/select2.min.css')); ?>">
<?php $this->load->view('template/asset_header');?>
<!-- Star Rating -->
<link rel="stylesheet" href="<?php print(base_url('assets/plugins/rating/dist/starrr.css')); ?>">


<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <!-- Box Form Unit Kerja -->
        <?php print($this->session->flashdata('alert')); ?>
        <?php print($this->session->flashdata('success')); ?>
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-primary" id="form-pengunjung">
              <div class="box-header with-border">
                <h3 class="box-title">Formulir Unit Kerja</h3>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <form class="form-horizontal" method="post" action="<?php print(base_url('unit_kerja/update/'.$unit_kerja->id))?>">
                <div class="box-body">
                  <div class="form-group">
                    <label for="nik" class="col-sm-2 control-label">Nama</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name="nama" placeholder="Nama Unit Kerja" required="required" id="nama" value="<?php print($unit_kerja->nama) ?>" />
                      <span class="text-danger col-md-8"><?php echo form_error('Nama'); ?></span>
                    </div>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <a href="<?php print(base_url('unit_kerja/detail/'.$unit_kerja->id)); ?>" class="btn btn-primary">Kembali</a> | 
                  <button type="submit" name="save" class="btn btn-success" value="simpan">Simpan</button>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
          </div>
          <!-- /Box Form Unit Kerja -->

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>

<?php $this->load->view('template/footer');?>