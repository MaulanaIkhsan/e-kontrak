<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_kegiatan_kontrak extends CI_Controller {

    /*--- jadwal_kegiatan_kontrak ---*/
    // id
    // jenis_kegiatan_id
    // kontrak_surat_penawaran_id
    // tgl
    // jam
    // created_at
    // updated_at
    
    
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $sess_data = $this->session->userdata('session_data');
        if($sess_data===NULL) {
            $this->session->set_flashdata('warning', '<div class="alert alert-warning fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a> 
                Harap login terlebih dahulu untuk mengakses sistem</div>');
			redirect('auth');
        }

        $this->load->model('Jadwal_kegiatan_kontrak_model', 'jadwal_kegiatan_kontrak');
        $this->load->model('Generic_model', 'generic');
    }
    
    private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('jenis_kegiatan_id') == '')
		{
			$data['inputerror'][] = 'jenis_kegiatan_id';
			$data['error_string'][] = 'Jenis Kegiatan Pengadaan is required';
			$data['status'] = FALSE;
        }
        
        if($this->input->post('tgl') == '')
		{
			$data['inputerror'][] = 'tgl';
			$data['error_string'][] = 'Tanggal is required';
			$data['status'] = FALSE;
        }
        
        if($this->input->post('jam') == '')
		{
			$data['inputerror'][] = 'jam';
			$data['error_string'][] = 'Jam Surat is required';
			$data['status'] = FALSE;
        }		
        
		if($data['status'] === FALSE)
		{
			echo json_encode((object)$data);
			exit();
        }
    }

    public function get_data($kontrak_surat_penawaran_id)
	{
		$list = $this->jadwal_kegiatan_kontrak->get_datatables($kontrak_surat_penawaran_id);

		$data = array();
        $no = $_POST['start'];
        
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->jenis_kegiatan_nama;
			$row[] = $field->tgl;
            $row[] = $field->jam;
            $row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit_jadwal_kegiatan('."'".$field->id."'".')" class="btn btn-primary">Edit</a>
                <a href="javascript:void(0)" title="Delete" onclick="delete_jadwal_kegiatan('."'".$field->id."'".')" class="btn btn-danger">Delete</a>';
			$row[] = "";

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->jadwal_kegiatan_kontrak->count_all($kontrak_surat_penawaran_id),
			"recordsFiltered" => $this->jadwal_kegiatan_kontrak->count_filtered($kontrak_surat_penawaran_id),
			"data" => $data,
		);
		// output dalam format JSON
		echo json_encode($output);
    }
    
    public function ajax_add($kontrak_surat_penawaran_id)
	{
        $this->_validate();
        
        $this->load->helper('generic');

        $random_name = rename_file();
        $new_name = $random_name.'.pdf';
        $this->conf_upload['file_name'] = $new_name;
        $this->load->library('upload', $this->conf_upload);
        $this->upload->initialize($this->conf_upload);

		$data = array(
                'kontrak_surat_penawaran_id'    => $kontrak_surat_penawaran_id,
                'jenis_kegiatan_id'             => $this->input->post('jenis_kegiatan_id'),
                'tgl'                           => $this->input->post('tgl'),
				'jam'                           => $this->input->post('jam')
            );
        
        $insert = $this->generic->insert('jadwal_kegiatan_kontrak',$data);
        
        if($insert=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => FALSE));
        }
    }

    public function ajax_edit($id)
	{
		$data = $this->jadwal_kegiatan_kontrak->fetch_data($id);
		echo json_encode($data);
	}

	public function ajax_update()
	{
        $this->_validate();
        
        $this->load->helper('generic');

        $id = $this->input->post('id');

		$data = array(
            'jenis_kegiatan_id' => $this->input->post('jenis_kegiatan_id'),
            'tgl'               => $this->input->post('tgl'),
            'jam'               => $this->input->post('jam')
        );
    
        $key    = array('id'=>$id);
        $update = $this->generic->update('jadwal_kegiatan_kontrak', $data, $key);
        
        if($update=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => 'Error edit data'));
        }

    }

    public function ajax_delete($id)
	{
        $key = array('id'=>$id);

		$delete = $this->generic->delete('jadwal_kegiatan_kontrak', $key);
        
        if($delete=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => FALSE));
        }
    }
}

/* End of file Jadwal_kegiatan_kontrak.php */
