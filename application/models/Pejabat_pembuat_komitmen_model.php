<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pejabat_pembuat_komitmen_model extends CI_Model {

    // ----- pejabat_pembuat_komitmen -----
    // id
    // pegawai_id
    // sk_id
    // tahun
    // created_at
    // updated_at

    public function fetch_data($id=NULL, $tahun=NULL, $pegawai_id=NULL)
    {
        $sql = $this->db->select('pejabat_pembuat_komitmen.id as pejabat_pembuat_komitmen_id,
                                pegawai.id as pegawai_id,
                                pegawai.nama_lengkap as pegawai_nama,
                                pegawai.nip as pegawai_nip,
                                jabatan_fungsional.nama as jabatan_nama,
                                unit_kerja.nama as unit_kerja_nama,
                                pejabat_pembuat_komitmen.tahun as tahun,
                                sk_pejabat_pembuat_komitmen.no_sk as sk_no,
                                sk_pejabat_pembuat_komitmen.tahun as sk_tahun,
                                sk_pejabat_pembuat_komitmen.tgl_penetapan as sk_tgl_penetapan')
                        ->from('pejabat_pembuat_komitmen')
                        ->join('pegawai', 'pejabat_pembuat_komitmen.pegawai_id=pegawai.id', 'left')
                        ->join('jabatan_fungsional', 'pegawai.jabatan_id=jabatan_fungsional.id', 'left')
                        ->join('unit_kerja', 'pegawai.unit_id=unit_kerja.id', 'left')
                        ->join('sk_pejabat_pembuat_komitmen', 'pejabat_pembuat_komitmen.sk_id=sk_pejabat_pembuat_komitmen.id', 'left');

        if($tahun!=NULL) {
            $sql->where('pejabat_pembuat_komitmen.tahun', $tahun);
        }

        if($pegawai_id!=NULL) {
            $sql->where('pejabat_pembuat_komitmen.pegawai_id', $pegawai_id);
        }
        
        if($id!=NULL) {
            return $sql->where('pejabat_pembuat_komitmen.id', $id)
                        ->get()->row();
        }
        elseif($pegawai_id!=NULL and $tahun!=NULL) {
            return $sql->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }
}

/* End of file Pejabat_pembuat_komitmen_model.php */
