<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pekerjaan_model extends CI_Model {

	var $main_table = 'pekerjaan',
		$pendamping1 = 'users';

	var $column_order = array(null, 'pekerjaan.tahun', 'pekerjaan.nama_pekerjaan','pekerjaan.anggaran_awal', 'pekerjaan.tahap_pekerjaan'); //field yang ada di table pekerjaan
	var $column_search = array('pekerjaan.nama_pekerjaan','pekerjaan.anggaran_awal', 'pekerjaan.tahap_pekerjaan', 'pekerjaan.tahun'); //field yang diizin untuk pencarian 

	// pekerjaan.id as id, pekerjaan.nama_pekerjaan as nama_pekerjaan, users.nama as nama_pptk,  pengadaan.nama as jenis_pengadaan, jenis_pekerjaan.nama as jenis_pekerjaan, pekerjaan.status as status
	var $order = array('id' => 'asc'); // default order 
	
	public $nama_pekerjaan 	= NULL, 
		$tahun 				= NULL,
		$kode_pekerjaan 	= NULL;

	public function fetch_data($id=NULL)
	{
		$sql = $this->db->select('pekerjaan.id as id, 
								pekerjaan.kode_pekerjaan as kode_pekerjaan,
								pekerjaan.nama_pekerjaan as nama_pekerjaan, 
								pekerjaan.anggaran_awal as anggaran_awal, 
								pekerjaan.keterangan as keterangan, 
								program.nama_program as nama_program, 
								pekerjaan.tahap_pekerjaan as tahap_pekerjaan,
								pekerjaan.tahun as tahun,
								pekerjaan.kode_rekening as kode_rekening,
								program.kode_program as kode_program,
								program.id as program_id')
						->from('pekerjaan')
						->join('program', 'program.uniq_code=pekerjaan.kode_program', 'left');
		
		if($this->kode_pekerjaan!=NULL) {
			$sql->where('pekerjaan.kode_pekerjaan', $this->kode_pekerjaan);
		}
		
		if($this->nama_pekerjaan!=NULL) {
			$sql->where('pekerjaan.nama_pekerjaan', $this->nama_pekerjaan);
		}

		if($this->tahun!=NULL) {
			$sql->where('pekerjaan.tahun', $this->tahun);
		}
		
		if($id!=NULL) {
			return $sql->where('pekerjaan.id', $id)
						->get()->result();
		}
		else {
			return $sql->get()->result();
		}
	}

	public function insert($data)
	{
		$this->db->query('set FOREIGN_KEY_CHECKS = 0;');
		$sql = $this->db->insert($this->main_table, $data);
		if($sql){
			return 'ok';
			$this->db->query('set FOREIGN_KEY_CHECKS = 1;');
		}
		else {
			return 'fail';
			$this->db->query('set FOREIGN_KEY_CHECKS = 1;');
		}

	}

	/*
	Nama Kegiatan = Nama Pekerjaan
	Kode Kegiatan = Kode Pekerjaan
	Anggaran = anggaran

	Update value Tahap
	*/
	// Insert data pekerjaan based on consume data from Simanggaran
	public function check_data_pekerjaan($kode_pekerjaan, $tahun_pekerjaan)
	{
		$sql = $this->db->select('kode_pekerjaan, nama_pekerjaan, tahap_pekerjaan, tahun')
						->from($this->main_table)
						->where('kode_pekerjaan', $kode_pekerjaan)
						->where('tahun', $tahun_pekerjaan)
						->get();
		if($sql->num_rows()==0){
			return FALSE;
		}
		else {
			return $sql->row();
		}
	}

	public function check_nama_pekerjaan($kode_pekerjaan, $nama_pekerjaan, $tahun_pekerjaan)
	{
		$sql = $this->check_data_pekerjaan($kode_pekerjaan, $tahun_pekerjaan);

		if($sql->nama_pekerjaan !== $nama_pekerjaan){
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	// Check flag tahap pekerjaan
	public function check_tahap_pekerjaan($kode_pekerjaan, $tahap_pekerjaan, $tahun_pekerjaan)
	{
		$sql = $this->check_data_pekerjaan($kode_pekerjaan, $tahun_pekerjaan);

		if($sql->tahap_pekerjaan !== $tahap_pekerjaan){
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	public function update($data, $key)
	{
		$sql = $this->db->where($key)
						->update($this->main_table, $data);
		if($sql){
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}


	public function insert_data_api($data, $linked_program)
	{
		$this->load->helper('generic');

		$nama_pekerjaan 	= $data['nama_pekerjaan'];
		$kode_pekerjaan 	= $data['kode_pekerjaan'];
		$tahap_pekerjaan 	= $data['tahap_pekerjaan'];
		$anggaran_awal 		= $data['anggaran_awal'];
		$tahun_pekerjaan	= $data['tahun'];

		$check_pekerjaan = $this->check_data_pekerjaan($kode_pekerjaan, $tahun_pekerjaan);
		if($check_pekerjaan!==FALSE) {

			$check_nama_pekerjaan = $this->check_nama_pekerjaan($kode_pekerjaan, $nama_pekerjaan, $tahun_pekerjaan);
			if($check_nama_pekerjaan==FALSE) {
				//update nama urusan & tahap pekerjaan
				$data = [];
				$data = array(
					'nama_pekerjaan' => $nama_pekerjaan,
					'tahap_pekerjaan' => $tahap_pekerjaan,
					'anggaran_awal' => $anggaran_awal
				);
				$key['kode_pekerjaan'] = $kode_pekerjaan;
				$update = $this->update($data, $key);
				if ($update=='ok') {
					return 'ok';
				}
				else {
					return 'fail';
				}
			}//check nama_pekerjaan
			else {
				$check_tahap_pekerjaan = $this->check_tahap_pekerjaan($kode_pekerjaan, $tahap_pekerjaan, $tahun_pekerjaan);
				if($check_tahap_pekerjaan===FALSE){
					$data = [];
					$data['tahap_pekerjaan'] = $tahap_pekerjaan;
					$data['anggaran_awal'] = $anggaran_awal;

					$key['kode_pekerjaan'] = $kode_pekerjaan;
					$update = $this->update($data, $key);
					if ($update=='ok') {
						return 'ok';
					}
					else {
						return 'fail';
					}
				} //Check tahap pekerjaan
			} // else
		} //check_pekerjaan
		else {
			$this->load->model('Program_model', 'program');

			// Get linked data program
			$kode_program = $linked_program['kode_program'];
			$nama_program = $linked_program['nama_program'];
			$tahun_program = $linked_program['tahun'];
			$get_linked = $this->program->get_program_by_uniq_code($kode_program, $nama_program, $tahun_program);
			$data['kode_program'] = $get_linked->uniq_code;

			$data['uniq_code'] = random_unique_code(12, 'p');

			//input data pekerjaan baru
			$insert_data = $this->insert($data);
			if($insert_data){
				return 'ok';
			}
			else {
				return 'fail';
			}
		}

	}

	public function get_pekerjaan_by_uniq_code($kode_pekerjaan, $nama_pekerjaan, $tahun)
	{
		return $this->db->select('uniq_code')
						->from($this->main_table)
						->where('kode_pekerjaan', $kode_pekerjaan)
						->where('nama_pekerjaan', $nama_pekerjaan)
						->where('tahun', $tahun)
						->get()->row();
	}

	public function show($tahun=null)
	{
		// $sql = $this->db->select('pekerjaan.id as id, pekerjaan.nama_pekerjaan as nama, users.nama as pptk,  pengadaan.nama as jenis_pengadaan, jenis_pekerjaan.nama as jenis_pekerjaan, pekerjaan.tahun as tahun, pekerjaan.uniq_code as uniq_code')
		// 				->from($this->main_table)
		// 				->join('users', 'users.id = pekerjaan.pendamping1_id', 'left')
		// 				->join('jenis_pengadaan as pengadaan', 'pengadaan.id = pekerjaan.pengadaan_id', 'left')
		// 				->join('jenis_pekerjaan', 'jenis_pekerjaan.id = pekerjaan.jenis_pekerjaan_id', 'left');
						
		// if($role!=='admin') {
		// 	$sql->where('pekerjaan.pendamping1_id', $user_id);
		// }

		// return $sql->get()->result();
		$sql = $this->db->select('id, kode_pekerjaan, nama_pekerjaan, kode_program, tahap_pekerjaan, anggaran_awal, uniq_code, tahun')
						->from($this->main_table);

		if($tahun!=null){
			$sql->where('tahun', $tahun);
			return $sql->get()->result();
		}
		else {
			return $sql->get()->result();
		}
	}

	public function show_update($key)
	{
		return $this->db->select('pekerjaan.id as pekerjaan_id,
						program.nama_program as nama_program, 
						pekerjaan.nama_pekerjaan as nama_pekerjaan, 
						pekerjaan.kode_rekening as kode_rekening')
						->from($this->main_table)
						->join('program', 'pekerjaan.kode_program=program.uniq_code', 'left')
						->where('pekerjaan.id', $key)
						->get()->row();
	}

	public function delete($key)
	{
		$sql = $this->db->delete($this->main_table, $key);
		if ($sql) {
			return 'ok';
		}
		else{
			return 'fail';
		}
	}

	public function detail($id)
	{
		return $this->db->query("SELECT 
							    pekerjaan.id as id, 
								pekerjaan.kode_pekerjaan as kode_pekerjaan,
							    pekerjaan.nama_pekerjaan as nama_pekerjaan, 
							    pekerjaan.anggaran_awal as anggaran_awal, 
							    pekerjaan.keterangan as keterangan, 
							    program.nama_program as nama_program, 
							    pekerjaan.tahap_pekerjaan as tahap_pekerjaan,
							    pekerjaan.tahun as tahun,
							    pekerjaan.kode_rekening as kode_rekening,
								program.kode_program as kode_program,
								program.id as program_id
							FROM 
							    pekerjaan 
							        LEFT JOIN 
							    program ON program.uniq_code=pekerjaan.kode_program 
							WHERE 
							    pekerjaan.id = $id")
						->row();
	}

	public function get_nama_pekerjaan($id)
	{
		return $this->db->select('pekerjaan.id as id,pekerjaan.nama_pekerjaan as nama_pekerjaan, pekerjaan.anggaran_awal as anggaran_awal, pekerjaan.tahun as tahun')
						->from($this->main_table)
						->where('pekerjaan.id', $id)
						->get()->row();
	}

	public function get_nilai_kontrak($pekerjaan_id)
	{
		return $this->db->select('nilai_kontrak, anggaran_awal')
						->from($this->main_table)
						->where('id', $pekerjaan_id)
						->get()->row();
	}

	public function stat_jenis_pengadaan($user_id, $role='admin')
	{
		$sql = $this->db->select('pengadaan.nama as jenis_pengadaan, count(pekerjaan.id) as total')
						->from($this->main_table)
						->join('jenis_pengadaan as pengadaan', 'pekerjaan.pengadaan_id=pengadaan.id', 'inner');
		if($role!=='admin'){
			$sql->where('pekerjaan.pendamping1_id', $user_id);
		}

		return $sql->group_by('pengadaan.nama')
					->order_by('total', 'desc')
					->get()->result();
	}

	public function stat_jenis_pekerjaan($user_id, $role='admin')
	{
		$sql = $this->db->select('jenis_pekerjaan.nama as jenis_pekerjaan, count(pekerjaan.id) as total')
						->from($this->main_table)
						->join('jenis_pekerjaan', 'pekerjaan.jenis_pekerjaan_id=jenis_pekerjaan.id', 'inner');
		if($role!=='admin'){
			$sql->where('pekerjaan.pendamping1_id', $user_id);
		}

		return $sql->group_by('jenis_pekerjaan.nama')
					->order_by('total', 'desc')
					->get()->result();
	}

	public function stat_status_pekerjaan($user_id, $role='admin')
	{
		$sql = $this->db->select('status, count(id) as total')
						->from($this->main_table);
		if($role!=='admin'){
			$sql->where('pendamping1_id', $user_id);
		}

		return $sql->group_by('status')
					->order_by('total', 'desc')
					->get()->result();
	}

	public function get_anggaran_awal($pekerjaan_id)
	{
		return $this->db->select('anggaran_awal')
						->from('pekerjaan')
						->where('id', $pekerjaan_id)
						->get()->row();
	}

	public function get_kode_pekerjaan($pekerjaan_id)
	{
		return $this->db->select('kode_pekerjaan')
						->from($this->main_table)
						->where('id', $pekerjaan_id)
						->get()->row();
	}

	public function get_uniq_code_pekerjaan($pekerjaan_id)
	{
		return $this->db->select('uniq_code')
						->from($this->main_table)
						->where('id', $pekerjaan_id)
						->get()->row();
	}
	
	public function statistik_realisasi($value='')
	{
		/*Query
		select 
			pekerjaan.id as id, 
			pekerjaan.nama_pekerjaan as nama, 
			users.nama as pptk, 
			pengadaan.nama as jenis_pengadaan, 
			jenis_pekerjaan.nama as jenis_pekerjaan, 
			pekerjaan.status as status, 
			pekerjaan.keterangan as keterangan, 
			pekerjaan.nilai_kontrak as nilai_kontrak, 
			pekerjaan.anggaran_awal as anggaran_awal, 
			vendor.nama as nama_vendor,
			sum(realisasi.realisasi_presentase) as realisasi_presentase,
			sum(realisasi.realisasi_fisik) as realisasi_fisik
		from
			pekerjaan
				left join
			users on (users.id = pekerjaan.pendamping1_id)
				left join
			jenis_pengadaan as pengadaan on (pengadaan.id=pekerjaan.pengadaan_id)
				left join
			jenis_pekerjaan on (jenis_pekerjaan.id = pekerjaan.jenis_pekerjaan_id)
				left join
			kontrak on (kontrak.pekerjaan_id = pekerjaan.id)
				left join
			vendor on (vendor.id = kontrak.vendor_id)
				left join
			realisasi on (pekerjaan.id = realisasi.pekerjaan_id)
		where
			pekerjaan.status = 'Belum'	
		group by
			pekerjaan.id, vendor.nama
		;
		*/
		return 'Hello World';
	}

	public function resume_aktivitas_by_pekerjaan($pekerjaan_id)
	{
		return $this->db->query("SELECT
						aktivitas.nama_aktivitas as nama_kegiatan,
						coalesce(aktivitas.anggaran_awal, 0) as anggaran_pekerjaan,
						coalesce(aktivitas.total_realisasi, 0) as serapan
					from
						aktivitas
							inner join
						pekerjaan on (pekerjaan.id=aktivitas.pekerjaan_id)
					where
						pekerjaan.id = $pekerjaan_id")
						->result();
	}
	/*====== Serverside Datatable ======*/
	private function _get_datatables_query($kode_program)
	{
		$this->db->select('pekerjaan.id as id, 
					pekerjaan.nama_pekerjaan as nama_pekerjaan, 
					pekerjaan.anggaran_awal as anggaran_awal, 
					pekerjaan.tahap_pekerjaan as tahap_pekerjaan, 
					pekerjaan.tahun as tahun');
		$this->db->from($this->main_table);
		$this->db->where('kode_program', $kode_program);
						
		// if($role!=='admin') {
		// 	$sub_query = "(select 
		// 			distinct(pekerjaan.id) as pekerjaan_id
		// 		from
		// 			pekerjaan
		// 				inner join
		// 			aktivitas on (aktivitas.pekerjaan_id=pekerjaan.id)
		// 				inner join
		// 			users on (aktivitas.pptk=users.id)
		// 		where
		// 			users.id = ".$user_id.")";
		// 	$this->db->join($sub_query.' as filter_aktivitas', 'filter_aktivitas.pekerjaan_id=pekerjaan.id', 'inner');
		// 	// $this->db->where('pekerjaan.pendamping1_id', $user_id);
		// }

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($kode_program)
	{
		$this->_get_datatables_query($kode_program);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($kode_program, $user_id, $role='admin')
	{
		$this->_get_datatables_query($kode_program);

		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($kode_program)
	{
		$this->db->select('pekerjaan.id as id, 
					pekerjaan.nama_pekerjaan as nama_pekerjaan, 
					pekerjaan.anggaran_awal as anggaran_awal, 
					pekerjaan.tahap_pekerjaan as tahap_pekerjaan, 
					pekerjaan.tahun as tahun');
		$this->db->from($this->main_table);
		$this->db->where('kode_program', $kode_program);
						
		return $this->db->count_all_results();
	}
	/*====== /Serverside Datatable ======*/

	
}

/* End of file Pekerjaan_model.php */
/* Location: ./application/models/Pekerjaan_model.php */