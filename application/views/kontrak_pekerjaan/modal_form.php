<!-- Modal Logout System -->
<?php 
$sess_data  = $this->session->userdata('session_data');
$user_role  = $sess_data['role'];
$user_id    = $sess_data['id'];
$user_as    = $sess_data['as'];
?>
<div class="modal fade" id="modal-create-penawaran">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-warning"></i> Create Penawaran</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-penawaran">
                    <input type="hidden" name="id" />
                    
                    
                    <div class="form-group">
                        <label for="namalengkap" class="col-sm-4 control-label">Pejabat Pengadaan</label>
                        <div class="col-sm-8">
                            <?php if($user_role=='Super Admin') {?>
                                <select name="pejabat_pengadaan" class="form-control">
                                    <option value="">-- Select Pejabat Pengadaan --</option>
                                    <?php foreach($pejabat_pengadaan as $item):?>
                                        <option value="<?php print($item->pejabat_pengadaan_id); ?>"><?php print($item->pegawai_nama); ?></option>
                                    <?php endforeach;?>
                                </select>
                            <?php } else { ?>
                                <input type="hidden" name="pejabat_pengadaan" />
                                <input type="text" name="pejabat_pengadaan_nama" class="form-control" readonly="readonly" />
                            <?php } ?>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="namalengkap" class="col-sm-4 control-label">Tgl Awal Kontrak</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control datepicker" name="tgl_awal_kontrak" readonly="readonly" />
                        </div>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label for="namalengkap" class="col-sm-4 control-label">Tgl Akhir Kontrak</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control datepicker" name="tgl_akhir_kontrak" readonly="readonly" />
                        </div>
                        <span class="help-block"></span>
                    </div>
                    <div class="form-group">
                        <label for="namalengkap" class="col-sm-4 control-label">HPS</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control money" name="hps" />
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <span id="harga-negosiasi"></span>
                    </div>
                    <div class="form-group">
                        <label for="namalengkap" class="col-sm-4 control-label">Status Kontrak Pekerjaan</label>
                        <div class="col-sm-8">
                            <select name="status" class="form-control">
                                <option value="">-- Select Status Kontrak --</option>
                                <?php foreach($status_kontrak as $item):
                                    if($item=='Pembukaan') { ?>
                                        <option value="<?php print($item); ?>" selected="selected"><?php print($item); ?></option>
                                    <?php } else { ?> 
                                        <option value="<?php print($item); ?>"><?php print($item); ?></option>
                                    <?php } ?>
                                    
                                <?php endforeach;?>
                            </select>
                            <span class="help-block"></span>
                        </div>
                    </div>
                </form>
                
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                <button type="submit" name="simpan" class="btn btn-primary" id="btnSave" onclick="save_kontrak()">Simpan</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

</div>
</div>
<!-- /Box Form Pengunjung -->