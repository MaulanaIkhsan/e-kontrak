<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function index()
    {
        $this->load->library('form_validation');

        $session_data = array('id' => '', 
                        'nama' => '', 
                        'as' => '', 
                        'is_pegawai' => '', 
                        'role' => '');

        $this->form_validation->set_rules('username', 'Username','required|trim');
        $this->form_validation->set_rules('password', 'Password','required|trim');
        $this->form_validation->set_rules('person', 'Sebagai','required|trim');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('auth/login');
        }
        else { 
            if($this->input->post('login')!==NULL){
                $this->load->model('Auth_model', 'auth');
                
                $data['person'] = $this->input->post('person');
                $data['username'] = $this->input->post('username');
                $data['password'] = $this->input->post('password');
                
                $checking = $this->auth->checking($data['person'], $data, $session_data);

                if(is_array($checking)) {
                    $this->session->set_userdata('session_data', $checking);

                    $this->session->set_flashdata('success', '<div class="alert alert-success fade in">
                        <a href="#" class="close" data-dismiss="alert">&times;</a> 
                        Selamat datang di sistem <strong>Sisuper </strong>'.$checking['nama'].'</div>');
                    
                    // Pegawai DPU
                    if($data['person']=='pegawai') {
                        redirect('beranda');
                    }
                    // Pihak Ketiga
                    else {
                        $this->load->model('Karyawan_model', 'karyawan');
                        $this->load->model('Dokumen_pihak_ketiga_model', 'dokumen_pihak_ketiga');
                        
                        $this->karyawan->set_username($data['username']);

                        $karyawan = $this->karyawan->fetch_data();

                        if(!empty($karyawan)) {
                            $dokumen = $this->dokumen_pihak_ketiga->fetch_data(NULL, $karyawan[0]->pihak_ketiga_id);
                            if(empty($dokumen)) {
                                $this->session->set_flashdata('alert', '<div class="alert alert-warning fade in">
                                    <a href="#" class="close" data-dismiss="alert">&times;</a> 
                                    Harap Melengkapi "<strong>Dokumen Pelengkap</strong>"</div>');
                            }
                        }
                        redirect('beranda');
                    }
                    
                }
                elseif($checking=='not_exist') {
                    $this->session->set_flashdata('alert', '<div class="alert alert-danger fade in">
                        <a href="#" class="close" data-dismiss="alert">&times;</a> 
                        Data pengguna tidak ditemukan !</div>');
                    
                    redirect('auth');
                }
                elseif ($checking=='development') {
                    $this->session->set_flashdata('alert', '<div class="alert alert-danger fade in">
                        <a href="#" class="close" data-dismiss="alert">&times;</a> 
                        Sistem masih dalam pengembangan !</div>');
                    
                    redirect('auth');
                }
                elseif ($checking=='fail') {
                    $this->session->set_flashdata('alert', '<div class="alert alert-danger fade in">
                        <a href="#" class="close" data-dismiss="alert">&times;</a> 
                        Harap cek kembali username dan password anda !</div>');
                    
                    redirect('auth');
                }
                else {
                    $this->session->set_flashdata('alert', '<div class="alert alert-danger fade in">
                        <a href="#" class="close" data-dismiss="alert">&times;</a> 
                        Not Expected Condition !</div>');
                    
                    redirect('auth');
                }
            }
            else {
                $this->session->set_flashdata('alert', '<div class="alert alert-danger fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a> 
                    Harap masuk sistem Sisuper melalui halaman login !</div>');
                redirect('auth');
            }
        }
        
    }
    public function landing_page()
    {
        $this->load->model('Pihak_ketiga_model', 'pihak_ketiga');
        $this->load->model('Aktivitas_model', 'aktivitas');
        $this->load->model('Pejabat_pengadaan_model', 'pejabat_pengadaan');
        $this->load->model('Kontrak_pekerjaan_model', 'kontrak_pekerjaan');
        
        $curr_year = date("Y");

        //Setting aktivitas for kontrak_pekerjaan
        $this->aktivitas->is_kontrak_pekerjaan = TRUE;
        $data['jml_aktivitas']          = $this->aktivitas->stat_jml_aktivitas(2, 'Super Admin');
        $data['jml_pihak_ketiga']       = count($this->pihak_ketiga->fetch_data());
        $data['jml_pejabat_pengadaan']  = count($this->pejabat_pengadaan->fetch_data(NULL, $curr_year));
        $data['jml_kontrak_pekerjaan']  = count($this->kontrak_pekerjaan->fetch_data(NULL, NULL, $curr_year));

        $this->load->view('landing_page', $data);
    }
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('auth');
    }

}

/* End of file Auth.php */
