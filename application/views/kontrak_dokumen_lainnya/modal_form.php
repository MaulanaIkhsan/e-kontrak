<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_dokumen_lainnya" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Upload</h4>
            </div>
            <div class="modal-body form">
                <div class="callout callout-danger">
                    <h4>Perhatian!</h4>
                    <p>Harap upload dokumen dengan format <strong><u>PDF</u></strong> dan size <strong><u>kurang dari 2MB</u></strong>  (atau compress terlebih dahulu)</p>
                </div>
                <form id="form-dokumen-lainnya" class="form-horizontal" enctype="multipart/form-data" method="post">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Dokumen</label>
                            <div class="col-md-9">
                                <input name="nama" placeholder="Nama Dokumen" class="form-control" type="text" required="required">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Dokumen</label>
                            <div class="col-md-9">
                                <input type="file" name="dokumen" required="required" />
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                    </div>
                
            </div>
            <div class="modal-footer">
                    <button type="submit" id="btnSaveDokumenLainnya" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->