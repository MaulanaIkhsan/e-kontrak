<!DOCTYPE html>
<HtmL>

<HeAd>
    <MeTa HTTP-EquIV="CONTENT-TYPE" CONTent="text/html; charset=utf-8">
    <TItlE>Semarang, 29 Maret 2004</tITLE>
    <meta name=GENERATOR coNTeNT="LibreOffice 4.1.6.2 (Linux)">
    <Meta nAMe="AUTHOR" CONTeNt="DPU">
    <MEtA NaMe=CREATED cONtENt="Tahun_Anggaran0817;20400000000000">
    <MeTA NAmE="CHANGEDBY" CoNteNt="isan">
    <META NAme="CHANGED" coNTent="20190320;163019000000000">
    <MEtA nAmE=KSOProductBuildVer CONTEnt=1033-10.1.0.6757>
    <sTyLE>
        @page {
            size: 8.47in 13.98in;
            margin-right: 0.88in;
            margin-top: 0.59in;
            margin-bottom: 0.69in
        }
        P {
            margin-bottom: 0.08in;
            direction: ltr;
            color: #000000
        }
        P.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
        H1 {
            margin-left: 3.5in;
            margin-top: 0in;
            margin-bottom: 0in;
            direction: ltr;
            color: #000000;
            text-align: justify;
            text-decoration: underline
        }
        H1.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
        .text {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
        }
    </sTYLE>
</heAd>

<bOdY LANg="en-US" tExT=#000000 dIR="LTR">
    <?php
        $dayname    = dayname(date('l', strtotime($kontrak_surat->tgl_surat)));
        $tglindo    = tgl_indo($kontrak_surat->tgl_surat);
        $split_tgl  = explode(' ', $tglindo);
    ?>
    <p ClASs=western ALIGN=CENTER sTylE="margin-bottom: 0in; page-break-before: none">
        <b>
            BERITA ACARA HASIL PENGADAAN LANGSUNG<br/>
            (B A H P L)<br/>
            -------------------------------------------------------------------------------------------------------------------<br>
            <?php print($kontrak_surat->no_surat);?>
        </b>
    </P>
    <p></p>
    
    <p cLAss=western ALIGN="justify" stylE="margin-bottom: 0in">
        Pada hari ini, <?php print($dayname); ?>, tanggal <?php print($split_tgl[0]); ?> bulan <?php print($split_tgl[1]); ?> tahun <?php print($split_tgl[2]); ?> (<?php print($tglindo); ?>) , bertempat di Kantor Dinas Pekerjaan Umum Kota Semarang, yang bertanda tangan dibawah ini :
    </p>
    <p><?php print($pejabat_pengadaan->pegawai_nama); ?></p>
    
    <p ClASs=western align="justify" STYle="margin-bottom: 0in">
        Selaku Pejabat Pengadaan pada Dinas Pekerjaan Umum Kota Semarang Tahun Anggaran <?php print(date("Y")); ?>, yang dibentuk berdasarkan Keputusan Kepala Dinas Pekerjaan Umum Kota Semarang Selaku Pengguna Anggaran Nomor <?php print($pejabat_pengadaan->sk_no); ?> tanggal <?php print(tgl_indo($pejabat_pengadaan->sk_tgl_penetapan)); ?> tentang Penunjukan Pejabat Pengadaan Pada Dinas Pekerjaan Umum kota Semarang APBD Tahun Anggaran <?php print($pejabat_pengadaan->sk_tahun); ?>, telah melakukan Penyusunan Hasil Pengadaan Langsung (BAHPL) , untuk :        
    </p>

    <br/>
    <table>
        <tr>
            <td class="text">Program</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->program_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Kegiatan</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->pekerjaan_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Pekerjaan</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->aktivitas_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Sumber Dana</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->aktivitas_sumber_dana); ?></td>
        </tr>
    </table>

    <p class="western" align="justify" STYle="margin-bottom: 0in">
        Adapun hal-hal yang disepakati adalah sebagai berikut :
    </p>
    
    <p class="western" align="justify" STYle="margin-bottom: 0in">
        Pelaksanaan pengadaan barang/jasa telah dilaksanakan sesuai Peraturan Presiden Republik Indonesia Nomor 4 Tahun 201 5 tentang Perubahan keempat atas Peraturan Presiden Nomor 54 Tahun 2010 tentang Pengadaan Barang/Jasa Pemerintah dan telah melalui tahapan-tahapan kegiatan.
    <p>

    <p class="western" align="center" STYle="margin-bottom: 0in">
        <b>TAHAP PROSES PENGADAAN LANGSUNG</b>
    </p>

    <p class="western" align="justify" STYle="margin-bottom: 0in">
        <ol type="1">
            <li>Perusahaan yang diundang sebanyak 1 (satu) dan memasukkan dokumen sebanyak 1 (satu) Perusahaan</li>
            <br/>
            <li>
                Pemasukkan dan Pembukaan Dokumen Penawaran
                <ul type="dist">
                    <li>Dokumen penawaran yang disampaikan oleh penyedia barang/jasa, setelah dibuka dan diteliti dinyatakan memenuhi syarat sehingga dapat dilanjutkan evaluasi.</li>
                    <li>
                        Hasil Pembukaan Dokumen Penawaran adalah sebagai berikut :<br/>
                        HPS/OE : <?php print('Rp '.format_money($kontrak_pekerjaan->hps).',-'); ?><br/>
                        <taBle WIDTH=649 CELLPADDING=7 CELLSPACING=0>
                            
                                        <tr>
                                            <tD ALIGN=CENTER sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                
                                                    <fOnT SIZE=2 stYLE="font-size: 11pt"><b>NO</b></fOnt>
                                                
                                            </td>
                                            <td ALIGN=CENTER WIDTH=297 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                
                                                    <FONT SIZE=2 StYle="font-size: 11pt"><b>NAMA PENYEDIA JASA</b></fOnT>
                                               
                                            </Td>
                                            <td ALIGN=CENTER WIDTH=156 StYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                
                                                    <fONT SIZE=2 sTyLE="font-size: 11pt"><b>NILAI PENAWARAN (Rp) </B></FoNT>
                                                
                                            </Td>
                                        </tr>
                                        <tR VALIGN=TOP>
                                            <TD ALIGN=CENTER sTyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                
                                                    <fONT SIZE=2 stYlE="font-size: 11pt">1</fONT>
                                                
                                            </Td>
                                            <Td WIDTH=297 sTYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p ClasS="western">
                                                    <fONt SIZE=2 stYlE="font-size: 11pt"><SPaN Lang="pt-BR"><?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama); ?></spaN></FONT>
                                                </p>
                                            </tD>
                                            <td ALIGN=CENTER WIDTH=156 StYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                <P cLass=western><?php print('Rp '.format_money($kontrak_pihak_ketiga[0]->nilai_kontrak).',-'); ?></P>
                                            </tD>
                                        </Tr>
                        </taBle>
                    </li>
                </ul>
            </li>
            <br/>
            <li>
                Unsur-unsur yang dievaluasi
                <ul type="a">
                    <li>
                        Evaluasi Administrasi <br/>
                        <taBLe WIDTH=649 CELLPADDING=7 CELLSPACING=0>
                            
                            <tR>
                                <tD ROWSPAN=3 ALIGN=CENTER WIDTH=23 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <fONt SIZE=2 STyle="font-size: 11pt">NO</Font>
                                </Td>
                                <Td ROWSPAN=3 ALIGN=CENTER WIDTH=196 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <FoNt SIZE=2 STYle="font-size: 11pt">NAMA DOKUMEN</FonT>
                                </TD>
                                <TD COLSPAN=2 ALIGN=CENTER WIDTH=132 VALIGN=TOP sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P lanG=pt-BR clASs=western ALIGN=CENTER>
                                        <FONT SIZE=2 STyLe="font-size: 11pt">KELENGKAPAN</foNt>
                                    </p>
                                </td>
                                <tD ROWSPAN=3 WIDTH=56 STYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LanG=pt-BR CLAsS=western ALIGN=CENTER>
                                        <fonT SIZE=2 sTyLe="font-size: 11pt">Tdk ada</FoNT>
                                    </P>
                                </TD>
                                <TD ROWSPAN=3 WIDTH=136 StyLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p laNg="pt-BR" cLASs="western" ALIGN=CENTER>
                                        <FoNT SIZE=2 STYle="font-size: 11pt">KETERANGAN</FONt>
                                    </p>
                                </Td>
                            </Tr>
                            <tR VALIGN=TOP>
                                <tD COLSPAN=2 WIDTH=132 sTylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P Lang=pt-BR cLass=western ALIGN=CENTER>
                                        <FoNt SIZE=2 STYlE="font-size: 11pt">Ada (+)</Font>
                                    </P>
                                </Td>
                            </Tr>
                            <Tr VALIGN=TOP>
                                <td WIDTH=46 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LAnG="pt-BR" CLasS=western ALIGN=JUSTIFY>
                                        <FONT SIZE=2 sTYLE="font-size: 11pt">Sesuai</font>
                                    </p>
                                </TD>
                                <Td WIDTH=72 StyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p lang="pt-BR" ClAss="western" ALIGN=JUSTIFY>
                                        <fOnT SIZE=2 STyLE="font-size: 11pt">Tdk Sesuai</fONt>
                                    </P>
                                </TD>
                            </Tr>
                            <tr VALIGN=TOP>
                                <td WIDTH=23 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P laNg="pt-BR" cLASS="western" ALIGN=CENTER>
                                        <foNt SIZE=2 STyle="font-size: 11pt">1</font>
                                    </P>
                                </tD>
                                <tD WIDTH=196 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P lAng=pt-BR class=western ALIGN=JUSTIFY>
                                        <foNT SIZE=2 sTylE="font-size: 11pt">Ditandatangai oleh pihak sebagaimana dalam ketentuan</foNt>
                                    </P>
                                </td>
                                <Td WIDTH=46 ALIGN=CENTER stYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                        <fonT SIZE=2 StyLE="font-size: 11pt">+</foNT>
                                    
                                </TD>
                                <TD WIDTH=72 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LaNg=pt-BR clasS="western" ALIGN=JUSTIFY>
                                        <bR>
                                    </P>
                                </td>
                                <tD WIDTH=56 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LANG=pt-BR ClAsS=western ALIGN=JUSTIFY>
                                        <bR>
                                    </P>
                                </tD>
                                <tD WIDTH=136 STylE="border: 1px solid #000000; padding: 0in 0.08in">
                                </tD>
                            </TR>
                            <Tr VALIGN=TOP>
                                <Td WIDTH=23 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LanG="pt-BR" ClASS=western ALIGN=CENTER>
                                        <fOnT SIZE=2 sTYlE="font-size: 11pt">2</FonT>
                                    </p>
                                </tD>
                                <TD WIDTH=196 style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LaNG=pt-BR claSS="western" ALIGN=JUSTIFY>
                                        <FOnt SIZE=2 styLE="font-size: 11pt">Mencantumkan penawaran harga</Font>
                                    </p>
                                </Td>
                                <td WIDTH=46 ALIGN=CENTER style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                        <fONt SIZE=2 StyLe="font-size: 11pt">+</fOnT>
                                    
                                </Td>
                                <tD WIDTH=72 STyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p Lang=pt-BR CLaSs="western" ALIGN=JUSTIFY>
                                        <bR>
                                    </P>
                                </Td>
                                <td WIDTH=56 StYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p laNG=pt-BR cLasS="western" ALIGN=JUSTIFY>
                                        <BR>
                                    </p>
                                </td>
                                <td WIDTH=136 sTYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <P lANG="pt-BR" ClAsS=western ALIGN=JUSTIFY>
                                        <Br>
                                    </P>
                                </tD>
                            </Tr>
                            <tr VALIGN=TOP>
                                <tD WIDTH=23 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LAnG=pt-BR cLASs="western" ALIGN=CENTER>
                                        <FoNT SIZE=2 sTYle="font-size: 11pt">3</font>
                                    </p>
                                </TD>
                                <TD WIDTH=196 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P lanG="pt-BR" ClaSS=western ALIGN=JUSTIFY>
                                        <fONt SIZE=2 stYLE="font-size: 11pt">Jangka waktu surat penawaran</FoNT>
                                    </P>
                                </td>
                                <tD WIDTH=46 ALIGN=CENTER STyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                        <fonT SIZE=2 styLe="font-size: 11pt">+</fONT>
                                    
                                </tD>
                                <td WIDTH=72 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LANG=pt-BR clasS=western ALIGN=JUSTIFY>
                                        <Br>
                                    </P>
                                </Td>
                                <TD WIDTH=56 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LAng=pt-BR ClasS="western" ALIGN=JUSTIFY>
                                        <Br>
                                    </p>
                                </TD>
                                <Td WIDTH=136 STyLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p LaNG=pt-BR clAsS="western" ALIGN=JUSTIFY>
                                        <br>
                                    </P>
                                </Td>
                            </tR>
                            <TR VALIGN=TOP>
                                <tD WIDTH=23 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P Lang="pt-BR" ClAss="western" ALIGN=CENTER>
                                        <FoNT SIZE=2 sTYlE="font-size: 11pt">4</Font>
                                    </P>
                                </td>
                                <tD WIDTH=196 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LANg="pt-BR" clasS=western ALIGN=JUSTIFY>
                                        <FonT SIZE=2 Style="font-size: 11pt">Jangka waktu pelaksanaan</FONt>
                                    </P>
                                </TD>
                                <Td WIDTH=46 ALIGN=CENTER stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                        <Font SIZE=2 stYLE="font-size: 11pt">+</fONT>
                                    
                                </Td>
                                <td WIDTH=72 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LanG=pt-BR ClaSS="western" ALIGN=JUSTIFY>
                                        <Br>
                                    </P>
                                </Td>
                                <TD WIDTH=56 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LAnG=pt-BR CLASS=western ALIGN=JUSTIFY>
                                        <Br>
                                    </p>
                                </tD>
                                <Td WIDTH=136 style="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p laNG=pt-BR CLASs="western" ALIGN=JUSTIFY>
                                        <br>
                                    </p>
                                </td>
                            </TR>
                            <tR VALIGN=TOP>
                                <td WIDTH=23 StYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P lAnG="pt-BR" ClaSS=western ALIGN=CENTER>
                                        <fONT SIZE=2 stYLe="font-size: 11pt">5</fOnT>
                                    </p>
                                </tD>
                                <td WIDTH=196 STyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p laNg=pt-BR cLAss=western ALIGN=JUSTIFY>
                                        <foNT SIZE=2 STyLe="font-size: 11pt">Tanggal</FonT>
                                    </P>
                                </Td>
                                <td WIDTH=46 ALIGN=CENTER StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                        <FOnt SIZE=2 sTYlE="font-size: 11pt">+</fOnt>
                                    
                                </TD>
                                <TD WIDTH=72 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P laNg=pt-BR CLAsS=western ALIGN=JUSTIFY>
                                        <bR>
                                    </p>
                                </td>
                                <Td WIDTH=56 sTyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LaNg=pt-BR clASS="western" ALIGN=JUSTIFY>
                                        <bR>
                                    </p>
                                </Td>
                                <TD WIDTH=136 styLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p Lang=pt-BR ClAsS=western ALIGN=JUSTIFY>
                                        <bR>
                                    </P>
                                </td>
                            </tR>
                            <TR VALIGN=TOP>
                                <td WIDTH=23 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p lANg="pt-BR" CLAsS="western" ALIGN=JUSTIFY>
                                        <bR>
                                    </p>
                                </TD>
                                <Td WIDTH=196 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P lANG=pt-BR cLasS=western ALIGN=JUSTIFY>
                                        <fONt SIZE=2 stylE="font-size: 11pt">HASIL AKHIR</FonT>
                                    </P>
                                </TD>
                                <tD WIDTH=46 StYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P lang="pt-BR" ClASs=western ALIGN=JUSTIFY>
                                        <bR>
                                    </p>
                                </tD>
                                <tD WIDTH=72 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LanG=pt-BR clasS="western" ALIGN=JUSTIFY>
                                        <BR>
                                    </P>
                                </td>
                                <td WIDTH=56 StYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LANG=pt-BR cLASS="western" ALIGN=JUSTIFY>
                                        <Br>
                                    </p>
                                </tD>
                                <TD WIDTH=136 stYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <P cLASs="western" ALIGN=JUSTIFY>
                                        <FoNt SIZE=2 StYLe="font-size: 11pt"><Span LanG="pt-BR">LULUS/</SPaN></FOnt>
                                        <sTRIKe><FoNt SIZE=2 styLe="font-size: 11pt"><SPAN LaNg=pt-BR>TDK LULUS</SpAn></fonT></StrIKe>
                                    </P>
                                </Td>
                            </Tr>
                        </TAblE>

                    </li>
                    <p></p>
                    <p></p>
                    <p></p>
                    <p></p>
                    <p></p>
                    <li>
                        Evaluasi Teknis
                        <TAblE WIDTH=649 CELLPADDING=7 CELLSPACING=0>
                            
                                                    <tR>
                                                        <td ROWSPAN=3 WIDTH=23 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <P lanG="pt-BR" cLaSS=western ALIGN=CENTER><font SIZE=2 sTyLE="font-size: 11pt"><B>NO</B></FoNT>
                                                            </p>
                                                        </Td>
                                                        <td ROWSPAN=3 WIDTH=196 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <P LANg=pt-BR cLass="western" ALIGN=CENTER>
                                                                <FOnt SIZE=2 style="font-size: 11pt"><B>NAMA DOKUMEN</B></font>
                                                            </P>
                                                        </tD>
                                                        <Td COLSPAN=2 WIDTH=132 VALIGN=TOP STylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <P lANg=pt-BR ClaSs=western ALIGN=CENTER>
                                                                <foNt SIZE=2 stYlE="font-size: 11pt"><B>KELENGKAPAN</B></FOnt>
                                                            </p>
                                                        </td>
                                                        <TD ROWSPAN=3 WIDTH=56 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <P lANg="pt-BR" cLasS=western ALIGN=CENTER>
                                                                <FoNt SIZE=2 STyle="font-size: 11pt"><B>Tdk ada</B></fONt>
                                                            </P>
                                                        </tD>
                                                        <TD ROWSPAN=3 WIDTH=136 StYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                            <P LAng="pt-BR" CLaSs=western ALIGN=CENTER>
                                                                <Font SIZE=2 STyle="font-size: 11pt"><B>KETERANGAN</B></FoNT>
                                                            </p>
                                                        </td>
                                                    </Tr>
                                                    <tR VALIGN=TOP>
                                                        <Td COLSPAN=2 WIDTH=132 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p lANg="pt-BR" clasS=western ALIGN=CENTER>
                                                                <fOnt SIZE=2 sTYLE="font-size: 11pt"><b>Ada (+)</B></foNt>
                                                            </P>
                                                        </tD>
                                                    </tr>
                                                    <tR VALIGN=TOP>
                                                        <TD WIDTH=46 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p LaNG=pt-BR clASs=western ALIGN=JUSTIFY>
                                                                <FONT SIZE=2 sTYlE="font-size: 11pt"><B>Sesuai</b></Font>
                                                            </p>
                                                        </TD>
                                                        <tD WIDTH=72 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p lang=pt-BR cLAss=western ALIGN=JUSTIFY>
                                                                <fOnT SIZE=2 stYLE="font-size: 11pt"><b>Tdk Sesuai</B></fONt>
                                                            </p>
                                                        </tD>
                                                    </Tr>
                                                    <tR VALIGN=TOP>
                                                        <tD WIDTH=23 STyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p LANg="pt-BR" Class=western ALIGN=CENTER>
                                                                <FONt SIZE=2 StylE="font-size: 11pt">1</FOnt>
                                                            </p>
                                                        </tD>
                                                        <td WIDTH=196 STylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <P laNg=pt-BR cLASs=western ALIGN=JUSTIFY>
                                                                <Font SIZE=2 sTYlE="font-size: 11pt">Time Schedule</fOnt>
                                                            </P>
                                                        </tD>
                                                        <td WIDTH=46 ALIGN=CENTER sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <font SIZE=2 StyLE="font-size: 11pt">+</foNt>
                                                            
                                                        </Td>
                                                        <tD WIDTH=72 sTYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p lAnG="pt-BR" cLASs=western ALIGN=JUSTIFY>
                                                                <br>
                                                            </p>
                                                        </Td>
                                                        <Td WIDTH=56 StyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p lANG=pt-BR ClAsS=western ALIGN=JUSTIFY>
                                                                <Br>
                                                            </p>
                                                        </td>
                                                        <TD WIDTH=136 sTyLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                            <P lAng=pt-BR cLaSs="western" ALIGN=JUSTIFY>
                                                                <bR>
                                                            </P>
                                                        </tD>
                                                    </tR>
                                                    <tR VALIGN=TOP>
                                                        <TD WIDTH=23 STyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p lanG="pt-BR" CLAss=western ALIGN=CENTER>
                                                                <fonT SIZE=2 sTYlE="font-size: 11pt">2</Font>
                                                            </p>
                                                        </td>
                                                        <Td WIDTH=196 style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p LAnG=pt-BR CLass=western ALIGN=JUSTIFY>
                                                                <fONT SIZE=2 stYlE="font-size: 11pt">Jangka Waktu pelaksanaan</fonT>
                                                            </p>
                                                        </Td>
                                                        <tD WIDTH=46 ALIGN=CENTER StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            
                                                                <foNT SIZE=2 STylE="font-size: 11pt">+</FONt>
                                                            
                                                        </TD>
                                                        <td WIDTH=72 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p LaNG=pt-BR ClAss=western ALIGN=JUSTIFY>
                                                                <bR>
                                                            </p>
                                                        </tD>
                                                        <td WIDTH=56 stYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p LANG="pt-BR" ClaSs=western ALIGN=JUSTIFY>
                                                                <bR>
                                                            </P>
                                                        </TD>
                                                        <TD WIDTH=136 StYLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                            <p LaNG=pt-BR cLASS=western ALIGN=JUSTIFY>
                                                                <BR>
                                                            </p>
                                                        </TD>
                                                    </TR>
                                                    <tR VALIGN=TOP>
                                                        <td WIDTH=23 stYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p lANG="pt-BR" clASS="western" ALIGN=JUSTIFY>
                                                                <BR>
                                                            </p>
                                                        </TD>
                                                        <TD WIDTH=196 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p lang=pt-BR clAsS=western ALIGN=JUSTIFY>
                                                                <FOnt SIZE=2 stYLe="font-size: 11pt">HASIL AKHIR</font>
                                                            </p>
                                                        </tD>
                                                        <Td WIDTH=46 StyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p lanG=pt-BR clASs="western" ALIGN=JUSTIFY>
                                                                <bR>
                                                            </P>
                                                        </tD>
                                                        <td WIDTH=72 StYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <P lAnG="pt-BR" ClaSs=western ALIGN=JUSTIFY>
                                                                <BR>
                                                            </p>
                                                        </Td>
                                                        <td WIDTH=56 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p LAng=pt-BR clASS=western ALIGN=JUSTIFY>
                                                                <BR>
                                                            </P>
                                                        </Td>
                                                        <tD WIDTH=136 styLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                            <P cLASS=western ALIGN=JUSTIFY>
                                                                <FonT SIZE=2 styLE="font-size: 11pt"><SPaN Lang="pt-BR">LULUS/</SpAn></fONT>
                                                                <sTrIKe><foNT SIZE=2 StYLe="font-size: 11pt"><Span LaNg=pt-BR>TDK LULUS</SPan></FoNT></STRike>
                                                            </p>
                                                        </tD>
                                                    </Tr>
                        </tAble>
                    </li>
                    <br/>
                    <li>
                        Evaluasi Harga
                        <TABLe WIDTH=649 CELLPADDING=7 CELLSPACING=0>
                            <Tr>
                                <Td ROWSPAN=3 WIDTH=23 stYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">                                
                                    <FOnt SIZE=2 StYle="font-size: 11pt"><B>NO</b></fonT>
                                </tD>
                                <td ROWSPAN=3 WIDTH=196 ALIGN=CENTER STyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <FOnt SIZE=2 STyLe="font-size: 11pt"><B>NAMA DOKUMEN</b></FoNt>
                                </tD>
                                <Td COLSPAN=2 ALIGN=CENTER WIDTH=132 VALIGN=TOP stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                        <FoNT SIZE=2 sTyLE="font-size: 11pt"><B>KELENGKAPAN</B></foNT>
                                
                                </TD>
                                <tD ROWSPAN=3 ALIGN=CENTER WIDTH=56 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                        <FONt SIZE=2 sTylE="font-size: 11pt"><b>Tdk ada</B></FonT>
                                    
                                </tD>
                                <tD ROWSPAN=3 ALIGN=CENTER WIDTH=136 Style="border: 1px solid #000000; padding: 0in 0.08in">
                                    
                                        <FONT SIZE=2 Style="font-size: 11pt"><b>KETERANGAN</B></FONt>
                                    
                                </Td>
                            </tR>
                            <Tr VALIGN=TOP>
                                <td COLSPAN=2 WIDTH=132 ALIGN=CENTER sTyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                
                                        <fONt SIZE=2 styLe="font-size: 11pt"><b>Ada (+)</b></FONt>
                                </Td>
                            </tR>
                            <TR VALIGN=TOP>
                                <TD WIDTH=46 ALIGN=CENTER sTylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                        <fOnT SIZE=2 StYle="font-size: 11pt"><B>Sesuai</b></FoNT>
                                    
                                </td>
                                <tD WIDTH=72 ALIGN=CENTER StYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p Lang=pt-BR CLaSs=western ALIGN=JUSTIFY>
                                        <FonT SIZE=2 StYLe="font-size: 11pt"><B>Tdk Sesuai</b></FOnt>
                                    </P>
                                </Td>
                            </tr>
                            <tR VALIGN=TOP>
                                <Td WIDTH=23 StYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LanG=pt-BR CLasS="western" ALIGN=CENTER>
                                        <FoNt SIZE=2 sTyLe="font-size: 11pt">1</FoNT>
                                    </P>
                                </Td>
                                <Td WIDTH=196 STYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LANg=pt-BR ClaSS=western ALIGN=JUSTIFY>
                                        <FoNT SIZE=2 StyLe="font-size: 11pt">Daftar Kuantitas dan Harga</FoNT>
                                    </P>
                                </td>
                                <td WIDTH=46 ALIGN=CENTER STYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                        <FONT SIZE=2 styLe="font-size: 11pt">+</fONT>
                                    
                                </tD>
                                <TD WIDTH=72 STYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LaNg="pt-BR" cLAsS="western" ALIGN=JUSTIFY>
                                        <Br>
                                    </p>
                                </Td>
                                <td WIDTH=56 stYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P lang="pt-BR" ClASs="western" ALIGN=JUSTIFY>
                                        <BR>
                                    </P>
                                </Td>
                                <Td WIDTH=136 stYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p lAnG=pt-BR ClasS=western ALIGN=JUSTIFY>
                                        <bR>
                                    </P>
                                </tD>
                            </tr>
                            <tr VALIGN=TOP>
                                <Td WIDTH=23 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LanG=pt-BR CLASS="western" ALIGN=JUSTIFY>
                                        <Br>
                                    </P>
                                </Td>
                                <tD WIDTH=196 STyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LANg=pt-BR clAsS="western" ALIGN=JUSTIFY>
                                        <Font SIZE=2 sTYlE="font-size: 11pt">HASIL AKHIR</FonT>
                                    </p>
                                </td>
                                <TD WIDTH=46 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LAnG=pt-BR class=western ALIGN=JUSTIFY>
                                        <Br>
                                    </p>
                                </TD>
                                <Td WIDTH=72 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P lANG=pt-BR ClASS=western ALIGN=JUSTIFY>
                                        <bR>
                                    </P>
                                </td>
                                <td WIDTH=56 STYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p Lang="pt-BR" cLasS=western ALIGN=JUSTIFY>
                                        <BR>
                                    </P>
                                </TD>
                                <td WIDTH=136 sTyLE="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p claSS=western ALIGN=JUSTIFY>
                                        <Font SIZE=2 Style="font-size: 11pt"><SpaN LanG="pt-BR">LULUS/</SPAN></FonT>
                                        <StrikE><FOnt SIZE=2 stYLE="font-size: 11pt"><SPan LAng=pt-BR>TDK LULUS</sPAn></fOnt></STriKE>
                                    </p>
                                </td>
                            </tR>
                        </TABLe>

                    </li>
                    <br/>
                    <li>
                        Berdasarkan Berita Acara Evaluasi, Klarifikasi dan Negosiasi Nomor : <?php print((!empty($surat_negosiasi[0]->no_surat))?$surat_negosiasi[0]->no_surat:''); ?> tanggal <?php print((!empty($surat_negosiasi[0]->tgl_surat))?tgl_indo($surat_negosiasi[0]->tgl_surat):''); ?>, Klarifikasi dilakukan terhadap 1 (satu) penawar yang responsive, dengan hasil sebagai berikut :
                            <taBle WIDTH=649 CELLPADDING=7 CELLSPACING=0>
                                <tr>
                                    <tD WIDTH=85 ALIGN=CENTER sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in; font-size:14pt">
                                        <b>NO</b>
                                    </td>
                                    <td WIDTH=297 ALIGN=CENTER sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in; font-size:14pt">
                                        <b>NAMA PERUSAHAAN</b>
                                    </Td>
                                    <td WIDTH=156 ALIGN=CENTER StYLe="border: 1px solid #000000; padding: 0in 0.08in; font-size:14pt">
                                        <b>NILAI PENAWARAN</B>
                                    </Td>
                                    <td WIDTH=156 ALIGN=CENTER StYLe="border: 1px solid #000000; padding: 0in 0.08in; font-size:14pt">
                                        <b>NILAI NEGOSIASI</B>
                                    </Td>
                                    <td WIDTH=156 ALIGN=CENTER StYLe="border: 1px solid #000000; padding: 0in 0.08in; font-size:14pt">
                                        <b>HASIL KUALIFIKASI</B>
                                    </Td>
                                </tr>
                                <tR VALIGN=TOP>
                                    <tD WIDTH=85 ALIGN=CENTER sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in; font-size:14pt">
                                        1
                                    </td>
                                    <td WIDTH=297 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in; font-size:14pt">
                                        <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama); ?>
                                    </Td>
                                    <td WIDTH=156 ALIGN=CENTER StYLe="border: 1px solid #000000; padding: 0in 0.08in; font-size:14pt">
                                        <?php print('Rp '.format_money($kontrak_pihak_ketiga[0]->nilai_kontrak).',-'); ?>
                                    </Td>
                                    <td WIDTH=156 ALIGN=CENTER StYLe="border: 1px solid #000000; padding: 0in 0.08in; font-size:14pt">
                                        <?php print('Rp '.format_money($kontrak_pekerjaan->harga_negosiasi).',-'); ?>
                                    </Td>
                                    <td WIDTH=156 ALIGN=CENTER StYLe="border: 1px solid #000000; padding: 0in 0.08in; font-size:14pt">
                                        LULUS
                                    </Td>
                                </Tr>
                            </taBle>
                    </li>

                </ul>
            </li>

        </ol>
    </p>
    
    <p cLAss=western ALIGN="justify" stylE="margin-bottom: 0in">
        Demikian Berita Acara Penyusunan Hasil Pengadaan (BAHPL) Pekerjaan <?php print($kontrak_pekerjaan->aktivitas_nama); ?> ini dibuat dengan sebenarnya, d itandatangani oleh Pejabat Pengadaan, untuk digunakan sebagaimana mestinya.
    </p>

    <p></p>
    <p></p>
    <table cellpadding="2">
        <tr>
            <td width="600" class="text">&nbsp;</td>
            <td align="center" width="400" style="font-size:16pt">
                <?php print($pejabat_pengadaan->jenis_pejabat_pengadaan_nama); ?> <?php print($pejabat_pengadaan->seri_pejabat_pengadaan_nama); ?><br/>
                Pada Dinas Pekerjaan Umum Kota Semarang<br/>
                Tahun Anggaran <?php print(date("Y"));?><br/>
                <br/><br/><br/><br/><br/>
                <u><?php print($pejabat_pengadaan->pegawai_nama); ?></u><br/>
                NIP. <?php print($pejabat_pengadaan->pegawai_nip); ?>
            </td>
        </tr>
    </table>
    
</BODY>

</HTML>