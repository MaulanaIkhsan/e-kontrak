<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumen_pihak_ketiga_model extends CI_Model {

    /*----- dokumen_pihak_ketiga -----*/
    // id
    // nama_dokumen
    // pihak_ketiga_id
    // jenis_dokumen_id
    // file
    // tgl_awal_aktif
    // tgl_akhir_aktif
    // is_aktif

    var $column_order = array(null, 'dokumen_pihak_ketiga.id', 
                'dokumen_pihak_ketiga.nama_dokumen',
                'pihak_ketiga.nama_perusahaan', 
                'jenis_dokumen_pihak_ketiga.nama', 
                'dokumen_pihak_ketiga.file', 
                'dokumen_pihak_ketiga.tgl_awal_aktif',
                'dokumen_pihak_ketiga.tgl_akhir_aktif',
                'dokumen_pihak_ketiga.is_aktif'); 

    var $column_search = array('dokumen_pihak_ketiga.id', 
                'dokumen_pihak_ketiga.nama_dokumen',
                'pihak_ketiga.nama_perusahaan', 
                'jenis_dokumen_pihak_ketiga.nama', 
                'dokumen_pihak_ketiga.file', 
                'dokumen_pihak_ketiga.tgl_awal_aktif',
                'dokumen_pihak_ketiga.tgl_akhir_aktif',
                'dokumen_pihak_ketiga.is_aktif');

    // default order 
    var $order = array('dokumen_pihak_ketiga.id' => 'asc');

    public function fetch_data($id=NULL, $pihak_ketiga_id=NULL, $is_aktif='n')
    {
        $sql = $this->db->select('dokumen_pihak_ketiga.id as id, 
                                dokumen_pihak_ketiga.nama_dokumen as nama_dokumen,
                                pihak_ketiga.nama_perusahaan as pihak_ketiga, 
                                dokumen_pihak_ketiga.jenis_dokumen_id as jenis_dokumen_id,
                                jenis_dokumen_pihak_ketiga.nama as jenis_dokumen, 
                                dokumen_pihak_ketiga.file as file, 
                                dokumen_pihak_ketiga.tgl_awal_aktif as tgl_awal_aktif,
                                dokumen_pihak_ketiga.tgl_akhir_aktif as tgl_akhir_aktif,
                                dokumen_pihak_ketiga.is_aktif as is_aktif')
                        ->from('dokumen_pihak_ketiga')
                        ->join('pihak_ketiga', 'dokumen_pihak_ketiga.pihak_ketiga_id=pihak_ketiga.id', 'inner')
                        ->join('jenis_dokumen_pihak_ketiga', 'dokumen_pihak_ketiga.jenis_dokumen_id=jenis_dokumen_pihak_ketiga.id', 'left');

        if($id!=NULL) {
            $sql->where('dokumen_pihak_ketiga.id', $id);
        }
        if($pihak_ketiga_id!=NULL) {
            $sql->where('dokumen_pihak_ketiga.pihak_ketiga_id', $pihak_ketiga_id);
        }
        if($is_aktif!='n') {
            $sql->where('dokumen_pihak_ketiga.is_aktif', 'y');
        }

        if($id!=NULL){
            return $sql->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }
    
    /*====== Serverside Datatable ======*/
	private function _get_datatables_query($pihak_ketiga_id)
	{
        $this->db->select('dokumen_pihak_ketiga.id as id, 
                    dokumen_pihak_ketiga.nama_dokumen as nama_dokumen,
                    pihak_ketiga.nama_perusahaan as pihak_ketiga, 
                    jenis_dokumen_pihak_ketiga.nama as jenis_dokumen, 
                    dokumen_pihak_ketiga.file as file, 
                    dokumen_pihak_ketiga.tgl_awal_aktif as tgl_awal_aktif,
                    dokumen_pihak_ketiga.tgl_akhir_aktif as tgl_akhir_aktif,
                    dokumen_pihak_ketiga.is_aktif as is_aktif');

        $this->db->from('dokumen_pihak_ketiga');
        $this->db->join('pihak_ketiga', 'dokumen_pihak_ketiga.pihak_ketiga_id=pihak_ketiga.id', 'inner');
        $this->db->join('jenis_dokumen_pihak_ketiga', 'dokumen_pihak_ketiga.jenis_dokumen_id=jenis_dokumen_pihak_ketiga.id', 'left');
        $this->db->where('dokumen_pihak_ketiga.pihak_ketiga_id', $pihak_ketiga_id);

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($pihak_ketiga_id)
	{
		$this->_get_datatables_query($pihak_ketiga_id);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($pihak_ketiga_id)
	{
		$this->_get_datatables_query($pihak_ketiga_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($pihak_ketiga_id)
	{
		$this->db->select('dokumen_pihak_ketiga.id as id, 
                    dokumen_pihak_ketiga.nama_dokumen as nama_dokumen,
                    pihak_ketiga.nama_perusahaan as pihak_ketiga, 
                    jenis_dokumen_pihak_ketiga.nama as jenis_dokumen, 
                    dokumen_pihak_ketiga.file as file, 
                    dokumen_pihak_ketiga.tgl_awal_aktif as tgl_awal_aktif,
                    dokumen_pihak_ketiga.tgl_akhir_aktif as tgl_akhir_aktif,
                    dokumen_pihak_ketiga.is_aktif as is_aktif');

        $this->db->from('dokumen_pihak_ketiga');
        $this->db->join('pihak_ketiga', 'dokumen_pihak_ketiga.pihak_ketiga_id=pihak_ketiga.id', 'inner');
        $this->db->join('jenis_dokumen_pihak_ketiga', 'dokumen_pihak_ketiga.jenis_dokumen_id=jenis_dokumen_pihak_ketiga.id', 'left');
        $this->db->where('dokumen_pihak_ketiga.pihak_ketiga_id', $pihak_ketiga_id);

		return $this->db->count_all_results();
	}
	/*====== /Serverside Datatable ======*/
}

/* End of file Dokumen_pihak_ketiga_model.php */
