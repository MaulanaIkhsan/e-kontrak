<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_surat_penawaran" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Upload</h4>
            </div>
            <div class="modal-body form">
                <div class="callout callout-danger">
                    <h4>Perhatian!</h4>
                    <p>Harap upload lampiran dengan format <strong><u>PDF</u></strong> dan size <strong><u>kurang dari 2MB</u></strong>  (atau compress terlebih dahulu)</p>
                </div>
                <form id="form-surat-penawaran" class="form-horizontal" enctype="multipart/form-data" method="post">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Jenis Surat / Dokumen</label>
                            <div class="col-md-9">
                                <select name="jenis_surat_id" class="form-control" id="jenis_surat">
                                    <option value="">--Select Nama Surat--</option>
                                    <?php foreach($jenis_surat as $item):?>
                                        <option value="<?php print($item->id); ?>"><?php print($item->nama); ?></option>
                                    <?php endforeach;?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Kontrak Pihak Ketiga</label>
                            <div class="col-md-9">
                                
                                <input type="hidden" name="kontrak_pihak_ketiga_id" />
                                <input type="text" name="pihak_ketiga_nama" class="form-control" readonly="readonly" />
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tempat Rapat</label>
                            <div class="col-md-9">
                                <select name="tempat_rapat_id" class="form-control" id="tempat_rapat">
                                    <option value="">--- Select Tempat Rapat ---</option>
                                    <?php foreach($tempat_rapat as $item):?>
                                        <option value="<?php print($item->id); ?>"><?php print($item->nama); ?></option>
                                    <?php endforeach;?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tgl Surat</label>
                            <div class="col-md-9">
                                <input name="tgl_surat" placeholder="Tanggal Surat" class="form-control datepicker" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">No Surat</label>
                            <div class="col-md-9">
                                <input name="no_surat" placeholder="Tanggal Surat" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Acara</label>
                            <div class="col-md-9">
                                <input name="acara" placeholder="Acara" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Lampiran</label>
                            <div class="col-md-9">
                                <input type="file" name="lampiran" id="lampiran" />
                                <input type="checkbox" name="use_lampiran" value="used" id="used" /> Gunakan Lampiran Sebagai File Utama
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                    </div>
                
            </div>
            <div class="modal-footer">
                    <button type="submit" id="btnSaveSuratPenawaran" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->