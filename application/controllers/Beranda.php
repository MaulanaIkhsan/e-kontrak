<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $sess_data = $this->session->userdata('session_data');
        if($sess_data===NULL) {
            $this->session->set_flashdata('warning', '<div class="alert alert-warning fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a> 
                Harap login terlebih dahulu untuk mengakses sistem</div>');
			redirect('auth');
        }
    }
    
    public function index()
    {
        $this->load->model('Aktivitas_model', 'aktivitas');
        $this->load->model('Pihak_ketiga_model', 'pihak_ketiga');
        $this->load->model('Kontrak_pekerjaan_model', 'kontrak_pekerjaan');
        
        $this->load->helper('generic');
        
        $sess_data  = $this->session->userdata('session_data');
        $user_id    = $sess_data['id'];
        $role       = $sess_data['role'];
        $as         = $sess_data['as'];
        
        if($as=='pegawai') {
            $data['jml_aktivitas']                  = $this->aktivitas->stat_jml_aktivitas($user_id, $role);
            $data['jml_anggaran']                   = $this->aktivitas->stat_anggaran($user_id, $role);
            $data['jml_pihak_ketiga']               = count($this->pihak_ketiga->fetch_data());
            $data['jenis_pekerjaan']                = $this->aktivitas->stat_jenis_pekerjaan($user_id, $role);
            $data['metode_pengadaan']               = $this->aktivitas->stat_jenis_pengadaan($user_id, $role);
            $data['awal_kontrak']                   = $this->kontrak_pekerjaan->stat_awal_kontrak($user_id, $role);
            $data['akhir_kontrak']                  = $this->kontrak_pekerjaan->stat_akhir_kontrak($user_id, $role);
            $data['jml_kontrak_pekerjaan']          = $this->kontrak_pekerjaan->jml_kontrak_pekerjaan($user_id, $role);
            $data['jml_status_kontrak_pekerjaan']   = $this->kontrak_pekerjaan->jml_status_kontrak_pekerjaan($user_id, $role);

            $data['user_id']    = $user_id;
            $data['role']       = $role;
            $data['as']         = $as;
            $data['curr_year']  = date('Y');

            $this->load->view('beranda', $data);
        }
        elseif($as='pihak_ketiga'){
            $data['curr_year']  = date('Y');

            $pihak_ketiga_id = $sess_data['pihak_ketiga_id'];
            

            $data['jml_aktivitas'] = $this->db->select('COUNT(kontrak_pihak_ketiga.id) as jml_aktivitas')
                                            ->from('aktivitas')
                                            ->join('kontrak_pekerjaan', 'aktivitas.id=kontrak_pekerjaan.aktivitas_id', 'inner')
                                            ->join('kontrak_pihak_ketiga', 'kontrak_pekerjaan.id=kontrak_pekerjaan_id', 'inner')
                                            ->join('pihak_ketiga', 'kontrak_pihak_ketiga.pihak_ketiga_id=pihak_ketiga.id', 'inner')
                                            ->where('pihak_ketiga.id', $pihak_ketiga_id)
                                            ->get()->row();
            
            $data['jenis_pekerjaan'] = $this->db->select('jenis_pekerjaan.nama as jenis_pekerjaan, 
                                                        count(aktivitas.id) as total')
                                            ->from('aktivitas')
                                            ->join('jenis_pekerjaan', 'aktivitas.jenis_pekerjaan_id=jenis_pekerjaan.id', 'inner')
                                            ->join('kontrak_pekerjaan', 'aktivitas.id=kontrak_pekerjaan.aktivitas_id', 'inner')
                                            ->join('kontrak_pihak_ketiga', 'kontrak_pekerjaan.id=kontrak_pekerjaan_id', 'inner')
                                            ->join('pihak_ketiga', 'kontrak_pihak_ketiga.pihak_ketiga_id=pihak_ketiga.id', 'inner')
                                            ->where('pihak_ketiga.id', $pihak_ketiga_id)
                                            ->group_by('jenis_pekerjaan.nama')
                                            ->get()->result();

            $data['metode_pengadaan'] = $this->db->select('pengadaan.nama as jenis_pengadaan, 
                                                        count(aktivitas.id) as total')
                                            ->from('aktivitas')
                                            ->join('jenis_pengadaan as pengadaan', 'aktivitas.pengadaan_id=pengadaan.id', 'inner')
                                            ->join('kontrak_pekerjaan', 'aktivitas.id=kontrak_pekerjaan.aktivitas_id', 'inner')
                                            ->join('kontrak_pihak_ketiga', 'kontrak_pekerjaan.id=kontrak_pekerjaan_id', 'inner')
                                            ->join('pihak_ketiga', 'kontrak_pihak_ketiga.pihak_ketiga_id=pihak_ketiga.id', 'inner')
                                            ->where('pihak_ketiga.id', $pihak_ketiga_id)
                                            ->group_by('pengadaan.nama')
                                            ->get()->result();
            
            $data['awal_kontrak'] = $this->db->select('monthname(kontrak_pekerjaan.tgl_awal_kontrak) as bulan, 
                                                count(monthname(kontrak_pekerjaan.tgl_awal_kontrak)) as jml_awal_kontrak')
                                            ->from('kontrak_pekerjaan')
                                            ->join('kontrak_pihak_ketiga', 'kontrak_pekerjaan.id=kontrak_pekerjaan_id', 'inner')
                                            ->join('pihak_ketiga', 'kontrak_pihak_ketiga.pihak_ketiga_id=pihak_ketiga.id', 'inner')
                                            ->where('pihak_ketiga.id', $pihak_ketiga_id)
                                            ->group_by('monthname(kontrak_pekerjaan.tgl_awal_kontrak)')
                                            ->order_by('monthname(kontrak_pekerjaan.tgl_awal_kontrak)', 'asc')
                                            ->get()->result();

            $data['akhir_kontrak'] = $this->db->select('monthname(kontrak_pekerjaan.tgl_akhir_kontrak) as bulan, 
                                                count(monthname(kontrak_pekerjaan.tgl_akhir_kontrak)) as jml_akhir_kontrak')
                                            ->from('kontrak_pekerjaan')
                                            ->join('kontrak_pihak_ketiga', 'kontrak_pekerjaan.id=kontrak_pekerjaan_id', 'inner')
                                            ->join('pihak_ketiga', 'kontrak_pihak_ketiga.pihak_ketiga_id=pihak_ketiga.id', 'inner')
                                            ->where('pihak_ketiga.id', $pihak_ketiga_id)
                                            ->group_by('monthname(kontrak_pekerjaan.tgl_akhir_kontrak)')
                                            ->order_by('monthname(kontrak_pekerjaan.tgl_akhir_kontrak)', 'asc')
                                            ->get()->result();

            $data['jml_status_kontrak_pekerjaan'] = $this->db->select('kontrak_pekerjaan.status as status,
                                                count(kontrak_pekerjaan.id) as jml_kontrak')
                                            ->from('kontrak_pekerjaan')
                                            ->join('kontrak_pihak_ketiga', 'kontrak_pekerjaan.id=kontrak_pekerjaan_id', 'inner')
                                            ->join('pihak_ketiga', 'kontrak_pihak_ketiga.pihak_ketiga_id=pihak_ketiga.id', 'inner')
                                            ->where('pihak_ketiga.id', $pihak_ketiga_id)
                                            ->group_by('kontrak_pekerjaan.status')
                                            ->get()->result();
            
            $this->load->view('kontrak_pihak_ketiga/beranda', $data);
        }
        else {
            print('Error check session user As');
        }
        
        
        
        
    }

}

/* End of file Beranda.php */
