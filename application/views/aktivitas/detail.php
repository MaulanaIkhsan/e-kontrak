<?php $this->load->view('template/header');?>
<!-- Select2 -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/select2/dist/css/select2.min.css')); ?>">
<?php $this->load->view('template/asset_header');?>
<link rel="stylesheet" type="text/css" href="<?php print(base_url('assets/bower_components/datatables.net-bs/css/custom_button_export.css'))?>" />
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')); ?>">
<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <!-- Box Form Pengunjung -->
        <?php 
          print($this->session->flashdata('alert'));
          print($this->session->flashdata('success')); 

          $sess_data  = $this->session->userdata('session_data');
          $user_id    = $sess_data['id'];
          $role       = $sess_data['role'];
          $user_nama  = $sess_data['nama'];
          $user_as    = $sess_data['as'];
        ?>
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-warning" id="form-pengunjung">
              <div class="box-header with-border">
                <h3 class="box-title">Detail Kegiatan</h3>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <form class="form-horizontal" method="post" action="#">
                <div class="box-body">
                  <br/>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Nama Program</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->nama_program); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Nama Pekerjaan</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->nama_pekerjaan); ?></div>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Nama Kegiatan</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->nama_aktivitas); ?></div>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Nomor Rekening</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->no_rekening); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Sumber Dana</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->sumber_dana); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">PPKOM</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->ppkom); ?></div>
                    </div>
                  </div>  
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">PPTK</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->pptk); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Bendahara</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->bendahara); ?></div>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Pagu Anggaran</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print(format_money($detail->anggaran_awal)); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Anggaran Perubahan</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print(format_money($detail->perubahan_anggaran)); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Nilai Kontrak</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print(format_money($detail->nilai_kontrak)); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Tgl Pengajuan Pencairan</label>
                    <div class="col-sm-9">
                      <div class="form-control">
                        <?php 
                        if($detail->tgl_pengajuan_pencairan=='Belum'){
                          print($detail->tgl_pengajuan_pencairan); 
                        } else {
                          print(format_date($detail->tgl_pengajuan_pencairan, 2));
                        }?>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Tgl Pencairan</label>
                    <div class="col-sm-9">
                      <div class="form-control">
                      <?php
                        if($detail->tgl_pencairan=='Belum'){
                          print($detail->tgl_pencairan); 
                        } else {
                          print(format_date($detail->tgl_pencairan, 2));
                        }?>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Jenis Pekerjaan</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->jenis_pekerjaan); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Metode Pengadaan</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->jenis_pengadaan); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Keterangan</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->keterangan); ?></div>
                    </div>
                  </div>            
                </div>
                 <div class="box-footer">
                  <?php 
                  // While on detail data pekerjaan
                  if($this->session->userdata('id_pekerjaan')!=NULL) { ?>
                    <a href="<?php print(base_url('pekerjaan/detail/'.$this->session->userdata('id_pekerjaan'))); ?>" class="btn btn-primary">Kembali</a>
                  <?php } 
                  // While on data kegiatan to open kontrak pekerjaan
                  elseif($data_kegiatan==TRUE) { ?>
                    <a href="<?php print(base_url('aktivitas/kegiatan')); ?>" class="btn btn-primary">Kembali</a>
                  <?php } else { ?>
                    <a href="<?php print(base_url('aktivitas')); ?>" class="btn btn-primary">Kembali</a>
                  <?php } ?>

                  <?php if($role=='Super Admin') { ?>
                    <a href="<?php print(base_url('aktivitas/update/'.$detail->id)); ?>" class="btn btn-warning">Update</a>
                    <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete">Delete</a>
                  <?php } else { 
                    if($detail->pptk_id==$user_id or $detail->ppkom_id==$user_id) {?>
                      <a href="<?php print(base_url('aktivitas/update/'.$detail->id)); ?>" class="btn btn-warning">Update</a>
                      <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete">Delete</a>
                  <?php } } ?>
                  
                  <?php if($data_kontrak==TRUE) { ?>
                    <span class="label label-success">Opened Contract</span></td>
                  <?php }?>
                </div>
                <!-- /.box-footer -->
              </form>

              <!-- Modal Delete Aktivitas -->
              <div class="modal fade" id="modal-delete">
                <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title"><i class="fa fa-warning"></i> Konfirmasi Delete Data</h4>
                      </div>
                      <div class="modal-body">
                          <p>Apakah anda yakin akan menghapus data kegiatan ini ?</p>
                      </div>
                      <div class="modal-footer">
                          <form method="post" action="<?php print(base_url('aktivitas/delete/'.$detail->id)); ?>">
                              <a href="#" class="btn btn-default" data-dismiss="modal">Tidak</a>
                              <button type="submit" name="delete" class="btn btn-primary">Delete</button>
                          </form>
                      </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>
              <!-- /.modal -->
              
              <?php if($penawaran==TRUE){ ?>              
                  <div class="nav-tabs-custom">
                    <!-- Tabs within a box -->
                    <ul class="nav nav-tabs pull-left">
                        <li class="active"><a href="#data-dokumen" data-toggle="tab">Data Kontrak Pekerjaan</a></li>                    
                    </ul>
                    <div class="tab-content no-padding">
                        <div class="chart tab-pane active" id="data-kontrak" style="position: relative;">
                            <?php if($data_kontrak==FALSE) { ?>
                              <p>
                                <a href="javascript:void(0)" class="btn btn-primary" onclick="add_kontrak()">Buka Penawaran</a>
                              </p>
                            <?php } ?>
                            <table id="tabel_kontrak" class="table table-bordered table-striped table-responsive">
                                <thead>
                                    <tr class="headings" align="center">
                                        <th class="column-title" align="center">No</th>
                                        <th class="column-title" align="center">Nama Pekerjaan</th>
                                        <th class="column-title" align="center">Tahun</th>
                                        <th class="column-title" align="center">Pejabat Pengadaan</th>
                                        <th class="column-title" align="center">Tgl Awal Kontrak</th>
                                        <th class="column-title" align="center">Tgl Akhir Kontrak</th>
                                        <th class="column-title" align="center">HPS</th>
                                        <th class="column-title" align="center">Status</th>
                                        <th class="column-title" align="center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                  </div>
              <?php } ?>

              

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/dataTables.buttons.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.flash.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/jszip.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/pdfmake.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/vfs_fonts.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.html5.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.print.min.js'))?>"></script>

<!-- Number divider -->
<script src="<?php print(base_url('assets/my_custom/js/number-divider.min.js')); ?>"></script>
<!-- bootstrap datepicker -->
<script src="<?php print(base_url('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>"></script>
<!-- Select2 -->
<script src="<?php print(base_url('assets/bower_components/select2/dist/js/select2.full.min.js'));?>"></script>
<script>
  var tabel_kontrak;
  $(function(){
    $('.select2').select2({
      closeOnSelect: true
    });

    $('.datepicker').datepicker({
        format: 'dd-M-yyyy',
        autoclose: true
    });

    //Just number input
    $(".money").on("keypress keyup",function (event) {    
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    tabel_kontrak = $('#tabel_kontrak').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
            "ajax": {
                "url": "<?php echo site_url('kontrak_pekerjaan/get_data/'.$detail->id);?>",
                "type": "POST"
            },
            dom: 'lBfrtip',
            button:[{
              extend:'pdf',
              oriented:'potrait',
              pageSize: 'A4',
              title : 'Data Dokumen Kontrak Pekerjaan',
              download : 'open'
            }, 'copy', 'csv', 'excel', 'pdf', 'print'],
            "columnDefs": [{ 
                "targets": [ 0 ], 
                "orderable": false, 
            }]
        });



  });

  function reload_data_kontrak() 
  {
    tabel_kontrak.ajax.reload();
  }

  function add_kontrak() {
      $('#form-penawaran')[0].reset(); // reset form on modals
      $('.form-group').removeClass('has-error'); // clear error class
      $('.help-block').empty(); // clear error string
      $('#modal-create-penawaran').modal('show'); // show bootstrap modal
      $('.modal-title').text('Create Penawaran'); // Set Title to Bootstrap modal title
      $('#aktivitas_id').val("<?php print $detail->id; ?>");
      <?php if($user_as=='Pegawai' or $role!='Super Admin') {?>
        $('[name="pejabat_pengadaan"]').val("<?php print $pejabat_pengadaan->pejabat_pengadaan_id; ?>");
        $('[name="pejabat_pengadaan_nama"]').val("<?php print $user_nama; ?>");
      <?php } ?>
      
  }

  function save_kontrak() 
  {
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
    
    url = "<?php echo site_url('kontrak_pekerjaan/ajax_add/'.$id)?>";
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form-penawaran').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            if(data.status)
            {
                $('#modal-create-penawaran').modal('hide');
                alert('Data berhasil tersimpan');
                reload_data_kontrak();
                window.location.reload();
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            console.log('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
  }
</script>
<?php 
    // Load Modal Form
    $this->load->view('kontrak_pekerjaan/modal_form');
?>
<?php $this->load->view('template/footer');?>