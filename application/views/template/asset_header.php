<!-- Tell the browser to be responsive to screen width -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css')); ?>">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/font-awesome/css/font-awesome.min.css')); ?>">
<!-- Ionicons -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/Ionicons/css/ionicons.min.css')); ?>">

<!-- DataTables -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')); ?>">
<!-- Theme style -->
<link rel="stylesheet" href="<?php print(base_url('assets/dist/css/AdminLTE.min.css')); ?>">
<!-- AdminLTE Skins. We have chosen the skin-blue for this starter
      page. However, you can choose any other skin. Make sure you
      apply the skin class to the body tag so the changes take effect. -->
<link rel="stylesheet" href="<?php print(base_url('assets/dist/css/skins/skin-yellow.min.css')); ?>">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php print(base_url('assets/plugins/iCheck/all.css')); ?>">


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<!-- <link rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->