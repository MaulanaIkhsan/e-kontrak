<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_pekerjaan_model extends CI_Model {

	var $main_table = 'jenis_pekerjaan';

	public function get_jenis_pekerjaan()
	{
		return $this->db->select('id, nama')
						->from($this->main_table)
						->get()->result();
	}

	public function insert($data)
	{
		$sql = $this->db->insert($this->main_table, $data);
		if($sql){
			return 'ok';
		}
		else {
			return 'fail';
		}
	}

	public function show()
	{
		return $this->db->select('id, nama, nickname')
						->from($this->main_table)
						->get()->result();
	}

	public function update($data, $key)
	{
		$sql = $this->db->where($key)
						->update($this->main_table, $data);
		if($sql){
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	public function show_update($key)
	{
		return $this->db->select('id, nama, nickname')
						->from($this->main_table)
						->where('id', $key)
						->get()->row();
	}

	public function delete($key)
	{
		$sql = $this->db->delete($this->main_table, $key);
		if ($sql) {
			return 'ok';
		}
		else{
			return 'fail';
		}
	}

}

/* End of file Jenis_pekerjaan_model.php */
/* Location: ./application/models/Jenis_pekerjaan_model.php */