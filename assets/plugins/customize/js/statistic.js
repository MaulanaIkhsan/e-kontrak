$(function(){
    function piechart(component, data_item, title_chart) {
        $(component).dxPieChart({
            size:{ 
                width: 450
            },
            dataSource: data_item,
            series: [
                {
                    argumentField: "country",
                    valueField: "area",
                    label:{
                        visible: true,
                        connector:{
                            visible:true,           
                            width: 1
                        }
                    }
                }
            ],
            title: title_chart
        });
    
        return true;
    }
});
