<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generic_model extends CI_Model {

	public function insert($table, $data)
	{
		$sql = $this->db->insert($table, $data);
		if($sql){
			return 'ok';
		}
		else {
			return 'fail';
		}
    }
    
  public function update($table, $data, $key)
	{
		foreach($key as $keys => $values):
			if(is_array($values)){
				$this->db->where_in($keys, $values);
			} else {
				$this->db->where($keys, $values);
			}
		endforeach;

		$sql = $this->db->update($table, $data);
		if($sql){
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}
    
	public function delete($table, $key)
	{
		foreach($key as $keys => $values):
			if(is_array($values)){
				$this->db->where_in($keys, $values);
			} else {
				$this->db->where($keys, $values);
			}
		endforeach;
		
		$sql = $this->db->delete($table);
		if ($sql) {
			return 'ok';
		}
		else{
			return 'fail';
		}
	}

	public function fetch_data($table, $column, $type='all', $format='object', $where='null', 
						$order_by='null', $group_by='null', $relations='null')
	{
		return 'fetch data';
	}

	public function get_last_id($table)
	{
		return $this->db->select('id')
						->from($table)
						->order_by('id', 'desc')
						->limit(1)
						->get()->row();
	}

}

/* End of file Generic_model.php */
/* Location: ./application/models/Generic_model.php */