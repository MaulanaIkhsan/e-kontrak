<?php
$this->load->view('template/header');?>

<?php $this->load->view('template/asset_header');?>
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/jquery-ui/themes/dot-luv/jquery-ui.min.css')); ?>">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="<?php print(base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')); ?>" />
<link rel="stylesheet" type="text/css" href="<?php print(base_url('assets/bower_components/datatables.net-bs/css/custom_button_export.css'))?>" />

<style>
    ul.wysihtml5-toolbar li a[title="Insert image"] { display: none; }
    ul.wysihtml5-toolbar li a[title="Insert link"] { display: none; }
    ul.wysihtml5-toolbar li a[title="Indent"] { display: none; }
    ul.wysihtml5-toolbar li a[title="Outdent"] { display: none; }
    ul.wysihtml5-toolbar li.dropdown { display: none; }
</style>
<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        	<div class="col-md-12">
            <!-- Horizontal Form -->
	          <div class="box box-primary">
	              <div class="box-header with-border">
	                <h3 class="box-title">Autocomplete</h3>
	                
	              </div>
	              <!-- /.box-header -->
	              
	              <!-- form start -->
	              <div class="box-body">
                    <table class="table table-striped jambo_table bulk_action" id="datatable">
                        <tr>
                            <th class="column-title" align="center">No</th>
                            <th class="column-title" align="center">Nama Kegiatan</th>
                            <th class="column-title" align="center">PPKOM</th>
                            <th class="column-title" align="center">PPTK</th>
                            <th class="column-title" align="center">Bendahara</th>
                            <th class="column-title" align="center">Jenis Pekerjaan</th>
                            <th class="column-title" align="center">Metode Pengadaan</th>
                            <th class="column-title" align="center">Anggaran</th>
                            <th class="column-title" align="center">Aksi</th>
                        </tr>
                        <?php 
                        $no=1;
                        if(!empty($aktivitas_kontrak)) {
                            foreach($aktivitas_kontrak as $item):
                                print('<tr>');
                                print('<td>'.$no.'</td>');
                                print('<td>'.$item->nama_aktivitas.'</td>');
                                print('<td>'.$item->ppkom.'</td>');
                                print('<td>'.$item->pptk.'</td>');
                                print('<td>'.$item->bendahara.'</td>');
                                print('<td>'.$item->jenis_pekerjaan.'</td>');
                                print('<td>'.$item->jenis_pengadaan.'</td>');
                                print('<td>'.$item->anggaran_awal.'</td>');
                                print('</tr>');
                                $no++;
                            endforeach;
                        }
                        ?>
                    </table>
	              </div>
	          	</div>
	      	</div>

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>

<script>
  $(function () {
    $('#datatable').DataTable();
  });
</script>
<?php $this->load->view('template/footer');?>
<!-- https://stackoverflow.com/questions/18328216/wysihtml5-setting-a-value-wont-work-because-sandbox-iframe-isnt-loaded-yet -->

