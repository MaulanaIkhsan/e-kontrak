<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Program extends CI_Controller {

	// ----- Program -----
	// id
	// kode_program
	// nama_program
	// tahun
	// uniq_code
	// keterangan
	// kode_urusan
	// ppkom
	// created_at
	// updated_at

	public function __construct()
	{
		parent::__construct();

		$sess_data = $this->session->userdata('session_data');
        if($sess_data===NULL) {
            $this->session->set_flashdata('warning', '<div class="alert alert-warning fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a> 
                Harap login terlebih dahulu untuk mengakses sistem</div>');
			redirect('auth');
		}
		
		//Load Dependencies
		$this->load->model('Program_model', 'program');
	}

	// List all your items
	public function index()
	{
		$this->load->view('program/show');
	}

	// Add a new item
	public function add()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('kode_program', 'Kode Program','required');
		$this->form_validation->set_rules('nama_program', 'Nama Program','required');
		$this->form_validation->set_rules('urusan', 'Urusan','required');
		$this->form_validation->set_rules('ppkom', 'PPKOM','trim');
		$this->form_validation->set_rules('keterangan', 'Keterangan','trim');

		if($this->form_validation->run() == FALSE) {
			$this->load->model('Urusan_model', 'urusan');
			$this->load->model('Users_model', 'user');

			$value['urusan'] = $this->urusan->show();
			$value['users'] = $this->user->get_user('ppkom');

			$this->load->view('program/add', $value);
		}
		else {
			if($this->input->post('simpan')!==NULL){
				$data['kode_program'] = $this->input->post('kode_program');
				$data['nama_program'] = $this->input->post('nama_program');
				$data['keterangan'] = $this->input->post('keterangan');
				$data['kode_urusan'] = $this->input->post('kode_urusan');
				$data['ppkom'] = $this->input->post('ppkom');

				$add = $this->program->insert($data);
				if ($add==='ok') {
					$this->session->set_flashdata('success', '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Data program kerja '.$data['nama_program'].' berhasil tersimpan</div>');
					redirect('program');
				}
				else {
					$this->session->set_flashdata('success', '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Data program '.$data['nama_program'].' gagal tersimpan. Harap cek kembali</div>');
					redirect('program');
				}

			}
			else {
				$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Harap masukkan data program kerja melalui tombol <strong>Tambah Baru</strong> !</div>');
				redirect('program');
			}
		}
		
		
	}

	//Update one item
	public function update($program_id)
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('kode_program', 'Kode Program','required');
		$this->form_validation->set_rules('nama_program', 'Nama Program','required');
		$this->form_validation->set_rules('urusan', 'Urusan','trim');
		$this->form_validation->set_rules('ppkom', 'PPKOM','trim');
		$this->form_validation->set_rules('keterangan', 'Keterangan','trim');

		if($this->form_validation->run() == FALSE) {
			$this->load->model('Urusan_model', 'urusan');
			$this->load->model('Users_model', 'user');

			$value['urusan'] = $this->urusan->show();
			$value['users'] = $this->user->get_user('ppkom');
			$value['program'] = $this->program->show_update($program_id);

			$this->load->view('program/update', $value);
		}
		else {
			if($this->input->post('simpan')!==NULL){
				$data['kode_program'] = $this->input->post('kode_program');
				$data['nama_program'] = $this->input->post('nama_program');
				$data['keterangan'] = $this->input->post('keterangan');
				// $data['kode_urusan'] = $this->input->post('kode_urusan');
				$data['ppkom'] = $this->input->post('ppkom');

				$key['id'] = $program_id;

				$edit = $this->program->update($data, $key);

				if ($edit==='ok') {
					$this->session->set_flashdata('success', '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Data program kerja berhasil diubah</div>');
					redirect('program/detail/'.$program_id);
				}
				else {
					$this->session->set_flashdata('success', '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Data program kerja gagal diubah. Harap cek kembali</div>');
					redirect('program/detail/'.$program_id);
				}
			}
			else {
				$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Harap ubah data program kerja melalui tombol <strong>Ubah</strong> !</div>');
				redirect('program/detail/'.$program_id);
			}
		}
	}

	//Delete one item
	public function delete($program_id)
	{
		if($this->input->post('delete')!==NULL){
			$key['id'] = $program_id;

			$delete = $this->program->delete($key);
			if ($delete=== 'ok') {
				$this->session->set_flashdata('success', '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Data program kerja  berhasil terhapus </div>');
				redirect('program');
			}
			else{
				$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Data program kerja gagal dihapus. Harap cek kembali </div>');
				redirect('program');
			}
		}
		else {
			$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Harap lakukan hapus data program kerja melalui tombol <strong>Hapus</strong> !</div>');
			redirect('program/detail/'.$program_id);
		}
	}

	public function detail($program_id)
	{
		$data['detail'] = $this->program->detail($program_id);
		$this->load->view('program/detail', $data);
	}

	public function get_data()
	{
		$list = $this->program->get_datatables();
		
		// print_r($list);
		// print('<br/>');
		// print_r($this->db->last_query());
		// die;

		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->nama_program;
			$row[] = $field->tahun;
			$row[] = $field->keterangan;
			$row[] = "<a href=".base_url('program/detail/'.$field->program_id)." class='btn btn-primary'>Detail</a>";
			$row[] = "";

			$data[] = $row;
		}

		// print_r($data);
		// print('<br/>');
		// print(count($data).'<br/>');
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->program->count_all(),
			"recordsFiltered" => $this->program->count_filtered(),
			"data" => $data,
		);
		// output dalam format JSON
		echo json_encode($output);
	}

	public function download_excel($program_id)
	{
		$data['detail'] = $this->program->detail($program_id);
		$data['serapan'] = $this->program->resume_pekerjaan_by_program($program_id);
		$this->load->view('program/resume_excel', $data);
	}

	
}

/* End of file Program.php */
/* Location: ./application/controllers/Program.php */
