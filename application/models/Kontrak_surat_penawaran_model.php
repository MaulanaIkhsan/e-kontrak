<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kontrak_surat_penawaran_model extends CI_Model {

    /*----- kontrak_surat_penawaran -----*/
    // id
    // kontrak_pekerjaan_id
    // jenis_surat_id
    // kontrak_pihak_ketiga_id
    // tempat_rapat_id
    // tgl_surat
    // no_surat
    // lampiran
    // used_lampiran
    // created_at
    // updated_at

    var $column_order = array(null, 'kontrak_surat_penawaran.id', 
                'jenis_surat.nama',
                'jenis_surat.kode',
                'kontrak_surat_penawaran.tgl_surat', 
                'kontrak_surat_penawaran.no_surat',
                'kontrak_surat_penawaran.lampiran'); 

    var $column_search = array('kontrak_surat_penawaran.id', 
                'jenis_surat.nama',
                'jenis_surat.kode',
                'kontrak_surat_penawaran.tgl_surat', 
                'kontrak_surat_penawaran.no_surat',
                'kontrak_surat_penawaran.lampiran');

    // default order 
    var $order = array('kontrak_surat_penawaran.id' => 'asc');

    public $used_lampiran;

    public function fetch_data($id=NULL, $kontrak_pekerjaan_id=NULL, $kode_jenis_surat=NULL)
    {
        $sql = $this->db->select('kontrak_surat_penawaran.id as id, 
                                kontrak_surat_penawaran.kontrak_pekerjaan_id as kontrak_pekerjaan_id,
                                kontrak_surat_penawaran.kontrak_pihak_ketiga_id as kontrak_pihak_ketiga_id,
                                kontrak_pekerjaan.id as kontrak_pekerjaan_id,
                                aktivitas.nama_aktivitas as aktivitas_nama,
                                pihak_ketiga.id as pihak_ketiga_id,
                                pihak_ketiga.nama_perusahaan as pihak_ketiga_nama,
                                jenis_surat.id as jenis_surat_id, 
                                jenis_surat.nama as jenis_surat_nama,
                                jenis_surat.kode as jenis_surat_kode,
                                tempat_rapat.id as tempat_rapat_id,
                                tempat_rapat.nama as tempat_rapat_nama,
                                tempat_rapat.alamat as tempat_rapat_alamat,
                                kontrak_surat_penawaran.tgl_surat as tgl_surat, 
                                kontrak_surat_penawaran.no_surat as no_surat,
                                kontrak_surat_penawaran.acara as acara,
                                kontrak_surat_penawaran.lampiran as lampiran,
                                kontrak_surat_penawaran.used_lampiran as used_lampiran')
                        ->from('kontrak_surat_penawaran')
                        ->join('jenis_surat', 'kontrak_surat_penawaran.jenis_surat_id=jenis_surat.id', 'left')
                        ->join('kontrak_pekerjaan', 'kontrak_surat_penawaran.kontrak_pekerjaan_id=kontrak_pekerjaan.id', 'left')
                        ->join('kontrak_pihak_ketiga', 'kontrak_surat_penawaran.kontrak_pihak_ketiga_id=kontrak_pihak_ketiga.id', 'left')
                        ->join('pihak_ketiga', 'kontrak_pihak_ketiga.pihak_ketiga_id=pihak_ketiga.id', 'left')
                        ->join('tempat_rapat', 'kontrak_surat_penawaran.tempat_rapat_id=tempat_rapat.id', 'left')
                        ->join('aktivitas', 'kontrak_pekerjaan.aktivitas_id=aktivitas.id', 'left');

        if($id!=NULL){
            $sql->where('kontrak_surat_penawaran.id', $id);
        }

        if($kontrak_pekerjaan_id!=NULL and $kode_jenis_surat==NULL) {
            $sql->where('kontrak_surat_penawaran.kontrak_pekerjaan_id', $kontrak_pekerjaan_id);
        }

        if(($kode_jenis_surat!=NULL or $id!=NULL) and $kontrak_pekerjaan_id!=NULL){
            $sql->where('jenis_surat.kode', $kode_jenis_surat)
                ->where('kontrak_surat_penawaran.kontrak_pekerjaan_id', $kontrak_pekerjaan_id);
        }

        if($kode_jenis_surat!=NULL) {
            $sql->where('jenis_surat.kode', $kode_jenis_surat);
        }
        
        if(!empty($this->used_lampiran)) {
            $sql->where('used_lampiran', 'y');
        }

        if($id!=NULL) {
            return $sql->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }

    /*====== Serverside Datatable ======*/
	private function _get_datatables_query($kontrak_pekerjaan_id)
	{
        $this->db->select('kontrak_surat_penawaran.id as id, 
                        jenis_surat.nama as jenis_surat_nama,
                        jenis_surat.kode as jenis_surat_kode,
                        kontrak_surat_penawaran.tgl_surat as tgl_surat, 
                        kontrak_surat_penawaran.no_surat as no_surat,
                        kontrak_surat_penawaran.lampiran as lampiran');

        $this->db->from('kontrak_surat_penawaran');
        $this->db->join('jenis_surat', 'kontrak_surat_penawaran.jenis_surat_id=jenis_surat.id', 'left');
        $this->db->join('kontrak_pekerjaan', 'kontrak_surat_penawaran.kontrak_pekerjaan_id=kontrak_pekerjaan.id', 'left');
        
        $this->db->where('kontrak_surat_penawaran.kontrak_pekerjaan_id', $kontrak_pekerjaan_id);
        
        $i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($kontrak_pekerjaan_id)
	{
		$this->_get_datatables_query($kontrak_pekerjaan_id);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($kontrak_pekerjaan_id)
	{
		$this->_get_datatables_query($kontrak_pekerjaan_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($kontrak_pekerjaan_id)
	{
		$this->db->select('kontrak_surat_penawaran.id as id, 
                        jenis_surat.nama as jenis_surat_nama,
                        jenis_surat.kode as jenis_surat_kode,
                        kontrak_surat_penawaran.tgl_surat as tgl_surat, 
                        kontrak_surat_penawaran.no_surat as no_surat,
                        kontrak_surat_penawaran.lampiran as lampiran');

        $this->db->from('kontrak_surat_penawaran');
        $this->db->join('jenis_surat', 'kontrak_surat_penawaran.jenis_surat_id=jenis_surat.id', 'left');
        $this->db->join('kontrak_pekerjaan', 'kontrak_surat_penawaran.kontrak_pekerjaan_id=kontrak_pekerjaan.id', 'left');
        
        $this->db->where('kontrak_surat_penawaran.kontrak_pekerjaan_id', $kontrak_pekerjaan_id);

		return $this->db->count_all_results();
	}
	/*====== /Serverside Datatable ======*/

}

/* End of file Kontrak_surat_penawaran_model.php */
