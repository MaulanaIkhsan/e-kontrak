<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>

<HEAD>
	<META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
	<TITLE><?php print($filename); ?></TITLE>
	<META NAME="GENERATOR" CONTENT="LibreOffice 4.1.6.2 (Linux)">
	<META NAME="AUTHOR" CONTENT="DPU">
	<META NAME="CREATED" CONTENT="Tahun_Anggaran0817;20400000000000">
	<META NAME="CHANGEDBY" CONTENT="isan">
	<META NAME="CHANGED" CONTENT="20190320;163019000000000">
	<META NAME="KSOProductBuildVer" CONTENT="1033-10.1.0.6757">
	<STYLE TYPE="text/css">
		 @page {
			size: 8.47in 13.98in;
			margin-right: 0.88in;
			margin-top: 0.59in;
			margin-bottom: 0.69in
		}

		P {
			margin-bottom: 0.08in;
			direction: ltr;
			color: #000000
		}

		P.western {
			font-family: "Times New Roman", serif;
			font-size: 12pt;
			so-language: en-US
		}

		P.cjk {
			font-family: "Times New Roman", serif;
			font-size: 12pt;
			so-language: en-US
		}

		P.ctl {
			font-family: "Times New Roman", serif;
			font-size: 12pt;
			so-language: ar-SA
		}

		H1 {
			margin-left: 3.5in;
			margin-top: 0in;
			margin-bottom: 0in;
			direction: ltr;
			color: #000000;
			text-align: justify;
			text-decoration: underline
		}

		H1.western {
			font-family: "Times New Roman", serif;
			font-size: 12pt;
			so-language: en-US
		}

		H1.cjk {
			font-family: "Times New Roman", serif;
			font-size: 12pt;
			so-language: en-US
		}

		H1.ctl {
			font-family: "Times New Roman", serif;
			font-size: 12pt;
			so-language: ar-SA
		}

		
	</STYLE>
</HEAD>

<BODY LANG="en-US" TEXT="#000000" DIR="LTR" >

    <!-- Undangan_Pengadaan (UPengadaan) -->
    <?php
    $kontrak_surat      = $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'UPengadaan');
    $kontrak_surat      = $kontrak_surat[0];
    $jadwal_pengadaan   = $controller->get_jadwal_kegiatan_kontrak($kontrak_surat->id);    
    ?>

<table>
        <tr>
            <td width="300" valign="top">
                <table cellpadding="2">
                    <tr>
                        <td class="text">Nomor</td>
                        <td class="text">:</td>
                        <td class="text"><?php print($kontrak_surat->no_surat);?></td>
                    </tr>
                    <tr>
                        <td class="text">Lampiran</td>
                        <td class="text">:</td>
                        <td class="text">-</td>
                    </tr>
                    <tr>
                        <td class="text">Perihal</td>
                        <td class="text">:</td>
                        <td class="text"><strong>Undangan</strong></td>
                    </tr>
                </table>
            </td>
            <td width="400"></td>
            <td width="300" align="left" class="text" valign="top">
                Semarang, <?php print(tgl_indo($kontrak_surat->tgl_surat));?><br/>
                Yth. <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama);?><br/>
                <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_alamat);?><br/>
                di - <br/>
                Tempat
            </td>
        </tr>
    </table>
    
    
    <ol type="1">
        <li>
            Dasar
            <ol type="a">
                <li>Peraturan Presiden Nomor 4 tahun 2015 tentang Perubahan Keempat atas dasar Peraturan Presiden Nomor
54 Tahun 2010 Tentang Pengadaan barang/ Jasa Pemerintah.</li>
                <li>Peraturan Daerah Kota Semarang Nomor 16 Tahun 2016 tentang Anggaran Pendapatan dan Belanja Daerah
Kota Semarang Tahun Anggaran Tahun_Anggaran</li>
                <li>Peraturan Walikota Semarang Nomor 129 Tahun 2016 tentang Penjabaran Anggaran Pendapatan dan
Belanja Daerah Kota Semarang Tahun Anggaran Tahun_Anggaran</li>
            </ol>
        </li>
        <li>
            Dengan ini kami mengharap kesediaan Saudara untuk ikut serta dalam Pengadaan Langsung untuk : <br/>
            <br/><table>
                <tr>
                    <td width="250">Program</td>
                    <td>:</td>
                    <td><?php print($kontrak_pekerjaan->program_nama);?></td>
                </tr>
                <tr>
                    <td width="250">Pekerjaaan</td>
                    <td>:</td>
                    <td><?php print($kontrak_pekerjaan->pekerjaan_nama);?></td>
                </tr>
                <tr>
                    <td width="250">Kegiatan</td>
                    <td>:</td>
                    <td><?php print($kontrak_pekerjaan->aktivitas_nama);?></td>
                </tr>
                <tr>
                    <td width="250">Sumber Dana</td>
                    <td>:</td>
                    <td><?php print($kontrak_pekerjaan->aktivitas_sumber_dana);?></td>
                </tr>
                <tr>
                    <td width="250">Kualifikasi</td>
                    <td>:</td>
                    <td>SIUP Kecil</td>
                </tr>
                <tr>
                    <td width="250">HPS</td>
                    <td>:</td>
                    <td><?php print('Rp '.format_money($kontrak_pekerjaan->hps).',-');?></td>
                </tr>
                <tr>
                    <td width="250">Tempat dan Alamat</td>
                    <td>:</td>
                    <td>
                        <?php print($kontrak_surat->tempat_rapat_nama);?><br/>
                        <?php print($kontrak_surat->tempat_rapat_alamat);?>
                    </td>
                </tr>
            </table><br/>
            dengan jadwal sebagai berikut:<br/><br/>
            <table CELLPADDING="7" cellspacing="0">
                <tr>
                    <th width="50" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">NO</th>
                    <th width="550" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">JADWAL KEGIATAN</th>
                    <th width="200" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">TANGGAL</th>
                    <th width="200" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">WAKTU</th>
                </tr>
                <?php 
                if(!empty($jadwal_pengadaan)) {
                    print_r($jadwal_pengadaan);
                    $no=1;
                    foreach($jadwal_pengadaan as $item):
                        print('<tr>');
                        print('<td STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in" align="center">'.$no.'</td>');
                        print('<td STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">'.$item->jenis_kegiatan_nama.'</td>');
                        print('<td STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in" align="center">'.tgl_indo($item->tgl).'</td>');
                        print('<td STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in" align="center">'.$item->jam.'</td>');
                        print('</tr>');
                    endforeach;
                } else {
                    print('<tr>');
                    print('<td colspan="4" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in" align="center">-- Kosong --</td>');
                    print('</tr>');
                }
                ?>
                
            </table>
            <br/>
        </li>
        <li>Apabila Saudara butuh keterangan dan penjelasan lebih lanjut, dapat menghubungi Pejabat Pengadaan sesuai
alamat tersebut di atas sampai dengan batas akhir pemasukan Dokumen Penawaran.</li>
    </ol>
    <br/>
    Demikian disampaikan untuk diketahui.
    <p></p>
    <table cellpadding="2">
        <tr>
            <td width="600" class="text">&nbsp;</td>
            <td align="center" width="400" class="text">
                <?php print($pejabat_pengadaan->jenis_pejabat_pengadaan_nama); ?> <?php print($pejabat_pengadaan->seri_pejabat_pengadaan_nama); ?><br/>
                Pada Dinas Pekerjaan Umum Kota Semarang<br/>
                Tahun Anggaran <?php print(date("Y"));?><br/>
                <br/><br/><br/><br/><br/>
                <u><?php print($pejabat_pengadaan->pegawai_nama); ?></u><br/>
                NIP. <?php print($pejabat_pengadaan->pegawai_nip); ?>
            </td>
        </tr>
    </table>
    <!-- /Undangan_Pengadaan (UPengadaan)-->

    <p style="page-break-before: always"></p>

    <!-- Pakta Integritas (PI) -->
    <?php
    $kontrak_surat      = $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'PI');
    $kontrak_surat      = $kontrak_surat[0];
    ?>
    <p CLasS=western ALIGN=CENTER styLe="margin-bottom: 0in">
       <B>PAKTA INTEGRITAS</B>
    </P>
    <p CLASS="western" ALIGN=CENTER STYlE="margin-bottom: 0in">
    
    </p>
    <p claSS="western" ALIGN=JUSTIFY STylE="margin-bottom: 0in">
        Saya yang bertanda tangan dibawah ini, dalam rangka Pengadaan Barang/Jasa
        <?php print(ucwords(strtolower($kontrak_pekerjaan->program_nama)));?> <?php print(ucwords(strtolower($kontrak_pekerjaan->pekerjaan_nama)));?> 
        Pekerjaan <?php print(ucwords(strtolower($kontrak_pekerjaan->aktivitas_nama)));?>, 
        pada Dinas Pekerjaan Umum Kota Semarang, dengan ini menyatakan bahwa :
    </P>
    <p cLaSS=western ALIGN=JUSTIFY stYLE="margin-bottom: 0in">
    </P>
    
    <Ol>
        <lI>Tidak akan melakukanpraktek KKN.</li>             
        <LI>
        Akan melaporkan kepada
        APIP (Aparat Pengawas Intern Pemerintah) yang bersangkutan dan atau
        LKPP apabila mengetahui ada indikasi KKN di dalam proses pengadaan
        ini.</li>
        <Li>                
        Akan mengikuti proses
        pengadaan secara bersih, transparan, dan professional untuk
        memberikan hasil kerja terbaik sesuai ketentuan peraturan
        perundangan-undangan.</li>
        <Li>
        Apabila melanggar
        hal-hal yang telah saya nyatakan dalam PAKTA INTEGRITAS ini, saya
        bersedia menerima sanksi administratif, menerima saknsi pencantuman
        dalam Daftar Hitam, digugat secara perdata dan / atau dilaporkan
        secara pidana.</li>         
    </OL>

    <p ClaSs="western" ALIGN=JUSTIFY STylE="margin-bottom: 0in">
        <br>
    </p>
    <P ClaSS=western ALIGN=RIGHT StylE="margin-bottom: 0in">
        Semarang, <?php print(tgl_indo($kontrak_surat->tgl_surat)); ?>
    </p>
    <P clAsS=western ALIGN=RIGHT StYlE="margin-bottom: 0in">
        <br>
    </P>
    <p CLASS=western ALIGN=RIGHT stYlE="margin-bottom: 0in">
        <BR>
    </p>
    <table cellspacing="2" cellpadding="10">
        <tr>
            <td valign="top">1</td>
            <td valign="top">
                A.n. Kepala Dinas Pekerjaan Umum<br/>
                Pejabat Pembuat Komitmen
            </td>
            <td valign="top">.........................................</td>
            <td valign="top"><?php print((!empty($pejabat_pembuat_komitmen->pegawai_nama))?$pejabat_pembuat_komitmen->pegawai_nama:''); ?></td>
        <tr>
        <tr>
            <td valign="top">2</td>
            <td valign="top">
            Pejabat Pengadaan <?php print($kontrak_pekerjaan->seri_pejabat_pengadaan_nama); ?><br/>
            <?php print($kontrak_pekerjaan->jenis_pejabat_pengadaan_nama); ?>
            </td>
            <td valign="top">.........................................</td>
            <td valign="top">
                <?php print($kontrak_pekerjaan->pejabat_pengadaan_nama); ?>
            </td>
        <tr>
    </table>
    <!-- /Pakta Integritas (PI) -->

    <p style="page-break-before: always"></p>

    <!-- Berita Acara Buka Penawaran (BABP) -->
    <?php
    $kontrak_surat      = $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'BABP');
    $kontrak_surat      = $kontrak_surat[0];
    $surat_undangan     = $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'UPengadaan');
    
    $dayname    = dayname(date('l', strtotime($kontrak_surat->tgl_surat)));
    $tglindo    = tgl_indo($kontrak_surat->tgl_surat);
    $split_tgl  = explode(' ', $tglindo);
    ?>

<p ClaSs=western ALIGN=CENTER StyLe="margin-bottom: 0in; page-break-before: none">
        <b>BERITA ACARA</B><br/>
        <U><b>PEMASUKAN / PEMBUKAAN DOKUMEN PENAWARAN</b></u><br/>
        <FoNt sTYle="font-size: 11pt">Nomor : <?php print($kontrak_surat->no_surat); ?></FONT>
    </P>
    
    <P  ALIGN="justify" StylE="margin-bottom: 0in">
    Pada hari ini <?php print($dayname); ?> tanggal <?php print(terbilang($split_tgl[0])); ?>, bulan
    <?php print(strtolower($split_tgl[1])); ?>, tahun <?php print(terbilang($split_tgl[2])); ?> (<?php print($tglindo); ?>) , Pejabat Pengadaan pada Dinas Pekerjaan Umum Kota Semarang Tahun Anggaran <?php print(date("Y")); ?> berdasarkan Keputusan Kepala Dinas Pekerjaan Umum Kota Semarang Selaku Pengguna Anggaran Nomor <?php print($pejabat_pengadaan->sk_no); ?> tanggal <?php print(tgl_indo($pejabat_pengadaan->sk_tgl_penetapan)); ?> tentang Penunjukan Pejabat Pengadaan Pada Dinas Pekerjaan Umum kota Semarang Tahun Anggaran <?php print($pejabat_pengadaan->sk_tahun); ?>, telah mengadakan Rapat Pemasukan / Pembukaan Dokumen Penawaran dalam rangka Pengadaan Langsung:
    </p>
    <p></p>
    <table>
        <tr>
            <td class="text">Program</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->program_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Kegiatan</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->pekerjaan_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Pekerjaan</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->aktivitas_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Sumber Dana</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->aktivitas_sumber_dana); ?></td>
        </tr>
    </table>

    <P LaNg="pt-BR" CLaSs=western ALIGN=JUSTIFY STylE="margin-bottom: 0in">
    </p>

    <ol type=I>
        <li>
            <strong>PESERTA RAPAT</strong><br/>
            <?php print($pejabat_pengadaan->jenis_pejabat_pengadaan_nama); ?> <?php print($pejabat_pengadaan->seri_pejabat_pengadaan_nama); ?><br/>
            <?php print($kontrak_pekerjaan->pejabat_pengadaan_nama);?><br/>
            <br/>
            Wakil Penyedia Jasa : <br/>
            <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama);?><br/>
            beralamat <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_alamat);?><br/>
        </li>
        <br/>
        <li>
            <strong>HASIL PEMBUKAAN PENAWARAN</strong>
            <ul type="1">
                <li>Perusahaan yang hadir dan memasukkan penawaran Sesuai dengan Surat Undangan Nomor
                <?php print((!empty($surat_undangan[0]->no_surat))?$surat_undangan[0]->no_surat:''); ?>, <?php print((!empty($surat_undangan[0]->tgl_surat))?tgl_indo($surat_undangan[0]->tgl_surat):''); ?>.</li>
                <li>Nilai Penawaran sebesar <?php print('Rp '.format_money($kontrak_pihak_ketiga[0]->nilai_kontrak).',-'); ?></li>
                <li>Jangka Waktu Pelaksanaan <?php print($kontrak_pekerjaan->durasi_kontrak); ?> hari kalender</li>
                <li>Setelah diadakan pembukaan, ternyata penawaran tersebut memenuhi (lengkap)</li>
                <li>Berdasarkan penelitian tersebut diatas, Pembukaan Penawaran ini dinyatakan <strong>Sah</strong></li>
                <li>Hasil Pembukaan Penawaran terlampir.</li>
            </ul>
        </li>
    </ol>
    <p></p>
    Demikian Berita Acara Pemasukan & Pembukaan Dokumen Penawaran ini dibuat dengan sebenarnya
untuk dapat dipergunakan sebagaimana mestinya.
    <p></p>
    <p></p>
    <table>
        <tr>
            <td width="300" align="center" style="font-size:14pt">
                Setuju:<br/>
                <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama);?><br/>
                <br/><br/><br/><br/><br/><br/><br/><br/><br/>
                <u><?php print($direktur_perusahaan);?></u><br/>
                Direktur
            </td>
            <td width="400"></td>
            <td width="300" align="center" style="font-size:14pt">
                <?php print($pejabat_pengadaan->jenis_pejabat_pengadaan_nama); ?> <?php print($pejabat_pengadaan->seri_pejabat_pengadaan_nama); ?><br/>
                Pada Dinas Pekerjaan Umum Kota Semarang<br/>
                Tahun Anggaran <?php print(date("Y"));?><br/>
                <br/><br/><br/><br/><br/>
                <u><?php print($pejabat_pengadaan->pegawai_nama); ?></u><br/>
                NIP. <?php print($pejabat_pengadaan->pegawai_nip); ?>
            </td>
        </tr>
    </table>
    <!-- /Berita Acara Buka Penawaran (BABP) -->
    
    <p style="page-break-before: always"></p>

    <!-- Daftar Hadir Nego -->
    <?php
    $kontrak_surat      = $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'DHN');
    $kontrak_surat      = $kontrak_surat[0];
    ?>

<P cLASS=western ALIGN=JUSTIFY StYlE="margin-bottom: 0in;">
        <foNt SIZE=2 sTyle="font-size: 11pt"><B>DAFTAR HADIR</b></FONt>
    </P>
    <P class=western ALIGN=CENTER stYle="margin-bottom: 0in">
        <bR>
    </p>
    <table cellpadding="3">
        <tr>
            <td>Program</td>
            <td>:</td>
            <td><?php print($kontrak_pekerjaan->program_nama); ?></td>
        </tr>
        <tr>
            <td>Kegiatan</td>
            <td>:</td>
            <td><?php print($kontrak_pekerjaan->pekerjaan_nama); ?></td>
        </tr>
        <tr>
            <td>Pekerjaan</td>
            <td>:</td>
            <td><?php print($kontrak_pekerjaan->aktivitas_nama); ?></td>
        </tr>
        <tr>
            <td>Tanggal</td>
            <td>:</td>
            <td><?php print(tgl_indo($kontrak_surat->tgl_surat)); ?></td>
        </tr>
        <tr>
            <td>Tempat</td>
            <td>:</td>
            <td><?php print($kontrak_surat->tempat_rapat_nama); ?></td>
        </tr>
        <tr>
            <td>Acara</td>
            <td>:</td>
            <td><?php print($kontrak_surat->acara); ?></td>
        </tr>
    </table>

    
    <P Lang="en-US" CLasS="western" ALIGN=JUSTIFY Style="margin-bottom: 0in">
        <FONT SIZE=2 STylE="font-size: 11pt"><B>PEJABAT PENGADAAN</b></foNt>
    </p>
    <P Lang="en-US" CLasS="western" ALIGN=JUSTIFY Style="margin-bottom: 0in">
        <br/>
    </p>
    

    <CeNTer>
        <tablE WIDTH=603 CELLPADDING=7 CELLSPACING=0>
            <COL WIDTH=23>
                <cOL WIDTH=244>
                    <CoL WIDTH=140>
                        <COl WIDTH=139>
                            <tR VALIGN=TOP>
                                <tD align="center" WIDTH=23 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P laNG="en-US" claSS=western ALIGN=CENTER>
                                        <fONt SIZE=2 StYlE="font-size: 11pt">No</Font>
                                    </P>
                                </td>
                                <tD align="center" WIDTH=244 StYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LAnG=en-US claSS="western" ALIGN=CENTER>
                                        <FONT SIZE=2 StYlE="font-size: 11pt">Nama</fonT>
                                    </P>
                                </TD>
                                <tD align="center" WIDTH=140 STylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LaNg="en-US" CLAss="western" ALIGN=CENTER>
                                        <FONt SIZE=2 STYLe="font-size: 11pt">Jabatan</FOnT>
                                    </P>
                                </Td>
                                <tD WIDTH=139 sTylE="border: 1px solid #000000; padding: 0in 0.08in" align="center">
                                    <p laNG="en-US" claSS="western" ALIGN=CENTER>
                                        <FonT SIZE=2 sTYlE="font-size: 11pt">Tanda
				Tangan</foNt>
                                    </p>
                                </tD>
                            </tR>
                            <tr VALIGN=TOP>
                                <TD WIDTH=23 StyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                    <P LaNG=en-US clAsS="western" valign="middle" ALIGN=CENTER sTYLE="margin-bottom: 0in">
                                        <foNt SIZE=2 stYLE="font-size: 11pt">1.</FONT>
                                    </P>
                                    
                                </TD>
                                <td WIDTH=244 valign="middle" StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                    <p LAng="en-US" cLasS="western" ALIGN=JUSTIFY><font SIZE=2 sTYlE="font-size: 11pt"><?php print($kontrak_pekerjaan->pejabat_pengadaan_nama); ?></Font>
                                    </P>
                                    
                                </td>
                                <TD WIDTH=140 valign="middle" StYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                    <p lAnG="en-US" CLAss="western" ALIGN=CENTER>
                                        <FOnt SIZE=2 sTYle="font-size: 11pt">Pejabat Pengadaan <?php print($kontrak_pekerjaan->seri_pejabat_pengadaan_nama); ?></Font>
                                    </P>
                                    
                                </Td>
                                <td WIDTH=139 valign="middle" STyLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p LANg=en-US ClASS="western" ALIGN=JUSTIFY stYLe="margin-bottom: 0in">
                                        <br/>
                                        <br/>
                                        <br/>
                                        <br/>
                                    </p>
                                </TD>
                            </Tr>
        </tAbLe>
    </cEntER>
    <p lANG="en-US" cLASs="western" ALIGN=JUSTIFY StylE="margin-bottom: 0in">
        <Br>
    </P>
    <P lANG=en-US cLaSS=western ALIGN=JUSTIFY sTyLe="margin-bottom: 0in">
        <FONT SIZE=2 StYlE="font-size: 11pt"><b>PIHAK KETIGA</B></FONt>
    </P>
    <p lANG=en-US CLAsS=western ALIGN=JUSTIFY sTylE="margin-bottom: 0in">
        <BR>
    </P>
    <ceNtEr>
        <TABLe WIDTH=625 CELLPADDING=7 CELLSPACING=0>
            <CoL WIDTH=22>
                <col WIDTH=230>
                    <col WIDTH=175>
                        <CoL WIDTH=140>
                            <tr VALIGN=TOP>
                                <td WIDTH=22 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LanG=en-US CLASS="western" ALIGN=CENTER>
                                        <fOnT SIZE=2 stYle="font-size: 11pt">No
				</FOnt>
                                    </P>
                                </td>
                                <Td align="center" WIDTH=230 StyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LAnG="en-US" ClasS="western" ALIGN=CENTER>
                                        <foNt SIZE=2 sTYle="font-size: 11pt">Nama Perusahaan</Font>
                                    </p>
                                </TD>
                                <TD align="center" WIDTH=175 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LAnG="en-US" ClAsS=western ALIGN=CENTER>
                                        <FONt SIZE=2 stYlE="font-size: 11pt">Nama Peserta</foNT>
                                    </p>
                                </td>
                                <Td align="center" WIDTH=140 styLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p lanG="en-US" clAss=western ALIGN=CENTER>
                                        <foNT SIZE=2 stYle="font-size: 11pt">Tanda Tangan</Font>
                                    </p>
                                </tD>
                            </tr>
                            <tR VALIGN=TOP>
                                <td valign="middle" WIDTH=22 sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                    <P lANg="en-US" ClAss="western" ALIGN=CENTER sTYLE="margin-bottom: 0in">
                                        <FoNT SIZE=2 STylE="font-size: 11pt">2.</font>
                                    </p>
                                    
                                </tD>
                                <td valign="middle" WIDTH=230 StYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P lanG=en-US CLASS=western ALIGN=JUSTIFY StylE="margin-bottom: 0in">
                                       <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama); ?>
                                    </P>
                                </TD>
                                <tD valign="middle" WIDTH=175 sTyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                
                                    <P lanG=en-US ClaSs=western ALIGN=CENTER StYlE="margin-bottom: 0in"><?php print($direktur_perusahaan); ?></p>

                                </TD>
                                <Td valign="middle" WIDTH=140 sTYlE="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p LANG="en-US" cLASS=western ALIGN=CENTER stylE="margin-bottom: 0in">
                                        <br/>
                                        <br/>
                                        <br/>
                                        <br/>
                                    </P>
                                    
                                    
                                </td>
                            </Tr>
        </TAblE>
    </center>
    <!-- /Daftar Hadir Nego -->

    <p style="page-break-before: always"></p>

    <!-- Berita Acara Evaluasi, Negosiasi dan Klarifikasi -->
    <?php
    $kontrak_surat      = $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'BAENK');
    $kontrak_surat      = $kontrak_surat[0];

    $dayname    = dayname(date('l', strtotime($kontrak_surat->tgl_surat)));
    $tglindo    = tgl_indo($kontrak_surat->tgl_surat);
    $split_tgl  = explode(' ', $tglindo);
    ?>
    <P LAng=pt-BR CLaSs="western" ALIGN=CENTER STylE="margin-bottom: 0in;">
        <b>BERITA ACARA<br/>
        EVALUASI, KLARIFIKASI DAN NEGOSIASI<br/>
        ---------------------------------------------------------------------------------------------------------------------
        </b><br/>
        Nomor : <?php print($kontrak_surat->no_surat); ?>
    </p>
    <p ClasS=western ALIGN=JUSTIFY sTyle="margin-bottom: 0in">
        Pada hari ini <b><?php print($dayname); ?></b>, tanggal <b><?php print(terbilang($split_tgl[0])); ?></b> bulan <b><?php print(strtolower($split_tgl[1])); ?></b> tahun <b><?php print(terbilang($split_tgl[2])); ?> (<?php print($tglindo); ?>) </b> , Pejabat Pengadaan pada Dinas Pekerjaan Umum Kota Semarang Tahun Anggaran Tahun_Anggaran, yang dibentuk berdasarkan Keputusan Kepala Dinas Pekerjaan Umum Kota Semarang Selaku Pengguna Anggaran <?php print($pejabat_pengadaan->sk_no); ?> tanggal <?php print(tgl_indo($pejabat_pengadaan->sk_tgl_penetapan)); ?> tentang Penunjukan Pejabat Pengadaan Pada Dinas Pekerjaan Umum Kota Semarang Tahun Anggaran <?php print($pejabat_pengadaan->sk_tahun); ?>, telah melakukan rapat Evaluasi, Klarifikasi dan Negosiasi dalam rangka Pengadaan Langsung.
    </p>
    <br/>
    <table>
        <tr>
            <td class="text">Program</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->program_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Kegiatan</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->pekerjaan_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Pekerjaan</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->aktivitas_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Sumber Dana</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->aktivitas_sumber_dana); ?></td>
        </tr>
    </table>
    <br/>
    
    <p LaNg="pt-BR" ClaSs=western ALIGN=JUSTIFY style="margin-bottom: 0in">
        <strong>PESERTA RAPAT</strong><br/>
        <?php print($pejabat_pengadaan->jenis_pejabat_pengadaan_nama); ?> <?php print($pejabat_pengadaan->seri_pejabat_pengadaan_nama); ?><br/>
        <?php print($kontrak_pekerjaan->pejabat_pengadaan_nama);?><br/>
        <br/>
        Calon Penyedia Jasa : <br/>
        <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama);?><br/>
        beralamat <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_alamat);?><br/>

    </P>
    <BR>
    <ol type="A">
        <li>
            <strong>PENELITIAN DAN PENILAIAN ADMINISTRASI</strong>
            <TAblE WIDTH=634 CELLPADDING=7 CELLSPACING=0>
                <cOl WIDTH=23>
                    <cOl WIDTH=203>
                        <Col WIDTH=52>
                            <cOl WIDTH=80>
                                <cOL WIDTH=62>
                                    <COL WIDTH=127>
                                        <tR>
                                            <tD ROWSPAN=3 ALIGN=CENTER WIDTH=23 STYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p Class="western" ALIGN=CENTER>
                                                    <fOnT SIZE=2 StYLe="font-size: 11pt"><b>NO</b></FONt>
                                                </P>
                                            </td>
                                            <Td ROWSPAN=3 ALIGN=CENTER WIDTH=203 StyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p CLAss=western ALIGN=CENTER>
                                                    <fOnT SIZE=2 stylE="font-size: 11pt"><B>NAMA
                    DOKUMEN</b></FoNT>
                                                </p>
                                            </Td>
                                            <TD COLSPAN=2 WIDTH=147 ALIGN=CENTER STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P class=western><font SIZE=2 Style="font-size: 11pt"><b>KELENGKAPAN</b></fONT>
                                                </p>
                                            </Td>
                                            <Td ROWSPAN=2 ALIGN=CENTER WIDTH=62 StYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P claSS="western" ALIGN=CENTER><font SIZE=2 sTYle="font-size: 11pt"><b>Tdk
                    ada</b></FONt>
                                                </P>
                                            </Td>
                                            <tD ROWSPAN=2 ALIGN=CENTER WIDTH=127 stylE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <P ClASS="western" ALIGN=CENTER>
                                                    <FoNT SIZE=2 sTylE="font-size: 11pt"><B>KETERANGAN</b></FONt>
                                                </P>
                                            </tD>
                                        </TR>
                                        <tr>
                                            <Td COLSPAN=2 ALIGN=CENTER WIDTH=147 sTylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P CLass=western ALIGN=CENTER>
                                                    <FoNT SIZE=2 sTYLE="font-size: 11pt"><b>Ada
                    (+)</b></FoNT>
                                                </p>
                                            </Td>
                                        </Tr>
                                        <tR>
                                            <td WIDTH=52 ALIGN=CENTER stYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p clASs=western ALIGN=CENTER>
                                                    <Font SIZE=2 STYLE="font-size: 11pt"><b>Sesuai</B></fOnT>
                                                </p>
                                            </TD>
                                            <td WIDTH=80 ALIGN=CENTER STyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p claSs="western" ALIGN=CENTER>
                                                    <FONT SIZE=2 stYLe="font-size: 11pt"><b>Tdk
                    Sesuai</B></fONt>
                                                </P>
                                            </Td>
                                            
                                        </TR>
                                        <TR>
                                            <TD WIDTH=23 sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P CLass=western>
                                                    <Font SIZE=2 StyLe="font-size: 11pt">1</fonT>
                                                </p>
                                            </tD>
                                            <Td WIDTH=203 sTyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p cLASs="western">
                                                    <foNt SIZE=2 STYLE="font-size: 11pt">Fotocopy
                    Surat Undangan</foNT>
                                                </p>
                                            </TD>
                                            <tD align="center" WIDTH=52 VALIGN=TOP sTYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P ClasS=western ALIGN=CENTER>√</p>
                                            </Td>
                                            <Td WIDTH=80 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p cLass="western" ALIGN=CENTER>
                                                    <Br>
                                                </p>
                                            </tD>
                                            <tD WIDTH=62 sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P CLAsS=western ALIGN=CENTER>
                                                    <Br>
                                                </p>
                                            </tD>
                                            <td WIDTH=127 style="border: 1px solid #000000; padding: 0in 0.08in">
                                                <P CLaSS=western ALIGN=CENTER>
                                                    <br>
                                                </p>
                                            </Td>
                                        </Tr>
                                        <TR VALIGN=TOP>
                                            <td WIDTH=23 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p cLASs=western>
                                                    <FoNt SIZE=2 styLE="font-size: 11pt">2</font>
                                                </p>
                                            </tD>
                                            <td WIDTH=203 sTyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p cLASs=western>
                                                    <FOnT SIZE=2 sTYlE="font-size: 11pt">Surat Penawaran</foNt>
                                                </P>
                                            </Td>
                                            <TD align="center" WIDTH=52 StYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P CLASS="western" ALIGN=CENTER>√</P>
                                            </tD>
                                            <TD WIDTH=80 StyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p cLAss="western" ALIGN=JUSTIFY>
                                                    <BR>
                                                </P>
                                            </TD>
                                            <tD WIDTH=62 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p ClASS=western ALIGN=JUSTIFY>
                                                    <Br>
                                                </P>
                                            </Td>
                                            <td WIDTH=127 styLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <p cLASS="western" ALIGN=JUSTIFY>
                                                    <br>
                                                </p>
                                            </Td>
                                        </Tr>
                                        <TR VALIGN=TOP>
                                            <TD WIDTH=23 STyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P ClAsS=western>
                                                    <Font SIZE=2 stYlE="font-size: 11pt">3</FONt>
                                                </P>
                                            </TD>
                                            <TD WIDTH=203 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P CLass="western">
                                                    <foNT SIZE=2 STyLE="font-size: 11pt">Masa berlaku surat penawaran</fONt>
                                                </p>
                                            </td>
                                            <TD WIDTH=52 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in" align="center">
                                                <P CLasS=western ALIGN=CENTER>√</p>
                                            </TD>
                                            <td WIDTH=80 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p ClasS="western" ALIGN=JUSTIFY>
                                                    <Br>
                                                </p>
                                            </tD>
                                            <tD WIDTH=62 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p ClASS="western" ALIGN=JUSTIFY>
                                                    <Br>
                                                </P>
                                            </TD>
                                            <Td WIDTH=127 stYlE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <p cLASS=western ALIGN=JUSTIFY>
                                                    <bR>
                                                </P>
                                            </Td>
                                        </tr>
                                        <TR VALIGN=TOP>
                                            <Td WIDTH=23 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p clAsS="western" ALIGN=JUSTIFY>
                                                    <BR>
                                                </p>
                                            </tD>
                                            <TD COLSPAN=4 WIDTH=440 STYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p clASS=western ALIGN=JUSTIFY>
                                                    <FONT SIZE=2 sTYLe="font-size: 11pt">HASIL AKHIR</FOnT>
                                                </p>
                                            </TD>
                                            <tD WIDTH=127 StYLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <p ClAsS=western ALIGN=CENTER>
                                                    <foNT SIZE=2 styLe="font-size: 11pt">LULUS / </fONT>
                                                    <STrIke><foNt SIZE=2 stYle="font-size: 11pt">TDK LULUS</foNt></sTrIke>
                                                </P>
                                            </td>
                                        </TR>
            </TABlE><br/>
            Hasil Penelitian dan penilaian Dokumen Administrasi dinyatakan [ LULUS / <strike>TIDAK LULUS</strike> ] dengan demikian dapat dilanjutkan pada Penelitian dan Penilaian Dokumen Teknis
        </li>
        <br/>
        <li>
            <strong>PENELITIAN DAN PENILAIAN DOKUMEN TEKNIS</strong><br/>
            <tabLE WIDTH=634 CELLPADDING=7 CELLSPACING=0>
                <COL WIDTH=23>
                    <col WIDTH=203>
                        <COL WIDTH=52>
                            <Col WIDTH=80>
                                <coL WIDTH=62>
                                    <COl WIDTH=127>
                                        <tr>
                                            <TD ROWSPAN=3 WIDTH=23 sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p claSs=western ALIGN=CENTER>
                                                    <fONT SIZE=2 Style="font-size: 11pt"><B>NO</b></FOnt>
                                                </P>
                                            </TD>
                                            <td ROWSPAN=3 WIDTH=203 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P ClasS="western" ALIGN=CENTER>
                                                    <FOnT SIZE=2 style="font-size: 11pt"><B>NAMA
                    DOKUMEN</b></fOnt>
                                                </p>
                                            </Td>
                                            <tD COLSPAN=2 WIDTH=147 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p CLass=western ALIGN=CENTER>
                                                    <FONT SIZE=2 sTyle="font-size: 11pt"><b>KELENGKAPAN</b></fonT>
                                                </P>
                                            </Td>
                                            <td ROWSPAN=2 WIDTH=62 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P clAss="western" ALIGN=CENTER>
                                                    <fONt SIZE=2 sTYLe="font-size: 11pt"><b>Tdk
                    ada</b></foNt>
                                                </p>
                                            </td>
                                            <TD ROWSPAN=2 WIDTH=127 sTyLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <p ClasS="western" ALIGN=CENTER>
                                                    <fONT SIZE=2 sTYLe="font-size: 11pt"><B>KETERANGAN</b></fONT>
                                                </p>
                                            </td>
                                        </tr>
                                        <TR>
                                            <TD COLSPAN=2 WIDTH=147 StYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P claSS=western ALIGN=CENTER>
                                                    <FONT SIZE=2 StYLE="font-size: 11pt"><b>Ada
                    (+)</b></fonT>
                                                </P>
                                            </TD>
                                        </tr>
                                        <Tr>
                                            <Td WIDTH=52 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p cLaSs=western ALIGN=CENTER>
                                                    <FONt SIZE=2 styLE="font-size: 11pt"><b>Sesuai</B></fOnT>
                                                </p>
                                            </tD>
                                            <td WIDTH=80 StYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P ClASS="western" ALIGN=CENTER>
                                                    <FOnt SIZE=2 Style="font-size: 11pt"><b>Tdk
                    Sesuai</b></FONt>
                                                </P>
                                            </TD>
                                            <Td WIDTH=62 sTYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p ClASS="western" ALIGN=CENTER>
                                                    <Br>
                                                </P>
                                            </TD>
                                            <td WIDTH=127 STyle="border: 1px solid #000000; padding: 0in 0.08in">
                                                <P cLASS="western" ALIGN=CENTER>
                                                    <Br>
                                                </P>
                                            </tD>
                                        </Tr>
                                        <tR VALIGN=TOP>
                                            <TD WIDTH=23 StYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P clASs=western>
                                                    <FonT SIZE=2 STyLe="font-size: 11pt">1</font>
                                                </p>
                                            </td>
                                            <TD WIDTH=203 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P ClAsS=western>
                                                    <foNT SIZE=2 STyle="font-size: 11pt">Jadwal
                    Pelaksanaan</FoNt>
                                                </p>
                                            </TD>
                                            <tD WIDTH=52 align="center" STyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P claSS=western ALIGN=CENTER>√</P>
                                            </tD>
                                            <tD WIDTH=80 STylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p cLAss="western" ALIGN=JUSTIFY>
                                                    <br>
                                                </P>
                                            </tD>
                                            <td WIDTH=62 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P CLAss="western" ALIGN=JUSTIFY>
                                                    <BR>
                                                </p>
                                            </td>
                                            <Td WIDTH=127 stYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                <p Class="western" ALIGN=JUSTIFY>
                                                    <bR>
                                                </P>
                                            </td>
                                        </tr>
                                        <TR VALIGN=TOP>
                                            <td WIDTH=23 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P LAng="id-ID" ClasS=western><font SIZE=2 styLE="font-size: 11pt">2</fonT>
                                                </P>
                                            </td>
                                            <tD WIDTH=203 StYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p CLASs=western>
                                                    <Font SIZE=2 sTylE="font-size: 11pt">Daftar
                    Personil yang ditugaskan</fOnt>
                                                </P>
                                            </TD>
                                            <td WIDTH=52 align="center" Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p clASs="western" ALIGN=CENTER>√</P>
                                            </tD>
                                            <tD WIDTH=80 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p claSS="western" ALIGN=JUSTIFY>
                                                    <BR>
                                                </P>
                                            </td>
                                            <td WIDTH=62 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P cLAss="western" ALIGN=JUSTIFY>
                                                    <br>
                                                </p>
                                            </td>
                                            <td WIDTH=127 stylE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <P clAsS="western" ALIGN=JUSTIFY>
                                                    <Br>
                                                </p>
                                            </Td>
                                        </TR>
                                        <TR VALIGN=TOP>
                                            <TD WIDTH=23 STyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p CLasS=western>
                                                    <bR>
                                                </P>
                                            </TD>
                                            <tD COLSPAN=4 WIDTH=440 StyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P cLASs=western ALIGN=JUSTIFY>
                                                    <FONT SIZE=2 StylE="font-size: 11pt">HASIL
                    AKHIR</FOnt>
                                                </P>
                                            </Td>
                                            <Td WIDTH=127 STylE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <p cLasS=western ALIGN=CENTER>
                                                    <fOnt SIZE=2 styLE="font-size: 11pt">LULUS
                    / </Font>
                                                    <StRIke><font SIZE=2 sTylE="font-size: 11pt">TDK LULUS</FoNt></StRIkE>
                                                </p>
                                            </Td>
                                        </Tr>
            </tAble><br/>
            Hasil Penelitian dan penilaian Dokumen Teknis dinyatakan [ LULUS / <strike>TIDAK LULUS</strike> ] dengan demikiandapat dilanjutkan pada Penelitian dan Penilaian Dokumen Biaya
        </li>
        <p></p>
        <p></p>
        <li>
            <strong>PENELITIAN DAN PENILAIAN DOKUMEN BIAYA</strong><br/>
            <Table WIDTH=634 CELLPADDING=7 CELLSPACING=0>
                <col WIDTH=23>
                    <cOl WIDTH=203>
                        <cOl WIDTH=52>
                            <CoL WIDTH=80>
                                <Col WIDTH=62>
                                    <Col WIDTH=127>
                                        <Tr>
                                            <tD ROWSPAN=3 WIDTH=23 STyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P CLaSs=western ALIGN=CENTER>
                                                    <Font SIZE=2 stylE="font-size: 11pt"><b>NO</b></FONt>
                                                </P>
                                            </td>
                                            <TD ROWSPAN=3 WIDTH=203 sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P ClAss="western" ALIGN=CENTER>
                                                    <foNt SIZE=2 StYle="font-size: 11pt"><b>NAMA
                    DOKUMEN</B></fONT>
                                                </P>
                                            </td>
                                            <Td COLSPAN=2 WIDTH=147 sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p clASs="western" ALIGN=CENTER>
                                                    <fOnt SIZE=2 STyLe="font-size: 11pt"><b>KELENGKAPAN</b></FoNT>
                                                </p>
                                            </td>
                                            <tD ROWSPAN=2 WIDTH=62 stYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p clasS="western" ALIGN=CENTER>
                                                    <foNT SIZE=2 STyLe="font-size: 11pt"><b>Tdk
                    ada</b></FONT>
                                                </p>
                                            </tD>
                                            <Td ROWSPAN=2 WIDTH=127 STylE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <p CLass=western ALIGN=CENTER>
                                                    <fONt SIZE=2 styLe="font-size: 11pt"><b>KETERANGAN</b></fONt>
                                                </p>
                                            </td>
                                        </tr>
                                        <Tr>
                                            <TD COLSPAN=2 WIDTH=147 sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p clASS="western" ALIGN=CENTER>
                                                    <fONT SIZE=2 Style="font-size: 11pt"><b>Ada
                    (+)</B></FonT>
                                                </P>
                                            </TD>
                                        </Tr>
                                        <tR>
                                            <td WIDTH=52 stYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p CLAss="western" ALIGN=CENTER>
                                                    <foNt SIZE=2 StyLE="font-size: 11pt"><b>Sesuai</b></fonT>
                                                </p>
                                            </td>
                                            <TD WIDTH=80 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p cLAsS=western ALIGN=CENTER>
                                                    <FoNT SIZE=2 stylE="font-size: 11pt"><b>Tdk
                    Sesuai</B></FonT>
                                                </P>
                                            </TD>
                                            <tD WIDTH=62 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P cLass=western ALIGN=CENTER>
                                                    <Br>
                                                </p>
                                            </tD>
                                            <TD WIDTH=127 stylE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <p cLasS=western ALIGN=CENTER>
                                                    <br>
                                                </P>
                                            </td>
                                        </Tr>
                                        <tR>
                                            <TD WIDTH=23 style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p clASs="western">
                                                    <FonT SIZE=2 sTYlE="font-size: 11pt">1</fONT>
                                                </p>
                                            </td>
                                            <TD WIDTH=203 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p ClaSs=western>
                                                    <foNT SIZE=2 STyle="font-size: 11pt">Rincian
                    RAB</fOnt>
                                                </p>
                                            </tD>
                                            <TD WIDTH=52 align="center" VALIGN=TOP sTylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P clASs="western" ALIGN=CENTER>√</p>
                                            </Td>
                                            <TD WIDTH=80 sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p ClaSS="western" ALIGN=CENTER>
                                                    <BR>
                                                </p>
                                            </td>
                                            <Td WIDTH=62 STYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P ClaSs="western" ALIGN=CENTER>
                                                    <Br>
                                                </P>
                                            </td>
                                            <Td WIDTH=127 STyLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                <P Class=western ALIGN=CENTER>
                                                    <br>
                                                </p>
                                            </TD>
                                        </TR>
                                        <TR VALIGN=TOP>
                                            <td WIDTH=23 STylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P ClasS=western>
                                                    <FOnt SIZE=2 sTylE="font-size: 11pt">2</font>
                                                </p>
                                            </tD>
                                            <TD WIDTH=203 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p cLass="western">
                                                    <FoNt SIZE=2 stYLE="font-size: 11pt">Koreksi
                    aritmatik </fonT>
                                                </p>
                                            </td>
                                            <TD WIDTH=52 align="center" sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P ClasS=western ALIGN=CENTER>√</P>
                                            </Td>
                                            <td WIDTH=80 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p Class=western ALIGN=JUSTIFY>
                                                    <Br>
                                                </p>
                                            </tD>
                                            <TD WIDTH=62 STYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P clAsS=western ALIGN=JUSTIFY>
                                                    <Br>
                                                </P>
                                            </TD>
                                            <tD WIDTH=127 STYlE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <p ClAsS=western ALIGN=JUSTIFY>
                                                    <br>
                                                </P>
                                            </td>
                                        </TR>
                                        <Tr VALIGN=TOP>
                                            <Td WIDTH=23 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p ClasS="western">
                                                    <bR>
                                                </P>
                                            </td>
                                            <Td COLSPAN=4 WIDTH=440 StyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P ClasS=western ALIGN=JUSTIFY>
                                                    <FOnt SIZE=2 sTyLe="font-size: 11pt">HASIL
                    AKHIR</FonT>
                                                </p>
                                            </Td>
                                            <Td WIDTH=127 sTYLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <p cLaSS=western ALIGN=CENTER>
                                                    <foNT SIZE=2 sTYle="font-size: 11pt">LULUS/</fONT>
                                                    <sTrIKe><fONT SIZE=2 STYLE="font-size: 11pt">TDK
                    LULUS</FoNT></STRiKe>
                                                </p>
                                            </td>
                                        </Tr>
            </TAblE><br/>
            Hasil Penelitian dan penilaian Dokumen Biaya dinyatakan [ LULUS / <strike>TIDAK LULUS</strike> ] dengan demikian dapat dilanjutkan klarifikasi dan negosiasi.
        </li>
        <br/>

        <li>
            <strong>PENELITIAN DOKUMEN KUALIFIKASI</strong><br/>
            <TaBlE WIDTH=643 CELLPADDING=7 CELLSPACING=0>
                <COl WIDTH=23>
                    <cOL WIDTH=314>
                        <cOl WIDTH=4358>
                            <col WIDTH=43>
                                <cOL WIDTH=52>
                                    <col WIDTH=43>
                                        <Col WIDTH=80>
                                            <Tr>
                                                <td ROWSPAN=3 WIDTH=23 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p claSs="western" ALIGN=CENTER>
                                                        <fOnT SIZE=2 Style="font-size: 11pt"><B>NO</B></Font>
                                                    </p>
                                                </tD>
                                                <tD ROWSPAN=3 WIDTH=314 sTylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p cLass="western" ALIGN=CENTER>
                                                        <fONt SIZE=2 stYLE="font-size: 11pt"><b>NAMA
                    DOKUMEN</b></FONt>
                                                    </p>
                                                </tD>
                                                <Td COLSPAN=3 WIDTH=111 sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p clasS=western ALIGN=CENTER>
                                                        <FonT SIZE=2 stYLE="font-size: 11pt"><b>KELENGKAPAN</b></fonT>
                                                    </P>
                                                </tD>
                                                <td ROWSPAN=2 WIDTH=43 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P cLaSs=western ALIGN=CENTER>
                                                        <fOnt SIZE=2 sTyle="font-size: 11pt"><b>Tdk
                    ada</b></Font>
                                                    </P>
                                                </tD>
                                                <td ROWSPAN=2 WIDTH=80 sTYLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P ClASs=western ALIGN=CENTER>
                                                        <FONt SIZE=2 STyle="font-size: 11pt"><b>KET.</b></Font>
                                                    </P>
                                                </Td>
                                            </Tr>
                                            <TR>
                                                <td COLSPAN=3 WIDTH=111 STylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLAss="western" ALIGN=CENTER><font SIZE=2 STYLe="font-size: 11pt"><B>Ada
                    (+)</b></foNT>
                                                    </p>
                                                </Td>
                                            </TR>
                                            <TR>
                                                <td COLSPAN=2 WIDTH=45 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLasS=western ALIGN=CENTER>
                                                        <FoNt SIZE=2 sTYle="font-size: 11pt"><b>Sesuai</b></Font>
                                                    </p>
                                                </tD>
                                                <Td WIDTH=52 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLAss="western" ALIGN=CENTER>
                                                        <Font SIZE=2 StYlE="font-size: 11pt"><B>Tdk
                    Sesuai</B></FONt>
                                                    </p>
                                                </tD>
                                                <TD WIDTH=43 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLAsS=western ALIGN=CENTER>
                                                        <Br>
                                                    </P>
                                                </Td>
                                                <TD WIDTH=80 stYle="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P ClasS=western ALIGN=CENTER>
                                                        <bR>
                                                    </P>
                                                </td>
                                            </tr>
                                            <tR>
                                                <tD WIDTH=23 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLASs="western"><font SIZE=2 STyLe="font-size: 11pt">1</Font>
                                                    </p>
                                                </tD>
                                                <TD WIDTH=314 style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLASS=western ALIGN=JUSTIFY>
                                                        <FonT SIZE=2 StyLe="font-size: 11pt">Surat
                    pernyataan minat</FonT>
                                                    </P>
                                                </td>
                                                <Td COLSPAN=2 align="center" WIDTH=45 VALIGN=TOP STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLASs=western ALIGN=CENTER>√</P>
                                                </Td>
                                                <tD WIDTH=52 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p ClAsS=western ALIGN=CENTER>
                                                        <BR>
                                                    </P>
                                                </Td>
                                                <Td WIDTH=43 STYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p ClaSs=western ALIGN=CENTER>
                                                        <Br>
                                                    </P>
                                                </tD>
                                                <Td WIDTH=80 sTYle="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P ClAss="western" ALIGN=CENTER>
                                                        <br>
                                                    </P>
                                                </td>
                                            </tR>
                                            <tR VALIGN=TOP>
                                                <Td WIDTH=23 stYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLass="western">
                                                        <fonT SIZE=2 StylE="font-size: 11pt">2</foNt>
                                                    </p>
                                                </Td>
                                                <TD WIDTH=314 STyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLASS=western ALIGN=JUSTIFY>
                                                        <fONT SIZE=2 styLE="font-size: 11pt">Data
                    Administrasi</fOnT>
                                                    </p>
                                                </Td>
                                                <TD COLSPAN=2 align="center" WIDTH=45 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p ClaSS="western" ALIGN=CENTER>√</P>
                                                </td>
                                                <tD WIDTH=52 StYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLASS=western ALIGN=JUSTIFY>
                                                        <BR>
                                                    </p>
                                                </Td>
                                                <TD WIDTH=43 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P claSS="western" ALIGN=JUSTIFY>
                                                        <Br>
                                                    </P>
                                                </td>
                                                <Td WIDTH=80 STYlE="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P CLasS=western ALIGN=JUSTIFY>
                                                        <br>
                                                    </P>
                                                </Td>
                                            </tr>
                                            <Tr VALIGN=TOP>
                                                <td WIDTH=23 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p ClASS=western>
                                                        <bR>
                                                    </P>
                                                </td>
                                                <Td WIDTH=314 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <ol>
                                                        <li>
                                                            <font FaCE="Times New Roman, serif"><foNT SIZE=2 StyLE="font-size: 11pt">SIUJK</fonT>
                                                                </Font>
                                                    </Ol>
                                                </td>
                                                <Td COLSPAN=2 align="center" WIDTH=45 StyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLasS=western ALIGN=CENTER>√</p>
                                                </TD>
                                                <TD WIDTH=52 sTyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p cLaSs=western ALIGN=JUSTIFY>
                                                        <br>
                                                    </P>
                                                </Td>
                                                <TD WIDTH=43 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P cLass=western ALIGN=JUSTIFY>
                                                        <Br>
                                                    </p>
                                                </TD>
                                                <TD WIDTH=80 StyLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P clASs=western ALIGN=JUSTIFY>
                                                        <Br>
                                                    </p>
                                                </tD>
                                            </tr>
                                            <tR VALIGN=TOP>
                                                <tD WIDTH=23 sTyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P claSs=western>
                                                        <bR>
                                                    </P>
                                                </td>
                                                <Td WIDTH=314 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <oL START=2>
                                                        <li>
                                                            <fONt FACe="Times New Roman, serif"><FOnT SIZE=2 StyLe="font-size: 11pt">Landasan Hukum</fONT>
                                                            </FoNt>
                                                        </li>
                                                    </OL>
                                                </TD>
                                                <TD COLSPAN=2 align="center" WIDTH=45 StYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P claSs=western ALIGN=CENTER>√</P>
                                                </tD>
                                                <Td WIDTH=52 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p claSS=western ALIGN=JUSTIFY>
                                                        <Br>
                                                    </P>
                                                </td>
                                                <tD WIDTH=43 STylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLAsS="western" ALIGN=JUSTIFY>
                                                        <bR>
                                                    </p>
                                                </TD>
                                                <TD WIDTH=80 StyLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P cLASs="western" ALIGN=JUSTIFY>
                                                        <br>
                                                    </p>
                                                </TD>
                                            </TR>
                                            <tR VALIGN=TOP>
                                                <Td WIDTH=23 stYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLAss=western>
                                                        <BR>
                                                    </P>
                                                </tD>
                                                <tD WIDTH=314 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <OL START=3>
                                                        <Li>
                                                            <FONt FAce="Times New Roman, serif"><FONt SIZE=2 stYlE="font-size: 11pt">Pengurus Perusahaan</FONT></FOnt>
                                                        </li>
                                                    </Ol>
                                                </td>
                                                <tD align="center" COLSPAN=2 WIDTH=45 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p ClASS="western" ALIGN=CENTER>√</p>
                                                </td>
                                                <td WIDTH=52 stYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P clasS="western" ALIGN=JUSTIFY>
                                                        <Br>
                                                    </p>
                                                </Td>
                                                <tD WIDTH=43 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLass=western ALIGN=JUSTIFY>
                                                        <br>
                                                    </P>
                                                </tD>
                                                <tD WIDTH=80 sTYLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P cLaSs="western" ALIGN=JUSTIFY>
                                                        <br>
                                                    </p>
                                                </tD>
                                            </tr>
                                            <tR VALIGN=TOP>
                                                <TD WIDTH=23 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P ClASs=western>
                                                        <BR>
                                                    </P>
                                                </TD>
                                                <Td WIDTH=314 StYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <Ol START=4>
                                                        <lI>
                                                            <fOnt FACE="Times New Roman, serif"><FoNT SIZE=2 stYLe="font-size: 11pt">Data Keuangan</FoNt>
                                                            </FoNt>
                                                        </li>
                                                    </Ol>
                                                </td>
                                                <td align="center" COLSPAN=2 WIDTH=45 sTYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p Class="western" ALIGN=CENTER>√</P>
                                                </tD>
                                                <TD WIDTH=52 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLAsS="western" ALIGN=JUSTIFY>
                                                        <bR>
                                                    </p>
                                                </Td>
                                                <Td WIDTH=43 StYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P clAsS="western" ALIGN=JUSTIFY>
                                                        <BR>
                                                    </P>
                                                </TD>
                                                <td WIDTH=80 StyLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p clAsS="western" ALIGN=JUSTIFY>
                                                        <BR>
                                                    </p>
                                                </TD>
                                            </tr>
                                            <Tr VALIGN=TOP>
                                                <tD WIDTH=23 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p Class=western>
                                                        <FoNt SIZE=2 STYLE="font-size: 11pt">3</fOnT>
                                                    </P>
                                                </TD>
                                                <TD WIDTH=314 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p cLaSS="western" ALIGN=JUSTIFY>
                                                        <FOnt SIZE=2 StYlE="font-size: 11pt">Neraca
                    Perusahaan (tidak harus)</font>
                                                    </P>
                                                </tD>
                                                <td align="center" COLSPAN=2 WIDTH=45 sTyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p clAsS=western ALIGN=CENTER>√</P>
                                                </Td>
                                                <Td WIDTH=52 StYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLASs=western ALIGN=JUSTIFY>
                                                        <Br>
                                                    </P>
                                                </td>
                                                <Td WIDTH=43 style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P ClAsS="western" ALIGN=JUSTIFY>
                                                        <Br>
                                                    </P>
                                                </tD>
                                                <TD WIDTH=80 sTYle="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p cLASS="western" ALIGN=JUSTIFY>
                                                        <br>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr VALIGN=TOP>
                                                <TD WIDTH=23 stYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p claSS=western>
                                                        <FONt SIZE=2 sTylE="font-size: 11pt">4</fONT>
                                                    </p>
                                                </Td>
                                                <Td WIDTH=314 sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P ClASs="western" ALIGN=JUSTIFY>
                                                        <fonT SIZE=2 style="font-size: 11pt">Data
                    Personalia (tidak harus)</FoNt>
                                                    </P>
                                                </td>
                                                <TD align="center" COLSPAN=2 WIDTH=45 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P clAss="western" ALIGN=CENTER>√</P>
                                                </tD>
                                                <Td WIDTH=52 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p ClASs="western" ALIGN=JUSTIFY>
                                                        <BR>
                                                    </P>
                                                </tD>
                                                <Td WIDTH=43 StYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLAsS="western" ALIGN=JUSTIFY>
                                                        <br>
                                                    </p>
                                                </td>
                                                <td WIDTH=80 stYle="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P CLaSS="western" ALIGN=JUSTIFY>
                                                        <Br>
                                                    </P>
                                                </td>
                                            </tr>
                                            <Tr VALIGN=TOP>
                                                <td WIDTH=23 STYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLaSs=western>
                                                        <foNt SIZE=2 stYle="font-size: 11pt">5</FONt>
                                                    </p>
                                                </tD>
                                                <tD WIDTH=314 STyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p cLaSs="western" ALIGN=JUSTIFY>
                                                        <fONT SIZE=2 STylE="font-size: 11pt">Surat
                    Pernyataan Kinerja yang baik</FOnt>
                                                    </p>
                                                </tD>
                                                <tD align="center" COLSPAN=2 WIDTH=45 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p clasS=western ALIGN=CENTER>√</p>
                                                </tD>
                                                <tD WIDTH=52 StYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLaSs=western ALIGN=JUSTIFY>
                                                        <bR>
                                                    </P>
                                                </Td>
                                                <Td WIDTH=43 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLASS=western ALIGN=JUSTIFY>
                                                        <Br>
                                                    </p>
                                                </tD>
                                                <TD WIDTH=80 STYLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P clAss="western" ALIGN=JUSTIFY>
                                                        <Br>
                                                    </P>
                                                </TD>
                                            </TR>
                                            <tR VALIGN=TOP>
                                                <Td WIDTH=23 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p cLASs=western>
                                                        <FoNt SIZE=2 STyLe="font-size: 11pt">6</fonT>
                                                    </p>
                                                </TD>
                                                <TD WIDTH=314 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p clAss="western" ALIGN=JUSTIFY>
                                                        <FoNT SIZE=2 STYle="font-size: 11pt">Surat
                    Pernyataan Bukan PNS/ TNI/POLRI</FOnT>
                                                    </P>
                                                </TD>
                                                <TD align="center" COLSPAN=2 WIDTH=45 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p cLaSs=western ALIGN=CENTER>√</p>
                                                </Td>
                                                <tD WIDTH=52 sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLAss=western ALIGN=JUSTIFY>
                                                        <br>
                                                    </p>
                                                </tD>
                                                <td WIDTH=43 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P ClASS="western" ALIGN=JUSTIFY>
                                                        <br>
                                                    </p>
                                                </TD>
                                                <td WIDTH=80 STYle="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p claSS="western" ALIGN=JUSTIFY>
                                                        <bR>
                                                    </p>
                                                </Td>
                                            </tR>
                                            <tr VALIGN=TOP>
                                                <TD WIDTH=23 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLasS=western><font SIZE=2 stYLe="font-size: 11pt">7</FoNt>
                                                    </P>
                                                </td>
                                                <tD COLSPAN=2 WIDTH=314 stYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLASS="western" ALIGN=JUSTIFY>
                                                        <FonT SIZE=2 styLE="font-size: 11pt">Memiliki
                    NPWP dan telah memenuhi kewajiban perpajakan tahun pajak terakhir
                    (SPT Tahunan) serta memiliki laporan bulanan PPh Pasal 21, PPh
                    Pasal 25/Pasal 29 dan PPN (bagi pengusaha kena pajak) paling
                    kurang 3 (tiga) bulan terakhir dalam tahun berjalan. Penyedia
                    dapat mengganti persyaratan ini dengan menyampaikan Surat
                    Keterangan Fiskal (SKF) yang dikeluarkan oleh kantor Pelayanan
                    Pajak dengan tanggal penerbitam paling lama 1 (satu) bulan sebelum
                    tanggal pemasukan Dokumen Kualifikasi; Pajak 3 bulan terakhir &amp;
                    pajak tahunan terakhir (SPT Tahunan)</foNt>
                                                    </p>
                                                </Td>
                                                <Td align="center" WIDTH=43 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P claSs="western" ALIGN=CENTER>√</p>
                                                </Td>
                                                <TD WIDTH=52 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p Class=western ALIGN=JUSTIFY>
                                                        <Br>
                                                    </p>
                                                </Td>
                                                <td WIDTH=43 STyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p ClASS=western ALIGN=JUSTIFY>
                                                        <BR>
                                                    </P>
                                                </TD>
                                                <TD WIDTH=80 stYLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p clASs=western ALIGN=JUSTIFY>
                                                        <bR>
                                                    </p>
                                                </Td>
                                            </Tr>
                                            <tr VALIGN=TOP>
                                                <TD WIDTH=23 StYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P cLaSS="western">
                                                        <foNT SIZE=2 StyLE="font-size: 11pt">8</FONT>
                                                    </P>
                                                </td>
                                                <TD COLSPAN=2 WIDTH=314 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p ClasS=western ALIGN=JUSTIFY>
                                                        <fONT SIZE=2 sTYLe="font-size: 11pt">Memperoleh paling sedikit 1 (satu) pekerjaan sebagai penyedia jasa konstruksi dalam kurun waktu 4 (empat) tahun terakhir, baik dilingkungan pemerintah maupun swasta termasuk pengalaman subkontrak; (dikecualikan perusahaan berdiri kurang dari 3 tahun)</FOnT>
                                                    </P>
                                                </TD>
                                                <Td align="center" WIDTH=43 sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P ClasS=western ALIGN=CENTER>√</P>
                                                </tD>
                                                <tD WIDTH=52 STYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLASS="western" ALIGN=JUSTIFY>
                                                        <BR>
                                                    </P>
                                                </tD>
                                                <TD WIDTH=43 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P ClAsS="western" ALIGN=JUSTIFY>
                                                        <BR>
                                                    </p>
                                                </Td>
                                                <Td WIDTH=80 STyle="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P ClAss="western" ALIGN=JUSTIFY>
                                                        <BR>
                                                    </p>
                                                </td>
                                            </TR>
                                            <Tr VALIGN=TOP>
                                                <Td WIDTH=23 STyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P cLass=western>
                                                        <FONT SIZE=2 stYLe="font-size: 11pt">9</font>
                                                    </P>
                                                </tD>
                                                <TD COLSPAN=2 WIDTH=314 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLAsS=western ALIGN=JUSTIFY>
                                                        <FoNt SIZE=2 StYle="font-size: 11pt">Data
                    pekerjaan yang sedang dilaksanakan</FonT>
                                                    </P>
                                                </Td>
                                                <TD align="center" WIDTH=43 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p claSS=western ALIGN=CENTER>√</p>
                                                </tD>
                                                <td WIDTH=52 sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p class=western ALIGN=JUSTIFY>
                                                        <Br>
                                                    </p>
                                                </td>
                                                <tD WIDTH=43 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p clASs="western" ALIGN=JUSTIFY>
                                                        <Br>
                                                    </P>
                                                </TD>
                                                <TD WIDTH=80 sTYlE="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p CLASS=western ALIGN=JUSTIFY>
                                                        <bR>
                                                    </p>
                                                </td>
                                            </Tr>
                                            <Tr VALIGN=TOP>
                                                <td WIDTH=23 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P ClaSS="western">
                                                        <Br>
                                                    </P>
                                                </td>
                                                <tD COLSPAN=2 WIDTH=496 sTyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLass=western ALIGN=JUSTIFY>
                                                        <foNt SIZE=2 STYle="font-size: 11pt">HASIL
                    AKHIR</FONT>
                                                    </P>
                                                </TD>
                                                <td COLSPAN=4 WIDTH=80 STYLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p clAss=western ALIGN=CENTER>
                                                        <foNT SIZE=2 sTyLE="font-size: 11pt">LULUS/</FONT>
                                                        <strIkE><FoNT SIZE=2 StyLE="font-size: 11pt">TDK
                    LULUS</FOnt></StriKe>
                                                    </p>
                                                </Td>
                                            </TR>
            </TABLe><br/>
            Hasil Penelitian dan penilaian Dokumen Kualifikasi (Keuangan dan Teknis) dinyatakan [ LULUS / <strike>TIDAK LULUS</strike> ] dengan demikian dapat dilanjutkan Klarifikasi dan Negosiasi.
        </li>
    </ol>
    
    <!-- ========================================== -->
    

    <P LanG=pt-BR clASs=western ALIGN=JUSTIFY StylE="margin-bottom: 0in;">
        <FOnT SIZE=2 STYlE="font-size: 11pt"><B>HAL-HAL YANG DILAKUKAN
KLARIFIKASI DAN NEGOSIASI :</B></FOnT>
    </p>
    <ol>
        <Li>
            <fONT SIZE=2 styLe="font-size: 11pt">Administrasi</fONt>
        </li>
        <LI>
           <fONt SIZE=2 StylE="font-size: 11pt">Teknis</FONt>
        </li>
        <lI>
            <fOnT SIZE=2 StYlE="font-size: 11pt">Harga</foNT>
        </li>
    </ol>
    <P LANG=pt-BR cLASS="western" ALIGN=JUSTIFY StYle="margin-bottom: 0in">
        <fOnT SIZE=2 StyLe="font-size: 11pt">Dengan rincian sebagai berikut :</FONt>
    </P>

    <Ol>
        <LI>
            <P LAnG="pt-BR" ClasS="western" ALIGN=JUSTIFY sTyLe="margin-bottom: 0in">
                <FONt SIZE=2 StylE="font-size: 11pt"><B>EVALUASI ADMINISTRASI</b></fonT>
            </p>
            <OL>
                <Li>
                    <FonT SIZE=2 sTyLE="font-size: 11pt">Evaluasi Administrasi
		dilakukan dengan sistem gugur</fONT>
                    <li>
                        <FOnt SIZE=2 StyLe="font-size: 11pt">Mengadakan penelitian dan
		penilaian secara seksama terhadap penawaran yang telah dikoreksi
		arimatiknya.</fONt>
                        <Li>
                            <FOnT SIZE=2 sTYlE="font-size: 11pt">Kriteria Evaluasi
		Administrasi terhadap Dokumen Penawaran :</Font>
                            <Li>
                                <Font SIZE=2 stYLe="font-size: 11pt">Penawaran dinyatakan
		memenuhi persyaratan administrasi, apabila :</FOnT>
                                <OL TYpE="A">
                                    <lI>
                                        <fONt SIZE=2 stYlE="font-size: 11pt">Surat penawaran memenuhi ketentuan sebagai berikut :</Font>
                                        <OL tyPE="a">
                                            <Li>
                                                <FoNt SIZE=2 sTyle="font-size: 11pt">Ditandatangani oleh :</foNT>
                                                <ul>
                                                    <LI>
                                                        <fONT SIZE=2 stylE="font-size: 11pt">Direktur utama / pimpinan
						perusahaan / pengurus koperasi;</FOnt>
                                                        <LI>
                                                            <fonT SIZE=2 styLe="font-size: 11pt">Penerima kuasa dari direktur
						utama / pimpinan perusahaan / pengurus koperasi yang nama penerima
						kuasanya tercantum dalam akta pendirian / anggaran dasar;</FONT>
                                                            <Li>
                                                                <FONt SIZE=2 stYLE="font-size: 11pt">Pihak lain yang bukan direktur
						utama / pimpinan perusahaan / pengurus koperasi yang namanya tidak
						tercatum dalam akta pendirian / anggaran dasar, sepanjang pihak lain
						tersebut adalah pengurus / karyawan perusahaan / karyawan koperasi
						yang berstatus sebagai tenaga kerja tetap dan mendapat kuasa atau
						pendelegasian wewenang yang sah dari direktur utama / pimpinan
						perusahaan / pengurus koperasi berdasarkan akta pendirian / anggaran
						dasar; atau</FoNt>
                                                                <li>
                                                                    <Font SIZE=2 styLE="font-size: 11pt">Kepala cabang perusahaan yang
						diangkat oleh kantor pusat</fONT>
                                                </UL>
                                                <li>
                                                    <foNt SIZE=2 sTylE="font-size: 11pt"><span Lang="pt-BR">Mencantumkan penawaran harga</sPan></FOnt>
                                                    </P>
                                                    <lI>
                                                        <FONT SIZE=2 stYLE="font-size: 11pt"><SpaN lANG=pt-BR>Jangka waktu berlakunya surat penawaran tidak kurang dari waktu sebagaimana tercantum dalam LDP</SpaN></FONt>
                                                        </p>
                                                        <Li>
                                                            <FOnT SIZE=2 StYLe="font-size: 11pt"><spaN LaNg="pt-BR">Jangka waktu pelaksanaan pekerjaan yang ditawarkan tidak melebihi jangka waktu sebagaimana tercantum dalam LDP</spaN></fONT>
                                                            </p>
                                                            <LI>
                                                                <fonT SIZE=2 styLE="font-size: 11pt"><spaN lANg=pt-BR>bertanggal</spAN></FoNt>
                                                                </p>
                                        </ol>
                                        <lI>
                                            <p lAnG=pt-BR cLAsS="western" ALIGN=JUSTIFY StYlE="margin-bottom: 0in">
                                                <fonT SIZE=2 sTyLe="font-size: 11pt">Pejabat Pengadaan dapat melakukan klarifikasi terhadap hal-hal yang kurang jelas dan meragukan</FoNt>
                                            </p>
                                </Ol>
                                <lI>
                                    <FoNT SIZE=2 StyLe="font-size: 11pt">Apabila penyedia tidak memenuhi
		persyaratan administrasi, Pejabat Pengadaan menyatakan Pengadaan
		Langsung gagal, dan mengundang penyedia lain</font>
                                    <LI>
                                        <fOnT SIZE=2 StYLE="font-size: 11pt">Hasil Evaluasi Administrasi
		adalah penawar memenuhi persyaratan administrasi dan dilanjutkan
		dengan Evaluasi Teknis</foNT>
            </Ol>
            <br/>
            <li>
                <FOnt SIZE=2 StYle="font-size: 11pt"><B>EVALUASI TEKNIS</b></fONt>
                <oL>
                    <LI>
                        <fonT SIZE=2 sTYLE="font-size: 11pt">Evaluasi Teknis dilakukan
			terhadap penyedia yang memenuhi persyaratan administrasi</FoNt>
                        <lI>
                            <fONt SIZE=2 StYle="font-size: 11pt">Unsur-unsur yang dievaluasi
			teknis sesuai dengan yang ditetapkan sebagaimana yang tercantumdi
			spesifikasi</FonT>
                            <Li>
                                <fOnt SIZE=2 sTylE="font-size: 11pt">Evaluasi teknis dilakukan
			dengan sistem gugur</fONt>
                                <Li>
                                    <foNT SIZE=2 styLE="font-size: 11pt">Pejabat Pengadaan menilai persyaratan teknis minimal yang harus dipenuhi sebagaimana tercantum di spesifikasi</FoNt>
                                    <li>
                                        <fONT SIZE=2 STyLe="font-size: 11pt">Penilaian syarat teknis minimal dilakukan terhadap :</fONt>
                                        <uL>
                                            <Li>
                                                <fOnt SIZE=2 Style="font-size: 11pt"><SpAn lANg=pt-BR>Spesifikasi
					teknis barang yang ditawarkan berdasarkan contoh, brosur dan
					gambar-gambar yang memuat identitas barang (jenis, tipe, dan merek)</SpAn></Font>
                                                <lI>
                                                    <FonT SIZE=2 StYLE="font-size: 11pt"><SPaN LaNG="pt-BR">Jangka
					waktu jadwal waktu pelaksanaan pekerjaan dan / atau jadwal serah
					terima pekerjaan (dalam hal serah terima pekerjaan dilakukan per
					termin) sebagai tercantum dalam LDP</sPaN></FONT>
                                                    <lI>
                                                        <FOnT SIZE=2 StYLe="font-size: 11pt"><SpAN LANg=pt-BR>Layanan
					purna jual (apabila dipersyaratkan)</spAn></FONT>
                                                        <LI>
                                                            <fONt SIZE=2 STYLE="font-size: 11pt"><sPan Lang="pt-BR">Tenaga
					teknis operasional / penggunaan barang (apabila dipersyaratkan), dan</sPan></FonT>
                                                            <LI>
                                                                <fonT SIZE=2 styLe="font-size: 11pt"><SPAN lanG=pt-BR>Bagian
					pekerjaan yang akan disubkontrakkan sebagaimana tercantum dalam LDP</sPan></FONt> </Ul>
                                        <lI>
                                            <FonT SIZE=2 STylE="font-size: 11pt">Apabila penyediaan tidak
			memenuhi persyaratan teknis, Pejabat Pengadaan menyatakan Pengadaan
			Langsung gagal, dan mengundang penyedia lain</fonT>
                                            <lI>
                                                <fonT SIZE=2 StYLe="font-size: 11pt">Hasil Evaluasi Teknis adalah
			penawar memenuhi persyaratan Teknis dan dilanjutkan dengan Evaluasi
			Harga</fonT>
                </ol>
                <br/>
                <Li>
                    <fOnt SIZE=2 stYLe="font-size: 11pt"><B>EVALUASI HARGA</B></FOnT><br/>
                    <ol>
                        <LI>
                            
                            <Font SIZE=2 STYLe="font-size: 11pt">Unsur – unsur yang perlu
        dievaluasi adalah hal-hal yang pokok atau penting, dengan ketentuan
        harga satuan penawaran yang nilainya lebih besar dari 110% (seratus
        sepuluh perseratus) dari harga satuan yang tercantum dalam HPS,
        dilakukan klarifikasi. Apabila setelah dilakukan klarifikasi,
        ternyata harga satuan penawaran tersebut dinyatakan timpang maka
        harga satuan timpang hanya berlaku untuk volume sesuai dengan Daftar
        Kuantitas dan Harga. Jika terjadi penambahan volume, harga satuan
        yang berlaku sesuai dengan harga satuan dalam HPS.</FOnT>
                            <Li>
                                <foNT SIZE=2 sTYle="font-size: 11pt">Harga penawaran terkoreksi yang
        melebihi nilai total HPS, dinyatakan gugur. Pejabat pengadaan
        menyatakan Pengadaan Langsung gagal, dan mengundang penyedia lain</fONT>
                            </li>
                            <Li>
                                <FoNt SIZE=2 STYlE="font-size: 11pt">Hasil Evaluasi Harga adalah
    Penawar memenuhi persyaratan harga</foNt>
                            </li>
                    </ol>
    </Ol>
    </ol>

    <p LAng=pt-BR clAss="western" ALIGN=JUSTIFY sTYLE="margin-bottom: 0in">
        <fONT SIZE=2 STyLE="font-size: 11pt"><B>HASIL KLARIFIKASI DAN
NEGOSIASI</b></FONT>
    </P>
    <Ol>
        <Li>
            <foNt SIZE=2 sTyle="font-size: 11pt">Administrasi</fONT>
            <BR>
            <FoNt SIZE=2 STyLe="font-size: 11pt">Calon Penyedia barang/jasa dapat
		menunjukkan semua dokumen “Asli” yang dipersyaratkan dalam
		Dokumen Pengadaan</FoNT>
            <li>
                <P LanG=pt-BR CLaSs=western ALIGN=JUSTIFY style="margin-bottom: 0in">
                    <fOnt SIZE=2 StyLE="font-size: 11pt">Teknis</FONT>
                </P>
                <ol>
                    <Li>
                        <font SIZE=2 StYle="font-size: 11pt"><sPAN lang="pt-BR">Calon
			Penyedia barang/jasa </SPaN></foNt>
                            <Font SIZE=2 StYLe="font-size: 11pt"><sPaN lAnG="pt-BR"><b>sanggup</b></spAN></foNT>
                            <FOnT SIZE=2 sTYLE="font-size: 11pt"><sPAn lAnG=pt-BR>
			melaksanakan / menyerahkan pekerjaan sesuai jadwal waktu
			pelaksanaan/penyerahan yang ditetapkan dalam dokumen pengadaan.</SPan></fONt>
                        </li>
                        <li>
                            
                                <foNt SIZE=2 StylE="font-size: 11pt"><SPan laNG="pt-BR">Calon
			Penyedia barang/jasa </sPan></Font>
                                <foNt SIZE=2 StYle="font-size: 11pt"><span lang="pt-BR"><b>sanggup</b></spAn></FoNt>
                                <fonT SIZE=2 sTylE="font-size: 11pt"><sPAN laNg="pt-BR">
			menyerahkan barang-barang / pekerjaan dalam keadaan baru dan
			berfungsi</sPan></FONt>
                        </li>
                        <Li>
                            <foNT SIZE=2 stYlE="font-size: 11pt"><SpAN LAng=pt-BR>Calon
    Penyedia barang/jasa </sPaN></FoNT>
                            <FOnT SIZE=2 StyLE="font-size: 11pt"><SpAn LanG=pt-BR><B>sanggup</B></sPAn></FONT>
                            <fonT SIZE=2 sTYle="font-size: 11pt"><SPAN LaNg="pt-BR">
    melaksanakan pekerjaan sesuai dengan spesifikasi teknis.</sPAn></fOnt>
                        </li>
                </ol>
                <br/>
                <lI>
                    
                    <FONt SIZE=2 sTyLE="font-size: 11pt">Harga</foNt><br/>
                    
                    
                        <fONt SIZE=2 stYle="font-size: 11pt">Setelah dilaksanakan
		Klarifikasi, selanjutnya dilakukan negosiasi harga dengan hasil
		sebagai berikut :</FoNt>
                    
                    <p LaNg="pt-BR" ClaSs="western" ALIGN=JUSTIFY stYLE="margin-left: 0.25in; margin-bottom: 0in">
                        Harga Penawaran yang tercantum dalam Dokumen Biaya yang diajukan Oleh<br/>
                        <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama); ?> sebesar <?php print('Rp '.format_money($kontrak_pihak_ketiga[0]->nilai_kontrak).',-'); ?> Terbilang (<?php print(ucwords(strtolower(terbilang($kontrak_pihak_ketiga[0]->nilai_kontrak))).' Rupiah'); ?> )
                    </P>
                    
    </Ol>

    <br/>
    <fONT SIZE=2 STyLe="font-size: 11pt">Setelah dilakukan Negosiasi, Pejabat Pengadaan Barang dan Penyedia adalah :</FoNT>
    <br/><br/>

    <table>
        <tr>
            <td class="text">Nama Perusahaan</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->program_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Alamat</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->pekerjaan_nama); ?></td>
        </tr>
        <tr>
            <td class="text">NPWP</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->aktivitas_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Harga Penawaran</td>
            <td class="text">:</td>
            <td class="text"><?php print('Rp '.format_money($kontrak_pihak_ketiga[0]->nilai_kontrak).',-'); ?></td>
        </tr>
        <tr>
            <td class="text">Harga Negosiasi</td>
            <td class="text">:</td>
            <td class="text"><?php print('Rp '.format_money($kontrak_pekerjaan->harga_negosiasi).',-'); ?></td>
        </tr>
        <tr>
            <td class="text">Terbilang</td>
            <td class="text">:</td>
            <td class="text"><?php print(ucwords(strtolower(terbilang($kontrak_pekerjaan->harga_negosiasi))).' Rupiah'); ?></td>
        </tr>
    </table>

    
    <P CLass=western ALIGN=JUSTIFY styLe="margin-bottom: 0in">
        <fOnT SIZE=2 stYLe="font-size: 11pt"><spAN lang="pt-BR">Demikian Berita Acara Klarifikasi dan Negosiasi untuk Paket pekerjaan Judul_Pekerjaan ini dibuat. Untuk dapat dipergunakan sebagaimana mestinya.</spAN></FoNT>
    </P>
    <p></p>
    <p></p>
    <table>
        <tr>
            <td width="300" align="center" style="font-size:14pt">
                Setuju:<br/>
                <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama);?><br/>
                <br/><br/><br/><br/><br/><br/><br/><br/><br/>
                <u><?php print($direktur_perusahaan);?></u><br/>
                Direktur
            </td>
            <td width="400"></td>
            <td width="300" align="center" style="font-size:14pt">
                <?php print($pejabat_pengadaan->jenis_pejabat_pengadaan_nama); ?> <?php print($pejabat_pengadaan->seri_pejabat_pengadaan_nama); ?><br/>
                Pada Dinas Pekerjaan Umum Kota Semarang<br/>
                Tahun Anggaran <?php print(date("Y"));?><br/>
                <br/><br/><br/><br/><br/>
                <u><?php print($pejabat_pengadaan->pegawai_nama); ?></u><br/>
                NIP. <?php print($pejabat_pengadaan->pegawai_nip); ?>
            </td>
        </tr>
    </table>
    <!-- /Berita Acara Evaluasi, Negosiasi dan Klarifikasi -->
    
    <p style="page-break-before: always"></p>

    <!-- Daftar Hadir Berita Acara Evaluasi, Negosiasi dan Klarifikasi -->
    <?php
    $kontrak_surat      = $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'DHN');
    $kontrak_surat      = $kontrak_surat[1];
    ?>

    <P cLASS=western ALIGN=JUSTIFY StYlE="margin-bottom: 0in;">
        <foNt SIZE=2 sTyle="font-size: 11pt"><B>DAFTAR HADIR</b></FONt>
    </P>
    <P class=western ALIGN=CENTER stYle="margin-bottom: 0in">
        <bR>
    </p>
    <table cellpadding="3">
        <tr>
            <td>Program</td>
            <td>:</td>
            <td><?php print($kontrak_pekerjaan->program_nama); ?></td>
        </tr>
        <tr>
            <td>Kegiatan</td>
            <td>:</td>
            <td><?php print($kontrak_pekerjaan->pekerjaan_nama); ?></td>
        </tr>
        <tr>
            <td>Pekerjaan</td>
            <td>:</td>
            <td><?php print($kontrak_pekerjaan->aktivitas_nama); ?></td>
        </tr>
        <tr>
            <td>Tanggal</td>
            <td>:</td>
            <td><?php print(tgl_indo($kontrak_surat->tgl_surat)); ?></td>
        </tr>
        <tr>
            <td>Tempat</td>
            <td>:</td>
            <td><?php print($kontrak_surat->tempat_rapat_nama); ?></td>
        </tr>
        <tr>
            <td>Acara</td>
            <td>:</td>
            <td><?php print($kontrak_surat->acara); ?></td>
        </tr>
    </table>

    
    <P Lang="en-US" CLasS="western" ALIGN=JUSTIFY Style="margin-bottom: 0in">
        <FONT SIZE=2 STylE="font-size: 11pt"><B>PEJABAT PENGADAAN</b></foNt>
    </p>
    <P Lang="en-US" CLasS="western" ALIGN=JUSTIFY Style="margin-bottom: 0in">
        <br/>
    </p>
    

    <CeNTer>
        <tablE WIDTH=603 CELLPADDING=7 CELLSPACING=0>
            <COL WIDTH=23>
                <cOL WIDTH=244>
                    <CoL WIDTH=140>
                        <COl WIDTH=139>
                            <tR VALIGN=TOP>
                                <tD align="center" WIDTH=23 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P laNG="en-US" claSS=western ALIGN=CENTER>
                                        <fONt SIZE=2 StYlE="font-size: 11pt">No</Font>
                                    </P>
                                </td>
                                <tD align="center" WIDTH=244 StYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LAnG=en-US claSS="western" ALIGN=CENTER>
                                        <FONT SIZE=2 StYlE="font-size: 11pt">Nama</fonT>
                                    </P>
                                </TD>
                                <tD align="center" WIDTH=140 STylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LaNg="en-US" CLAss="western" ALIGN=CENTER>
                                        <FONt SIZE=2 STYLe="font-size: 11pt">Jabatan</FOnT>
                                    </P>
                                </Td>
                                <tD WIDTH=139 sTylE="border: 1px solid #000000; padding: 0in 0.08in" align="center">
                                    <p laNG="en-US" claSS="western" ALIGN=CENTER>
                                        <FonT SIZE=2 sTYlE="font-size: 11pt">Tanda
				Tangan</foNt>
                                    </p>
                                </tD>
                            </tR>
                            <tr VALIGN=TOP>
                                <TD WIDTH=23 StyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                    <P LaNG=en-US clAsS="western" valign="middle" ALIGN=CENTER sTYLE="margin-bottom: 0in">
                                        <foNt SIZE=2 stYLE="font-size: 11pt">1.</FONT>
                                    </P>
                                    
                                </TD>
                                <td WIDTH=244 valign="middle" StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                    <p LAng="en-US" cLasS="western" ALIGN=JUSTIFY><font SIZE=2 sTYlE="font-size: 11pt"><?php print($kontrak_pekerjaan->pejabat_pengadaan_nama); ?></Font>
                                    </P>
                                    
                                </td>
                                <TD WIDTH=140 valign="middle" StYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                    <p lAnG="en-US" CLAss="western" ALIGN=CENTER>
                                        <FOnt SIZE=2 sTYle="font-size: 11pt">Pejabat Pengadaan <?php print($kontrak_pekerjaan->seri_pejabat_pengadaan_nama); ?></Font>
                                    </P>
                                    
                                </Td>
                                <td WIDTH=139 valign="middle" STyLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p LANg=en-US ClASS="western" ALIGN=JUSTIFY stYLe="margin-bottom: 0in">
                                        <br/>
                                        <br/>
                                        <br/>
                                        <br/>
                                    </p>
                                </TD>
                            </Tr>
        </tAbLe>
    </cEntER>
    <p lANG="en-US" cLASs="western" ALIGN=JUSTIFY StylE="margin-bottom: 0in">
        <Br>
    </P>
    <P lANG=en-US cLaSS=western ALIGN=JUSTIFY sTyLe="margin-bottom: 0in">
        <FONT SIZE=2 StYlE="font-size: 11pt"><b>PIHAK KETIGA</B></FONt>
    </P>
    <p lANG=en-US CLAsS=western ALIGN=JUSTIFY sTylE="margin-bottom: 0in">
        <BR>
    </P>
    <ceNtEr>
        <TABLe WIDTH=625 CELLPADDING=7 CELLSPACING=0>
            <CoL WIDTH=22>
                <col WIDTH=230>
                    <col WIDTH=175>
                        <CoL WIDTH=140>
                            <tr VALIGN=TOP>
                                <td WIDTH=22 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LanG=en-US CLASS="western" ALIGN=CENTER>
                                        <fOnT SIZE=2 stYle="font-size: 11pt">No
				</FOnt>
                                    </P>
                                </td>
                                <Td align="center" WIDTH=230 StyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LAnG="en-US" ClasS="western" ALIGN=CENTER>
                                        <foNt SIZE=2 sTYle="font-size: 11pt">Nama Perusahaan</Font>
                                    </p>
                                </TD>
                                <TD align="center" WIDTH=175 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LAnG="en-US" ClAsS=western ALIGN=CENTER>
                                        <FONt SIZE=2 stYlE="font-size: 11pt">Nama Peserta</foNT>
                                    </p>
                                </td>
                                <Td align="center" WIDTH=140 styLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p lanG="en-US" clAss=western ALIGN=CENTER>
                                        <foNT SIZE=2 stYle="font-size: 11pt">Tanda Tangan</Font>
                                    </p>
                                </tD>
                            </tr>
                            <tR VALIGN=TOP>
                                <td valign="middle" WIDTH=22 sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                    <P lANg="en-US" ClAss="western" ALIGN=CENTER sTYLE="margin-bottom: 0in">
                                        <FoNT SIZE=2 STylE="font-size: 11pt">2.</font>
                                    </p>
                                    
                                </tD>
                                <td valign="middle" WIDTH=230 StYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P lanG=en-US CLASS=western ALIGN=JUSTIFY StylE="margin-bottom: 0in">
                                       <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama); ?>
                                    </P>
                                </TD>
                                <tD valign="middle" WIDTH=175 sTyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                
                                    <P lanG=en-US ClaSs=western ALIGN=CENTER StYlE="margin-bottom: 0in"><?php print($direktur_perusahaan); ?></p>

                                </TD>
                                <Td valign="middle" WIDTH=140 sTYlE="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p LANG="en-US" cLASS=western ALIGN=CENTER stylE="margin-bottom: 0in">
                                        <br/>
                                        <br/>
                                        <br/>
                                        <br/>
                                    </P>
                                    
                                    
                                </td>
                            </Tr>
        </TAblE>
    </center>
    <!-- /Daftar Hadir Berita Acara Evaluasi, Negosiasi dan Klarifikasi -->

    <p style="page-break-before: always"></p>

    <!-- Berita Acara Hasil Pengadaan Langsung -->
    <?php
    $kontrak_surat      = $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'BHPL');
    $kontrak_surat      = $kontrak_surat[0];
    $surat_negosiasi    = $this->kontrak_surat_penawaran->fetch_data(NULL, $kontrak_pekerjaan->id, 'BAENK');

    $dayname    = dayname(date('l', strtotime($kontrak_surat->tgl_surat)));
    $tglindo    = tgl_indo($kontrak_surat->tgl_surat);
    $split_tgl  = explode(' ', $tglindo);
    ?>
    <p ClASs=western ALIGN=CENTER sTylE="margin-bottom: 0in; page-break-before: none">
        <b>
            BERITA ACARA HASIL PENGADAAN LANGSUNG<br/>
            (B A H P L)<br/>
            -------------------------------------------------------------------------------------------------------------------<br>
            <?php print($kontrak_surat->no_surat);?>
        </b>
    </P>
    <p></p>
    
    <p cLAss=western ALIGN="justify" stylE="margin-bottom: 0in">
        Pada hari ini, <?php print($dayname); ?>, tanggal <?php print($split_tgl[0]); ?> bulan <?php print($split_tgl[1]); ?> tahun <?php print($split_tgl[2]); ?> (<?php print($tglindo); ?>) , bertempat di Kantor Dinas Pekerjaan Umum Kota Semarang, yang bertanda tangan dibawah ini :
    </p>
    <p><?php print($pejabat_pengadaan->pegawai_nama); ?></p>
    
    <p ClASs=western align="justify" STYle="margin-bottom: 0in">
        Selaku Pejabat Pengadaan pada Dinas Pekerjaan Umum Kota Semarang Tahun Anggaran <?php print(date("Y")); ?>, yang dibentuk berdasarkan Keputusan Kepala Dinas Pekerjaan Umum Kota Semarang Selaku Pengguna Anggaran Nomor <?php print($pejabat_pengadaan->sk_no); ?> tanggal <?php print(tgl_indo($pejabat_pengadaan->sk_tgl_penetapan)); ?> tentang Penunjukan Pejabat Pengadaan Pada Dinas Pekerjaan Umum kota Semarang APBD Tahun Anggaran <?php print($pejabat_pengadaan->sk_tahun); ?>, telah melakukan Penyusunan Hasil Pengadaan Langsung (BAHPL) , untuk :        
    </p>

    <br/>
    <table>
        <tr>
            <td class="text">Program</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->program_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Kegiatan</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->pekerjaan_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Pekerjaan</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->aktivitas_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Sumber Dana</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->aktivitas_sumber_dana); ?></td>
        </tr>
    </table>

    <p class="western" align="justify" STYle="margin-bottom: 0in">
        Adapun hal-hal yang disepakati adalah sebagai berikut :
    </p>
    
    <p class="western" align="justify" STYle="margin-bottom: 0in">
        Pelaksanaan pengadaan barang/jasa telah dilaksanakan sesuai Peraturan Presiden Republik Indonesia Nomor 4 Tahun 201 5 tentang Perubahan keempat atas Peraturan Presiden Nomor 54 Tahun 2010 tentang Pengadaan Barang/Jasa Pemerintah dan telah melalui tahapan-tahapan kegiatan.
    <p>

    <p class="western" align="center" STYle="margin-bottom: 0in">
        <b>TAHAP PROSES PENGADAAN LANGSUNG</b>
    </p>

    <p class="western" align="justify" STYle="margin-bottom: 0in">
        <ol type="1">
            <li>Perusahaan yang diundang sebanyak 1 (satu) dan memasukkan dokumen sebanyak 1 (satu) Perusahaan</li>
            <br/>
            <li>
                Pemasukkan dan Pembukaan Dokumen Penawaran
                <ul type="dist">
                    <li>Dokumen penawaran yang disampaikan oleh penyedia barang/jasa, setelah dibuka dan diteliti dinyatakan memenuhi syarat sehingga dapat dilanjutkan evaluasi.</li>
                    <li>
                        Hasil Pembukaan Dokumen Penawaran adalah sebagai berikut :<br/>
                        HPS/OE : <?php print('Rp '.format_money($kontrak_pekerjaan->hps).',-'); ?><br/>
                        <taBle WIDTH=649 CELLPADDING=7 CELLSPACING=0>
                            
                                        <tr>
                                            <tD ALIGN=CENTER sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                
                                                    <fOnT SIZE=2 stYLE="font-size: 11pt"><b>NO</b></fOnt>
                                                
                                            </td>
                                            <td ALIGN=CENTER WIDTH=297 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                
                                                    <FONT SIZE=2 StYle="font-size: 11pt"><b>NAMA PENYEDIA JASA</b></fOnT>
                                               
                                            </Td>
                                            <td ALIGN=CENTER WIDTH=156 StYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                
                                                    <fONT SIZE=2 sTyLE="font-size: 11pt"><b>NILAI PENAWARAN (Rp) </B></FoNT>
                                                
                                            </Td>
                                        </tr>
                                        <tR VALIGN=TOP>
                                            <TD ALIGN=CENTER sTyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                
                                                    <fONT SIZE=2 stYlE="font-size: 11pt">1</fONT>
                                                
                                            </Td>
                                            <Td WIDTH=297 sTYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p ClasS="western">
                                                    <fONt SIZE=2 stYlE="font-size: 11pt"><SPaN Lang="pt-BR"><?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama); ?></spaN></FONT>
                                                </p>
                                            </tD>
                                            <td ALIGN=CENTER WIDTH=156 StYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                <P cLass=western><?php print('Rp '.format_money($kontrak_pihak_ketiga[0]->nilai_kontrak).',-'); ?></P>
                                            </tD>
                                        </Tr>
                        </taBle>
                    </li>
                </ul>
            </li>
            <br/>
            <li>
                Unsur-unsur yang dievaluasi
                <ul type="a">
                    <li>
                        Evaluasi Administrasi <br/>
                        <taBLe WIDTH=649 CELLPADDING=7 CELLSPACING=0>
                            
                            <tR>
                                <tD ROWSPAN=3 ALIGN=CENTER WIDTH=23 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <fONt SIZE=2 STyle="font-size: 11pt">NO</Font>
                                </Td>
                                <Td ROWSPAN=3 ALIGN=CENTER WIDTH=196 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <FoNt SIZE=2 STYle="font-size: 11pt">NAMA DOKUMEN</FonT>
                                </TD>
                                <TD COLSPAN=2 ALIGN=CENTER WIDTH=132 VALIGN=TOP sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P lanG=pt-BR clASs=western ALIGN=CENTER>
                                        <FONT SIZE=2 STyLe="font-size: 11pt">KELENGKAPAN</foNt>
                                    </p>
                                </td>
                                <tD ROWSPAN=3 WIDTH=56 STYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LanG=pt-BR CLAsS=western ALIGN=CENTER>
                                        <fonT SIZE=2 sTyLe="font-size: 11pt">Tdk ada</FoNT>
                                    </P>
                                </TD>
                                <TD ROWSPAN=3 WIDTH=136 StyLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p laNg="pt-BR" cLASs="western" ALIGN=CENTER>
                                        <FoNT SIZE=2 STYle="font-size: 11pt">KETERANGAN</FONt>
                                    </p>
                                </Td>
                            </Tr>
                            <tR VALIGN=TOP>
                                <tD COLSPAN=2 WIDTH=132 sTylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P Lang=pt-BR cLass=western ALIGN=CENTER>
                                        <FoNt SIZE=2 STYlE="font-size: 11pt">Ada (+)</Font>
                                    </P>
                                </Td>
                            </Tr>
                            <Tr VALIGN=TOP>
                                <td WIDTH=46 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LAnG="pt-BR" CLasS=western ALIGN=JUSTIFY>
                                        <FONT SIZE=2 sTYLE="font-size: 11pt">Sesuai</font>
                                    </p>
                                </TD>
                                <Td WIDTH=72 StyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p lang="pt-BR" ClAss="western" ALIGN=JUSTIFY>
                                        <fOnT SIZE=2 STyLE="font-size: 11pt">Tdk Sesuai</fONt>
                                    </P>
                                </TD>
                            </Tr>
                            <tr VALIGN=TOP>
                                <td WIDTH=23 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P laNg="pt-BR" cLASS="western" ALIGN=CENTER>
                                        <foNt SIZE=2 STyle="font-size: 11pt">1</font>
                                    </P>
                                </tD>
                                <tD WIDTH=196 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P lAng=pt-BR class=western ALIGN=JUSTIFY>
                                        <foNT SIZE=2 sTylE="font-size: 11pt">Ditandatangai oleh pihak sebagaimana dalam ketentuan</foNt>
                                    </P>
                                </td>
                                <Td WIDTH=46 ALIGN=CENTER stYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                        <fonT SIZE=2 StyLE="font-size: 11pt">+</foNT>
                                    
                                </TD>
                                <TD WIDTH=72 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LaNg=pt-BR clasS="western" ALIGN=JUSTIFY>
                                        <bR>
                                    </P>
                                </td>
                                <tD WIDTH=56 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LANG=pt-BR ClAsS=western ALIGN=JUSTIFY>
                                        <bR>
                                    </P>
                                </tD>
                                <tD WIDTH=136 STylE="border: 1px solid #000000; padding: 0in 0.08in">
                                </tD>
                            </TR>
                            <Tr VALIGN=TOP>
                                <Td WIDTH=23 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LanG="pt-BR" ClASS=western ALIGN=CENTER>
                                        <fOnT SIZE=2 sTYlE="font-size: 11pt">2</FonT>
                                    </p>
                                </tD>
                                <TD WIDTH=196 style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LaNG=pt-BR claSS="western" ALIGN=JUSTIFY>
                                        <FOnt SIZE=2 styLE="font-size: 11pt">Mencantumkan penawaran harga</Font>
                                    </p>
                                </Td>
                                <td WIDTH=46 ALIGN=CENTER style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                        <fONt SIZE=2 StyLe="font-size: 11pt">+</fOnT>
                                    
                                </Td>
                                <tD WIDTH=72 STyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p Lang=pt-BR CLaSs="western" ALIGN=JUSTIFY>
                                        <bR>
                                    </P>
                                </Td>
                                <td WIDTH=56 StYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p laNG=pt-BR cLasS="western" ALIGN=JUSTIFY>
                                        <BR>
                                    </p>
                                </td>
                                <td WIDTH=136 sTYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <P lANG="pt-BR" ClAsS=western ALIGN=JUSTIFY>
                                        <Br>
                                    </P>
                                </tD>
                            </Tr>
                            <tr VALIGN=TOP>
                                <tD WIDTH=23 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LAnG=pt-BR cLASs="western" ALIGN=CENTER>
                                        <FoNT SIZE=2 sTYle="font-size: 11pt">3</font>
                                    </p>
                                </TD>
                                <TD WIDTH=196 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P lanG="pt-BR" ClaSS=western ALIGN=JUSTIFY>
                                        <fONt SIZE=2 stYLE="font-size: 11pt">Jangka waktu surat penawaran</FoNT>
                                    </P>
                                </td>
                                <tD WIDTH=46 ALIGN=CENTER STyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                        <fonT SIZE=2 styLe="font-size: 11pt">+</fONT>
                                    
                                </tD>
                                <td WIDTH=72 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LANG=pt-BR clasS=western ALIGN=JUSTIFY>
                                        <Br>
                                    </P>
                                </Td>
                                <TD WIDTH=56 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LAng=pt-BR ClasS="western" ALIGN=JUSTIFY>
                                        <Br>
                                    </p>
                                </TD>
                                <Td WIDTH=136 STyLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p LaNG=pt-BR clAsS="western" ALIGN=JUSTIFY>
                                        <br>
                                    </P>
                                </Td>
                            </tR>
                            <TR VALIGN=TOP>
                                <tD WIDTH=23 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P Lang="pt-BR" ClAss="western" ALIGN=CENTER>
                                        <FoNT SIZE=2 sTYlE="font-size: 11pt">4</Font>
                                    </P>
                                </td>
                                <tD WIDTH=196 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LANg="pt-BR" clasS=western ALIGN=JUSTIFY>
                                        <FonT SIZE=2 Style="font-size: 11pt">Jangka waktu pelaksanaan</FONt>
                                    </P>
                                </TD>
                                <Td WIDTH=46 ALIGN=CENTER stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                        <Font SIZE=2 stYLE="font-size: 11pt">+</fONT>
                                    
                                </Td>
                                <td WIDTH=72 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LanG=pt-BR ClaSS="western" ALIGN=JUSTIFY>
                                        <Br>
                                    </P>
                                </Td>
                                <TD WIDTH=56 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LAnG=pt-BR CLASS=western ALIGN=JUSTIFY>
                                        <Br>
                                    </p>
                                </tD>
                                <Td WIDTH=136 style="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p laNG=pt-BR CLASs="western" ALIGN=JUSTIFY>
                                        <br>
                                    </p>
                                </td>
                            </TR>
                            <tR VALIGN=TOP>
                                <td WIDTH=23 StYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P lAnG="pt-BR" ClaSS=western ALIGN=CENTER>
                                        <fONT SIZE=2 stYLe="font-size: 11pt">5</fOnT>
                                    </p>
                                </tD>
                                <td WIDTH=196 STyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p laNg=pt-BR cLAss=western ALIGN=JUSTIFY>
                                        <foNT SIZE=2 STyLe="font-size: 11pt">Tanggal</FonT>
                                    </P>
                                </Td>
                                <td WIDTH=46 ALIGN=CENTER StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                        <FOnt SIZE=2 sTYlE="font-size: 11pt">+</fOnt>
                                    
                                </TD>
                                <TD WIDTH=72 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P laNg=pt-BR CLAsS=western ALIGN=JUSTIFY>
                                        <bR>
                                    </p>
                                </td>
                                <Td WIDTH=56 sTyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LaNg=pt-BR clASS="western" ALIGN=JUSTIFY>
                                        <bR>
                                    </p>
                                </Td>
                                <TD WIDTH=136 styLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p Lang=pt-BR ClAsS=western ALIGN=JUSTIFY>
                                        <bR>
                                    </P>
                                </td>
                            </tR>
                            <TR VALIGN=TOP>
                                <td WIDTH=23 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p lANg="pt-BR" CLAsS="western" ALIGN=JUSTIFY>
                                        <bR>
                                    </p>
                                </TD>
                                <Td WIDTH=196 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P lANG=pt-BR cLasS=western ALIGN=JUSTIFY>
                                        <fONt SIZE=2 stylE="font-size: 11pt">HASIL AKHIR</FonT>
                                    </P>
                                </TD>
                                <tD WIDTH=46 StYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P lang="pt-BR" ClASs=western ALIGN=JUSTIFY>
                                        <bR>
                                    </p>
                                </tD>
                                <tD WIDTH=72 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LanG=pt-BR clasS="western" ALIGN=JUSTIFY>
                                        <BR>
                                    </P>
                                </td>
                                <td WIDTH=56 StYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LANG=pt-BR cLASS="western" ALIGN=JUSTIFY>
                                        <Br>
                                    </p>
                                </tD>
                                <TD WIDTH=136 stYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <P cLASs="western" ALIGN=JUSTIFY>
                                        <FoNt SIZE=2 StYLe="font-size: 11pt"><Span LanG="pt-BR">LULUS/</SPaN></FOnt>
                                        <sTRIKe><FoNt SIZE=2 styLe="font-size: 11pt"><SPAN LaNg=pt-BR>TDK LULUS</SpAn></fonT></StrIKe>
                                    </P>
                                </Td>
                            </Tr>
                        </TAblE>

                    </li>
                    <p></p>
                    <p></p>
                    <p></p>
                    <p></p>
                    <p></p>
                    <li>
                        Evaluasi Teknis
                        <TAblE WIDTH=649 CELLPADDING=7 CELLSPACING=0>
                            
                                                    <tR>
                                                        <td ROWSPAN=3 WIDTH=23 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <P lanG="pt-BR" cLaSS=western ALIGN=CENTER><font SIZE=2 sTyLE="font-size: 11pt"><B>NO</B></FoNT>
                                                            </p>
                                                        </Td>
                                                        <td ROWSPAN=3 WIDTH=196 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <P LANg=pt-BR cLass="western" ALIGN=CENTER>
                                                                <FOnt SIZE=2 style="font-size: 11pt"><B>NAMA DOKUMEN</B></font>
                                                            </P>
                                                        </tD>
                                                        <Td COLSPAN=2 WIDTH=132 VALIGN=TOP STylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <P lANg=pt-BR ClaSs=western ALIGN=CENTER>
                                                                <foNt SIZE=2 stYlE="font-size: 11pt"><B>KELENGKAPAN</B></FOnt>
                                                            </p>
                                                        </td>
                                                        <TD ROWSPAN=3 WIDTH=56 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <P lANg="pt-BR" cLasS=western ALIGN=CENTER>
                                                                <FoNt SIZE=2 STyle="font-size: 11pt"><B>Tdk ada</B></fONt>
                                                            </P>
                                                        </tD>
                                                        <TD ROWSPAN=3 WIDTH=136 StYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                            <P LAng="pt-BR" CLaSs=western ALIGN=CENTER>
                                                                <Font SIZE=2 STyle="font-size: 11pt"><B>KETERANGAN</B></FoNT>
                                                            </p>
                                                        </td>
                                                    </Tr>
                                                    <tR VALIGN=TOP>
                                                        <Td COLSPAN=2 WIDTH=132 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p lANg="pt-BR" clasS=western ALIGN=CENTER>
                                                                <fOnt SIZE=2 sTYLE="font-size: 11pt"><b>Ada (+)</B></foNt>
                                                            </P>
                                                        </tD>
                                                    </tr>
                                                    <tR VALIGN=TOP>
                                                        <TD WIDTH=46 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p LaNG=pt-BR clASs=western ALIGN=JUSTIFY>
                                                                <FONT SIZE=2 sTYlE="font-size: 11pt"><B>Sesuai</b></Font>
                                                            </p>
                                                        </TD>
                                                        <tD WIDTH=72 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p lang=pt-BR cLAss=western ALIGN=JUSTIFY>
                                                                <fOnT SIZE=2 stYLE="font-size: 11pt"><b>Tdk Sesuai</B></fONt>
                                                            </p>
                                                        </tD>
                                                    </Tr>
                                                    <tR VALIGN=TOP>
                                                        <tD WIDTH=23 STyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p LANg="pt-BR" Class=western ALIGN=CENTER>
                                                                <FONt SIZE=2 StylE="font-size: 11pt">1</FOnt>
                                                            </p>
                                                        </tD>
                                                        <td WIDTH=196 STylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <P laNg=pt-BR cLASs=western ALIGN=JUSTIFY>
                                                                <Font SIZE=2 sTYlE="font-size: 11pt">Time Schedule</fOnt>
                                                            </P>
                                                        </tD>
                                                        <td WIDTH=46 ALIGN=CENTER sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <font SIZE=2 StyLE="font-size: 11pt">+</foNt>
                                                            
                                                        </Td>
                                                        <tD WIDTH=72 sTYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p lAnG="pt-BR" cLASs=western ALIGN=JUSTIFY>
                                                                <br>
                                                            </p>
                                                        </Td>
                                                        <Td WIDTH=56 StyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p lANG=pt-BR ClAsS=western ALIGN=JUSTIFY>
                                                                <Br>
                                                            </p>
                                                        </td>
                                                        <TD WIDTH=136 sTyLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                            <P lAng=pt-BR cLaSs="western" ALIGN=JUSTIFY>
                                                                <bR>
                                                            </P>
                                                        </tD>
                                                    </tR>
                                                    <tR VALIGN=TOP>
                                                        <TD WIDTH=23 STyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p lanG="pt-BR" CLAss=western ALIGN=CENTER>
                                                                <fonT SIZE=2 sTYlE="font-size: 11pt">2</Font>
                                                            </p>
                                                        </td>
                                                        <Td WIDTH=196 style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p LAnG=pt-BR CLass=western ALIGN=JUSTIFY>
                                                                <fONT SIZE=2 stYlE="font-size: 11pt">Jangka Waktu pelaksanaan</fonT>
                                                            </p>
                                                        </Td>
                                                        <tD WIDTH=46 ALIGN=CENTER StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            
                                                                <foNT SIZE=2 STylE="font-size: 11pt">+</FONt>
                                                            
                                                        </TD>
                                                        <td WIDTH=72 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p LaNG=pt-BR ClAss=western ALIGN=JUSTIFY>
                                                                <bR>
                                                            </p>
                                                        </tD>
                                                        <td WIDTH=56 stYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p LANG="pt-BR" ClaSs=western ALIGN=JUSTIFY>
                                                                <bR>
                                                            </P>
                                                        </TD>
                                                        <TD WIDTH=136 StYLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                            <p LaNG=pt-BR cLASS=western ALIGN=JUSTIFY>
                                                                <BR>
                                                            </p>
                                                        </TD>
                                                    </TR>
                                                    <tR VALIGN=TOP>
                                                        <td WIDTH=23 stYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p lANG="pt-BR" clASS="western" ALIGN=JUSTIFY>
                                                                <BR>
                                                            </p>
                                                        </TD>
                                                        <TD WIDTH=196 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p lang=pt-BR clAsS=western ALIGN=JUSTIFY>
                                                                <FOnt SIZE=2 stYLe="font-size: 11pt">HASIL AKHIR</font>
                                                            </p>
                                                        </tD>
                                                        <Td WIDTH=46 StyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p lanG=pt-BR clASs="western" ALIGN=JUSTIFY>
                                                                <bR>
                                                            </P>
                                                        </tD>
                                                        <td WIDTH=72 StYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <P lAnG="pt-BR" ClaSs=western ALIGN=JUSTIFY>
                                                                <BR>
                                                            </p>
                                                        </Td>
                                                        <td WIDTH=56 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                            <p LAng=pt-BR clASS=western ALIGN=JUSTIFY>
                                                                <BR>
                                                            </P>
                                                        </Td>
                                                        <tD WIDTH=136 styLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                            <P cLASS=western ALIGN=JUSTIFY>
                                                                <FonT SIZE=2 styLE="font-size: 11pt"><SPaN Lang="pt-BR">LULUS/</SpAn></fONT>
                                                                <sTrIKe><foNT SIZE=2 StYLe="font-size: 11pt"><Span LaNg=pt-BR>TDK LULUS</SPan></FoNT></STRike>
                                                            </p>
                                                        </tD>
                                                    </Tr>
                        </tAble>
                    </li>
                    <br/>
                    <li>
                        Evaluasi Harga
                        <TABLe WIDTH=649 CELLPADDING=7 CELLSPACING=0>
                            <Tr>
                                <Td ROWSPAN=3 WIDTH=23 stYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">                                
                                    <FOnt SIZE=2 StYle="font-size: 11pt"><B>NO</b></fonT>
                                </tD>
                                <td ROWSPAN=3 WIDTH=196 ALIGN=CENTER STyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <FOnt SIZE=2 STyLe="font-size: 11pt"><B>NAMA DOKUMEN</b></FoNt>
                                </tD>
                                <Td COLSPAN=2 ALIGN=CENTER WIDTH=132 VALIGN=TOP stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                        <FoNT SIZE=2 sTyLE="font-size: 11pt"><B>KELENGKAPAN</B></foNT>
                                
                                </TD>
                                <tD ROWSPAN=3 ALIGN=CENTER WIDTH=56 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                        <FONt SIZE=2 sTylE="font-size: 11pt"><b>Tdk ada</B></FonT>
                                    
                                </tD>
                                <tD ROWSPAN=3 ALIGN=CENTER WIDTH=136 Style="border: 1px solid #000000; padding: 0in 0.08in">
                                    
                                        <FONT SIZE=2 Style="font-size: 11pt"><b>KETERANGAN</B></FONt>
                                    
                                </Td>
                            </tR>
                            <Tr VALIGN=TOP>
                                <td COLSPAN=2 WIDTH=132 ALIGN=CENTER sTyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                
                                        <fONt SIZE=2 styLe="font-size: 11pt"><b>Ada (+)</b></FONt>
                                </Td>
                            </tR>
                            <TR VALIGN=TOP>
                                <TD WIDTH=46 ALIGN=CENTER sTylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                        <fOnT SIZE=2 StYle="font-size: 11pt"><B>Sesuai</b></FoNT>
                                    
                                </td>
                                <tD WIDTH=72 ALIGN=CENTER StYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p Lang=pt-BR CLaSs=western ALIGN=JUSTIFY>
                                        <FonT SIZE=2 StYLe="font-size: 11pt"><B>Tdk Sesuai</b></FOnt>
                                    </P>
                                </Td>
                            </tr>
                            <tR VALIGN=TOP>
                                <Td WIDTH=23 StYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LanG=pt-BR CLasS="western" ALIGN=CENTER>
                                        <FoNt SIZE=2 sTyLe="font-size: 11pt">1</FoNT>
                                    </P>
                                </Td>
                                <Td WIDTH=196 STYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LANg=pt-BR ClaSS=western ALIGN=JUSTIFY>
                                        <FoNT SIZE=2 StyLe="font-size: 11pt">Daftar Kuantitas dan Harga</FoNT>
                                    </P>
                                </td>
                                <td WIDTH=46 ALIGN=CENTER STYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                        <FONT SIZE=2 styLe="font-size: 11pt">+</fONT>
                                    
                                </tD>
                                <TD WIDTH=72 STYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LaNg="pt-BR" cLAsS="western" ALIGN=JUSTIFY>
                                        <Br>
                                    </p>
                                </Td>
                                <td WIDTH=56 stYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P lang="pt-BR" ClASs="western" ALIGN=JUSTIFY>
                                        <BR>
                                    </P>
                                </Td>
                                <Td WIDTH=136 stYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p lAnG=pt-BR ClasS=western ALIGN=JUSTIFY>
                                        <bR>
                                    </P>
                                </tD>
                            </tr>
                            <tr VALIGN=TOP>
                                <Td WIDTH=23 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LanG=pt-BR CLASS="western" ALIGN=JUSTIFY>
                                        <Br>
                                    </P>
                                </Td>
                                <tD WIDTH=196 STyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LANg=pt-BR clAsS="western" ALIGN=JUSTIFY>
                                        <Font SIZE=2 sTYlE="font-size: 11pt">HASIL AKHIR</FonT>
                                    </p>
                                </td>
                                <TD WIDTH=46 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LAnG=pt-BR class=western ALIGN=JUSTIFY>
                                        <Br>
                                    </p>
                                </TD>
                                <Td WIDTH=72 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P lANG=pt-BR ClASS=western ALIGN=JUSTIFY>
                                        <bR>
                                    </P>
                                </td>
                                <td WIDTH=56 STYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p Lang="pt-BR" cLasS=western ALIGN=JUSTIFY>
                                        <BR>
                                    </P>
                                </TD>
                                <td WIDTH=136 sTyLE="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p claSS=western ALIGN=JUSTIFY>
                                        <Font SIZE=2 Style="font-size: 11pt"><SpaN LanG="pt-BR">LULUS/</SPAN></FonT>
                                        <StrikE><FOnt SIZE=2 stYLE="font-size: 11pt"><SPan LAng=pt-BR>TDK LULUS</sPAn></fOnt></STriKE>
                                    </p>
                                </td>
                            </tR>
                        </TABLe>

                    </li>
                    <br/>
                    <li>
                        Berdasarkan Berita Acara Evaluasi, Klarifikasi dan Negosiasi Nomor : <?php print((!empty($surat_negosiasi[0]->no_surat))?$surat_negosiasi[0]->no_surat:''); ?> tanggal <?php print((!empty($surat_negosiasi[0]->tgl_surat))?tgl_indo($surat_negosiasi[0]->tgl_surat):''); ?>, Klarifikasi dilakukan terhadap 1 (satu) penawar yang responsive, dengan hasil sebagai berikut :
                            <taBle WIDTH=649 CELLPADDING=7 CELLSPACING=0>
                                <tr>
                                    <tD WIDTH=85 ALIGN=CENTER sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in; font-size:14pt">
                                        <b>NO</b>
                                    </td>
                                    <td WIDTH=297 ALIGN=CENTER sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in; font-size:14pt">
                                        <b>NAMA PERUSAHAAN</b>
                                    </Td>
                                    <td WIDTH=156 ALIGN=CENTER StYLe="border: 1px solid #000000; padding: 0in 0.08in; font-size:14pt">
                                        <b>NILAI PENAWARAN</B>
                                    </Td>
                                    <td WIDTH=156 ALIGN=CENTER StYLe="border: 1px solid #000000; padding: 0in 0.08in; font-size:14pt">
                                        <b>NILAI NEGOSIASI</B>
                                    </Td>
                                    <td WIDTH=156 ALIGN=CENTER StYLe="border: 1px solid #000000; padding: 0in 0.08in; font-size:14pt">
                                        <b>HASIL KUALIFIKASI</B>
                                    </Td>
                                </tr>
                                <tR VALIGN=TOP>
                                    <tD WIDTH=85 ALIGN=CENTER sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in; font-size:14pt">
                                        1
                                    </td>
                                    <td WIDTH=297 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in; font-size:14pt">
                                        <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama); ?>
                                    </Td>
                                    <td WIDTH=156 ALIGN=CENTER StYLe="border: 1px solid #000000; padding: 0in 0.08in; font-size:14pt">
                                        <?php print('Rp '.format_money($kontrak_pihak_ketiga[0]->nilai_kontrak).',-'); ?>
                                    </Td>
                                    <td WIDTH=156 ALIGN=CENTER StYLe="border: 1px solid #000000; padding: 0in 0.08in; font-size:14pt">
                                        <?php print('Rp '.format_money($kontrak_pekerjaan->harga_negosiasi).',-'); ?>
                                    </Td>
                                    <td WIDTH=156 ALIGN=CENTER StYLe="border: 1px solid #000000; padding: 0in 0.08in; font-size:14pt">
                                        LULUS
                                    </Td>
                                </Tr>
                            </taBle>
                    </li>

                </ul>
            </li>

        </ol>
    </p>
    
    <p cLAss=western ALIGN="justify" stylE="margin-bottom: 0in">
        Demikian Berita Acara Penyusunan Hasil Pengadaan (BAHPL) Pekerjaan <?php print($kontrak_pekerjaan->aktivitas_nama); ?> ini dibuat dengan sebenarnya, d itandatangani oleh Pejabat Pengadaan, untuk digunakan sebagaimana mestinya.
    </p>

    <p></p>
    <p></p>
    <table cellpadding="2">
        <tr>
            <td width="600" class="text">&nbsp;</td>
            <td align="center" width="400" style="font-size:16pt">
                <?php print($pejabat_pengadaan->jenis_pejabat_pengadaan_nama); ?> <?php print($pejabat_pengadaan->seri_pejabat_pengadaan_nama); ?><br/>
                Pada Dinas Pekerjaan Umum Kota Semarang<br/>
                Tahun Anggaran <?php print(date("Y"));?><br/>
                <br/><br/><br/><br/><br/>
                <u><?php print($pejabat_pengadaan->pegawai_nama); ?></u><br/>
                NIP. <?php print($pejabat_pengadaan->pegawai_nip); ?>
            </td>
        </tr>
    </table>
    <!-- /Berita Acara Hasil Pengadaan Langsung -->

    <p style="page-break-before: always"></p>

    <!-- Dokumen Penetapan Pemenang -->
    <?php
    $kontrak_surat  = $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'DPPemenang');
    $kontrak_surat  = $kontrak_surat[0];
    $bahpl          = $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'BHPL');     
    
    $dayname    = dayname(date('l', strtotime($kontrak_surat->tgl_surat)));
    $tglindo    = tgl_indo($kontrak_surat->tgl_surat);
    $split_tgl  = explode(' ', $tglindo);       
    ?>

    <p clAss="western" ALIGN=CENTER stYLe="margin-bottom: 0in; page-break-before: none">
        <u><b>PENETAPAN PEMENANG</b></U><br/>
        Nomor : <?php print($kontrak_surat->no_surat);?>
    </P>
    <p></p>

    <p cLaSs=western ALIGN=JUSTIFY style="margin-bottom: 0in">
        <?php
        $no_bahpl = NULL;
        $tgl_bahpl = 'Kosong';
        if(!empty($bahpl)) {
            $no_bahpl = $bahpl[0]->no_surat;
            $tgl_bahpl = $bahpl[0]->tgl_surat;
        }
        ?>

        Berdasarkan BAHPL Nomor <?php print($no_bahpl); ?> tanggal <?php print($tgl_bahpl!='Kosong'?tgl_indo($tgl_bahpl):$tgl_bahpl); ?>, Pejabat Pengadaan pada Dinas Pekerjaan Umum Kota Semarang Tahun Anggaran <?php print(date("Y")); ?> Keputusan Kepala Dinas Pekerjaan Umum Kota Semarang Selaku Pengguna Anggaran Nomor <?php print($pejabat_pengadaan->sk_no); ?> tanggal <?php print(tgl_indo($pejabat_pengadaan->sk_tgl_penetapan)); ?> tentang Penunjukan Pejabat Pengadaan Pada Dinas Pekerjaan Umum Kota Semarang Tahun Anggaran <?php print($pejabat_pengadaan->sk_tahun); ?> untuk :
    </p>
    <p></p>
    <table align="center" cellpadding="2">
        <tr>
            <td class="text" style="font-size:12pt">Program</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pekerjaan->program_nama); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Kegiatan</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pekerjaan->pekerjaan_nama); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Pekerjaan</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pekerjaan->aktivitas_nama); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Sumber Dana</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pekerjaan->aktivitas_sumber_dana); ?></td>
        </tr>
    </table>
    <p></p>
    <p cLaSs=western ALIGN="center" style="margin-bottom: 0in">
        <strong>Menetapkan :</strong>
    </p>
    <p></p>
    <table align="center" cellpadding="2">
        <tr>
            <td class="text" style="font-size:12pt">Nama Perusahaan</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Alamat</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_alamat); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">NPWP</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_npwp); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Harga Penawaran Terkoreksi</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pekerjaan->harga_negosiasi); ?></td>
        </tr>
    </table>

    <p cLaSs=western ALIGN=JUSTIFY style="margin-bottom: 0in">
        Demikian penetapan ini untuk diketahui dan dipergunakan sebagaimana mestinya.
    </p>
    <p></p>
    <p></p>
    <table>
        <tr>
            <td width="250"></td>
            <td width="250"></td>
            <td width="500" align="center" style="font-size:16pt">
                <?php print($pejabat_pengadaan->jenis_pejabat_pengadaan_nama); ?> <?php print($pejabat_pengadaan->seri_pejabat_pengadaan_nama); ?><br/>
                Pada Dinas Pekerjaan Umum Kota Semarang<br/>
                Tahun Anggaran <?php print(date("Y"));?><br/>
                <br/><br/><br/><br/><br/>
                <u><?php print($pejabat_pengadaan->pegawai_nama); ?></u><br/>
                NIP. <?php print($pejabat_pengadaan->pegawai_nip); ?>
            </td>
        </tr>
    </table>
    
    <p></p>
    <p></p>
    <p ClaSs=western StYLe="margin-bottom: 0in">
        Tembusan : <br/>
        <ol type="1">
            <li>Kuasa Pengguna Anggaran selaku Pejabat Pembuat Komitmen</li>
            <li>Arsip</li>
        </ol>
    </p>
    
    
    <!-- /Dokumen Penetapan Pemenang -->

    <p style="page-break-before: always"></p>

    <!-- Dokumen Penyampaian Pemenang -->
    <?php
    $kontrak_surat  = $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'DSampaiP');
    $kontrak_surat  = $kontrak_surat[0];
    ?>

    <p clasS=western ALIGN=CENTER>
        <u><strong>PENYAMPAIAN HASIL PENETAPAN PEMENANG</strong></u><br/>
        Nomor : <?php print($kontrak_surat->no_surat); ?>
    </p>
    <p></p>
    <p></p>
    <p clasS=western ALIGN="justify">
        Sehubungan dengan Pelaksanaan Pengadaan Langsung :
    </p>
    <p></p>
    <table cellpadding="2">
        <tr>
            <td class="text" style="font-size:12pt">Program</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pekerjaan->program_nama); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Kegiatan</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pekerjaan->pekerjaan_nama); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Pekerjaan</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pekerjaan->aktivitas_nama); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Sumber Dana</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pekerjaan->aktivitas_sumber_dana); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Tempat</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_surat->tempat_rapat_nama.' '.$kontrak_surat->tempat_rapat_alamat); ?></td>
        </tr>
    </table>
    <p></p>
    <p clAss=western ALIGN="justify" StylE="margin-bottom: 0in">
        Dengan ini memberitahukan bahwa setelah diadakan penelitian oleh Pejabat Pengadaan Kegiatan pada Dinas Pekerjaan Umum Kota Semarang Sumber Dana APBD Tahun Anggaran <?php print(date("Y")); ?>, sesuai dengan ketentuan-ketentuan yang berlaku dalam Peraturan Presiden Republik Indonesia Nomor 4 Tahun 2015 tentang Perubahan keempat atas Peraturan Presiden Nomor 54 Tahun 2010 tentang Pengadaan Barang/Jasa Pemerintah, dengan ini diumumkan Pemenang Pengadaan Langsung tersebut di atas adalah :
    </p>
    <p></p>
    <table cellpadding="2">
        <tr>
            <td class="text" style="font-size:12pt">Nama Perusahaan</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Alamat</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_alamat); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">NPWP</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_npwp); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt" valign="top">Harga Penawaran</td>
            <td class="text" style="font-size:12pt" valign="top">:</td>
            <td class="text" style="font-size:12pt">
                <?php print('Rp '.format_money($kontrak_pihak_ketiga[0]->nilai_kontrak).',-'); ?><br/>
                (<?php print(ucwords(strtolower(terbilang($kontrak_pihak_ketiga[0]->nilai_kontrak))).' Rupiah'); ?>)
            </td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt" valign="top">Harga Negosiasi</td>
            <td class="text" style="font-size:12pt" valign="top">:</td>
            <td class="text" style="font-size:12pt">
                <?php print('Rp '.format_money($kontrak_pekerjaan->harga_negosiasi).',-'); ?><br/>
                (<?php print(ucwords(strtolower(terbilang($kontrak_pekerjaan->harga_negosiasi))).' Rupiah'); ?>)
            </td>
        </tr>
    </table>
    <p cLaSs="western" ALIGN="justify" StYLe="margin-bottom: 0in">
        Demikian pengumuman ini agar para calon penyedia / penawa r Barang / Jasa maklum.
    </p>
    <p></p>
    <p></p>
    <table>
        <tr>
            <td width="250"></td>
            <td width="250"></td>
            <td width="500" align="center" style="font-size:16pt">
                <?php print($pejabat_pengadaan->jenis_pejabat_pengadaan_nama); ?> <?php print($pejabat_pengadaan->seri_pejabat_pengadaan_nama); ?><br/>
                Pada Dinas Pekerjaan Umum Kota Semarang<br/>
                Tahun Anggaran <?php print(date("Y"));?><br/>
                <br/><br/><br/><br/><br/>
                <u><?php print($pejabat_pengadaan->pegawai_nama); ?></u><br/>
                NIP. <?php print($pejabat_pengadaan->pegawai_nip); ?>
            </td>
        </tr>
    </table>
    <p></p>
    <p></p>
    <p ClaSs=western StYLe="margin-bottom: 0in">
        Tembusan Yth : <br/>
        <ol type="i">
            <li>Kuasa Pengguna Anggaran selaku Pejabat Pembuat Komitmen</li>
            <li>Arsip</li>
        </ol>
    </p>
    <!-- /Dokumen Penyampaian Pemenang -->
    
    <p style="page-break-before: always"></p>

    <!-- Surat Penunjukan Penyedia -->
    <?php
    $kontrak_surat  = $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'SPVendor');
    $kontrak_surat  = $kontrak_surat[0];
    $surat_penawaran = $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'UPengadaan');
    $surat_penawaran = $surat_penawaran[0];
    $surat_kegiatan_penawaran =  $controller->get_jadwal_kegiatan_kontrak($surat_penawaran->id, 'evaluasi');
    $surat_penawaran = $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'UPengadaan');
    ?>

    <p>
        <Img SRc="<?php print(base_url('assets/template_surat/img/kop_surat_dpu_monokrom.png'));?>" nAme="Picture 8" width="100%" ALIGN=center BORDER=0>
    </p>
    <table>
        <tr>
            <td width="650">
                <table>
                    <tr>
                        <td style="font-size:16pt">Nomor</td>
                        <td style="font-size:16pt">:</td>
                        <td style="font-size:16pt"><?php print($kontrak_surat->no_surat); ?></td>
                    </tr>
                    <tr>
                        <td style="font-size:16pt">Lampiran</td>
                        <td style="font-size:16pt">:</td>
                        <td style="font-size:16pt">-</td>
                    </tr>
                </table>
            </td>
            <td width="350" align="right" valign="center" style="font-size:16pt">
                Semarang, <?php print(tgl_indo($kontrak_surat->tgl_surat)); ?>
            </td>
        </tr>
    </table>
    <p></p>
    <p cLaSs="western" ALIGN="justify" StYLe="margin-bottom: 0in; width:500;">
        Semarang, <?php print(tgl_indo($kontrak_surat->tgl_surat)); ?><br/>
        Yth. <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama); ?><br/>
        <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_alamat); ?><br/>
        di -<br/>
        Tempat
    </p>
    <p></p>
    <p cLaSs="western" ALIGN="justify" StYLe="margin-bottom: 0in;">
        Perihal : Penunjukan Penyedia untuk Pelaksanaan Paket Pekerjaan <?php print(ucwords($kontrak_pekerjaan->pekerjaan_nama));?>
    </p>
    <p></p>
    <p cLaSs="western" ALIGN="justify" StYLe="margin-bottom: 0in;">
    Dengan ini kami beritahukan bahwa penawaran Saudara <?php print((!empty($surat_penawaran[0]->no_surat))?$surat_penawaran[0]->no_surat:''); ?> tanggal 
        <?php 
            if(!empty($surat_kegiatan_penawaran->tgl)) {
                print(tgl_indo($surat_kegiatan_penawaran->tgl));
            } else {
                print(NULL);
            }
        ?>
         perihal Penawaran <?php print($kontrak_pekerjaan->pekerjaan_nama); ?>, pada kegiatan <?php print($kontrak_pekerjaan->aktivitas_nama); ?> dengan nilai penawaran/penawaran terkoreksi sebesar <?php print('Rp '.format_money($kontrak_pekerjaan->harga_negosiasi).',-'); ?> (<?php print(ucwords(strtolower(terbilang($kontrak_pekerjaan->harga_negosiasi))).' Rupiah'); ?>) kami nyatakan diterima/disetujui.<br/> 
        Sebagai tindak lanjut dari Surat Penunjukan Penyedia/Jasa (SPPBJ) ini Saudara diharuskan untuk menandatangani Surat Perjanjian paling lambat 14 (empat belas) hari kerja setelah diterbitkannya SPPBJ. Kegagalan Saudara untuk menerima penunjukan ini yang disusun berdasarkan evaluasi terhadap penawaran Saudara, akan dikenakan sanksi sesuai ketentuan dalam Peraturan Presiden Republik Indonesia Nomor 4 Tahun 201 5 tentang Perubahan keempat atas Peraturan Presiden Nomor 54 Tahun 2010 tentang Pengadaan Barang/Jasa Pemerintah .
    </p>
    <p></p>
    <table>
        <tr>
            <td width="500" align="left" style="font-size:16pt">
                A.n. Kepala Dinas Pekerjaan Umum<br/>
                Kota Semarang<br/>
                Kuasa Pengguna Anggaran<br/>
                Selaku Pejabat Pembuat Komitmen<br/>
                Tahun Anggaran <?php print(date("Y"));?><br/>
                <br/><br/><br/><br/><br/>
                <u><?php print((!empty($pejabat_pembuat_komitmen->pegawai_nama))?$pejabat_pembuat_komitmen->pegawai_nama:''); ?></u><br/>
                NIP. <?php print((!empty($pejabat_pembuat_komitmen->pegawai_nip))?$pejabat_pembuat_komitmen->pegawai_nip:''); ?>
            </td>
            <td width="250"></td>
            <td width="250"></td>
        </tr>
    </table>
    <p></p>
    <p></p>
    <p ClaSs=western StYLe="margin-bottom: 0in">
        Tembusan Yth : <br/>
        <ol type="i">
            <li>Pengguna Anggaran Dinas Pekerjaan Umum Kota Smarang;</li>
            <li>Inspektur Kota Semarag;</li>
            <li>Ka. DPKAD Kota Semarang;</li>
            <li>Ka. Bagian Pembangunan Kota Semarang;</li>
            <li>Ka. Bagian Hukum Kota Semarang;</li>
            <li>Pejabat Pengadaan ybs;</li>
            <li>Pertinggal;</li>
        </ol>
    </p>
    <!-- /Surat Penunjukan Penyedia -->

    <p style="page-break-before: always"></p>
    
    <!-- Surat Perintah Kerja -->
    <?php
    $kontrak_surat  = $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'SPK');
    $kontrak_surat  = $kontrak_surat[0];
    $bahpl          =  $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'BHPL');
    $surat_undangan =  $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'UPengadaan');
    ?>

    <p>
        <Img SRc="<?php print(base_url('assets/template_surat/img/kop_surat_dpu_monokrom.png'));?>" nAme="Picture 8" width="100%" ALIGN=center BORDER=0>
    </p>
    <p></p>
    <table CELLPADDING="7" cellspacing="0">
        <tr>
            <td align="center" valign="middle" rowspan="2" width="400" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                <strong>SURAT PERINTAH KERJA</strong>
            </td>
            <td width="600" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                <strong>DINAS PEKERJAAN UMUM KOTA SEMARANG</strong>
            </td>
        </tr>
        <tr>
            <td width="600" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                Nomor : <br/>
                Tanggal : </br/>
            </td>
        </tr>
        <tr>
            <td align="center" valign="middle" rowspan="2" width="400" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                <strong>PAKET PEKERJAAN :</strong><br/>
                <?php print($kontrak_pekerjaan->aktivitas_nama); ?>
            </td>
            <td valign="center" width="400" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                <?php
                $no_surat_undangan  = NULL;
                $tgl_surat_undangan = 'Kosong';
                if(!empty($surat_undangan)) {
                    $no_surat_undangan = $surat_undangan[0]->no_surat;
                    $tgl_surat_undangan = $surat_undangan[0]->tgl_surat;
                }
                ?>
                NOMOR DAN TANGGAL SURAT UNDANGAN PENGADAAN LANGSUNG <?php print($no_surat_undangan); ?>, <?php print($tgl_surat_undangan!='Kosong'?tgl_indo($tgl_surat_undangan):$tgl_surat_undangan); ?>
            </td>
        </tr>
        <tr>
            <td valign="center" width="400" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
            <?php
            $no_bahpl   = NULL;
            $tgl_bahpl  = 'Kosong';
            if(!empty($bahpl)) {
                $no_bahpl = $bahpl[0]->no_surat;
                $tgl_bahpl = $bahpl[0]->tgl_surat;
            }
            ?>
            NOMOR DAN TANGGAL, BERITA ACARA HASIL PENGADAAN LANGSUNG <?php print($no_bahpl); ?>, <?php print($tgl_bahpl!='Kosong'?tgl_indo($tgl_bahpl):$tgl_bahpl); ?>
            </td>
        </tr>
        <tr>
            <td valign="center" align="justify" colspan="2" width="400" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                <strong>SUMBER DANA : </strong><br/>
                <?php print($kontrak_pekerjaan->aktivitas_sumber_dana); ?> untuk mata anggaran kegiatan <?php print(ucwords(strtolower($kontrak_pekerjaan->pekerjaan_nama))); ?>
            </td>
        </tr>
        <tr>
            <td valign="center" align="justify" colspan="2" width="400" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                <strong>WAKTU PELAKSANAAN PEKERJAAN : </strong><br/>
                <?php print($kontrak_pekerjaan->durasi_kontrak); ?> (<?php print(terbilang($kontrak_pekerjaan->durasi_kontrak)); ?>) hari kalender <?php print(tgl_indo($kontrak_pekerjaan->tgl_awal_kontrak)); ?>
            </td>
        </tr>
        <tr>
            <td valign="middle" align="justify" colspan="2" width="400" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                
                <table CELLPADDING="7" cellspacing="0" align="right">
                    <tr>
                        <td width="80" valign="middle" align="justify" colspan="2" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:8pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                            Paraf PPKOM
                        </td>
                        <td width="80" valign="middle" align="justify" colspan="2" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:8pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">

                        </td>
                    </tr>
                    <tr>
                        <td width="80" valign="middle" align="justify" colspan="2" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:8pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                            Paraf Penyedia
                        </td>
                        <td width="80" valign="middle" align="justify" colspan="2" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:8pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">

                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
    <p></p>
    <p></p>
    <!-- /Surat Perintah Kerja -->
</BODY>

</HTML>