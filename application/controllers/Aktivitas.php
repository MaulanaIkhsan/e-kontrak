<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Aktivitas extends CI_Controller {

    // ----- aktivitas -----
    // id
	// nama_aktivitas
	// no_rekening
	// sumber_dana
    // anggaran_awal
    // perubahan_anggaran
    // hps
    // nilai_kontrak
    // tgl_pengajuan_pencairan
    // tgl_pencairan
    // keterangan
    // pptk
    // ppkom
    // bendahara
    // pekerjaan_id
    // pengadaan_id
    // jenis_pekerjaan_id
    // created_at
    // updated_at
    // created_by
	// updated_by
	
	var $jenis_pekerjaan 	= ['Fisik', 'Jasa Konsultan', 
								'Jasa Lainnya', 'Pengadaan Barang'],
		$jenis_pengadaan 	= ['Lelang', 'Pengadaan Langsung'],
		$status_kontrak 	= ['Pembukaan', 'Penawaran', 
								'Penetepan', 'Penunjukan'];
	
	var $conf_upload = array(
		'allowed_types' => "csv",
		'max_size'      => '2000',
		'encrypt_name'  => FALSE
	);
        
    public function __construct()
	{
		parent::__construct();
		
        $sess_data = $this->session->userdata('session_data');
		if($sess_data===NULL) {
            $this->session->set_flashdata('warning', '<div class="alert alert-warning fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a> 
                Harap login terlebih dahulu untuk mengakses sistem</div>');
			redirect('auth');
        }
		
		//Load Dependencies
		$this->load->model('aktivitas_model', 'aktivitas');
		$this->load->helper('generic');
	}

	private function _validate($is_update=NULL, $id=NULL)
	{
        $this->load->helper('generic');
        
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
	
		if(empty($_FILES))
        {
            // Re-initialize data
            $data = array();
		    $data['error_string'] = array();
            $data['inputerror'] = array();
            
            $data['inputerror'][] = 'csvfile';
            $data['error_string'][] = 'Dokumen Harap berformat PDF*';
            $data['status'] = FALSE;
        }
		elseif($_FILES['csvfile']['name']!='')
		{
            $nama_dok = $_FILES['csvfile']['name'];
            $split = explode('.', $nama_dok);
            $ekstensi = strtolower($split[1]);

            //'2000000' as same as 2MB
            if($_FILES['csvfile']['size']>2000000) {
                $data['inputerror'][] = 'csvfile';
                $data['error_string'][] = 'CSV dokumen melebihi 2MB';
                $data['status'] = FALSE;
            }
            else if(strpos($this->conf_upload['allowed_types'], $ekstensi)===FALSE) {
                $data['inputerror'][] = 'csvfile';
                $data['error_string'][] = 'Format dokumen harus CSV';
                $data['status'] = FALSE;
            }
		}
		
		if($data['status'] === FALSE)
		{
			echo json_encode((object)$data);
			exit();
		}
	}

	public function format_date($date_input, $format)
	{
		$date = date_create($date_input);
		if($format == 1){
			return date_format($date, 'Y-m-d');
		}
		elseif($format == 2) {
			return date_format($date, 'd-M-Y');
		}
		else {
			return FALSE;
		}
	}

	// List all your items
	public function index()
	{
		$this->session->unset_userdata('id_pekerjaan');
		$this->session->unset_userdata('data_kegiatan');
		$this->load->view('aktivitas/resume');
	}

	public function kegiatan()
	{
		$this->session->unset_userdata('id_pekerjaan');
		$this->session->set_userdata('data_kegiatan', TRUE);
		$this->load->view('aktivitas/data_kegiatan');
	}

	public function get_realisasi($kontrak_id)
	{
		$this->load->model('Realisasi_model', 'realisasi');
		return $this->realisasi->get_realisasi_by_kontrak_all($kontrak_id);
	}

	// Add a new item
	public function add()
	{
		$sess_data = $this->session->userdata('session_data');

		$this->load->model('Pegawai_model', 'pegawai');
		$this->load->model('Jenis_pengadaan_model', 'pengadaan');
		$this->load->model('Jenis_pekerjaan_model', 'tipe_pekerjaan');
		$this->load->model('Program_model', 'program');
		$this->load->model('Pekerjaan_model', 'pekerjaan');
		$this->load->model('Pejabat_pembuat_komitmen_model', 'pejabat_pembuat_komitmen');

		$this->load->library('form_validation');
		$this->form_validation->set_rules('kegiatan', 'Nama Kegiatan','trim');
		$this->form_validation->set_rules('nama', 'Nama Pekerjaan','required|trim');
		$this->form_validation->set_rules('no_rekening', 'No Rekening','required|trim');
		$this->form_validation->set_rules('sumber_dana', 'Sumber Dana','required|trim');
		$this->form_validation->set_rules('ppkom', 'PPKOM', 'trim');
		$this->form_validation->set_rules('pptk', 'PPTK', 'required|trim');
		$this->form_validation->set_rules('bendahara', 'Bendahara','trim');
		$this->form_validation->set_rules('anggaran_awal', 'Pagu Anggaran','required|trim');
		$this->form_validation->set_rules('perubahan_anggaran', 'Perubahan_Anggaran','trim');
		$this->form_validation->set_rules('hps', 'HPS','trim');
		$this->form_validation->set_rules('nilai_kontrak', 'Nilai Kontrak','trim');
		$this->form_validation->set_rules('tgl_pengajuan_pencairan', 'Nilai Kontrak','trim');
		$this->form_validation->set_rules('tgl_pencairan', 'Nilai Kontrak','trim');
		$this->form_validation->set_rules('jenis_pengadaan', 'Metode Pengadaan','required|trim');
		$this->form_validation->set_rules('jenis_pekerjaan', 'Jenis Pekerjaan','required');
		$this->form_validation->set_rules('keterangan', 'Keterangan','trim');

		$pekerjaan_id = $this->session->userdata('id_pekerjaan');
		// $get_pekerjaan = $this->pekerjaan->get_nama_pekerjaan($pekerjaan_id);

        $sess_data = $this->session->userdata('session_data');
		$user_id = $sess_data['id'];
		$role = $sess_data['role'];

		if ($this->form_validation->run() == FALSE) {
			// load view form add pekerjaan
			$value['user'] 				= $this->pegawai->get_nama_pegawai();
			$value['jenis_pengadaan'] 	= $this->pengadaan->get_jenis_pengadaan();
			$value['jenis_pekerjaan'] 	= $this->tipe_pekerjaan->get_jenis_pekerjaan();
			$value['pekerjaan'] 		= $this->pekerjaan->get_nama_pekerjaan($pekerjaan_id);
			$value['jabatan'] 			= $this->session->userdata('jabatan');
			$value['ppkom']				= $this->pejabat_pembuat_komitmen->fetch_data(NULL, $value['pekerjaan']->tahun, NULL);
			
			$this->load->view('aktivitas/add', $value);
		}
		else {
			if($this->input->post('simpan')!==NULL) {
				$curdate = date("d-M-Y");

				$data['nama_aktivitas'] = $this->input->post('nama');
				$data['no_rekening']	= $this->input->post('no_rekening');
				$data['sumber_dana']	= $this->input->post('sumber_dana');
				$data['anggaran_awal'] = $this->input->post('anggaran_awal');
				$data['perubahan_anggaran'] = $this->input->post('perubahan_anggaran');
				if($data['perubahan_anggaran']==NULL){
					$data['perubahan_anggaran'] = $data['anggaran_awal'];
				}
				$data['hps'] = $this->input->post('hps');
				$data['nilai_kontrak'] = $this->input->post('nilai_kontrak');
				
				$data['tgl_pengajuan_pencairan'] = $this->input->post('tgl_pengajuan_pencairan');
				$data['tgl_pencairan'] = $this->input->post('tgl_pencairan');
				
				if($data['tgl_pengajuan_pencairan']==NULL){
					$data['tgl_pengajuan_pencairan'] = NULL;
				}
				else {
					$data['tgl_pengajuan_pencairan'] = $this->format_date($this->input->post('tgl_pengajuan_pencairan'), 1);
				}

				if($data['tgl_pencairan']==NULL){
					$data['tgl_pencairan'] = NULL;
				}
				else {
					$data['tgl_pencairan'] = $this->format_date($this->input->post('tgl_pencairan'), 1);
				}
				
				$data['pekerjaan_id'] = $pekerjaan_id;	
				$data['pengadaan_id'] = $this->input->post('jenis_pengadaan');
				$data['jenis_pekerjaan_id'] = $this->input->post('jenis_pekerjaan');
				$data['created_by'] = $user_id;
				$data['updated_by'] = $user_id;
				$data['keterangan'] = $this->input->post('keterangan');

				// Set value data PPTK, PPKOM and Bendahara based on role of users Pegawai
				if($sess_data['role']=='Staff') {
					$data['pptk'] = $sess_data['id'];
				}
				else {
					$data['ppkom'] = $this->input->post('ppkom');
					$data['pptk'] = $this->input->post('pptk');
					$data['bendahara'] = $this->input->post('bendahara');

					if($data['bendahara']==$data['pptk']) {
						$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> PPTK dan Bendahara tidak boleh sama. Mohon Input kembali</div>');
						redirect('pekerjaan/detail/'.$this->session->userdata('id_pekerjaan'));
					}
				}

				$add = $this->aktivitas->insert($data);

				if($add==='ok') {					
					$this->session->set_flashdata('success', '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Data pekerjaan berhasil tersimpan</div>');
					redirect('pekerjaan/detail/'.$this->session->userdata('id_pekerjaan'));
				}
				else {
					$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Data pekerjaan gagal tersimpan. Mohon cek kembali</div>');
					redirect('pekerjaan/detail/'.$this->session->userdata('id_pekerjaan'));
				}
			}
			else {
				$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Harap masukkan data pekerjaan melalui tombol <strong>Tambah Baru</strong> !</div>');
				redirect('pekerjaan/detail/'.$this->session->userdata('id_pekerjaan'));
			}	
			
		}
	}

	//Update one item
	public function update($id)
	{
		$sess_data = $this->session->userdata('session_data');

		$this->load->model('Pegawai_model', 'pegawai');
		$this->load->model('Jenis_pengadaan_model', 'pengadaan');
		$this->load->model('Jenis_pekerjaan_model', 'tipe_pekerjaan');
		$this->load->model('Program_model', 'program');
		$this->load->model('Pekerjaan_model', 'pekerjaan');
		$this->load->model('Pejabat_pembuat_komitmen_model', 'pejabat_pembuat_komitmen');

		$this->load->library('form_validation');
		$this->form_validation->set_rules('kegiatan', 'Nama Kegiatan','trim');
		$this->form_validation->set_rules('nama', 'Nama Pekerjaan','required|trim');
		$this->form_validation->set_rules('no_rekening', 'No Rekening','required|trim');
		$this->form_validation->set_rules('sumber_dana', 'Sumber Dana','required|trim');
		$this->form_validation->set_rules('ppkom', 'PPKOM', 'trim');
		$this->form_validation->set_rules('pptk', 'PPTK', 'required|trim');
		$this->form_validation->set_rules('bendahara', 'Bendahara','trim');
		$this->form_validation->set_rules('anggaran_awal', 'Pagu Anggaran','required|trim');
		$this->form_validation->set_rules('perubahan_anggaran', 'Perubahan_Anggaran','trim');
		$this->form_validation->set_rules('hps', 'HPS','trim');
		$this->form_validation->set_rules('nilai_kontrak', 'Nilai Kontrak','trim');
		$this->form_validation->set_rules('tgl_pengajuan_pencairan', 'Nilai Kontrak','trim');
		$this->form_validation->set_rules('tgl_pencairan', 'Nilai Kontrak','trim');
		$this->form_validation->set_rules('jenis_pengadaan', 'Metode Pengadaan','required|trim');
		$this->form_validation->set_rules('jenis_pekerjaan', 'Jenis Pekerjaan','required');
		$this->form_validation->set_rules('keterangan', 'Keterangan','trim');

		$pekerjaan_id = $this->session->userdata('id_pekerjaan');
		

		$user_id = $this->session->userdata('id');
		$role = $this->session->userdata('role');

		if ($this->form_validation->run() == FALSE) {
			// load view form add pekerjaan
			$value['user'] 				= $this->pegawai->get_nama_pegawai();
			$value['jenis_pengadaan'] 	= $this->pengadaan->get_jenis_pengadaan();
			$value['jenis_pekerjaan'] 	= $this->tipe_pekerjaan->get_jenis_pekerjaan();
			$value['aktivitas'] 		= $this->aktivitas->show_update($id);
			
			$pekerjaan = $this->pekerjaan->detail($value['aktivitas']->pekerjaan_id);
			$value['ppkom']				= $this->pejabat_pembuat_komitmen->fetch_data(NULL, $pekerjaan->tahun, NULL);
			
			$value['controller'] 		= $this;

			$this->load->view('aktivitas/update', $value);
		}
		else {
			if($this->input->post('simpan')!==NULL) {
				$curdate = date("d-M-Y");

				$data['nama_aktivitas'] = $this->input->post('nama');
				$data['no_rekening']	= $this->input->post('no_rekening');
				$data['sumber_dana']	= $this->input->post('sumber_dana');
				$data['anggaran_awal'] = $this->input->post('anggaran_awal');
				$data['perubahan_anggaran'] = $this->input->post('perubahan_anggaran');
				if($data['perubahan_anggaran']==NULL){
					$data['perubahan_anggaran'] = $data['anggaran_awal'];
				}
				$data['hps'] = $this->input->post('hps');
				$data['nilai_kontrak'] = $this->input->post('nilai_kontrak');
				
				$data['tgl_pengajuan_pencairan'] = $this->input->post('tgl_pengajuan_pencairan');
				$data['tgl_pencairan'] = $this->input->post('tgl_pencairan');
				
				if($data['tgl_pengajuan_pencairan']==NULL){
					$data['tgl_pengajuan_pencairan'] = NULL;
				}
				else {
					$data['tgl_pengajuan_pencairan'] = $this->format_date($this->input->post('tgl_pengajuan_pencairan'), 1);
				}

				if($data['tgl_pencairan']==NULL){
					$data['tgl_pencairan'] = NULL;
				}
				else {
					$data['tgl_pencairan'] = $this->format_date($this->input->post('tgl_pencairan'), 1);
				}
				
				if(!empty($pekerjaan_id)) {
					$data['pekerjaan_id'] = $pekerjaan_id;
				}
					
				$data['pengadaan_id'] = $this->input->post('jenis_pengadaan');
				$data['jenis_pekerjaan_id'] = $this->input->post('jenis_pekerjaan');
				$data['updated_by'] = $user_id;
				$data['keterangan'] = $this->input->post('keterangan');
				$data['ppkom'] = $this->input->post('ppkom');
				$data['bendahara'] = $this->input->post('bendahara');

				// Set value data PPTK, PPKOM and Bendahara based on role of users Pegawai
				if($sess_data['role']=='Staff') {
					$data['pptk'] = $sess_data['id'];
				}
				else {
					$data['pptk'] = $this->input->post('pptk');
				}

				if($data['bendahara']==$data['pptk']) {
					$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> PPTK dan Bendahara tidak boleh sama. Mohon Input kembali</div>');
					redirect('pekerjaan/detail/'.$this->session->userdata('id_pekerjaan'));
				}

				$key['id'] = $id;

				$edit = $this->aktivitas->update($data, $key);

				if($edit==='ok') {
					$this->session->set_flashdata('success', '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Data pekerjaan berhasil diubah</div>');
					redirect('aktivitas/detail/'.$id);
				}
				else {
					$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Data pekerjaan gagal diubah. Mohon cek kembali</div>');
					redirect('aktivitas/detail/'.$id);
				}
			}
			else {
				$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Harap masukkan data pekerjaan melalui tombol <strong>Tambah Baru</strong> !</div>');
				redirect('aktivitas/detail/'.$id);
			}

		}
	}

	//Delete one item
	public function delete( $id)
	{
		if($this->input->post('delete')!==NULL) {
			$key['id'] = $id;
			$delete = $this->aktivitas->delete($key);

			if($delete==='ok'){
				$this->session->set_flashdata('success', '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Data pekerjaan  berhasil terhapus </div>');
				if($this->session->userdata('id_pekerjaan')!=NULL) {
					redirect('pekerjaan/detail/'.$this->session->userdata('id_pekerjaan'));
				}
				else {
					redirect('aktivitas');
				}
			}
			else {
				$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Data pekerjaan gagal dihapus. Harap cek kembali </div>');
				redirect('aktivitas/detail/'.$id);
			}
		}
		else {
			$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Harap lakukan hapus data pekerjaan melalui tombol <strong>Hapus</strong> !</div>');
			if($this->session->userdata('id_pekerjaan')!=NULL) {
				redirect('pekerjaan/detail/'.$this->session->userdata('id_pekerjaan'));
			}
			else {
				redirect('aktivitas');
			}
		}
	}

	public function detail($id)
	{
		$this->load->model('Pejabat_pengadaan_model', 'pejabat_pengadaan');
		$this->load->model('Pegawai_model', 'pegawai');
		$this->load->model('Kontrak_pekerjaan_model', 'kontrak_pekerjaan');
		$this->load->model('Pejabat_pembuat_komitmen_model', 'pejabat_pembuat_komitmen');
		
		$data['data_kegiatan'] = FALSE;
		
		if($this->session->userdata('data_kegiatan')==TRUE) {
			$data['data_kegiatan'] = TRUE;
		}
		$this->session->unset_userdata('data_kegiatan');

		$this->session->set_userdata('id_aktivitas', $id);
		
		$sess_data = $this->session->userdata('session_data');
		$data['detail']	= $this->aktivitas->detail($id);
		$curr_year 		= $data['detail']->pekerjaan_tahun;
		
		$data['controller'] 				= $this;
		$data['jabatan'] 					= $this->session->userdata('jabatan');
		$data['penawaran'] 					= FALSE;
		$data['user'] 						= $this->pegawai->get_nama_pegawai();
		$data['pejabat_pengadaan']			= $this->pejabat_pengadaan->fetch_data(NULL, $curr_year, NULL);
		$data['pejabat_pembuat_komitmen']	= $this->pejabat_pembuat_komitmen->fetch_data(NULL, $curr_year, NULL);
		$data['status_kontrak'] 			= $this->status_kontrak;
		$data['data_kontrak'] 				= FALSE;
		$data['id']							= $id;
		
		$curr_ppkom 				= $data['detail']->ppkom;
		$curr_jenis_pekerjaan 		= $data['detail']->jenis_pekerjaan;
		$curr_jenis_pengadaan		= $data['detail']->jenis_pengadaan;
		$curr_anggaran				= $data['detail']->anggaran_awal;
		$user_id 					= $sess_data['id'];
		$user_nama					= $sess_data['nama'];
		$curr_year 					= date("Y");
		$check_pejabat_pengadaan 	= $this->pejabat_pengadaan->fetch_data(NULL, $curr_year, $user_id);
		$flag  						= 0;
		
		// Validation User
		// if($curr_ppkom==$user_nama or 
		// 	!empty($check_pejabat_pengadaan) or
		// 	$sess_data['role']=='Super Admin') {
		// 	$flag++;
		// 	print_r($check_pejabat_pengadaan);
		// 	die;
		// 	$data['pegawai_id'] = $check_pejabat_pengadaan[0]->pejabat_pengadaan_id;
		// }
		
		// Validation User
		if($sess_data['role']=='Super Admin') {
			$flag++;
		}
		elseif($sess_data['role']!='Super Admin' and ($curr_ppkom==$user_nama or 
		!empty($check_pejabat_pengadaan))) {
			$flag++;
			$data['pejabat_pengadaan'] = $this->pejabat_pengadaan->fetch_data(NULL, $curr_year, $user_id);
		}

		// // Validation Jenis Pekerjaan and Metode Pengadaan
		// if(in_array($curr_jenis_pekerjaan, $this->jenis_pekerjaan) and 
		// 	in_array($curr_jenis_pengadaan, $this->jenis_pengadaan)) {
		// 	$flag++;
		// }

		// // Validate anggaran
		// if($curr_jenis_pekerjaan=='Fisik' or 
		// 	$curr_jenis_pekerjaan=='Pengadaan Barang' and
		// 	$curr_anggaran>=50000000 and
		// 	$curr_anggaran<=200000000){
		// 		$flag++;
		// }
		// elseif($curr_jenis_pekerjaan=='Jasa Konsultan' or 
		// 	$curr_jenis_pekerjaan=='Jasa Lainnya' and 
		// 	$curr_anggaran>=50000000 and
		// 	$curr_anggaran<=100000000) {
		// 		$flag++;
		// }
		// Check data jenis_pekerjaan, metode pengadaan and anggaran awal to open contract
		switch ($curr_jenis_pengadaan) {
			case 'Penunjukan Langsung':
				if(($curr_jenis_pekerjaan=='Fisik' or $curr_jenis_pekerjaan=='Pengadaan Barang')
					and
					($curr_anggaran>=50000000 and $curr_anggaran<=200000000)) {
						$flag++;
				}
				elseif(($curr_jenis_pekerjaan=='Jasa Konsultansi' or $curr_jenis_pekerjaan=='Pengadaan Langsung')
					and
					($curr_anggaran>=50000000 and $curr_anggaran<=100000000)) {
						$flag++;
				}
				else {
					$flag--;
				}
				break;
			case 'Pengadaan Langsung':
				if(($curr_jenis_pekerjaan=='Fisik' or $curr_jenis_pekerjaan=='Pengadaan Barang')
					and
					($curr_anggaran>=50000000 and $curr_anggaran<=200000000)) {
						$flag++;
				}
				elseif(($curr_jenis_pekerjaan=='Jasa Konsultansi' or $curr_jenis_pekerjaan=='Pengadaan Langsung')
					and
					($curr_anggaran>=50000000 and $curr_anggaran<=100000000)) {
						$flag++;
				}
				else {
					$flag--;
				}
				break;
			default:
				$flag--;
				break;
		}

		// print('jenis pekerjaan : '.$curr_jenis_pekerjaan.'<br/>');
		// print('metode pengadaan : '.$curr_jenis_pengadaan.'<br/>');

		if($flag==2) {
			$data['penawaran'] 	= TRUE;
		}
		
		// Check data kontrak pekerjaan based on aktivitas_id
		$check_kontrak_pekerjaan = $this->kontrak_pekerjaan->fetch_data(NULL, $id);
		
		if(!empty($check_kontrak_pekerjaan)) {
			$data['data_kontrak'] 		= TRUE;
		}
		// print($flag);

		$this->load->view('aktivitas/detail', $data);
	}

	public function json_group_value($data_array)
	{
		$this->load->helper('generic');
		
		$column_data = ['tahun'];
		$column_label = array_column($data_array, 'jenis');
		foreach($column_label as $item):
			array_push($column_data, $item);
		endforeach;

		$data_json = [];
		foreach($data_array as $item):
			$data = [];

			if(empty($data_json)){
				$data['tahun'] = $item['tahun'];
				$data[$item['jenis']] = (int)$item['total'];
				array_push($data_json, $data);
			}
			else {
				$idx_item = index_by_column($data_json, 'tahun', $item['tahun']);

				if($idx_item!==FALSE){
					$data_json[$idx_item][$item['jenis']] = (int)$item['total'];
				}
				else {
					$data['tahun'] = $item['tahun'];
					$data[$item['jenis']]=(int)$item['total'];
					array_push($data_json, $data);
				}
			}
		endforeach;

		return json_encode($data_json);
	}

	public function statistik()
	{
		$user_id = $this->session->userdata('id');
		$role = $this->session->userdata('role');
		$data['pengadaan'] 	= $this->aktivitas->stat_jenis_pengadaan($user_id, $role);
		$data['pekerjaan'] 	= $this->aktivitas->stat_jenis_pekerjaan($user_id, $role);
		// $data['status'] 	= $this->aktivitas->stat_status_aktivitas($user_id, $role);
		$data['serapan'] 		= $this->aktivitas->stat_anggaran($user_id, $role);
		$data['aktivitas'] 		= $this->aktivitas->stat_jml_aktivitas($user_id, $role);
		$data['awal_kontrak'] 	= $this->aktivitas->stat_awal_kontrak($user_id, $role);
		$data['akhir_kontrak'] 	= $this->aktivitas->stat_akhir_kontrak($user_id, $role);
		
		$this->load->view('statistik/beranda', $data);
	}

	public function detail_statistik()
	{
		$user_id = $this->session->userdata('id');
		$role = $this->session->userdata('role');

		// Statistik tahun ini
		$data['pengadaan'] 		= $this->aktivitas->stat_jenis_pengadaan($user_id, $role);
		$data['pekerjaan'] 		= $this->aktivitas->stat_jenis_pekerjaan($user_id, $role);
		$data['serapan'] 		= $this->aktivitas->stat_anggaran($user_id, $role);
		$data['aktivitas'] 		= $this->aktivitas->stat_jml_aktivitas($user_id, $role);
		$data['awal_kontrak'] 	= $this->aktivitas->stat_awal_kontrak($user_id, $role);
		$data['akhir_kontrak'] 	= $this->aktivitas->stat_akhir_kontrak($user_id, $role);
		$data['pptk']			= $this->aktivitas->stat_pptk();

		// Statistik dalam 5 tahun
		$data['aktivitas_5'] 	 = $this->aktivitas->stat_5_jml_aktivitas($user_id, $role);
		$data['pptk_5']			 = $this->aktivitas->stat_5_pptk();
		$data['stat_5_anggaran'] = $this->aktivitas->stat_5_anggaran($user_id, $role);
		
		$pengadaan_5 = $this->aktivitas->stat_5_jenis_pengadaan($user_id, $role);
		
		if(empty($pengadaan_5)){
			$data['pengadaan_5'] = json_encode([]);
			$data['label_pengadaan_5'] = json_encode([]);
		}
		else {
			$data['pengadaan_5'] = $this->json_group_value($pengadaan_5);
			$data['label_pengadaan_5'] = json_encode(array_unique(array_column($pengadaan_5, 'jenis')));
		}

		$pekerjaan_5 = $this->aktivitas->stat_5_jenis_pekerjaan($user_id, $role);
		
		if(empty($pekerjaan_5)){
			$data['pekerjaan_5'] = json_encode([]);
			$data['label_pekerjaan_5'] = array_column([]);
		}
		else {
			$data['pekerjaan_5'] = $this->json_group_value($pekerjaan_5);
			$data['label_pekerjaan_5'] = json_encode(array_unique(array_column($pekerjaan_5, 'jenis')));
		}

		$this->load->view('statistik/detail_statistik', $data);		
	}

	public function resume_pdf($aktivitas_id)
	{
		$this->load->library('pdf');
		$this->load->model('Kontrak_model', 'kontrak');

		$data['detail'] = $this->aktivitas->detail($aktivitas_id);
		$data['kontrak'] = $this->kontrak->get_kontrak_by_aktivitas($aktivitas_id);
		$data['controller'] = $this;

	    $this->pdf->setPaper('legal', 'portrait');
	    $this->pdf->filename = "resume-pekerjaan.pdf";
	    $this->pdf->load_view('aktivitas/report_pdf', $data);
	}

	public function resume_excel($aktivitas_id)
	{
		$this->load->model('Kontrak_model', 'kontrak');

		$data['detail'] 	= $this->aktivitas->detail($aktivitas_id);
		$data['kontrak'] 	= $this->kontrak->get_kontrak_by_aktivitas($aktivitas_id);
		$data['controller'] = $this;
		$this->load->view('aktivitas/resume_excel', $data);
	}

	public function import_csv()
	{	
		$this->_validate();
		$this->load->helper('generic');

		/*--- Struktur column data aktivitas ---*/
		// pekerjaan_kode
		// pekerjaan_nama
		// pekerjaan_tahun
		// aktivitas_nama
		// anggaran_awal
		// perubahan_anggaran
		// hps
		// nilai_kontrak
		// tgl_pengajuan_pencairan
		// tgl_pencairan
		// jenis_pekerjaan
		// metode_pengadaan
		// pptk_nama
		// pptk_unit_kerja

		$csv_data = csv_reader($_FILES['csvfile']['tmp_name']);

		if(!empty($csv_data)) {
			$this->load->model('Unit_kerja_model', 'unit_kerja');
			$this->load->model('Pegawai_model', 'pegawai');
			$this->load->model('Jenis_pekerjaan_model', 'jenis_pekerjaan_model');
			$this->load->model('Jenis_pengadaan_model', 'jenis_pengadaan_model');
			$this->load->model('Aktivitas_model', 'aktivitas');
			$this->load->model('Generic_model', 'generic');

			// Initialize data
			$cache_data = array(
				'unit_kerja' 		=> [],
				'pegawai'	 		=> [],
				'jenis_pekerjaan'	=> [],
				'jenis_pengadaan'	=> []
			);

			if(empty($this->session->userdata('cache_import_csv'))) {
				$unit_kerja 	 = object_to_array($this->unit_kerja->fetch_data());
				$pegawai 		 = object_to_array($this->pegawai->fetch_data());
				$jenis_pekerjaan = object_to_array($this->jenis_pekerjaan_model->show());
				$jenis_pengadaan = object_to_array($this->jenis_pengadaan_model->show());
				$curr_year		 = date("Y");
				
				$this->aktivitas->tahun_pekerjaan = $curr_year;
				$aktivitas 		 = object_to_array($this->aktivitas->fetch_data());

				if(empty($cache_data['unit_kerja']) and !empty($unit_kerja)) {
					foreach($unit_kerja as $item):
						$cache_data['unit_kerja'][] = $item;
					endforeach;
				}
				if(empty($cache_data['pegawai']) and !empty($pegawai)) {
					foreach($pegawai as $item):
						$cache_data['pegawai'][] = $item;
					endforeach;
				}
				if(empty($cache_data['jenis_pekerjaan']) and !empty($jenis_pekerjaan)) {
					foreach($jenis_pekerjaan as $item):
						$cache_data['jenis_pekerjaan'][] = $item;
					endforeach;
				}
				if(empty($cache_data['jenis_pengadaan']) and !empty($jenis_pengadaan)) {
					foreach($jenis_pengadaan as $item):
						$cache_data['jenis_pengadaan'][] = $item;
					endforeach;
				}
				

				$this->session->set_userdata('cache_import_csv', $cache_data);
			}
			else {
				$cache_data = $this->session->userdata('cache_import_csv');
			}

			$curr_year = date("Y");	
			$this->aktivitas->tahun_pekerjaan = $curr_year;
			$aktivitas = object_to_array($this->aktivitas->fetch_data());

			if(empty($cache_data['aktivitas_'.$curr_year]) and !empty($aktivitas)) {
				foreach($aktivitas as $item):
					$cache_data['aktivitas_'.$curr_year][] = $item;
				endforeach;
			}
			
			$row = 0;
			// Inserting data based on csv value
			foreach($csv_data as $data):
				if($row==0) {
					$row++;
					continue;
				}
				
				// Data Unit Kerja
				$checking = index_by_column($cache_data['unit_kerja'], 'nama', $data[13]);
				if($checking===FALSE) {
					$values = array('nama' => $data[13]);
					$insert = $this->generic->insert('unit_kerja', $values);
					if($insert=='ok') {
						$last_unit_kerja = $this->generic->get_last_id('unit_kerja');
						$values['id'] = $last_unit_kerja->id;
						$cache_data['unit_kerja'][] = $values;

						$unit_kerja_id = $last_unit_kerja->id;
					}
				} else {
					$unit_kerja_id = $cache_data['unit_kerja'][$checking]['id'];
				}

				// Data Jenis Pekerjaan
				$checking = index_by_column($cache_data['jenis_pekerjaan'], 'nama', $data[10]);
				if($checking===FALSE) {
					$values = array('nama' => $data[10], 'nickname' => strtolower($data[10]));
					$insert = $this->generic->insert('jenis_pekerjaan', $values);
					
					if($insert=='ok') {
						$last_jenis_pekerjaan = $this->generic->get_last_id('jenis_pekerjaan');
						$values['id'] = $last_jenis_pekerjaan->id;
						$cache_data['jenis_pekerjaan'][] = $values;

						$jenis_pekerjaan_id = $last_jenis_pekerjaan->id;
					}
				} else {
					$jenis_pekerjaan_id = $cache_data['jenis_pekerjaan'][$checking]['id'];
				}
				// unset($checking);
				

				// Data Jenis Pengadaan
				$checking = index_by_column($cache_data['jenis_pengadaan'], 'nama', $data[11]);
				if($checking===FALSE) {
					$values = array('nama' => $data[11]);
					$insert = $this->generic->insert('jenis_pengadaan', $values);
					if($insert=='ok') {
						$last_jenis_pengadaan = $this->generic->get_last_id('jenis_pengadaan');
						$values['id'] = $last_jenis_pengadaan->id;
						$cache_data['jenis_pengadaan'][] = $values;

						$jenis_pengadaan_id = $last_jenis_pengadaan->id;
					}
				} else {
					$jenis_pengadaan_id = $cache_data['jenis_pengadaan'][$checking]['id'];
				}

				// Data Pegawai
				$checking = index_by_column($cache_data['pegawai'], 'nama_lengkap', $data[12]);

				if($checking===FALSE) {
					$this->load->model('Role_model', 'role');
					$this->role->name = 'Staff';
					$staff = $this->role->fetch_data();
					
					$values = array(
						'id'			=> '',
						'nip'			=> '123',
						'nama_lengkap' 	=> $data[12],
						'jenis_kelamin' => 'Pria',
						'alamat' 		=> 'Semarang', 
						'no_telepon' 	=> '1234567890',
						'is_pegawai'	=> 'y',
						'unit_id'		=> $unit_kerja_id,
						'role_id'		=> $staff[0]->id,
						'username'		=> strtolower(explode(' ', $data[12])[0]),
						'password'		=> password_hash('dpusmg123', PASSWORD_BCRYPT, ['cost' => 13])
					);
					$insert = $this->generic->insert('pegawai', $values);
					if($insert=='ok') {
						$last_pegawai = $this->generic->get_last_id('pegawai');
						$values['id'] = $last_pegawai->id;
						$cache_data['pegawai'][] = $values;

						$pegawai_id = $last_pegawai->id;
					}
				} else {
					$pegawai_id = $cache_data['pegawai'][$checking]['id'];
				}

				// Data Aktivitas
				$tahun = $data[2];
				$kode_pekerjaan = $data[0];

				if(empty($cache_data['aktivitas_'.$tahun])) {
					$this->aktivitas->tahun_pekerjaan = $data[2];
					$aktivitas = object_to_array($this->aktivitas->fetch_data());

					$cache_data['aktivitas_'.$tahun] = $aktivitas;
				}

				#====== filter data aktivitas based on kode_pekerjaan
				

				// $filtering 	= [];
				// $sliced		= [];
				// foreach($cache_data['aktivitas_'.$tahun] as $item):
				// 	print('kode pekerjaan : '.$item['kode_pekerjaan']);
				// 	// if($item['kode_pekerjaan']==$kode_pekerjaan) {
				// 	// 	$filtering[] = $item;
				// 	// }
				// 	// else {
				// 	// 	$sliced[] = $item;
				// 	// }
				// endforeach;

				$kode_pekerjaan = $data[0];
				if(!empty($cache_data['aktivitas_'.$tahun])) {
					$filtering = array_filter($cache_data['aktivitas_'.$tahun], function($item) use ($kode_pekerjaan){
						// print('kode pekerjaan : '.$item['kode_pekerjaan']);
						// print('------------------------');
						// $temp = $item['kode_pekerjaan'];
						if(isset($item['kode_pekerjaan'])) {
							return ($item['kode_pekerjaan']==$kode_pekerjaan);
						}
						
					});

					$checking = index_by_column($filtering, 'nama_aktivitas', $data[3]);
					// print('count (after)'.count($cache_data['aktivitas_'.$tahun]));

					if($checking===FALSE) {
						$this->load->model('Pekerjaan_model', 'pekerjaan');
						$this->pekerjaan->nama_pekerjaan = $data[1];
						$this->pekerjaan->kode_pekerjaan = $data[0];
						$this->pekerjaan->tahun = $data[2];
						$pekerjaan_id = (!empty($this->pekerjaan->fetch_data()) ? $this->pekerjaan->fetch_data()[0]->id:NULL);

						$last_aktivitas = $this->generic->get_last_id('aktivitas');
						;
						$values = array(
							'nama_aktivitas' 			=> $data[3],
							'anggaran_awal' 			=> $data[4],
							'perubahan_anggaran' 		=> $data[5],
							'hps'						=> $data[6],
							'nilai_kontrak'				=> $data[7],
							'tgl_pengajuan_pencairan' 	=> ((!empty($data[8]))?date('Y-m-d', strtotime($data[8])):NULL),
							'tgl_pencairan'				=> ((!empty($data[9]))?date('Y-m-d', strtotime($data[9])):NULL),
							'pptk'						=> $pegawai_id,
							'pekerjaan_id'				=> $pekerjaan_id,
							'pengadaan_id'				=> $jenis_pengadaan_id,
							'jenis_pekerjaan_id'		=> $jenis_pekerjaan_id
						);

						$insert = $this->generic->insert('aktivitas', $values);
						if($insert=='ok') {
							$last_aktivitas = $this->generic->get_last_id('aktivitas');
							
							$values['id'] 						= $last_aktivitas->id;
							$values['kode_pekerjaan'] 			= $kode_pekerjaan;
							$cache_data['aktivitas_'.$tahun][] 	= $values;
						}
					}
				}

				

				// if($checking!==FALSE) {
				// 	print('running update -----|');
				// 	$data_aktivitas = $filtering[$checking];
				// 	$values 		= [];

				// 	if($data_aktivitas['nama_aktivitas'] != $data[3]) {
				// 		$values['nama_aktivitas'] = $data[3];
				// 		$data_aktivitas['nama_aktivitas'] = $data[3];
				// 	}

				// 	if($data_aktivitas['anggaran_awal'] != $data[4]) {
				// 		$values['anggaran_awal'] = $data[4];
				// 		$data_aktivitas['anggaran_awal'] = $data[4];
				// 	}

				// 	if($data_aktivitas['perubahan_anggaran'] != $data[5]) {
				// 		$values['perubahan_anggaran'] = $data[5];
				// 		$data_aktivitas['perubahan_anggaran'] = $data[5];
				// 	}

				// 	if($data_aktivitas['hps'] != $data[6]) {
				// 		$values['hps'] = $data[6];
				// 		$data_aktivitas['perubahan_anggaran'] = $data[6];
				// 	}

				// 	if($data_aktivitas['nilai_kontrak'] != $data[7]) {
				// 		$values['nilai_kontrak'] = $data[7];
				// 		$data_aktivitas['nilai_kontrak'] = $data[7];
				// 	}

				// 	if($data_aktivitas['tgl_pengajuan_pencairan'] != $data[8]) {
				// 		$values['tgl_pengajuan_pencairan'] = $data[8];
				// 		$data_aktivitas['tgl_pengajuan_pencairan'] = $data[8];
				// 	}

				// 	if($data_aktivitas['tgl_pencairan'] != $data[9]) {
				// 		$values['tgl_pencairan'] = $data[9];
				// 		$data_aktivitas['tgl_pengajuan_pencairan'] = $data[9];
				// 	}

				// 	if(!empty($values)) {
				// 		$key = ['id'=>$data_aktivitas['id']];

				// 		$update = $this->generic->update('aktivitas', $values, $key);
				// 	}

				// }
				// else {
				// 	print('running insert -----|');

					
				// }

				// // inserting again
				// foreach($filtering as $item):
				// 	$sliced[] = $item;
				// 	print('insert new item +++++++');
				// endforeach;
				// $cache_data['aktivitas_'.$tahun] = $sliced;
				// print('count (after)'.count($cache_data['aktivitas_'.$tahun]));

			endforeach;

			$this->session->set_userdata('cache_import_csv', $cache_data);

		}
		echo json_encode(array("status" => TRUE));
		exit;
		// $insert = $this->generic->insert('karyawan',$data);
        // if($insert=='ok') {
        //     echo json_encode(array("status" => TRUE));
        //     exit;
        // }
        // else {
        //     echo json_encode(array("status" => 'Fail insert data'));
        //     exit;
        // }
		
	}

	public function template_import()
	{
		$this->load->view('aktivitas/template_import');
	}

	public function get_data($pekerjaan_id=NULL)
	{
        $sess_data = $this->session->userdata('session_data');
		$user_id = $sess_data['id'];
		$role = $sess_data['role'];
		// $is_resume = $this->session->userdata('is_resume');
		// $jabatan = $this->session->userdata('jabatan');

		//Show data kontrak that will be kontrak_pekerjaan
		if($this->session->userdata('data_kegiatan')==TRUE) {
			$this->aktivitas->is_kontrak_pekerjaan = TRUE;
			$list = $this->aktivitas->get_datatables($pekerjaan_id, $user_id, 'Super Admin');
			$role = 'Super Admin';
		}
		else {
			$list = $this->aktivitas->get_datatables($pekerjaan_id, $user_id, $role);
		}
        
		$data = array();
		$no = (isset($_POST['start']) ? $_POST['start']:0);
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->nama_aktivitas;
			$row[] = $field->tahun;
            $row[] = $field->ppkom;
            $row[] = $field->pptk;
			$row[] = $field->jenis_pekerjaan;
            $row[] = $field->jenis_pengadaan;
            $row[] = format_money($field->anggaran_awal);
			$row[] = "<a href=".base_url('aktivitas/detail/'.$field->id)." class='btn btn-primary'>Detail</i></a>";
			$row[] = "";

			$data[] = $row;
		}

		// print_r($data);
		// print('<br/>');
		// print(count($data).'<br/>');
		$output = array(
			"draw" => (isset($_POST['draw']) ? $_POST['draw']:FALSE),
			"recordsTotal" => $this->aktivitas->count_all($pekerjaan_id, $user_id, $role),
			"recordsFiltered" => $this->aktivitas->count_filtered($pekerjaan_id, $user_id, $role),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

}

/* End of file Aktivitas.php */
