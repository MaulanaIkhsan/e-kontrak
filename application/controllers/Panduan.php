<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Panduan extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $sess_data = $this->session->userdata('session_data');
        if($sess_data===NULL) {
            $this->session->set_flashdata('warning', '<div class="alert alert-warning fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a> 
                Harap login terlebih dahulu untuk mengakses sistem</div>');
			redirect('auth');
        }

        $this->load->model('Panduan_model', 'panduan');

        $this->load->library('grocery_CRUD');
    }
    
    
    public function index()
    {
        /*--- panduan_user ---*/
		// id
		// nama
		// jenis (internal, umum)
		// file
		// created_at
		// updated_at

		$title = 'Panduan Pengguna';
		$table = 'panduan_user';

        $sess_data 	= $this->session->userdata('session_data');
		$user_id    = $sess_data['id'];
        $user_role  = $sess_data['role'];
        $user_as    = $sess_data['as'];

		$crud = new grocery_CRUD();

        // condition to show jenis panduan
        if($user_as=='pihak_ketiga') {
            $crud->where('panduan_user.jenis', 'umum');
        }

		$crud->set_table($table);
		$crud->columns('nama', 'file');

		$crud->set_subject($title);
		$crud->display_as('nama', 'Nama Panduan');
		$crud->display_as('jenis', 'Jenis');
		$crud->display_as('file', 'Lampiran File');

        // Set columns that show, edit and insert operation
        $crud->fields('nama', 'jenis', 'file');
        
            

		// Disable button clone
        $crud->unset_clone();

        // condition to set restriction access manage data
        if(($user_as=='pegawai' and $user_role!='Super Admin') or $user_as=='pihak_ketiga') {
            $crud->unset_edit();
            $crud->unset_delete();
            $crud->unset_add();
            $crud->unset_read();
            $crud->unset_print();
            $crud->unset_export();
        }
        
        
        // Field type beside textbox
        $crud->field_type('jenis', 'dropdown', array('internal'=>'Internal', 'umum'=>'Umum'));

        // Upload field
        $crud->set_field_upload('file', 'assets/uploads/documents_panduan/', 'pdf');

		// Required field
		$crud->required_fields('nama', 'jenis', 'file');

		// Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);		
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/panduan', $output);
    }

}

/* End of file Panduan.php */
