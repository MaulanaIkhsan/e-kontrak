<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Si Super</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Google Fonts -->
  <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Poppins:300,400,500,700" rel="stylesheet"> -->

  <!-- Bootstrap CSS File -->
  <link href="<?php print(base_url('assets/landing/lib/bootstrap/css/bootstrap.min.css'));?>" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?php print(base_url('assets/landing/lib/font-awesome/css/font-awesome.min.css'));?>" rel="stylesheet">
  
  <link href="<?php print(base_url('assets/landing/lib/animate/animate.min.css'));?>" rel="stylesheet">
  

  <!-- Main Stylesheet File -->
  <link href="<?php print(base_url('assets/landing/css/style.css'));?>" rel="stylesheet">

  <!-- =======================================================
    Theme Name: Regna
    Theme URL: https://bootstrapmade.com/regna-bootstrap-onepage-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body>

  <!--==========================
  Header
  ============================-->
  <header id="header">
    <div class="container">

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="#hero"><i class="fa fa-home"></i> Home</a></li>
          <li><a href="#services"><i class="fa fa-info-circle"></i> Deskripsi</a></li>
          <li><a href="#portfolio"><i class="fa fa-user"></i> Tentang Kami</a></li>
          <li><a href="<?php print(base_url('auth')); ?>"><i class="fa fa-key"></i> Login</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

  <!--==========================
    Hero Section
  ============================-->
  <section id="hero">
    <div class="hero-container">
      <h1>Selamat Datang di SiSuper</h1>
      <h2>Sistem Informasi Surat Perintah Kerja</h2>
      <a href="#services" class="btn-get-started">Selengkapnya</a>
    </div>
  </section><!-- #hero -->

  <main id="main">


    <!--==========================
      Services Section
    ============================-->
    <section id="services">
      <div class="container wow fadeIn">
        <div class="section-header">
          <h3 class="section-title">Deskripsi</h3>
          <p class="section-description">Sistem Informasi Surat Perintah Kerja (SiSuper) merupakan sebuah sistem untuk mengelola kontrak pekerjaan 
            antara Dinas Pekerjaan Umum Kota Semarang dengan Pihak Ketiga. Dengan menggunakan SiSuper kontrak pekerjaan
            menjadi lebih mudah untuk dikelola dan mempermudah Dinas Pekerjaan Umum dalam membuat Surat Perintah Kerja. Berikut ini adalah fitur yang ada : </p>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
            <div class="box">
              <div class="icon"><a href=""><i class="fa fa-clipboard"></i></a></div>
              <h4 class="title"><a href="">Data Program Pekerjaan</a></h4>
              <p class="description">Data Program pekerjaan yang sudah terintegrasi dengan Sistem Simanggaran</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.2s">
            <div class="box">
              <div class="icon"><a href=""><i class="fa fa-users"></i></a></div>
              <h4 class="title"><a href="">Data Pihak Ketiga</a></h4>
              <p class="description">Tersimpannya data profil pihak ketiga beserta data karyawan yang pada perusahaan tersebut</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.4s">
            <div class="box">
              <div class="icon"><a href=""><i class="fa fa-user"></i></a></div>
              <h4 class="title"><a href="">Data Pejabat Pengadaan</a></h4>
              <p class="description">Tersimpannya data pejabat pengadaan berdasarkan tahun penugasan</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 wow fadeInUp" data-wow-delay="0.6s">
            <div class="box">
              <div class="icon"><a href=""><i class="fa fa-file-text"></i></a></div>
              <h4 class="title"><a href="">Surat Perintah Kerja</a></h4>
              <p class="description">Pegawai dapat mencetak surat perintah pekerjaan berdasarkan kontrak pekerjaan terkait</p>
            </div>
          </div>

          
          
        </div>

      </div>
    </section><!-- #services -->

    <!--==========================
      Data Section
    ============================-->
    <section id="facts">
      <div class="container wow fadeIn">
        <div class="section-header">
          <h3 class="section-title">Data Pekerjaan Tahun Ini</h3>
          <p class="section-description"></p>
        </div>
        <div class="row counters">
            <div class="col-lg-3 col-6 text-center">
                <span data-toggle="counter-up"><?php print($jml_aktivitas->jml_aktivitas); ?></span>
                <p>Program Pekerjaan</p>
            </div>
          
  			<div class="col-lg-3 col-6 text-center">
                <span data-toggle="counter-up"><?php print($jml_kontrak_pekerjaan); ?></span>
                <p>Kontrak Pekerjaan</p>
  			</div>

            <div class="col-lg-3 col-6 text-center">
                <span data-toggle="counter-up"><?php print($jml_pejabat_pengadaan); ?></span>
                <p>Pejabat Pengadaan</p>
            </div>

            <div class="col-lg-3 col-6 text-center">
                <span data-toggle="counter-up"><?php print($jml_pihak_ketiga); ?></span>
                <p>Penyedia Barang/Jasa</p>
  			</div>

  			</div>

      </div>
    </section><!-- #facts -->

    <!--==========================
      Portfolio Section
    ============================-->
    <section id="portfolio">
      <div class="container wow fadeInUp">
        
        <div class="row">
          <div class="transparant">
              <div>
                <h2><strong>Dinas Pekerjaan Umum <br/>Kota Semarang</strong></h2>
              </div>
              <!-- <h3 class="section-title">Dinas Pekerjaan Umum</h3>
              <p class="section-description">Kota Semarang</p> -->
            <span>
              <i class="fa fa-map-marker"></i> <strong>Alamat :</strong> 
              <p>Jl. Madukoro Raya No.7, Krobokan, Semarang Barat
                Kota Semarang, Jawa Tengah 50141 </p>
            </span>
            <div class="row">
              <div class="col-md-6">
                  <i class="fa fa-envelope"> <strong>E-mail : </strong></i>
                  <p>dpusemarangkota@gmail.com</p>
              </div>
              <div class="col-md-6">
                  <i class="fa fa-phone"> <strong>Telepon : </strong></i>
                  <p>(024)76433969 </p>
              </div>
            </div>
            <span>
                <div class="row">
                  <div class="col-md-6">
                      <i class="fa fa-twitter"></i> pu_kotasemarang<br/>
                      <i class="fa fa-instagram"></i> pu_kotasemarang<br/>
                  </div>
                  <div class="col-md-6">
                      <i class="fa fa-facebook"></i> DPU Kota Semarang<br/>
                      <i class="fa fa-youtube"></i> DPU Kota Semarang<br/>
                  </div>
                </div>
            </span>
          </div>
          
        </div>

      </div>
    </section><!-- #portfolio -->

  </main>

  
  <!-- edited from : Regna (https://bootstrapmade.com/) -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="<?php print(base_url('assets/landing/lib/jquery/jquery.min.js'));?>"></script>
  <script src="<?php print(base_url('assets/landing/lib/jquery/jquery-migrate.min.js'));?>"></script>
  <script src="<?php print(base_url('assets/landing/lib/bootstrap/js/bootstrap.bundle.min.js'));?>"></script>
  <script src="<?php print(base_url('assets/landing/lib/easing/easing.min.js'));?>"></script>
  <script src="<?php print(base_url('assets/landing/lib/wow/wow.min.js'));?>"></script>
  <script src="<?php print(base_url('assets/landing/lib/waypoints/waypoints.min.js'));?>"></script>
  <script src="<?php print(base_url('assets/landing/lib/counterup/counterup.min.js'));?>"></script>
  <script src="<?php print(base_url('assets/landing/lib/superfish/hoverIntent.js'));?>"></script>
  <script src="<?php print(base_url('assets/landing/lib/superfish/superfish.min.js'));?>"></script>
  

  <!-- Contact Form JavaScript File -->
  <script src="<?php print(base_url('assets/landing/contactform/contactform.js'));?>"></script>

  <!-- Template Main Javascript File -->
  <script src="<?php print(base_url('assets/landing/js/main.js'));?>"></script>
  

</body>
</html>
