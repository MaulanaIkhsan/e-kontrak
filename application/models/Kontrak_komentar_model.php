<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kontrak_komentar_model extends CI_Model {

    /*----- kontrak_komentar -----*/
    // id
	// kontrak_pekerjaan_id
	// pegawai_id
	// karyawan_id
	// komentar
	// created_at
    // updated_at

    public function fetch_data($id=NULL, $kontrak_pekerjaan_id=NULL)
    {
        $sql = $this->db->select('kontrak_komentar.id as id, 
								pegawai.id as pegawai_id,
								pegawai.nama_lengkap as pegawai_nama,
								karyawan.id as karyawan_id,
								karyawan.nama_lengkap as karyawan_nama,
								pihak_ketiga.id as pihak_ketiga_id,
								pihak_ketiga.nama_perusahaan as pihak_ketiga_nama,
                                kontrak_komentar.komentar as komentar')
                        ->from('kontrak_komentar')
						->join('kontrak_pekerjaan', 'kontrak_komentar.kontrak_pekerjaan_id=kontrak_pekerjaan.id', 'left')
						->join('aktivitas', 'kontrak_pekerjaan.aktivitas_id=aktivitas.id', 'left')
						->join('karyawan', 'kontrak_pekerjaan.karyawan_id=karyawan.id', 'left')
						->join('pihak_ketiga', 'karyawan.pihak_ketiga_id=pihak_ketiga.id', 'left')
						->join('pegawai', 'kontrak_pekerjaan.pegawai_id=pegawai.id', 'left');

        if($id!=NULL){
            $sql->where('kontrak_komentar.id', $id);
        }

        if($kontrak_pekerjaan_id!=NULL) {
            $sql->where('kontrak_komentar.kontrak_pekerjaan_id', $kontrak_pekerjaan_id);
        }

        if($id!=NULL) {
            return $sql->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }

}

/* End of file Kontrak_komentar_model.php */
