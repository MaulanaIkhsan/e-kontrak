<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_surat_model extends CI_Model {

    // ----- jenis_surat -----
    // id
    // nama
    // kode
    // created_at
    // updated_at
        
    public function fetch_data($id=NULL, array $list_kode_surat=NULL)
    {
        $sql = $this->db->select('jenis_surat.id as id,
                                jenis_surat.nama as nama,
                                jenis_surat.kode as kode')
                        ->from('jenis_surat')
                        ->order_by('jenis_surat.nama');
        
        if(is_array($list_kode_surat) and !empty($list_kode_surat)) {
            $sql->where_in('jenis_surat.kode', $list_kode_surat);
        }
        
        if($id!=NULL) {
            return $sql->where('jenis_surat.id', $id)
                        ->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }

}

/* End of file Jenis_surat_model.php */
