<?php
$this->load->view('template/header');?>
<!-- Select2 -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/select2/dist/css/select2.min.css')); ?>">
<?php $this->load->view('template/asset_header');?>
<!-- Star Rating -->
<link rel="stylesheet" href="<?php print(base_url('assets/plugins/rating/dist/starrr.css')); ?>">


<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <!-- Box Form Pengunjung -->
        <?php print($this->session->flashdata('alert')); ?>
        <?php print($this->session->flashdata('success')); ?>
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-primary" id="form-pengunjung">
              <div class="box-header with-border">
                <h3 class="box-title">Detail Kunjungan</h3>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <form class="form-horizontal" method="post" action="<?php print(base_url('pengunjung/add'))?>">
                <div class="box-body">
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Nama Lengkap</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->tamu_nama); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Kode Kunjungan</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->kode_kunjungan); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Tanggal</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->tanggal); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="keperluan" class="col-sm-2 control-label">Waktu</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->waktu); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Tujuan</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->tujuan_nama); ?></div>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Keperluan</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->keperluan); ?></div>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Rating</label>
                    <div class="col-sm-9">
                      <div class='starrr' id='star'></div>
                      <input type="hidden" id="star_input" value="<?php print($detail->rating); ?>">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Penilaian</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->penilaian); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Masukan</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->masukan); ?></div>
                    </div>
                  </div>                  
                </div>
                 <div class="box-footer">
                  <?php if($this->session->userdata('pengunjung_id')!=FALSE) {?>
                    <a href="<?php print(base_url('pengunjung/detail/'.$this->session->userdata('pengunjung_id') ));?>" class="btn btn-primary">Kembali</a>
                  <?php } else { ?>
                    <a href="<?php print(base_url('kunjungan'));?>" class="btn btn-primary">Kembali</a>
                  <?php } ?>
                  <a href="<?php print(base_url('kunjungan/update/'.$detail->id));?>" class="btn btn-primary">Update</a>
                  <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete">Delete</a>
                </div>
                <!-- /.box-footer -->
              </form>

              <!-- Confirmation Modal -->
              <div class="modal fade" id="modal-delete">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title"><i class="fa fa-warning"></i> Konfirmasi Delete Data</h4>
                    </div>
                    <div class="modal-body">
                      <p>Apakah anda yakin ingin menghapus data kunjungan ini ?</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                      <form action="<?php print(base_url('kunjungan/delete/'.$detail->id)); ?>" method="post">
                        <button type="submit" name="delete" value="delete" class="btn btn-danger">Delete</button>
                      </form>
                      
                    </div>
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>
              <!-- /Confirmation Modal -->

            </div>
          </div>
          <!-- /Box Form Pengunjung -->

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<!-- Select2 -->
<script src="<?php print(base_url('assets/bower_components/select2/dist/js/select2.full.min.js'));?>"></script>
<!-- Star Rating -->
<script type="text/javascript" src="<?php print(base_url('assets/plugins/rating/dist/starrr.js')); ?>"></script>
<script>
  $(function(){
    

    var $s2input = $('#star_input');
    $('#star').starrr({
      max: 10,
      rating: $s2input.val()
    });

    
  });
</script>
<?php $this->load->view('template/footer');?>