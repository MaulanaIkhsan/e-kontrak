<!-- - 
- Kode rek
- Program
- Kegiatan
- tahun anggaran
- pagu
- ppkom
- pptk
- pejabat pengadaan
- No. Kontrak
- Tanggal awal Kontrak
- Tanggal Akhir Kontrak
- Masa Pengerjaan
- nilai hps
- nilai kontrak
- Nama Perusahaan
- Alamat Perusahaan
- Kontak perusahaan
- status kontrak -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Rekap Data</title>
</head>
<body>
	<?php 
	header('Content-type: application/vnd-ms-excel');
	header('Content-Disposition: attachment; filename=rekap-data-kontrak-group.xls');
	?>
	<table>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<th colspan="16" align="center">LAPORAN DATA KONTRAK PEKERJAAN</th>
		</tr>
		<tr>
			<th colspan="16" align="center">DINAS PEKERJAAN UMUM</th>
		</tr>
		<tr>
			<th colspan="16" align="center">KOTA SEMARANG</th>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" align="center"><strong>Kategori Laporan</strong></td>
		</tr>
		<tr>	
			<td>Tahun : </td>
			<td><?php print(date("Y")); ?></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		
	</table>
	<table border="1">
		<thead>
			<tr>
				<td rowspan="2" align="center" valign="middle"><strong>No</strong></td>
				<td rowspan="2" align="center" valign="middle"><strong>Pejabat Pengadaan</strong></td>
				<td rowspan="2" align="center" valign="middle"><strong>Kode Rekening</strong></td>
				<td rowspan="2" align="center" valign="middle"><strong>Kegiatan</strong></td>
				<td rowspan="2" align="center" valign="middle"><strong>Pekerjaan</strong></td>
				<td rowspan="2" align="center" valign="middle"><strong>Tahun</strong></td>
				<td rowspan="2" align="center" valign="middle"><strong>Pagu Murni (Rp)</strong></td>
				<td rowspan="2" align="center" valign="middle"><strong>Pagu Perubahan (Rp)</strong></td>
				<td rowspan="2" align="center" valign="middle"><strong>PPKOM</strong></td>
				<td rowspan="2" align="center" valign="middle"><strong>PPTK</strong></td>
				
				<td colspan="2" align="center"><strong>SPK</strong></td>
				<td colspan="5" align="center"><strong>SPMK</strong></td>
				<td colspan="3" align="center"><strong>Penyedia Barang/Jasa </strong></td>
				
				<td rowspan="2" align="center" valign="middle"><strong>Status Kontrak </strong></td>
			</tr>
			<tr>
				<td align="center"><strong>Nomor<strong></td>
				<td align="center"><strong>Tanggal<strong></td>

				<td align="center"><strong>Nomor<strong></td>
				<td align="center"><strong>Tanggal<strong></td>
				<td align="center"><strong>Jangka Waktu Pelaksanaan<strong></td>
				<td align="center"><strong>Mulai<strong></td>
				<td align="center"><strong>Akhir<strong></td>

				<td align="center"><strong>Nama Perusahaan<strong></td>
				<td align="center"><strong>Alamat<strong></td>
				<td align="center"><strong>Telepon<strong></td>
			</tr>
		</thead>
		<tbody>
		<?php 
			$no=1; 
			if(empty($kontrak_pekerjaan)) {
				print('<tr><td colspan="20"><center>-- Data Kosong --</center></td></tr>');
			}
			else {
				$temp_pekerjaan = NULL;
				$temp_pejabat_pengadaan = NULL;
				foreach($kontrak_pekerjaan as $item): 
					if($item->pejabat_pengadaan_nama!=$temp_pejabat_pengadaan and $item->pekerjaan_nama!=$temp_pekerjaan) { 
						$temp_pekerjaan=$item->pekerjaan_nama;
						$temp_pejabat_pengadaan=$item->pejabat_pengadaan_nama; ?>

						<tr>
							<td><?php print('&nbsp;'); ?></td>
							<td><?php print($item->pejabat_pengadaan_nama);?></td>
							<td><?php print('&nbsp;');?></td>
							<td><strong><?php print($item->pekerjaan_nama);?></strong></td>
							<td><?php print('&nbsp;'); ?></td>
						</tr>
						<tr>
							<td><?php print($no); ?></td>
							<td><?php print('&nbsp;');?></td>
							<td><?php print($item->aktivitas_no_rekening);?></td>
							<td><?php print('&nbsp;');?></td>
							<td><?php print($item->aktivitas_nama); ?></td>
							<td><?php print($item->pekerjaan_tahun);?></td>
							<td align="right"><?php print(format_money($item->anggaran_awal)); ?></td>
							<td align="right"><?php print(format_money($item->perubahan_anggaran)); ?></td>
							<td><?php print($item->ppkom_nama);?></td>
							<td><?php print($item->pptk_nama);?></td>
							
							<!-- No SPK & SPMK-->
							<?php 
							if(!empty($item->kontrak_pekerjaan_id)) {

								$spk = $controller->surat_kontrak($item->kontrak_pekerjaan_id, 'SPK');
								if(!empty($spk)) {
									print('<td>'.$spk[0]->no_surat.'</td>');
									print('<td>'.tgl_indo($spk[0]->tgl_surat).'</td>');
								} else {
									print('<td>&nbsp;</td>');
									print('<td>&nbsp;</td>');
								}

								$spmk = $controller->surat_kontrak($item->kontrak_pekerjaan_id, 'SPMK');
								if(!empty($spmk)) { ?>
									<td><?php print($spmk[0]->no_surat);?></td>
									<td><?php print(tgl_indo($spmk[0]->tgl_surat));?></td>
									<td><?php print(($item->tgl_awal_kontrak!='Kosong')?tgl_indo($item->tgl_awal_kontrak):'&nbsp;');?></td>
									<td><?php print(($item->tgl_akhir_kontrak!='Kosong')?tgl_indo($item->tgl_akhir_kontrak):'&nbsp;');?></td>
									<td><?php print(($item->durasi_kontrak!=NULL)?$item->durasi_kontrak.' hari':'&nbsp;');?></td>
								<?php } else {
									print('<td>&nbsp;</td>');
									print('<td>&nbsp;</td>');
									print('<td>&nbsp;</td>');
									print('<td>&nbsp;</td>');
									print('<td>&nbsp;</td>');
								}

							} else {
								print('<td>&nbsp;</td>');
								print('<td>&nbsp;</td>');
								print('<td>&nbsp;</td>');
								print('<td>&nbsp;</td>');
								print('<td>&nbsp;</td>');
								print('<td>&nbsp;</td>');
								print('<td>&nbsp;</td>');
							}
							?>

							<?php 
							if(!empty($item->kontrak_pekerjaan_id)) {
								$kontrak_pihak_ketiga = $controller->data_kontrak_pihak_ketiga($item->kontrak_pekerjaan_id);
								
								if(empty($kontrak_pihak_ketiga)) {
									print('<td colspan="3" align="center">&nbsp;</td>');
									print('<td>'.$item->status.'</td>');
									print('</tr>');
								} else {
									$flag_pihak_ketiga = 1;
									foreach($kontrak_pihak_ketiga as $row):
										if($flag_pihak_ketiga==1) {
											print('<td>'.$row->pihak_ketiga_nama.'</td>');
											print('<td>'.$row->pihak_ketiga_alamat.'</td>');
											print('<td>'.$row->pihak_ketiga_no_telepon.'</td>');
											print('<td>'.$item->status.'</td>');
											print('</tr>');
										} else {
											print('<tr>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											
											print('<td>'.$row->pihak_ketiga_nama.'</td>');
											print('<td>'.$row->pihak_ketiga_alamat.'</td>');
											print('<td>'.$row->pihak_ketiga_no_telepon.'</td>');
											print('<td>'.$item->status.'</td>');
											print('</tr>');
										}
									endforeach;
								}

							}
							else {
								print('<td colspan="4" align="center">&nbsp;</td>');
								print('</tr>');
							}
							?>
					<?php } elseif($item->pejabat_pengadaan_nama==$temp_pejabat_pengadaan and $item->pekerjaan_nama!=$temp_pekerjaan) { 
						$temp_pekerjaan=$item->pekerjaan_nama; ?>
						<tr>
							<td><?php print('&nbsp;'); ?></td>
							<td><?php print('&nbsp;');?></td>
							<td><?php print('&nbsp;');?></td>
							<td><strong><?php print($item->pekerjaan_nama);?></strong></td>
							<td><?php print('&nbsp;'); ?></td>
						</tr>
						<tr>
							<td><?php print($no); ?></td>
							<td><?php print('&nbsp;');?></td>
							<td><?php print($item->aktivitas_no_rekening);?></td>
							<td><?php print('&nbsp;');?></td>
							<td><?php print($item->aktivitas_nama); ?></td>
							<td><?php print($item->pekerjaan_tahun);?></td>
							<td align="right"><?php print(format_money($item->anggaran_awal)); ?></td>
							<td align="right"><?php print(format_money($item->perubahan_anggaran)); ?></td>
							<td><?php print($item->ppkom_nama);?></td>
							<td><?php print($item->pptk_nama);?></td>
							
							<!-- No SPK & SPMK-->
							<?php 
							if(!empty($item->kontrak_pekerjaan_id)) {

								$spk = $controller->surat_kontrak($item->kontrak_pekerjaan_id, 'SPK');
								if(!empty($spk)) {
									print('<td>'.$spk[0]->no_surat.'</td>');
									print('<td>'.tgl_indo($spk[0]->tgl_surat).'</td>');
								} else {
									print('<td>&nbsp;</td>');
									print('<td>&nbsp;</td>');
								}

								$spmk = $controller->surat_kontrak($item->kontrak_pekerjaan_id, 'SPMK');
								if(!empty($spmk)) { ?>
									<td><?php print($spmk[0]->no_surat);?></td>
									<td><?php print(tgl_indo($spmk[0]->tgl_surat));?></td>
									<td><?php print(($item->tgl_awal_kontrak!='Kosong')?tgl_indo($item->tgl_awal_kontrak):'&nbsp;');?></td>
									<td><?php print(($item->tgl_akhir_kontrak!='Kosong')?tgl_indo($item->tgl_akhir_kontrak):'&nbsp;');?></td>
									<td><?php print(($item->durasi_kontrak!=NULL)?$item->durasi_kontrak.' hari':'&nbsp;');?></td>
								<?php } else {
									print('<td>&nbsp;</td>');
									print('<td>&nbsp;</td>');
									print('<td>&nbsp;</td>');
									print('<td>&nbsp;</td>');
									print('<td>&nbsp;</td>');
								}

							} else {
								print('<td>&nbsp;</td>');
								print('<td>&nbsp;</td>');
								print('<td>&nbsp;</td>');
								print('<td>&nbsp;</td>');
								print('<td>&nbsp;</td>');
								print('<td>&nbsp;</td>');
								print('<td>&nbsp;</td>');
							}
							?>

							<?php 
							if(!empty($item->kontrak_pekerjaan_id)) {
								$kontrak_pihak_ketiga = $controller->data_kontrak_pihak_ketiga($item->kontrak_pekerjaan_id);
								
								if(empty($kontrak_pihak_ketiga)) {
									print('<td colspan="3" align="center">&nbsp;</td>');
									print('<td>'.$item->status.'</td>');
									print('</tr>');
								} else {
									$flag_pihak_ketiga = 1;
									foreach($kontrak_pihak_ketiga as $row):
										if($flag_pihak_ketiga==1) {
											print('<td>'.$row->pihak_ketiga_nama.'</td>');
											print('<td>'.$row->pihak_ketiga_alamat.'</td>');
											print('<td>'.$row->pihak_ketiga_no_telepon.'</td>');
											print('<td>'.$item->status.'</td>');
											print('</tr>');
										} else {
											print('<tr>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											
											print('<td>'.$row->pihak_ketiga_nama.'</td>');
											print('<td>'.$row->pihak_ketiga_alamat.'</td>');
											print('<td>'.$row->pihak_ketiga_no_telepon.'</td>');
											print('<td>'.$item->status.'</td>');
											print('</tr>');
										}
									endforeach;
								}

							}
							else {
								print('<td colspan="4" align="center">&nbsp;</td>');
								print('</tr>');
							}
							?>
					<?php } else { ?>
						<tr>
							<td><?php print($no); ?></td>
							<td><?php print('&nbsp;');?></td>
							<td><?php print($item->aktivitas_no_rekening);?></td>
							<td><?php print('&nbsp;');?></td>
							<td><?php print($item->aktivitas_nama); ?></td>
							<td><?php print($item->pekerjaan_tahun);?></td>
							<td align="right"><?php print(format_money($item->anggaran_awal)); ?></td>
							<td align="right"><?php print(format_money($item->perubahan_anggaran)); ?></td>
							<td><?php print($item->ppkom_nama);?></td>
							<td><?php print($item->pptk_nama);?></td>
							<td><?php print($item->pejabat_pengadaan_nama);?></td>
							
							<!-- No SPK & SPMK-->
							<?php 
							if(!empty($item->kontrak_pekerjaan_id)) {
								
								$spk = $controller->surat_kontrak($item->kontrak_pekerjaan_id, 'SPK');
								if(!empty($spk)) {
									print('<td>'.$spk[0]->no_surat.'</td>');
									print('<td>'.tgl_indo($spk[0]->tgl_surat).'</td>');
								} else {
									print('<td>&nbsp;</td>');
									print('<td>&nbsp;</td>');
								}

								$spmk = $controller->surat_kontrak($item->kontrak_pekerjaan_id, 'SPMK');
								if(!empty($spmk)) { ?>
									<td><?php print($spmk[0]->no_surat);?></td>
									<td><?php print(tgl_indo($spmk[0]->tgl_surat));?></td>
									<td><?php print(($item->tgl_awal_kontrak!='Kosong')?tgl_indo($item->tgl_awal_kontrak):'&nbsp;');?></td>
									<td><?php print(($item->tgl_akhir_kontrak!='Kosong')?tgl_indo($item->tgl_akhir_kontrak):'&nbsp;');?></td>
									<td><?php print(($item->durasi_kontrak!=NULL)?$item->durasi_kontrak.' hari':'&nbsp;');?></td>
								<?php } else {
									print('<td>&nbsp;</td>');
									print('<td>&nbsp;</td>');
									print('<td>&nbsp;</td>');
									print('<td>&nbsp;</td>');
									print('<td>&nbsp;</td>');
								}

							} else {
								print('<td>&nbsp;</td>');
								print('<td>&nbsp;</td>');
								print('<td>&nbsp;</td>');
								print('<td>&nbsp;</td>');
								print('<td>&nbsp;</td>');
								print('<td>&nbsp;</td>');
								print('<td>&nbsp;</td>');
							}
							?>

							<?php 
							if(!empty($item->kontrak_pekerjaan_id)) {
								$kontrak_pihak_ketiga = $controller->data_kontrak_pihak_ketiga($item->kontrak_pekerjaan_id);
								
								if(empty($kontrak_pihak_ketiga)) {
									print('<td colspan="3" align="center">&nbsp;</td>');
									print('<td>'.$item->status.'</td>');
									print('</tr>');
								} else {
									$flag_pihak_ketiga = 1;
									foreach($kontrak_pihak_ketiga as $row):
										if($flag_pihak_ketiga==1) {
											print('<td>'.$row->pihak_ketiga_nama.'</td>');
											print('<td>'.$row->pihak_ketiga_alamat.'</td>');
											print('<td>'.$row->pihak_ketiga_no_telepon.'</td>');
											print('<td>'.$item->status.'</td>');
											print('</tr>');
										} else {
											print('<tr>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											print('<td>&nbsp;</td>');
											
											print('<td>'.$row->pihak_ketiga_nama.'</td>');
											print('<td>'.$row->pihak_ketiga_alamat.'</td>');
											print('<td>'.$row->pihak_ketiga_no_telepon.'</td>');
											print('<td>'.$item->status.'</td>');
											print('</tr>');
										}
									endforeach;
								}

							}
							else {
								print('<td colspan="4" align="center">&nbsp;</td>');
								print('</tr>');
							}
							?>
					<?php } ?>


<!-- ---------------------------------------------------------------------------- -->
				

				<?php $no++; endforeach; 
			}?>
		</tbody>
	</table>
	
</body>
</html>