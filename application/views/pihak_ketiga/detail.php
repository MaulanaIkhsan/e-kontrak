<?php
$this->load->view('template/header');?>
<!-- Select2 -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/select2/dist/css/select2.min.css')); ?>">
<?php $this->load->view('template/asset_header');?>
<link rel="stylesheet" type="text/css" href="<?php print(base_url('assets/bower_components/datatables.net-bs/css/custom_button_export.css'))?>" />
<link href="<?php echo base_url('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css')?>" rel="stylesheet">

<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <!-- Box Form Pengunjung -->
        <?php 
            print($this->session->flashdata('alert')); 
            print($this->session->flashdata('success')); 
            $pihak_ketiga_id = $this->session->userdata('pihak_ketiga_id');

            $data['jenis_dokumen'] 		= $jenis_dokumen;
            $data['jenjang_pendidikan'] = $jenjang_pendidikan;
            $data['jabatan_perusahaan'] = $jabatan_perusahaan;
        ?>
        
        <div class="col-md-12">
            <?php if($flag_dokumen==FALSE) { ?>
                <div class="alert alert-warning fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a> 
                    Mohon lengkapi dokumen kelengkapan pengadaan terlebih dahulu
                </div>
            <?php } ?>

            <?php if($flag_karyawan == FALSE) { ?>
                <div class="alert alert-warning fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a> 
                    Silahkan masukkan daftar karyawan pada "<strong>Data Karyawan</strong>"
                </div>
            <?php } else { 
                if($flag_karyawan_admin == FALSE) {?>
                <div class="alert alert-warning fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a> 
                    Harap pilih salah satu karyawan untuk menjadi admin perusahaan
                </div>
            <?php }
            } ?>
        </div>
        
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-warning" id="form-pengunjung">
              <div class="box-header with-border">
                <h3 class="box-title">Detail Penyedia Barang/Jasa</h3>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <form class="form-horizontal" method="post" action="<?php print(base_url('pengunjung/add'))?>">
                <div class="box-body">
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Nama Perusahaan</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_nama_perusahaan"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">NPWP</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_npwp"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Alamat Lengkap</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_alamat_lengkap"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">No Telepon</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_no_telepon"></div>
                    </div>
                  </div>  
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_email"></div>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Status</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_status"></div>
                    </div>
                  </div>  
                  <div class="form-group data-pusat">
                    <label for="namalengkap" class="col-sm-2 control-label">Alamat Pusat</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_alamat_pusat"></div>
                    </div>
                  </div>
                  <div class="form-group data-pusat">
                    <label for="namalengkap" class="col-sm-2 control-label">No Telepon Pusat</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_no_telepon_pusat"></div>
                    </div>
                  </div>
                  <div class="form-group data-pusat">
                    <label for="namalengkap" class="col-sm-2 control-label">Email Pusat</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_email_pusat"></div>
                    </div>
                  </div>                  
                </div>
                <div class="box-footer">
                    <?php 
                    $sess_data = $this->session->userdata('session_data');
                    if($sess_data['as']!=='pihak_ketiga') { ?>
                        <a href="<?php print(base_url('pihak_ketiga'));?>" class="btn btn-primary">Kembali</a>
                    <?php } ?>
                  <a href="#" class="btn btn-primary" onclick="edit_pihak_ketiga()">Edit</a>
                  <a href="#" class="btn btn-danger" onclick="delete_data()">Delete</a>
                </div>
                <!-- /.box-footer -->
              </form>

                <!-- Tabs Content -->
                <div class="nav-tabs-custom">
                    <!-- Tabs within a box -->
                    <ul class="nav nav-tabs pull-left">
                        <li class="active"><a href="#data-dokumen" data-toggle="tab">Dokumen Pelengkap</a></li>
                        <li><a href="#data-karyawan" data-toggle="tab">Data Karyawan</a></li>
                        
                    </ul>
                    <div class="tab-content no-padding">
                        <div class="chart tab-pane active" id="data-dokumen" style="position: relative;">
                            <p>
                                <a href="#" class="btn btn-primary" onclick="add_dokumen()">Upload Dokumen</a>
                            </p>
                            <table id="tabel_dokumen" class="table table-bordered table-striped table-responsive">
                                <thead>
                                    <tr class="headings" align="center">
                                        <th class="column-title" align="center">No</th>
                                        <th class="column-title" align="center">Nama Dokumen</th>
                                        <th class="column-title" align="center">Jenis Dokumen</th>
                                        <th class="column-title" align="center">Tgl Awal Aktif</th>
                                        <th class="column-title" align="center">Tgl Akhir Aktif</th>
                                        <th class="column-title" align="center">File</th>
                                        <th class="column-title" align="center">Aktif</th>
                                        <th class="column-title" align="center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <div class="chart tab-pane" id="data-karyawan" style="position: relative; width:100%;">
                            <p>
                                <a href="#" class="btn btn-primary" onclick="add_karyawan()">Insert Data</a>
                            </p>
                            <table id="tabel_karyawan" class="table table-bordered table-striped table-responsive">
                                <thead>
                                    <tr class="headings" align="center">
                                        <th class="column-title" align="center">No</th>
                                        <th class="column-title" align="center">Nama</th>
                                        <th class="column-title" align="center">Jabatan</th>
                                        <th class="column-title" align="center">Alamat</th>
                                        <th class="column-title" align="center">Telepon</th>
                                        
                                        <th class="column-title" align="center">User?</th>
                                        <th class="column-title" align="center">Dokumen Profil</th>
                                        <th class="column-title" align="center">Dokumen Pendidikan</th>
                                        <th class="column-title" align="center">Dokumen Lainnya</th>
                                        <th class="column-title" align="center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                       
                    </div>
                </div>
                <!-- /Tabs Content -->

            </div>
          </div>
          <!-- /Box Form Pengunjung -->

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/dataTables.buttons.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.flash.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/jszip.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/pdfmake.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/vfs_fonts.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.html5.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.print.min.js'))?>"></script>
<script src="<?php echo base_url('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')?>"></script>
<!-- Select2 -->
<script src="<?php print(base_url('assets/bower_components/select2/dist/js/select2.full.min.js'));?>"></script>
<script>
    var save_method_dokumen, //for save method flag dokumen
        table_dokumen,
        save_method_karyawan, //for save method flag karyawan
        table_karyawan;

    $(function(){
        $.fn.dataTable.ext.errMode = 'none';
        detail_data();

        $('.data-akun').hide();

        table_dokumen = $('#tabel_dokumen').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
            "ajax": {
                "url": "<?php echo site_url('dokumen_pihak_ketiga/get_data/'.$pihak_ketiga_id);?>",
                "type": "POST"
            },
            dom: 'lBfrtip',
            button:[{
              extend:'pdf',
              oriented:'potrait',
              pageSize: 'A4',
              title : 'Data Dokumen Pihak Ketiga',
              download : 'open'
            }, 'copy', 'csv', 'excel', 'pdf', 'print'],
            "columnDefs": [{ 
                "targets": [ 0 ], 
                "orderable": false, 
            }]
        });

        table_karyawan = $('#tabel_karyawan').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
            "ajax": {
                "url": "<?php echo site_url('karyawan/get_data/'.$pihak_ketiga_id);?>",
                "type": "POST"
            },
            "autoWidth": false,
            dom: 'lBfrtip',
            button:[{
              extend:'pdf',
              oriented:'potrait',
              pageSize: 'A4',
              title : 'Data Dokumen Karyawan',
              download : 'open'
            }, 'copy', 'csv', 'excel', 'pdf', 'print'],
            "columnDefs": [{ 
                "targets": [ 0 ], 
                "orderable": false, 
            }]
        });

        $('.select2').select2({
            closeOnSelect: false
        });

        $("input").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $("textarea").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $("select").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();

            if($('[name="status"]').val()=='Cabang') {
                $('.data-pusat').show();
            }
            else {
                $('.data-pusat').hide();
                $('[name="alamat_pusat"]').val('');
                $('[name="no_telepon_pusat"]').val('');
                $('[name="email_pusat"]').val('');
            }

            if($('[name="is_user"]').val()=='y') {
                $('.data-akun').show();
            }
            else {
                $('.data-akun').hide();
                $('[name="username"]').val('');
                $('[name="password"]').val('');
            }
        });

        $('.datepicker').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
            todayHighlight: true,
            todayBtn: true,
            todayHighlight: true,  
        });

        //Just number input
        $(".number").on("keypress keyup",function (event) {    
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        // Upload Dokumen
        $('#form-upload-dokumen').submit(function(obj){
            obj.preventDefault();
            if(save_method_dokumen=='add') {
                url_dokumen = "<?php echo site_url('dokumen_pihak_ketiga/ajax_add/')?>";
            } else {
                url_dokumen = "<?php echo site_url('dokumen_pihak_ketiga/ajax_update/')?>";
            }
            $.ajax({
                url: url_dokumen,
                type: 'POST',
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(datas){
                    // Parsing to object
                    datas=JSON.parse(datas);                    
                    
                    //if success close modal and reload ajax table dokumen
                    if(datas.hasOwnProperty('status') && datas.status) 
                    {
                        alert('Data berhasil tersimpan');
                        $('#modal_form_upload_dokumen').modal('hide');
                        reload_tabel_dokumen();
                        $('#form-upload-dokumen')[0].reset(); // reset form on modals
                    }
                    else
                    {
                        if(datas.hasOwnProperty('inputerror')) {
                            for (var i = 0; i < datas.inputerror.length; i++) 
                            {
                                $('[name="'+datas.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                $('[name="'+datas.inputerror[i]+'"]').next().text(datas.error_string[i]); //select span help-block class set text error string
                            }
                        }
                    }
                    $('#btnSaveDokumen').text('save'); //change button text
                    $('#btnSaveDokumen').attr('disabled',false); //set button enable 
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error calling save data. Please check again');
                    $('#modal_form_upload_dokumen').modal('hide');
                }
            });
        });

        // Upload Dokumen
        $('#form-karyawan').submit(function(obj){
            obj.preventDefault();
            if(save_method_karyawan=='add') {
                url_dokumen = "<?php echo site_url('karyawan/ajax_add/')?>";
            } else {
                url_dokumen = "<?php echo site_url('karyawan/ajax_update/')?>";
            }
            $.ajax({
                url: url_dokumen,
                type: 'POST',
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(datas){
                    console.log(datas);

                    // Parsing to object
                    datas=JSON.parse(datas);                    
                    
                    //if success close modal and reload ajax table dokumen
                    if(datas.hasOwnProperty('status') && datas.status) 
                    {
                        alert('Data berhasil tersimpan');
                        $('#modal_form_karyawan').modal('hide');
                        reload_tabel_karyawan();
                        $('#form-karyawan')[0].reset(); // reset form on modals
                    }
                    else
                    {
                        if(datas.hasOwnProperty('inputerror')) {
                            for (var i = 0; i < datas.inputerror.length; i++) 
                            {
                                $('[name="'+datas.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                $('[name="'+datas.inputerror[i]+'"]').next().text(datas.error_string[i]); //select span help-block class set text error string
                            }
                        }
                    }
                    $('#btnSaveDokumen').text('save'); //change button text
                    $('#btnSaveDokumen').attr('disabled',false); //set button enable 
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error calling save data. Please check again');
                    $('#modal_form_karyawan').modal('hide');
                }
            });
        });
    });

    //reload datatable ajax tabel dokumen
    function reload_tabel_dokumen() {
        table_dokumen.ajax.reload(); 
    }

    function reload_tabel_karyawan() {
        table_karyawan.ajax.reload(); 
    }

    function detail_data() {
        $.ajax({
            url: "<?php echo site_url('pihak_ketiga/ajax_edit/'.$pihak_ketiga_id)?>",
            method: 'GET',
            type: 'JSON',
            success: function(data) {
                // Parsing to object
                data=JSON.parse(data);
                
                //Show and hide data Pusat Perusahaan
                if(data.status=='Pusat') {
                    $('.data-pusat').hide();
                }
                else {
                    $('#detail_alamat_pusat').text(data.alamat_pusat);
                    $('#detail_no_telepon_pusat').text(data.no_telepon_pusat);
                    $('#detail_email_pusat').text(data.email_pusat);
                }

                $('#detail_nama_perusahaan').html(data.nama_perusahaan);
                $('#detail_npwp').html(data.npwp);
                $('#detail_alamat_lengkap').text(data.alamat_lengkap);
                $('#detail_no_telepon').text(data.no_telepon);
                $('#detail_email').text(data.email);
                $('#detail_status').text(data.status);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error while load detail data pihak ketiga !');
            }
        });
    }

    function delete_data() {
        if(confirm('Apakah anda yakin akan menghapus data ini ?')){
            $.ajax({
                url: "<?php echo site_url('pihak_ketiga/ajax_delete/'.$pihak_ketiga_id)?>",
                method: 'POST',
                type: 'JSON',
                success: function(){
                    window.location.replace("<?php print(base_url('pihak_ketiga')); ?>");
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error while delete data data pihak ketiga !');
                }
            });
        }
    }

    function edit_pihak_ketiga() {
        save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('pihak_ketiga/ajax_edit/'.$pihak_ketiga_id)?>",
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id);
                $('[name="nama_perusahaan"]').val(data.nama_perusahaan);
                $('[name="npwp"]').val(data.npwp);
                $('[name="alamat_lengkap"]').val(data.alamat_lengkap);
                $('[name="no_telepon"]').val(data.no_telepon);
                $('[name="email"]').val(data.email);
                $('[name="status"]').val(data.status);

                if($('[name="status"]').val()=='Cabang') {
                    $('.data-pusat').show();
                    $('[name="alamat_pusat"]').val(data.alamat_pusat);
                    $('[name="no_telepon_pusat"]').val(data.no_telepon_pusat);
                    $('[name="email_pusat"]').val(data.email_pusat);
                }
                else {
                    $('.data-pusat').hide();
                    $('[name="alamat_pusat"]').val('');
                    $('[name="no_telepon_pusat"]').val('');
                    $('[name="email_pusat"]').val('');
                }
                
                $('#modal_form_pihak_ketiga').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Update Data'); // Set title to Bootstrap modal title

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    function save_pihak_ketiga()
    {        
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable 

        // ajax adding data to database
        $.ajax({
            url : "<?php echo site_url('pihak_ketiga/ajax_update')?>",
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                //if success close modal and reload ajax table
                if(data.status) 
                {
                    $('#modal_form_pihak_ketiga').modal('hide');
                    alert('Data berhasil tersimpan');
                    detail_data();
                }
                else
                {
                    for (var i = 0; i < data.inputerror.length; i++) 
                    {
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    }
                }
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 


            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error while process updating data pihak ketiga');
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 

            }
        });
    }

    // Upload Dokumen
    function add_dokumen() {
        save_method_dokumen = 'add';
        $('#form-upload-dokumen')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form_upload_dokumen').modal('show'); // show bootstrap modal
        $('.modal-title').text('Upload Dokumen'); // Set Title to Bootstrap modal title
    }

    // Delete dokumen
    function delete_dokumen(id)
    {
        if(confirm('Are you sure delete this data?'))
        {
            
            // ajax delete data to database
            $.ajax({
                url : "<?php echo site_url('dokumen_pihak_ketiga/ajax_delete')?>/"+id,
                method: 'POST',
                type: 'JSON',
                success: function(datas)
                {
                    alert('Data berhasil dihapus');
                    reload_tabel_dokumen();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });

        }
    }

    function edit_dokumen(id) {
        save_method_dokumen = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('dokumen_pihak_ketiga/ajax_edit/')?>/"+id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id);
                $('[name="nama_dokumen"]').val(data.nama_dokumen);
                $('[name="jenis_dokumen"]').val(data.jenis_dokumen_id);
                $('[name="tgl_awal_aktif"]').datepicker('update', data.tgl_awal_aktif);
                $('[name="tgl_akhir_aktif"]').datepicker('update', data.tgl_akhir_aktif);
                $('[name="is_aktif"]').val(data.is_aktif);
                
                $('#modal_form_upload_dokumen').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Update Data'); // Set title to Bootstrap modal title

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    // Dokumen karyawan
    function add_karyawan() {
        save_method_karyawan = 'add';
        $('#form-karyawan')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form_karyawan').modal('show'); // show bootstrap modal
        $('.modal-title').text('Data Karyawan'); // Set Title to Bootstrap modal title

        $('.data-akun').hide();
        $('[name="username"]').val('');
        $('[name="password"]').val('');
    }

    // Delete karyawan
    function delete_karyawan(id)
    {
        if(confirm('Are you sure delete this data?'))
        {
            
            // ajax delete data to database
            $.ajax({
                url : "<?php echo site_url('karyawan/ajax_delete')?>/"+id,
                method: 'POST',
                type: 'JSON',
                success: function(datas)
                {
                    alert('Data berhasil dihapus');
                    reload_tabel_karyawan();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });

        }
    }

    function edit_karyawan(id) {
        save_method_karyawan = 'update';
        $('#form-karyawan')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('karyawan/ajax_edit/')?>/"+id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id);
                $('[name="nama_lengkap"]').val(data.nama_lengkap);
                $('[name="jabatan_id"]').val(data.jabatan_id);
                $('[name="jenjang_pendidikan_id"]').val(data.pendidikan_id);
                $('[name="jenis_kelamin"]').val(data.jenis_kelamin);
                $('[name="alamat_lengkap"]').val(data.alamat_lengkap);
                $('[name="tgl_lahir"]').datepicker('update', data.tgl_lahir);
                $('[name="no_telepon"]').val(data.no_telepon);
                $('[name="email"]').val(data.email);
                $('[name="is_user"]').val(data.is_user);

                if(data.is_user=='y') {
                    $('.data-akun').show();
                    $('[name="username"]').val(data.username);
                }
                
                $('#modal_form_karyawan').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Update Data'); // Set title to Bootstrap modal title

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

</script>

<?php 
    // Load Modal Form
    $this->load->view('pihak_ketiga/modal_form', $data);
    $this->load->view('dokumen_pihak_ketiga/modal_form', $data);
    $this->load->view('karyawan/modal_form', $data);
?>

<?php $this->load->view('template/footer');?>