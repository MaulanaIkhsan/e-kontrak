<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pekerjaan extends CI_Controller {

	// ----- Pekerjaan -----
	// id
	// nama_pekerjaan
	// kode_rekening
	// anggaran_awal
	// tahun
	// kode_pekerjaan
	// kode_progam
	// tahap_pekerjaan
	// uniq_code
	// keterangan
	// created_at
	// updated_at
	
	var $status = array(
				'Belum' 	=> 'Belum', 
				'Selesai' 	=> 'Selesai', 
				'Berhenti' 	=> 'Berhenti',
				'Putus' 	=> 'Putus Kontrak'
			);
	
	public function __construct()
	{
		parent::__construct();
		
		$sess_data = $this->session->userdata('session_data');
        if($sess_data===NULL) {
            $this->session->set_flashdata('warning', '<div class="alert alert-warning fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a> 
                Harap login terlebih dahulu untuk mengakses sistem</div>');
			redirect('auth');
		}
		
		//Load Dependencies
		$this->load->model('Pekerjaan_model', 'pekerjaan');
		$this->load->helper('generic');
		
	}

	public function beranda()
	{
		$this->load->view('beranda');
	}

	public function form_kontrak()
	{
		$this->load->model('Partner_model', 'partner');
		$data['partner'] = $this->partner->get_partner();
		$this->load->view('pekerjaan/form_kontrak', $data);
	}
	// List all your items
	public function index()
	{
		$user_id = $this->session->userdata('id');
		$role = $this->session->userdata('role');
		$data['pekerjaan'] = $this->pekerjaan->show($user_id, $role);

		$this->load->view('pekerjaan/show', $data);
	}

	// Add a new item
	public function add()
	{
		$this->load->model('Users_model', 'users');
		$this->load->model('Jenis_pengadaan_model', 'pengadaan');
		$this->load->model('Jenis_pekerjaan_model', 'tipe_pekerjaan');
		$this->load->model('Program_model', 'program');

		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama', 'Nama Pekerjaan','required');
		$this->form_validation->set_rules('pendamping1_id', 'Penanggung Jawab','required');
		$this->form_validation->set_rules('anggaran', 'Anggaran','required|numeric');
		$this->form_validation->set_rules('jenis_pengadaan', 'Jenis Pengadaan','required');
		$this->form_validation->set_rules('jenis_pekerjaan', 'Nama Pekerjaan','required');
		$this->form_validation->set_rules('nilai_kontrak', 'Nilai Kontrak','numeric');
		$this->form_validation->set_rules('status', 'Status Pekerjaan','required');

		$current_program = $this->session->userdata('id_program');
		$get_program = $this->program->get_data_program($current_program);

		if ($this->form_validation->run() == FALSE) {
			// load view form add pekerjaan
			$value['user'] = $this->users->get_user('pptk');
			$value['status_kerja'] = $this->status;
			$value['jenis_pengadaan'] = $this->pengadaan->get_jenis_pengadaan();
			$value['jenis_pekerjaan'] = $this->tipe_pekerjaan->get_jenis_pekerjaan();
			$value['program'] = $get_program;
			$this->load->view('pekerjaan/add', $value);
		} else {
			// insert to pekerjaan table
			if($this->input->post('simpan')!==NULL) {
				$data['nama_pekerjaan'] = $this->input->post('nama');
				$data['anggaran_awal'] = $this->input->post('anggaran');
				$data['pengadaan_id'] = $this->input->post('jenis_pengadaan');
				$data['jenis_pekerjaan_id'] = $this->input->post('jenis_pekerjaan');
				$data['nilai_kontrak'] = $this->input->post('nilai_kontrak');
				$data['status'] = $this->input->post('status');
				$data['keterangan'] = $this->input->post('keterangan');
				$data['created_by'] = $this->session->userdata('id');
				$data['updated_by'] = $this->session->userdata('id');
				$data['pendamping1_id'] = $this->input->post('pendamping1_id');
				$data['kode_program'] = $get_program->kode_program;

				if(($data['nilai_kontrak']!==NULL) && $data['anggaran_awal']<$data['nilai_kontrak']){
					$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Nilai kontrak harus lebih kecil atau sama dengan Nilai Anggaran. Mohon isikan kembali</div>');
					redirect('pekerjaan/add');
				}

				$add = $this->pekerjaan->insert($data);
				if($add==='ok') {
					$this->session->set_flashdata('success', '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Data pekerjaan '.$data['nama_pekerjaan'].' berhasil tersimpan</div>');
					redirect('pekerjaan/detail/'.$this->session->userdata('id_pekerjaan'));
				}
				else {
					$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Data pekerjaan '.$data['nama_pekerjaan'].' gagal tersimpan. Mohon cek kembali</div>');
					redirect('pekerjaan/detail/'.$this->session->userdata('id_pekerjaan'));
				}
			}
			else {
				$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Harap masukkan data pekerjaan melalui tombol <strong>Tambah Baru</strong> !</div>');
				redirect('pekerjaan/detail/'.$this->session->userdata('id_pekerjaan'));
			}
		}
		
	}

	//Update one item
	public function update($id)
	{
		$this->load->model('Users_model', 'users');
		$this->load->model('Jenis_pengadaan_model', 'pengadaan');
		$this->load->model('Jenis_pekerjaan_model', 'tipe_pekerjaan');
		$this->load->model('Program_model', 'program');
		$this->load->model('Kontrak_model', 'kontrak');

		$this->load->library('form_validation');
		$this->form_validation->set_rules('nama_program', 'Nama_Program','trim');
		$this->form_validation->set_rules('nama_pekerjaan', 'Nama_Pekerjaan','trim');
		$this->form_validation->set_rules('kode_rekening', 'Kode_Rekening','required');

		$current_program = $this->session->userdata('id_program');
		$get_program = $this->program->get_data_program($current_program);

		if ($this->form_validation->run() == FALSE) {
			$value['pekerjaan'] = $this->pekerjaan->show_update($id);
			$this->load->view('pekerjaan/update', $value);
		}
		else {
			if($this->input->post('update')!==NULL){
				$data['kode_rekening'] = $this->input->post('kode_rekening');

				$key['id'] = $id;

				$edit = $this->pekerjaan->update($data, $key);
				if($edit==='ok'){
					$this->session->set_flashdata('success', '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Data pekerjaan  berhasil diubah </div>');
					redirect('pekerjaan/detail/'.$id);
				}
				else {
					$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Data pekerjaan gagal diperbarui. Harap cek kembali </div>');
					redirect('pekerjaan/detail/'.$id);
				}
			}
			else {
				$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Harap lakukan pengubahan data pekerjaan melalui tombol <strong>Ubah</strong> !</div>');
				redirect('program/detail/'.$this->session->userdata('id_program'));
			}
		}
		
	}

	public function download_excel($id)
	{
		$this->load->model('Riwayat_anggaran_model', 'riwayat_anggaran');

		$kode_pekerjaan = $this->pekerjaan->get_uniq_code_pekerjaan($id);

		$data['detail'] = $this->pekerjaan->detail($id);
		$data['serapan'] = $this->pekerjaan->resume_aktivitas_by_pekerjaan($id);
		if($kode_pekerjaan!==NULL) {
			$data['anggaran'] = $this->riwayat_anggaran->show($kode_pekerjaan->uniq_code);
		}
		$this->load->view('pekerjaan/resume_excel', $data);
	}

	//Delete one item
	public function delete($id)
	{
		if($this->input->post('delete')!==NULL) {
			$key['id'] = $id;
			$delete = $this->pekerjaan->delete($key);

			if($delete==='ok'){
				$this->session->set_flashdata('success', '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Data pekerjaan  berhasil terhapus </div>');
				redirect('program/detail/'.$this->session->userdata('id_program'));
			}
			else {
				$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Data pekerjaan gagal dihapus. Harap cek kembali </div>');
				redirect('pekerjaan/detail/'.$id);
			}
		}
		else {
			$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Harap lakukan hapus data pekerjaan melalui tombol <strong>Hapus</strong> !</div>');
			redirect('program/detail/'.$this->session->userdata('id_program'));
		}
	}

	public function detail($id)
	{
		// ----- aktivitas -----
		// id
		// nama_aktivitas
		// anggaran_awal
		// perubahan_anggaran
		// hps
		// nilai_kontrak
		// tgl_pengajuan_pencairan
		// tgl_pencairan
		// keterangan
		// pptk
		// ppkom
		// bendahara
		// pekerjaan_id
		// pengadaan_id
		// jenis_pekerjaan_id
		// created_at
		// updated_at
		// created_by
		// updated_by

		$this->load->library('grocery_CRUD');

		$this->load->model('Riwayat_anggaran_model', 'riwayat_anggaran');
		
		$kode_pekerjaan = $this->pekerjaan->get_uniq_code_pekerjaan($id);

		$data['detail'] = $this->pekerjaan->detail($id);
		
		if($kode_pekerjaan!==NULL) {
			$data['anggaran'] = $this->riwayat_anggaran->show($kode_pekerjaan->uniq_code);
		}
		$this->session->set_userdata('id_pekerjaan', $id);

		$this->load->view('pekerjaan/detail', $data);
	}

	function before_insert_aktivitas_callback($post_array, $primary_key=null)
	{
		$curr_pekerjaan = $this->session->userdata('id_pekerjaan');
		$sess_data = $this->session->userdata('session_data');
		
		$post_array['pekerjaan_id'] = $sess_data['id_pekerjaan'];
		$post_array['created_by'] = $sess_data['id'];
		$post_array['updated_by'] = $sess_data['id'];

		// return $post_array;
	}

	function before_update_aktivitas_callback($post_array, $primary_key=null)
	{
		$curr_pekerjaan = $this->session->userdata('id_pekerjaan');
		$sess_data = $this->session->userdata('session_data');

		$post_array['pekerjaan_id'] = $sess_data['id_pekerjaan'];
		$post_array['updated_by'] = $sess_data['id'];

		return $post_array;
	}

	public function format_date($date, $format)
	{
		$date = date_create($date);
		if($format === 1){
			return date_format($date, 'Y-m-d');
		}
		else {
			return date_format($date, 'd-M-Y');
		}
		
		
		// print('<br/>');

		// $date2 = date_create("2018-03-05");
		// print(date_format($date2, 'd-M-Y'));

	}

	public function statistik()
	{
		$user_id = $this->session->userdata('id');
		$role = $this->session->userdata('role');
		$data['pengadaan'] = $this->pekerjaan->stat_jenis_pengadaan($user_id, $role);
		$data['pekerjaan'] = $this->pekerjaan->stat_jenis_pekerjaan($user_id, $role);
		$data['status'] = $this->pekerjaan->stat_status_pekerjaan($user_id, $role);

		// print(json_encode($data['status']));
		$this->load->view('beranda', $data);
	}

	public function get_data($kode_program)
	{
		$user_id = $this->session->userdata('id');
		$role = $this->session->userdata('role');

		$list = $this->pekerjaan->get_datatables($kode_program, $user_id, $role);
		// print_r($this->db->last_query());
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			// 'nama','pptk', 'jenis_pengadaan', 'jenis_pekerjaan', 'status'
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->nama_pekerjaan;
			$row[] = $field->tahun;
			$row[] = 'Rp '.number_format($field->anggaran_awal, 0, '.','.').',-';;
			$row[] = $field->tahap_pekerjaan;
			$row[] = "<a href=".base_url('pekerjaan/detail/'.$field->id)." class='btn btn-primary'>Detail</a>";
			$row[] = "";

			$data[] = $row;
		}

		// print_r($data);
		// print('<br/>');
		// print(count($data).'<br/>');
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->pekerjaan->count_all($kode_program, $user_id, $role),
			"recordsFiltered" => $this->pekerjaan->count_filtered($kode_program, $user_id, $role),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function sample()
	{
		$password = 'dpusmg123';
		$encode = base64_encode($password);
		$decode = base64_decode($encode);
		print('encode base64 : '.$encode.'<br/>');
		print('karakter : '.strlen($encode).'<br/>');
		print('decode base64 : '.$decode.'<br/>');
		print('karakter : '.strlen($decode).'<br/>');

		// ZHB1czNtNHI0bmcxMjM=
		// ZHB1c2VtYXJhbmcxMjM=	
	}

}

/* End of file Pekerjaan.php */
/* Location: ./application/controllers/Pekerjaan.php */
