<!-- Bootstrap modal -->
<?php 
$sess_data  = $this->session->userdata('session_data');
$user_role  = $sess_data['role'];
$user_id    = $sess_data['id'];
$user_as    = $sess_data['as'];
?>
<div class="modal fade" id="modal_form_kontrak_pihak_ketiga" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Upload</h4>
            </div>
            <div class="modal-body form">
                <form id="form-kontrak-pihak-ketiga" class="form-horizontal" enctype="multipart/form-data" method="post">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Pihak Ketiga</label>
                            <div class="col-md-9">
                                <?php if($user_as=='pegawai') {?>
                                    <select name="pihak_ketiga_id" class="form-control">
                                        <option value="">--Select Pihak Ketiga--</option>
                                        <?php foreach($pihak_ketiga as $item):?>
                                            <option value="<?php print($item->id); ?>"><?php print($item->nama_perusahaan); ?></option>
                                        <?php endforeach;?>
                                    </select>
                                <?php } elseif($user_as=='pihak_ketiga') { ?>
                                    <input type="hidden" name="pihak_ketiga_id" placeholder="pihak_ketiga_id" class="form-control money" />
                                    <input type="text" name="pihak_ketiga_nama" placeholder="pihak_ketiga_nama" class="form-control money" readonly="readonly" />
                                <?php } ?>
                                
                                <span class="help-block"></span>
                            </div>
                        </div>
                        
                        <?php if(($user_as=='pegawai' and $user_role=='Super Admin') or $user_as=='pihak_ketiga') {?>
                            <div class="form-group">
                                <label class="control-label col-md-3">Nama Bank</label>
                                <div class="col-md-9">
                                    <select name="nama_bank" class="form-control">
                                        <option value="">--Select Nama Bank --</option>
                                        <?php foreach($bank as $item):?>
                                            <option value="<?php print($item->id); ?>"><?php print($item->nama); ?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Nomor Rekening</label>
                                <div class="col-md-9">
                                    <input name="no_rekening" placeholder="Nomor Rekening" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Nilai Penawaran</label>
                                <div class="col-md-9">
                                    <input name="nilai_kontrak" placeholder="Nilai Kontrak" class="form-control money" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        <?php } ?>                        

                    </div>
                
            </div>
            <div class="modal-footer">
                    <button type="submit" id="btnSaveKontrakPihakKetiga" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->