<!DOCTYPE html>
<HtmL>

<HeAd>
    <MeTa HTTP-EquIV="CONTENT-TYPE" CONTent="text/html; charset=utf-8">
    <TItlE>Semarang, 29 Maret 2004</tITLE>
    <meta name=GENERATOR coNTeNT="LibreOffice 4.1.6.2 (Linux)">
    <Meta nAMe="AUTHOR" CONTeNt="DPU">
    <MEtA NaMe=CREATED cONtENt="Tahun_Anggaran0817;20400000000000">
    <MeTA NAmE="CHANGEDBY" CoNteNt="isan">
    <META NAme="CHANGED" coNTent="20190320;163019000000000">
    <MEtA nAmE=KSOProductBuildVer CONTEnt=1033-10.1.0.6757>
    <sTyLE>
        @page {
            size: 8.47in 13.98in;
            margin-right: 0.88in;
            margin-top: 0.59in;
            margin-bottom: 0.69in
        }
        P {
            margin-bottom: 0.08in;
            direction: ltr;
            color: #000000
        }
        P.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
        H1 {
            margin-left: 3.5in;
            margin-top: 0in;
            margin-bottom: 0in;
            direction: ltr;
            color: #000000;
            text-align: justify;
            text-decoration: underline
        }
        H1.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
    </sTYLE>
</heAd>

<bOdY LANg="en-US" tExT=#000000 dIR="LTR">
    <?php
    $dayname    = dayname(date('l', strtotime($kontrak_surat->tgl_surat)));
    $tglindo    = tgl_indo($kontrak_surat->tgl_surat);
    $split_tgl  = explode(' ', $tglindo);
    ?>
    <P LAng=pt-BR CLaSs="western" ALIGN=CENTER STylE="margin-bottom: 0in;">
        <b>BERITA ACARA<br/>
        EVALUASI, KLARIFIKASI DAN NEGOSIASI<br/>
        ---------------------------------------------------------------------------------------------------------------------
        </b><br/>
        Nomor : <?php print($kontrak_surat->no_surat); ?>
    </p>
    <p ClasS=western ALIGN=JUSTIFY sTyle="margin-bottom: 0in">
        Pada hari ini <b><?php print($dayname); ?></b>, tanggal <b><?php print(terbilang($split_tgl[0])); ?></b> bulan <b><?php print(strtolower($split_tgl[1])); ?></b> tahun <b><?php print(terbilang($split_tgl[2])); ?> (<?php print($tglindo); ?>) </b> , Pejabat Pengadaan pada Dinas Pekerjaan Umum Kota Semarang Tahun Anggaran Tahun_Anggaran, yang dibentuk berdasarkan Keputusan Kepala Dinas Pekerjaan Umum Kota Semarang Selaku Pengguna Anggaran <?php print($pejabat_pengadaan->sk_no); ?> tanggal <?php print(tgl_indo($pejabat_pengadaan->sk_tgl_penetapan)); ?> tentang Penunjukan Pejabat Pengadaan Pada Dinas Pekerjaan Umum Kota Semarang Tahun Anggaran <?php print($pejabat_pengadaan->sk_tahun); ?>, telah melakukan rapat Evaluasi, Klarifikasi dan Negosiasi dalam rangka Pengadaan Langsung.
    </p>
    <br/>
    <table>
        <tr>
            <td class="text">Program</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->program_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Kegiatan</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->pekerjaan_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Pekerjaan</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->aktivitas_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Sumber Dana</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->aktivitas_sumber_dana); ?></td>
        </tr>
    </table>
    <br/>
    
    <p LaNg="pt-BR" ClaSs=western ALIGN=JUSTIFY style="margin-bottom: 0in">
        <strong>PESERTA RAPAT</strong><br/>
        <?php print($pejabat_pengadaan->jenis_pejabat_pengadaan_nama); ?> <?php print($pejabat_pengadaan->seri_pejabat_pengadaan_nama); ?><br/>
        <?php print($kontrak_pekerjaan->pejabat_pengadaan_nama);?><br/>
        <br/>
        Calon Penyedia Jasa : <br/>
        <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama);?><br/>
        beralamat <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_alamat);?><br/>

    </P>
    <BR>
    <ol type="A">
        <li>
            <strong>PENELITIAN DAN PENILAIAN ADMINISTRASI</strong>
            <TAblE WIDTH=634 CELLPADDING=7 CELLSPACING=0>
                <cOl WIDTH=23>
                    <cOl WIDTH=203>
                        <Col WIDTH=52>
                            <cOl WIDTH=80>
                                <cOL WIDTH=62>
                                    <COL WIDTH=127>
                                        <tR>
                                            <tD ROWSPAN=3 ALIGN=CENTER WIDTH=23 STYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p Class="western" ALIGN=CENTER>
                                                    <fOnT SIZE=2 StYLe="font-size: 11pt"><b>NO</b></FONt>
                                                </P>
                                            </td>
                                            <Td ROWSPAN=3 ALIGN=CENTER WIDTH=203 StyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p CLAss=western ALIGN=CENTER>
                                                    <fOnT SIZE=2 stylE="font-size: 11pt"><B>NAMA
                    DOKUMEN</b></FoNT>
                                                </p>
                                            </Td>
                                            <TD COLSPAN=2 WIDTH=147 ALIGN=CENTER STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P class=western><font SIZE=2 Style="font-size: 11pt"><b>KELENGKAPAN</b></fONT>
                                                </p>
                                            </Td>
                                            <Td ROWSPAN=2 ALIGN=CENTER WIDTH=62 StYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P claSS="western" ALIGN=CENTER><font SIZE=2 sTYle="font-size: 11pt"><b>Tdk
                    ada</b></FONt>
                                                </P>
                                            </Td>
                                            <tD ROWSPAN=2 ALIGN=CENTER WIDTH=127 stylE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <P ClASS="western" ALIGN=CENTER>
                                                    <FoNT SIZE=2 sTylE="font-size: 11pt"><B>KETERANGAN</b></FONt>
                                                </P>
                                            </tD>
                                        </TR>
                                        <tr>
                                            <Td COLSPAN=2 ALIGN=CENTER WIDTH=147 sTylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P CLass=western ALIGN=CENTER>
                                                    <FoNT SIZE=2 sTYLE="font-size: 11pt"><b>Ada
                    (+)</b></FoNT>
                                                </p>
                                            </Td>
                                        </Tr>
                                        <tR>
                                            <td WIDTH=52 ALIGN=CENTER stYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p clASs=western ALIGN=CENTER>
                                                    <Font SIZE=2 STYLE="font-size: 11pt"><b>Sesuai</B></fOnT>
                                                </p>
                                            </TD>
                                            <td WIDTH=80 ALIGN=CENTER STyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p claSs="western" ALIGN=CENTER>
                                                    <FONT SIZE=2 stYLe="font-size: 11pt"><b>Tdk
                    Sesuai</B></fONt>
                                                </P>
                                            </Td>
                                            
                                        </TR>
                                        <TR>
                                            <TD WIDTH=23 sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P CLass=western>
                                                    <Font SIZE=2 StyLe="font-size: 11pt">1</fonT>
                                                </p>
                                            </tD>
                                            <Td WIDTH=203 sTyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p cLASs="western">
                                                    <foNt SIZE=2 STYLE="font-size: 11pt">Fotocopy
                    Surat Undangan</foNT>
                                                </p>
                                            </TD>
                                            <tD align="center" WIDTH=52 VALIGN=TOP sTYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P ClasS=western ALIGN=CENTER>√</p>
                                            </Td>
                                            <Td WIDTH=80 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p cLass="western" ALIGN=CENTER>
                                                    <Br>
                                                </p>
                                            </tD>
                                            <tD WIDTH=62 sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P CLAsS=western ALIGN=CENTER>
                                                    <Br>
                                                </p>
                                            </tD>
                                            <td WIDTH=127 style="border: 1px solid #000000; padding: 0in 0.08in">
                                                <P CLaSS=western ALIGN=CENTER>
                                                    <br>
                                                </p>
                                            </Td>
                                        </Tr>
                                        <TR VALIGN=TOP>
                                            <td WIDTH=23 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p cLASs=western>
                                                    <FoNt SIZE=2 styLE="font-size: 11pt">2</font>
                                                </p>
                                            </tD>
                                            <td WIDTH=203 sTyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p cLASs=western>
                                                    <FOnT SIZE=2 sTYlE="font-size: 11pt">Surat Penawaran</foNt>
                                                </P>
                                            </Td>
                                            <TD align="center" WIDTH=52 StYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P CLASS="western" ALIGN=CENTER>√</P>
                                            </tD>
                                            <TD WIDTH=80 StyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p cLAss="western" ALIGN=JUSTIFY>
                                                    <BR>
                                                </P>
                                            </TD>
                                            <tD WIDTH=62 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p ClASS=western ALIGN=JUSTIFY>
                                                    <Br>
                                                </P>
                                            </Td>
                                            <td WIDTH=127 styLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <p cLASS="western" ALIGN=JUSTIFY>
                                                    <br>
                                                </p>
                                            </Td>
                                        </Tr>
                                        <TR VALIGN=TOP>
                                            <TD WIDTH=23 STyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P ClAsS=western>
                                                    <Font SIZE=2 stYlE="font-size: 11pt">3</FONt>
                                                </P>
                                            </TD>
                                            <TD WIDTH=203 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P CLass="western">
                                                    <foNT SIZE=2 STyLE="font-size: 11pt">Masa berlaku surat penawaran</fONt>
                                                </p>
                                            </td>
                                            <TD WIDTH=52 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in" align="center">
                                                <P CLasS=western ALIGN=CENTER>√</p>
                                            </TD>
                                            <td WIDTH=80 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p ClasS="western" ALIGN=JUSTIFY>
                                                    <Br>
                                                </p>
                                            </tD>
                                            <tD WIDTH=62 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p ClASS="western" ALIGN=JUSTIFY>
                                                    <Br>
                                                </P>
                                            </TD>
                                            <Td WIDTH=127 stYlE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <p cLASS=western ALIGN=JUSTIFY>
                                                    <bR>
                                                </P>
                                            </Td>
                                        </tr>
                                        <TR VALIGN=TOP>
                                            <Td WIDTH=23 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p clAsS="western" ALIGN=JUSTIFY>
                                                    <BR>
                                                </p>
                                            </tD>
                                            <TD COLSPAN=4 WIDTH=440 STYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p clASS=western ALIGN=JUSTIFY>
                                                    <FONT SIZE=2 sTYLe="font-size: 11pt">HASIL AKHIR</FOnT>
                                                </p>
                                            </TD>
                                            <tD WIDTH=127 StYLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <p ClAsS=western ALIGN=CENTER>
                                                    <foNT SIZE=2 styLe="font-size: 11pt">LULUS / </fONT>
                                                    <STrIke><foNt SIZE=2 stYle="font-size: 11pt">TDK LULUS</foNt></sTrIke>
                                                </P>
                                            </td>
                                        </TR>
            </TABlE><br/>
            Hasil Penelitian dan penilaian Dokumen Administrasi dinyatakan [ LULUS / <strike>TIDAK LULUS</strike> ] dengan demikian dapat dilanjutkan pada Penelitian dan Penilaian Dokumen Teknis
        </li>
        <br/>
        <li>
            <strong>PENELITIAN DAN PENILAIAN DOKUMEN TEKNIS</strong><br/>
            <tabLE WIDTH=634 CELLPADDING=7 CELLSPACING=0>
                <COL WIDTH=23>
                    <col WIDTH=203>
                        <COL WIDTH=52>
                            <Col WIDTH=80>
                                <coL WIDTH=62>
                                    <COl WIDTH=127>
                                        <tr>
                                            <TD ROWSPAN=3 WIDTH=23 sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p claSs=western ALIGN=CENTER>
                                                    <fONT SIZE=2 Style="font-size: 11pt"><B>NO</b></FOnt>
                                                </P>
                                            </TD>
                                            <td ROWSPAN=3 WIDTH=203 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P ClasS="western" ALIGN=CENTER>
                                                    <FOnT SIZE=2 style="font-size: 11pt"><B>NAMA
                    DOKUMEN</b></fOnt>
                                                </p>
                                            </Td>
                                            <tD COLSPAN=2 WIDTH=147 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p CLass=western ALIGN=CENTER>
                                                    <FONT SIZE=2 sTyle="font-size: 11pt"><b>KELENGKAPAN</b></fonT>
                                                </P>
                                            </Td>
                                            <td ROWSPAN=2 WIDTH=62 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P clAss="western" ALIGN=CENTER>
                                                    <fONt SIZE=2 sTYLe="font-size: 11pt"><b>Tdk
                    ada</b></foNt>
                                                </p>
                                            </td>
                                            <TD ROWSPAN=2 WIDTH=127 sTyLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <p ClasS="western" ALIGN=CENTER>
                                                    <fONT SIZE=2 sTYLe="font-size: 11pt"><B>KETERANGAN</b></fONT>
                                                </p>
                                            </td>
                                        </tr>
                                        <TR>
                                            <TD COLSPAN=2 WIDTH=147 StYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P claSS=western ALIGN=CENTER>
                                                    <FONT SIZE=2 StYLE="font-size: 11pt"><b>Ada
                    (+)</b></fonT>
                                                </P>
                                            </TD>
                                        </tr>
                                        <Tr>
                                            <Td WIDTH=52 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p cLaSs=western ALIGN=CENTER>
                                                    <FONt SIZE=2 styLE="font-size: 11pt"><b>Sesuai</B></fOnT>
                                                </p>
                                            </tD>
                                            <td WIDTH=80 StYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P ClASS="western" ALIGN=CENTER>
                                                    <FOnt SIZE=2 Style="font-size: 11pt"><b>Tdk
                    Sesuai</b></FONt>
                                                </P>
                                            </TD>
                                            <Td WIDTH=62 sTYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p ClASS="western" ALIGN=CENTER>
                                                    <Br>
                                                </P>
                                            </TD>
                                            <td WIDTH=127 STyle="border: 1px solid #000000; padding: 0in 0.08in">
                                                <P cLASS="western" ALIGN=CENTER>
                                                    <Br>
                                                </P>
                                            </tD>
                                        </Tr>
                                        <tR VALIGN=TOP>
                                            <TD WIDTH=23 StYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P clASs=western>
                                                    <FonT SIZE=2 STyLe="font-size: 11pt">1</font>
                                                </p>
                                            </td>
                                            <TD WIDTH=203 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P ClAsS=western>
                                                    <foNT SIZE=2 STyle="font-size: 11pt">Jadwal
                    Pelaksanaan</FoNt>
                                                </p>
                                            </TD>
                                            <tD WIDTH=52 align="center" STyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P claSS=western ALIGN=CENTER>√</P>
                                            </tD>
                                            <tD WIDTH=80 STylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p cLAss="western" ALIGN=JUSTIFY>
                                                    <br>
                                                </P>
                                            </tD>
                                            <td WIDTH=62 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P CLAss="western" ALIGN=JUSTIFY>
                                                    <BR>
                                                </p>
                                            </td>
                                            <Td WIDTH=127 stYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                <p Class="western" ALIGN=JUSTIFY>
                                                    <bR>
                                                </P>
                                            </td>
                                        </tr>
                                        <TR VALIGN=TOP>
                                            <td WIDTH=23 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P LAng="id-ID" ClasS=western><font SIZE=2 styLE="font-size: 11pt">2</fonT>
                                                </P>
                                            </td>
                                            <tD WIDTH=203 StYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p CLASs=western>
                                                    <Font SIZE=2 sTylE="font-size: 11pt">Daftar
                    Personil yang ditugaskan</fOnt>
                                                </P>
                                            </TD>
                                            <td WIDTH=52 align="center" Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p clASs="western" ALIGN=CENTER>√</P>
                                            </tD>
                                            <tD WIDTH=80 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p claSS="western" ALIGN=JUSTIFY>
                                                    <BR>
                                                </P>
                                            </td>
                                            <td WIDTH=62 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P cLAss="western" ALIGN=JUSTIFY>
                                                    <br>
                                                </p>
                                            </td>
                                            <td WIDTH=127 stylE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <P clAsS="western" ALIGN=JUSTIFY>
                                                    <Br>
                                                </p>
                                            </Td>
                                        </TR>
                                        <TR VALIGN=TOP>
                                            <TD WIDTH=23 STyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p CLasS=western>
                                                    <bR>
                                                </P>
                                            </TD>
                                            <tD COLSPAN=4 WIDTH=440 StyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P cLASs=western ALIGN=JUSTIFY>
                                                    <FONT SIZE=2 StylE="font-size: 11pt">HASIL
                    AKHIR</FOnt>
                                                </P>
                                            </Td>
                                            <Td WIDTH=127 STylE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <p cLasS=western ALIGN=CENTER>
                                                    <fOnt SIZE=2 styLE="font-size: 11pt">LULUS
                    / </Font>
                                                    <StRIke><font SIZE=2 sTylE="font-size: 11pt">TDK LULUS</FoNt></StRIkE>
                                                </p>
                                            </Td>
                                        </Tr>
            </tAble><br/>
            Hasil Penelitian dan penilaian Dokumen Teknis dinyatakan [ LULUS / <strike>TIDAK LULUS</strike> ] dengan demikiandapat dilanjutkan pada Penelitian dan Penilaian Dokumen Biaya
        </li>
        <p></p>
        <p></p>
        <li>
            <strong>PENELITIAN DAN PENILAIAN DOKUMEN BIAYA</strong><br/>
            <Table WIDTH=634 CELLPADDING=7 CELLSPACING=0>
                <col WIDTH=23>
                    <cOl WIDTH=203>
                        <cOl WIDTH=52>
                            <CoL WIDTH=80>
                                <Col WIDTH=62>
                                    <Col WIDTH=127>
                                        <Tr>
                                            <tD ROWSPAN=3 WIDTH=23 STyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P CLaSs=western ALIGN=CENTER>
                                                    <Font SIZE=2 stylE="font-size: 11pt"><b>NO</b></FONt>
                                                </P>
                                            </td>
                                            <TD ROWSPAN=3 WIDTH=203 sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P ClAss="western" ALIGN=CENTER>
                                                    <foNt SIZE=2 StYle="font-size: 11pt"><b>NAMA
                    DOKUMEN</B></fONT>
                                                </P>
                                            </td>
                                            <Td COLSPAN=2 WIDTH=147 sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p clASs="western" ALIGN=CENTER>
                                                    <fOnt SIZE=2 STyLe="font-size: 11pt"><b>KELENGKAPAN</b></FoNT>
                                                </p>
                                            </td>
                                            <tD ROWSPAN=2 WIDTH=62 stYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p clasS="western" ALIGN=CENTER>
                                                    <foNT SIZE=2 STyLe="font-size: 11pt"><b>Tdk
                    ada</b></FONT>
                                                </p>
                                            </tD>
                                            <Td ROWSPAN=2 WIDTH=127 STylE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <p CLass=western ALIGN=CENTER>
                                                    <fONt SIZE=2 styLe="font-size: 11pt"><b>KETERANGAN</b></fONt>
                                                </p>
                                            </td>
                                        </tr>
                                        <Tr>
                                            <TD COLSPAN=2 WIDTH=147 sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p clASS="western" ALIGN=CENTER>
                                                    <fONT SIZE=2 Style="font-size: 11pt"><b>Ada
                    (+)</B></FonT>
                                                </P>
                                            </TD>
                                        </Tr>
                                        <tR>
                                            <td WIDTH=52 stYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p CLAss="western" ALIGN=CENTER>
                                                    <foNt SIZE=2 StyLE="font-size: 11pt"><b>Sesuai</b></fonT>
                                                </p>
                                            </td>
                                            <TD WIDTH=80 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p cLAsS=western ALIGN=CENTER>
                                                    <FoNT SIZE=2 stylE="font-size: 11pt"><b>Tdk
                    Sesuai</B></FonT>
                                                </P>
                                            </TD>
                                            <tD WIDTH=62 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P cLass=western ALIGN=CENTER>
                                                    <Br>
                                                </p>
                                            </tD>
                                            <TD WIDTH=127 stylE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <p cLasS=western ALIGN=CENTER>
                                                    <br>
                                                </P>
                                            </td>
                                        </Tr>
                                        <tR>
                                            <TD WIDTH=23 style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p clASs="western">
                                                    <FonT SIZE=2 sTYlE="font-size: 11pt">1</fONT>
                                                </p>
                                            </td>
                                            <TD WIDTH=203 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p ClaSs=western>
                                                    <foNT SIZE=2 STyle="font-size: 11pt">Rincian
                    RAB</fOnt>
                                                </p>
                                            </tD>
                                            <TD WIDTH=52 align="center" VALIGN=TOP sTylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P clASs="western" ALIGN=CENTER>√</p>
                                            </Td>
                                            <TD WIDTH=80 sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p ClaSS="western" ALIGN=CENTER>
                                                    <BR>
                                                </p>
                                            </td>
                                            <Td WIDTH=62 STYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P ClaSs="western" ALIGN=CENTER>
                                                    <Br>
                                                </P>
                                            </td>
                                            <Td WIDTH=127 STyLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                <P Class=western ALIGN=CENTER>
                                                    <br>
                                                </p>
                                            </TD>
                                        </TR>
                                        <TR VALIGN=TOP>
                                            <td WIDTH=23 STylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P ClasS=western>
                                                    <FOnt SIZE=2 sTylE="font-size: 11pt">2</font>
                                                </p>
                                            </tD>
                                            <TD WIDTH=203 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p cLass="western">
                                                    <FoNt SIZE=2 stYLE="font-size: 11pt">Koreksi
                    aritmatik </fonT>
                                                </p>
                                            </td>
                                            <TD WIDTH=52 align="center" sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P ClasS=western ALIGN=CENTER>√</P>
                                            </Td>
                                            <td WIDTH=80 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p Class=western ALIGN=JUSTIFY>
                                                    <Br>
                                                </p>
                                            </tD>
                                            <TD WIDTH=62 STYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P clAsS=western ALIGN=JUSTIFY>
                                                    <Br>
                                                </P>
                                            </TD>
                                            <tD WIDTH=127 STYlE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <p ClAsS=western ALIGN=JUSTIFY>
                                                    <br>
                                                </P>
                                            </td>
                                        </TR>
                                        <Tr VALIGN=TOP>
                                            <Td WIDTH=23 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <p ClasS="western">
                                                    <bR>
                                                </P>
                                            </td>
                                            <Td COLSPAN=4 WIDTH=440 StyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                <P ClasS=western ALIGN=JUSTIFY>
                                                    <FOnt SIZE=2 sTyLe="font-size: 11pt">HASIL
                    AKHIR</FonT>
                                                </p>
                                            </Td>
                                            <Td WIDTH=127 sTYLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                <p cLaSS=western ALIGN=CENTER>
                                                    <foNT SIZE=2 sTYle="font-size: 11pt">LULUS/</fONT>
                                                    <sTrIKe><fONT SIZE=2 STYLE="font-size: 11pt">TDK
                    LULUS</FoNT></STRiKe>
                                                </p>
                                            </td>
                                        </Tr>
            </TAblE><br/>
            Hasil Penelitian dan penilaian Dokumen Biaya dinyatakan [ LULUS / <strike>TIDAK LULUS</strike> ] dengan demikian dapat dilanjutkan klarifikasi dan negosiasi.
        </li>
        <br/>

        <li>
            <strong>PENELITIAN DOKUMEN KUALIFIKASI</strong><br/>
            <TaBlE WIDTH=643 CELLPADDING=7 CELLSPACING=0>
                <COl WIDTH=23>
                    <cOL WIDTH=314>
                        <cOl WIDTH=4358>
                            <col WIDTH=43>
                                <cOL WIDTH=52>
                                    <col WIDTH=43>
                                        <Col WIDTH=80>
                                            <Tr>
                                                <td ROWSPAN=3 WIDTH=23 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p claSs="western" ALIGN=CENTER>
                                                        <fOnT SIZE=2 Style="font-size: 11pt"><B>NO</B></Font>
                                                    </p>
                                                </tD>
                                                <tD ROWSPAN=3 WIDTH=314 sTylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p cLass="western" ALIGN=CENTER>
                                                        <fONt SIZE=2 stYLE="font-size: 11pt"><b>NAMA
                    DOKUMEN</b></FONt>
                                                    </p>
                                                </tD>
                                                <Td COLSPAN=3 WIDTH=111 sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p clasS=western ALIGN=CENTER>
                                                        <FonT SIZE=2 stYLE="font-size: 11pt"><b>KELENGKAPAN</b></fonT>
                                                    </P>
                                                </tD>
                                                <td ROWSPAN=2 WIDTH=43 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P cLaSs=western ALIGN=CENTER>
                                                        <fOnt SIZE=2 sTyle="font-size: 11pt"><b>Tdk
                    ada</b></Font>
                                                    </P>
                                                </tD>
                                                <td ROWSPAN=2 WIDTH=80 sTYLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P ClASs=western ALIGN=CENTER>
                                                        <FONt SIZE=2 STyle="font-size: 11pt"><b>KET.</b></Font>
                                                    </P>
                                                </Td>
                                            </Tr>
                                            <TR>
                                                <td COLSPAN=3 WIDTH=111 STylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLAss="western" ALIGN=CENTER><font SIZE=2 STYLe="font-size: 11pt"><B>Ada
                    (+)</b></foNT>
                                                    </p>
                                                </Td>
                                            </TR>
                                            <TR>
                                                <td COLSPAN=2 WIDTH=45 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLasS=western ALIGN=CENTER>
                                                        <FoNt SIZE=2 sTYle="font-size: 11pt"><b>Sesuai</b></Font>
                                                    </p>
                                                </tD>
                                                <Td WIDTH=52 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLAss="western" ALIGN=CENTER>
                                                        <Font SIZE=2 StYlE="font-size: 11pt"><B>Tdk
                    Sesuai</B></FONt>
                                                    </p>
                                                </tD>
                                                <TD WIDTH=43 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLAsS=western ALIGN=CENTER>
                                                        <Br>
                                                    </P>
                                                </Td>
                                                <TD WIDTH=80 stYle="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P ClasS=western ALIGN=CENTER>
                                                        <bR>
                                                    </P>
                                                </td>
                                            </tr>
                                            <tR>
                                                <tD WIDTH=23 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLASs="western"><font SIZE=2 STyLe="font-size: 11pt">1</Font>
                                                    </p>
                                                </tD>
                                                <TD WIDTH=314 style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLASS=western ALIGN=JUSTIFY>
                                                        <FonT SIZE=2 StyLe="font-size: 11pt">Surat
                    pernyataan minat</FonT>
                                                    </P>
                                                </td>
                                                <Td COLSPAN=2 align="center" WIDTH=45 VALIGN=TOP STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLASs=western ALIGN=CENTER>√</P>
                                                </Td>
                                                <tD WIDTH=52 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p ClAsS=western ALIGN=CENTER>
                                                        <BR>
                                                    </P>
                                                </Td>
                                                <Td WIDTH=43 STYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p ClaSs=western ALIGN=CENTER>
                                                        <Br>
                                                    </P>
                                                </tD>
                                                <Td WIDTH=80 sTYle="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P ClAss="western" ALIGN=CENTER>
                                                        <br>
                                                    </P>
                                                </td>
                                            </tR>
                                            <tR VALIGN=TOP>
                                                <Td WIDTH=23 stYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLass="western">
                                                        <fonT SIZE=2 StylE="font-size: 11pt">2</foNt>
                                                    </p>
                                                </Td>
                                                <TD WIDTH=314 STyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLASS=western ALIGN=JUSTIFY>
                                                        <fONT SIZE=2 styLE="font-size: 11pt">Data
                    Administrasi</fOnT>
                                                    </p>
                                                </Td>
                                                <TD COLSPAN=2 align="center" WIDTH=45 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p ClaSS="western" ALIGN=CENTER>√</P>
                                                </td>
                                                <tD WIDTH=52 StYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLASS=western ALIGN=JUSTIFY>
                                                        <BR>
                                                    </p>
                                                </Td>
                                                <TD WIDTH=43 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P claSS="western" ALIGN=JUSTIFY>
                                                        <Br>
                                                    </P>
                                                </td>
                                                <Td WIDTH=80 STYlE="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P CLasS=western ALIGN=JUSTIFY>
                                                        <br>
                                                    </P>
                                                </Td>
                                            </tr>
                                            <Tr VALIGN=TOP>
                                                <td WIDTH=23 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p ClASS=western>
                                                        <bR>
                                                    </P>
                                                </td>
                                                <Td WIDTH=314 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <ol>
                                                        <li>
                                                            <font FaCE="Times New Roman, serif"><foNT SIZE=2 StyLE="font-size: 11pt">SIUJK</fonT>
                                                                </Font>
                                                    </Ol>
                                                </td>
                                                <Td COLSPAN=2 align="center" WIDTH=45 StyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLasS=western ALIGN=CENTER>√</p>
                                                </TD>
                                                <TD WIDTH=52 sTyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p cLaSs=western ALIGN=JUSTIFY>
                                                        <br>
                                                    </P>
                                                </Td>
                                                <TD WIDTH=43 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P cLass=western ALIGN=JUSTIFY>
                                                        <Br>
                                                    </p>
                                                </TD>
                                                <TD WIDTH=80 StyLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P clASs=western ALIGN=JUSTIFY>
                                                        <Br>
                                                    </p>
                                                </tD>
                                            </tr>
                                            <tR VALIGN=TOP>
                                                <tD WIDTH=23 sTyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P claSs=western>
                                                        <bR>
                                                    </P>
                                                </td>
                                                <Td WIDTH=314 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <oL START=2>
                                                        <li>
                                                            <fONt FACe="Times New Roman, serif"><FOnT SIZE=2 StyLe="font-size: 11pt">Landasan Hukum</fONT>
                                                            </FoNt>
                                                        </li>
                                                    </OL>
                                                </TD>
                                                <TD COLSPAN=2 align="center" WIDTH=45 StYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P claSs=western ALIGN=CENTER>√</P>
                                                </tD>
                                                <Td WIDTH=52 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p claSS=western ALIGN=JUSTIFY>
                                                        <Br>
                                                    </P>
                                                </td>
                                                <tD WIDTH=43 STylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLAsS="western" ALIGN=JUSTIFY>
                                                        <bR>
                                                    </p>
                                                </TD>
                                                <TD WIDTH=80 StyLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P cLASs="western" ALIGN=JUSTIFY>
                                                        <br>
                                                    </p>
                                                </TD>
                                            </TR>
                                            <tR VALIGN=TOP>
                                                <Td WIDTH=23 stYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLAss=western>
                                                        <BR>
                                                    </P>
                                                </tD>
                                                <tD WIDTH=314 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <OL START=3>
                                                        <Li>
                                                            <FONt FAce="Times New Roman, serif"><FONt SIZE=2 stYlE="font-size: 11pt">Pengurus Perusahaan</FONT></FOnt>
                                                        </li>
                                                    </Ol>
                                                </td>
                                                <tD align="center" COLSPAN=2 WIDTH=45 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p ClASS="western" ALIGN=CENTER>√</p>
                                                </td>
                                                <td WIDTH=52 stYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P clasS="western" ALIGN=JUSTIFY>
                                                        <Br>
                                                    </p>
                                                </Td>
                                                <tD WIDTH=43 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLass=western ALIGN=JUSTIFY>
                                                        <br>
                                                    </P>
                                                </tD>
                                                <tD WIDTH=80 sTYLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P cLaSs="western" ALIGN=JUSTIFY>
                                                        <br>
                                                    </p>
                                                </tD>
                                            </tr>
                                            <tR VALIGN=TOP>
                                                <TD WIDTH=23 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P ClASs=western>
                                                        <BR>
                                                    </P>
                                                </TD>
                                                <Td WIDTH=314 StYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <Ol START=4>
                                                        <lI>
                                                            <fOnt FACE="Times New Roman, serif"><FoNT SIZE=2 stYLe="font-size: 11pt">Data Keuangan</FoNt>
                                                            </FoNt>
                                                        </li>
                                                    </Ol>
                                                </td>
                                                <td align="center" COLSPAN=2 WIDTH=45 sTYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p Class="western" ALIGN=CENTER>√</P>
                                                </tD>
                                                <TD WIDTH=52 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLAsS="western" ALIGN=JUSTIFY>
                                                        <bR>
                                                    </p>
                                                </Td>
                                                <Td WIDTH=43 StYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P clAsS="western" ALIGN=JUSTIFY>
                                                        <BR>
                                                    </P>
                                                </TD>
                                                <td WIDTH=80 StyLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p clAsS="western" ALIGN=JUSTIFY>
                                                        <BR>
                                                    </p>
                                                </TD>
                                            </tr>
                                            <Tr VALIGN=TOP>
                                                <tD WIDTH=23 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p Class=western>
                                                        <FoNt SIZE=2 STYLE="font-size: 11pt">3</fOnT>
                                                    </P>
                                                </TD>
                                                <TD WIDTH=314 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p cLaSS="western" ALIGN=JUSTIFY>
                                                        <FOnt SIZE=2 StYlE="font-size: 11pt">Neraca
                    Perusahaan (tidak harus)</font>
                                                    </P>
                                                </tD>
                                                <td align="center" COLSPAN=2 WIDTH=45 sTyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p clAsS=western ALIGN=CENTER>√</P>
                                                </Td>
                                                <Td WIDTH=52 StYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLASs=western ALIGN=JUSTIFY>
                                                        <Br>
                                                    </P>
                                                </td>
                                                <Td WIDTH=43 style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P ClAsS="western" ALIGN=JUSTIFY>
                                                        <Br>
                                                    </P>
                                                </tD>
                                                <TD WIDTH=80 sTYle="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p cLASS="western" ALIGN=JUSTIFY>
                                                        <br>
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr VALIGN=TOP>
                                                <TD WIDTH=23 stYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p claSS=western>
                                                        <FONt SIZE=2 sTylE="font-size: 11pt">4</fONT>
                                                    </p>
                                                </Td>
                                                <Td WIDTH=314 sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P ClASs="western" ALIGN=JUSTIFY>
                                                        <fonT SIZE=2 style="font-size: 11pt">Data
                    Personalia (tidak harus)</FoNt>
                                                    </P>
                                                </td>
                                                <TD align="center" COLSPAN=2 WIDTH=45 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P clAss="western" ALIGN=CENTER>√</P>
                                                </tD>
                                                <Td WIDTH=52 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p ClASs="western" ALIGN=JUSTIFY>
                                                        <BR>
                                                    </P>
                                                </tD>
                                                <Td WIDTH=43 StYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLAsS="western" ALIGN=JUSTIFY>
                                                        <br>
                                                    </p>
                                                </td>
                                                <td WIDTH=80 stYle="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P CLaSS="western" ALIGN=JUSTIFY>
                                                        <Br>
                                                    </P>
                                                </td>
                                            </tr>
                                            <Tr VALIGN=TOP>
                                                <td WIDTH=23 STYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLaSs=western>
                                                        <foNt SIZE=2 stYle="font-size: 11pt">5</FONt>
                                                    </p>
                                                </tD>
                                                <tD WIDTH=314 STyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p cLaSs="western" ALIGN=JUSTIFY>
                                                        <fONT SIZE=2 STylE="font-size: 11pt">Surat
                    Pernyataan Kinerja yang baik</FOnt>
                                                    </p>
                                                </tD>
                                                <tD align="center" COLSPAN=2 WIDTH=45 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p clasS=western ALIGN=CENTER>√</p>
                                                </tD>
                                                <tD WIDTH=52 StYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLaSs=western ALIGN=JUSTIFY>
                                                        <bR>
                                                    </P>
                                                </Td>
                                                <Td WIDTH=43 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLASS=western ALIGN=JUSTIFY>
                                                        <Br>
                                                    </p>
                                                </tD>
                                                <TD WIDTH=80 STYLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P clAss="western" ALIGN=JUSTIFY>
                                                        <Br>
                                                    </P>
                                                </TD>
                                            </TR>
                                            <tR VALIGN=TOP>
                                                <Td WIDTH=23 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p cLASs=western>
                                                        <FoNt SIZE=2 STyLe="font-size: 11pt">6</fonT>
                                                    </p>
                                                </TD>
                                                <TD WIDTH=314 stylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p clAss="western" ALIGN=JUSTIFY>
                                                        <FoNT SIZE=2 STYle="font-size: 11pt">Surat
                    Pernyataan Bukan PNS/ TNI/POLRI</FOnT>
                                                    </P>
                                                </TD>
                                                <TD align="center" COLSPAN=2 WIDTH=45 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p cLaSs=western ALIGN=CENTER>√</p>
                                                </Td>
                                                <tD WIDTH=52 sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLAss=western ALIGN=JUSTIFY>
                                                        <br>
                                                    </p>
                                                </tD>
                                                <td WIDTH=43 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P ClASS="western" ALIGN=JUSTIFY>
                                                        <br>
                                                    </p>
                                                </TD>
                                                <td WIDTH=80 STYle="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p claSS="western" ALIGN=JUSTIFY>
                                                        <bR>
                                                    </p>
                                                </Td>
                                            </tR>
                                            <tr VALIGN=TOP>
                                                <TD WIDTH=23 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLasS=western><font SIZE=2 stYLe="font-size: 11pt">7</FoNt>
                                                    </P>
                                                </td>
                                                <tD COLSPAN=2 WIDTH=314 stYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLASS="western" ALIGN=JUSTIFY>
                                                        <FonT SIZE=2 styLE="font-size: 11pt">Memiliki
                    NPWP dan telah memenuhi kewajiban perpajakan tahun pajak terakhir
                    (SPT Tahunan) serta memiliki laporan bulanan PPh Pasal 21, PPh
                    Pasal 25/Pasal 29 dan PPN (bagi pengusaha kena pajak) paling
                    kurang 3 (tiga) bulan terakhir dalam tahun berjalan. Penyedia
                    dapat mengganti persyaratan ini dengan menyampaikan Surat
                    Keterangan Fiskal (SKF) yang dikeluarkan oleh kantor Pelayanan
                    Pajak dengan tanggal penerbitam paling lama 1 (satu) bulan sebelum
                    tanggal pemasukan Dokumen Kualifikasi; Pajak 3 bulan terakhir &amp;
                    pajak tahunan terakhir (SPT Tahunan)</foNt>
                                                    </p>
                                                </Td>
                                                <Td align="center" WIDTH=43 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P claSs="western" ALIGN=CENTER>√</p>
                                                </Td>
                                                <TD WIDTH=52 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p Class=western ALIGN=JUSTIFY>
                                                        <Br>
                                                    </p>
                                                </Td>
                                                <td WIDTH=43 STyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p ClASS=western ALIGN=JUSTIFY>
                                                        <BR>
                                                    </P>
                                                </TD>
                                                <TD WIDTH=80 stYLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p clASs=western ALIGN=JUSTIFY>
                                                        <bR>
                                                    </p>
                                                </Td>
                                            </Tr>
                                            <tr VALIGN=TOP>
                                                <TD WIDTH=23 StYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P cLaSS="western">
                                                        <foNT SIZE=2 StyLE="font-size: 11pt">8</FONT>
                                                    </P>
                                                </td>
                                                <TD COLSPAN=2 WIDTH=314 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p ClasS=western ALIGN=JUSTIFY>
                                                        <fONT SIZE=2 sTYLe="font-size: 11pt">Memperoleh paling sedikit 1 (satu) pekerjaan sebagai penyedia jasa konstruksi dalam kurun waktu 4 (empat) tahun terakhir, baik dilingkungan pemerintah maupun swasta termasuk pengalaman subkontrak; (dikecualikan perusahaan berdiri kurang dari 3 tahun)</FOnT>
                                                    </P>
                                                </TD>
                                                <Td align="center" WIDTH=43 sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P ClasS=western ALIGN=CENTER>√</P>
                                                </tD>
                                                <tD WIDTH=52 STYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLASS="western" ALIGN=JUSTIFY>
                                                        <BR>
                                                    </P>
                                                </tD>
                                                <TD WIDTH=43 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P ClAsS="western" ALIGN=JUSTIFY>
                                                        <BR>
                                                    </p>
                                                </Td>
                                                <Td WIDTH=80 STyle="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P ClAss="western" ALIGN=JUSTIFY>
                                                        <BR>
                                                    </p>
                                                </td>
                                            </TR>
                                            <Tr VALIGN=TOP>
                                                <Td WIDTH=23 STyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P cLass=western>
                                                        <FONT SIZE=2 stYLe="font-size: 11pt">9</font>
                                                    </P>
                                                </tD>
                                                <TD COLSPAN=2 WIDTH=314 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLAsS=western ALIGN=JUSTIFY>
                                                        <FoNt SIZE=2 StYle="font-size: 11pt">Data
                    pekerjaan yang sedang dilaksanakan</FonT>
                                                    </P>
                                                </Td>
                                                <TD align="center" WIDTH=43 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p claSS=western ALIGN=CENTER>√</p>
                                                </tD>
                                                <td WIDTH=52 sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p class=western ALIGN=JUSTIFY>
                                                        <Br>
                                                    </p>
                                                </td>
                                                <tD WIDTH=43 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p clASs="western" ALIGN=JUSTIFY>
                                                        <Br>
                                                    </P>
                                                </TD>
                                                <TD WIDTH=80 sTYlE="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p CLASS=western ALIGN=JUSTIFY>
                                                        <bR>
                                                    </p>
                                                </td>
                                            </Tr>
                                            <Tr VALIGN=TOP>
                                                <td WIDTH=23 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P ClaSS="western">
                                                        <Br>
                                                    </P>
                                                </td>
                                                <tD COLSPAN=2 WIDTH=496 sTyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P CLass=western ALIGN=JUSTIFY>
                                                        <foNt SIZE=2 STYle="font-size: 11pt">HASIL
                    AKHIR</FONT>
                                                    </P>
                                                </TD>
                                                <td COLSPAN=4 WIDTH=80 STYLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p clAss=western ALIGN=CENTER>
                                                        <foNT SIZE=2 sTyLE="font-size: 11pt">LULUS/</FONT>
                                                        <strIkE><FoNT SIZE=2 StyLE="font-size: 11pt">TDK
                    LULUS</FOnt></StriKe>
                                                    </p>
                                                </Td>
                                            </TR>
            </TABLe><br/>
            Hasil Penelitian dan penilaian Dokumen Kualifikasi (Keuangan dan Teknis) dinyatakan [ LULUS / <strike>TIDAK LULUS</strike> ] dengan demikian dapat dilanjutkan Klarifikasi dan Negosiasi.
        </li>
    </ol>
    
    <!-- ========================================== -->
    

    <P LanG=pt-BR clASs=western ALIGN=JUSTIFY StylE="margin-bottom: 0in;">
        <FOnT SIZE=2 STYlE="font-size: 11pt"><B>HAL-HAL YANG DILAKUKAN
KLARIFIKASI DAN NEGOSIASI :</B></FOnT>
    </p>
    <ol>
        <Li>
            <fONT SIZE=2 styLe="font-size: 11pt">Administrasi</fONt>
        </li>
        <LI>
           <fONt SIZE=2 StylE="font-size: 11pt">Teknis</FONt>
        </li>
        <lI>
            <fOnT SIZE=2 StYlE="font-size: 11pt">Harga</foNT>
        </li>
    </ol>
    <P LANG=pt-BR cLASS="western" ALIGN=JUSTIFY StYle="margin-bottom: 0in">
        <fOnT SIZE=2 StyLe="font-size: 11pt">Dengan rincian sebagai berikut :</FONt>
    </P>

    <Ol>
        <LI>
            <P LAnG="pt-BR" ClasS="western" ALIGN=JUSTIFY sTyLe="margin-bottom: 0in">
                <FONt SIZE=2 StylE="font-size: 11pt"><B>EVALUASI ADMINISTRASI</b></fonT>
            </p>
            <OL>
                <Li>
                    <FonT SIZE=2 sTyLE="font-size: 11pt">Evaluasi Administrasi
		dilakukan dengan sistem gugur</fONT>
                    <li>
                        <FOnt SIZE=2 StyLe="font-size: 11pt">Mengadakan penelitian dan
		penilaian secara seksama terhadap penawaran yang telah dikoreksi
		arimatiknya.</fONt>
                        <Li>
                            <FOnT SIZE=2 sTYlE="font-size: 11pt">Kriteria Evaluasi
		Administrasi terhadap Dokumen Penawaran :</Font>
                            <Li>
                                <Font SIZE=2 stYLe="font-size: 11pt">Penawaran dinyatakan
		memenuhi persyaratan administrasi, apabila :</FOnT>
                                <OL TYpE="A">
                                    <lI>
                                        <fONt SIZE=2 stYlE="font-size: 11pt">Surat penawaran memenuhi ketentuan sebagai berikut :</Font>
                                        <OL tyPE="a">
                                            <Li>
                                                <FoNt SIZE=2 sTyle="font-size: 11pt">Ditandatangani oleh :</foNT>
                                                <ul>
                                                    <LI>
                                                        <fONT SIZE=2 stylE="font-size: 11pt">Direktur utama / pimpinan
						perusahaan / pengurus koperasi;</FOnt>
                                                        <LI>
                                                            <fonT SIZE=2 styLe="font-size: 11pt">Penerima kuasa dari direktur
						utama / pimpinan perusahaan / pengurus koperasi yang nama penerima
						kuasanya tercantum dalam akta pendirian / anggaran dasar;</FONT>
                                                            <Li>
                                                                <FONt SIZE=2 stYLE="font-size: 11pt">Pihak lain yang bukan direktur
						utama / pimpinan perusahaan / pengurus koperasi yang namanya tidak
						tercatum dalam akta pendirian / anggaran dasar, sepanjang pihak lain
						tersebut adalah pengurus / karyawan perusahaan / karyawan koperasi
						yang berstatus sebagai tenaga kerja tetap dan mendapat kuasa atau
						pendelegasian wewenang yang sah dari direktur utama / pimpinan
						perusahaan / pengurus koperasi berdasarkan akta pendirian / anggaran
						dasar; atau</FoNt>
                                                                <li>
                                                                    <Font SIZE=2 styLE="font-size: 11pt">Kepala cabang perusahaan yang
						diangkat oleh kantor pusat</fONT>
                                                </UL>
                                                <li>
                                                    <foNt SIZE=2 sTylE="font-size: 11pt"><span Lang="pt-BR">Mencantumkan penawaran harga</sPan></FOnt>
                                                    </P>
                                                    <lI>
                                                        <FONT SIZE=2 stYLE="font-size: 11pt"><SpaN lANG=pt-BR>Jangka waktu berlakunya surat penawaran tidak kurang dari waktu sebagaimana tercantum dalam LDP</SpaN></FONt>
                                                        </p>
                                                        <Li>
                                                            <FOnT SIZE=2 StYLe="font-size: 11pt"><spaN LaNg="pt-BR">Jangka waktu pelaksanaan pekerjaan yang ditawarkan tidak melebihi jangka waktu sebagaimana tercantum dalam LDP</spaN></fONT>
                                                            </p>
                                                            <LI>
                                                                <fonT SIZE=2 styLE="font-size: 11pt"><spaN lANg=pt-BR>bertanggal</spAN></FoNt>
                                                                </p>
                                        </ol>
                                        <lI>
                                            <p lAnG=pt-BR cLAsS="western" ALIGN=JUSTIFY StYlE="margin-bottom: 0in">
                                                <fonT SIZE=2 sTyLe="font-size: 11pt">Pejabat Pengadaan dapat melakukan klarifikasi terhadap hal-hal yang kurang jelas dan meragukan</FoNt>
                                            </p>
                                </Ol>
                                <lI>
                                    <FoNT SIZE=2 StyLe="font-size: 11pt">Apabila penyedia tidak memenuhi
		persyaratan administrasi, Pejabat Pengadaan menyatakan Pengadaan
		Langsung gagal, dan mengundang penyedia lain</font>
                                    <LI>
                                        <fOnT SIZE=2 StYLE="font-size: 11pt">Hasil Evaluasi Administrasi
		adalah penawar memenuhi persyaratan administrasi dan dilanjutkan
		dengan Evaluasi Teknis</foNT>
            </Ol>
            <br/>
            <li>
                <FOnt SIZE=2 StYle="font-size: 11pt"><B>EVALUASI TEKNIS</b></fONt>
                <oL>
                    <LI>
                        <fonT SIZE=2 sTYLE="font-size: 11pt">Evaluasi Teknis dilakukan
			terhadap penyedia yang memenuhi persyaratan administrasi</FoNt>
                        <lI>
                            <fONt SIZE=2 StYle="font-size: 11pt">Unsur-unsur yang dievaluasi
			teknis sesuai dengan yang ditetapkan sebagaimana yang tercantumdi
			spesifikasi</FonT>
                            <Li>
                                <fOnt SIZE=2 sTylE="font-size: 11pt">Evaluasi teknis dilakukan
			dengan sistem gugur</fONt>
                                <Li>
                                    <foNT SIZE=2 styLE="font-size: 11pt">Pejabat Pengadaan menilai persyaratan teknis minimal yang harus dipenuhi sebagaimana tercantum di spesifikasi</FoNt>
                                    <li>
                                        <fONT SIZE=2 STyLe="font-size: 11pt">Penilaian syarat teknis minimal dilakukan terhadap :</fONt>
                                        <uL>
                                            <Li>
                                                <fOnt SIZE=2 Style="font-size: 11pt"><SpAn lANg=pt-BR>Spesifikasi
					teknis barang yang ditawarkan berdasarkan contoh, brosur dan
					gambar-gambar yang memuat identitas barang (jenis, tipe, dan merek)</SpAn></Font>
                                                <lI>
                                                    <FonT SIZE=2 StYLE="font-size: 11pt"><SPaN LaNG="pt-BR">Jangka
					waktu jadwal waktu pelaksanaan pekerjaan dan / atau jadwal serah
					terima pekerjaan (dalam hal serah terima pekerjaan dilakukan per
					termin) sebagai tercantum dalam LDP</sPaN></FONT>
                                                    <lI>
                                                        <FOnT SIZE=2 StYLe="font-size: 11pt"><SpAN LANg=pt-BR>Layanan
					purna jual (apabila dipersyaratkan)</spAn></FONT>
                                                        <LI>
                                                            <fONt SIZE=2 STYLE="font-size: 11pt"><sPan Lang="pt-BR">Tenaga
					teknis operasional / penggunaan barang (apabila dipersyaratkan), dan</sPan></FonT>
                                                            <LI>
                                                                <fonT SIZE=2 styLe="font-size: 11pt"><SPAN lanG=pt-BR>Bagian
					pekerjaan yang akan disubkontrakkan sebagaimana tercantum dalam LDP</sPan></FONt> </Ul>
                                        <lI>
                                            <FonT SIZE=2 STylE="font-size: 11pt">Apabila penyediaan tidak
			memenuhi persyaratan teknis, Pejabat Pengadaan menyatakan Pengadaan
			Langsung gagal, dan mengundang penyedia lain</fonT>
                                            <lI>
                                                <fonT SIZE=2 StYLe="font-size: 11pt">Hasil Evaluasi Teknis adalah
			penawar memenuhi persyaratan Teknis dan dilanjutkan dengan Evaluasi
			Harga</fonT>
                </ol>
                <br/>
                <Li>
                    <fOnt SIZE=2 stYLe="font-size: 11pt"><B>EVALUASI HARGA</B></FOnT><br/>
                    <ol>
                        <LI>
                            
                            <Font SIZE=2 STYLe="font-size: 11pt">Unsur – unsur yang perlu
        dievaluasi adalah hal-hal yang pokok atau penting, dengan ketentuan
        harga satuan penawaran yang nilainya lebih besar dari 110% (seratus
        sepuluh perseratus) dari harga satuan yang tercantum dalam HPS,
        dilakukan klarifikasi. Apabila setelah dilakukan klarifikasi,
        ternyata harga satuan penawaran tersebut dinyatakan timpang maka
        harga satuan timpang hanya berlaku untuk volume sesuai dengan Daftar
        Kuantitas dan Harga. Jika terjadi penambahan volume, harga satuan
        yang berlaku sesuai dengan harga satuan dalam HPS.</FOnT>
                            <Li>
                                <foNT SIZE=2 sTYle="font-size: 11pt">Harga penawaran terkoreksi yang
        melebihi nilai total HPS, dinyatakan gugur. Pejabat pengadaan
        menyatakan Pengadaan Langsung gagal, dan mengundang penyedia lain</fONT>
                            </li>
                            <Li>
                                <FoNt SIZE=2 STYlE="font-size: 11pt">Hasil Evaluasi Harga adalah
    Penawar memenuhi persyaratan harga</foNt>
                            </li>
                    </ol>
    </Ol>
    </ol>

    <p LAng=pt-BR clAss="western" ALIGN=JUSTIFY sTYLE="margin-bottom: 0in">
        <fONT SIZE=2 STyLE="font-size: 11pt"><B>HASIL KLARIFIKASI DAN
NEGOSIASI</b></FONT>
    </P>
    <Ol>
        <Li>
            <foNt SIZE=2 sTyle="font-size: 11pt">Administrasi</fONT>
            <BR>
            <FoNt SIZE=2 STyLe="font-size: 11pt">Calon Penyedia barang/jasa dapat
		menunjukkan semua dokumen “Asli” yang dipersyaratkan dalam
		Dokumen Pengadaan</FoNT>
            <li>
                <P LanG=pt-BR CLaSs=western ALIGN=JUSTIFY style="margin-bottom: 0in">
                    <fOnt SIZE=2 StyLE="font-size: 11pt">Teknis</FONT>
                </P>
                <ol>
                    <Li>
                        <font SIZE=2 StYle="font-size: 11pt"><sPAN lang="pt-BR">Calon
			Penyedia barang/jasa </SPaN></foNt>
                            <Font SIZE=2 StYLe="font-size: 11pt"><sPaN lAnG="pt-BR"><b>sanggup</b></spAN></foNT>
                            <FOnT SIZE=2 sTYLE="font-size: 11pt"><sPAn lAnG=pt-BR>
			melaksanakan / menyerahkan pekerjaan sesuai jadwal waktu
			pelaksanaan/penyerahan yang ditetapkan dalam dokumen pengadaan.</SPan></fONt>
                        </li>
                        <li>
                            
                                <foNt SIZE=2 StylE="font-size: 11pt"><SPan laNG="pt-BR">Calon
			Penyedia barang/jasa </sPan></Font>
                                <foNt SIZE=2 StYle="font-size: 11pt"><span lang="pt-BR"><b>sanggup</b></spAn></FoNt>
                                <fonT SIZE=2 sTylE="font-size: 11pt"><sPAN laNg="pt-BR">
			menyerahkan barang-barang / pekerjaan dalam keadaan baru dan
			berfungsi</sPan></FONt>
                        </li>
                        <Li>
                            <foNT SIZE=2 stYlE="font-size: 11pt"><SpAN LAng=pt-BR>Calon
    Penyedia barang/jasa </sPaN></FoNT>
                            <FOnT SIZE=2 StyLE="font-size: 11pt"><SpAn LanG=pt-BR><B>sanggup</B></sPAn></FONT>
                            <fonT SIZE=2 sTYle="font-size: 11pt"><SPAN LaNg="pt-BR">
    melaksanakan pekerjaan sesuai dengan spesifikasi teknis.</sPAn></fOnt>
                        </li>
                </ol>
                <br/>
                <lI>
                    
                    <FONt SIZE=2 sTyLE="font-size: 11pt">Harga</foNt><br/>
                    
                    
                        <fONt SIZE=2 stYle="font-size: 11pt">Setelah dilaksanakan
		Klarifikasi, selanjutnya dilakukan negosiasi harga dengan hasil
		sebagai berikut :</FoNt>
                    
                    <p LaNg="pt-BR" ClaSs="western" ALIGN=JUSTIFY stYLE="margin-left: 0.25in; margin-bottom: 0in">
                        Harga Penawaran yang tercantum dalam Dokumen Biaya yang diajukan Oleh<br/>
                        <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama); ?> sebesar Rp. <?php print('Rp '.format_money($kontrak_pihak_ketiga[0]->nilai_kontrak).',-'); ?> Terbilang (<?php print(ucwords(strtolower(terbilang($kontrak_pihak_ketiga[0]->nilai_kontrak))).' Rupiah'); ?> )
                    </P>
                    
    </Ol>

    <br/>
    <fONT SIZE=2 STyLe="font-size: 11pt">Setelah dilakukan Negosiasi, Pejabat Pengadaan Barang dan Penyedia adalah :</FoNT>
    <br/><br/>

    <table>
        <tr>
            <td class="text">Nama Perusahaan</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->program_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Alamat</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->pekerjaan_nama); ?></td>
        </tr>
        <tr>
            <td class="text">NPWP</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->aktivitas_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Harga Penawaran</td>
            <td class="text">:</td>
            <td class="text"><?php print('Rp '.format_money($kontrak_pihak_ketiga[0]->nilai_kontrak).',-'); ?></td>
        </tr>
        <tr>
            <td class="text">Harga Negosiasi</td>
            <td class="text">:</td>
            <td class="text"><?php print('Rp '.format_money($kontrak_pekerjaan->harga_negosiasi).',-'); ?></td>
        </tr>
        <tr>
            <td class="text">Terbilang</td>
            <td class="text">:</td>
            <td class="text"><?php print(ucwords(strtolower(terbilang($kontrak_pekerjaan->harga_negosiasi))).' Rupiah'); ?></td>
        </tr>
    </table>

    
    <P CLass=western ALIGN=JUSTIFY styLe="margin-bottom: 0in">
        <fOnT SIZE=2 stYLe="font-size: 11pt"><spAN lang="pt-BR">Demikian Berita Acara Klarifikasi dan Negosiasi untuk Paket pekerjaan Judul_Pekerjaan ini dibuat. Untuk dapat dipergunakan sebagaimana mestinya.</spAN></FoNT>
    </P>
    <p></p>
    <p></p>
    <table>
        <tr>
            <td width="300" align="center" style="font-size:14pt">
                Setuju:<br/>
                <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama);?><br/>
                <!-- <br/><br/><br/><br/><br/><br/><br/><br/><br/> -->
                <br/><br/><br/><br/><br/><br/><br/>
                <u><?php print($direktur_perusahaan);?></u><br/>
                Direktur
            </td>
            <td width="400"></td>
            <td width="300" align="center" style="font-size:14pt">
                <?php print($pejabat_pengadaan->jenis_pejabat_pengadaan_nama); ?> <?php print($pejabat_pengadaan->seri_pejabat_pengadaan_nama); ?><br/>
                Pada Dinas Pekerjaan Umum Kota Semarang<br/>
                Tahun Anggaran <?php print(date("Y"));?><br/>
                <br/><br/><br/><br/><br/>
                <u><?php print($pejabat_pengadaan->pegawai_nama); ?></u><br/>
                NIP. <?php print($pejabat_pengadaan->pegawai_nip); ?>
            </td>
        </tr>
    </table>

</BODY>

</HTML>