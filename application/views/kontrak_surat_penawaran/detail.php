<?php
$this->load->view('template/header');?>
<!-- Select2 -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/select2/dist/css/select2.min.css')); ?>">
<?php $this->load->view('template/asset_header');?>
<link rel="stylesheet" type="text/css" href="<?php print(base_url('assets/bower_components/datatables.net-bs/css/custom_button_export.css'))?>" />
<link href="<?php echo base_url('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css')?>" rel="stylesheet">

<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <!-- Box Form Pengunjung -->
        <?php 
            print($this->session->flashdata('alert')); 
            print($this->session->flashdata('success')); 

            $sess_aktivitas_id  = $this->session->userdata('id_aktivitas');
            $sess_data          = $this->session->userdata('session_data');
            $user_as            = $sess_data['as'];
            $user_role          = $sess_data['role'];
            $user_id            = $sess_data['id'];
        ?>
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-warning" id="form-pengunjung">
              <div class="box-header with-border">
                <h3 class="box-title">Detail Surat/Dokumen Pengadaan</h3>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <form class="form-horizontal" method="post">
                <div class="box-body">
                    
                    <div class="form-group">
                        <label for="namalengkap" class="col-sm-2 control-label">Nama Pekerjaan</label>
                        <div class="col-sm-9">
                            <div class="form-control" id="detail_nama_pekerjaan"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="namalengkap" class="col-sm-2 control-label">Pihak Penyedia</label>
                        <div class="col-sm-9">
                            <div class="form-control" id="detail_nama_penyedia"></div>
                        </div>
                    </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Nama Surat/Dokumen Pengadaan</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_nama_surat"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Tempat</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_nama_tempat"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Tgl Surat/Dokumen</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_tgl_surat"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">No Surat/Dokumen</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_no_surat"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Acara</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_acara"></div>
                    </div>
                  </div>   
                  
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Lampiran</label>
                    <div class="col-sm-9">
                      <div class="form-control">
                        <a href="javascript:void(0)" id="lampiran_detail">Kosong</a>
                      </div>
                    </div>
                  </div>        
                </div>
                <div class="box-footer">
                    <a href="<?php print(base_url('kontrak_pekerjaan/detail/'.$detail->kontrak_pekerjaan_id)); ?>" class="btn btn-primary">Kembali</a>
                    <a href="javascript:void(0)" class="btn btn-primary" onclick="edit_surat_penawaran(<?php print($detail->id); ?>)">Edit</a>
                    <a href="javascript:void(0)" class="btn btn-danger" onclick="delete_surat_penawaran(<?php print($detail->id); ?>)">Delete</a>

                    <a href="<?php print(base_url('kontrak_surat_penawaran/generate_surat/'.$detail->id)); ?>" class="btn btn-success pull-right" id="generate_surat">Generate</a>
                </div>
                <!-- /.box-footer -->
              </form>
                <?php if($detail->jenis_surat_kode=='UPengadaan') { ?>
                    <!-- Tabs Content -->
                    <div class="nav-tabs-custom">
                            <!-- Tabs within a box -->
                            <ul class="nav nav-tabs pull-left">
                                <li class="active"><a href="#data-jadwal-pengadaan" data-toggle="tab">Jadwal Pengadaan</a></li>
                            </ul>
                            <div class="tab-content no-padding">
                                <div class="chart tab-pane active" id="data-jadwal-pengadaan" style="position: relative;">
                                    <p>
                                        <a href="javascript:void(0)" class="btn btn-primary" onclick="add_jadwal_kegiatan()">Insert Kegiatan Pengadaan</a>
                                    </p>
                                    <table id="tabel_jadwal_kegiatan" class="table table-bordered table-striped table-responsive">
                                        <thead>
                                            <tr class="headings" align="center">
                                                <th class="column-title" align="center">No</th>
                                                <th class="column-title" align="center">Jadwal Kegiatan</th>
                                                <th class="column-title" align="center">Tanggal</th>
                                                <th class="column-title" align="center">Waktu</th>
                                                <th class="column-title" align="center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            
                            
                            </div>
                        </div>
                        <!-- /Tabs Content -->
                <?php } ?>

            </div>
          </div>
          <!-- /Box Form Pengunjung -->

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/dataTables.buttons.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.flash.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/jszip.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/pdfmake.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/vfs_fonts.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.html5.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.print.min.js'))?>"></script>
<script src="<?php echo base_url('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')?>"></script>
<!-- Select2 -->
<script src="<?php print(base_url('assets/bower_components/select2/dist/js/select2.full.min.js'));?>"></script>
<!-- Number divider -->
<script src="<?php print(base_url('assets/my_custom/js/number-divider.min.js')); ?>"></script>
<!-- InputMask -->
<script src="<?php print(base_url('assets/plugins/input-mask/jquery.inputmask.js'))?>"></script>
<script src="<?php print(base_url('assets/plugins/input-mask/jquery.inputmask.date.extensions.js'))?>"></script>
<script src="<?php print(base_url('assets/plugins/input-mask/jquery.inputmask.extensions.js'))?>"></script>

<script>
    var save_method_surat_penawaran,
        url_surat_penawaran,
        
        table_jadwal_kegiatan,
        save_method_jadwal_kegiatan,
        url_jadwal_kegiatan;

    $(function(){
        detail_data();

        $('.data-akun').hide();

        $('[data-mask]').inputmask();

        

        //Just number input
        $(".money").on("keypress keyup",function (event) {    
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        $("input").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $("textarea").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $("select").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $('.datepicker').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
            todayHighlight: true,
            todayBtn: true,
            todayHighlight: true,  
        });

        //Just number input
        $(".number").on("keypress keyup",function (event) {    
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
        
        table_jadwal_kegiatan = $('#tabel_jadwal_kegiatan').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
            "ajax": {
                "url": "<?php echo site_url('jadwal_kegiatan_kontrak/get_data/'.$detail->id);?>",
                "type": "POST"
            },
            "autoWidth": false,
            dom: 'lBfrtip',
            button:[{
              extend:'pdf',
              oriented:'potrait',
              pageSize: 'A4',
              title : 'Data Jadwal Kegiatan Kontrak Pekerjaan',
              download : 'open'
            }, 'copy', 'csv', 'excel', 'pdf', 'print'],
            "columnDefs": [{ 
                "targets": [ 0 ], 
                "orderable": false, 
            }]
        });

        // Save and Update surat penawawan
        $('#form-surat-penawaran').submit(function(obj){
            obj.preventDefault();
            if(save_method_surat_penawaran=='add') {
                url_surat_penawaran = "<?php echo site_url('kontrak_surat_penawaran/ajax_add/'.$detail->id)?>";
            } else {
                url_surat_penawaran = "<?php echo site_url('kontrak_surat_penawaran/ajax_update/')?>";
            }
            $.ajax({
                url: url_surat_penawaran,
                type: 'POST',
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(datas){
                    console.log(datas);  
                    
                    // Parsing to object
                    datas=JSON.parse(datas);                  
                    
                    //if success close modal and reload ajax table dokumen
                    if(datas.hasOwnProperty('status') && datas.status) 
                    {
                        alert('Data berhasil tersimpan');
                        $('#modal_form_surat_penawaran').modal('hide');
                        $('#form-surat-penawaran')[0].reset(); // reset form on modals
                        detail_data();
                    }
                    else
                    {
                        if(datas.hasOwnProperty('inputerror')) {
                            for (var i = 0; i < datas.inputerror.length; i++) 
                            {
                                $('[name="'+datas.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                $('[name="'+datas.inputerror[i]+'"]').next().text(datas.error_string[i]); //select span help-block class set text error string
                            }
                        }
                    }
                    $('#btnSaveSuratPenawaran').text('save'); //change button text
                    $('#btnSaveSuratPenawaran').attr('disabled',false); //set button enable 
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error calling save data. Please check again');
                    $('#modal_form_surat_penawaran').modal('hide');
                }
            });
        });

        // Save and Update jadwal kegiatan
        $('#form-jadwal-kegiatan').submit(function(obj){
            obj.preventDefault();
            if(save_method_jadwal_kegiatan=='add') {
                url_jadwal_kegiatan = "<?php echo site_url('jadwal_kegiatan_kontrak/ajax_add/'.$detail->id)?>";
            } else {
                url_jadwal_kegiatan = "<?php echo site_url('jadwal_kegiatan_kontrak/ajax_update/')?>";
            }
            $.ajax({
                url: url_jadwal_kegiatan,
                type: 'POST',
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(datas){
                    console.log(datas);  
                    
                    // Parsing to object
                    datas=JSON.parse(datas);                  
                    
                    //if success close modal and reload ajax table dokumen
                    if(datas.hasOwnProperty('status') && datas.status) 
                    {
                        alert('Data berhasil tersimpan');
                        $('#modal_form_jadwal_kegiatan').modal('hide');
                        $('#form-jadwal-kegiatan')[0].reset(); // reset form on modals
                        reload_data_jadwal_kegiatan();
                    }
                    else
                    {
                        if(datas.hasOwnProperty('inputerror')) {
                            for (var i = 0; i < datas.inputerror.length; i++) 
                            {
                                $('[name="'+datas.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                $('[name="'+datas.inputerror[i]+'"]').next().text(datas.error_string[i]); //select span help-block class set text error string
                            }
                        }
                    }
                    $('#btnSaveJadwalKegiatan').text('save'); //change button text
                    $('#btnSaveJadwalKegiatan').attr('disabled',false); //set button enable 
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error calling save data. Please check again');
                    $('#modal_form_jadwal_kegiatan').modal('hide');
                }
            });
        });
        
    });

    function detail_data() {
        $.ajax({
            url: "<?php echo site_url('kontrak_surat_penawaran/ajax_edit/'.$detail->id)?>",
            method: 'GET',
            type: 'JSON',
            success: function(data) {
                // Parsing to object
                data=JSON.parse(data);
                
                $('#detail_nama_pekerjaan').html(data.aktivitas_nama);
                $('#detail_nama_penyedia').text(data.pihak_ketiga_nama);
                $('#detail_nama_surat').text(data.jenis_surat_nama);
                $('#detail_nama_tempat').text(data.tempat_rapat_nama);
                if(data.tgl_surat=='0000-00-00') {
                    $('#detail_tgl_surat').text('Kosong');
                }
                else {
                    $('#detail_tgl_surat').text(data.tgl_surat);
                }
                $('#detail_no_surat').text(data.no_surat);
                $('#detail_acara').text(data.acara);
                // $('#lampiran_detail').text(data.lampiran);
                if(data.lampiran) {
                   $('#generate_surat').attr('href', '<?php print(base_url('/assets/uploads/documents_lampiran_surat/'));?>'+data.lampiran);
                   $('#generate_surat').attr('target', '_blank');
                    
                    $('#lampiran_detail').attr("href", '<?php print(base_url('/assets/uploads/documents_lampiran_surat/'));?>'+data.lampiran);
                    $('#lampiran_detail').text('Dokumen Lampiran');
                    $('#lampiran_detail').attr('target', '_blank');
                }
                else {
                    $('#lampiran_detail').text('Kosong');
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error while load detail data detail kontrak surat pengadaan !');
            }
        });
    }

    function edit_surat_penawaran(id) {
        save_method_surat_penawaran = 'update';
        $('#form-surat-penawaran')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('kontrak_surat_penawaran/ajax_edit/')?>/"+id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('#form-surat-penawaran')[0].reset();

                $('[name="id"]').val(data.id);
                $('[name="jenis_surat_id"]').val(data.jenis_surat_id);
                $('[name="kontrak_pihak_ketiga_id"]').val(data.kontrak_pihak_ketiga_id);
                $('[name="tempat_rapat_id"]').val(data.tempat_rapat_id);
                $('[name="tgl_surat"]').datepicker('update', data.tgl_surat);
                $('[name="no_surat"]').val(data.no_surat);
                $('[name="acara"]').val(data.acara);

                if(data.used_lampiran=='y') {
                    $('#used').attr('checked', 'checked');
                    $('#lampiran').attr('required', 'required');
                }
                else {
                    $('#used').removeAttr('checked');
                    $('#lampiran').removeAttr('required');
                }

                <?php if(!empty($kontrak_pihak_ketiga)) {?>
                    $('[name="kontrak_pihak_ketiga_id"]').val('<?php print($kontrak_pihak_ketiga[0]->id); ?>');
                    $('[name="pihak_ketiga_nama"]').val('<?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama); ?>');
                <?php } ?>
                
                $('#modal_form_surat_penawaran').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Update Dokumen Pengadaan'); // Set title to Bootstrap modal title

               
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_surat_penawaran(id)
    {
        if(confirm('Are you sure delete this data?'))
        {
            
            // ajax delete data to database
            $.ajax({
                url : "<?php echo site_url('kontrak_surat_penawaran/ajax_delete')?>/"+id,
                method: 'POST',
                type: 'JSON',
                success: function(datas)
                {
                    alert('Data berhasil dihapus');
                    window.location.replace("<?php print(base_url('kontrak_pekerjaan/detail/'.$detail->kontrak_pekerjaan_id)); ?>");
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });

        }
    }

    // Manage jadwal kegiatan surat penawaran
    function reload_data_jadwal_kegiatan()
    {
        table_jadwal_kegiatan.ajax.reload();
    }

    function add_jadwal_kegiatan() {
        save_method_jadwal_kegiatan = 'add';
        $('#form-jadwal-kegiatan')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form_jadwal_kegiatan').modal('show'); // show bootstrap modal
        $('.modal-title').text('Insert Jadwal Pengadaan'); // Set Title to Bootstrap modal title
    }

    function edit_jadwal_kegiatan(id) {
        save_method_jadwal_kegiatan = 'update';
        $('#form-jadwal-kegiatan')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('jadwal_kegiatan_kontrak/ajax_edit/')?>/"+id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id);
                $('[name="jenis_kegiatan_id"]').val(data.jenis_kegiatan_id);
                $('[name="tgl"]').datepicker('update',data.tgl);
                $('[name="jam"]').val(data.jam);
                
                $('#modal_form_jadwal_kegiatan').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Update Jadwal Pengadaan'); // Set title to Bootstrap modal title

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_jadwal_kegiatan(id)
    {
        if(confirm('Are you sure delete this data?'))
        {
            
            // ajax delete data to database
            $.ajax({
                url : "<?php echo site_url('jadwal_kegiatan_kontrak/ajax_delete')?>/"+id,
                method: 'POST',
                type: 'JSON',
                success: function(datas)
                {
                    alert('Data berhasil dihapus');
                    reload_data_jadwal_kegiatan();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });

        }
    }

</script>

<?php 
    // Load Modal Form
    $this->load->view('kontrak_surat_penawaran/modal_form');
    $this->load->view('jadwal_kegiatan_kontrak/modal_form');
    
?>

<?php $this->load->view('template/footer');?>