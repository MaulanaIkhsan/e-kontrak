<?php
$this->load->view('template/header');?>
<!-- Select2 -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/select2/dist/css/select2.min.css')); ?>">
<?php $this->load->view('template/asset_header');?>
<link rel="stylesheet" type="text/css" href="<?php print(base_url('assets/bower_components/datatables.net-bs/css/custom_button_export.css'))?>" />
<link href="<?php echo base_url('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css')?>" rel="stylesheet">
<?php
//print_r($_SESSION); ?>
<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">

        <!-- Box Form Pengunjung -->
          <div class="col-md-12"><?php
            $sess_data  = $this->session->userdata('session_data');
            $user_id    = $sess_data['id'];
            $role       = $sess_data['role'];
            $as         = $sess_data['as'];

            if(isset($_POST['komentar_kirim'])) {
                // print('running');
                // die;
                
                unset($_POST['komentar_kirim']);

                $this->db->insert("kontrak_komentar", $_POST);
                unset($_POST);

                if($as == 'pihak_ketiga') {
                    $data = array("file_id" => $dokumen_id,
                                "pegawai_id" => null,
                                "pihak_ketiga_id" => $sess_data['pihak_ketiga_id']);
                    $this->db->insert("kontrak_komentar_notif", $data);
                }
                else {
                    $data = array("file_id" => $dokumen_id,
                                  "pegawai_id" => $user_id,
                                  "pihak_ketiga_id" => null);
                    $this->db->insert("kontrak_komentar_notif", $data);
                }
                
            } 
            ?>
            <!-- DIRECT CHAT -->
            <div class="box box-warning direct-chat direct-chat-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Komentar</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div clas="col-md-12">
                    <form class="form-horizontal" method="post">
                      <div class="form-group">
                        <label for="namalengkap" class="col-sm-2 control-label">Dokumen</label>
                        <div class="col-sm-9">
                          <div class="form-control" id="dokumen">
                            <a href="<?php print(base_url('assets/uploads/documents/'.$detail->dokumen_pihak_ketiga_file)); ?>" target='_blank'><?php print($detail->dokumen_pihak_ketiga_nama); ?></a>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="namalengkap" class="col-sm-2 control-label">Urutan</label>
                        <div class="col-sm-9">
                          <div class="form-control"><?php print($detail->urutan); ?></div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="namalengkap" class="col-sm-2 control-label">Dokumen</label>
                        <div class="col-sm-9">
                          <div class="form-control">
                          <?php if($detail->terkoreksi=='y') { ?>
                            <div class="label label-success">Sudah</div>
                          <?php } else { ?>
                            <div class="label label-danger">Belum</div>
                          <?php } ?>
                        </div>
                      </div>
                      <div class="form-group">
                       <label for="namalengkap" class="col-sm-2 control-label">
                        <br/>
                        <a href="<?php print(base_url('kontrak_pihak_ketiga/detail/'.$detail->kontrak_pihak_ketiga_id));?>" class="btn btn-primary">Kembali</a>
                       </label>
                        <div class="col-md-9"></div>
                      </div>
                      
                    </form>
                  </div>

                  <div class="col-md-12">
                    <!-- Conversations are loaded here -->
                    <div class="direct-chat-messages">
                      <?php
                      $chats = $this->db->select('kontrak_komentar.id as kontak_komentar_id,
                                              kontrak_komentar.pegawai_id as pegawai_id,
                                              pegawai.nama_lengkap as pegawai_nama_lengkap,
                                              kontrak_komentar.karyawan_id as karyawan_id,
                                              karyawan.nama_lengkap as karyawan_nama_lengkap,
                                              kontrak_komentar.komentar as komentar,
                                              kontrak_komentar.created_at as created_at')
                                        ->from('kontrak_komentar')
                                        ->join('pegawai', 'kontrak_komentar.pegawai_id=pegawai.id', 'left')
                                        ->join('karyawan', 'kontrak_komentar.karyawan_id=karyawan.id', 'left')
                                        ->order_by('kontrak_komentar.id', 'DESC')
                                        ->where('kontrak_komentar.file_id', $dokumen_id)
                                        ->get()->result_array();

                      if(!empty($chats)) {
                        foreach ($chats as $chat) {
                          if(!empty($chat['pegawai_id'])){
                              $user_by = $chat['pegawai_nama_lengkap'];
                          }else{
                              $user_by = $chat['karyawan_nama_lengkap'];
                          }
                          ?>
                          <div class="direct-chat-msg">
                              <div class="direct-chat-info clearfix">
                                <span class="direct-chat-name pull-left"><?php print($user_by); ?></span>
                                <span class="direct-chat-timestamp pull-right"><?php print($chat['created_at']); ?></span>
                              </div>
                              <div class="direct-chat-text">
                                  <?php print($chat['komentar']); ?>
                              </div>
                          </div><?php
                        } 
                      } else {
                        print('Komentar masih kosong');
                      }

                      ?>

                    </div>
                    <!-- /.direct-chat-pane -->
                  </div>
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <form action="" method="post">
                  <input type="hidden" name="file_id" value="<?php print($dokumen_id); ?>" >
                  <?php
                  if($as == "pegawai"){ ?>
                    <input type="hidden" name="pegawai_id" value="<?php print($_SESSION['session_data']['id']); ?>" >
                    <?php
                  }else{ ?>
                    <input type="hidden" name="karyawan_id" value="<?php print($_SESSION['session_data']['id']); ?>" >
                    <?php
                  } ?>
                  <div class="input-group">
                    <input type="text" name="komentar" placeholder="Kirimkan komentar..." class="form-control" required="required">
                    <span class="input-group-btn">
                      <button name="komentar_kirim" type="submit" class="btn btn-warning btn-flat">Kirim</button>
                    </span>
                  </div>
                </form>
              </div>
          </div>
        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/dataTables.buttons.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.flash.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/jszip.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/pdfmake.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/vfs_fonts.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.html5.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.print.min.js'))?>"></script>
<script src="<?php echo base_url('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')?>"></script>
<!-- Select2 -->
<script src="<?php print(base_url('assets/bower_components/select2/dist/js/select2.full.min.js'));?>"></script>
<!-- Number divider -->
<script src="<?php print(base_url('assets/my_custom/js/number-divider.min.js')); ?>"></script>

<?php $this->load->view('template/footer');?>
