<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_profil" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Profil</h4>
            </div>
            <div class="modal-body form">
                <form action="#" id="form-profil" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label for="namalengkap" class="col-sm-2 control-label">NIP</label>
                            <div class="col-sm-9">
                                <input type="text" name="nip" class="form-control" placeholder="NIP" />
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="namalengkap" class="col-sm-2 control-label">Nama Lengkap</label>
                            <div class="col-sm-9">
                                <input type="text" name="nama_lengkap" class="form-control" placeholder="Nama Lengkap" />
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="namalengkap" class="col-sm-2 control-label">Jenis Kelamin</label>
                            <div class="col-sm-9">
                                <select name="jenis_kelamin" class="form-control">
                                    <option value="">--Select Role--</option>
                                    <option value="Pria">Pria</option>
                                    <option value="Wanita">Wanita</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="namalengkap" class="col-sm-2 control-label">Alamat</label>
                            <div class="col-sm-9">
                                <textarea name="alamat" placeholder="Alamat Lengkap" class="form-control"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>  
                        <div class="form-group">
                            <label for="namalengkap" class="col-sm-2 control-label">No Telepon</label>
                            <div class="col-sm-9">
                                <input type="text" name="no_telepon" class="form-control number" placeholder="No Telepon" />
                                <span class="help-block"></span>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label for="namalengkap" class="col-sm-2 control-label">Unit Kerja</label>
                            <div class="col-sm-9">
                                <select name="unit_kerja" class="form-control">
                                    <option value="">--Select Unit Kerja--</option>
                                    <?php 
                                    foreach($unit_kerja as $item): ?>
                                        <option value="<?php print($item->id); ?>"><?php print($item->nama); ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>  
                        <div class="form-group">
                            <label for="namalengkap" class="col-sm-2 control-label">Username</label>
                            <div class="col-sm-9">
                                <input type="text" name="username" class="form-control" placeholder="Username" />
                                <span class="help-block"></span>
                            </div>
                        </div> 
                        <div class="form-group">
                            <label for="namalengkap" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-9">
                                <input type="password" name="password" class="form-control" placeholder="Password" />
                                <span class="help-block"></span>
                            </div>
                        </div>   
                        
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_profil()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->