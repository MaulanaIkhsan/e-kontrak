<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan_model extends CI_Model {

    /*----- karyawan -----*/
    // id
    // nama_lengkap
    // jabatan_id
    // jenjang_pendidikan_id
    // pihak_ketiga_id
    // jenis_kelamin (Pria, Wanita)
    // alamat_lengkap
    // tgl_lahir
    // no_telepon
    // email
    // dokumen_profil
    // dokumen_pendidikan
    // dokumen_lainnya
    // username
    // password
    // is_user (y, n)
    
    var $column_order = array(null, 'karyawan.id', 
                'karyawan.nama_lengkap',
                'jabatan_perusahaan.id',
                'jabatan_perusahaan.nama', 
                'jenjang_pendidikan.id',
                'jenjang_pendidikan.nama',
                'pihak_ketiga.id',
                'pihak_ketiga.nama_perusahaan', 
                'karyawan.jenis_kelamin', 
                'karyawan.alamat_lengkap',
                'karyawan.tgl_lahir',
                'karyawan.no_telepon',
                'karyawan.email',
                'karyawan.dokumen_profil',
                'karyawan.dokumen_pendidikan',
                'karyawan.dokumen_lainnya',
                'karyawan.username',
                'karyawan.password',
                'karyawan.is_user'); 

    var $column_search = array('karyawan.id', 
                'karyawan.nama_lengkap',
                'jabatan_perusahaan.id',
                'jabatan_perusahaan.nama', 
                'jenjang_pendidikan.id',
                'jenjang_pendidikan.nama',
                'pihak_ketiga.id',
                'pihak_ketiga.nama_perusahaan', 
                'karyawan.jenis_kelamin', 
                'karyawan.alamat_lengkap',
                'karyawan.tgl_lahir',
                'karyawan.no_telepon',
                'karyawan.email',
                'karyawan.dokumen_profil',
                'karyawan.dokumen_pendidikan',
                'karyawan.dokumen_lainnya',
                'karyawan.username',
                'karyawan.password',
                'karyawan.is_user');
    
    var $username;                

    // default order 
    var $order = array('karyawan.id' => 'asc');

    public function fetch_data($id=NULL, $pihak_ketiga_id=NULL)
    {
        $sql = $this->db->select('karyawan.id as id, 
                                karyawan.nama_lengkap as nama_lengkap,
                                jabatan_perusahaan.id as jabatan_id,
                                jabatan_perusahaan.nama as jabatan_nama, 
                                jenjang_pendidikan.id as pendidikan_id,
                                jenjang_pendidikan.nama as pendidikan_nama,
                                pihak_ketiga.id as pihak_ketiga_id,
                                pihak_ketiga.nama_perusahaan as pihak_ketiga_nama, 
                                karyawan.jenis_kelamin as jenis_kelamin, 
                                karyawan.alamat_lengkap as alamat_lengkap,
                                karyawan.tgl_lahir as tgl_lahir,
                                karyawan.no_telepon as no_telepon,
                                karyawan.email as email,
                                karyawan.dokumen_profil as dokumen_profil,
                                karyawan.dokumen_pendidikan as dokumen_pendidikan,
                                karyawan.dokumen_lainnya as dokumen_lainnya,
                                karyawan.username as username,
                                karyawan.password as password,
                                karyawan.is_user as is_user')
                        ->from('karyawan')
                        ->join('jabatan_perusahaan', 'jabatan_perusahaan.id=karyawan.jabatan_id', 'left')
                        ->join('jenjang_pendidikan', 'jenjang_pendidikan.id=karyawan.jenjang_pendidikan_id', 'left')
                        ->join('pihak_ketiga', 'pihak_ketiga.id=karyawan.pihak_ketiga_id', 'left');

        if($pihak_ketiga_id!=NULL) {
            $sql->where('karyawan.pihak_ketiga_id', $pihak_ketiga_id);
        }

        if($this->username!=NULL) {
            $sql->where('karyawan.username', $this->username);
        }

        if($id!=NULL){
            return $sql->where('karyawan.id', $id)
                        ->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }

    public function set_username($username)
    {
        $this->username = $username;
    }

    public function check_username($username, $id=NULL, $exclude='n')
    {
        $sql = $this->db->select('username')
                        ->from('karyawan')
                        ->where('username', $username);

        if($id!=NULL) {
            if($exclude=='n'){
                return $sql->where('karyawan.id', $id)
                            ->get()->row();
            }
            elseif($exclude=='y') {
                return $sql->where('karyawan.id !=', $id)
                            ->get()->result_array();
            }
        }
        else {
            return $sql->get()
                        ->result();
        }
    }
    
    /*====== Serverside Datatable ======*/
	private function _get_datatables_query($pihak_ketiga_id)
	{
        $this->db->select('karyawan.id as id, 
                        karyawan.nama_lengkap as nama_lengkap,
                        jabatan_perusahaan.id as jabatan_id,
                        jabatan_perusahaan.nama as jabatan_nama, 
                        jenjang_pendidikan.id as pendidikan_id,
                        jenjang_pendidikan.nama as pendidikan_nama,
                        pihak_ketiga.id as pihak_ketiga_id,
                        pihak_ketiga.nama_perusahaan as pihak_ketiga_nama, 
                        karyawan.jenis_kelamin as jenis_kelamin, 
                        karyawan.alamat_lengkap as alamat_lengkap,
                        karyawan.tgl_lahir as tgl_lahir,
                        karyawan.no_telepon as no_telepon,
                        karyawan.email as email,
                        karyawan.dokumen_profil as dokumen_profil,
                        karyawan.dokumen_pendidikan as dokumen_pendidikan,
                        karyawan.dokumen_lainnya as dokumen_lainnya,
                        karyawan.username as username,
                        karyawan.password as password,
                        karyawan.is_user as is_user');

        $this->db->from('karyawan');
        $this->db->join('jabatan_perusahaan', 'jabatan_perusahaan.id=karyawan.jabatan_id', 'left');
        $this->db->join('jenjang_pendidikan', 'jenjang_pendidikan.id=karyawan.jenjang_pendidikan_id', 'left');
        $this->db->join('pihak_ketiga', 'pihak_ketiga.id=karyawan.pihak_ketiga_id', 'left');
        $this->db->where('karyawan.pihak_ketiga_id', $pihak_ketiga_id);

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($pihak_ketiga_id)
	{
		$this->_get_datatables_query($pihak_ketiga_id);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($pihak_ketiga_id)
	{
		$this->_get_datatables_query($pihak_ketiga_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($pihak_ketiga_id)
	{
		$this->db->select('karyawan.id as id, 
                        karyawan.nama_lengkap as nama_lengkap,
                        jabatan_perusahaan.id as jabatan_id,
                        jabatan_perusahaan.nama as jabatan_nama, 
                        jenjang_pendidikan.id as pendidikan_id,
                        jenjang_pendidikan.nama as pendidikan_nama,
                        pihak_ketiga.id as pihak_ketiga_id,
                        pihak_ketiga.nama_perusahaan as pihak_ketiga_nama, 
                        karyawan.jenis_kelamin as jenis_kelamin, 
                        karyawan.alamat_lengkap as alamat_lengkap,
                        karyawan.tgl_lahir as tgl_lahir,
                        karyawan.no_telepon as no_telepon,
                        karyawan.email as email,
                        karyawan.dokumen_profil as dokumen_profil,
                        karyawan.dokumen_pendidikan as dokumen_pendidikan,
                        karyawan.dokumen_lainnya as dokumen_lainnya,
                        karyawan.username as username,
                        karyawan.password as password,
                        karyawan.is_user as is_user');

        $this->db->from('karyawan');
        $this->db->join('jabatan_perusahaan', 'jabatan_perusahaan.id=karyawan.jabatan_id', 'left');
        $this->db->join('jenjang_pendidikan', 'jenjang_pendidikan.id=karyawan.jenjang_pendidikan_id', 'left');
        $this->db->join('pihak_ketiga', 'pihak_ketiga.id=karyawan.pihak_ketiga_id', 'left');
        $this->db->where('karyawan.pihak_ketiga_id', $pihak_ketiga_id);

		return $this->db->count_all_results();
	}
	/*====== /Serverside Datatable ======*/
}

/* End of file Karyawan_model.php */
