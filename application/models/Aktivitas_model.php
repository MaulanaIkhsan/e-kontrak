<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aktivitas_model extends CI_Model {

	// ----- aktivitas -----
    // id
	// nama_aktivitas
	// no_rekening
	// sumber_dana
    // anggaran_awal
    // perubahan_anggaran
    // hps
    // nilai_kontrak
    // tgl_pengajuan_pencairan
    // tgl_pencairan
    // keterangan
    // pptk
    // ppkom
    // bendahara
    // pekerjaan_id
    // pengadaan_id
    // jenis_pekerjaan_id
    // created_at
    // updated_at
    // created_by
	// updated_by

	var $main_table = 'aktivitas';

	public $tahun_pekerjaan = '';

	var $column_order = array(null, 'aktivitas.id', 
				'aktivitas.nama_aktivitas', 
				'pengadaan.nama', 
				'jenis_pekerjaan.nama', 
				'pegawai_ppkom.pegawai_nama_lengkap',
				'pegawai.nama_lengkap',
				'bendahara.pegawai_nama_lengkap',
				'aktivitas.anggaran_awal',
				'pengadaan.nama',
				'jenis_pekerjaan.nama',
				'pekerjaan.tahun'); //field yang ada di table pekerjaan

	var $column_search = array('aktivitas.id', 
				'aktivitas.nama_aktivitas', 
				'pengadaan.nama', 
				'jenis_pekerjaan.nama', 
				'pegawai_ppkom.pegawai_nama_lengkap',
				'pegawai.nama_lengkap',
				'bendahara.pegawai_nama_lengkap',
				'aktivitas.anggaran_awal',
				'pengadaan.nama',
				'jenis_pekerjaan.nama',
				'pekerjaan.tahun'); //field yang diizin untuk pencarian 

	var $order = array('pekerjaan.tahun'=>'desc', 'aktivitas.id' => 'asc'); // default order 
	
	// Initial attribute for kontrak_pekerjaan
	var $is_kontrak_pekerjaan 	= FALSE,
		$filter_jenis_pengadaan = ['Penunjukan Langsung', 'Pengadaan Langsung'],
		$filter_jenis_pekerjaan = ['Fisik', 'Pengadaan Barang', 'Jasa Konsultansi', 'Pengadaan Langsung'],
		$filter_min_anggaran 	= 50000000,
		$filter_max_anggaran 	= 200000000;

	//aktivitas.nama_aktivitas as nama_aktivitas, aktivitas.status, jenis_pengadaan.nama as jenis_pengadaan, jenis_pekerjaan.nama as jenis_pekerjaan, users.nama as pptk

	public function insert($data)
	{
		$sql = $this->db->insert($this->main_table, $data);
		if($sql){
			return 'ok';
		}
		else {
			return 'fail';
		}
	}

	public function fetch_data($id=NULL)
	{
		$sql = $this->db->select("aktivitas.id as id, 
								aktivitas.nama_aktivitas as nama_aktivitas,
								aktivitas.no_rekening as no_rekening,
								aktivitas.sumber_dana as sumber_dana, 
								aktivitas.anggaran_awal as anggaran_awal, 
								aktivitas.keterangan as keterangan, 
								pekerjaan.nama_pekerjaan as nama_pekerjaan,
								pekerjaan.kode_pekerjaan as kode_pekerjaan,
								aktivitas.nilai_kontrak as nilai_kontrak, 
								aktivitas.hps as hps, 
								pegawai.nama_lengkap as pptk, 
								pegawai.id as pptk_id,
								bendahara.pegawai_nama_lengkap as bendahara,
								bendahara.pegawai_id as bendahara_id,
								pegawai_ppkom.pegawai_nama_lengkap as ppkom,
								aktivitas.ppkom as ppkom_id,
								pengadaan.nama as jenis_pengadaan, 
								jenis_pekerjaan.nama as jenis_pekerjaan, 
								pekerjaan.id as pekerjaan_id, 
								program.id as program_id, 
								pekerjaan.tahun as pekerjaan_tahun, 
								program.tahun as program_tahun, 
								program.nama_program as nama_program,
								aktivitas.perubahan_anggaran as perubahan_anggaran,
								coalesce(aktivitas.tgl_pengajuan_pencairan, 'Belum') as tgl_pengajuan_pencairan,
								coalesce(aktivitas.tgl_pencairan, 'Belum') as tgl_pencairan,
								pekerjaan.kode_rekening as kode_rekening,
								aktivitas.keterangan as keterangan")
							->from($this->main_table)
							->join('pekerjaan','aktivitas.pekerjaan_id=pekerjaan.id', 'inner')
							->join('program', 'pekerjaan.kode_program=program.uniq_code', 'inner')
							->join('pegawai', 'pegawai.id=aktivitas.pptk', 'left')
							->join('jenis_pengadaan as pengadaan', 'pengadaan.id=aktivitas.pengadaan_id', 'left')
							->join('jenis_pekerjaan', 'jenis_pekerjaan.id=aktivitas.jenis_pekerjaan_id', 'left')
							->join('view_data_pegawai as bendahara', 'bendahara.pegawai_id=aktivitas.bendahara', 'left')
							->join('view_data_pegawai as ppkom', 'ppkom.pegawai_id=aktivitas.ppkom', 'left')
							->join('pejabat_pembuat_komitmen', 'aktivitas.ppkom=pejabat_pembuat_komitmen.id', 'left')
							->join('view_data_pegawai as pegawai_ppkom', 'pejabat_pembuat_komitmen.pegawai_id=pegawai_ppkom.pegawai_id', 'left');
		
		if(!empty($this->tahun_pekerjaan)) {
			$sql->where('pekerjaan.tahun', $this->tahun_pekerjaan);
		}

		if($id!=NULL) {
			return $sql->where('aktivitas.id', $id)
						->get()->row();
		}
		else {
			return $sql->get()->result();
		}
	}

	public function show()
	{
		return $this->db->select('id, nama')
						->from($this->main_table)
						->get()->result();
	}

	public function update($data, $key)
	{
		$sql = $this->db->where($key)
						->update($this->main_table, $data);
		if($sql){
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	public function show_update($key)
	{
		return $this->db->select('id, 
							nama_aktivitas, 
							no_rekening,
							sumber_dana,
							anggaran_awal,
							perubahan_anggaran, 
							hps, 
							nilai_kontrak, 
							tgl_pengajuan_pencairan,
							tgl_pencairan,
							keterangan, 
							ppkom,
							pptk,
							bendahara, 
							pekerjaan_id, 
							pengadaan_id, 
							jenis_pekerjaan_id')
						->from($this->main_table)
						->where('id', $key)
						->get()->row();
	}

	public function detail($id)
	{
		return $this->db->select("aktivitas.id as id, 
								aktivitas.nama_aktivitas as nama_aktivitas,
								aktivitas.no_rekening as no_rekening,
								aktivitas.sumber_dana as sumber_dana, 
								aktivitas.anggaran_awal as anggaran_awal, 
								aktivitas.keterangan as keterangan, 
								pekerjaan.nama_pekerjaan as nama_pekerjaan,
								aktivitas.nilai_kontrak as nilai_kontrak, 
								aktivitas.hps as hps, 
								pegawai.nama_lengkap as pptk, 
								pegawai.id as pptk_id,
								bendahara.pegawai_nama_lengkap as bendahara,
								bendahara.pegawai_id as bendahara_id,
								pegawai_ppkom.pegawai_nama_lengkap as ppkom,
								aktivitas.ppkom as ppkom_id,
								pengadaan.nama as jenis_pengadaan, 
								jenis_pekerjaan.nama as jenis_pekerjaan, 
								pekerjaan.id as pekerjaan_id, 
								program.id as program_id, 
								pekerjaan.tahun as pekerjaan_tahun, 
								program.tahun as program_tahun, 
								program.nama_program as nama_program,
								aktivitas.perubahan_anggaran as perubahan_anggaran,
								coalesce(aktivitas.tgl_pengajuan_pencairan, 'Belum') as tgl_pengajuan_pencairan,
								coalesce(aktivitas.tgl_pencairan, 'Belum') as tgl_pencairan,
								pekerjaan.kode_rekening as kode_rekening,
								aktivitas.keterangan as keterangan")
						->from($this->main_table)
						->join('pekerjaan','aktivitas.pekerjaan_id=pekerjaan.id', 'inner')
						->join('program', 'pekerjaan.kode_program=program.uniq_code', 'inner')
						->join('pegawai', 'pegawai.id=aktivitas.pptk', 'left')
						->join('jenis_pengadaan as pengadaan', 'pengadaan.id=aktivitas.pengadaan_id', 'left')
						->join('jenis_pekerjaan', 'jenis_pekerjaan.id=aktivitas.jenis_pekerjaan_id', 'left')
						->join('view_data_pegawai as bendahara', 'bendahara.pegawai_id=aktivitas.bendahara', 'left')
						->join('view_data_pegawai as ppkom', 'ppkom.pegawai_id=aktivitas.ppkom', 'left')
						->join('pejabat_pembuat_komitmen', 'aktivitas.ppkom=pejabat_pembuat_komitmen.id', 'left')
						->join('view_data_pegawai as pegawai_ppkom', 'pejabat_pembuat_komitmen.pegawai_id=pegawai_ppkom.pegawai_id', 'left')
						->where('aktivitas.id', $id)
						->get()->row();
	}

	// get program_id
	public function get_program_id($user_id)
	{
		return $this->db->select('distinct(pekerjaan.kode_program) as kode_program')
						->from($this->main_table)
						->join('pekerjaan', 'pekerjaan.id=aktivitas.pekerjaan_id', 'inner')
						->where('aktivitas.pptk', $user_id)
						->get()->result();
	}

	public function delete($key)
	{
		$sql = $this->db->delete($this->main_table, $key);
		if ($sql) {
			return 'ok';
		}
		else{
			return 'fail';
		}
	}

	public function get_nama_aktivitas($id)
	{
		return $this->db->select('id, nama_aktivitas, anggaran_awal, hps, nilai_kontrak, pengadaan_id')
						->from($this->main_table)
						->where('id', $id)
						->get()->row();
	}

	public function get_nilai_kontrak($id)
	{
		return $this->db->select('nilai_kontrak, anggaran_awal')
						->from($this->main_table)
						->where('id', $id)
						->get()->row();
	}

	public function get_last_aktivitas()
	{
		return $this->db->select('id, nama_aktivitas')
						->from($this->main_table)
						->order_by('id', 'desc')
						->get()->row();
	}

	public function get_aktivitas_by_tahun($tahun)
	{
		return $this->db->select('aktivitas.id as aktivitas_id')
						->from('aktivitas')
						->join('pekerjaan', 'pekerjaan.id=aktivitas.pekerjaan_id', 'inner')
						->where('pekerjaan.tahun', $tahun)
						->order_by('aktivitas.id')
						->get()->result();
	}

	/*================ Statistik Page Beranda ================*/
	// Statistik jml data metode pengadaan pada tahun ini
	public function stat_jenis_pengadaan($user_id, $role='Super Admin')
	{
		$sql = $this->db->select('pengadaan.nama as jenis_pengadaan, 
								count(aktivitas.id) as total')
						->from($this->main_table)
						->join('jenis_pengadaan as pengadaan', 'aktivitas.pengadaan_id=pengadaan.id', 'inner')
						->join('pekerjaan', 'aktivitas.pekerjaan_id=pekerjaan.id', 'inner');

		if($role!=='Super Admin'){
			$sql->where('aktivitas.pptk', $user_id)
				->or_where('aktivitas.ppkom', $user_id)
				->or_where('aktivitas.bendahara', $user_id);
		}

		return $sql->where('pekerjaan.tahun', date("Y"))
					->group_by('pengadaan.nama')
					->order_by('total', 'asc')
					->get()->result();
	}

	// Statistik jml data metode pengadaan dalam 5 tahun
	public function stat_5_jenis_pengadaan($user_id, $role='admin')
	{
		$curr_year	= date("Y");
		$min_year	= $curr_year - 5;

		$sql = $this->db->select('pekerjaan.tahun as tahun,
							pengadaan.nama as jenis, 
							count(aktivitas.id) as total')
						->from($this->main_table)
						->join('jenis_pengadaan as pengadaan', 'aktivitas.pengadaan_id=pengadaan.id', 'left')
						->join('pekerjaan', 'aktivitas.pekerjaan_id=pekerjaan.id', 'inner');

		if($role!=='admin'){
			$sql->where('aktivitas.pptk', $user_id);
		}

		return $sql->where('pekerjaan.tahun <= ', $curr_year)
					->where('pekerjaan.tahun >= ', $min_year)
					->group_by('pekerjaan.tahun, pengadaan.nama')
					->order_by('total', 'asc')
					->get()->result_array();
	}

	// Statistik jml data Jenis Pekerjaan pada tahun ini
	public function stat_jenis_pekerjaan($user_id, $role='Super Admin')
	{
		$sql = $this->db->select('jenis_pekerjaan.nama as jenis_pekerjaan, 
							count(aktivitas.id) as total')
						->from($this->main_table)
						->join('jenis_pekerjaan', 'aktivitas.jenis_pekerjaan_id=jenis_pekerjaan.id', 'inner')
						->join('pekerjaan', 'aktivitas.pekerjaan_id=pekerjaan.id', 'inner');
		
		if($role!=='Super Admin'){
			$sql->where('aktivitas.pptk', $user_id)
				->or_where('aktivitas.ppkom', $user_id)
				->or_where('aktivitas.bendahara', $user_id);
		}

		return $sql->where('pekerjaan.tahun', date("Y"))
					->group_by('jenis_pekerjaan.nama')
					->order_by('total', 'asc')
					->get()->result();
	}

	public function stat_5_jenis_pekerjaan($user_id, $role='admin')
	{
		$curr_year	= date("Y");
		$min_year	= $curr_year - 5;

		$sql = $this->db->select('pekerjaan.tahun as tahun,
							jenis_pekerjaan.nama as jenis, 
							count(aktivitas.id) as total')
						->from($this->main_table)
						->join('jenis_pekerjaan', 'aktivitas.jenis_pekerjaan_id=jenis_pekerjaan.id', 'left')
						->join('pekerjaan', 'aktivitas.pekerjaan_id=pekerjaan.id', 'inner');

		if($role!=='admin'){
			$sql->where('aktivitas.pptk', $user_id);
		}

		return $sql->where('pekerjaan.tahun <= ', $curr_year)
					->where('pekerjaan.tahun >= ', $min_year)
					->group_by('pekerjaan.tahun, jenis_pekerjaan.nama')
					->order_by('total', 'asc')
					->get()->result_array();
	}

	

	public function stat_status_aktivitas($user_id, $role='admin')
	{
		$sql = $this->db->select('status, count(id) as total')
						->from($this->main_table);
		if($role!=='admin'){
			$sql->where('pptk', $user_id);
		}

		return $sql->group_by('status')
					->order_by('total', 'asc')
					->get()->result();
	}

	// Statistik anggaran pada tahun ini
	public function stat_anggaran($user_id, $role='Super Admin')
	{
		$sql = $this->db->select('sum(pekerjaan.anggaran_awal) as total_anggaran')
						->from('pekerjaan')
						->join('aktivitas', 'pekerjaan.id=aktivitas.id', 'left');
						
		if($role=='Super Admin'){
			$sql->where('pekerjaan.tahun', date("Y"));
		}
		else {
			$sql->where('aktivitas.pptk', $user_id)
				->or_where('aktivitas.ppkom', $user_id)
				->or_where('aktivitas.bendahara', $user_id)
				->where('pekerjaan.tahun', date("Y"));
		}

		return $sql->get()->row();
	}

	// Statistik anggaran dalam 5 tahun
	public function stat_5_anggaran($user_id, $role='admin')
	{
		$curr_year	= date("Y");
		$min_year	= $curr_year - 5;

		$sql = $this->db->select('pekerjaan.tahun as tahun,
								sum(pekerjaan.anggaran_awal) as total_anggaran,
								sum(aktivitas.total_realisasi) as total_serapan')
						->from('pekerjaan')
						->join('aktivitas', 'pekerjaan.id=aktivitas.id', 'left');
						
		if($role!='admin'){
			$sql->join('users', 'aktivitas.pptk=users.id', 'left')
				->where('aktivitas.pptk', $user_id);
		}

		return $sql->where('pekerjaan.tahun <= ', $curr_year)
					->where('pekerjaan.tahun >= ', $min_year)
					->group_by('pekerjaan.tahun')
					->order_by('pekerjaan.tahun', 'asc')
					->get()->result();
	}

	// Statistik jumlah aktivitas pada tahun ini
	public function stat_jml_aktivitas($user_id, $role='Super Admin')
	{
		$sql = $this->db->select('count(aktivitas.id) as jml_aktivitas')
						->from('aktivitas')
						->join('pekerjaan', 'pekerjaan.id=aktivitas.id', 'left')
						->join('jenis_pekerjaan', 'aktivitas.jenis_pekerjaan_id=jenis_pekerjaan.id', 'left')
						->join('jenis_pengadaan', 'aktivitas.pengadaan_id=jenis_pengadaan.id', 'left');

		if($this->is_kontrak_pekerjaan==TRUE) {
			$sql->where_in('jenis_pekerjaan.nama', $this->filter_jenis_pekerjaan)
				->where_in('jenis_pengadaan.nama', $this->filter_jenis_pengadaan)
				->where('aktivitas.anggaran_awal >=', $this->filter_min_anggaran)
				->where('aktivitas.anggaran_awal <=', $this->filter_max_anggaran);
		}

		if($role=='Super Admin'){
			$sql->where('pekerjaan.tahun', date("Y"));
		}
		else {
			$sql->where('aktivitas.pptk', $user_id)
				->or_where('aktivitas.ppkom', $user_id)
				->or_where('aktivitas.bendahara', $user_id)
				->where('pekerjaan.tahun', date("Y"));
		}

		return $sql->get()->row();
	}

	// Statistik jumlah aktivitas dalam 5 tahun 
	public function stat_5_jml_aktivitas($user_id, $role='admin')
	{
		$curr_year	= date("Y");
		$min_year	= $curr_year - 5;

		$sql = $this->db->select('pekerjaan.tahun as tahun,
								count(aktivitas.id) as jml_aktivitas')
						->from('pekerjaan')
						->join('aktivitas', 'pekerjaan.id=aktivitas.id', 'left');
		
		if($role!='admin'){
			$sql->join('users', 'aktivitas.pptk=users.id', 'left')
				->where('aktivitas.pptk', $user_id);
		}

		return $sql->where('pekerjaan.tahun <= ', $curr_year)
					->where('pekerjaan.tahun >= ', $min_year)
					->group_by('pekerjaan.tahun')
					->order_by('pekerjaan.tahun', 'asc')
					->get()->result();
	}

	// Statistik jumlah pptk pada tahun ini
	public function stat_pptk()
	{
		return $this->db->select('count(DISTINCT(aktivitas.pptk)) as jml_pptk')
						->from('aktivitas')
						->join('pekerjaan', 'aktivitas.pekerjaan_id=pekerjaan.id', 'inner')
						->where('pekerjaan.tahun', date("Y"))
						->get()->row();
	}

	// Statistik jumlah pptk dalam 5 tahun
	public function stat_5_pptk()
	{
		$curr_year	= date("Y");
		$min_year	= $curr_year - 5;

		return $this->db->select('pekerjaan.tahun as tahun,
								count(DISTINCT(aktivitas.pptk)) as jml_pptk')
						->from('aktivitas')
						->join('pekerjaan', 'aktivitas.pekerjaan_id=pekerjaan.id', 'inner')
						->where('pekerjaan.tahun <= ', $curr_year)
						->where('pekerjaan.tahun >= ', $min_year)
						->group_by('pekerjaan.tahun')
						->get()->result();
	} 
	/*================ /Statistik Page Beranda ================*/

	/*================ Query for generate report ================*/
	public function report($user_id, $role='admin', $jenis_pekerjaan=FALSE, $jenis_pengadaan=FALSE, $status=FALSE, $tgl_awal=FALSE, $tgl_akhir=FALSE)
	{		
		$query = $this->db->select('aktivitas.nama_aktivitas as nama_aktivitas, 
						pekerjaan.nama_pekerjaan as nama_pekerjaan,
						users.nama as pptk, 
						pengadaan.nama as jenis_pengadaan, 
						jenis_pekerjaan.nama as jenis_pekerjaan, 
						aktivitas.status as status, 
						aktivitas.keterangan as keterangan, 
						aktivitas.nilai_kontrak as nilai_kontrak, 
						aktivitas.anggaran_awal as anggaran_awal, 
						vendor.nama as nama_vendor,
						kontrak.no_kontrak as no_kontrak, 
						sum(realisasi.realisasi_presentase) as realisasi_presentase,
						sum(realisasi.realisasi_fisik) as realisasi_fisik,
						sum(realisasi.rencana_presentase) as rencana_presentase,
						sum(realisasi.rencana_fisik) as rencana_fisik,
						realisasi.status_teknis as status_teknis,
						realisasi.status_pencairan as status_pencairan')
						->from('realisasi')
						->join('kontrak', 'realisasi.kontrak_id = kontrak.id', 'right')
						->join($this->main_table, 'kontrak.aktivitas_id = aktivitas.id', 'right')
						->join('pekerjaan', 'aktivitas.pekerjaan_id = pekerjaan.id', 'left')
						->join('program', 'pekerjaan.uniq_code = program.kode_program','left')
						->join('users', 'users.id = aktivitas.pptk', 'left')
						->join('vendor', 'vendor.id = kontrak.vendor_id', 'left')
						->join('jenis_pekerjaan', 'jenis_pekerjaan.id = aktivitas.jenis_pekerjaan_id', 'left')
						->join('jenis_pengadaan as pengadaan', 'pengadaan.id=aktivitas.pengadaan_id', 'left');

		if($jenis_pekerjaan!=='semua') {
			$query->where('aktivitas.jenis_pekerjaan_id', $jenis_pekerjaan);
		}

		if($jenis_pengadaan!=='semua') {
			$query->where('aktivitas.pengadaan_id', $jenis_pengadaan);
		}

		if($status!=='semua'){
			$query->where('aktivitas.status', $status);
		}

		if (($tgl_awal!=NULL) && ($tgl_akhir==NULL)) {
			return $query->where('kontrak.awal_kontrak', $tgl_awal)
						->get()->result();
		}
		else {
			$query->where('kontrak.awal_kontrak >= ', $tgl_awal)
				->where('kontrak.akhir_kontrak <= ', $tgl_akhir);
		}

		if($role!=='admin') {
			$query->where('aktivitas.pptk', $user_id);
		}

		return $query->group_by('aktivitas.nama_aktivitas, pekerjaan.nama_pekerjaan, users.nama, pengadaan.nama, jenis_pekerjaan.nama, vendor.nama, aktivitas.status, aktivitas.keterangan, aktivitas.nilai_kontrak, aktivitas.anggaran_awal, kontrak.no_kontrak, realisasi.status_teknis, realisasi.status_pencairan')
					->get()->result();		
	}
	/*================ /Query for generate report ================*/

	// Get distinct program_id and pekerjaan_id for spesific user
	public function get_program_pekerjaan($user_id, $column)
	{
		$sql = $this->db->select('DISTINCT program.id as program_id, pekerjaan.id as pekerjaan_id', FALSE)
						->from($this->main_table)
						->join('pekerjaan', 'pekerjaan.id=aktivitas.pekerjaan_id', 'inner')
						->join('program', 'program.uniq_code=pekerjaan.kode_program', 'inner')
						->where('aktivitas.pptk', $user_id)
						->get()->result();
		return array_column($sql, $column);
	}

	// Datatable kontrak_pekerjaan
	public function aktivitas_kontrak($user_id, $role='Super Admin')
	{
		return $this->db->select('aktivitas.id as id, 
						aktivitas.nama_aktivitas as nama_aktivitas, 
						pengadaan.nama as jenis_pengadaan, 
						jenis_pekerjaan.nama as jenis_pekerjaan, 
						ppkom.pegawai_nama_lengkap as ppkom,
						pegawai.nama_lengkap as pptk,
						bendahara.pegawai_nama_lengkap as bendahara,
						aktivitas.anggaran_awal as anggaran_awal')
					->from($this->main_table)
					->join('pegawai', 'pegawai.id = aktivitas.pptk', 'left')
					->join('view_data_pegawai as ppkom', 'ppkom.pegawai_id = aktivitas.ppkom', 'left')
					->join('view_data_pegawai as bendahara', 'bendahara.pegawai_id = aktivitas.bendahara', 'left')
					->join('jenis_pengadaan as pengadaan', 'aktivitas.pengadaan_id = pengadaan.id', 'left')
					->join('jenis_pekerjaan', 'aktivitas.jenis_pekerjaan_id = jenis_pekerjaan.id', 'left')
					->where_in('pengadaan.nama', ['Pengadaan Langsung', 'Penunjukan Langsung'])
					->get()->result();
	}

	/*====== Serverside Datatable ======*/
	private function _get_datatables_query($pekerjaan_id=NULL, $user_id, $role='Super Admin')
	{
		$this->db->select("aktivitas.id as id, 
					aktivitas.nama_aktivitas as nama_aktivitas, 
					pengadaan.nama as jenis_pengadaan, 
					jenis_pekerjaan.nama as jenis_pekerjaan, 
					coalesce(pegawai_ppkom.pegawai_nama_lengkap, 'kosong') as ppkom,
					coalesce(pegawai.nama_lengkap, 'kosong') as pptk,
					aktivitas.anggaran_awal as anggaran_awal,
					pekerjaan.tahun as tahun");
		$this->db->from($this->main_table);
		$this->db->join('pegawai', 'pegawai.id = aktivitas.pptk', 'left');
		$this->db->join('view_data_pegawai as bendahara', 'bendahara.pegawai_id = aktivitas.bendahara', 'left');
		$this->db->join('jenis_pengadaan as pengadaan', 'pengadaan.id = aktivitas.pengadaan_id', 'left');
		$this->db->join('jenis_pekerjaan', 'jenis_pekerjaan.id = aktivitas.jenis_pekerjaan_id', 'left');
		$this->db->join('pejabat_pembuat_komitmen', 'aktivitas.ppkom=pejabat_pembuat_komitmen.id', 'left');
		$this->db->join('view_data_pegawai as pegawai_ppkom', 'pejabat_pembuat_komitmen.pegawai_id=pegawai_ppkom.pegawai_id', 'left');
		$this->db->join('pekerjaan', 'aktivitas.pekerjaan_id=pekerjaan.id', 'left');

		if($this->is_kontrak_pekerjaan==TRUE) {
			$this->db->where_in('jenis_pekerjaan.nama', $this->filter_jenis_pekerjaan)
					->where_in('pengadaan.nama', $this->filter_jenis_pengadaan)
					->where('aktivitas.anggaran_awal >=', $this->filter_min_anggaran)
					->where('aktivitas.anggaran_awal <=', $this->filter_max_anggaran);
		}

		if ($pekerjaan_id!=NULL) {
			$this->db->where('aktivitas.pekerjaan_id', $pekerjaan_id);
		}
						
		if($role!='Super Admin') {
			$this->db->where('pegawai.id', $user_id);
			$this->db->or_where('pegawai_ppkom.pegawai_id', $user_id);
			$this->db->or_where('bendahara.pegawai_id', $user_id);
		}

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if(isset($_POST['search']['value'])) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			
			$i++;
		}
		
		if(!empty($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($pekerjaan_id=NULL, $user_id, $role='Super Admin')
	{
		if($role=='Staff'){
			$this->_get_datatables_query($pekerjaan_id, $user_id, $role);
		}
		else {
			$this->_get_datatables_query($pekerjaan_id, $user_id, $role='Super Admin');
		}
		
		if(isset($_POST['length']) and $_POST['length'] != -1 and isset($_POST['start']))
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($pekerjaan_id=NULL, $user_id, $role='Super Admin')
	{
		if(($role!=='Admin') and ($role!=='Super Admin')) {
			$this->_get_datatables_query($pekerjaan_id, $user_id, $role);
		}
		else {
			$this->_get_datatables_query($pekerjaan_id, $user_id, $role='Super Admin');
		}
		

		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($pekerjaan_id=NULL, $user_id, $role='Super Admin')
	{
		$this->db->select("aktivitas.id as id, 
					aktivitas.nama_aktivitas as nama_aktivitas, 
					pengadaan.nama as jenis_pengadaan, 
					jenis_pekerjaan.nama as jenis_pekerjaan, 
					coalesce(pegawai_ppkom.pegawai_nama_lengkap, 'kosong') as ppkom,
					coalesce(pegawai.nama_lengkap, 'kosong') as pptk,
					coalesce(bendahara.pegawai_nama_lengkap, 'kosong') as bendahara,
					aktivitas.anggaran_awal as anggaran_awal,
					pekerjaan.tahun as tahun");
		$this->db->from($this->main_table);
		$this->db->join('pegawai', 'pegawai.id = aktivitas.pptk', 'left');
		$this->db->join('view_data_pegawai as bendahara', 'bendahara.pegawai_id = aktivitas.bendahara', 'left');
		$this->db->join('jenis_pengadaan as pengadaan', 'pengadaan.id = aktivitas.pengadaan_id', 'left');
		$this->db->join('jenis_pekerjaan', 'jenis_pekerjaan.id = aktivitas.jenis_pekerjaan_id', 'left');
		$this->db->join('pejabat_pembuat_komitmen', 'aktivitas.ppkom=pejabat_pembuat_komitmen.id', 'left');
		$this->db->join('view_data_pegawai as pegawai_ppkom', 'pejabat_pembuat_komitmen.pegawai_id=pegawai_ppkom.pegawai_id', 'left');
		$this->db->join('pekerjaan', 'aktivitas.pekerjaan_id=pekerjaan.id', 'left');

		if($this->is_kontrak_pekerjaan==TRUE) {
			$this->db->where_in('jenis_pekerjaan.nama', $this->filter_jenis_pekerjaan)
					->where_in('pengadaan.nama', $this->filter_jenis_pengadaan)
					->where('aktivitas.anggaran_awal >=', $this->filter_min_anggaran)
					->where('aktivitas.anggaran_awal <=', $this->filter_max_anggaran);
		}

		if ($pekerjaan_id!=NULL) {
			$this->db->where('aktivitas.pekerjaan_id', $pekerjaan_id);
		}
						
		if($role!='Super Admin') {
			$this->db->where('pegawai.id', $user_id);
			$this->db->or_where('pegawai_ppkom.pegawai_id', $user_id);
			$this->db->or_where('bendahara.pegawai_id', $user_id);
		}

		return $this->db->count_all_results();
	}
	/*====== /Serverside Datatable ======*/
}

/* End of file Aktivitas_model.php */
/* Location: ./application/models/Aktivitas_model.php */