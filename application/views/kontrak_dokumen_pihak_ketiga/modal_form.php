<!-- Modal Logout System -->
<div class="modal fade" id="modal_dokumen_pihak_ketiga">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-warning"></i> Create Penawaran</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="form-dokumen-pihak-ketiga" method="post">
                    <input type="hidden" name="id" value="" />
                    <input type="hidden" name="kontrak_pihak_ketiga_id" value="<?php print($id)?>" />
                    
                    <div class="form-group">
                        <label for="namalengkap" class="col-sm-4 control-label">Dokumen</label>
                        <div class="col-sm-8">
                            <select name="dokumen_pihak_ketiga_id" class="form-control">
                                <option value="">-- Select Dokumen --</option>
                                
                                <?php foreach($dokumen as $item):?>
                                    <option value="<?php print($item->id); ?>"><?php print($item->nama_dokumen); ?></option>
                                <?php endforeach;?>
                            </select>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="namalengkap" class="col-sm-4 control-label">Urutan</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control money" name="urutan">
                        </div>
                        <span class="help-block"></span>
                    </div>
                   
            </div>
            <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                    <button type="submit" name="simpan" class="btn btn-primary" id="btnSaveDokumenPihakKetiga">Simpan</button>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

</div>
</div>
<!-- /Box Form Pengunjung -->