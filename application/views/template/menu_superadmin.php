<!-- Sidebar Menu -->
<ul class="sidebar-menu" data-widget="tree">
    <li class="header"><div style="text-align: center;">DAFTAR MENU</div> </li>
    <!-- Optionally, you can add icons to the links -->
    <li><a href="<?php print(base_url('beranda')); ?>"><i class="fa fa-home"></i> <span>Beranda</span></a></li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-copy"></i> <span>Info Program Pekerjaan</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="<?php print(base_url('program')); ?>"><i class="fa fa-file-text-o"></i> <span>Data Program Pekerjaan</span></a></li>
        <li><a href="<?php print(base_url('aktivitas')); ?>"><i class="fa fa-list-alt"></i> <span>Resume Pekerjaan</span></a></li>
      </ul>
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-paste"></i> <span>Kontrak Pekerjaan</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="<?php print(base_url('aktivitas/kegiatan')); ?>"><i class="fa fa-list-alt"></i> <span>Data Pekerjaan</span></a></li>
        <li><a href="<?php print(base_url('kontrak_pekerjaan')); ?>"><i class="fa fa-file-text-o"></i> <span>Resume Data Kontrak</span></a></li>
        <li><a href="#" data-toggle="modal" data-target="#modal-rekap-kontrak-pekerjaan"><i class="fa fa-print"></i> <span>Rekap Data Kontrak</span></a></li>
      </ul>
    </li>
    
    <li class="treeview">
          <a href="#"><i class="fa fa-database"></i> <span>Master Data</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i> <span>Pegawai</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php print(base_url('pegawai')); ?>"><i class="fa fa-child"></i> <span>Seluruh Pegawai</span></a></li>
                                
                <!-- Pejabat Pengadaan  -->
                <li class="treeview">
                  <a href="#">
                    <i class="fa fa-street-view"></i> <span>Pejabat Pengadaan</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="<?php print(base_url('pegawai/pejabat_pengadaan')); ?>"><i class="fa fa-file-text-o"></i> <span>Master Data</span></a></li>
                    <li><a href="<?php print(base_url('pegawai/sk_pejabat_pengadaan')); ?>"><i class="fa fa-file-text-o"></i> <span>SK Pejabat Pengadaan</span></a></li>
                  </ul>
                </li>


                <!-- Pejabat Pembuat Komitmen -->
                <li class="treeview">
                  <a href="#">
                    <i class="fa fa-street-view"></i><span> PPKOM</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="<?php print(base_url('pegawai/pejabat_pembuat_komitmen')); ?>"><i class="fa fa-file-text-o"></i> <span>Master Data</span></a></li>
                    <li><a href="<?php print(base_url('pegawai/sk_pejabat_pembuat_komitmen')); ?>"><i class="fa fa-file-text-o"></i> <span>SK PPKOM</span></a></li>
                  </ul>
                </li>

              </ul>
            </li>
            
            <li><a href="<?php print(base_url('pihak_ketiga')); ?>"><i class="fa fa-industry"></i> <span>Penyedia Barang/Jasa</span></a></li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-code-fork"></i> <span>Consume Data</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php print(base_url('lainnya/list_api')); ?>"><i class="fa fa-share-alt"></i> <span>API</span></a></li>
                <li><a href="<?php print(base_url('lainnya/list_skpd')); ?>"><i class="fa fa-bank"></i> <span>SKPD</span></a></li>
                <li class="treeview">
                  <a href="#"><i class="fa fa-gg"></i> <span>Sistem Informasi</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#" data-toggle="modal" data-target="#modal-consume-simanggaran"><i class="fa fa-money"></i> <span>Simanggaran</span></a></li>
                  </ul>
                </li>
              </ul>
            </li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-file-text-o"></i> <span>Data Lainnya</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php print(base_url('lainnya/role')); ?>"><i class="fa fa-tag"></i> <span>Role Pegawai</span></a></li>
                <li><a href="<?php print(base_url('lainnya/jabatan_fungsional')); ?>"><i class="fa fa-check-circle-o"></i> <span>Jabatan Struktural</span></a></li>
                <li><a href="<?php print(base_url('lainnya/unit_kerja')); ?>"><i class="fa fa-th-list"></i> <span>Unit Kerja</span></a></li>
                <li><a href="<?php print(base_url('lainnya/jenis_pengadaan')); ?>"><i class="fa fa-sticky-note-o"></i> <span>Metode Pengadaan</span></a></li>
                <li><a href="<?php print(base_url('lainnya/jenis_pekerjaan')); ?>"><i class="fa fa-sticky-note-o"></i> <span>Jenis Pekerjaan</span></a></li>
                <li><a href="<?php print(base_url('lainnya/jenis_pejabat_pengadaan')); ?>"><i class="fa fa-wrench"></i> <span>Jenis Pejabat Pengadaan</span></a></li>
                <li><a href="<?php print(base_url('lainnya/jenis_surat')); ?>"><i class="fa fa-envelope"></i> <span>Jenis Dokumen Pengadaan</span></a></li>
                <li><a href="<?php print(base_url('lainnya/tempat_rapat')); ?>"><i class="fa fa-map-marker"></i> <span>Data Tempat Rapat</span></a></li>
                <li><a href="<?php print(base_url('lainnya/jenis_kegiatan_pengadaan')); ?>"><i class="fa fa-sticky-note-o"></i> <span>Jenis Kegiatan Pengadaan</span></a></li>
                <li><a href="<?php print(base_url('lainnya/bank')); ?>"><i class="fa fa-bank"></i> <span>Data Bank</span></a></li>
                <li><a href="<?php print(base_url('lainnya/seri_pejabat_pengadaan')); ?>"><i class="fa fa-reorder"></i> <span>Seri Pejabat Pengadaan</span></a></li>
                <li><a href="<?php print(base_url('lainnya/jabatan_perusahaan')); ?>"><i class="fa fa-street-view"></i> <span>Jabatan Perusahaan</span></a></li>
                <li><a href="<?php print(base_url('lainnya/jenjang_pendidikan')); ?>"><i class="fa fa-graduation-cap"></i> <span>Jenjang Pendidikan</span></a></li>
                <li><a href="<?php print(base_url('lainnya/jenis_dokumen')); ?>"><i class="fa fa-file-o"></i> <span>Jenis Dokumen</span></a></li>
              </ul>
            </li>
            
          </ul>
        </li>
    <li><a href="<?php print(base_url('panduan')); ?>"><i class="fa fa-book"></i> <span>Panduan Pengguna</span></a></li>
    <li><a href="<?php print(base_url('pegawai/profil')); ?>"><i class="fa fa-user-secret"></i> <span>Profil</span></a></li>
    <li><a href="#" data-toggle="modal" data-target="#modal-logout"><i class="fa fa-user"></i> <span>Logout</span></a></li>    
</ul>
<!-- /.sidebar-menu -->