<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model 
{
    public function checking($type_user, $data, $session_data)
    {
        if($type_user=='pegawai') {
            $sql = $this->db->select('pegawai.id as pegawai_id, 
                                pegawai.nama_lengkap as pegawai_nama, 
                                role.nama as role, 
                                pegawai.is_pegawai as is_pegawai,
                                pegawai.password as password')
                        ->from('pegawai')
                        ->join('role', 'pegawai.role_id=role.id', 'inner')
						->where('username', $data['username'])
                        ->get();
            
            $total = $sql->num_rows();
            $user = $sql->row();
            
            if($total==1 and password_verify($data['password'], $user->password)) {
                $session_data = array(
                    'id'            => $user->pegawai_id,
                    'nama'          => $user->pegawai_nama,
                    'as'            => $data['person'],
                    'is_pegawai'    => $user->is_pegawai,
                    'role'          => $user->role
                );

                return $session_data;
            }
            else {
                return 'not_exist';
            }
        }
        elseif($type_user=='pihak_ketiga') {
            $sql = $this->db->select('karyawan.id as karyawan_id, 
                                karyawan.nama_lengkap as karyawan_nama, 
                                karyawan.pihak_ketiga_id as pihak_ketiga_id,
                                karyawan.password as password,
                                karyawan.pihak_ketiga_id as pihak_ketiga_id')
                        ->from('karyawan')
						->where('username', $data['username'])
                        ->where('is_user', 'y')
                        ->get();
            
            $total = $sql->num_rows();
            $user = $sql->row();

            if($total==1 and password_verify($data['password'], $user->password)) {
                $session_data = array(
                    'id'                => $user->karyawan_id,
                    'nama'              => $user->karyawan_nama,
                    'as'                => $data['person'],
                    'is_pegawai'        => '',
                    'role'              => '',
                    'pihak_ketiga_id'   => $user->pihak_ketiga_id
                );

                return $session_data;
            }
            else {
                return 'not_exist';
            }
        }
        else{
            return 'fail';
        }
    }
}

/* End of file Auth.php */
