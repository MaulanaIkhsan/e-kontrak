<?php
$this->load->view('template/header');?>
<!-- Select2 -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/select2/dist/css/select2.min.css')); ?>">
<?php $this->load->view('template/asset_header');?>
<!-- Star Rating -->
<link rel="stylesheet" href="<?php print(base_url('assets/plugins/rating/dist/starrr.css')); ?>">


<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <!-- Box Form Detail Unit Kerja -->
        <?php print($this->session->flashdata('alert')); ?>
        <?php print($this->session->flashdata('success')); ?>
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-primary" id="form-pengunjung">
              <div class="box-header with-border">
                <h3 class="box-title">Detail Pengunjung</h3>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <form class="form-horizontal" method="post" action="<?php print(base_url('unit_kerja/add'))?>">
                <div class="box-body">
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Nama</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->nama); ?></div>
                    </div>
                  </div>
                  
                                 
                </div>
                <div class="box-footer">
                  <a href="<?php print(base_url('unit_kerja'));?>" class="btn btn-primary">Kembali</a>
                  <a href="<?php print(base_url('unit_kerja/update/'.$detail->id));?>" class="btn btn-primary">Update</a>
                  <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete">Delete</a>
                </div>
                <!-- /.box-footer -->
              </form>

              <!-- Confirmation Modal -->
              <div class="modal fade" id="modal-delete">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title"><i class="fa fa-warning"></i> Konfirmasi Delete Data</h4>
                    </div>
                    <div class="modal-body">
                      <p>Apakah anda yakin ingin menghapus data <?php print($detail->nama); ?> ?</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                      <form action="<?php print(base_url('unit_kerja/delete/'.$detail->id)); ?>" method="post">
                        <button type="submit" name="delete" value="delete" class="btn btn-danger">Delete</button>
                      </form>
                      
                    </div>
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>
              <!-- /Confirmation Modal -->

            </div>
          </div>
          <!-- /Box Form Detail Unit Kerja -->

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<?php $this->load->view('template/footer');?>