<!DOCTYPE html>
<HtmL>

<HeAd>
    <MeTa HTTP-EquIV="CONTENT-TYPE" CONTent="text/html; charset=utf-8">
    <TItlE>Semarang, 29 Maret 2004</tITLE>
    <meta name=GENERATOR coNTeNT="LibreOffice 4.1.6.2 (Linux)">
    <Meta nAMe="AUTHOR" CONTeNt="DPU">
    <MEtA NaMe=CREATED cONtENt="Tahun_Anggaran0817;20400000000000">
    <MeTA NAmE="CHANGEDBY" CoNteNt="isan">
    <META NAme="CHANGED" coNTent="20190320;163019000000000">
    <MEtA nAmE=KSOProductBuildVer CONTEnt=1033-10.1.0.6757>
    <sTyLE>
        @page {
            size: 8.47in 13.98in;
            margin-right: 0.88in;
            margin-top: 0.59in;
            margin-bottom: 0.69in
        }
        P {
            margin-bottom: 0.08in;
            direction: ltr;
            color: #000000
        }
        P.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
        H1 {
            margin-left: 3.5in;
            margin-top: 0in;
            margin-bottom: 0in;
            direction: ltr;
            color: #000000;
            text-align: justify;
            text-decoration: underline
        }
        H1.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
        .text {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
        }
    </sTYLE>
</heAd>

<bOdY LANg="en-US" tExT=#000000 dIR="LTR">
    <p clasS=western ALIGN=CENTER>
        <u><strong>PENYAMPAIAN HASIL PENETAPAN PEMENANG</strong></u><br/>
        Nomor : <?php print($kontrak_surat->no_surat); ?>
    </p>
    <p></p>
    <p></p>
    <p clasS=western ALIGN="justify">
        Sehubungan dengan Pelaksanaan Pengadaan Langsung :
    </p>
    <p></p>
    <table cellpadding="2">
        <tr>
            <td class="text" style="font-size:12pt">Program</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pekerjaan->program_nama); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Kegiatan</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pekerjaan->pekerjaan_nama); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Pekerjaan</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pekerjaan->aktivitas_nama); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Sumber Dana</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pekerjaan->aktivitas_sumber_dana); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Tempat</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_surat->tempat_rapat_nama.' '.$kontrak_surat->tempat_rapat_alamat); ?></td>
        </tr>
    </table>
    <p></p>
    <p clAss=western ALIGN="justify" StylE="margin-bottom: 0in">
        Dengan ini memberitahukan bahwa setelah diadakan penelitian oleh Pejabat Pengadaan Kegiatan pada Dinas Pekerjaan Umum Kota Semarang Sumber Dana APBD Tahun Anggaran <?php print(date("Y")); ?>, sesuai dengan ketentuan-ketentuan yang berlaku dalam Peraturan Presiden Republik Indonesia Nomor 4 Tahun 2015 tentang Perubahan keempat atas Peraturan Presiden Nomor 54 Tahun 2010 tentang Pengadaan Barang/Jasa Pemerintah, dengan ini diumumkan Pemenang Pengadaan Langsung tersebut di atas adalah :
    </p>
    <p></p>
    <table cellpadding="2">
        <tr>
            <td class="text" style="font-size:12pt">Nama Perusahaan</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Alamat</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_alamat); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">NPWP</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_npwp); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt" valign="top">Harga Penawaran</td>
            <td class="text" style="font-size:12pt" valign="top">:</td>
            <td class="text" style="font-size:12pt">
                <?php print('Rp '.format_money($kontrak_pihak_ketiga[0]->nilai_kontrak).',-'); ?><br/>
                (<?php print(ucwords(strtolower(terbilang($kontrak_pihak_ketiga[0]->nilai_kontrak))).' Rupiah'); ?>)
            </td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt" valign="top">Harga Negosiasi</td>
            <td class="text" style="font-size:12pt" valign="top">:</td>
            <td class="text" style="font-size:12pt">
                <?php print('Rp '.format_money($kontrak_pekerjaan->harga_negosiasi).',-'); ?><br/>
                (<?php print(ucwords(strtolower(terbilang($kontrak_pekerjaan->harga_negosiasi))).' Rupiah'); ?>)
            </td>
        </tr>
    </table>
    <p cLaSs="western" ALIGN="justify" StYLe="margin-bottom: 0in">
        Demikian pengumuman ini agar para calon penyedia / penawa r Barang / Jasa maklum.
    </p>
    <p></p>
    <p></p>
    <table>
        <tr>
            <td width="250"></td>
            <td width="250"></td>
            <td width="500" align="center" style="font-size:16pt">
                <?php print($pejabat_pengadaan->jenis_pejabat_pengadaan_nama); ?> <?php print($pejabat_pengadaan->seri_pejabat_pengadaan_nama); ?><br/>
                Pada Dinas Pekerjaan Umum Kota Semarang<br/>
                Tahun Anggaran <?php print(date("Y"));?><br/>
                <br/><br/><br/><br/><br/>
                <u><?php print($pejabat_pengadaan->pegawai_nama); ?></u><br/>
                NIP. <?php print($pejabat_pengadaan->pegawai_nip); ?>
            </td>
        </tr>
    </table>
    <p></p>
    <p></p>
    <p ClaSs=western StYLe="margin-bottom: 0in">
        Tembusan Yth : <br/>
        <ol type="i">
            <li>Kuasa Pengguna Anggaran selaku Pejabat Pembuat Komitmen</li>
            <li>Arsip</li>
        </ol>
    </p>

</BODY>

</HTML>