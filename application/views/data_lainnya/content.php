<?php $this->load->view('template/header');?>
<?php foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php $this->load->view('template/asset_header');?>

<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
    	 <div class="row"> 
    	 	<div class="col-md-12">
    	 		<div class="box box-warning">
	            	<div class="box-header with-border">
	                	<h3 class="box-title"><?php print($title_content); ?></h3>
	            	</div>
	            	<div class="box-body">	
						<?php echo $output; ?>
	            	</div>
	            </div>
    	 	</div>
	        
    	</div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<?php $this->load->view('template/footer');?>