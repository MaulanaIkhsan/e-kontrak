<!DOCTYPE html>
<HtmL>

<HeAd>
    <MeTa HTTP-EquIV="CONTENT-TYPE" CONTent="text/html; charset=utf-8">
    <TItlE>Semarang, 29 Maret 2004</tITLE>
    <meta name=GENERATOR coNTeNT="LibreOffice 4.1.6.2 (Linux)">
    <Meta nAMe="AUTHOR" CONTeNt="DPU">
    <MEtA NaMe=CREATED cONtENt="Tahun_Anggaran0817;20400000000000">
    <MeTA NAmE="CHANGEDBY" CoNteNt="isan">
    <META NAme="CHANGED" coNTent="20190320;163019000000000">
    <MEtA nAmE=KSOProductBuildVer CONTEnt=1033-10.1.0.6757>
    <sTyLE>
        @page {
            size: 8.47in 13.98in;
            margin-right: 0.88in;
            margin-top: 0.59in;
            margin-bottom: 0.69in
        }
        P {
            margin-bottom: 0.08in;
            direction: ltr;
            color: #000000
        }
        P.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
        H1 {
            margin-left: 3.5in;
            margin-top: 0in;
            margin-bottom: 0in;
            direction: ltr;
            color: #000000;
            text-align: justify;
            text-decoration: underline
        }
        H1.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
    </sTYLE>
</heAd>

<bOdY LANg="en-US" tExT=#000000 dIR="LTR">
    <p>
        <Img SRc="<?php print(base_url('assets/template_surat/img/kop_surat_dpu_monokrom.png'));?>" nAme="Picture 8" width="100%" ALIGN=center BORDER=0>
    </p>
    <p></p>
    <table CELLPADDING="7" cellspacing="0">
        <tr>
            <td align="center" valign="middle" rowspan="2" width="400" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                <strong>SURAT PERINTAH KERJA</strong>
            </td>
            <td width="600" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                <strong>DINAS PEKERJAAN UMUM KOTA SEMARANG</strong>
            </td>
        </tr>
        <tr>
            <td width="600" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                Nomor : <br/>
                Tanggal : </br/>
            </td>
        </tr>
        <tr>
            <td align="center" valign="middle" rowspan="2" width="400" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                <strong>PAKET PEKERJAAN :</strong><br/>
                <?php print($kontrak_pekerjaan->aktivitas_nama); ?>
            </td>
            <td valign="center" width="400" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                <?php
                $no_surat_undangan  = NULL;
                $tgl_surat_undangan = 'Kosong';
                if(!empty($surat_undangan)) {
                    $no_surat_undangan = $surat_undangan[0]->no_surat;
                    $tgl_surat_undangan = $surat_undangan[0]->tgl_surat;
                }
                ?>
                NOMOR DAN TANGGAL SURAT UNDANGAN PENGADAAN LANGSUNG <?php print($no_surat_undangan); ?>, <?php print($tgl_surat_undangan!='Kosong'?tgl_indo($tgl_surat_undangan):$tgl_surat_undangan); ?>
            </td>
        </tr>
        <tr>
            <td valign="center" width="400" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
            <?php
            $no_bahpl   = NULL;
            $tgl_bahpl  = 'Kosong';
            if(!empty($bahpl)) {
                $no_bahpl = $bahpl[0]->no_surat;
                $tgl_bahpl = $bahpl[0]->tgl_surat;
            }
            ?>
            NOMOR DAN TANGGAL, BERITA ACARA HASIL PENGADAAN LANGSUNG <?php print($no_bahpl); ?>, <?php print($tgl_bahpl!='Kosong'?tgl_indo($tgl_bahpl):$tgl_bahpl); ?>
            </td>
        </tr>
        <tr>
            <td valign="center" align="justify" colspan="2" width="400" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                <strong>SUMBER DANA : </strong><br/>
                <?php print($kontrak_pekerjaan->aktivitas_sumber_dana); ?> untuk mata anggaran kegiatan <?php print(ucwords(strtolower($kontrak_pekerjaan->pekerjaan_nama))); ?>
            </td>
        </tr>
        <tr>
            <td valign="center" align="justify" colspan="2" width="400" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                <strong>WAKTU PELAKSANAAN PEKERJAAN : </strong><br/>
                <?php print($kontrak_pekerjaan->durasi_kontrak); ?> (<?php print(terbilang($kontrak_pekerjaan->durasi_kontrak)); ?>) hari kalender <?php print(tgl_indo($kontrak_pekerjaan->tgl_awal_kontrak)); ?>
            </td>
        </tr>
        <tr>
            <td valign="middle" align="justify" colspan="2" width="400" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                
                <table CELLPADDING="7" cellspacing="0" align="right">
                    <tr>
                        <td width="80" valign="middle" align="justify" colspan="2" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:8pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                            Paraf PPKOM
                        </td>
                        <td width="80" valign="middle" align="justify" colspan="2" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:8pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">

                        </td>
                    </tr>
                    <tr>
                        <td width="80" valign="middle" align="justify" colspan="2" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:8pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                            Paraf Penyedia
                        </td>
                        <td width="80" valign="middle" align="justify" colspan="2" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:8pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">

                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
    <p></p>
    <p></p>


   
</BODY>

</HTML>