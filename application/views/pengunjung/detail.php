<?php
$this->load->view('template/header');?>
<!-- Select2 -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/select2/dist/css/select2.min.css')); ?>">
<?php $this->load->view('template/asset_header');?>
<!-- Star Rating -->
<link rel="stylesheet" href="<?php print(base_url('assets/plugins/rating/dist/starrr.css')); ?>">


<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <!-- Box Form Pengunjung -->
        <?php print($this->session->flashdata('alert')); ?>
        <?php print($this->session->flashdata('success')); ?>
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-primary" id="form-pengunjung">
              <div class="box-header with-border">
                <h3 class="box-title">Detail Pengunjung</h3>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <form class="form-horizontal" method="post">
                <div class="box-body">
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">NIK</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->nik); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Nama Lengkap</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->nama); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Alamat</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->alamat); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Jenis Kelamin</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->jenis_kelamin); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="keperluan" class="col-sm-2 control-label">Asal</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->asal); ?></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Nomor Handphone</label>
                    <div class="col-sm-9">
                      <div class="form-control"><?php print($detail->no_hp); ?></div>
                    </div>
                  </div>                  
                </div>
                 <div class="box-footer">
                  <a href="<?php print(base_url('pengunjung'));?>" class="btn btn-primary">Kembali</a>
                  <a href="<?php print(base_url('pengunjung/update/'.$detail->id));?>" class="btn btn-primary">Update</a>
                  <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete">Delete</a>
                </div>
                <!-- /.box-footer -->
              </form>

              <!-- Confirmation Modal -->
              <div class="modal fade" id="modal-delete">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title"><i class="fa fa-warning"></i> Konfirmasi Delete Data</h4>
                    </div>
                    <div class="modal-body">
                      <p>Apakah anda yakin ingin menghapus data <?php print($detail->nama); ?> ?</p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                      <form action="<?php print(base_url('pengunjung/delete/'.$detail->id)); ?>" method="post">
                        <button type="submit" name="delete" value="delete" class="btn btn-danger">Delete</button>
                      </form>
                      
                    </div>
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>
              <!-- /Confirmation Modal -->

            </div>
          </div>
          <!-- /Box Form Pengunjung -->

          <!-- Box Show Kunjungan -->
          <div class="col-md-12">
            <div class="box box-primary" id="data-kunjungan">
              <div class="box-header with-border">
                <h3 class="box-title">Data Kunjungan</h3>
              </div>
              <div class="box-body">
                <table id="kunjungan" class="table table-bordered table-striped table-responsive">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Tanggal</th>
                      <th>Waktu</th>
                      <th>Nama Pengunjung</th>
                      <th>Asal</th>
                      <th>Tujuan</th>
                      <th>Keperluan</th>
                      <th><i class="fa fa-gear"></i></th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </div> 

            </div>

          </div>
          <!-- Box Show Kunjungan -->

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<!-- Select2 -->
<script src="<?php print(base_url('assets/bower_components/select2/dist/js/select2.full.min.js'));?>"></script>
<!-- Star Rating -->
<script type="text/javascript" src="<?php print(base_url('assets/plugins/rating/dist/starrr.js')); ?>"></script>
<script>
  $(function(){
    var data = $('#kunjungan').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
            "ajax": {
                "url": "<?php echo site_url('kunjungan/get_data/'.$detail->id);?>",
                "type": "POST"
            },
            dom: 'lBfrtip',
            button:[{
              extend:'pdf',
              oriented:'potrait',
              pageSize: 'A4',
              title : 'Data Kunjungan',
              download : 'open'
            }, 'copy', 'csv', 'excel', 'pdf', 'print'],
            "columnDefs": [{ 
                "targets": [ 0 ], 
                "orderable": false, 
            }]
        });    
  });
</script>
<?php $this->load->view('template/footer');?>