<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengunjung_model extends CI_Model {

	//column : 
	// id
	// nik
	// nama
	// alamat
	// jenis_kelamin
	// asal
	// no_hp
	var $column_order = array(null, 'id','nik', 'nama', 'alamat', 'jenis_kelamin', 'asal', 'no_hp'); 

	var $column_search = array('id','nik', 'nama', 'alamat', 'jenis_kelamin', 'asal', 'no_hp');

	var $order = array('id' => 'asc'); // default order 

	public function data_pengunjung()
	{
		return $this->db->select('id, nik, nama')
						->from('tamu')
						->get()->result();
	}

	public function detail_pengunjung($pengunjung_id)
	{
		return $this->db->select('id, nik, nama, alamat, jenis_kelamin, asal, no_hp')
						->from('tamu')
						->where('id', $pengunjung_id)
						->get()->row();
	}

	public function get_pengunjung_id($nama, $nik)
	{
		return $this->db->select('id')
						->from('tamu')
						->where('nama', $nama)
						->where('nik', $nik)
						->get()->row();
	}

	public function get_pengunjung_nama($id)
	{
		return $this->db->select('nama')
						->from('tamu')
						->where('id', $id)
						->get()->row();
	}

	public function get_last_id()
	{
		$sql = $this->db->select('id')
						->from('tamu')
						->order_by('id', 'desc')
						->limit(10)
						->get()->row();
		return $sql->id;
	}

	/*====== Serverside Datatable ======*/
	private function _get_datatables_query()
	{
		$this->db->select('id, nik, nama, alamat, asal');
		$this->db->from('tamu');

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{	
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->select('id, nik, nama, alamat, asal');
		$this->db->from('tamu');

		return $this->db->count_all_results();
	}
	/*====== /Serverside Datatable ======*/

}

/* End of file Pengunjung_model.php */
/* Location: ./application/models/Pengunjung_model.php */