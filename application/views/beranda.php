<?php $this->load->view('template/header');?>
<?php $this->load->view('template/asset_header');?>
 <!-- Morris charts -->
 <link rel="stylesheet" href="<?php print(base_url('assets/bower_components/morris.js/morris.css')); ?>" />
<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
            <?php print($this->session->flashdata('error')); ?>
            <?php print($this->session->flashdata('warning')); ?>
            <?php print($this->session->flashdata('alert')); ?>
            <?php print($this->session->flashdata('success')); ?>           
            
            <div class="col-md-12">
                <div class="small-box bg-red">
                    <div class="inner">
                        <p><strong>Total Anggaran <?php print($curr_year); ?></strong></p>
                        <h3><?php print('Rp '.format_money($jml_anggaran->total_anggaran).',-'); ?></h3>
                    </div>
                    <div class="icon">
                        <i class="fa fa-money"></i>
                    </div>
                    <!-- <a href="#" class="small-box-footer">Detail <i class="fa fa-arrow-circle-right"></i></a> -->
                    <div class="small-box-footer" style="height: 25px;"></div>
                </div>
            </div>
            
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        <div class="small-box bg-blue">
                            <div class="inner">
                                <p><strong>Total Pekerjaan <?php print($curr_year); ?></strong></p>
                                <h3><?php print(format_money($jml_aktivitas->jml_aktivitas)); ?></h3>
                                
                            </div>
                            <div class="icon">
                                <i class="fa fa-cubes"></i>
                            </div>
                            <a href="<?php print(base_url('aktivitas'));?>" class="small-box-footer">Detail <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                        <div class="small-box bg-green">
                            <div class="inner">
                                <p><strong>Total Pihak Ketiga <?php print($curr_year); ?></strong></p>
                                <h3><?php print(format_money($jml_pihak_ketiga)); ?></h3>
                                
                            </div>
                            <div class="icon">
                                <i class="fa fa-group"></i>
                            </div>
                            <a href="<?php print(base_url('pihak_ketiga'));?>" class="small-box-footer">Detail <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <p><strong>Total Kontrak Pekerjaan <?php print($curr_year); ?></strong></p>
                                <h3><?php print(format_money($jml_kontrak_pekerjaan->jml_kontrak)); ?></h3>
                                
                            </div>
                            <div class="icon">
                                <i class="fa fa-file-text-o"></i>
                            </div>
                            <a href="<?php print(base_url('kontrak_pekerjaan'));?>" class="small-box-footer">Detail <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="box box-warning">
                            <div class="box-header with-border">
                                <h3 class="box-title">Statistik Data Perintah Kerja <?php print($curr_year); ?></h3>
                            </div>
                            <div class="box-body">
                                <div style="height: 365px;" id="status_kontrak_pekerjaan"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-6">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Metode Pengadaan <?php print($curr_year); ?></h3>
                    </div>
                    <div class="box-body">
                        <div id="pengadaan_graph"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Jenis Pekerjaan <?php print($curr_year); ?></h3>
                    </div>
                    <div class="box-body">
                        <div id="pekerjaan_graph"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Statistik Awal Kontrak <?php print($curr_year); ?></h3>
                    </div>
                    <div class="box-body">
                        <div id="stat_awal_kontrak"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Statistik Akhir Kontrak <?php print($curr_year); ?></h3>
                    </div>
                    <div class="box-body">
                        <div id="stat_akhir_kontrak"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<!-- Morris.js charts -->
<script src="<?php print(base_url('assets/bower_components/raphael/raphael.min.js')); ?>"></script>
<script src="<?php print(base_url('assets/bower_components/morris.js/morris.min.js')); ?>"></script>

<script type="text/javascript">
    $(function(){
        if($('#pekerjaan_graph').length) {
			Morris.Bar({
				element: 'pekerjaan_graph',
				data: [
				<?php if(!empty($jenis_pekerjaan)) {
					foreach($jenis_pekerjaan as $item):
						print("{jenis_pekerjaan:'".$item->jenis_pekerjaan."'");
						print(",jumlah:".$item->total."},");
					endforeach;
				} ?>
				],
				xkey: 'jenis_pekerjaan',
				ykeys: ['jumlah'],
				labels: ['Jumlah'],
				barRatio: 0.4,
				barColors: ['#CF3A24', '#34495E', '#ACADAC', '#3498DB'],
				xLabelAngle: 35,
				hideHover: 'auto',
				resize: true
			});
		}

		if($('#pengadaan_graph').length) {
			Morris.Bar({
				element: 'pengadaan_graph',
				data: [
				<?php if(!empty($metode_pengadaan)) {
					foreach($metode_pengadaan as $item):
						print("{jenis_pengadaan:'".$item->jenis_pengadaan."'");
						print(",jumlah:".$item->total."},");
					endforeach;
				} ?>
				],
				xkey: 'jenis_pengadaan',
				ykeys: ['jumlah'],
				labels: ['Jumlah'],
				barRatio: 0.4,
				barColors: ['#CF3A24', '#34495E', '#ACADAC', '#3498DB'],
				xLabelAngle: 35,
				hideHover: 'auto',
				resize: true
			});
		}

        if($('#stat_awal_kontrak').length) {
			Morris.Bar({
				element: 'stat_awal_kontrak',
				data: [
				<?php if(!empty($awal_kontrak)) {
					foreach($awal_kontrak as $item):
						print("{bulan:'".$item->bulan."'");
						print(",jumlah:".$item->jml_awal_kontrak."},");
					endforeach;
				} ?>
				],
				xkey: 'bulan',
				ykeys: ['jumlah'],
				labels: ['Jumlah'],
				barRatio: 0.4,
				barColors: ['#CF3A24', '#34495E', '#ACADAC', '#3498DB'],
				xLabelAngle: 35,
				hideHover: 'auto',
				resize: true
			});
		}

		if($('#stat_akhir_kontrak').length) {
			Morris.Bar({
				element: 'stat_akhir_kontrak',
				data: [
				<?php if(!empty($akhir_kontrak)) {
					foreach($akhir_kontrak as $item):
						print("{bulan:'".$item->bulan."'");
						print(",jumlah:".$item->jml_akhir_kontrak."},");
					endforeach;
				} ?>
				],
				xkey: 'bulan',
				ykeys: ['jumlah'],
				labels: ['Jumlah'],
				barRatio: 0.4,
				barColors: ['#CF3A24', '#34495E', '#ACADAC', '#3498DB'],
				xLabelAngle: 35,
				hideHover: 'auto',
				resize: true
			});
		}

        if($('#status_kontrak_pekerjaan').length) {
			Morris.Bar({
				element: 'status_kontrak_pekerjaan',
				data: [
				<?php if(!empty($akhir_kontrak)) {
					foreach($jml_status_kontrak_pekerjaan as $item):
						print("{status:'".$item->status."'");
						print(",jumlah:".$item->jml_kontrak."},");
					endforeach;
				} ?>
				],
				xkey: 'status',
				ykeys: ['jumlah'],
				labels: ['Jumlah'],
				barRatio: 0.4,
				barColors: ['#CF3A24', '#34495E', '#ACADAC', '#3498DB'],
				xLabelAngle: 35,
				hideHover: 'auto',
				resize: true
			});
		}
    });
</script>
<?php $this->load->view('template/footer');?>