<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jabatan_perusahaan_model extends CI_Model {

    public function fetch_data($id=NULL)
    {
        $sql = $this->db->select('jabatan_perusahaan.id as id,
                            jabatan_perusahaan.nama as nama')
                        ->from('jabatan_perusahaan');
        
        if($id!=NULL) {
            return $sql->where('id', $id)
                        ->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }

}

/* End of file Jabatabn_perusahaan_model.php */
