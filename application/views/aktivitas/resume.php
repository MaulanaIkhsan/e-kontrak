<?php
$this->load->view('template/header');?>
<?php $this->load->view('template/asset_header');?>

<link rel="stylesheet" type="text/css" href="<?php print(base_url('assets/bower_components/datatables.net-bs/css/custom_button_export.css'))?>" />

<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <?php print($this->session->flashdata('alert')); ?>
        <?php print($this->session->flashdata('success')); ?>
        <div class="col-md-12">
            <!-- Horizontal Form -->
          <div class="box box-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Resume Data Pekerjaan</h3>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <div class="box-body">
                <table id="aktivitas" class="table table-bordered table-striped table-responsive">
                    <thead>
                        <tr class="headings" align="center">
                            <th class="column-title" align="center">No</th>
                            <th class="column-title" align="center">Nama Pekerjaan</th>
                            <th class="column-title" align="center">Tahun</th>
                            <th class="column-title" align="center">PPKOM</th>
                            <th class="column-title" align="center">PPTK</th>
                            <th class="column-title" align="center">Jenis Pekerjaan</th>
                            <th class="column-title" align="center">Metode Pengadaan</th>
                            <th class="column-title" align="center">Anggaran</th>
                            <th class="column-title" align="center">Aksi</th>
                        </tr>
                    </thead>
                  <tbody></tbody>
                </table>
              </div> 

            </div>
          </div>

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/dataTables.buttons.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.flash.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/jszip.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/pdfmake.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/vfs_fonts.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.html5.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.print.min.js'))?>"></script>

<script>
  $(function(){
    var data = $('#aktivitas').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "ajax": {
                "url": "<?php echo site_url('aktivitas/get_data');?>",
                "type": "POST"
            },
            "order": [],
            dom: 'lBfrtip',
            button:[{
              extend:'pdf',
              oriented:'potrait',
              pageSize: 'A4',
              title : 'Data Kegiatan',
              download : 'open'
            }, 'copy', 'csv', 'excel', 'pdf', 'print'],
            "columnDefs": [{ 
                "targets": [ 0 ], 
                "orderable": false, 
            }]
        });
    console.log(data);
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>
<?php $this->load->view('template/footer');?>