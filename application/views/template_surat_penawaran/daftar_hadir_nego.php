<!DOCTYPE html>
<HtmL>

<HeAd>
    <MeTa HTTP-EquIV="CONTENT-TYPE" CONTent="text/html; charset=utf-8">
    <TItlE>Semarang, 29 Maret 2004</tITLE>
    <meta name=GENERATOR coNTeNT="LibreOffice 4.1.6.2 (Linux)">
    <Meta nAMe="AUTHOR" CONTeNt="DPU">
    <MEtA NaMe=CREATED cONtENt="Tahun_Anggaran0817;20400000000000">
    <MeTA NAmE="CHANGEDBY" CoNteNt="isan">
    <META NAme="CHANGED" coNTent="20190320;163019000000000">
    <MEtA nAmE=KSOProductBuildVer CONTEnt=1033-10.1.0.6757>
    <sTyLE>
        @page {
            size: 8.47in 13.98in;
            margin-right: 0.88in;
            margin-top: 0.59in;
            margin-bottom: 0.69in
        }
        P {
            margin-bottom: 0.08in;
            direction: ltr;
            color: #000000
        }
        P.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
        H1 {
            margin-left: 3.5in;
            margin-top: 0in;
            margin-bottom: 0in;
            direction: ltr;
            color: #000000;
            text-align: justify;
            text-decoration: underline
        }
        H1.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
    </sTYLE>
</heAd>

<bOdY LANg="en-US" tExT=#000000 dIR="LTR">
    <P cLASS=western ALIGN=JUSTIFY StYlE="margin-bottom: 0in;">
        <foNt SIZE=2 sTyle="font-size: 11pt"><B>DAFTAR HADIR</b></FONt>
    </P>
    <P class=western ALIGN=CENTER stYle="margin-bottom: 0in">
        <bR>
    </p>
    <table cellpadding="3">
        <tr>
            <td>Program</td>
            <td>:</td>
            <td><?php print($kontrak_pekerjaan->program_nama); ?></td>
        </tr>
        <tr>
            <td>Kegiatan</td>
            <td>:</td>
            <td><?php print($kontrak_pekerjaan->pekerjaan_nama); ?></td>
        </tr>
        <tr>
            <td>Pekerjaan</td>
            <td>:</td>
            <td><?php print($kontrak_pekerjaan->aktivitas_nama); ?></td>
        </tr>
        <tr>
            <td>Tanggal</td>
            <td>:</td>
            <td><?php print(tgl_indo($kontrak_surat->tgl_surat)); ?></td>
        </tr>
        <tr>
            <td>Tempat</td>
            <td>:</td>
            <td><?php print($kontrak_surat->tempat_rapat_nama); ?></td>
        </tr>
        <tr>
            <td>Acara</td>
            <td>:</td>
            <td><?php print($kontrak_surat->acara); ?></td>
        </tr>
    </table>

    
    <P Lang="en-US" CLasS="western" ALIGN=JUSTIFY Style="margin-bottom: 0in">
        <FONT SIZE=2 STylE="font-size: 11pt"><B>PEJABAT PENGADAAN</b></foNt>
    </p>
    <P Lang="en-US" CLasS="western" ALIGN=JUSTIFY Style="margin-bottom: 0in">
        <br/>
    </p>
    

    <CeNTer>
        <tablE WIDTH=603 CELLPADDING=7 CELLSPACING=0>
            <COL WIDTH=23>
                <cOL WIDTH=244>
                    <CoL WIDTH=140>
                        <COl WIDTH=139>
                            <tR VALIGN=TOP>
                                <tD align="center" WIDTH=23 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P laNG="en-US" claSS=western ALIGN=CENTER>
                                        <fONt SIZE=2 StYlE="font-size: 11pt">No</Font>
                                    </P>
                                </td>
                                <tD align="center" WIDTH=244 StYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LAnG=en-US claSS="western" ALIGN=CENTER>
                                        <FONT SIZE=2 StYlE="font-size: 11pt">Nama</fonT>
                                    </P>
                                </TD>
                                <tD align="center" WIDTH=140 STylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LaNg="en-US" CLAss="western" ALIGN=CENTER>
                                        <FONt SIZE=2 STYLe="font-size: 11pt">Jabatan</FOnT>
                                    </P>
                                </Td>
                                <tD WIDTH=139 sTylE="border: 1px solid #000000; padding: 0in 0.08in" align="center">
                                    <p laNG="en-US" claSS="western" ALIGN=CENTER>
                                        <FonT SIZE=2 sTYlE="font-size: 11pt">Tanda
				Tangan</foNt>
                                    </p>
                                </tD>
                            </tR>
                            <tr VALIGN=TOP>
                                <TD WIDTH=23 StyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                    <P LaNG=en-US clAsS="western" valign="middle" ALIGN=CENTER sTYLE="margin-bottom: 0in">
                                        <foNt SIZE=2 stYLE="font-size: 11pt">1.</FONT>
                                    </P>
                                    
                                </TD>
                                <td WIDTH=244 valign="middle" StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                    <p LAng="en-US" cLasS="western" ALIGN=JUSTIFY><font SIZE=2 sTYlE="font-size: 11pt"><?php print($kontrak_pekerjaan->pejabat_pengadaan_nama); ?></Font>
                                    </P>
                                    
                                </td>
                                <TD WIDTH=140 valign="middle" StYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                    <p lAnG="en-US" CLAss="western" ALIGN=CENTER>
                                        <FOnt SIZE=2 sTYle="font-size: 11pt">Pejabat Pengadaan <?php print($kontrak_pekerjaan->seri_pejabat_pengadaan_nama); ?></Font>
                                    </P>
                                    
                                </Td>
                                <td WIDTH=139 valign="middle" STyLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p LANg=en-US ClASS="western" ALIGN=JUSTIFY stYLe="margin-bottom: 0in">
                                        <br/>
                                        <br/>
                                        <br/>
                                        <br/>
                                    </p>
                                </TD>
                            </Tr>
        </tAbLe>
    </cEntER>
    <p lANG="en-US" cLASs="western" ALIGN=JUSTIFY StylE="margin-bottom: 0in">
        <Br>
    </P>
    <P lANG=en-US cLaSS=western ALIGN=JUSTIFY sTyLe="margin-bottom: 0in">
        <FONT SIZE=2 StYlE="font-size: 11pt"><b>PIHAK KETIGA</B></FONt>
    </P>
    <p lANG=en-US CLAsS=western ALIGN=JUSTIFY sTylE="margin-bottom: 0in">
        <BR>
    </P>
    <ceNtEr>
        <TABLe WIDTH=625 CELLPADDING=7 CELLSPACING=0>
            <CoL WIDTH=22>
                <col WIDTH=230>
                    <col WIDTH=175>
                        <CoL WIDTH=140>
                            <tr VALIGN=TOP>
                                <td WIDTH=22 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LanG=en-US CLASS="western" ALIGN=CENTER>
                                        <fOnT SIZE=2 stYle="font-size: 11pt">No
				</FOnt>
                                    </P>
                                </td>
                                <Td align="center" WIDTH=230 StyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p LAnG="en-US" ClasS="western" ALIGN=CENTER>
                                        <foNt SIZE=2 sTYle="font-size: 11pt">Nama Perusahaan</Font>
                                    </p>
                                </TD>
                                <TD align="center" WIDTH=175 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P LAnG="en-US" ClAsS=western ALIGN=CENTER>
                                        <FONt SIZE=2 stYlE="font-size: 11pt">Nama Peserta</foNT>
                                    </p>
                                </td>
                                <Td align="center" WIDTH=140 styLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p lanG="en-US" clAss=western ALIGN=CENTER>
                                        <foNT SIZE=2 stYle="font-size: 11pt">Tanda Tangan</Font>
                                    </p>
                                </tD>
                            </tr>
                            <tR VALIGN=TOP>
                                <td valign="middle" WIDTH=22 sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    
                                    <P lANg="en-US" ClAss="western" ALIGN=CENTER sTYLE="margin-bottom: 0in">
                                        <FoNT SIZE=2 STylE="font-size: 11pt">2.</font>
                                    </p>
                                    
                                </tD>
                                <td valign="middle" WIDTH=230 StYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P lanG=en-US CLASS=western ALIGN=JUSTIFY StylE="margin-bottom: 0in">
                                       <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama); ?>
                                    </P>
                                </TD>
                                <tD valign="middle" WIDTH=175 sTyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                
                                    <P lanG=en-US ClaSs=western ALIGN=CENTER StYlE="margin-bottom: 0in"><?php print($direktur_perusahaan); ?></p>

                                </TD>
                                <Td valign="middle" WIDTH=140 sTYlE="border: 1px solid #000000; padding: 0in 0.08in">
                                    <p LANG="en-US" cLASS=western ALIGN=CENTER stylE="margin-bottom: 0in">
                                        <br/>
                                        <br/>
                                        <br/>
                                        <br/>
                                    </P>
                                    
                                    
                                </td>
                            </Tr>
        </TAblE>
    </center>
</BODY>

</HTML>