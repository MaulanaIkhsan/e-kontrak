<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kontrak_surat_penawaran extends CI_Controller {

    /*----- kontrak_surat_penawaran -----*/
    // id
    // kontrak_pekerjaan_id
    // jenis_surat_id
    // kontrak_pihak_ketiga_id
    // tempat_rapat_id
    // tgl_surat
    // no_surat
    // acara
    // lampiran
    // used_lampiran
    // created_at
    // updated_at
    
    // Configuration upload file 
    var $conf_upload = array(
        'upload_path'   => "./assets/uploads/documents_lampiran_surat/",
        'allowed_types' => "pdf",
        'max_size'      => '2000',
        'encrypt_name'  => FALSE
    );

    var $kontrak_surat_penawaran_id,
        $kontrak_pekerjaan_id;

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $sess_data = $this->session->userdata('session_data');
        if($sess_data===NULL) {
            $this->session->set_flashdata('warning', '<div class="alert alert-warning fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a> 
                Harap login terlebih dahulu untuk mengakses sistem</div>');
			redirect('auth');
        }

        $this->load->model('Kontrak_surat_penawaran_model', 'kontrak_surat_penawaran');
        $this->load->model('Generic_model', 'generic');
    }
    
    private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
        $data['status'] = TRUE;
        
        $this->load->model('Jenis_surat_model', 'jenis_surat');
        $this->load->helper('generic');
        
		

        // if($this->input->post('tgl_surat') == '')
		// {
		// 	$data['inputerror'][] = 'tgl_surat';
		// 	$data['error_string'][] = 'Tgl Surat is required';
		// 	$data['status'] = FALSE;
        // }
        
        // if($this->input->post('no_surat') == '')
		// {
		// 	$data['inputerror'][] = 'no_surat';
		// 	$data['error_string'][] = 'Nomor Surat is required';
		// 	$data['status'] = FALSE;
        // }

        // lampiran sebagai konten utama

        // print($this->input->post('use_lampiran'));

        if(!empty($this->input->post('use_lampiran'))) 
        {
            if(empty($_FILES))
            {
                // Re-initialize data
                $data = array();
                $data['error_string'] = array();
                $data['inputerror'] = array();
                
                $data['inputerror'][] = 'lampiran';
                $data['error_string'][] = 'Lampiran Harap berformat PDF*';
                $data['status'] = FALSE;
            }
            elseif($_FILES['lampiran']['name']!='') {
                $nama_dok = $_FILES['lampiran']['name'];
                $split = explode('.', $nama_dok);
                $ekstensi = strtolower($split[1]);

                //'2000000' as same as 2MB
                if($_FILES['lampiran']['size']>2000000 or $_FILES['lampiran']['size']<=0) {
                    $data['inputerror'][] = 'lampiran';
                    $data['error_string'][] = 'Size dokumen melebihi 2MB';
                    $data['status'] = FALSE;
                }
                else if(strpos($this->conf_upload['allowed_types'], $ekstensi)===FALSE) {
                    $data['inputerror'][] = 'lampiran';
                    $data['error_string'][] = 'Format dokumen harus PDF';
                    $data['status'] = FALSE;
                }
            }

            elseif($_FILES['lampiran']['name']!='') {
                $data['inputerror'][] = 'lampiran';
                $data['error_string'][] = 'Lampiran is required';
                $data['status'] = FALSE;
            }
            
        }

        //====== validation for jenis surat
        if($this->input->post('jenis_surat_id') == '')
        {
            $data['inputerror'][] = 'jenis_surat_id';
            $data['error_string'][] = 'Jenis Surat is required';
            $data['status'] = FALSE;
        }
        
        // validation to check inserted jenis_surat
        if($this->input->post('jenis_surat_id') != '') 
        {
            // Validation for spesific kode_surat on jenis_surat
            $jenis_surat_empty  = ['cover', 'LDP'];
            $jenis_surat_tgl    = ['PI', 'DHN'];

            $jenis_surat = $this->jenis_surat->fetch_data($this->input->post('jenis_surat_id'));

            if(!in_array($jenis_surat->kode, $jenis_surat_empty) and !in_array($jenis_surat->kode, $jenis_surat_tgl)) 
            {
                if($this->input->post('tgl_surat') == '')
                {
                    $data['inputerror'][] = 'tgl_surat';
                    $data['error_string'][] = 'Tgl Surat is required';
                    $data['status'] = FALSE;
                }
                
                if($this->input->post('no_surat') == '')
                {
                    $data['inputerror'][] = 'no_surat';
                    $data['error_string'][] = 'Nomor Surat is required';
                    $data['status'] = FALSE;
                }
            }

            if(in_array($jenis_surat->kode, $jenis_surat_tgl) and $this->input->post('tgl_surat') == '')
            {
                $data['inputerror'][] = 'tgl_surat';
                $data['error_string'][] = 'Tgl Surat is required';
                $data['status'] = FALSE;
            }
            // -----

            $jenis_surat_id = $this->input->post('jenis_surat_id');
            
            // Insert data
            if(!empty($this->kontrak_pekerjaan_id))
            {
                $kontrak_surat  = object_to_array($this->kontrak_surat_penawaran->fetch_data(NULL, $this->kontrak_pekerjaan_id));
                $index_value    = index_by_column($kontrak_surat, 'jenis_surat_id', $jenis_surat_id);
                
                $daftar_hadir = 0;
                foreach($kontrak_surat as $item):
                    if($item['jenis_surat_kode']=='DHN') {
                        $daftar_hadir++;
                    }
                endforeach;

                if(!empty($kontrak_surat) and !empty($index_value) and $daftar_hadir>=2) 
                {
                    $data['inputerror'][]   = 'jenis_surat_id';
                    $data['error_string'][] = 'Dokumen pengadaan sudah terinput. Harap cek kembali';
                    $data['status']         = FALSE;
                }
            }
            
            // Update data
            if(!empty($this->kontrak_surat_penawaran_id))
            {
                $kontrak_surat      = $this->kontrak_surat_penawaran->fetch_data($this->kontrak_surat_penawaran_id);
                $all_kontrak_surat  = object_to_array($this->kontrak_surat_penawaran->fetch_data(NULL, $kontrak_surat->kontrak_pekerjaan_id));
                $index_value        = index_by_column($all_kontrak_surat, 'jenis_surat_id', $kontrak_surat->id);

                if(!empty($index_value)) {
                    unset($all_kontrak_surat[$index_value]);
                }

                $daftar_hadir = 0;
                foreach($all_kontrak_surat as $item):
                    if($item['jenis_surat_kode']=='DHN') {
                        $daftar_hadir++;
                    }
                endforeach;

                $jenis_surat = $this->jenis_surat->fetch_data($jenis_surat_id);
                $index_value = index_by_column($all_kontrak_surat, 'jenis_surat_id', $jenis_surat_id);
                
                if($jenis_surat->kode=='DHN' and $daftar_hadir>=2) 
                {
                    $data['inputerror'][]   = 'jenis_surat_id';
                    $data['error_string'][] = 'Dokumen pengadaan sudah terinput. Harap cek kembali';
                    $data['status']         = FALSE;
                }

                if($jenis_surat->kode!='DHN' and !empty($index_value)) 
                {
                    $data['inputerror'][]   = 'jenis_surat_id';
                    $data['error_string'][] = 'Dokumen pengadaan sudah terinput. Harap cek kembali';
                    $data['status']         = FALSE;
                }
            }
        } // close not empty jenis_surat_id			
        
		if($data['status'] === FALSE)
		{
			echo json_encode((object)$data);
			exit();
		}
        
    }

    public function detail($kontrak_surat_penawaran_id)
    {
        $this->load->model('Pihak_ketiga_model', 'pihak_ketiga');
        $this->load->model('Jenis_surat_model', 'jenis_surat');
        $this->load->model('Tempat_rapat_model', 'tempat_rapat');
        $this->load->model('Kontrak_pihak_ketiga_model', 'kontrak_pihak_ketiga');
        $this->load->model('Jadwal_kegiatan_kontrak_model', 'jadwal_kegiatan_kontrak');
        $this->load->model('Jenis_kegiatan_pengadaan_model', 'jenis_kegiatan_pengadaan');
        
        $this->load->helper('generic');

        $data['detail']                 = $this->kontrak_surat_penawaran->fetch_data($kontrak_surat_penawaran_id);
        $data['jenis_surat']            = $this->jenis_surat->fetch_data();
        $data['tempat_rapat']			= $this->tempat_rapat->fetch_data();
		$data['kontrak_pihak_ketiga']	= $this->kontrak_pihak_ketiga->fetch_data(NULL, $data['detail']->kontrak_pekerjaan_id, NULL);
        $data['jenis_kegiatan']         = $this->jenis_kegiatan_pengadaan->fetch_data();

        $this->load->view('kontrak_surat_penawaran/detail', $data);
    }


    public function get_data($kontrak_pekerjaan_id)
	{
        $this->load->model('Kontrak_pekerjaan_model', 'kontrak_pekerjaan');
        $kontrak_pekerjaan = $this->kontrak_pekerjaan->fetch_data($kontrak_pekerjaan_id);

        $sess_data  = $this->session->userdata('session_data');
        $user_id    = $sess_data['id'];
        $user_role  = $sess_data['role'];
        $user_as    = $sess_data['as'];

		$list = $this->kontrak_surat_penawaran->get_datatables($kontrak_pekerjaan_id);

		$data = array();
        $no = $_POST['start'];
        
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->jenis_surat_nama;
            if($field->tgl_surat=='0000-00-00') {
                $row[] = 'Kosong';
            }
            else {
                $row[] = $field->tgl_surat;
            }
            $row[] = $field->no_surat;
            if(strlen($field->lampiran)>0) {
                $row[] = "<a href=".base_url('assets/uploads/documents_lampiran_surat/'.$field->lampiran)." target='_blank'>Lampiran</a>";
            }
            else {
                $row[] = 'Kosong';
            }

            // ($kontrak_pekerjaan->pejabat_pengadaan_pegawai_id==$user_id or $kontrak_pekerjaan->ppkom==$user_id or
            // $user_role=='Super Admin')

            // Access right edit kontrak_surat_penawaran based on PPTK and Pejabat Pengadaan
            $list_jenis_surat = [];
            if($user_as=='pegawai' and $user_role!='Super Admin' and $kontrak_pekerjaan->pejabat_pembuat_komitmen_pegawai_id==$user_id) {
                $list_jenis_surat = ['SPK', 'SPMK', 'SPVendor'];
            }
            elseif($user_as=='pegawai' and $user_role!='Super Admin' and $kontrak_pekerjaan->pejabat_pengadaan_pegawai_id==$user_id) {
                $list_jenis_surat = ['cover', 'UPengadaan', 'PI', 'BABP', 'DHN', 'BAENK', 'BHPL', 'DPPemenang', 'DSampaiP', 'LDP', 'SKHPS', 'SPesanan', 'SPK', 'SPMK', 'SPVendor'];
            }

            if($user_as=='pegawai' and $user_role=='Super Admin') {           
                $row[] = '<a href="'.base_url('kontrak_surat_penawaran/detail/'.$field->id).'" title="Detail" class="btn btn-primary">Detail</a>
                <a href="'.base_url('kontrak_surat_penawaran/generate_surat/'.$field->id).'" title="Print" class="btn btn-success"> Generate</a>';
            }
            elseif($user_as=='pegawai' and $user_role!='Super Admin' and ($kontrak_pekerjaan->pejabat_pembuat_komitmen_pegawai_id==$user_id or $kontrak_pekerjaan->pejabat_pengadaan_pegawai_id==$user_id) and in_array($field->jenis_surat_kode, $list_jenis_surat)) {
                $row[] = '<a href="'.base_url('kontrak_surat_penawaran/detail/'.$field->id).'" title="Detail" class="btn btn-primary">Detail</a>
                <a href="'.base_url('kontrak_surat_penawaran/generate_surat/'.$field->id).'" title="Print" class="btn btn-success"> Generate</a>';
            }
			$row[] = "";

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->kontrak_surat_penawaran->count_all($kontrak_pekerjaan_id),
			"recordsFiltered" => $this->kontrak_surat_penawaran->count_filtered($kontrak_pekerjaan_id),
			"data" => $data,
		);
		// output dalam format JSON
		echo json_encode($output);
	}

    
    public function ajax_add($kontrak_pekerjaan_id)
	{
        $this->kontrak_pekerjaan_id = $kontrak_pekerjaan_id;
        $this->_validate();
        
        $this->load->helper('generic');

        $random_name = rename_file();
        $new_name = $random_name.'.pdf';
        $this->conf_upload['file_name'] = $new_name;
        $this->load->library('upload', $this->conf_upload);
        $this->upload->initialize($this->conf_upload);

		$data = array(
                'kontrak_pekerjaan_id'      => $kontrak_pekerjaan_id,
                'jenis_surat_id'            => $this->input->post('jenis_surat_id'),
                'kontrak_pihak_ketiga_id'   => $this->input->post('kontrak_pihak_ketiga_id'),
                'tempat_rapat_id'           => $this->input->post('tempat_rapat_id'),
                'tgl_surat'                 => $this->input->post('tgl_surat'),
                'no_surat'                  => $this->input->post('no_surat'),
                'acara'                     => $this->input->post('acara'),
                'used_lampiran'             => (!empty($this->input->post('use_lampiran')) ? 'y':'n')
            );

        if($this->input->post('tebusan')!=NULL) {
            $data['tebusan'] = $this->input->post('tebusan');
        }

        // if($this->input->post('tgl_surat')!=NULL) {
        //     $data['tgl_surat'] = NULL;
        // }
        // else {
        //     $data['tgl_surat'] = $this->input->post('tgl_surat');
        // }

        //'file' is name of textbox_upload in view
        $filename = str_replace(' ','', $_FILES['lampiran']['name']);
        if(strlen($filename)>0) {
            $uploading = $this->upload->do_upload("lampiran");
            if($uploading==TRUE){
                $data['lampiran'] = $new_name;
            }
            else {
                echo json_encode(array('status' => FALSE));
                die;
            }
        }

        $this->db->query('set FOREIGN_KEY_CHECKS = 0;');
        $insert = $this->generic->insert('kontrak_surat_penawaran',$data);
        if($insert=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => FALSE));
        }
        $this->db->query('set FOREIGN_KEY_CHECKS = 1;');
    }
    
    public function ajax_edit($id)
	{
		$data = $this->kontrak_surat_penawaran->fetch_data($id);
		echo json_encode($data);
	}

	public function ajax_update()
	{
        $id = $this->input->post('id');
        $this->kontrak_surat_penawaran_id = $id;
        $this->_validate();
        
        $this->load->helper('generic');

        $random_name = rename_file();
        $new_name = $random_name.'.pdf';
        $this->conf_upload['file_name'] = $new_name;
        $this->load->library('upload', $this->conf_upload);
        $this->upload->initialize($this->conf_upload);        

		$data = array(
            'jenis_surat_id'            => $this->input->post('jenis_surat_id'),
            'kontrak_pihak_ketiga_id'   => $this->input->post('kontrak_pihak_ketiga_id'),
            'tempat_rapat_id'           => $this->input->post('tempat_rapat_id'),
            'tgl_surat'                 => $this->input->post('tgl_surat'),
            'no_surat'                  => $this->input->post('no_surat'),
            'acara'                     => $this->input->post('acara'),
            'used_lampiran'             => (!empty($this->input->post('use_lampiran')) ? 'y':'n')
        );
    
        
        //'file' is name of textbox_upload in view
        $filename = str_replace(' ','', $_FILES['lampiran']['name']);

        if(strlen($filename)>0) {
            $uploading = $this->upload->do_upload("lampiran");

            //Deleting old file
            $dokumen        = $this->kontrak_surat_penawaran->fetch_data($id);
            if(!empty($dokumen->lampiran)) {
                $path_dokumen   = $this->conf_upload['upload_path'].$dokumen->lampiran;
                if($path_dokumen!=NULL and file_exists($path_dokumen)){
                    unlink($path_dokumen);
                }
            }

            if($uploading==TRUE){
                $data['lampiran'] = $new_name;
            }
            else {
                echo json_encode(array('status' => 'Error upload new lampiran'));
            }
        }

        $this->db->query('set FOREIGN_KEY_CHECKS = 0;');

        $key    = array('id'=>$id);
        $update = $this->generic->update('kontrak_surat_penawaran', $data, $key);
        
        if($update=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => 'Error edit data'));
        }
        $this->db->query('set FOREIGN_KEY_CHECKS = 1;');
    }

    public function ajax_delete($id)
	{
        $key = array('id'=>$id);

        //Deleting file
        $dokumen = $this->kontrak_surat_penawaran->fetch_data($id);
        $path_dokumen = $this->conf_upload['upload_path'].$dokumen->lampiran;
        if($path_dokumen!=NULL and file_exists($path_dokumen)){
            unlink($path_dokumen);
        }

		$delete = $this->generic->delete('kontrak_surat_penawaran', $key);
        
        if($delete=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => FALSE));
        }
    }
    
    public function generate_surat($kontrak_surat_id)
    {
        ob_start();
        
        $mpdf = new \Mpdf\Mpdf(['format'=>'legal']);
        $mpdf->SetImportUse();

        $this->load->model('Kontrak_pekerjaan_model', 'kontrak_pekerjaan');
        $this->load->model('Kontrak_pihak_ketiga_model', 'kontrak_pihak_ketiga');
        $this->load->model('Pejabat_pengadaan_model', 'pejabat_pengadaan');
        $this->load->model('Pejabat_pembuat_komitmen_model', 'pejabat_pembuat_komitmen');
        
        $this->load->helper('generic');
        
        $kontrak_surat = $this->kontrak_surat_penawaran->fetch_data($kontrak_surat_id);

        $data['kontrak_pekerjaan']          = $this->kontrak_pekerjaan->fetch_data($kontrak_surat->kontrak_pekerjaan_id);
        $data['kontrak_surat']              = $kontrak_surat;
        $data['kontrak_pihak_ketiga']       = $this->kontrak_pihak_ketiga->fetch_data(NULL, $kontrak_surat->kontrak_pekerjaan_id);
        $data['pejabat_pengadaan']          = $this->pejabat_pengadaan->fetch_data($data['kontrak_pekerjaan']->pejabat_pengadaan_id);
        $data['pejabat_pembuat_komitmen']   = $this->pejabat_pembuat_komitmen->fetch_data($data['kontrak_pekerjaan']->pejabat_pembuat_komitmen_id);

        switch ($kontrak_surat->jenis_surat_kode) {
            case 'cover':
                $filename = 'Cover';

                $data['spk'] = $this->kontrak_surat_penawaran->fetch_data(NULL, $kontrak_surat->kontrak_pekerjaan_id, 'SPK');

                $views = $this->load->view('template_surat_penawaran/cover', $data, TRUE);
                break;
            
            case 'UPengadaan':
                $filename = 'Undangan Pengadaan';
                $this->load->model('Jadwal_kegiatan_kontrak_model', 'jadwal_kegiatan_kontrak');
                // $this->load->model('Pejabat_pengadaan_model', 'pejabat_pengadaan');
                
                $data['jadwal_pengadaan'] = $this->jadwal_kegiatan_kontrak->fetch_data(NULL, NULL, $kontrak_surat->id);
                // $data['pejabat_pengadaan']  = $this->pejabat_pengadaan->fetch_data($data['kontrak_pekerjaan']->pejabat_pengadaan_id);
                
                $views = $this->load->view('template_surat_penawaran/undangan_pengadaan', $data, TRUE);
                break;
            
            case 'PI':
                $filename = 'Pakta Integritas';
                $views = $this->load->view('template_surat_penawaran/pakta_integritas', $data, TRUE);
                break;
            
            case 'BABP':
                $filename = 'Berita Acara Buka Penawaran';
                $this->load->model('Karyawan_model', 'karyawan');

                $data['direktur_perusahaan'] = NULL;
                
                $karyawan = $this->karyawan->fetch_data(NULL, $kontrak_surat->pihak_ketiga_id);
                
               
                
                if(!empty($karyawan) and !empty($kontrak_surat->pihak_ketiga_id) ) {                    
                    foreach($karyawan as $item):
                        if($item->jabatan_nama=='Direktur'){
                            $data['direktur_perusahaan'] = $item->nama_lengkap;
                            break;
                        }
                    endforeach;
                }

                $data['surat_undangan'] = $this->kontrak_surat_penawaran->fetch_data(NULL, $data['kontrak_pekerjaan']->id, 'UPengadaan');
                
                $views = $this->load->view('template_surat_penawaran/berita_acara_buka_penawaran', $data, TRUE);
                break;
            
            case 'DHN':
                $this->load->model('Karyawan_model', 'karyawan');
                $filename = 'Daftar Hadir '.$kontrak_surat->acara;
                $data['direktur_perusahaan'] = NULL;
                $karyawan = $this->karyawan->fetch_data(NULL, $kontrak_surat->pihak_ketiga_id);
                if(!empty($karyawan) and !empty($kontrak_surat->pihak_ketiga_id) ) {                    
                    foreach($karyawan as $item):
                        if($item->jabatan_nama=='Direktur'){
                            $data['direktur_perusahaan'] = $item->nama_lengkap;
                            break;
                        }
                    endforeach;
                }
                
                $views = $this->load->view('template_surat_penawaran/daftar_hadir_nego', $data, TRUE);
                break;
            
            case 'BAENK':
                $this->load->model('Karyawan_model', 'karyawan');
                $filename = 'Berita Acara Evaluasi, Negosiasi dan Klarifikasi';

                $data['direktur_perusahaan'] = NULL;
                $karyawan = $this->karyawan->fetch_data(NULL, $kontrak_surat->pihak_ketiga_id);
                if(!empty($karyawan) and !empty($kontrak_surat->pihak_ketiga_id) ) {                    
                    foreach($karyawan as $item):
                        if($item->jabatan_nama=='Direktur'){
                            $data['direktur_perusahaan'] = $item->nama_lengkap;
                            break;
                        }
                    endforeach;
                }
                
                $views = $this->load->view('template_surat_penawaran/berita_acara_negosiasi_klarifikasi', $data, TRUE);
                break;
            
            case 'BHPL':
                $filename = 'Berita Acara Hasil Pengadaan Langsung';

                // $data['surat_negosiasi'] = $this->kontrak_surat_penawaran->fetch_data($kontrak_surat_id, NULL, 'BAENK');
                $data['surat_negosiasi'] = $this->kontrak_surat_penawaran->fetch_data(NULL, $data['kontrak_pekerjaan']->id, 'BAENK');

                $views = $this->load->view('template_surat_penawaran/bahpl', $data, TRUE);
                break;

            case 'DPPemenang':
                $filename = 'Dokumen Penetapan Pemenang';

                $data['bahpl'] = $this->kontrak_surat_penawaran->fetch_data(NULL, $data['kontrak_pekerjaan']->id, 'BHPL');

                $views = $this->load->view('template_surat_penawaran/penetapan_pemenang', $data, TRUE);
                break;
            
            case 'DSampaiP':
                $filename = 'Dokumen Penyampaian Pemenang';
                $views = $this->load->view('template_surat_penawaran/penyampaian_pemenang', $data, TRUE);
                break;

            case 'SPVendor':
                $filename = 'Surat Penunjukan Penyedia';
                $this->load->model('Jadwal_kegiatan_kontrak_model', 'jadwal_kegiatan_kontrak');
                
                $data['surat_penawaran']          = $this->kontrak_surat_penawaran->fetch_data(NULL, $data['kontrak_pekerjaan']->id, 'UPengadaan');
                $surat_penawaran                  = $data['surat_penawaran'][0];
                $data['surat_kegiatan_penawaran'] = $this->jadwal_kegiatan_kontrak->fetch_data(NULL, 'evaluasi', $surat_penawaran->id);

                $views = $this->load->view('template_surat_penawaran/surat_penunjukan_penyedia', $data, TRUE);
                break;
            
            case 'SPK':
                ob_end_flush();
                ob_start();
                $this->load->model('Karyawan_model', 'karyawan');
                $filename = 'Surat Perintah Kerja';

                $data['direktur_perusahaan'] = NULL;
                $karyawan = $this->karyawan->fetch_data(NULL, $kontrak_surat->pihak_ketiga_id);
                if(!empty($karyawan) and !empty($kontrak_surat->pihak_ketiga_id) ) {                    
                    foreach($karyawan as $item):
                        if($item->jabatan_nama=='Direktur'){
                            $data['direktur_perusahaan'] = $item->nama_lengkap;
                            break;
                        }
                    endforeach;
                }

                $data['bahpl']          = $this->kontrak_surat_penawaran->fetch_data(NULL, $data['kontrak_pekerjaan']->id, 'BHPL');
                $data['surat_undangan'] = $this->kontrak_surat_penawaran->fetch_data(NULL, $data['kontrak_pekerjaan']->id, 'UPengadaan');

                // First section
                $views = $this->load->view('template_surat_penawaran/spk_first', $data, TRUE);
                $mpdf->AddPage();
                $mpdf->WriteHTML($views);
                
                // Lampiran SPK section
                $file_location = $this->conf_upload['upload_path'].$kontrak_surat->lampiran;
                if(file_exists($file_location) and !empty($kontrak_surat->lampiran)) {
                    // Get all pages in current file
					$pageInFile = $mpdf->SetSourceFile($file_location);

					for ($flag=1; $flag<=$pageInFile ; $flag++) { 
						$pageId = $mpdf->ImportPage($flag);

						$page = $mpdf->getTemplateSize($pageId);

						if($flag==1) {
							$mpdf->state=0;
							$mpdf->UseTemplate($pageId);
						}
						else {
							$mpdf->state = 1;
							$mpdf->AddPage($page['w']>$page['h']?'L':'P');
							$mpdf->UseTemplate($pageId);
						}
					}
                }
                else {
                    $view = "<h1 style='color: #000; font-family: arial, sans-serif; font-size: 24px; font-weight: bold; margin-top: 0px;
                    margin-bottom: 1px;text-decoration: none; text-align:left; margin-left:0'>Data lampiran perincian harga masih kosong / dokumen laampiran tidak ditemukan</h1>";
                    $mpdf->AddPage();
                    $mpdf->WriteHTML($view);
                }
                
                // Second section
                $views = $this->load->view('template_surat_penawaran/spk_second', $data, TRUE);
                
        
                break;
            
            case 'SPMK':
                $this->load->model('Karyawan_model', 'karyawan');
                $filename = 'Surat Perintah Mulai Kerja';
                
                $data['direktur_perusahaan'] = NULL;
                $karyawan = $this->karyawan->fetch_data(NULL, $kontrak_surat->pihak_ketiga_id);
                if(!empty($karyawan) and !empty($kontrak_surat->pihak_ketiga_id) ) {                    
                    foreach($karyawan as $item):
                        if($item->jabatan_nama=='Direktur'){
                            $data['direktur_perusahaan'] = $item->nama_lengkap;
                            break;
                        }
                    endforeach;
                }

                $data['spk']    = $this->kontrak_surat_penawaran->fetch_data(NULL, $kontrak_surat->kontrak_pekerjaan_id, 'SPK');
                $views = $this->load->view('template_surat_penawaran/spmk', $data, TRUE);
                break;

            case 'LDP':
                $filename = 'Lembar Data Pengadaan';

                $views = $this->load->view('template_surat_penawaran/ldp', $data, TRUE);
                break;
            
            case 'SKHPS':
                $filename = 'Surat Ketetapan HPS';
                $views = $this->load->view('template_surat_penawaran/sk_hps', $data, TRUE);
                break;
            
            case 'SPesanan':
                $this->load->model('Karyawan_model', 'karyawan');
                $filename = 'Surat Pesanan';

                $data['direktur_perusahaan'] = NULL;
                $karyawan = $this->karyawan->fetch_data(NULL, $kontrak_surat->pihak_ketiga_id);
                if(!empty($karyawan) and !empty($kontrak_surat->pihak_ketiga_id) ) {                    
                    foreach($karyawan as $item):
                        if($item->jabatan_nama=='Direktur'){
                            $data['direktur_perusahaan'] = $item->nama_lengkap;
                            break;
                        }
                    endforeach;
                }

                $data['spk']    = $this->kontrak_surat_penawaran->fetch_data(NULL, $kontrak_surat->kontrak_pekerjaan_id, 'SPK');

                $views = $this->load->view('template_surat_penawaran/surat_pesanan', $data, TRUE);
                break;

            // ================================================           


            default:
                $filename = 'Not Found';
                $views = '<h1>Document Code Not Found</h1>';
                break;
        }

        $mpdf->AddPage();
        $mpdf->WriteHTML($views);
        
        // Lampiran kontrak_surat_penawaran
        if($kontrak_surat->jenis_surat_kode!='SPK' and !empty($kontrak_surat->lampiran)) {
            $file_location = $this->conf_upload['upload_path'].$kontrak_surat->lampiran;
            if(file_exists($file_location)) {
                // Get all pages in current file
                //print($file_location);
                $pageInFile = $mpdf->SetSourceFile($file_location);

                for ($flag=1; $flag<=$pageInFile ; $flag++) { 
                    $pageId = $mpdf->ImportPage($flag);

                    $page = $mpdf->getTemplateSize($pageId);

                    if($flag==1) {
                        $mpdf->state=0;
                        $mpdf->UseTemplate($pageId);
                    }
                    else {
                        $mpdf->state = 1;
                        $mpdf->AddPage($page['w']>$page['h']?'L':'P');
                        $mpdf->UseTemplate($pageId);
                    }
                }
            }
        }
        
        ob_clean();
        
        $mpdf->output($filename.'.pdf', "D");
		unset($mpdf);
		
		ob_end_flush(); 
    }

}

/* End of file Kontrak_surat_penawaran.php */
