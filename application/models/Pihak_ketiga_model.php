<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pihak_ketiga_model extends CI_Model {

    //----- pihak_ketiga -----
    // id
	// nama_perusahaan
	// npwp
    // alamat_lengkap
    // no_telepon
    // email
    // status ('Cabang', 'Pusat')
    // alamat_pusat
    // no_telepon_pusat
    // email_pusat
    // created_at
    // updated_at

    var $main_table = 'pihak_ketiga';
    
    //field yang ada di table
    var $column_order = array(null, 'pihak_ketiga.nama_perusahaan', 
                        'pihak_ketiga.alamat_lengkap',
                        'pihak_ketiga.no_telepon',
                        'pihak_ketiga.email',
                        'pihak_ketiga.status'); 
    
    //field yang diizin untuk pencarian 
    var $column_search = array('pihak_ketiga.nama_perusahaan', 
                        'pihak_ketiga.alamat_lengkap',
                        'pihak_ketiga.no_telepon',
                        'pihak_ketiga.email',
                        'pihak_ketiga.status'); 
	
	// default order 
    var $order = array('pihak_ketiga.nama_perusahaan' => 'asc'); 
	
	public function fetch_data($id=NULL)
	{
		$sql = $this->db->select('pihak_ketiga.id as id,
							pihak_ketiga.nama_perusahaan as nama_perusahaan, 
							pihak_ketiga.npwp as npwp,
							pihak_ketiga.alamat_lengkap as alamat_lengkap,
							pihak_ketiga.no_telepon as no_telepon,
							pihak_ketiga.email as email,
							pihak_ketiga.status as status,
							pihak_ketiga.alamat_pusat as alamat_pusat,
							pihak_ketiga.no_telepon_pusat as no_telepon_pusat,
							pihak_ketiga.email_pusat as email_pusat')
						->from($this->main_table)
						->order_by('pihak_ketiga.nama_perusahaan', 'asc');
		
		if($id!=NULL) {
			return $sql->where('pihak_ketiga.id', $id)
						->get()->row();
		}
		else {
			return $sql->get()->result();
		}
	}

    /*====== Serverside Datatable ======*/
	private function _get_datatables_query()
	{
        $this->db->select('pihak_ketiga.id as id,
						pihak_ketiga.nama_perusahaan as nama_perusahaan, 
						pihak_ketiga.npwp as npwp,
                        pihak_ketiga.alamat_lengkap as alamat_perusahaan,
                        pihak_ketiga.no_telepon as no_telepon,
                        pihak_ketiga.email as email,
                        pihak_ketiga.status as status');

		$this->db->from($this->main_table);

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->select('pihak_ketiga.id as id,
						pihak_ketiga.nama_perusahaan as nama_perusahaan,
						pihak_ketiga.npwp as npwp, 
                        pihak_ketiga.alamat_lengkap as alamat_perusahaan,
                        pihak_ketiga.no_telepon as no_telepon,
                        pihak_ketiga.email as email,
                        pihak_ketiga.status as status');

		$this->db->from($this->main_table);

		return $this->db->count_all_results();
	}
	/*====== /Serverside Datatable ======*/

}

/* End of file Pihak_ketiga_model.php */
