<?php
$this->load->view('template/header');?>
<!-- Select2 -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/select2/dist/css/select2.min.css')); ?>">
<?php $this->load->view('template/asset_header');?>
<!-- Star Rating -->
<link rel="stylesheet" href="<?php print(base_url('assets/plugins/rating/dist/starrr.css')); ?>">


<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <!-- Box Form Pengunjung -->
        <?php print($this->session->flashdata('alert')); ?>
        <?php print($this->session->flashdata('success')); ?>
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-primary" id="form-pengunjung">
              <div class="box-header with-border">
                <h3 class="box-title">Input Data Pekerjaan</h3>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <form class="form-horizontal" method="post" action="<?php print(base_url('kunjungan/add'))?>">
                <div class="box-body">
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Kode Pengunjung</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name="kodepengunjung" maxlength="20" value="<?php print($kode_tamu);?>" readonly="readonly">
                      <span class="text-danger col-md-8"><?php echo form_error('KodePengunjung'); ?></span>
                    </div>
                  </div>
                 
                  <div class="form-group">
                    <label for="keperluan" class="col-sm-2 control-label">Keperluan</label>
                    <div class="col-sm-9">
                    <textarea class="form-control" rows="3" placeholder="Cantumkan keperluan anda" maxlength="500" name="keperluan" required="required"></textarea>
                    <span class="text-danger col-md-8"><?php echo form_error('Keperluan'); ?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Tujuan</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="tujuan" id="tujuan" required="required">
                          <option value="">-- Pilih Unit Kerja --</option>
                          <?php foreach ($unit_kerja as $item): ?>
                            <option value="<?php print($item->id);?>"><?php print($item->nama);?></option>
                          <?php endforeach; ?>
                        </select>
                        <span class="text-danger col-md-8"><?php echo form_error('Tujuan'); ?></span>
                    </div>
                  </div>
                  
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <a href="<?php print(base_url('kunjungan')); ?>" class="btn btn-primary">Kembali</a> 
                  <button type="submit" name="save" class="btn btn-success" value="simpan">Simpan</button>
                  <a href="#" class="btn btn-primary pull-right btn-form-feedback">Form Feedback</a>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
          </div>
          <!-- /Box Form Pengunjung -->

          <!-- Box Form Response -->
          <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-primary" id="form-feedback">
              <div class="box-header with-border">
                <h3 class="box-title">Formulir Feedback</h3>
              </div>
              <form class="form-horizontal" method="post" action="<?php print(base_url('pengunjung/feedback'))?>">
                <div class="box-body">
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Kode Pengunjung</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name="kodepengunjung" maxlength="20" required="required" />
                      <span class="text-danger col-md-8"><?php echo form_error('KodePengunjung'); ?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Penilaian</label>
                    <div class="col-sm-9">
                      <div class='starrr' id='star'></div>
                      <br />
                      <input type='hidden' name='rating' id='star_input' />
                      <span class="text-danger col-md-8"><?php echo form_error('Rating'); ?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Kritik dan Saran</label>
                    <div class="col-sm-9">
                     <textarea rows="3" class="form-control" placeholder="Masukan untuk DPU Kota Semarang" maxlength="500" name="masukan" required="required" maxlength="500"></textarea>
                      <span class="text-danger col-md-8"><?php echo form_error('Masukan'); ?></span>
                    </div>
                  </div>

                </div>
                <!-- /Div Box body -->
                <div class="box-footer">
                  <button type="submit" name="save" class="btn btn-success" value="simpan">Simpan</button>
                  <a href="#" class="btn btn-primary pull-right btn-form-pengunjung">Form Kunjungan</a>
                </div>
              </form>
              
            </div>
          </div>
           <!-- /Box Form Response -->

           <!-- Box Form New Pengunjung & Kunjungan -->
           <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-primary" id="form-new-pengunjung">
              <div class="box-header with-border">
                <h3 class="box-title">Formulir Pengunjung Baru</h3>
              </div>
              <form class="form-horizontal" method="post" action="<?php print(base_url('kunjungan/new_pengunjung')) ?>">
                <div class="box-body">
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Kode Pengunjung</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name="kodepengunjung" maxlength="20" value="<?php print($kode_tamu);?>" readonly="readonly">
                      <span class="text-danger col-md-8"><?php echo form_error('KodePengunjung'); ?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="nik" class="col-sm-2 control-label">NIK</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name="nik" placeholder="Input NIK" maxlength="15" required="required" id="nik" />
                      <span class="text-danger col-md-8"><?php echo form_error('NIK'); ?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Nama Lengkap</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" name="namalengkap" placeholder="Input nama lengkap" maxlength="24" required="required">
                      <span class="text-danger col-md-8"><?php echo form_error('NamaLengkap'); ?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="keperluan" class="col-sm-2 control-label">Alamat</label>
                    <div class="col-sm-9">
                    <textarea class="form-control" rows="3" placeholder="Alamat Anda" maxlength="500" name="alamat" required="required"></textarea>
                    <span class="text-danger col-md-8"><?php echo form_error('Alamat'); ?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Jenis Kelamin</label>
                    <div class="col-sm-9">
                      <input type="radio" name="jk" value="Pria" class="minimal" required="required" /> Pria<br/>
                      <input type="radio" name="jk" value="Wanita" class="minimal" required="required" /> Wanita<br/>
                      <span class="text-danger col-md-8"><?php echo form_error('jk'); ?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Asal</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" placeholder="Asal instansi/perusahaan" maxlength="15" required="required" name="asal">
                      <span class="text-danger col-md-8"><?php echo form_error('Asal'); ?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Nomor Handphone</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" placeholder="Nomor handphone anda" maxlength="15" required="required" name="nohp" id="nohp" />
                      <span class="text-danger col-md-8"><?php echo form_error('Nohp'); ?></span>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="keperluan" class="col-sm-2 control-label">Keperluan</label>
                    <div class="col-sm-9">
                    <textarea class="form-control" rows="3" placeholder="Cantumkan keperluan anda" maxlength="500" name="keperluan" required="required"></textarea>
                    <span class="text-danger col-md-8"><?php echo form_error('Keperluan'); ?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Tujuan</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="tujuan" id="tujuan" required="required">
                          <option value="">-- Pilih Unit Kerja --</option>
                          <?php foreach ($unit_kerja as $item): ?>
                            <option value="<?php print($item->id);?>"><?php print($item->nama);?></option>
                          <?php endforeach; ?>
                        </select>
                        <span class="text-danger col-md-8"><?php echo form_error('Tujuan'); ?></span>
                    </div>
                  </div>

                </div>
                <div class="box-footer">
                  <button type="submit" name="save" class="btn btn-success pull-left" value="simpan">Simpan</button>
                  <div class="pull-right">
                    <a href="#" class="btn btn-primary btn-form-pengunjung">Form Kunjungan</a> | 
                    <a href="#" class="btn btn-primary btn-form-feedback">Form Feedback</a>
                  </div>
                </div>
              </form>
              
            </div>
          </div>
           <!-- /Box Form New Pengunjung & Kunjungan -->

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<!-- Select2 -->
<script src="<?php print(base_url('assets/bower_components/select2/dist/js/select2.full.min.js'));?>"></script>
<!-- Star Rating -->
<script type="text/javascript" src="<?php print(base_url('assets/plugins/rating/dist/starrr.js')); ?>"></script>
<script>
  $(function(){
    //Initialize Select2 Elements
     $('#tujuan, #namalengkap').select2();

    var $s2input = $('#star_input');
    $('#star').starrr({
      max: 10,
      rating: $s2input.val(),
      change: function(e, value){
        $s2input.val(value).trigger('input');
      }
    });

    $('#form-feedback').hide();
    $('#form-new-pengunjung').hide();

    $('.btn-form-feedback').click(function(){
      $('#form-feedback').show();
      $('#form-pengunjung').hide();
      $('#form-new-pengunjung').hide();
    });

    $('.btn-form-pengunjung').click(function(){
      $('#form-pengunjung').show();
      $('#form-feedback').hide();
      $('#form-new-pengunjung').hide();
    });

    $('.btn-new-pengunjung').click(function(){
      $('#form-feedback').hide();
      $('#form-pengunjung').hide();
      $('#form-new-pengunjung').show();
    });

    $('#nohp, #nik').bind('keyup paste', function(){
      this.value = this.value.replace(/[^0-9]/g, '');
    });

    $('input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    });
  });
</script>
<?php $this->load->view('template/footer');?>