<!DOCTYPE html>
<HTmL>

<HEaD>
    <MetA HTTP-EqUiv=CONTENT-TYPE COnteNt="text/html; charset=utf-8">
    <tiTLe>Semarang, 29 Maret 2004</TITlE>
    <mETa Name="GENERATOR" cONteNT="LibreOffice 4.1.6.2 (Linux)">
    <MeTA nAmE=AUTHOR cOntENT=DPU>
    <meta nAME=CREATED ContEnT=Tahun_Anggaran0817;20400000000000>
    <MEta nAMe=CHANGEDBY CONteNT="isan">
    <mETa nAMe=CHANGED coNtENT="20190320;163019000000000">
    <MetA NAME=KSOProductBuildVer ConteNT=1033-10.1.0.6757>
    <sTYLE>
        @page {
            size: 8.47in 13.98in;
            margin-right: 0.88in;
            margin-top: 0.59in;
            margin-bottom: 0.69in
        }
        P {
            margin-bottom: 0.08in;
            direction: ltr;
            color: #000000
        }
        P.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
        H1 {
            margin-left: 3.5in;
            margin-top: 0in;
            margin-bottom: 0in;
            direction: ltr;
            color: #000000;
            text-align: justify;
            text-decoration: underline
        }
        H1.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
    </stylE>
</HeaD>

<BOdY lANG=en-US tEXT="#000000" Dir=LTR>
    <p CLasS=western ALIGN=CENTER styLe="margin-bottom: 0in">
       <B>PAKTA INTEGRITAS</B>
    </P>
    <p CLASS="western" ALIGN=CENTER STYlE="margin-bottom: 0in">
    
    </p>
    <p claSS="western" ALIGN=JUSTIFY STylE="margin-bottom: 0in">
        Saya yang bertanda tangan dibawah ini, dalam rangka Pengadaan Barang/Jasa
        <?php print(ucwords(strtolower($kontrak_pekerjaan->program_nama)));?> <?php print(ucwords(strtolower($kontrak_pekerjaan->pekerjaan_nama)));?> 
        Pekerjaan <?php print(ucwords(strtolower($kontrak_pekerjaan->aktivitas_nama)));?>, 
        pada Dinas Pekerjaan Umum Kota Semarang, dengan ini menyatakan bahwa :
    </P>
    <p cLaSS=western ALIGN=JUSTIFY stYLE="margin-bottom: 0in">
    </P>
    
    <Ol>
        <lI>Tidak akan melakukanpraktek KKN.</li>             
        <LI>
        Akan melaporkan kepada
        APIP (Aparat Pengawas Intern Pemerintah) yang bersangkutan dan atau
        LKPP apabila mengetahui ada indikasi KKN di dalam proses pengadaan
        ini.</li>
        <Li>                
        Akan mengikuti proses
        pengadaan secara bersih, transparan, dan professional untuk
        memberikan hasil kerja terbaik sesuai ketentuan peraturan
        perundangan-undangan.</li>
        <Li>
        Apabila melanggar
        hal-hal yang telah saya nyatakan dalam PAKTA INTEGRITAS ini, saya
        bersedia menerima sanksi administratif, menerima saknsi pencantuman
        dalam Daftar Hitam, digugat secara perdata dan / atau dilaporkan
        secara pidana.</li>         
    </OL>

    <p ClaSs="western" ALIGN=JUSTIFY STylE="margin-bottom: 0in">
        <br>
    </p>
    <P ClaSS=western ALIGN=RIGHT StylE="margin-bottom: 0in">
        Semarang, <?php print(tgl_indo($kontrak_surat->tgl_surat)); ?>
    </p>
    <P clAsS=western ALIGN=RIGHT StYlE="margin-bottom: 0in">
        <br>
    </P>
    <p CLASS=western ALIGN=RIGHT stYlE="margin-bottom: 0in">
        <BR>
    </p>
    <table cellspacing="2" cellpadding="10">
        <tr>
            <td valign="top">1</td>
            <td valign="top">
                A.n. Kepala Dinas Pekerjaan Umum<br/>
                Pejabat Pembuat Komitmen
            </td>
            <td valign="top">.........................................</td>
            <td valign="top"><?php print((!empty($pejabat_pembuat_komitmen->pegawai_nama))?$pejabat_pembuat_komitmen->pegawai_nama:''); ?></td>
        <tr>
        <tr>
            <td valign="top">2</td>
            <td valign="top">
            Pejabat Pengadaan <?php print($kontrak_pekerjaan->seri_pejabat_pengadaan_nama); ?><br/>
            <?php print($kontrak_pekerjaan->jenis_pejabat_pengadaan_nama); ?>
            </td>
            <td valign="top">.........................................</td>
            <td valign="top">
                <?php print($kontrak_pekerjaan->pejabat_pengadaan_nama); ?>
            </td>
        <tr>
    </table>
    

   
</Body>

</htML>