<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php print(base_url('assets/customize/css/dompdf-a3-lanscape.css'));?>">
</head>
<body>
	<header>
		<table class="header">
			<tr>
				<td rowspan="3">
					<img src="<?php print(APPPATH.'../assets/customize/img/logo_pemkot_smg.jpg') ?>" height="100" width="100" style="float: right;" />
				</td>
				<td>
					<h3>PEMERINTAH KOTA SEMARANG</h3>
					<h1>DINAS PEKERJAAN UMUM</h1>
					Jl. Madukoro Raya No.7, Krobokan, Semarang Barat, Kota Semarang, Jawa Tengah 50141
				</td>				
			</tr>
		</table>
		<hr />
	</header>
	<p></p>
	<main>
		<div class="title-report">
			<center><h2>RESUME DATA PEKERJAAN</h2></center>
		</div>
		<h2>Data Pekerjaan</h2>
		<table>
			<tr>
                <td><strong>Nama Program</strong></td>
                <td>:</td>
                <td><?php print($detail->nama_program); ?></td>
            </tr>
            <tr>
                <td><strong>Tahun</strong></td>
                <td>:</td>
                <td><?php print($detail->program_tahun); ?></td>
            </tr>
            <tr>
                <td><strong>Nama Pekerjaan</strong></td>
                <td>:</td>
                <td><?php print($detail->nama_pekerjaan); ?></td>
            </tr>
            <tr>
                <td><strong>Tahun</strong></td>
                <td>:</td>
                <td><?php print($detail->pekerjaan_tahun); ?></td>
            </tr>
			<tr>
                <td><strong>Nama Pekerjaan</strong></td>
                <td>:</td>
                <td><?php print($detail->nama_aktivitas); ?></td>
            </tr>
            <tr>
                <td><strong>PPTK</strong></td>
                <td>:</td>
                <td><?php print($detail->pptk); ?></td>
            </tr>
            <tr>
                <td><strong>Metode Pengadaan</strong></td>
                <td>:</td>
                <td><?php print($detail->jenis_pengadaan); ?></td>
            </tr>
            <tr>
                <td><strong>Jenis Pekerjaan</strong></td>
                <td>:</td>
                <td><?php print($detail->jenis_pekerjaan); ?></td>
            </tr>
             <tr>
                <td><strong>Anggaran Awal</strong></td>
                <td>:</td>
                <td><?php print('Rp '.number_format($detail->anggaran_awal, 0, '.','.').',-'); ?></td>
            </tr>
            <tr>
                <td><strong>Perubahan Anggaran</strong></td>
                <td>:</td>
                <td><?php print('Rp '.number_format($detail->perubahan_anggaran, 0, '.','.').',-'); ?></td>
            </tr>
            <tr>
                <td><strong>Tgl Pengajuan Pencairan</strong></td>
                <td>:</td>
                <td><?php print($detail->tgl_pengajuan_pencairan); ?></td>
            </tr>
            <tr>
                <td><strong>Tgl Pencairan</strong></td>
                <td>:</td>
                <td><?php print($detail->tgl_pencairan); ?></td>
            </tr>
            <tr>
                <td><strong>Nilai Kontrak</strong></td>
                <td>:</td>
                <td><?php print('Rp '.number_format($detail->nilai_kontrak, 0, '.','.').',-'); ?></td>
            </tr>

            <tr>
                <td><strong>HPS</strong></td>
                <td>:</td>
                <td><?php print('Rp '.number_format($detail->hps, 0, '.','.').',-'); ?></td>
            </tr>
            <tr>
                <td><strong>Serapan</strong></td>
                <td>:</td>
                <td><?php print('Rp '.number_format($detail->serapan, 0, '.','.').',-'); ?></td>
            </tr>
            <tr>
                <td><strong>Keterangan</strong></td>
                <td>:</td>
                <td><?php print($detail->keterangan); ?></td>
            </tr>
		</table>
		<h2>Data Pihak Ketiga dan Progress Pekerjaan</h2>

		<?php 
		$no=1;
		if(count($kontrak)>0) { 
			foreach($kontrak as $item): ?>
			<h3>Data Pihak Ketiga</h3>
			<table>
				<tr>
                    <td><strong>Nomor</strong></td>
                    <td>:</td>
                    <td><?php print($no); ?></td>
                </tr>
				<tr>
                    <td><strong>Pihak Ketiga</strong></td>
                    <td>:</td>
                    <td><?php print($item->nama_vendor); ?></td>
                </tr>
                <tr>
                    <td><strong>Nomor Kontrak</strong></td>
                    <td>:</td>
                    <td><?php print($item->no_kontrak); ?></td>
                </tr>
                <tr>
                    <td><strong>Penawaran Harga</strong></td>
                    <td>:</td>
                    <td><?php print('Rp '.number_format($item->hps, 0, '.',',').',-'); ?></td>
                </tr>
                 <tr>
                    <td><strong>Tanggal Awal</strong></td>
                    <td>:</td>
                    <td><?php print(format_date($item->awal_kontrak, 2)); ?></td>
                </tr>
                <tr>
                    <td><strong>Tanggal Akhir</strong></td>
                    <td>:</td>
                    <td><?php print(format_date($item->akhir_kontrak, 2)); ?></td>
                </tr>
                <tr>
                    <td><strong>Status</strong></td>
                    <td>:</td>
                    <td><?php print($item->status); ?></td>
                </tr>
                <tr>
                    <td><strong>Keterangan</strong></td>
                    <td>:</td>
                    <td><?php print($item->keterangan); ?></td>
                </tr>
            </table>

            <h3>Data Progress Pekerjaan</h3>
            <?php
            	$flag_progress = 1; 
            	$progress = $controller->get_realisasi($item->kontrak_id);
            	$jml = count($progress);
            	if($jml>0){
            		foreach ($progress as $value): ?>
	            <table>
	            	<tr>
	            		<td><strong>No</strong></td>
	                    <td>:</td>
	                    <td><?php print($no.'.'.$flag_progress); ?></td>
	            	</tr>
	                <tr>
	                    <td><strong>Tanggal</strong></td>
	                    <td>:</td>
	                    <td><?php print(format_date($value->tanggal, 2)); ?></td>
	                </tr>
	                <tr>
	                    <td><strong>Serapan Anggaran</strong></td>
	                    <td>:</td>
	                    <td><?php print('Rp '.number_format($value->harga,0,'.',',').',-'); ?></td>
	                </tr>
	                
	                
	                <tr>
	                    <td><strong>Rencana Fisik</strong></td>
	                    <td>:</td>
	                    <td><?php print($value->rencana_fisik.'%'); ?></td>
	                </tr>
	                <tr>
	                    <td><strong>Realisasi Fisik</strong></td>
	                    <td>:</td>
	                    <td><?php print($value->realisasi_fisik.'%'); ?></td>
	                </tr>
	                <tr>
	                	<td><strong>Deviasi</strong></td>
	                    <td>:</td>
	                    <td><?php print($value->deviasi.'%'); ?></td>
	                </tr>
	                <tr>
	                    <td><strong>Status Teknis</strong></td>
	                    <td>:</td>
	                    <td><?php print($value->status_teknis); ?></td>
	                </tr>
	                <tr>
	                    <td><strong>Status Pencairan</strong></td>
	                    <td>:</td>
	                    <td><?php print($value->status_pencairan); ?></td>
	                </tr>
	                <tr>
	                    <td><strong>Keterangan</strong></td>
	                    <td>:</td>
	                    <td><?php print($value->keterangan); ?></td>
	                </tr>
	                <?php if($value->foto1!=NULL) { ?>
	                	<tr>
	                    	<td>Foto 1</td>
	                    	<td>:</td>
	                    	<td>
	                    		<?php 
	                    		$file = './assets/foto_realisasi/'.$value->foto1; 
	                    		if(file_exists($file)) { ?>
	                    			<img class="example-image" src="<?php print($file);?>" alt="" height="300" width="500" alt="Tidak ada gambar" />
	                    		<?php } else { print('Gambar tidak ditemukan'); }?>
	                    	</td>
	                    </tr>
	                <?php } ?>
	                
	                <?php if($value->foto2!=NULL) { ?>
	                	<tr>
	                    	<td>Foto 2</td>
	                    	<td>:</td>
	                    	<td>
	                    		<?php 
	                    		$file = './assets/foto_realisasi/'.$value->foto2; 
	                    		if(file_exists($file)) { ?>
	                    			<img class="example-image" src="<?php print($file);?>" alt="" height="350" width="500" alt="Tidak ada gambar" />
	                    		<?php } else { print('Gambar tidak ditemukan'); }?>
	                    	</td>
	                    </tr>
	                <?php } ?>
	                <?php if($value->foto3!=NULL) { ?>
	                	<tr>
	                    	<td>Foto 3</td>
	                    	<td>:</td>
	                    	<td>
	                    		<?php 
	                    		$file = './assets/foto_realisasi/'.$value->foto3; 
	                    		if(file_exists($file)) { ?>
	                    			<img class="example-image" src="<?php print($file);?>" alt="" height="350" width="500" alt="Tidak ada gambar" />
	                    		<?php } else { print('Gambar tidak ditemukan'); }?>
	                    	</td>
	                    </tr>
	                <?php } ?>
	                <?php if($value->foto4!=NULL) { ?>
	                	<tr>
	                    	<td>Foto 4</td>
	                    	<td>:</td>
	                    	<td>
	                    		<?php 
	                    		$file = './assets/foto_realisasi/'.$value->foto4; 
	                    		if(file_exists($file)) { ?>
	                    			<img class="example-image" src="<?php print($file);?>" alt="" height="350" width="500" alt="Tidak ada gambar" />
	                    		<?php } else { print('Gambar tidak ditemukan'); }?>
	                    	</td>
	                    </tr>
	                <?php } ?>
	                <?php if($value->foto5!=NULL) { ?>
	                	<tr>
	                    	<td>Foto 5</td>
	                    	<td>:</td>
	                    	<td>
	                    		<?php 
	                    		$file = './assets/foto_realisasi/'.$value->foto5; 
	                    		if(file_exists($file)) { ?>
	                    			<img class="example-image" src="<?php print($file);?>" alt="" height="350" width="500" alt="Tidak ada gambar" />
	                    		<?php } else { print('Gambar tidak ditemukan'); }?>
	                    	</td>
	                    </tr>
	                <?php } ?>  
	            </table>
	            <?php if($flag_progress<$jml) { ?>
	            	<hr style="border: 1px dashed black;" />
             	<?php } $flag_progress++; endforeach; } ?> <!-- End looping data progress -->
            <hr />
		<?php $no++; endforeach; } ?>
		

	</main>
</body>
</html>
