<!DOCTYPE html>
<HtmL>

<HeAd>
    <MeTa HTTP-EquIV="CONTENT-TYPE" CONTent="text/html; charset=utf-8">
    <TItlE><?php print($filename); ?></tITLE>
    <meta name=GENERATOR coNTeNT="LibreOffice 4.1.6.2 (Linux)">
    <Meta nAMe="AUTHOR" CONTeNt="DPU">
    <MEtA NaMe=CREATED cONtENt="Tahun_Anggaran0817;20400000000000">
    <MeTA NAmE="CHANGEDBY" CoNteNt="isan">
    <META NAme="CHANGED" coNTent="20190320;163019000000000">
    <MEtA nAmE=KSOProductBuildVer CONTEnt=1033-10.1.0.6757>
    <sTyLE>
        @page {
            size: 8.47in 13.98in;
            margin-right: 0.88in;
            margin-top: 0.59in;
            margin-bottom: 0.69in
        }
        P {
            margin-bottom: 0.08in;
            direction: ltr;
            color: #000000
        }
        P.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
        H1 {
            margin-left: 3.5in;
            margin-top: 0in;
            margin-bottom: 0in;
            direction: ltr;
            color: #000000;
            text-align: justify;
            text-decoration: underline
        }
        H1.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
    </sTYLE>
</heAd>

<bOdY LANg="en-US" tExT=#000000 dIR="LTR">

    <!-- Surat Perintah Kerja Second -->
    <p></p>
    <table CELLPADDING="7" cellspacing="0">
        <tr>
            <td valign="center" align="justify" colspan="2" width="400" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
            <strong>INSTRUKSI KEPADA PENYEDIA :</strong> Penagihan hanya dapat dilakukan setelah penyelesaian pekerjaan yang diperintahkan dalam SPK ini dan dibuktikan dengan Berita Acara Serah Terima. Jika pekerjaan tidak dapat diselesaikan dalam jangka waktu pelaksanaan pekerjaan karena kesalahan atau kelalaian Penyedia maka Penyedia berkewajiban untuk membayar denda kepada PPK sebesar 1/1000 (satu per seribu) dari bagian tertentu nilai SPK sebelum PPN setiap hari kalender keterlambatan.
            </td>
        </tr>
        <tr>
            <td align="center" valign="middle" width="500" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                A.n. Kepala Dinas Pekerjaan Umum<br/>
                Kota Semarang<br/>
                Kuasa Pengguna Anggaran<br/>
                Selaku Pejabat Pembuat Komitmen<br/>
                Tahun Anggaran <?php print(date("Y"));?><br/>
                <br/><br/><br/><br/><br/>
                <u><?php print((!empty($pejabat_pembuat_komitmen->pegawai_nama))?$pejabat_pembuat_komitmen->pegawai_nama:''); ?></u><br/>
                NIP. <?php print((!empty($pejabat_pembuat_komitmen->pegawai_nip))?$pejabat_pembuat_komitmen->pegawai_nip:''); ?>
            </td>
            <td align="center" width="500" valign="middle" STYLe="padding-left:500px; border-top:1px solid #000; border-bottom:1px solid #000; border-left:1px solid #000; border-right:1px solid #000; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                Penyedia Barang/Jasa <?php print(ucwords(strtolower($kontrak_pekerjaan->aktivitas_nama)));?><br/>
                <br/><br/><br/><br/><br/><br/><br/><br/><br/>
                <u><?php print($direktur_perusahaan);?></u><br/>
                Direktur
            </td>
        </tr>
        
    </table>
    <p></p>
    <p></p>
    <!-- /Surat Perintah Kerja Second -->    
    
    <p style="page-break-before: always"></p>

    <!-- Surat Perinntah Mulai Kerja -->    
    <?php
    $kontrak_surat  = $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'SPMK');
    $kontrak_surat  = $kontrak_surat[0];
    $spk            =  $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'SPK');
    ?>

<p>
        <Img SRc="<?php print(base_url('assets/template_surat/img/kop_surat_dpu_monokrom.png'));?>" nAme="Picture 8" width="100%" ALIGN=center BORDER=0>
    </p>
    <p clAsS="western" ALIGN="center" StyLE="margin-bottom: 0in">
        <u><strong>SURAT PERINTAH MULAI KERJA (SPMK)</strong></u><br/>
        Nomor : <?php print($kontrak_surat->no_surat); ?>
    </p>
    
    <p></p>
    
    <p clAsS="western" ALIGN="justify" StyLE="margin-bottom: 0in">
    Berdasarkan Surat Perintah Kerja Nomor <?php print((!empty($spk[0]->no_surat))?$spk[0]->no_surat:''); ?>, Tanggal 
    <?php
    if(!empty($spk[0]->tgl_surat)) {
        print(tgl_indo($spk[0]->tgl_surat));
    }
    else {
        print('');
    }
    ?>, dengan ini Kuasa Pengguna Anggaran Selaku Pejabat Pembuat Komitmen Judul_Program Kegiatan Judul_Kegiatan , memerintahkan kepada
    </p>
    <p></p>
    <table cellpadding="2">
        <tr>
            <td class="text" style="font-size:12pt">Nama</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($direktur_perusahaan);?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Jabatan</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt">Direktur <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Alamat</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_alamat); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt" valign="top">NPWP</td>
            <td class="text" style="font-size:12pt" valign="top">:</td>
            <td class="text" style="font-size:12pt">
                <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_npwp); ?>
            </td>
        </tr>
    </table>
    <p></p>
    <p clAsS="western" ALIGN="justify" StyLE="margin-bottom: 0in">
        Untuk melaksanakan :
    </p>
    <p></p>
    <table cellpadding="2">
        <tr>
            <td class="text" valign="top" style="font-size:12pt">Program</td>
            <td class="text" valign="top" style="font-size:12pt">:</td>
            <td class="text" valign="top" style="font-size:12pt"><?php print($kontrak_pekerjaan->program_nama); ?></td>
        </tr>
        <tr>
            <td class="text" valign="top" style="font-size:12pt">Kegiatan</td>
            <td class="text" valign="top" style="font-size:12pt">:</td>
            <td class="text" valign="top" style="font-size:12pt"><?php print($kontrak_pekerjaan->pekerjaan_nama); ?></td>
        </tr>
        <tr>
            <td class="text" valign="top" style="font-size:12pt">Pekerjaan</td>
            <td class="text" valign="top" style="font-size:12pt">:</td>
            <td class="text" valign="top" style="font-size:12pt"><?php print($kontrak_pekerjaan->aktivitas_nama); ?></td>
        </tr>
        <tr>
            <td class="text" valign="top" style="font-size:12pt">No. Rekening Belanja</td>
            <td class="text" valign="top" style="font-size:12pt">:</td>
            <td class="text" valign="top" style="font-size:12pt"><?php print($kontrak_pekerjaan->aktivitas_no_rekening); ?></td>
        </tr>
        <tr>
            <td class="text" valign="top" style="font-size:12pt">Sumber Dana</td>
            <td class="text" valign="top" style="font-size:12pt">:</td>
            <td class="text" valign="top" style="font-size:12pt"><?php print($kontrak_pekerjaan->aktivitas_sumber_dana); ?></td>
        </tr>
        <tr>
            <td class="text" valign="top" style="font-size:12pt">Harga Borongan</td>
            <td class="text" valign="top" style="font-size:12pt">:</td>
            <td class="text" valign="top" style="font-size:12pt">
                <?php print('Rp '.format_money($kontrak_pekerjaan->harga_negosiasi).',-'); ?><br/>
                <?php print(ucwords(strtolower(terbilang($kontrak_pekerjaan->harga_negosiasi))).' Rupiah'); ?>
            </td>
        </tr>
        <tr>
            <td class="text" valign="top" style="font-size:12pt">Waktu Pelaksanaan</td>
            <td class="text" valign="top" style="font-size:12pt">:</td>
            <td class="text" valign="top" style="font-size:12pt">
                Selama <?php print($kontrak_pekerjaan->durasi_kontrak); ?> 
                (<?php print(terbilang($kontrak_pekerjaan->durasi_kontrak)); ?>) hari kalender<br/>
                <ul>
                    <li>Mulai pelaksanaan tanggal <?php print(tgl_indo($kontrak_pekerjaan->tgl_awal_kontrak)); ?></li>
                    <li>Selesai Pelaksanaan tanggal <?php print(tgl_indo($kontrak_pekerjaan->tgl_akhir_kontrak)); ?></li>
                </ul>
            </td>
        </tr>
    </table>
    <p></p>
    <p clAsS="western" ALIGN="justify" StyLE="margin-bottom: 0in">
        Demikian Surat Perintah Mulai Kerja (SPMK) ini disampaikan untuk dilaksanakan sebaik – baiknya dan penuh tanggung jawab.
    </p>
    <p></p>
    <p></p>
    <table>
        <tr>
            <td width="300" align="center" style="font-size:16pt">
                Setuju:<br/>
                <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama);?><br/>
                <br/><br/><br/><br/><br/><br/><br/><br/><br/>
                <u><?php print($direktur_perusahaan);?></u><br/>
                Direktur
            </td>
            <td width="300"></td>
            <td width="400" align="center" style="font-size:16pt">
                A.n. Kepala Dinas Pekerjaan Umum<br/>
                Kota Semarang<br/>
                Kuasa Pengguna Anggaran<br/>
                Selaku Pejabat Pembuat Komitmen<br/>
                Tahun Anggaran <?php print((!empty($pejabat_pembuat_komitmen->tahun))?$pejabat_pembuat_komitmen->tahun:''); ?><br/>
                <br/><br/><br/><br/><br/>
                <u><?php print((!empty($pejabat_pembuat_komitmen->pegawai_nama))?$pejabat_pembuat_komitmen->pegawai_nama:''); ?></u><br/>
                NIP. <?php print((!empty($pejabat_pembuat_komitmen->pegawai_nip))?$pejabat_pembuat_komitmen->pegawai_nip:''); ?>
            </td>
        </tr>
    </table>
    <!-- /Surat Perinntah Mulai Kerja -->
    
    <p style="page-break-before: always"></p>
    
    <!-- Lembar Data Pengadaan -->
    <?php
    $kontrak_surat  = $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'LDP');
    $kontrak_surat  = $kontrak_surat[0];
    ?>

<p cLaSs="western" ALIGN=CENTER StYLE="margin-bottom: 0in; widows: 0; orphans: 0">
        <FOnt faCE="Footlight MT Light, FreeSerif, serif"><FoNT SIZE=4>LEMBAR
DATA PENGADAAN</fONt>
        </foNt>
    </p>
    <p clAss="western" ALIGN=CENTER STyLe="margin-bottom: 0in; widows: 0; orphans: 0">
        <br>
    </P>
    <P CLASs="western" ALIGN=CENTER StYle="margin-bottom: 0in; widows: 0; orphans: 0">
        <Br>
    </p>
    <TaBle WIDTH=696 CELLPADDING=7 CELLSPACING=0>

                <Tr VALIGN=TOP>
                    <TD WIDTH=249 sTyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                        <P cLaSs=western StYLE="widows: 0; orphans: 0">
                            <FonT CoLoR="#000000"><FoNT FAce="Footlight MT Light, FreeSerif, serif">A. LINGKUP
			PEKERJAAN</fOnT>
                            </foNT>
                        </p>
                    </tD>
                    <tD WIDTH=317 styLe="border: 1px solid #000000; padding: 0in 0.08in">
                        <ol type="1">
                            <li>
                                Pejabat Pengadaan : <br/>
                                <?php print($pejabat_pengadaan->jenis_pejabat_pengadaan_nama); ?> pada Dinas Pekerjaan Umum Kota Semarang Tahun Anggaran <?php print($pejabat_pengadaan->sk_tahun); ?>
                            </li>
                            <li>
                                Alamat Pejabat Pengadaan:<br/>
                                Jl . Madukoro Raya No. 7 Semarang
                            </li>
                            <li>
                                Website : <br/>
                                http://dpu.semarangkota.go.id/
                            </li>
                            <li>
                                Nama paket pekerjaan : <br/>
                                <?php print($kontrak_pekerjaan->aktivitas_nama); ?>
                            </li>
                            <li>
                                Jangka waktu penyelesaian pekerjaan :
                                <?php print($kontrak_pekerjaan->durasi_kontrak); ?> (<?php print(terbilang($kontrak_pekerjaan->durasi_kontrak)); ?>) hari kalender hari kalender
                            </li>
                        </ol>

                    </tD>
                </tR>
                <tr VALIGN=TOP>
                    <tD WIDTH=249 sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">

                        <P cLAss=western stYLe="widows: 0; orphans: 0">
                            <fOnt COlOr="#000000"><FOnT fACe="Footlight MT Light, FreeSerif, serif">B. SUMBER
			DANA</font>
                            </FONt>
                        </p>
                    </td>
                    <tD WIDTH=317 sTyLE="border: 1px solid #000000; padding: 0in 0.08in">

                        <P cLAsS=western ALIGN=JUSTIFY styLe="margin-bottom: 0in; widows: 0; orphans: 0">
                            Pekerjaan ini dibiayai dari sumber pendanaan <?php print($kontrak_pekerjaan->aktivitas_sumber_dana); ?>
                            
                        </P>
                        <P CLass="western" ALIGN=JUSTIFY STYLe="widows: 0; orphans: 0">
                            <Br>
                        </P>
                    </TD>
                </tr>
                <tR VALIGN=TOP>
                    <td WIDTH=249 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">

                        <p CLASS=western ALIGN=JUSTIFY sTyle="margin-left: -0.06in; margin-top: 0in; widows: 0; orphans: 0">
                            <FOnT cOLOR=#000000><fonT fACe="Footlight MT Light, FreeSerif, serif">C. MASA
			BERLAKU PENAWARAN</FoNT>
                            </fOnT>
                        </P>
                    </td>
                    <tD WIDTH=317 sTyLe="border: 1px solid #000000; padding: 0in 0.08in">

                        <p ClaSs=western STyLe="margin-left: 0.01in; margin-bottom: 0in; widows: 0; orphans: 0">
                            <FOnT Face="Footlight MT Light, FreeSerif, serif">Masa berlaku
			suat penawaran:</fOnt>
                        </p>
                        <P cLasS=western STYLE="margin-left: 0.01in; margin-bottom: 0in; widows: 0; orphans: 0">
                            <font fACE="Footlight MT Light, FreeSerif, serif">15(lima belas)
			hari kalender </FOnT>
                        </p>
                        <p ClaSs="western" sTYLE="margin-left: 0.01in; widows: 0; orphans: 0">
                            <Br>
                        </p>
                    </td>
                </TR>
                <tr VALIGN=TOP>
                    <Td WIDTH=249 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">

                        <P cLasS="western" StYLe="margin-bottom: 0in; widows: 0; orphans: 0">
                            <FonT COLOR="#000000"><fOnT FaCE="Footlight MT Light, FreeSerif, serif">D. DOKUMEN
			PENAWARAN</FonT>
                            </foNt>
                        </P>
                        <P CLaSs=western StyLe="margin-left: -0.06in; widows: 0; orphans: 0">
                            <br>
                        </P>
                    </Td>
                    <tD WIDTH=317 sTYlE="border: 1px solid #000000; padding: 0in 0.08in">

                        <P ClASs=western stYLe="margin-left: 0.01in; margin-bottom: 0in; widows: 0; orphans: 0">
                            <fOnT coLOR=#000000><FONt fACe="Footlight MT Light, FreeSerif, serif">Bagian
				Pekerjaan 	yang</fOnt>
                            </FONT>
                        
                            <fONt COLor=#000000><FOnT FAce="Footlight MT Light, FreeSerif, serif">Disubkontrakkan
			</fONT>
                            </FoNt>
                            <fONT COlOR=#000000><fONt face="Footlight MT Light, FreeSerif, serif"><i>tidak
			ada</i></fOnT>
                            </font>
                            <FonT CoLOR=#000000><FoNT face="Footlight MT Light, FreeSerif, serif">
			</FOnt>
                            </foNT>
                            <fONT cOlOr=#000000><fOnT FacE="Footlight MT Light, FreeSerif, serif">[diisi,
			   apabila  ada  bagian  pekerjaan yang </FONT>
                            </FoNT>
                            <FOnt COlOr=#000000><FOnT Face="Footlight MT Light, FreeSerif, serif">disubkontrakkan
			kepada penyedia spesialis].</FoNt>
                            </fONT>
                        </p>
                        <P CLAsS=western styLE="margin-left: 0.01in; widows: 0; orphans: 0">
                            <bR>
                        </p>
                    </Td>
                </tr>
                <TR VALIGN=TOP>
                    <Td WIDTH=249 stYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">

                        <p CLaSS=western stYLE="margin-left: -0.06in; widows: 0; orphans: 0">
                            <FONt cOlor="#000000"><Font Face="Footlight MT Light, FreeSerif, serif">E. SYARAT
			PENYEDIA</fONT>
                            </foNt>
                        </p>
                    </TD>
                    <tD WIDTH=317 stYLe="border: 1px solid #000000; padding: 0in 0.08in">
                        <P CLaSS="western" StYLE="margin-bottom: 0in; widows: 0; orphans: 0">
                            <FOnt facE="Footlight MT Light, FreeSerif, serif">Memiliki Izin
			Usaha Jasa Konstruksi Nasional </fONT>
                        </P>
                        <p CLasS="western" stYLe="margin-left: 0.07in; widows: 0; orphans: 0">
                            <Br>
                        </p>
                    </Td>
                </tR>
    </TABLE>
    <!-- /Lembar Data Pengadaan -->

    <p style="page-break-before: always"></p>
    
    <!-- Surat Ketetapan HPS -->
    <?php
    $kontrak_surat  = $controller->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan->id, 'SKHPS');
    $kontrak_surat  = $kontrak_surat[0];
    ?>

<p>
        <Img SRc="<?php print(base_url('assets/template_surat/img/kop_surat_dpu_monokrom.png'));?>" nAme="Picture 8" width="100%" ALIGN=center BORDER=0>
    </p>
	<p LanG=id-ID clASs=western ALIGN=CENTER sTyLe="margin-bottom: 0in">
		<strong>
			KEPUTUSAN PEJABAT PEMBUAT KOMITMEN <br/>
			DINAS PEKERJAAN UMUM KOTA SEMARANG
		</strong>
	</p>
	<p LanG=id-ID clASs=western ALIGN=CENTER sTyLe="margin-bottom: 0in">
		<strong>
			NOMOR : <?php print($kontrak_surat->no_surat); ?>
		</strong>
	</p>
	<p LanG=id-ID clASs=western ALIGN=CENTER sTyLe="margin-bottom: 0in">
		<strong>
			TENTANG <br/>
			PENETAPAN HARGA PERKIRAAN SENDIRI (HPS)
			<?php print($kontrak_pekerjaan->pekerjaan_nama); ?>
		</strong>
	</p>
	<p LanG=id-ID clASs=western ALIGN=CENTER sTyLe="margin-bottom: 0in">
		<strong>
			KEGIATAN <br/>
			<?php print($kontrak_pekerjaan->pekerjaan_nama); ?>
		</strong>
	</p>
	<p LanG=id-ID clASs=western ALIGN=CENTER sTyLe="margin-bottom: 0in">
		<strong>
			PEKERJAAN <br/>
			<?php print($kontrak_pekerjaan->aktivitas_nama); ?>
		</strong>
	</p>
	<p></p>
	<table CELLPADDING="7" cellspacing="0">
        <tr>
            <td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                <strong>Menimbang</strong>
            </td>
            <td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
			<td valign="top" align="justify" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
				<ol type="a">
					<li>Bahwa dalam rangka pelaksanaan Pengadaan Barang/Jasa pada Dinas Pekerjaan Umum Kota Semarang maka perlu ditetapkan Harga Perkiraan Sendri (HPS).</li>
					<li>Bahwa penetapan HPS berdasarkan pada hasil data  survey biaya harga satuan, harga dasar, informasi lain yang dapat dipertanggungjawabkan dan hasil perhitungan tim perencana sebagaimana terlampir dalam keputusan ini.</li>
					<li>Bahwa untuk melaksanakan maksud tersebut diatas, maka perlu diterbitkan Keputusan Pejabat Pembuat Komitmen Dinas Pekerjaan Umum Kota Semarang tahun Anggaran <?php print(date("Y")); ?> tentang Penetapan Harga Perkiraan Sendiri (HPS).</li>
				</ol>
			</td>
        </tr>
		<tr>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                <strong>Mengingat</strong>
            </td>
            <td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
			<td valign="top" align="justify" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in; display: block;">
				<ol type="1">
					<li style="left: -10px;">Peraturan Presiden Nomor 4 tahun 2015 tentang Perubahan Keempat atas dasar Peraturan Presiden Nomor 54 Tahun 2010 Tentang Pengadaan barang/ Jasa Pemerintah ;</li>
					<li>Peraturan Daerah Kota Semarang Nomor 16 Tahun 2016 tentang Anggaran Pendapatan dan Belanja Daerah Kota Semarang Tahun Anggaran 2017;</li>
					<li>Peraturan Walikota Semarang Nomor 129 Tahun 2016 tentang Penjabaran  Anggaran Pendapatan dan Belanja Daerah Kota Semarang Tahun Anggaran 2017;</li>
					<li>Daftar Pelaksanaan Anggaran (DPA) Dinas Pekerjaan Umum Kota Semarang Tahun Anggaran 2017;</li>
					<li>
						Keputusan Kepala Dinas Pekerjaan Umum Kota Semarang Nomor <?php print((!empty($pejabat_pembuat_komitmen->sk_no))?$pejabat_pembuat_komitmen->sk_no:''); ?> tanggal <?php print((!empty($pejabat_pembuat_komitmen->sk_tgl_penetapan))?tgl_indo($pejabat_pembuat_komitmen->sk_tgl_penetapan):''); ?> tentang Perubahan Penunjukkan Pejabat Pembuat Komitmen dan Pejabat Pelaksana Teknis Kegiatan (PPTK) pada Dinas Pekerjaan Umum Kota Semarang Tahun Anggaran <?php print((!empty($pejabat_pembuat_komitmen->tahun))?$pejabat_pembuat_komitmen->tahun:''); ?>.
					</li>
				</ol>
			</td>
		</tr>
	</table>
	<p LanG=id-ID clASs=western ALIGN="right" sTyLe="margin-bottom: 0in">
		Memutuskan
	</p>
	<p style="page-break-before: always"></p>
	<p LanG=id-ID align="center" clASs=western sTyLe="margin-bottom: 0in">
		<strong>MEMUTUSKAN</strong>
	</p>
	<p LanG=id-ID align="left" clASs=western sTyLe="margin-bottom: 0in;">
		Menetapkan : 
	</p>
	<table CELLPADDING="7" cellspacing="0">
		<tr>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
				PERTAMA
			</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
				Menetapkan Harga Perkiraan Sendiri (HPS) untuk Pelaksanaan:
				<table CELLPADDING="7" cellspacing="0">
					<tr>
						<td valign="top">
							Kegiatan
						</td>
						<td valign="top">:</td>
						<td valign="top">
							<?php print(ucwords(strtolower($kontrak_pekerjaan->pekerjaan_nama))); ?>
						</td>
					</tr>
					<tr>
						<td valign="top">
							Pekerjaan
						</td>
						<td valign="top">:</td>
						<td valign="top">
							<?php print($kontrak_pekerjaan->aktivitas_nama); ?>
						</td>
					</tr>
					<tr>
						<td valign="top">
							Pagu Anggaran
						</td>
						<td valign="top">:</td>
						<td valign="top">
							<?php print('Rp '.format_money($kontrak_pekerjaan->aktivitas_anggaran_awal).',-'); ?>
						</td>
					</tr>
					<tr>
						<td valign="top">
							Harga Perkiraan Sendiri
						</td>
						<td valign="top">:</td>
						<td valign="top">
							<?php print('Rp '.format_money($kontrak_pekerjaan->hps).',-'); ?>
						</td>
					</tr>
					<tr>
						<td valign="top">
							Dengan huruf
						</td>
						<td valign="top">:</td>
						<td valign="top">
							<?php print(ucwords(strtolower(terbilang($kontrak_pekerjaan->hps))).' Rupiah'); ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
				KEDUA
			</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
				Apabila kelak kemudian hari ternyata terdapat kekeliruan dalam keputusan ini akan diubah dan diperbaiki sebagaimana mestinya.
			</td>
		</tr>
		<tr>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
				KETIGA
			</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
				Keputusan ini mulai berlaku pada tanggal ditetapkan.
			</td>
		</tr>
	</table>
	<p></p>
	<p></p>
	<table>
		<tr>
			<td width="550"></td>
			<td width="450" align="center" STYLe="padding-left:500px; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
				<table align="center">
					<tr>
						<td align="left" STYLe="padding-left:500px; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">Ditetapkan di </td>
						<td align="left" STYLe="padding-left:500px; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
						<td align="left" STYLe="padding-left:500px; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">Semarang</td>
					</tr>
					<tr>
						<td align="left" STYLe="padding-left:500px; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">Tanggal</td>
						<td align="left" STYLe="padding-left:500px; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
						<td align="left" STYLe="padding-left:500px; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in"><?php print(tgl_indo($kontrak_surat->tgl_surat)); ?></td>
					</tr>
					<tr>
						<td colspan="3">
							<hr width="200" style="height:3px;border:none;color:#333;background-color:#333;">
						</td>
					</tr>
				</table>
				<br/>

				Pejabat Pembuat Komitmen
				Kegiatan <?php print(ucwords(strtolower($kontrak_pekerjaan->pekerjaan_nama))); ?>
				<br/><br/><br/><br/><br/>
				<u><?php print((!empty($pejabat_pembuat_komitmen->pegawai_nama))?$pejabat_pembuat_komitmen->pegawai_nama:''); ?></u><br/>
                NIP. <?php print((!empty($pejabat_pembuat_komitmen->pegawai_nip))?$pejabat_pembuat_komitmen->pegawai_nip:''); ?>
			</td>
		</tr>
	</table>
	<p></p>
	<p></p>
	<p LanG=id-ID align="left" clASs=western sTyLe="margin-bottom: 0in; font-size:10pt">
		Tembusan disampaikan kepada Yth :
		<ol style="font-size:10pt">
			<li>Inspektur Wilayah Kota Semarang;</li>
			<li>Kepala DPKAD Kota Semarang;</li>
			<li>Kepala Bagian Hukum Setda Kota Semarang;</li>
			<li>Pertinggal</li>
		</ol>
	</p>
    <!-- /Surat Ketetapan HPS -->

</BODY>

</HTML>