<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kontrak_dokumen_lainnya_pihak_ketiga_model extends CI_Model {

    /*--- kontrak_dokumen_lainnya_pihak_ketiga ---*/
    // id
    // kontrak_pihak_ketiga_id
    // nama
    // file
    // created_at
    // updated_at

    var $column_order = array(null, 'kontrak_dokumen_lainnya_pihak_ketiga.id',
                'kontrak_dokumen_lainnya_pihak_ketiga.nama', 
                'kontrak_dokumen_lainnya_pihak_ketiga.file'); 

    var $column_search = array('kontrak_dokumen_lainnya_pihak_ketiga.id',
                'kontrak_dokumen_lainnya_pihak_ketiga.nama', 
                'kontrak_dokumen_lainnya_pihak_ketiga.file');

    // default order 
    var $order = array('kontrak_dokumen_lainnya_pihak_ketiga.id' => 'asc');

    public function fetch_data($id=NULL, $kontrak_pihak_ketiga_id=NULL)
    {
        $sql = $this->db->select('kontrak_dokumen_lainnya_pihak_ketiga.id as id, 
                                kontrak_pihak_ketiga.id as kontrak_pihak_ketiga_id,
                                kontrak_dokumen_lainnya_pihak_ketiga.nama as nama, 
                                kontrak_dokumen_lainnya_pihak_ketiga.file as file')
                        ->from('kontrak_dokumen_lainnya_pihak_ketiga')
                        ->join('kontrak_pihak_ketiga', 'kontrak_dokumen_lainnya_pihak_ketiga.kontrak_pihak_ketiga_id=kontrak_pihak_ketiga.id', 'left');

        if($id!=NULL){
            $sql->where('kontrak_dokumen_lainnya_pihak_ketiga.id', $id);
        }

        if($kontrak_pihak_ketiga_id!=NULL) {
            $sql->where('kontrak_dokumen_lainnya_pihak_ketiga.kontrak_pihak_ketiga_id', $kontrak_pihak_ketiga_id);
        }

        if($id!=NULL) {
            return $sql->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }

    /*====== Serverside Datatable ======*/
	private function _get_datatables_query($kontrak_pihak_ketiga_id)
	{
        $this->db->select('kontrak_dokumen_lainnya_pihak_ketiga.id,
                    kontrak_dokumen_lainnya_pihak_ketiga.nama, 
                    kontrak_dokumen_lainnya_pihak_ketiga.file');

        $this->db->from('kontrak_dokumen_lainnya_pihak_ketiga');
        $this->db->join('kontrak_pihak_ketiga', 'kontrak_dokumen_lainnya_pihak_ketiga.kontrak_pihak_ketiga_id=kontrak_pihak_ketiga.id', 'left');
        
        $this->db->where('kontrak_dokumen_lainnya_pihak_ketiga.kontrak_pihak_ketiga_id', $kontrak_pihak_ketiga_id);
        
        $i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($kontrak_pihak_ketiga_id)
	{
		$this->_get_datatables_query($kontrak_pihak_ketiga_id);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($kontrak_pihak_ketiga_id)
	{
		$this->_get_datatables_query($kontrak_pihak_ketiga_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($kontrak_pihak_ketiga_id)
	{
		$this->db->select('kontrak_dokumen_lainnya_pihak_ketiga.id,
                    kontrak_dokumen_lainnya_pihak_ketiga.nama, 
                    kontrak_dokumen_lainnya_pihak_ketiga.file');

        $this->db->from('kontrak_dokumen_lainnya_pihak_ketiga');
        $this->db->join('kontrak_pihak_ketiga', 'kontrak_dokumen_lainnya_pihak_ketiga.kontrak_pihak_ketiga_id=kontrak_pihak_ketiga.id', 'left');
        
        $this->db->where('kontrak_dokumen_lainnya_pihak_ketiga.kontrak_pihak_ketiga_id', $kontrak_pihak_ketiga_id);

		return $this->db->count_all_results();
	}
	/*====== /Serverside Datatable ======*/
}

/* End of file kontrak_dokumen_lainnya_pihak_ketiga_model.php */
