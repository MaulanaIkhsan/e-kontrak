<!DOCTYPE html>
<HtmL>
<HeAd>
	<MeTa HTTP-EquIV="CONTENT-TYPE" CONTent="text/html; charset=utf-8">
	<TItlE>Semarang, 29 Maret 2004</tITLE>
	<meta name=GENERATOR coNTeNT="LibreOffice 4.1.6.2 (Linux)">
	<Meta nAMe="AUTHOR" CONTeNt="DPU">
	<MEtA NaMe=CREATED cONtENt="Tahun_Anggaran0817;20400000000000">
	<MeTA NAmE="CHANGEDBY" CoNteNt="isan">
	<META NAme="CHANGED" coNTent="20190320;163019000000000">
	<MEtA nAmE=KSOProductBuildVer CONTEnt=1033-10.1.0.6757>
	<sTyLE>
		@page { size: 8.47in 13.98in; margin-right: 0.88in; margin-top: 0.59in; margin-bottom: 0.69in }
		P { margin-bottom: 0.08in; direction: ltr; color: #000000 }
		P.western { font-family: "Times New Roman", serif; font-size: 12pt; so-language: en-US }
		P.cjk { font-family: "Times New Roman", serif; font-size: 12pt; so-language: en-US }
		P.ctl { font-family: "Times New Roman", serif; font-size: 12pt; so-language: ar-SA }
		H1 { margin-left: 3.5in; margin-top: 0in; margin-bottom: 0in; direction: ltr; color: #000000; text-align: justify; text-decoration: underline }
		H1.western { font-family: "Times New Roman", serif; font-size: 12pt; so-language: en-US }
		H1.cjk { font-family: "Times New Roman", serif; font-size: 12pt; so-language: en-US }
		H1.ctl { font-family: "Times New Roman", serif; font-size: 12pt; so-language: ar-SA }
	</sTYLE>
</heAd>
<bOdY LANg="en-US" tExT=#000000 dIR="LTR">
	<p>
        <Img SRc="<?php print(base_url('assets/template_surat/img/kop_surat_dpu_monokrom.png'));?>" nAme="Picture 8" width="100%" ALIGN=center BORDER=0>
    </p>
	<p LanG=id-ID clASs=western ALIGN=CENTER sTyLe="margin-bottom: 0in">
		<strong>SURAT PESANAN </strong><br/>
		Nomor : <?php print($kontrak_surat->no_surat); ?>
	</p>
	<p></p>
	<p LanG=id-ID clASs=western sTyLe="margin-bottom: 0in">
		Paket Pekerjaan <?php print($kontrak_pekerjaan->aktivitas_nama); ?> yang bertanda tangan di bawah ini:
	</p>
	<table cellspacing="0" cellpadding="7">
		<tr>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">Nama</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in"><?php print((!empty($pejabat_pembuat_komitmen->pegawai_nama))?$pejabat_pembuat_komitmen->pegawai_nama:''); ?></td>
		</tr>
		<tr>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">NIP</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in"><?php print((!empty($pejabat_pembuat_komitmen->pegawai_nip))?$pejabat_pembuat_komitmen->pegawai_nip:''); ?></td>
		</tr>
		<tr>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">Jabatan</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in"><?php print((!empty($pejabat_pembuat_komitmen->jabatan_nama))?ucwords(strtolower($pejabat_pembuat_komitmen->jabatan_nama)):''); ?> pada <?php print((!empty($pejabat_pembuat_komitmen->unit_kerja_nama))?ucwords(strtolower($pejabat_pembuat_komitmen->unit_kerja_nama)):''); ?></td>
		</tr>
		<tr>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">Alamat Satuan Kerja</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">Jl.  Madukoro Raya No. 7 Semarang</td>
		</tr>
	</table>
	<p></p>
	<p LanG=id-ID clASs=western ALIGN="justify" sTyLe="margin-bottom: 0in">
		selanjutnya disebut sebagai Pejabat Pembuat Komitmen;
	</p>
	<p></p>
	<p LanG=id-ID clASs=western ALIGN="justify" sTyLe="margin-bottom: 0in">
		berdasarkan Surat Perjanjian (Kontrak) untuk melaksanakan paket pekerjaan <?php print($kontrak_pekerjaan->aktivitas_nama); ?> Nomor <?php print((!empty($spk[0]->no_surat))?$spk[0]->no_surat:''); ?> tanggal <?php print((!empty($spk[0]->tgl_surat))?tgl_indo($spk[0]->tgl_surat):''); ?> bersama ini mmerintahkan : 
	</p>
	<p></p>
	<table cellspacing="0" cellpadding="7">
		<tr>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">Nama Penyedia</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in"><?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama);?></td>
		</tr>
		<tr>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">Alamat</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in"><?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_alamat);?></td>
		</tr>
		<tr>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">yang dalam hal ini diwakili oleh</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in"><?php print($direktur_perusahaan); ?></td>
		</tr>
	</table>
	<p></p>
	<p LanG=id-ID clASs=western ALIGN="justify" sTyLe="margin-bottom: 0in">
		selanjutnya disebut sebagai Penyedia;
	</p>
	<p></p>
	<p LanG=id-ID clASs=western ALIGN="justify" sTyLe="margin-bottom: 0in">
		Untuk segera memulai pelaksanaan pekerjaan dengan memperhatikan ketentuan-ketentuan sebagai berikut:
		<ol type="1">
			<li>Macam pekerjaan <?php print($kontrak_pekerjaan->aktivitas_nama); ?></li>
			<li>Tanggal mulai kerja <?php print((!empty($spk[0]->tgl_surat))?tgl_indo($spk[0]->tgl_surat):''); ?></li>
			<li>Syarat-syarat pekerjaan, sesuai dengan persyaratan dan ketentuan Kontrak;</li>
			<li>Waktu penyelesaian selama <?php print($kontrak_pekerjaan->durasi_kontrak); ?> (<?php print(terbilang($kontrak_pekerjaan->durasi_kontrak)); ?>) dan pekerjaan harus sudah selesai pada tanggal <?php print(tgl_indo($kontrak_pekerjaan->tgl_akhir_kontrak)); ?></li>
			<li>Sanksi Terhadap keterlambatan penyerahan hasil kerja dan laporan akhir, Kontrak Pengadaan Barang jasa dan pembayaran kepada penyedia dapat dihentikan sesuai dengan ketentuan dalam Syarat-Syarat Umum Kontrak.</li>
		</ol>
	</p>
	<p></p>
    <p></p>
    <table>
        <tr>
            <td width="300" align="center" style="font-size:16pt">
                Setuju:<br/>
                <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama);?><br/>
                <br/><br/><br/><br/><br/><br/>
                <u><?php print($direktur_perusahaan);?></u><br/>
                Direktur
            </td>
            <td width="300"></td>
            <td width="400" align="center" style="font-size:16pt">
				An. Kepala Dinas Pekerjaan Umum Kota Semarang<br/>
				Kuasa Pengguan Anggaran sebagai<br/>
				Pejabat Pembuat Komitmen<br/>
                <br/><br/><br/><br/><br/>
                <u><?php print((!empty($pejabat_pembuat_komitmen->pegawai_nama))?$pejabat_pembuat_komitmen->pegawai_nama:''); ?></u><br/>
                NIP. <?php print((!empty($pejabat_pembuat_komitmen->pegawai_nip))?$pejabat_pembuat_komitmen->pegawai_nip:''); ?>
            </td>
        </tr>
    </table>
</BOdy>
</hTmL>