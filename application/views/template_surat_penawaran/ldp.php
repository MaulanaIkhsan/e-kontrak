<!DOCTYPE html>
<HtmL>

<HeAd>
    <MeTa HTTP-EquIV="CONTENT-TYPE" CONTent="text/html; charset=utf-8">
    <TItlE>Semarang, 29 Maret 2004</tITLE>
    <meta name=GENERATOR coNTeNT="LibreOffice 4.1.6.2 (Linux)">
    <Meta nAMe="AUTHOR" CONTeNt="DPU">
    <MEtA NaMe=CREATED cONtENt="Tahun_Anggaran0817;20400000000000">
    <MeTA NAmE="CHANGEDBY" CoNteNt="isan">
    <META NAme="CHANGED" coNTent="20190320;163019000000000">
    <MEtA nAmE=KSOProductBuildVer CONTEnt=1033-10.1.0.6757>
    <sTyLE>
        @page {
            size: 8.47in 13.98in;
            margin-right: 0.88in;
            margin-top: 0.59in;
            margin-bottom: 0.69in
        }
        P {
            margin-bottom: 0.08in;
            direction: ltr;
            color: #000000
        }
        P.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
        H1 {
            margin-left: 3.5in;
            margin-top: 0in;
            margin-bottom: 0in;
            direction: ltr;
            color: #000000;
            text-align: justify;
            text-decoration: underline
        }
        H1.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
    </sTYLE>
</heAd>

<bOdY LANg="en-US" tExT=#000000 dIR="LTR">
    <p cLaSs="western" ALIGN=CENTER StYLE="margin-bottom: 0in; widows: 0; orphans: 0">
        <FOnt faCE="Footlight MT Light, FreeSerif, serif"><FoNT SIZE=4>LEMBAR
DATA PENGADAAN</fONt>
        </foNt>
    </p>
    <p clAss="western" ALIGN=CENTER STyLe="margin-bottom: 0in; widows: 0; orphans: 0">
        <br>
    </P>
    <P CLASs="western" ALIGN=CENTER StYle="margin-bottom: 0in; widows: 0; orphans: 0">
        <Br>
    </p>
    <TaBle WIDTH=696 CELLPADDING=7 CELLSPACING=0>

                <Tr VALIGN=TOP>
                    <TD WIDTH=249 sTyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                        <P cLaSs=western StYLE="widows: 0; orphans: 0">
                            <FonT CoLoR="#000000"><FoNT FAce="Footlight MT Light, FreeSerif, serif">A. LINGKUP
			PEKERJAAN</fOnT>
                            </foNT>
                        </p>
                    </tD>
                    <tD WIDTH=317 styLe="border: 1px solid #000000; padding: 0in 0.08in">
                        <ol type="1">
                            <li>
                                Pejabat Pengadaan : <br/>
                                <?php print($pejabat_pengadaan->jenis_pejabat_pengadaan_nama); ?> pada Dinas Pekerjaan Umum Kota Semarang Tahun Anggaran <?php print($pejabat_pengadaan->sk_tahun); ?>
                            </li>
                            <li>
                                Alamat Pejabat Pengadaan:<br/>
                                Jl . Madukoro Raya No. 7 Semarang
                            </li>
                            <li>
                                Website : <br/>
                                http://dpu.semarangkota.go.id/
                            </li>
                            <li>
                                Nama paket pekerjaan : <br/>
                                <?php print($kontrak_pekerjaan->aktivitas_nama); ?>
                            </li>
                            <li>
                                Jangka waktu penyelesaian pekerjaan :
                                <?php print($kontrak_pekerjaan->durasi_kontrak); ?> (<?php print(terbilang($kontrak_pekerjaan->durasi_kontrak)); ?>) hari kalender hari kalender
                            </li>
                        </ol>

                    </tD>
                </tR>
                <tr VALIGN=TOP>
                    <tD WIDTH=249 sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">

                        <P cLAss=western stYLe="widows: 0; orphans: 0">
                            <fOnt COlOr="#000000"><FOnT fACe="Footlight MT Light, FreeSerif, serif">B. SUMBER
			DANA</font>
                            </FONt>
                        </p>
                    </td>
                    <tD WIDTH=317 sTyLE="border: 1px solid #000000; padding: 0in 0.08in">

                        <P cLAsS=western ALIGN=JUSTIFY styLe="margin-bottom: 0in; widows: 0; orphans: 0">
                            Pekerjaan ini dibiayai dari sumber pendanaan <?php print($kontrak_pekerjaan->aktivitas_sumber_dana); ?>
                            
                        </P>
                        <P CLass="western" ALIGN=JUSTIFY STYLe="widows: 0; orphans: 0">
                            <Br>
                        </P>
                    </TD>
                </tr>
                <tR VALIGN=TOP>
                    <td WIDTH=249 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">

                        <p CLASS=western ALIGN=JUSTIFY sTyle="margin-left: -0.06in; margin-top: 0in; widows: 0; orphans: 0">
                            <FOnT cOLOR=#000000><fonT fACe="Footlight MT Light, FreeSerif, serif">C. MASA
			BERLAKU PENAWARAN</FoNT>
                            </fOnT>
                        </P>
                    </td>
                    <tD WIDTH=317 sTyLe="border: 1px solid #000000; padding: 0in 0.08in">

                        <p ClaSs=western STyLe="margin-left: 0.01in; margin-bottom: 0in; widows: 0; orphans: 0">
                            <FOnT Face="Footlight MT Light, FreeSerif, serif">Masa berlaku
			suat penawaran:</fOnt>
                        </p>
                        <P cLasS=western STYLE="margin-left: 0.01in; margin-bottom: 0in; widows: 0; orphans: 0">
                            <font fACE="Footlight MT Light, FreeSerif, serif">15(lima belas)
			hari kalender </FOnT>
                        </p>
                        <p ClaSs="western" sTYLE="margin-left: 0.01in; widows: 0; orphans: 0">
                            <Br>
                        </p>
                    </td>
                </TR>
                <tr VALIGN=TOP>
                    <Td WIDTH=249 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">

                        <P cLasS="western" StYLe="margin-bottom: 0in; widows: 0; orphans: 0">
                            <FonT COLOR="#000000"><fOnT FaCE="Footlight MT Light, FreeSerif, serif">D. DOKUMEN
			PENAWARAN</FonT>
                            </foNt>
                        </P>
                        <P CLaSs=western StyLe="margin-left: -0.06in; widows: 0; orphans: 0">
                            <br>
                        </P>
                    </Td>
                    <tD WIDTH=317 sTYlE="border: 1px solid #000000; padding: 0in 0.08in">

                        <P ClASs=western stYLe="margin-left: 0.01in; margin-bottom: 0in; widows: 0; orphans: 0">
                            <fOnT coLOR=#000000><FONt fACe="Footlight MT Light, FreeSerif, serif">Bagian
				Pekerjaan 	yang</fOnt>
                            </FONT>
                        
                            <fONt COLor=#000000><FOnT FAce="Footlight MT Light, FreeSerif, serif">Disubkontrakkan
			</fONT>
                            </FoNt>
                            <fONT COlOR=#000000><fONt face="Footlight MT Light, FreeSerif, serif"><i>tidak
			ada</i></fOnT>
                            </font>
                            <FonT CoLOR=#000000><FoNT face="Footlight MT Light, FreeSerif, serif">
			</FOnt>
                            </foNT>
                            <fONT cOlOr=#000000><fOnT FacE="Footlight MT Light, FreeSerif, serif">[diisi,
			   apabila  ada  bagian  pekerjaan yang </FONT>
                            </FoNT>
                            <FOnt COlOr=#000000><FOnT Face="Footlight MT Light, FreeSerif, serif">disubkontrakkan
			kepada penyedia spesialis].</FoNt>
                            </fONT>
                        </p>
                        <P CLAsS=western styLE="margin-left: 0.01in; widows: 0; orphans: 0">
                            <bR>
                        </p>
                    </Td>
                </tr>
                <TR VALIGN=TOP>
                    <Td WIDTH=249 stYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">

                        <p CLaSS=western stYLE="margin-left: -0.06in; widows: 0; orphans: 0">
                            <FONt cOlor="#000000"><Font Face="Footlight MT Light, FreeSerif, serif">E. SYARAT
			PENYEDIA</fONT>
                            </foNt>
                        </p>
                    </TD>
                    <tD WIDTH=317 stYLe="border: 1px solid #000000; padding: 0in 0.08in">
                        <P CLaSS="western" StYLE="margin-bottom: 0in; widows: 0; orphans: 0">
                            <FOnt facE="Footlight MT Light, FreeSerif, serif">Memiliki Izin
			Usaha Jasa Konstruksi Nasional </fONT>
                        </P>
                        <p CLasS="western" stYLe="margin-left: 0.07in; widows: 0; orphans: 0">
                            <Br>
                        </p>
                    </Td>
                </tR>
    </TABLE>
</BODY>

</HTML>