<?php
$this->load->view('template/header');?>
<!-- Select2 -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/select2/dist/css/select2.min.css')); ?>">

<?php $this->load->view('template/asset_header');?>

<!-- bootstrap datepicker -->

<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')); ?>">

<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <!-- Box Form Pengunjung -->
        <?php 
            print($this->session->flashdata('alert')); 
            print($this->session->flashdata('success')); 
            $sess_data = $this->session->userdata('session_data');
        ?>
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-warning" id="form-pengunjung">
              <div class="box-header with-border">
                <h3 class="box-title">Input Data Pekerjaan</h3>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <form class="form-horizontal" method="post" action="<?php print(base_url('aktivitas/add'))?>">
                <div class="box-body">
                  
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama-lengkap">Nama Kegiatan<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="" name="nama"  class="form-control col-md-7 col-xs-12" required="required" />
                            <span class="text-danger col-md-8"><?php echo form_error('nama'); ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama-lengkap">No Rekening<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="no_rekening" name="no_rekening" class="form-control col-md-7 col-xs-12" required="required" />
                            <span class="text-danger col-md-8"><?php echo form_error('no_rekening'); ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama-lengkap">Sumber Dana<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="sumber_dana" name="sumber_dana" class="form-control col-md-7 col-xs-12" required="required" />
                            <span class="text-danger col-md-8"><?php echo form_error('sumber_dana'); ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="unit-kerja">PPKOM<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="ppkom" class="form-control select2" required="required">
                                <option value="">-- Pilih --</option>
                                <?php foreach($ppkom as $row):?>
                                    <option value="<?php print($row->pejabat_pembuat_komitmen_id);?>"><?php print($row->pegawai_nama);?></option>
                                <?php endforeach;?>
                            </select>
                            <span class="text-danger col-md-8"><?php echo form_error('ppkom'); ?></span>
                        </div>
                    </div>
                    <?php
                    // Restriction to set value PPKOM, PPTK and Bendahara based on role of users Pegawai
                    if($sess_data['role']=='Staff') { ?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="unit-kerja">PPTK<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="pptk" value="<?php print($sess_data['nama']); ?>" readonly="readonly" class="form-control col-md-7 col-xs-12" />
                            </div>
                        </div>
                    <?php } elseif ($sess_data['role']=='Super Admin' or $sess_data['role']=='Admin') { ?>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="unit-kerja">PPTK<span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="pptk" class="form-control select2" required="required">
                                    <option value="">-- Pilih --</option>
                                    <?php foreach($user as $row):?>
                                        <option value="<?php print($row->id);?>"><?php print($row->nama_lengkap);?></option>
                                    <?php endforeach;?>
                                </select>
                                <span class="text-danger col-md-8"><?php echo form_error('pptk'); ?></span>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="unit-kerja">Bendahara<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="bendahara" class="form-control select2" required="required">
                                <option value="">-- Pilih --</option>
                                <?php foreach($user as $row):?>
                                    <option value="<?php print($row->id);?>"><?php print($row->nama_lengkap);?></option>
                                <?php endforeach;?>
                            </select>
                            <span class="text-danger col-md-8"><?php echo form_error('bendahara'); ?></span>
                        </div>
                    </div>
                   
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="anggaran_awal">Pagu Anggaran<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="anggaran" name="anggaran_awal"  class="form-control col-md-7 col-xs-12 money" required="required" />
                            <span class="text-danger col-md-8"><?php echo form_error('anggaran_awal'); ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="perubahan_anggaran">Anggaran Perubahan<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="anggaran" name="perubahan_anggaran"  class="form-control col-md-7 col-xs-12 money" />
                            <span class="text-danger col-md-8"><?php echo form_error('Perubahan Anggaran'); ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="golongan">HPS
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="hps" name="hps"  class="form-control col-md-7 col-xs-12 money" />
                            <span class="text-danger col-md-8"><?php echo form_error('hps'); ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="golongan">Nilai Kontrak
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="nilai_kontrak" name="nilai_kontrak"  class="form-control col-md-7 col-xs-12 money" />
                            <span class="text-danger col-md-8"><?php echo form_error('nilai_kontrak'); ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="anggaran_awal">Tgl Pengajuan Pencairan<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="pengajuan_pencairan" name="tgl_pengajuan_pencairan"  class="form-control col-md-7 col-xs-12 datepicker" readonly="readonly" />
                            <span class="text-danger col-md-8"><?php echo form_error('Tgl_Pengajuan_Pencairan'); ?></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="anggaran_awal">Tgl Pencairan<span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="text" id="pencairan" name="tgl_pencairan" readonly="readonly" class="form-control col-md-7 col-xs-12 datepicker" />
                            <span class="text-danger col-md-8"><?php echo form_error('Tgl_Pencairan'); ?></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="golongan">Metode Pengadaan <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select name="jenis_pengadaan" class="form-control select2" required="required">
                                <option value="">-- Pilih --</option>
                                <?php foreach($jenis_pengadaan as $row):?>
                                    <option value="<?php print($row->id);?>"><?php print($row->nama);?></option>
                                <?php endforeach;?>
                            </select>
                            <span class="text-danger col-md-8"><?php echo form_error('jenis_pengadaan'); ?></span>
                        </div>
                    </div>
                <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama-lengkap">Jenis Pekerjaan <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            
                            <select name="jenis_pekerjaan" class="form-control select2" required="required">
                                <option value="">-- Pilih --</option>
                                <?php foreach($jenis_pekerjaan as $row):?>
                                    <option value="<?php print($row->id);?>"><?php print($row->nama);?></option>
                                <?php endforeach;?>
                            </select>
                            <span class="text-danger col-md-8"><?php echo form_error('jenis_pekerjaan'); ?></span>
                        </div>
                    </div>            
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="golongan">Keterangan
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea class="form-control" name="keterangan" cols="10" rows="3" maxlength="200"></textarea>
                        </div>
                    </div>
                  
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <a href="<?php print(base_url('pekerjaan/detail/'.$this->session->userdata('id_pekerjaan'))); ?>" class="btn btn-primary">Kembali</a> 
                  <button type="submit" name="simpan" class="btn btn-success" value="simpan">Simpan</button>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
          </div>
          <!-- /Box Form Pengunjung -->           

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/select2/dist/js/select2.min.js')) ?>"></script>
<script src="<?php print(base_url('assets/my_custom/js/number-divider.min.js')); ?>"></script>
<!-- bootstrap datepicker -->
<script src="<?php print(base_url('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')); ?>"></script>
<script type="text/javascript">
    $(function(){
        $('.select2').select2();

        $('.datepicker').datepicker({
            format: 'dd-M-yyyy',
            autoclose: true
        });

        //Money format
        $('.money').divide({
            delimiter:',',
            divideThousand:true
        });
        //Just number input
        $(".money").on("keypress keyup",function (event) {    
           $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
    });
</script>
<?php $this->load->view('template/footer');?>