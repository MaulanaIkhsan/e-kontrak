<?php
$this->load->view('template/header');?>
<!-- Select2 -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/select2/dist/css/select2.min.css')); ?>">
<?php $this->load->view('template/asset_header');?>
<link rel="stylesheet" type="text/css" href="<?php print(base_url('assets/bower_components/datatables.net-bs/css/custom_button_export.css'))?>" />
<link href="<?php echo base_url('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css')?>" rel="stylesheet">

<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <!-- Box Form Pengunjung -->
        <?php 
            print($this->session->flashdata('alert')); 
            print($this->session->flashdata('success')); 
            $sess_data = $this->session->userdata('session_data');
        ?>
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-warning" id="form-pengunjung">
              <div class="box-header with-border">
                <h3 class="box-title">Detail Profil Pegawai</h3>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <form class="form-horizontal" method="post" action="<?php print(base_url('pengunjung/add'))?>">
                  <div class="box-body">
                    <div class="form-group">
                        <label for="namalengkap" class="col-sm-2 control-label">NIP</label>
                        <div class="col-sm-9">
                        <div class="form-control" id="nip"></div>
                        </div>
                    </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Nama Lengkap</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="nama_lengkap"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Jenis Kelamin</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="jenis_kelamin"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Alamat</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="alamat"></div>
                    </div>
                  </div>  
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">No Telepon</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="no_telepon"></div>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Unit Kerja</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="unit_kerja"></div>
                    </div>
                  </div>                                  
                </div>
                <div class="box-footer">
                  <a href="#" class="btn btn-primary" onclick="edit_profil()">Edit Profil</a>
                </div>
                <!-- /.box-footer -->
              </form>

               

            </div>
          </div>
          <!-- /Box Form Pengunjung -->

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<script>
$(function(){
  detail_data();

  //Just number input
  $(".number").on("keypress keyup",function (event) {    
      $(this).val($(this).val().replace(/[^\d].+/, ""));
      if ((event.which < 48 || event.which > 57)) {
          event.preventDefault();
      }
  });
});

function detail_data(){
  $.ajax({
    url: "<?php echo site_url('pegawai/ajax_edit/'.$sess_data['id'])?>",
    method: 'GET',
    type: 'JSON',
    success: function(data) {
        // Parsing to object
        data=JSON.parse(data);

        $('#nip').html(data.nip);
        $('#nama_lengkap').text(data.nama_lengkap);
        $('#jenis_kelamin').text(data.jenis_kelamin);
        $('#alamat').text(data.alamat);
        $('#no_telepon').text(data.no_telepon);
        $('#unit_kerja').text(data.unit_kerja);
        $('#role').text(data.role);

    },
    error: function(jqXHR, textStatus, errorThrown) {
        alert('Error while load detail profil pegawai !');
    }
  });
}

function edit_profil() {
  save_method = 'update';
  $('#form-profil')[0].reset(); // reset form on modals
  $('.form-group').removeClass('has-error'); // clear error class
  $('.help-block').empty(); // clear error string

  //Ajax Load data from ajax
  $.ajax({
      url : "<?php print site_url('pegawai/ajax_edit/'.$sess_data['id'])?>",
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
          $('[name="id"]').val(data.id);
          $('[name="nip"]').val(data.nip);
          $('[name="nama_lengkap"]').val(data.nama_lengkap);
          $('[name="jenis_kelamin"]').val(data.jenis_kelamin);
          $('[name="alamat"]').val(data.alamat);
          $('[name="no_telepon"]').val(data.no_telepon);
          $('[name="unit_kerja"]').val(data.unit_id);
          $('[name="username"]').val(data.username);
          
          $('#modal_form_profil').modal('show'); // show bootstrap modal when complete loaded
          $('.modal-title').text('Update Profil'); // Set title to Bootstrap modal title

      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error get data from ajax');
      }
  });
}


function save_profil()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    
    console.log($('#form-profil').serialize());
    // ajax adding data to database
    $.ajax({
        url : "<?php echo site_url('pegawai/ajax_update')?>",
        type: "POST",
        data: $('#form-profil').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            // console.log(data);
            // data=JSON.parse(data);

            if(data.status) //if success close modal and reload ajax table
            {
                alert('Data profil berhasil diupdate !');
                $('#modal_form_profil').modal('hide');
                detail_data();
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(jqXHR+' | '+textStatus+' | '+errorThrown);
            alert('Error update profil !');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}
</script>
<?php 
  $this->load->view('pegawai/modal_form');
  $this->load->view('template/footer');
?>