<!DOCTYPE html>
<HtmL>

<HeAd>
    <MeTa HTTP-EquIV="CONTENT-TYPE" CONTent="text/html; charset=utf-8">
    <TItlE>Semarang, 29 Maret 2004</tITLE>
    <meta name=GENERATOR coNTeNT="LibreOffice 4.1.6.2 (Linux)">
    <Meta nAMe="AUTHOR" CONTeNt="DPU">
    <MEtA NaMe=CREATED cONtENt="Tahun_Anggaran0817;20400000000000">
    <MeTA NAmE="CHANGEDBY" CoNteNt="isan">
    <META NAme="CHANGED" coNTent="20190320;163019000000000">
    <MEtA nAmE=KSOProductBuildVer CONTEnt=1033-10.1.0.6757>
    <sTyLE>
        @page {
            size: 8.47in 13.98in;
            margin-right: 0.88in;
            margin-top: 0.59in;
            margin-bottom: 0.69in
        }
        P {
            margin-bottom: 0.08in;
            direction: ltr;
            color: #000000
        }
        P.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
        H1 {
            margin-left: 3.5in;
            margin-top: 0in;
            margin-bottom: 0in;
            direction: ltr;
            color: #000000;
            text-align: justify;
            text-decoration: underline
        }
        H1.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
    </sTYLE>
</heAd>

<bOdY LANg="en-US" tExT=#000000 dIR="LTR">
    <p>
        <Img SRc="<?php print(base_url('assets/template_surat/img/kop_surat_dpu_monokrom.png'));?>" nAme="Picture 8" width="100%" ALIGN=center BORDER=0>
    </p>
    <table>
        <tr>
            <td width="650">
                <table>
                    <tr>
                        <td style="font-size:16pt">Nomor</td>
                        <td style="font-size:16pt">:</td>
                        <td style="font-size:16pt"><?php print($kontrak_surat->no_surat); ?></td>
                    </tr>
                    <tr>
                        <td style="font-size:16pt">Lampiran</td>
                        <td style="font-size:16pt">:</td>
                        <td style="font-size:16pt">-</td>
                    </tr>
                </table>
            </td>
            <td width="350" align="right" valign="center" style="font-size:16pt">
                Semarang, <?php print(tgl_indo($kontrak_surat->tgl_surat)); ?>
            </td>
        </tr>
    </table>
    <p></p>
    <p cLaSs="western" ALIGN="justify" StYLe="margin-bottom: 0in; width:500;">
        Semarang, <?php print(tgl_indo($kontrak_surat->tgl_surat)); ?><br/>
        Yth. <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama); ?><br/>
        <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_alamat); ?><br/>
        di -<br/>
        Tempat
    </p>
    <p></p>
    <p cLaSs="western" ALIGN="justify" StYLe="margin-bottom: 0in;">
        Perihal : Penunjukan Penyedia untuk Pelaksanaan Paket Pekerjaan <?php print(ucwords($kontrak_pekerjaan->pekerjaan_nama));?>
    </p>
    <p></p>
    <p cLaSs="western" ALIGN="justify" StYLe="margin-bottom: 0in;">
        Dengan ini kami beritahukan bahwa penawaran Saudara <?php print((!empty($surat_penawaran[0]->no_surat))?$surat_penawaran[0]->no_surat:''); ?> tanggal 
        <?php 
            if(!empty($surat_kegiatan_penawaran->tgl)) {
                print(tgl_indo($surat_kegiatan_penawaran->tgl));
            } else {
                print(NULL);
            }
        ?>
         perihal Penawaran <?php print($kontrak_pekerjaan->pekerjaan_nama); ?>, pada kegiatan <?php print($kontrak_pekerjaan->aktivitas_nama); ?> dengan nilai penawaran/penawaran terkoreksi sebesar <?php print('Rp '.format_money($kontrak_pekerjaan->harga_negosiasi).',-'); ?> (<?php print(ucwords(strtolower(terbilang($kontrak_pekerjaan->harga_negosiasi))).' Rupiah'); ?>) kami nyatakan diterima/disetujui.<br/> 
        Sebagai tindak lanjut dari Surat Penunjukan Penyedia/Jasa (SPPBJ) ini Saudara diharuskan untuk menandatangani Surat Perjanjian paling lambat 14 (empat belas) hari kerja setelah diterbitkannya SPPBJ. Kegagalan Saudara untuk menerima penunjukan ini yang disusun berdasarkan evaluasi terhadap penawaran Saudara, akan dikenakan sanksi sesuai ketentuan dalam Peraturan Presiden Republik Indonesia Nomor 4 Tahun 201 5 tentang Perubahan keempat atas Peraturan Presiden Nomor 54 Tahun 2010 tentang Pengadaan Barang/Jasa Pemerintah .
    </p>
    <p></p>
    <table>
        <tr>
            <td width="500" align="left" style="font-size:16pt">
                A.n. Kepala Dinas Pekerjaan Umum<br/>
                Kota Semarang<br/>
                Kuasa Pengguna Anggaran<br/>
                Selaku Pejabat Pembuat Komitmen<br/>
                Tahun Anggaran <?php print(date("Y"));?><br/>
                <br/><br/><br/><br/><br/>
                <u><?php print((!empty($pejabat_pembuat_komitmen->pegawai_nama))?$pejabat_pembuat_komitmen->pegawai_nama:''); ?></u><br/>
                NIP. <?php print((!empty($pejabat_pembuat_komitmen->pegawai_nip))?$pejabat_pembuat_komitmen->pegawai_nip:''); ?>
            </td>
            <td width="250"></td>
            <td width="250"></td>
        </tr>
    </table>
    <p></p>
    <p></p>
    <p ClaSs=western StYLe="margin-bottom: 0in">
        Tembusan Yth : <br/>
        <ol type="i">
            <li>Pengguna Anggaran Dinas Pekerjaan Umum Kota Smarang;</li>
            <li>Inspektur Kota Semarag;</li>
            <li>Ka. DPKAD Kota Semarang;</li>
            <li>Ka. Bagian Pembangunan Kota Semarang;</li>
            <li>Ka. Bagian Hukum Kota Semarang;</li>
            <li>Pejabat Pengadaan ybs;</li>
            <li>Pertinggal;</li>
        </ol>
    </p>

</BODY>

</HTML>