<?php 
// From : https://phpans.com/random-code-generator-in-php/
// Generate Unique Code
function random_code($length=10) {
	$min_lenght= 0;
	$max_lenght = 100;
	
	$bigL = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$smallL = "abcdefghijklmnopqrstuvwxyz";
	$number = "0123456789";
	
	$bigB = str_shuffle($bigL);
	$smallS = str_shuffle($smallL);
	$numberS = str_shuffle($number);
	$subA = substr($bigB,0,5);
	$subB = substr($bigB,6,5);
	$subC = substr($bigB,10,5);
	$subD = substr($smallS,0,5);
	$subE = substr($smallS,6,5);
	$subF = substr($smallS,10,5);
	$subG = substr($numberS,0,5);
	$subH = substr($numberS,6,5);
	$subI = substr($numberS,10,5);

	$RandCode1 = str_shuffle($subA.$subD.$subB.$subF.$subC.$subE);
	$RandCode2 = str_shuffle($RandCode1);
	$RandCode = $RandCode1.$RandCode2;
	
	if ($length>$min_lenght && $length<$max_lenght) {
		$CodeEX = substr($RandCode,0,$length);
	}
	else {
		$CodeEX = $RandCode;
	}

	return $CodeEX;
}

if(!function_exists('random_code_II')){
	// from : https://www.thecodedeveloper.com/generate-random-alphanumeric-string-with-php/
	function random_code_II($length, $prefix=null)
	{
		$rand_array = range(16, 36);
		$rand_index = array_rand($rand_array);
		$rand_number = $rand_array[$rand_index];

		$unique = substr(base_convert(sha1(uniqid(mt_rand())), 16,$rand_number), 0, $length);
		
		if($prefix!=null) {
			return $prefix.$unique;
		}
		else {
			return $unique;
		}
	}
}

function format_money($value) {
	return (''.number_format($value, 0, '.','.').'');
}

// Format Date
function format_date($date, $format)
{
	$date = date_create($date);
	if($format === 1){
		return date_format($date, 'Y-m-d');
	}
	else {
		return date_format($date, 'd-M-Y');
	}
}

// Convert format datetime database ke format tgl indonesia
if(!function_exists('tgl_indo')){
	function tgl_indo($tanggal) {
		$bulan = array('Januari', 'Februari', 'Maret', 'April',
					'Mei', 'Juni', 'Juli', 'Agustus', 
					'September', 'Oktober', 'November', 'Desember');
		
		$split = explode("-", $tanggal);

		return $split[2]." ".$bulan[((int)$split[1]-1)]." ".$split[0];
	}
}

// Convert format day ke format hari indonesia
if(!function_exists('dayname')){
	function dayname($day) {
		switch ($day) {
			case 'Sunday':
				return 'Minggu';
				break;
			case 'Monday':
				return 'Senin';
				break;
			case 'Tuesday':
				return 'Selasa';
				break;
			case 'Wednesday':
				return 'Rabu';
				break;
			case 'Thursday':
				return 'Kamis';
				break;
			case 'Friday':
				return 'Jumat';
				break;
			case 'Saturday':
				return 'Kamis';
				break;
			default:
				return 'Not Found';
				break;
		}
	}
}

// Convert string to lowercase and remove whitespace
function normalize_str($string)
{
	$string = htmlentities($string);
	$string = str_replace(' ', '', $string);
	$string = str_replace("'", '',$string);
    $string = str_replace('"', '',$string);
	$string = str_replace(".", '',$string);
	$string = str_replace(',', '',$string);

	return $string; 
	// return  str_replace(strtolower($string), ' ', '') ;
}

/* Fungsi terbilang angka */
if(!function_exists('kekata')){
	function kekata($nilai) {
		$nilai 	= abs($nilai);
		$angka 	= array("", "satu", "dua", "tiga", 
					"empat", "lima", "enam", "tujuh", 
					"delapan", "sembilan", "sepuluh", "sebelas");
		$temp	= "";

		if($nilai<12) {
			$temp = " ".$angka[$nilai];
		}
		elseif($nilai<20) {
			$temp = kekata($nilai-10)." belas";
		}
		elseif($nilai<100) {
			$temp = kekata($nilai/10)." puluh".kekata($nilai%10);
		}
		elseif($nilai<200) {
			$temp = " seratus".kekata($nilai-100);
		}
		elseif($nilai<1000) {
			$temp = kekata($nilai/100). " ratus".kekata($nilai%100);
		}
		elseif($nilai<2000) {
			$temp = " seribu".kekata($nilai-1000);
		}
		elseif($nilai<1000000) {
			$temp = kekata($nilai/1000)." ribu".kekata($nilai%1000);
		}
		elseif($nilai<1000000000) {
			$temp = kekata($nilai/1000000)." juta".kekata($nilai%1000000);
		}
		elseif($nilai<1000000000000) {
			$temp = kekata($nilai/1000000000)." milyar".kekata(fmod($nilai, 1000000000));
		}
		elseif($nilai<1000000000000000){
			$temp = kekata($nilai/1000000000000)." trilyun".kekata(fmod($nilai, 1000000000000));
		}

		return $temp;
	}
}

if(!function_exists('terbilang')){
	function terbilang($nilai, $type=4){
		if($nilai<0) {
			$hasil = "minus ".kekata($nilai);
		}
		else {
			$hasil = trim(kekata($nilai));
		}

		switch ($type) {
			case 1:
				$hasil = strtoupper($hasil);
				break;
			case 2:
				$hasil = strtolower($hasil);
				break;
			case 3:
				$hasil = ucwords($hasil);
				break;
			default:
				$hasil = ucfirst($hasil);
				break;
		}

		return $hasil;
	}
}

// BCRYPT Encryptor
if(!function_exists('bcrypt_encryptor')){
	function bcrypt_encryptor($value, $cost) {
		$options = array('cost'=>$cost);

		return password_hash($value, PASSWORD_BCRYPT, $options);
	}
}

if(!function_exists('rename_file')) {
	function rename_file() {
		return 'file'.date('Ymd').random_code().date('His');
	}
}

/*================================= ARRAY HELPER ================================= */

// Improvement from : https://gist.github.com/kjbrum/73e89a40e14631c08553

/**
 *  Check if an array is a multidimensional array.
 *
 *  @param   array   $arr  The array to check
 *  @return  boolean       Whether the the array is a multidimensional array or not
 */
if(!function_exists('is_multi_array')) {
	function is_multi_array( $x ) {
	    if( count( array_filter( $x,'is_array' ) ) > 0 ) return true;
	    return false;
	}
}
/**
 *  Convert an object to an array.
 *
 *  @param   array   $object  The object to convert
 *  @return  array            The converted array
 */
if(!function_exists('object_to_array')) {
	function object_to_array( $object ) {
	    if( !is_object( $object ) && !is_array( $object ) ) return $object;
	    return array_map( 'object_to_array', (array) $object );
	}
}

if(!function_exists('normalize_value'))
{
	function normalize_value(array $array, array $list_key)
	{
		if(!empty($array) and !empty($list_value)) {
			foreach($array as $item):
				foreach($item as $key=>$value):
					foreach($list_value as $row):
						if($row==$key){
							$item[$key] = normalize_str($value);
						}
					endforeach;
				endforeach;
			endforeach;
		}
		else {
			return FALSE;
		}
	}
}

/**
 *  Check if a value exists in the array/object.
 *
 *  @param   mixed    $needle    The value that you are searching for
 *  @param   mixed    $haystack  The array/object to search
 *  @column  boolean  $strict    The key that you are searching for 
 *  @return  boolean             Whether the value was found or not
 */
if(!function_exists('search_for_value')) {
	function search_for_value( $needle, $haystack, $column=false ) {
	    $haystack = object_to_array( $haystack );
	    
	    if( is_array( $haystack ) ) {
	    	// Multidimensional array
	        if( is_multi_array( $haystack ) ) {   
	            foreach( $haystack as $subhaystack ) {
	                if( search_for_value( $needle, $subhaystack) ) {
	                    return true;
	                }
	            }
	        } 
	        // Associative array
	        elseif( array_keys( $haystack ) !== range( 0, count( $haystack ) - 1 ) ) {    
	            if($column) {
	            	$check = array_search($needle, array_column($haystack, $column));
	            	if($check){
	            		return true;
	            	} else {
	            		return false;
	            	}

	            } else {
	            	foreach( $haystack as $key => $val ) {
		                if( $needle == $val) {
		                    return true;
		                } elseif( $needle === $val) {
		                    return true;
		                }
		            }
		            return false;
	            }
	        } 
	        // Normal array
	        else {    
	        	$check = in_array($needle, $haystack);
	            
	            if($check){
	            	return true;
	            } else {
	            	return false;
	            }
	        }
	    }
	    return false;
	}
}

//Need Improvement
if(!function_exists('search_for_index')) {
	function search_for_index( $needle, $haystack ) {
		$index = 0;
		$haystack = object_to_array( $haystack );

		if( is_array( $haystack ) ) {
			// Multidimensional array
	        if( is_multi_array( $haystack ) ) {   
	            foreach( $haystack as $subhaystack ) {
	                if( search_for_value( $needle, $subhaystack) ) {
	                    return true;
	                }
	            }
	        }
	        // Associative array
	        elseif( array_keys( $haystack ) !== range( 0, count( $haystack ) - 1 ) ) {    
	            if($column) {
	            	$check = array_search($needle, array_column($haystack, $column));
	            	if($check){
	            		return $check;
	            	} else {
	            		return false;
	            	}

	            }
	            else {
	            	// foreach( $haystack as $key => $val ):
	                	//check for each item
	            	// endforeach;
	            	return 'Hello';
	            }
	            
	        }
	        // Normal array
	        else {    
	        	foreach ($haystack as $item) {
					if($item==$needle){
						break;
						return $index;
					} else {
						$index++;
					}
            	}
	        } 
		}
		return false;
	}
}


if(!function_exists('get_last_item')) {
	// For assosiative array only
	function get_last_item($array)
	{
		$last_index = count($array) - 1;
		if($last_index>1){
			return $array[$last_index];
		}
		elseif ($last_index<0) {
			return false;
		}
		else {
			return $array[$last_index];
		}
	}
}

if (!function_exists('get_keys')) {
	// Get keys of data from table database 
	function get_keys($array) {
		if(is_array($array)){
			return false;
		}
		else {
			$amount = count($array);
			if($amount>1) {
				return array_keys($array[0]);
			}
			elseif ($amount==1) {
				return array_keys($array);
			}
			else {
				return false;
			}
		}
	}
}

// Group by assosiative array based on key
if(!function_exists('group_by_array')){
	function group_by_array($array, $key)
	{
		$result = [];

		//If $key is a array
		// if(is_array($key)){
			
		// }
		// else {
		// 	foreach($array as $val):
		// 		if(array_key_exists($key, $array)){
		// 			$result[$val[$key]][] = $val;
		// 		}
		// 		else {
		// 			$result[""][] = $val;
		// 		}
		// 	endforeach;
			
		// 	return $result;
		// }

		$uniquekeys = [];
		$output = [];

		foreach($array as $item):
			if(!in_array($item[$key], $uniquekeys)){
				// $uniquekeys[] = $item[$key];
				// $output[] = $key;

				$uniquekeys[] = $item;
				$output[] = $item;
			}
		endforeach;

		return $output;

	}
}

// Insert item into array
if(!function_exists('item_into_array')){
	function item_into_array(array $target, $item)
	{
		if(is_object($item) ){
			array_push($target, (array)$item);
			return $target;
		}
		elseif(is_array($item)){
			array_push($target, $item);
			return $target;
		}
		else {
			return $target;
		}
	}
}

// Search index on assosiative array based on key and value
if(!function_exists('index_by_column')){
	function index_by_column($array, $column, $value)
	{
		return array_search($value, array_column($array, $column));
	}
}

// CSV File Reader
if(!function_exists('csv_reader')){
	function csv_reader($filepath)
	{
		if(!file_exists($filepath)) {
			return FALSE;
		} else {
			$csvFile = file($filepath);
			$data = [];
			
			foreach($csvFile as $line):
				if(!empty($line)) {
					$data[] = str_getcsv($line);
				}
				else {
					continue;
				}
				
			endforeach;
			
			return $data;
		}
	}
}

