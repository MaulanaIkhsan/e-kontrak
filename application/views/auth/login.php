<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Sisuper DPU</title>

        <!-- CSS -->
        <!-- Tell the browser to be responsive to screen width -->
        <link rel="stylesheet" href="<?php print(base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css')); ?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php print(base_url('assets/bower_components/font-awesome/css/font-awesome.min.css')); ?>">
        <link rel="stylesheet" href="<?php print(base_url('assets/bootstrap-login/css/style.css'));?>">
        
    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left" id="text">
                        			<h3><strong>Form Login</strong></h3>
                            		<p>Silahkan login untuk masuk sistem <strong>Sisuper</strong> :</p>
                        		</div>
                        		<div class="form-top-right">
                                    <img src="<?php print(base_url('assets/bootstrap-login/img/logo_pemkot_smg.png'))?>" width="80" height="85"/>
                        		</div>
                            </div>
                            <div class="form-bottom">
                                <?php print($this->session->flashdata('error')); ?>
                                <?php print($this->session->flashdata('warning')); ?>
                                <?php print($this->session->flashdata('alert')); ?>

			                    <form role="form" action="<?php print(base_url('auth')); ?>" method="post" class="login-form">
			                    	<div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                            <input type="text" name="username" placeholder="Username" class="form-control input-lg" id="form-username" required="required" />
                                        </div>
                                        <span class="text-danger col-md-8"><?php echo form_error('username'); ?></span>
			                        </div>
			                        <div class="form-group">
			                        	<div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                            <input type="password" name="password" placeholder="Password" class="form-control input-lg" id="form-password" required="required" maxlength="20" />
                                        </div>
                                        <span class="text-danger col-md-8"><?php echo form_error('password'); ?></span>
                                    </div>
                                    <div class="form-group">
                                        <label for="nik" class="col-sm-3 control-label text">Sebagai </label>
                                        <div class="col-sm-9">
                                            <select name="person" class="form-control">
                                                <option value="">--Pilih--</option>
                                                <option value="pegawai">Pejabat Pengadaan / PPKOM</option>
                                                <option value="pihak_ketiga">Penyedia Barang/Jasa</option>
                                            </select>                                          
                                        </div>
                                        <span class="text-danger col-md-8"><?php echo form_error('person'); ?></span>
                                    </div>
                                    <div class="form-group"><div class="col-md-12"><p></p></div></div>
			                        <button type="submit" class="btn btn-success btn-lg" name="login"><i class="fa fa-sign-in"></i> Login!</button>
			                    </form>
		                    </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>


        <!-- Javascript -->
        <!-- jQuery 3 -->
        <script src="<?php print(base_url('assets/bower_components/jquery/dist/jquery.min.js')); ?>"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?php print(base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
        <script src="<?php print(base_url('assets/bootstrap-login/js/jquery.backstretch.min.js'));?>"></script>
        <script src="<?php print(base_url('assets/bootstrap-login/js/scripts.js'));?>"></script>
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->
    </body>
</html>