<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Riwayat_anggaran_model extends CI_Model {

	var $main_table = 'riwayat_anggaran';

	public function check_data_riwayat($kode_pekerjaan)
	{
		$sql = $this->db->select('kode_pekerjaan, tahap_pekerjaan, anggaran')
						->from($this->main_table)
						->where('kode_pekerjaan', $kode_pekerjaan)
						->get();
		if($sql->num_rows()==0){
			return FALSE;
		}
		else {
			return $sql->result();
		}
	}

	public function check_tahap_riwayat($kode_pekerjaan, $tahap_pekerjaan)
	{
		$sql = $this->check_data_riwayat($kode_pekerjaan);

		$result = FALSE;
		if(!empty($sql)){
			foreach ($sql as $item):
				if($item->tahap_pekerjaan==$tahap_pekerjaan) {
					$result = TRUE;
				}
			endforeach;
		}
		elseif(empty($sql)){
			return $result;
		}
		
		// if($sql->tahap_pekerjaan !== $tahap_pekerjaan){
		// 	return FALSE;
		// }
		// else {
		// 	return TRUE;
		// }
		return $result;
	}

	public function check_anggaran_riwayat($kode_pekerjaan, $tahap_pekerjaan ,$anggaran)
	{
		$sql = $this->check_data_riwayat($kode_pekerjaan);
		$result = FALSE;

		foreach ($sql as $item):
			if(($item->tahap_pekerjaan==$tahap_pekerjaan)&&($item->anggaran!==$anggaran)) {
				$result = TRUE;
			}
		endforeach;
		return $result;
	}

	public function update($data, $key)
	{
		$sql = $this->db->where($key)
						->update($this->main_table, $data);
		if($sql){
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	// Base Insert Data
	public function insert($data)
	{
		//input data program baru
		$sql = $this->db->insert($this->main_table, $data);
		if($sql){
			return 'ok';
		}
		else {
			return 'fail';
		}
	}

	public function insert_data_api($data, $linked_pekerjaan)
	{
		$tahap_pekerjaan = $data['tahap_pekerjaan'];
		// $kode_pekerjaan = $data['kode_pekerjaan'];
		$anggaran = $data['anggaran'];

		$this->load->model('Pekerjaaan_model', 'pekerjaan');

		$kode_pekerjaan = $linked_pekerjaan['kode_pekerjaan'];
		$nama_pekerjaan = $linked_pekerjaan['nama_pekerjaan'];
		$tahun_pekerjaan = $linked_pekerjaan['tahun'];
		$get_linked = $this->pekerjaan->get_pekerjaan_by_uniq_code($kode_pekerjaan, $nama_pekerjaan, $tahun_pekerjaan);
		$kode_pekerjaan = $get_linked->uniq_code;
		

		$check_riwayat = $this->check_data_riwayat($kode_pekerjaan);
		if($check_riwayat!==FALSE) {

			$check_tahap_riwayat = $this->check_tahap_riwayat($kode_pekerjaan, $tahap_pekerjaan);
			if($check_tahap_riwayat===FALSE) {
				$data['kode_pekerjaan'] = $kode_pekerjaan;
				//input data riwayat anggaran baru
				$insert_data = $this->insert($data);

				if($insert_data==='ok'){
					return 'ok';
				}
				else {
					return 'fail';
				}
			}//check nama_program
			else {
				//tahap_program already exist and update anggaran
				$check_anggaran = $this->check_anggaran_riwayat($kode_pekerjaan, $tahap_pekerjaan, $anggaran);
				if ($check_anggaran===FALSE) {
					$data = [];
					$data['anggaran'] = $anggaran;
					$key = array(
						'kode_pekerjaan' => $kode_pekerjaan,
						'tahap_pekerjaan' => $tahap_pekerjaan
					);
					// updata data riwayat anggaran
					$update = $this->update($data, $key);
					if ($update==='ok') {
						return 'ok';
					}
					else {
						return 'fail';
					}
				}// check anggaran
			} //else
		} //check_program
		else {
			$data['kode_pekerjaan'] = $kode_pekerjaan;

			//input data riwayat anggaran baru
			$sql = $this->db->insert($this->main_table, $data);
			if($sql){
				return 'ok';
			}
			else {
				return 'fail';
			}
		}

	}

	public function show($kode_pekerjaan)
	{
		return $this->db->select('id, kode_pekerjaan, tahap_pekerjaan, anggaran')
						->from($this->main_table)
						->where('kode_pekerjaan', $kode_pekerjaan)
						->get()->result();
	}

	public function show_update($key)
	{
		return $this->db->select('id, kode_pekerjaan, tahap_pekerjaan, anggaran')
						->from($this->main_table)
						->where('id', $key)
						->get()->row();
	}

	public function delete($key)
	{
		$sql = $this->db->delete($this->main_table, $key);
		if ($sql) {
			return 'ok';
		}
		else{
			return 'fail';
		}
	}

	public function get_riwayat_anggaran($kode_pekerjaan=null, $tahap_pekerjaan=null)
	{
		$sql = $this->db->select('id, kode_pekerjaan, tahap_pekerjaan, anggaran')
						->from($this->main_table);

		if($kode_pekerjaan!=null){
			$sql->where('kode_pekerjaan', $kode_pekerjaan);
		}

		if($tahap_pekerjaan!=null){
			$sql->where('tahap_pekerjaan', $tahap_pekerjaan);
			return $sql->get()->row();
		}

		return $sql->get()->result();
		
	}
}

/* End of file Riwayat_anggaran_model.php */
/* Location: ./application/models/Riwayat_anggaran_model.php */