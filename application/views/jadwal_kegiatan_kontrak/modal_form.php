<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_jadwal_kegiatan" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Upload</h4>
            </div>
            <div class="modal-body form">
                <form id="form-jadwal-kegiatan" class="form-horizontal" enctype="multipart/form-data" method="post">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Jenis Kegiatan Pengadaan</label>
                            <div class="col-md-9">
                                <select name="jenis_kegiatan_id" class="form-control">
                                    <option value="">--Select Kegiatan Pengadaan--</option>
                                    <?php foreach($jenis_kegiatan as $item):?>
                                        <option value="<?php print($item->id); ?>"><?php print($item->nama); ?></option>
                                    <?php endforeach;?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                       
                        <div class="form-group">
                            <label class="control-label col-md-3">Tanggal</label>
                            <div class="col-md-9">
                                <input name="tgl" placeholder="Tanggal Kegiatan" class="form-control datepicker" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Jam</label>
                            <div class="col-md-9">
                                <input name="jam" placeholder="Jam" class="form-control" type="text" data-inputmask="'alias': 'hh:mm'" data-mask>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                
            </div>
            <div class="modal-footer">
                    <button type="submit" id="btnSaveJadwalKegiatan" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->