<!-- Sidebar Menu -->
<ul class="sidebar-menu" data-widget="tree">
    <li class="header"><div style="text-align: center;">DAFTAR MENU</div> </li>
    <!-- Optionally, you can add icons to the links -->
    <li><a href="<?php print(base_url('beranda')); ?>"><i class="fa fa-home"></i> <span>Beranda</span></a></li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-copy"></i> <span>Info Program Pekerjaan</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="<?php print(base_url('program')); ?>"><i class="fa fa-file-text-o"></i> <span>Data Program Pekerjaan</span></a></li>
        <li><a href="<?php print(base_url('aktivitas')); ?>"><i class="fa fa-list-alt"></i> <span>Resume Kegiatan</span></a></li>
      </ul>
      
    </li>
    <li class="treeview">
      <a href="#">
        <i class="fa fa-paste"></i> <span>Kontrak Pekerjaan</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
      </a>
      <ul class="treeview-menu">
        <li><a href="<?php print(base_url('aktivitas/kegiatan')); ?>"><i class="fa fa-list-alt"></i> <span>Data Pekerjaan</span></a></li>
        <li><a href="<?php print(base_url('kontrak_pekerjaan')); ?>"><i class="fa fa-file-text-o"></i> <span>Resume Data Kontrak</span></a></li>
        <li><a href="#" data-toggle="modal" data-target="#modal-rekap-kontrak-pekerjaan"><i class="fa fa-print"></i> <span>Rekap Data Kontrak</span></a></li>
      </ul>
    </li>
    <li class="treeview">
          <a href="#"><i class="fa fa-database"></i> <span>Master Data</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i> <span>Pegawai</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php print(base_url('pegawai')); ?>"><i class="fa fa-child"></i> <span>Seluruh Pegawai</span></a></li>
                <li><a href="<?php print(base_url('pegawai/pejabat_pengadaan')); ?>"><i class="fa fa-file-text-o"></i> <span>Pejabat Pengadaan</span></a></li>
              </ul>
            </li>
            
            <li><a href="<?php print(base_url('pihak_ketiga')); ?>"><i class="fa fa-industry"></i> <span>Penyedia Barang/Jasa</span></a></li>

            <li class="treeview">
              <a href="#">
                <i class="fa fa-file-text-o"></i> <span>Data Lainnya</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="<?php print(base_url('lainnya/unit_kerja')); ?>"><i class="fa fa-th-list"></i> <span>Unit Kerja</span></a></li>
                <li><a href="<?php print(base_url('lainnya/jenis_pengadaan')); ?>"><i class="fa fa-sticky-note-o"></i> <span>Metode Pengadaan</span></a></li>
                <li><a href="<?php print(base_url('lainnya/jenis_pekerjaan')); ?>"><i class="fa fa-sticky-note-o"></i> <span>Jenis Pekerjaan</span></a></li>
                <li><a href="<?php print(base_url('lainnya/dokumen_template')); ?>"><i class="fa fa-file-o"></i> <span>Dokumen Template</span></a></li>
                <li><a href="<?php print(base_url('lainnya/jenis_surat')); ?>"><i class="fa fa-envelope"></i> <span>Jenis Surat Penawaran</span></a></li>
              </ul>
            </li>
            
          </ul>
        </li>
    <li><a href="<?php print(base_url('panduan')); ?>"><i class="fa fa-book"></i> <span>Panduan Pengguna</span></a></li>
    <li><a href="<?php print(base_url('pegawai/profil')); ?>"><i class="fa fa-user-secret"></i> <span>Profil</span></a></li>
    <li><a href="#" data-toggle="modal" data-target="#modal-logout"><i class="fa fa-user"></i> <span>Logout</span></a></li>    
</ul>
<!-- /.sidebar-menu -->