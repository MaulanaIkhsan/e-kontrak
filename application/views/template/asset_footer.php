<!-- jQuery 3 -->
<script src="<?php print(base_url('assets/bower_components/jquery/dist/jquery.min.js')); ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php print(base_url('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')); ?>"></script>
<!-- SlimScroll -->
<script src="<?php print(base_url('assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')); ?>"></script>
<!-- DataTables -->
<script src="<?php print(base_url('assets/bower_components/datatables.net/js/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')); ?>"></script>
<!-- AdminLTE App -->
<script src="<?php print(base_url('assets/dist/js/adminlte.min.js')); ?>"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php print(base_url('assets/plugins/iCheck/icheck.min.js')); ?>"></script>

<script type="text/javascript">
	$(function(){
		$('.alert').fadeOut(30000);
	})
</script>