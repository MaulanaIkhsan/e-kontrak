<!DOCTYPE html>
<HtmL>
<HeAd>
	<MeTa HTTP-EquIV="CONTENT-TYPE" CONTent="text/html; charset=utf-8">
	<TItlE>Semarang, 29 Maret 2004</tITLE>
	<meta name=GENERATOR coNTeNT="LibreOffice 4.1.6.2 (Linux)">
	<Meta nAMe="AUTHOR" CONTeNt="DPU">
	<MEtA NaMe=CREATED cONtENt="Tahun_Anggaran0817;20400000000000">
	<MeTA NAmE="CHANGEDBY" CoNteNt="isan">
	<META NAme="CHANGED" coNTent="20190320;163019000000000">
	<MEtA nAmE=KSOProductBuildVer CONTEnt=1033-10.1.0.6757>
	<sTyLE>
		@page { size: 8.47in 13.98in; margin-right: 0.88in; margin-top: 0.59in; margin-bottom: 0.69in }
		P { margin-bottom: 0.08in; direction: ltr; color: #000000 }
		P.western { font-family: "Times New Roman", serif; font-size: 12pt; so-language: en-US }
		P.cjk { font-family: "Times New Roman", serif; font-size: 12pt; so-language: en-US }
		P.ctl { font-family: "Times New Roman", serif; font-size: 12pt; so-language: ar-SA }
		H1 { margin-left: 3.5in; margin-top: 0in; margin-bottom: 0in; direction: ltr; color: #000000; text-align: justify; text-decoration: underline }
		H1.western { font-family: "Times New Roman", serif; font-size: 12pt; so-language: en-US }
		H1.cjk { font-family: "Times New Roman", serif; font-size: 12pt; so-language: en-US }
		H1.ctl { font-family: "Times New Roman", serif; font-size: 12pt; so-language: ar-SA }
	</sTYLE>
</heAd>
<bOdY LANg="en-US" tExT=#000000 dIR="LTR">
	<p>
        <Img SRc="<?php print(base_url('assets/template_surat/img/kop_surat_dpu_monokrom.png'));?>" nAme="Picture 8" width="100%" ALIGN=center BORDER=0>
    </p>
	<p LanG=id-ID clASs=western ALIGN=CENTER sTyLe="margin-bottom: 0in">
		<strong>
			KEPUTUSAN PEJABAT PEMBUAT KOMITMEN <br/>
			DINAS PEKERJAAN UMUM KOTA SEMARANG
		</strong>
	</p>
	<p LanG=id-ID clASs=western ALIGN=CENTER sTyLe="margin-bottom: 0in">
		<strong>
			NOMOR : <?php print($kontrak_surat->no_surat); ?>
		</strong>
	</p>
	<p LanG=id-ID clASs=western ALIGN=CENTER sTyLe="margin-bottom: 0in">
		<strong>
			TENTANG <br/>
			PENETAPAN HARGA PERKIRAAN SENDIRI (HPS)
			<?php print($kontrak_pekerjaan->pekerjaan_nama); ?>
		</strong>
	</p>
	<p LanG=id-ID clASs=western ALIGN=CENTER sTyLe="margin-bottom: 0in">
		<strong>
			KEGIATAN <br/>
			<?php print($kontrak_pekerjaan->pekerjaan_nama); ?>
		</strong>
	</p>
	<p LanG=id-ID clASs=western ALIGN=CENTER sTyLe="margin-bottom: 0in">
		<strong>
			PEKERJAAN <br/>
			<?php print($kontrak_pekerjaan->aktivitas_nama); ?>
		</strong>
	</p>
	<p></p>
	<table CELLPADDING="7" cellspacing="0">
        <tr>
            <td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                <strong>Menimbang</strong>
            </td>
            <td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
			<td valign="top" align="justify" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
				<ol type="a">
					<li>Bahwa dalam rangka pelaksanaan Pengadaan Barang/Jasa pada Dinas Pekerjaan Umum Kota Semarang maka perlu ditetapkan Harga Perkiraan Sendri (HPS).</li>
					<li>Bahwa penetapan HPS berdasarkan pada hasil data  survey biaya harga satuan, harga dasar, informasi lain yang dapat dipertanggungjawabkan dan hasil perhitungan tim perencana sebagaimana terlampir dalam keputusan ini.</li>
					<li>Bahwa untuk melaksanakan maksud tersebut diatas, maka perlu diterbitkan Keputusan Pejabat Pembuat Komitmen Dinas Pekerjaan Umum Kota Semarang tahun Anggaran <?php print(date("Y")); ?> tentang Penetapan Harga Perkiraan Sendiri (HPS).</li>
				</ol>
			</td>
        </tr>
		<tr>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
                <strong>Mengingat</strong>
            </td>
            <td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
			<td valign="top" align="justify" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in; display: block;">
				<ol type="1">
					<li style="left: -10px;">Peraturan Presiden Nomor 4 tahun 2015 tentang Perubahan Keempat atas dasar Peraturan Presiden Nomor 54 Tahun 2010 Tentang Pengadaan barang/ Jasa Pemerintah ;</li>
					<li>Peraturan Daerah Kota Semarang Nomor 16 Tahun 2016 tentang Anggaran Pendapatan dan Belanja Daerah Kota Semarang Tahun Anggaran 2017;</li>
					<li>Peraturan Walikota Semarang Nomor 129 Tahun 2016 tentang Penjabaran  Anggaran Pendapatan dan Belanja Daerah Kota Semarang Tahun Anggaran 2017;</li>
					<li>Daftar Pelaksanaan Anggaran (DPA) Dinas Pekerjaan Umum Kota Semarang Tahun Anggaran 2017;</li>
					<li>
						Keputusan Kepala Dinas Pekerjaan Umum Kota Semarang Nomor <?php print((!empty($pejabat_pembuat_komitmen->sk_no))?$pejabat_pembuat_komitmen->sk_no:''); ?> tanggal <?php print((!empty($pejabat_pembuat_komitmen->sk_tgl_penetapan))?tgl_indo($pejabat_pembuat_komitmen->sk_tgl_penetapan):''); ?> tentang Perubahan Penunjukkan Pejabat Pembuat Komitmen dan Pejabat Pelaksana Teknis Kegiatan (PPTK) pada Dinas Pekerjaan Umum Kota Semarang Tahun Anggaran <?php print((!empty($pejabat_pembuat_komitmen->tahun))?$pejabat_pembuat_komitmen->tahun:''); ?>.
					</li>
				</ol>
			</td>
		</tr>
	</table>
	<p LanG=id-ID clASs=western ALIGN="right" sTyLe="margin-bottom: 0in">
		Memutuskan
	</p>
	<p style="page-break-before: always"></p>
	<p LanG=id-ID align="center" clASs=western sTyLe="margin-bottom: 0in">
		<strong>MEMUTUSKAN</strong>
	</p>
	<p LanG=id-ID align="left" clASs=western sTyLe="margin-bottom: 0in;">
		Menetapkan : 
	</p>
	<table CELLPADDING="7" cellspacing="0">
		<tr>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
				PERTAMA
			</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
				Menetapkan Harga Perkiraan Sendiri (HPS) untuk Pelaksanaan:
				<table CELLPADDING="7" cellspacing="0">
					<tr>
						<td valign="top">
							Kegiatan
						</td>
						<td valign="top">:</td>
						<td valign="top">
							<?php print(ucwords(strtolower($kontrak_pekerjaan->pekerjaan_nama))); ?>
						</td>
					</tr>
					<tr>
						<td valign="top">
							Pekerjaan
						</td>
						<td valign="top">:</td>
						<td valign="top">
							<?php print($kontrak_pekerjaan->aktivitas_nama); ?>
						</td>
					</tr>
					<tr>
						<td valign="top">
							Pagu Anggaran
						</td>
						<td valign="top">:</td>
						<td valign="top">
							<?php print('Rp '.format_money($kontrak_pekerjaan->aktivitas_anggaran_awal).',-'); ?>
						</td>
					</tr>
					<tr>
						<td valign="top">
							Harga Perkiraan Sendiri
						</td>
						<td valign="top">:</td>
						<td valign="top">
							<?php print('Rp '.format_money($kontrak_pekerjaan->hps).',-'); ?>
						</td>
					</tr>
					<tr>
						<td valign="top">
							Dengan huruf
						</td>
						<td valign="top">:</td>
						<td valign="top">
							<?php print(ucwords(strtolower(terbilang($kontrak_pekerjaan->hps))).' Rupiah'); ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
				KEDUA
			</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
				Apabila kelak kemudian hari ternyata terdapat kekeliruan dalam keputusan ini akan diubah dan diperbaiki sebagaimana mestinya.
			</td>
		</tr>
		<tr>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
				KETIGA
			</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
			<td valign="top" STYLe="padding-left:500px; font-size:12pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
				Keputusan ini mulai berlaku pada tanggal ditetapkan.
			</td>
		</tr>
	</table>
	<p></p>
	<p></p>
	<table>
		<tr>
			<td width="550"></td>
			<td width="450" align="center" STYLe="padding-left:500px; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">
				<table align="center">
					<tr>
						<td align="left" STYLe="padding-left:500px; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">Ditetapkan di </td>
						<td align="left" STYLe="padding-left:500px; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
						<td align="left" STYLe="padding-left:500px; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">Semarang</td>
					</tr>
					<tr>
						<td align="left" STYLe="padding-left:500px; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">Tanggal</td>
						<td align="left" STYLe="padding-left:500px; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in">:</td>
						<td align="left" STYLe="padding-left:500px; font-size:14pt; padding-top: 0.10in; padding-bottom: 0.10in; padding-left: 0.10in; padding-right: 0.10in"><?php print(tgl_indo($kontrak_surat->tgl_surat)); ?></td>
					</tr>
					<tr>
						<td colspan="3">
							<hr width="200" style="height:3px;border:none;color:#333;background-color:#333;">
						</td>
					</tr>
				</table>
				<br/>

				Pejabat Pembuat Komitmen
				Kegiatan <?php print(ucwords(strtolower($kontrak_pekerjaan->pekerjaan_nama))); ?>
				<br/><br/><br/><br/><br/>
				<u><?php print((!empty($pejabat_pembuat_komitmen->pegawai_nama))?$pejabat_pembuat_komitmen->pegawai_nama:''); ?></u><br/>
                NIP. <?php print((!empty($pejabat_pembuat_komitmen->pegawai_nip))?$pejabat_pembuat_komitmen->pegawai_nip:''); ?>
			</td>
		</tr>
	</table>
	<p></p>
	<p></p>
	<p LanG=id-ID align="left" clASs=western sTyLe="margin-bottom: 0in; font-size:10pt">
		Tembusan disampaikan kepada Yth :
		<ol style="font-size:10pt">
			<li>Inspektur Wilayah Kota Semarang;</li>
			<li>Kepala DPKAD Kota Semarang;</li>
			<li>Kepala Bagian Hukum Setda Kota Semarang;</li>
			<li>Pertinggal</li>
		</ol>
	</p>
	
</BODY>
</HTML>