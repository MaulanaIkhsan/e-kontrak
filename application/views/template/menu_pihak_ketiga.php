<!-- Sidebar Menu -->
<ul class="sidebar-menu" data-widget="tree">
    <li class="header"><div style="text-align: center;">DAFTAR MENU</div> </li>
    <!-- Optionally, you can add icons to the links -->
    <li><a href="<?php print(base_url('beranda')); ?>"><i class="fa fa-home"></i> <span>Beranda</span></a></li>
    <li><a href="<?php print(base_url('kontrak_pekerjaan/resume')); ?>"><i class="fa fa-file-text-o"></i> <span>Kontrak Kegiatan</span></a></li>    
    <li class="treeview">
        <a href="#"><i class="fa fa-database"></i> <span>Master Data</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="<?php print(base_url('pihak_ketiga/redirect_menu/')); ?>"><i class="fa fa-industry"></i> <span>Penyedia Barang/Jasa</span></a></li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-file-text-o"></i> <span>Data Lainnya</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php print(base_url('lainnya/jabatan_perusahaan')); ?>"><i class="fa fa-street-view"></i> <span>Jabatan Perusahaan</span></a></li>
                    <li><a href="<?php print(base_url('lainnya/jenjang_pendidikan')); ?>"><i class="fa fa-graduation-cap"></i> <span>Jenjang Pendidikan</span></a></li>
                    <li><a href="<?php print(base_url('lainnya/jenis_dokumen')); ?>"><i class="fa fa-file-o"></i> <span>Data Jenis Dokumen</span></a></li>
                </ul>
            </li>
        </ul>
        
    </li>
    <li><a href="<?php print(base_url('panduan')); ?>"><i class="fa fa-book"></i> <span>Panduan Pengguna</span></a></li>
    <li><a href="<?php print(base_url('karyawan/profil')); ?>"><i class="fa fa-user-secret"></i> <span>Profil</span></a></li>
    <li><a href="#" data-toggle="modal" data-target="#modal-logout"><i class="fa fa-user"></i> <span>Logout</span></a></li>    
</ul>
<!-- /.sidebar-menu -->