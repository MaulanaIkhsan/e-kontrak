<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumen_template_model extends CI_Model {

    // ----- dokumen_template -----
    // id
    // nama
    // file
    // created_at
    // updated_at

    public function fetch_data($id=NULL)
    {
        $sql = $this->db->select('dokumen_template.id as id,
                                dokumen_template.nama as nama,
                                dokumen_template.file as file')
                        ->from('dokumen_template');

        if($id!=NULL) {
            return $sql->where('dokumen_template.id', $id)
                        ->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }

}

/* End of file Dokumen_template_model.php */
