<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kontrak_dokumen_pihak_ketiga extends CI_Controller {

    /*-----  kontrak_dokumen_pihak_ketiga -----*/
    // id
	// kontrak_pihak_ketiga_id
	// dokumen_pihak_ketiga_id
	// urutan
	// terkoreksi ('y', 'n')
	// created_at
	// updated_at

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $sess_data = $this->session->userdata('session_data');
        if($sess_data===NULL) {
            $this->session->set_flashdata('warning', '<div class="alert alert-warning fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                Harap login terlebih dahulu untuk mengakses sistem</div>');
			redirect('auth');
        }

        $this->load->model('Kontrak_dokumen_pihak_ketiga_model', 'kontrak_dokumen_pihak_ketiga');
        $this->load->model('Generic_model', 'generic');
    }

    public function ajax_add($kontrak_pihak_ketiga_id)
	{
		$this->_validate();

		$data = array(
				'kontrak_pihak_ketiga_id'   => $this->input->post('kontrak_pihak_ketiga_id'),
				'dokumen_pihak_ketiga_id' 	=> $this->input->post('dokumen_pihak_ketiga_id'),
				'urutan'                    => $this->input->post('urutan')
			);
        $insert = $this->generic->insert('kontrak_dokumen_pihak_ketiga', $data);

        if($insert=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => FALSE));
        }
	}

    public function ajax_edit($id)
	{
		$data = $this->kontrak_dokumen_pihak_ketiga->fetch_data($id);
		echo json_encode($data);
	}

	public function ajax_update()
	{
		$this->_validate();
		$data = array(
                'id'                        => $this->input->post('id'),
                'dokumen_pihak_ketiga_id' 	=> $this->input->post('dokumen_pihak_ketiga_id'),
                'urutan'                    => $this->input->post('urutan')
            );

        $key = array('id'=>$this->input->post('id'));
		$update = $this->generic->update('kontrak_dokumen_pihak_ketiga', $data, $key);

        if($update=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => FALSE));
        }
	}
	
	public function update_terkoreksi()
	{
		$list_id = $this->input->post('ids');

		if(!empty($list_id)) {
			$list_id	= array('id' => explode(",", $list_id));
			$data 		= array('terkoreksi' => 'y');
			$update 	= $this->generic->update('kontrak_dokumen_pihak_ketiga', $data, $list_id);

			if($update=='ok') {
				echo json_encode(array("status" => TRUE));
			}
			else {
				echo json_encode(array("status" => FALSE));
			}
		}
		else {
			echo json_encode(array("status" => FALSE));
		}		
	}

    public function ajax_delete($id)
	{
        $key = array('id'=>$id);
		$delete = $this->generic->delete('kontrak_dokumen_pihak_ketiga', $key);

        if($delete=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => FALSE));
        }
    }

    private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('dokumen_pihak_ketiga_id') == '')
		{
			$data['inputerror'][] = 'dokumen_pihak_ketiga_id';
			$data['error_string'][] = 'Dokumen is required';
			$data['status'] = FALSE;
        }

        if($this->input->post('urutan') == '')
		{
			$data['inputerror'][] = 'urutan';
			$data['error_string'][] = 'Urutan is required';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode((object)$data);
			exit();
		}
    }

    public function get_data($kontrak_pihak_ketiga_id)
	{
		$this->load->helper('generic');

		$this->load->model('Pihak_ketiga_model', 'pihak_ketiga');
		$this->load->model('Karyawan_model', 'karyawan');
		$this->load->model('Kontrak_pihak_ketiga_model', 'kontrak_pihak_ketiga');

		$sess_data  = $this->session->userdata('session_data');
        $user_id    = $sess_data['id'];
        $user_role  = $sess_data['role'];
		$user_as    = $sess_data['as'];

		$karyawan 			= NULL;
		$pihak_ketiga_id 	= NULL;
        $karyawan			= $this->karyawan->fetch_data($user_id);

        if(!empty($karyawan)) {
            $pihak_ketiga_id = $karyawan->pihak_ketiga_id;
		}

		$kontrak_pihak_ketiga = $this->kontrak_pihak_ketiga->fetch_data($kontrak_pihak_ketiga_id);

		$list = $this->kontrak_dokumen_pihak_ketiga->get_datatables($kontrak_pihak_ketiga_id);

		$data = array();
        $no = $_POST['start'];

		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = '<input type="checkbox" class="sub_chk" data-id="'.$field->id.'">';
			$row[] = $no;
			if(strlen($field->dokumen_pihak_ketiga_nama)>0) {
                $row[] = "<a href=".base_url('assets/uploads/documents/'.$field->dokumen_pihak_ketiga_file)." target='_blank'>".$field->dokumen_pihak_ketiga_nama."</a>";
            }
            else {
                $row[] = 'Kosong';
            }
			$row[] = $field->urutan;

			if($field->terkoreksi=='y') {
				$row[] = '<div class="label label-success">Sudah</div>';
			}
			else {
				$row[] = '<div class="label label-danger">Belum</div>';
			}

			if(($user_as=='pegawai' and $user_role=='Super Admin') or ($user_as=='pihak_ketiga' and $kontrak_pihak_ketiga->pihak_ketiga_id==$pihak_ketiga_id)) {
				$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_dokumen_pihak_ketiga('."'".$field->id."'".')"> Edit</a>
				<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_dokumen_pihak_ketiga('."'".$field->id."'".')">Delete</a>
        <a class="btn btn-sm btn-primary" href="'.base_url().'kontrak_pihak_ketiga/kontrak_pihak_ketiga_komentar/'.$field->id.'"> Komentar</a>';
			}

      if(($user_as=='pegawai' and $user_role=='Staff') or ($_SESSION['session_data']['as'] == 'pihak_ketiga')) {
				$row[] = '<a class="btn btn-sm btn-primary" href="'.base_url().'kontrak_pihak_ketiga/kontrak_pihak_ketiga_komentar/'.$field->id.'"> Komentar</a>';
			}

			$row[] = "";

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->kontrak_dokumen_pihak_ketiga->count_all($kontrak_pihak_ketiga_id),
			"recordsFiltered" => $this->kontrak_dokumen_pihak_ketiga->count_filtered($kontrak_pihak_ketiga_id),
			"data" => $data,
		);

		// output dalam format JSON
		echo json_encode($output);
	}
}

/* End of file Kontrak_dokumen_pihak_ketiga.php */
