<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai_model extends CI_Model {

    // ----- Pegawai -----
    // id
    // nip
    // nama_lengkap
    // jenis_kelamin
    // alamat
    // no_telepon
    // unit_id
    // role_id
    // username
    // password
    // created_at
    // updated_at

    public function get_nama_pegawai()
    {
        return $this->db->select('id, nama_lengkap')
                        ->from('pegawai')
                        ->where('is_pegawai', 'y')
                        ->order_by('nama_lengkap', 'asc')
                        ->get()->result();
    }

    public function fetch_data($id=NULL)
    {
        $sql = $this->db->select('pegawai.id as id,
                                pegawai.nip as nip,
                                pegawai.nama_lengkap as nama_lengkap,
                                pegawai.jenis_kelamin as jenis_kelamin,
                                pegawai.alamat as alamat, 
                                pegawai.no_telepon as no_telepon,
                                jabatan_fungsional.id as jabatan_id,
                                jabatan_fungsional.nama as jabatan_nama,
                                unit_kerja.id as unit_id,
                                unit_kerja.nama as unit_kerja,
                                role.id as role_id,
                                role.nama as role,
                                pegawai.username as username,
                                pegawai.password as password
                                ')
                        ->from('pegawai')
                        ->join('unit_kerja', 'pegawai.unit_id=unit_kerja.id', 'left')
                        ->join('jabatan_fungsional', 'pegawai.jabatan_id=jabatan_fungsional.id', 'left')
                        ->join('role', 'pegawai.role_id=role.id', 'left')
                        ->order_by('pegawai.nama_lengkap', 'asc');
        if($id!=NULL){
            return $sql->where('pegawai.id', $id)
                        ->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }

    public function check_username($username, $id=NULL, $exclude='n')
    {
        $sql = $this->db->select('username')
                        ->from('pegawai')
                        ->where('username', $username);

        if($id!=NULL) {
            if($exclude=='n'){
                return $sql->where('id', $id)
                            ->get()->row();
            }
            elseif($exclude=='y') {
                return $sql->where('id !=', $id)
                            ->get()->result_array();
            }
        }
        else {
            return $sql->get()
                        ->result();
        }
    }

}

/* End of file Pegawai_model.php */
