<!DOCTYPE html>
<HtmL>

<HeAd>
    <MeTa HTTP-EquIV="CONTENT-TYPE" CONTent="text/html; charset=utf-8">
    <TItlE>Semarang, 29 Maret 2004</tITLE>
    <meta name=GENERATOR coNTeNT="LibreOffice 4.1.6.2 (Linux)">
    <Meta nAMe="AUTHOR" CONTeNt="DPU">
    <MEtA NaMe=CREATED cONtENt="Tahun_Anggaran0817;20400000000000">
    <MeTA NAmE="CHANGEDBY" CoNteNt="isan">
    <META NAme="CHANGED" coNTent="20190320;163019000000000">
    <MEtA nAmE=KSOProductBuildVer CONTEnt=1033-10.1.0.6757>
    <sTyLE>
        @page {
            size: 8.47in 13.98in;
            margin-right: 0.88in;
            margin-top: 0.59in;
            margin-bottom: 0.69in
        }
        P {
            margin-bottom: 0.08in;
            direction: ltr;
            color: #000000
        }
        P.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
        H1 {
            margin-left: 3.5in;
            margin-top: 0in;
            margin-bottom: 0in;
            direction: ltr;
            color: #000000;
            text-align: justify;
            text-decoration: underline
        }
        H1.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }

        .text {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
        }
    </sTYLE>
</heAd>

<bOdY LANg="en-US" tExT=#000000 dIR="LTR">
    <?php
    $dayname = dayname(date('l', strtotime($kontrak_surat->tgl_surat)));
    $tglindo = tgl_indo($kontrak_surat->tgl_surat);
    $split_tgl = explode(' ', $tglindo);
    ?>
    <p ClaSs=western ALIGN=CENTER StyLe="margin-bottom: 0in; page-break-before: none">
        <b>BERITA ACARA</B><br/>
        <U><b>PEMASUKAN / PEMBUKAAN DOKUMEN PENAWARAN</b></u><br/>
        <FoNt sTYle="font-size: 11pt">Nomor : <?php print($kontrak_surat->no_surat); ?></FONT>
    </P>
    
    <P  ALIGN="justify" StylE="margin-bottom: 0in">
    Pada hari ini <?php print($dayname); ?> tanggal <?php print(terbilang($split_tgl[0])); ?>, bulan
    <?php print(strtolower($split_tgl[1])); ?>, tahun <?php print(terbilang($split_tgl[2])); ?> (<?php print($tglindo); ?>) , Pejabat Pengadaan pada Dinas Pekerjaan Umum Kota Semarang Tahun Anggaran <?php print(date("Y")); ?> berdasarkan Keputusan Kepala Dinas Pekerjaan Umum Kota Semarang Selaku Pengguna Anggaran Nomor <?php print($pejabat_pengadaan->sk_no); ?> tanggal <?php print(tgl_indo($pejabat_pengadaan->sk_tgl_penetapan)); ?> tentang Penunjukan Pejabat Pengadaan Pada Dinas Pekerjaan Umum kota Semarang Tahun Anggaran <?php print($pejabat_pengadaan->sk_tahun); ?>, telah mengadakan Rapat Pemasukan / Pembukaan Dokumen Penawaran dalam rangka Pengadaan Langsung:
    </p>
    <p></p>
    <table>
        <tr>
            <td class="text">Program</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->program_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Kegiatan</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->pekerjaan_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Pekerjaan</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->aktivitas_nama); ?></td>
        </tr>
        <tr>
            <td class="text">Sumber Dana</td>
            <td class="text">:</td>
            <td class="text"><?php print($kontrak_pekerjaan->aktivitas_sumber_dana); ?></td>
        </tr>
    </table>

    <P LaNg="pt-BR" CLaSs=western ALIGN=JUSTIFY STylE="margin-bottom: 0in">
    </p>

    <ol type=I>
        <li>
            <strong>PESERTA RAPAT</strong><br/>
            <?php print($pejabat_pengadaan->jenis_pejabat_pengadaan_nama); ?> <?php print($pejabat_pengadaan->seri_pejabat_pengadaan_nama); ?><br/>
            <?php print($kontrak_pekerjaan->pejabat_pengadaan_nama);?><br/>
            <br/>
            Wakil Penyedia Jasa : <br/>
            <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama);?><br/>
            beralamat <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_alamat);?><br/>
        </li>
        <br/>
        <li>
            <strong>HASIL PEMBUKAAN PENAWARAN</strong>
            <ul type="1">
                <li>Perusahaan yang hadir dan memasukkan penawaran Sesuai dengan Surat Undangan Nomor
                <?php print((!empty($surat_undangan[0]->no_surat))?$surat_undangan[0]->no_surat:''); ?>, <?php print((!empty($surat_undangan[0]->tgl_surat))?tgl_indo($surat_undangan[0]->tgl_surat):''); ?>.</li>
                <li>Nilai Penawaran sebesar <?php print('Rp '.format_money($kontrak_pihak_ketiga[0]->nilai_kontrak).',-'); ?></li>
                <li>Jangka Waktu Pelaksanaan <?php print($kontrak_pekerjaan->durasi_kontrak); ?> hari kalender</li>
                <li>Setelah diadakan pembukaan, ternyata penawaran tersebut memenuhi (lengkap)</li>
                <li>Berdasarkan penelitian tersebut diatas, Pembukaan Penawaran ini dinyatakan <strong>Sah</strong></li>
                <li>Hasil Pembukaan Penawaran terlampir.</li>
            </ul>
        </li>
    </ol>
    <p></p>
    Demikian Berita Acara Pemasukan & Pembukaan Dokumen Penawaran ini dibuat dengan sebenarnya
untuk dapat dipergunakan sebagaimana mestinya.
    <p></p>
    <p></p>
    <table>
        <tr>
            <td width="300" align="center" style="font-size:14pt">
                Setuju:<br/>
                <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama);?><br/>
                <br/><br/><br/><br/><br/><br/><br/><br/><br/>
                <u><?php print($direktur_perusahaan);?></u><br/>
                Direktur
            </td>
            <td width="400"></td>
            <td width="300" align="center" style="font-size:14pt">
                <?php print($pejabat_pengadaan->jenis_pejabat_pengadaan_nama); ?> <?php print($pejabat_pengadaan->seri_pejabat_pengadaan_nama); ?><br/>
                Pada Dinas Pekerjaan Umum Kota Semarang<br/>
                Tahun Anggaran <?php print(date("Y"));?><br/>
                <br/><br/><br/><br/><br/>
                <u><?php print($pejabat_pengadaan->pegawai_nama); ?></u><br/>
                NIP. <?php print($pejabat_pengadaan->pegawai_nip); ?>
            </td>
        </tr>
    </table>

</BODY>

</HTML>