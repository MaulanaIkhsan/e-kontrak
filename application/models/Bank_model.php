<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bank_model extends CI_Model {
    // ----- bank -----
    // id
    // nama
    // created_at
    // updated_at
    
    public function fetch_data($id=NULL)
    {
        $sql = $this->db->select('bank.id as id,
                                bank.nama as nama')
                        ->from('bank')
                        ->order_by('bank.nama', 'asc');

        if($id!=NULL){
            return $sql->where('bank.id', $id)
                        ->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }

}

/* End of file Bank_model.php */
