<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Role_model extends CI_Model {

    public $name;

    public function fetch_data($id=NULL)
    {
        $sql = $this->db->select('role.id as id,
                                role.nama as nama')
                        ->from('role');
        
        if(!empty($this->name)) {
            $sql->where('role.nama', $this->name);
        }
        
        if($id!=NULL) {
            return $sql->where('role.id', $id)
                        ->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }

}

/* End of file Role_model.php */
