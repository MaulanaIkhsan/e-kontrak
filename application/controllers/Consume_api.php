<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Consume_api extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		$this->load->library('curl');

		$this->load->helper('url');
		$this->load->helper('generic');

		$this->load->model('Generic_model', 'generic');
	}
	
	public function index()
    {
        print("Hello Consume");
    }

	public function initialize_data($tahun)
	{
		$this->load->model('Urusan_model', 'urusan');
		$this->load->model('Program_model', 'program');
		$this->load->model('Pekerjaan_model', 'pekerjaan');
		$this->load->model('Riwayat_anggaran_model', 'riwayat_anggaran');
		
		// ================== Format Data From Database ==================
		// $data_urusan = array(
		// 	'id'			=>'', 
		// 	'kode_urusan'	=>'', 
		// 	'nama_urusan'	=>'', 
		// 	'tahun'			=>'',
		// 	'uniq_code'		=>''
		// );

		// $data_program = array(
		// 	'id'			=> '',
		// 	'kode_program'	=> '',
		// 	'nama_program'	=> '',
		// 	'tahun'			=> '',
		// 	'uniq_code'		=> '',
		// 	'kode_urusan'  	=> '',
		// );

		// $data_pekerjaan = array(
		// 	'id'				=> '',
		// 	'kode_pekerjaan'	=> '',
		// 	'nama_pekerjaan'	=> '',
		// 	'tahun'				=> '',
		// 	'tahap_pekerjaan'	=> '',
		//  'anggaran_awal'		=> '',
		// 	'uniq_code'			=> '',
		// 	'kode_program'  	=> '',
		// );

		// $data_riwayat_anggaran = array(
		//  'id'				=> '',
		// 	'tahap_pekerjaan'	=> '',
		// 	'anggaran'			=> '',
		// 	'kode_pekerjaan'	=> ''
		// );

		$cache_program_kerja = [
			"urusan"	=> [],
			"program" 	=> [],
			"pekerjaan" => [],
		];

		$temp_program_kerja = $this->session->userdata('cache_data_pekerjaan');
		$curr_year = date("Y");
		if($tahun==$curr_year) {
			if($temp_program_kerja!==NULL){
				$cache_program_kerja = $temp_program_kerja;			
				return $cache_program_kerja;
			}
			elseif ($temp_program_kerja===NULL) {
				$data_urusan 	= (array)$this->urusan->show($tahun);
				$data_program 	= (array)$this->program->show($tahun);
				$data_pekerjaan = (array)$this->pekerjaan->show($tahun);
	
				//---------- Inserting element to cache data ----------
				// Data Urusan
				if((empty($cache_program_kerja['urusan'])) and (!empty($data_urusan))){
					foreach($data_urusan as $item):
						$inserting = item_into_array($cache_program_kerja['urusan'], $item);
						$cache_program_kerja['urusan'] = $inserting;
					endforeach;
				}
				// Data Program
				if((empty($cache_program_kerja['program']) and (!empty($data_program)))){
					foreach($data_program as $item):
						$inserting = item_into_array($cache_program_kerja['program'], $item);
						$cache_program_kerja['program'] = $inserting;
					endforeach;
				}
				// Data Pekerjaan and Riwayat Anggaran
				if((empty($cache_program_kerja['pekerjaan'])) and (!empty($data_pekerjaan))){
					foreach($data_pekerjaan as $item):
						$inserting = item_into_array($cache_program_kerja['pekerjaan'], $item);
						$cache_program_kerja['pekerjaan'] = $inserting;
					endforeach;			
				}
				return $cache_program_kerja;
			}
			else {
				return $cache_program_kerja;
			}
		}
		elseif($tahun!=$curr_year) {
			$data_urusan 	= (array)$this->urusan->show($tahun);
			$data_program 	= (array)$this->program->show($tahun);
			$data_pekerjaan = (array)$this->pekerjaan->show($tahun);

			//---------- Inserting element to cache data ----------
			// Data Urusan
			if((empty($cache_program_kerja['urusan'])) and (!empty($data_urusan))){
				foreach($data_urusan as $item):
					$inserting = item_into_array($cache_program_kerja['urusan'], $item);
					$cache_program_kerja['urusan'] = $inserting;
				endforeach;
			}
			// Data Program
			if((empty($cache_program_kerja['program']) and (!empty($data_program)))){
				foreach($data_program as $item):
					$inserting = item_into_array($cache_program_kerja['program'], $item);
					$cache_program_kerja['program'] = $inserting;
				endforeach;
			}
			// Data Pekerjaan and Riwayat Anggaran
			if((empty($cache_program_kerja['pekerjaan'])) and (!empty($data_pekerjaan))){
				foreach($data_pekerjaan as $item):
					$inserting = item_into_array($cache_program_kerja['pekerjaan'], $item);
					$cache_program_kerja['pekerjaan'] = $inserting;
				endforeach;			
			}

			return $cache_program_kerja;
		}
		else {
			return $cache_program_kerja;
		}

	}
	
    public function consume_data($tahun=null)
	{
		$this->load->model('Skpd_model', 'skpd');
		$this->load->model('Api_model', 'api');
		
		$tahun = date("Y");

		$respon = [];

		$cache_program_kerja = $this->initialize_data($tahun);
		// print('initialize data : <br/>');
		// print_r($cache_program_kerja);
		// print('<br/>==============================================<br/>');
		// print('<br/><br/><br/>');

		// ========================= Consume Data from simanggaran =========================
		$this->db->query('set FOREIGN_KEY_CHECKS = 0;');

		$skpd = $this->skpd->current_skpd();
		$url_get_anggaran = $this->api->get_api_url('Master data kegiatan');

		$tahap = array(
			'Murni' 	 => 1,
			'Pergeseran' => 2,
			'Perubahan'	 => 3
		);

		$err_message = 'Terjadi kesalahan ketika ';
		$success 	 = TRUE;

		foreach($tahap as $key_tahap=>$value):
			$params = array(
				'uid' 		=> 'apis',
				'pass'		=> 'BanjirKanal',
				'tahap' 	=> $value,
				'kode_skpd' => $skpd->kode_skpd,
				'tahun' 	=> $tahun
			);
	
			$kegiatan = $this->curl->simple_post($url_get_anggaran->url, $params);
			$kegiatan = json_decode($kegiatan, TRUE);
	
			if(is_array($kegiatan) and (is_array($kegiatan['result']))) {
				
				foreach($kegiatan['result'] as $item):
					$index_urusan 	 = index_by_column($cache_program_kerja['urusan'],'kode_urusan', $item['kode_urusan']);
					$index_program 	 = index_by_column($cache_program_kerja['program'], 'kode_program', $item['kode_program']);
					$index_pekerjaan = index_by_column($cache_program_kerja['pekerjaan'], 'kode_pekerjaan', $item['kode_kegiatan']);
					
					// print_r($index_urusan);
					// print('<br/>');
					// print_r($index_program);
					// print('<br/>');
					// print_r($index_pekerjaan);
					// print('<br/>');
					// print('Index Urusan : '.$index_urusan.' | '.gettype($index_urusan));

					// Processing Data Urusan
					if(($index_urusan!==FALSE) or ($index_urusan!="0")) {
						$nama_urusan = $cache_program_kerja['urusan'][$index_urusan]['nama_urusan'];
						
						if($nama_urusan!=$item['nama_urusan']){
							$key['kode_urusan'] = $item['kode_urusan'];
							$data = array(
									'nama_urusan' => $item['nama_urusan']
								);
							$update_urusan = $this->generic->update('urusan', $data, $key);

							if($update_urusan=='ok'){
								print('Running 4<br/>');
								$cache_program_kerja['urusan'][$index_urusan]['nama_urusan'] = $item['nama_urusan'];
							}
							else {
								$success = FALSE;
								$err_message = 'update Data Urusan';
								break;
							}
						}	
					}
					elseif(($index_urusan===FALSE) or ($index_urusan=="0")) {
						$uniq_urusan 	= random_code_II(12, 'U');

						$data_urusan = array(
								'id'			=>'', 
								'kode_urusan'	=> $item['kode_urusan'], 
								'nama_urusan'	=> $item['nama_urusan'], 
								'tahun'			=> $tahun,
								'uniq_code'		=> $uniq_urusan
							);
						
						$new_urusan = $this->generic->insert('urusan', $data_urusan);
						if($new_urusan=='ok') {
							$inserting = item_into_array($cache_program_kerja['urusan'], $data_urusan);
							$cache_program_kerja['urusan'] = $inserting;
						}
						else {
							$success = FALSE;
							$err_message = 'insert baru Data Urusan';
							break;
						}
					}
					
					// Processing Data Program
					if(($index_program!==FALSE) or ($index_program!="0")) {
						$nama_program = $cache_program_kerja['program'][$index_urusan]['nama_program'];
						if($nama_program!=$item['nama_program']){
							
							$key = array('kode_program'=> $item["kode_program"]);
							
							$data = array(
									'nama_program' => $item['nama_program']
								);
							$update_program = $this->generic->update('program', $data, $key);
							if($update_program=='ok'){
								$cache_program_kerja['program'][$index_program]['nama_program'] = $item['nama_program'];
							}
							else {
								$success = FALSE;
								$err_message .= 'update Data Program';
								break;
							}
						}	
					}
					elseif(($index_program===FALSE) or ($index_program=="0")) {
						$uniq_program 		= random_code_II(12, 'P');
						$index_kode_urusan 	= index_by_column($cache_program_kerja['urusan'], 'kode_urusan', $item['kode_urusan']);
						$find_kode_urusan 	= $cache_program_kerja['urusan'][$index_kode_urusan]['uniq_code'];
						
						$data_program = array(
							'id'			=> '',
							'kode_program'	=> $item['kode_program'],
							'nama_program'	=> $item['nama_program'],
							'tahun'			=> $tahun,
							'uniq_code'		=> $uniq_program,
							'kode_urusan'  	=> $find_kode_urusan,
						);
						
						$new_program = $this->generic->insert('program', $data_program);
						if($new_program=='ok') {
							$inserting = item_into_array($cache_program_kerja['program'], $data_program);
							$cache_program_kerja['program'] = $inserting;
						}
						else {
							$success = FALSE;
							$err_message .= 'insert baru Data Program';
							break;
						}
					}

					// Processing Data Pekerjaan & Anggaran
					if(($index_pekerjaan!==FALSE) or ($index_program!="0")) {
						$cache_pekerjaan = $cache_program_kerja['pekerjaan'][$index_pekerjaan];
						$nama_pekerjaan	 = $cache_pekerjaan['nama_pekerjaan'];
						
						$data_pekerjaan = [];
						if($nama_pekerjaan!=$item['nama_kegiatan']){
							$data_pekerjaan = array(
								'nama_pekerjaan' => $item['nama_kegiatan']
							);
						}
						if($cache_pekerjaan['tahap_pekerjaan']!=$key_tahap){
							$data_pekerjaan['tahap_pekerjaan'] = $key_tahap;
						}
						if($cache_pekerjaan['anggaran_awal']!=$item['anggaran']){
							$data_pekerjaan['anggaran_awal'] = $item['anggaran'];
						}

						$key = array(
							'kode_pekerjaan' => $item['kode_kegiatan'],
							'tahun' 		 => $tahun
						);
						if(!empty($data_pekerjaan)) {
							$update_pekerjaan = $this->generic->update('pekerjaan', $data_pekerjaan, $key);

							if($update_pekerjaan=='ok'){
								$cache_pekerjaan['nama_pekerjaan'] 	 = $item['nama_kegiatan'];
								$cache_pekerjaan['tahap_pekerjaann'] = $key_tahap;
								$cache_pekerjaan['anggaran_awal'] 	 = $item['anggaran'];										
							}
							else {
								$success = FALSE;
								$err_message .= 'update Data Pekerjaan';
								break;
							}
						}

						// Data Riwayat Anggaran
						$index_kode_pekerjaan 	= index_by_column($cache_program_kerja['pekerjaan'], 'kode_pekerjaan', $item['kode_kegiatan']);
						$find_kode_pekerjaan  	= $cache_program_kerja['pekerjaan'][$index_kode_pekerjaan]['uniq_code'];
						$anggaran 		 	 	= $this->riwayat_anggaran->get_riwayat_anggaran($find_kode_pekerjaan, $key_tahap);
						$data_riwayat_anggaran 	= [];
						$key_riwayat_anggaran 	= [];

						if($anggaran===NULL){
							// Insert new riwayat anggaran
							$data_riwayat_anggaran = array(
								'tahap_pekerjaan'	=> $key_tahap,
								'anggaran'			=> $item['anggaran'],
								'kode_pekerjaan'	=> $find_kode_pekerjaan
							);
							$new_riwayat_anggaran = $this->generic->insert('riwayat_anggaran', $data_riwayat_anggaran);

							if($new_riwayat_anggaran!='ok'){
								$success = FALSE;
								$err_message .= 'insert Data Riwayat Anggaran Update data pekerjaan';
								break;
							}
						}
						
						elseif(($anggaran!==NULL) and ($anggaran->anggaran!=$item['anggaran'])){
							$data_riwayat_anggaran = array(
								'anggaran'			=> $item['anggaran'],
							);

							$key_riwayat_anggaran = array(
								'tahap_pekerjaan'	=> $key_tahap,
								'kode_pekerjaan'	=> $find_kode_pekerjaan
							);

							$update_riwayat_anggaran = $this->generic->update('riwayat_anggaran', $data_riwayat_anggaran, $key_riwayat_anggaran);

							if($update_riwayat_anggaran!='ok'){
								$success = FALSE;
								$err_message .= 'update Data Riwayat Anggaran update data pekerjaan';
								break;
							}
							
						}

					}
					elseif(($index_pekerjaan===FALSE) or ($index_program=="0")) {
						$index_kode_program = index_by_column($cache_program_kerja['program'], 'kode_program', $item['kode_program']);
						$find_kode_program 	= $cache_program_kerja['program'][$index_kode_program]['uniq_code'];
						$uniq_pekerjaan 	= random_code_II(12, 'p');

						$data_pekerjaan = array(
								'id'				=> '',
								'kode_pekerjaan'	=> $item['kode_kegiatan'],
								'nama_pekerjaan'	=> $item['nama_kegiatan'],
								'tahun'				=> $tahun,
								'tahap_pekerjaan'	=> $key_tahap,
								'anggaran_awal'		=> $item['anggaran'],
								'uniq_code'			=> $uniq_pekerjaan,
								'kode_program'  	=> $find_kode_program,
							);
						$new_pekerjaan = $this->generic->insert('pekerjaan', $data_pekerjaan);
						
						// Insert new riwayat anggaran
						$data_riwayat_anggaran = array(
							'tahap_pekerjaan'	=> $key_tahap,
							'anggaran'			=> $item['anggaran'],
							'kode_pekerjaan'	=> $uniq_pekerjaan
						);
						$new_riwayat_anggaran = $this->generic->insert('riwayat_anggaran', $data_riwayat_anggaran);

						if(($new_pekerjaan=='ok') and ($new_riwayat_anggaran=='ok')) {
							$inserting = item_into_array($cache_program_kerja['pekerjaan'], $data_pekerjaan);
							$cache_program_kerja['pekerjaan'] = $inserting;
						}
						else {
							$success = FALSE;
							$err_message .= 'insert baru Data Pekerjaan';
							break;
						}
					}
										
				endforeach;
			}
		endforeach;

		if(($success===TRUE) && ($tahun==date("Y"))){
			$this->session->set_userdata('cache_data_pekerjaan', $cache_program_kerja);
		}

		$respon = array('status'=> $success, 'error_message'=>$err_message);
		return $respon;
	}

	public function cron_consume_data()
	{
		$tahun = date("Y");
		$consume_data_program_kerja = $this->consume_data($tahun);
		print('<br/>');
		print('time : '.date('d-m-Y -|- H:i:s').'<br/>');
		print('status : '.$consume_data_program_kerja['status'].'<br/>');
		if($consume_data_program_kerja['status']===FALSE) {
			print('error message : '.$consume_data_program_kerja['error_message'].'<br/>');
		}
		print('=======================================================<br/>');
		print('<br/>');
	}

	public function consume_simanggaran()
	{
		if($this->input->post('consume')!==NULL) {
			// ================== Data structure of kegiatan from Simanggaran ==================
			// [0] => Array ( 
			// [id] 			=> 1 
			// [kode_skpd] 		=> 1.1.03.01 
			// [kode_urusan] 	=> 1.1.03 
			// [nama_urusan]	=> URUSAN WAJIB PELAYANAN DASAR PEKERJAAN UMUM DAN PENATAAN RUANG 
			// [kode_program]  	=> 1.1.03.1.1.03.01.01 
			// [nama_program]  	=> PROGRAM PELAYANAN ADMINISTRASI PERKANTORAN 
			// [kode_kegiatan] 	=> 1.1.03.1.1.03.01.01.001 
			// [nama_kegiatan] 	=> PENYEDIAAN JASA SURAT MENYURAT 
			// [anggaran] 		=> 210000000)

			$cache_program_kerja = '';
			$sess_program_kerja  = $this->session->userdata('cache_program_kerja');
			$tahun 				 = $this->input->post('tahun');

			$consume_data_program_kerja = $this->consume_data($tahun);
			
			// print_r($consume_data_program_kerja);
			// die;

			// $this->consume_data($tahun);
			// print_r($consume_data_program_kerja['data']);
			if($consume_data_program_kerja['status']==TRUE) {
				// if($tahun!=$curr_year) {
				// 	$this->session->set_userdata('cache_program_kerja', $cache_pekerjaan);
				// }
				$this->session->set_flashdata('success', '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert">&times;</a>Consume data pekerjaan dari Simanggaran berhasil dilakukan</div>');
				redirect('program');
				// print('<br/>Success</br>');
			}
			elseif($consume_data_program_kerja['status']==FALSE) {
				$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a>'.$consume_data_program_kerja['error_message'].'. Harap hubungi bagian administrator !</div>');
				redirect('program');
			}
			else {
				$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Consume data kegiatan dari Simanggaran unconditional status. Harap hubungi bagian administrator !</div>');
				redirect('beranda');
			}

			$this->db->query('set FOREIGN_KEY_CHECKS = 1;');
		} 
		else {
			$this->session->set_flashdata('alert', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert">&times;</a> Harap lakukan proses consume data melalui menu <strong>Consume Data</strong> !</div>');
			redirect('beranda');
		}
	}

	public function sample()
	{
		$this->load->model('Skpd_model', 'skpd');
		$this->load->model('Api_model', 'api');
		
		if ($tahun==null) {
			$tahun = date("Y");
		}

		$skpd = $this->skpd->current_skpd();
		$url_get_anggaran = $this->api->get_api_url('Master data kegiatan');

		$tahap = array(
			'Murni' 	 => 1,
			'Pergeseran' => 2,
			'Perubahan'	 => 3
		);

		$err_message = 'Terjadi kesalahan ketika ';
		$success 	 = TRUE;

		foreach($tahap as $key_tahap=>$value):
			$params = array(
				'uid' 		=> 'apis',
				'pass'		=> 'BanjirKanal',
				'tahap' 	=> $value,
				'kode_skpd' => $skpd->kode_skpd,
				'tahun' 	=> $tahun
			);

			$kegiatan = $this->curl->simple_post($url_get_anggaran->url, $params);
			$kegiatan = json_decode($kegiatan, TRUE);

			print_r($kegiatan);
			die;
		endforeach;
	}
}

/* End of file Consume_api.php */
