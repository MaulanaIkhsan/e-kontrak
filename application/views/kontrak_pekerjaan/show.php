<?php
$this->load->view('template/header');?>

<?php $this->load->view('template/asset_header');?>
<link rel="stylesheet" type="text/css" href="<?php print(base_url('assets/bower_components/datatables.net-bs/css/custom_button_export.css'))?>" />

<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <?php 
        print($this->session->flashdata('alert'));
        print($this->session->flashdata('success')); 
        
        $sess_data  = $this->session->userdata('session_data');
        $user_id    = $sess_data['id'];
        $role       = $sess_data['role'];
        ?>
        <div class="col-md-12">
            <!-- Horizontal Form -->
          <div class="box box-warning">
              <div class="box-header with-border">
                <?php if($flag=='resume') { ?>
                  <h3 class="box-title">Resume Kontrak Kegiatan</h3>
                <?php } else { ?>
                  <h3 class="box-title">Data Kontrak Kegiatan</h3>
                <?php } ?>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <div class="box-body">
                <table id="tabel_kontrak" class="table table-bordered table-striped table-responsive">
                    <thead>
                        <tr class="headings" align="center">
                            <th class="column-title" align="center">No</th>
                            <th class="column-title" align="center">Nama Pekerjaan</th>
                            <th class="column-title" align="center">Tahun</th>
                            <th class="column-title" align="center">Pejabat Pengadaan</th>
                            <th class="column-title" align="center">Tgl Awal Kontrak</th>
                            <th class="column-title" align="center">Tgl Akhir Kontrak</th>
                            <th class="column-title" align="center">HPS</th>
                            <th class="column-title" align="center">Status</th>
                            <th class="column-title" align="center">Aksi</th>
                        </tr>
                    </thead>
                  <tbody></tbody>
                </table>
              </div> 

            </div>
          </div>

        </div>
    </section>
</div>
<?php $this->load->view('template/asset_footer');?>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/dataTables.buttons.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.flash.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/jszip.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/pdfmake.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/vfs_fonts.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.html5.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.print.min.js'))?>"></script>

<script>
    var tabel_kontrak;

  $(function(){
    $.fn.dataTable.ext.errMode = 'none';
    
    tabel_kontrak = $('#tabel_kontrak').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
            "ajax": {
                "url": "<?php echo site_url('kontrak_pekerjaan/get_data/');?>",
                "type": "POST"
            },
            dom: 'lBfrtip',
            button:[{
              extend:'pdf',
              oriented:'potrait',
              pageSize: 'A4',
              title : 'Data Dokumen Kontrak Pekerjaan',
              download : 'open'
            }, 'copy', 'csv', 'excel', 'pdf', 'print'],
            "columnDefs": [{ 
                "targets": [ 0 ], 
                "orderable": false, 
            }]
        });
  });


</script>


<?php $this->load->view('template/footer');?>