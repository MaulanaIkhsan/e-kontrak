<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Urusan_model extends CI_Model {
	
	// ----- Urusan -----
	// id
	// kode_urusan
	// nama_urusan
	// tahun
	// uniq_code
	// created_at
	// updated_at

	var $main_table = 'urusan';

	public function check_data_urusan($kode_urusan, $tahun_urusan)
	{
		$sql = $this->db->select('kode_urusan, nama_urusan, tahun')
						->from($this->main_table)
						->where('kode_urusan', $kode_urusan)
						->where('tahun', $tahun_urusan)
						->get();
		if($sql->num_rows()==0){
			return FALSE;
		}
		else {
			return $sql->row();
		}
	}

	public function check_nama_urusan($kode_urusan, $nama_urusan, $tahun_urusan)
	{
		$sql = $this->check_data_urusan($kode_urusan, $tahun_urusan);

		if($sql->nama_urusan !== $nama_urusan){
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	public function update($data, $key)
	{
		$sql = $this->db->where($key)
						->update($this->main_table, $data);
		if($sql){
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	public function insert($data)
	{
		$this->load->helper('generic');
		
		$nama_urusan = $data['nama_urusan'];
		$kode_urusan = $data['kode_urusan'];
		$tahun_urusan = $data['tahun'];

		$check_urusan = $this->check_data_urusan($kode_urusan, $tahun_urusan);
		if($check_urusan!==FALSE) {

			$check_nama_urusan = $this->check_nama_urusan($kode_urusan, $nama_urusan, $tahun_urusan);
			if($check_nama_urusan==FALSE) {
				//update nama urusan
				$data = [];
				$data['nama_urusan'] = $nama_urusan;
				$key['kode_urusan'] = $kode_urusan;
				$update = $this->update($data, $key);
				if ($update=='ok') {
					return 'ok';
				}
				else {
					return 'fail';
				}
			}//check nama_urusan
			else {
				return 'exist';
			}
		} //check_urusan
		else {
			$data['uniq_code'] = random_unique_code(12, 'U');

			//input data urusan baru
			$sql = $this->db->insert($this->main_table, $data);
			if($sql){
				return 'ok';
			}
			else {
				return 'fail';
			}
		}

	}

	public function get_urusan_by_uniq_code($kode_urusan, $nama_urusan, $tahun)
	{
		return $this->db->select('uniq_code')
						->from($this->main_table)
						->where('kode_urusan', $kode_urusan)
						->where('nama_urusan', $nama_urusan)
						->where('tahun', $tahun)
						->get()->row();
	}
	
	public function show($tahun=null)
	{
		$sql = $this->db->select('id, kode_urusan, nama_urusan, tahun, uniq_code')
						->from($this->main_table);
		
		if($tahun!=null){
			$sql->where('tahun', $tahun);
			return $sql->get()->result_array();
		}
		else {
			return $sql->get()->result_array();
		}
	}

	

	public function show_update($key)
	{
		return $this->db->select('id, kode_urusan, nama_urusan')
						->from($this->main_table)
						->where('id', $key)
						->get()->row();
	}

	public function delete($key)
	{
		$sql = $this->db->delete($this->main_table, $key);
		if ($sql) {
			return 'ok';
		}
		else{
			return 'fail';
		}
	}
}

/* End of file Urusan_model.php */
/* Location: ./application/models/Urusan_model.php */