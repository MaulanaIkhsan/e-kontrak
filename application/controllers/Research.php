<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Research extends CI_Controller {

		
		public function __construct()
		{
			parent::__construct();
			//Do your magic here
			$sess_data = $this->session->userdata('session_data');
      if($sess_data===NULL) {
				$this->session->set_flashdata('warning', '<div class="alert alert-warning fade in">
						<a href="#" class="close" data-dismiss="alert">&times;</a> 
						Harap login terlebih dahulu untuk mengakses sistem</div>');
				redirect('auth');
      }
		}
		
    public function mpdf()
    {
			$pdf_1 = './assets/uploads/documents_karyawan/file20190212jWFPMDrVJn110442.pdf';
			$pdf_2 = base_url('assets/uploads/documents_karyawan/file20190212opQJdXffBl110442.pdf');;
			
			$documents = array(
				'./assets/uploads/documents_karyawan/file20190212jWFPMDrVJn110442.pdf',
				'./assets/uploads/documents_karyawan/file20190212opQJdXffBl110442.pdf'
			);

			$mpdf = new \Mpdf\Mpdf();
			// $mpdf->AddPage();

			// // Import the last page of the source PDF file
			// $pagecount = $mpdf->setSourceFile($pdf_1);
			// $template_id = $mpdf->ImportPage($pagecount);
			// $mpdf->UseTemplate($template_id);

			// $pagecount = $mpdf->setSourceFile($documents);
			
			$filestotal = sizeof($documents);
			$filenumber = 1;
			$mpdf->SetImportUse();

			foreach($documents as $file):
				if(file_exists($file)){
					// Get all pages in current file
					$pageInFile = $mpdf->SetSourceFile($file);

					for ($flag=1; $flag<=$pageInFile ; $flag++) { 
						$pageId = $mpdf->ImportPage($flag);

						$page = $mpdf->getTemplateSize($pageId);

						if($flag==1) {
							$mpdf->state=0;
							$mpdf->UseTemplate($pageId);
						}
						else {
							$mpdf->state = 1;
							$mpdf->AddPage($page['w']>$page['h']?'L':'P');
							$mpdf->UseTemplate($pageId);
						}
					}
				}
			endforeach;

			foreach($documents as $file):
				if(file_exists($file)){
					// Get all pages in current file
					$pageInFile = $mpdf->SetSourceFile($file);

					for ($flag=1; $flag<=$pageInFile ; $flag++) { 
						$pageId = $mpdf->ImportPage($flag);

						$page = $mpdf->getTemplateSize($pageId);

						if($flag==1) {
							$mpdf->state=0;
							$mpdf->UseTemplate($pageId);
						}
						else {
							$mpdf->state = 1;
							$mpdf->AddPage($page['w']>$page['h']?'L':'P');
							$mpdf->UseTemplate($pageId);
						}
					}
				}
			endforeach;
			
			$mpdf->output();

			unset($mpdf);
    }

		public function dropdown()
		{
			$this->load->model('Unit_kerja_model', 'unit_kerja');
			
			$data['unit_kerja'] = $this->unit_kerja->fetch_data();
			$this->load->view('research/content', $data);
		}

		public function mpdf_overwrite()
		{
			$this->load->model('Kontrak_pekerjaan_model', 'kontrak_pekerjaan');
			$this->load->model('Dokumen_template_model', 'dokumen_template');
			
			
			$documents = [
				['name'=>'file_1', 'file'=>'./assets/uploads/documents_template/9b2e2-template-pakta-integritas.pdf'],
				['name'=>'file_1', 'file'=>'./assets/uploads/documents_template/142ef-template-berita-acara-pembukaan-dokumen-penawaran.pdf'],
				['name'=>'cover', 'file'=>'./assets/uploads/documents_template/dba95-template-cover.pdf']
			];

			

			// $documents = array(
			// 	'./assets/uploads/documents_karyawan/file20190212jWFPMDrVJn110442.pdf',
			// 	'./assets/uploads/documents_karyawan/file20190212opQJdXffBl110442.pdf',
			// 	'./assets/uploads/documents_template/acddf-template-cover.pdf'
			// );

			$mpdf = new \Mpdf\Mpdf(['format'=>'Legal']);
			$mpdf->setImportUse();

			// foreach($documents as $item):
			// 	if(file_exists($item['file'])){
			// 		// Get all pages in current file
			// 		$pageInFile = $mpdf->SetSourceFile($item['file']);

			// 		for ($flag=1; $flag<=$pageInFile ; $flag++) { 
			// 			$pageId = $mpdf->ImportPage($flag);

			// 			$page = $mpdf->getTemplateSize($pageId);

			// 			if($item['name']=='cover' and $flag==1) {
			// 				$mpdf->state=0;
			// 				$mpdf->UseTemplate($pageId);

			// 				$stylesheet = file_get_contents('./assets/my_custom/css/mpdf-style.css');
			// 				$mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
			// 				$mpdf->WriteHTML("<span style='color:white;'>init</span>", \Mpdf\HTMLParserMode::HTML_BODY);
			// 				$mpdf->WriteHTML('<p id="cover-tgl">08-01-2019</p>', \Mpdf\HTMLParserMode::HTML_BODY);
			// 				$mpdf->WriteHTML('<p id="cover-nama-program">PROGRAM PENGEMBANGAN TEKNOLOGI</p>', \Mpdf\HTMLParserMode::HTML_BODY);
			// 			}
			// 			elseif($item['name']=='cover' and $flag!=1){
			// 				$mpdf->state = 1;
			// 				$mpdf->AddPage($page['w']>$page['h']?'L':'P');
			// 				$mpdf->UseTemplate($pageId);
			// 			}
			// 		}
			// 	}
			// endforeach;
			$template_dokumen = $this->dokumen_template->fetch_data();
			// foreach($template_dokumen as $item):
			// 	$template = './assets/uploads/documents_template/'.$item->file;
			// 	print('template : '.$template);
				
			// 	if(file_exists($template)){
			// 		// Get all pages in current file
			// 		$pageInFile = $mpdf->SetSourceFile($item->file);

			// 		for ($flag=1; $flag<=$pageInFile ; $flag++) { 
			// 			$pageId = $mpdf->ImportPage($flag);

			// 			$page = $mpdf->getTemplateSize($pageId);

			// 			$dokumen_name = strtolower($item->nama);

			// 			switch ($dokumen_name) {
			// 				case 'cover':
			// 					if($flag==1) {
			// 						$mpdf->state=0;
			// 						$mpdf->UseTemplate($pageId);

			// 						$stylesheet = file_get_contents('./assets/my_custom/css/mpdf-style.css');
			// 						$mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
			// 						$mpdf->WriteHTML("<span style='color:white;'>init</span>", \Mpdf\HTMLParserMode::HTML_BODY);
			// 						$mpdf->WriteHTML('<p id="cover-tgl">08-01-2019</p>', \Mpdf\HTMLParserMode::HTML_BODY);
			// 						$mpdf->WriteHTML('<p id="cover-nama-program">PROGRAM PENGEMBANGAN TEKNOLOGI</p>', \Mpdf\HTMLParserMode::HTML_BODY);
			// 					}
			// 					else {
			// 						$mpdf->state = 1;
			// 						$mpdf->AddPage($page['w']>$page['h']?'L':'P');
			// 						$mpdf->UseTemplate($pageId);
			// 					}
			// 					break;
							
			// 			}
			// 		}
			
			// 	}
			// endforeach;
				$documents = [];
				if(!empty($template_dokumen)) {
					foreach($template_dokumen as $item):
						$data = [];
						$data['nama'] = $item->nama;
						$data['file'] = './assets/uploads/documents_template/'.$item->file;
						
						array_push($documents, $data);
					endforeach;
				}
				

			foreach($documents as $item):
				// $template = (string)'./assets/uploads/documents_template/'.$item['file'];
				// print(gettype($template).'<br/>');
				if(file_exists($item['file'])){
					// Get all pages in current file
					$pageInFile = $mpdf->SetSourceFile($item['file']);

					for ($flag=1; $flag<=$pageInFile ; $flag++) { 
						$pageId = $mpdf->ImportPage($flag);
						$page = $mpdf->getTemplateSize($pageId);
						$dokumen_name = strtolower($item['nama']);

						switch ($dokumen_name) {
							case 'cover':
								if($flag==1) {
									$mpdf->state=0;
									$mpdf->UseTemplate($pageId);

									$stylesheet = file_get_contents('./assets/my_custom/css/mpdf-style.css');
									$mpdf->WriteHTML($stylesheet, \Mpdf\HTMLParserMode::HEADER_CSS);
									$mpdf->WriteHTML("<span style='color:white;'>init</span>", \Mpdf\HTMLParserMode::HTML_BODY);
									$mpdf->WriteHTML('<p id="cover-tgl">08-01-2019</p>', \Mpdf\HTMLParserMode::HTML_BODY);
									$mpdf->WriteHTML('<p id="cover-nama-program">PROGRAM PENGEMBANGAN TEKNOLOGI</p>', \Mpdf\HTMLParserMode::HTML_BODY);
								}
								else {
									$mpdf->state = 1;
									$mpdf->AddPage($page['w']>$page['h']?'L':'P');
									$mpdf->UseTemplate($pageId);
								}
								break;
							
							case 'pakta integritas':
								if($flag==1) {
									$mpdf->state=0;
									$mpdf->UseTemplate($pageId);
								}
								else{
									$mpdf->state = 1;
									$mpdf->AddPage($page['w']>$page['h']?'L':'P');
									$mpdf->UseTemplate($pageId);
								}
								break;
						}
					
					}
				}
			endforeach;

			// $sample_page = $this->load->view('research/pdf_file', [], true);
			// $mpdf->writeHTML($sample_page);
			
			$mpdf->output();

			unset($mpdf);
		}

		public function get_pegawai()
		{
			$unit_id = $this->input->post('unit_kerja');

			$datas = $this->db->select('pegawai.id as id,
																pegawai.nama_lengkap as nama_lengkap')
												->from('pegawai')
												->where('pegawai.unit_id', $unit_id)
												->get()->result_array();
			
			if(count($datas)>0){
				print json_encode($datas);
			}
			else {
				print json_encode(array('status'=>FALSE));
			} 
		}

		public function array_operation()
		{
			$list_pekerjaan = ['Fisik', 'Jasa Konsultan', 'Jasa Lainnya', 'Pengadaan Barang'];
			$list_pengadaan = ['Lelang', 'Pengadaan Langsung', 'Penunjukan Langsung', 'Tender', 'Tender Cepat'];

			// Check value contained in array
			$value = 'Sembarang';

			print(in_array($value, $list_pekerjaan));

			$jenis_surat 	= ['cover', 'undangan_pengadaan', 'SPK', 'SPMK', 'BAHPL', 'daftar_hadir'];
			$inserted_surat = ['cover', 'undangan_pengadaan', 'daftar_hadir', 'daftar_hadir'];

			print('<br/>');
			print_r(array_diff($jenis_surat, $inserted_surat));
			print('<br/>');
			print('qty : '.$count_daftar_hadir);

			$bio = array('name'=>'name_value', 'age'=>'age_value');

			foreach($bio as $key=>$value):
				print($key.'|'.$value.'<br/>');
			endforeach;
		}

		public function password_encrypt()
		{
			$value = 'dpusmg123';
			$options = ['cost' => 13];
			$encrypt = password_hash($value, PASSWORD_BCRYPT, $options);
			$decrypt = password_verify($value, $encrypt);
			print($encrypt.' | '.$decrypt);
			print('<br/>');
			print('simojan:'.md5('k4nnt012ku_PU'));
			//
		}

		public function data_kontrak()
		{
			$this->load->model('Kontrak_pekerjaan_model', 'kontrak_pekerjaan');
			
			$list = $this->kontrak_pekerjaan->get_datatables();
			print_r($list);
		}

		public function generate_user_pihak_ketiga()
		{
			$this->load->model('Pihak_ketiga_model', 'pihak_ketiga');
			$this->load->model('Generic_model', 'generic');
			
			/*----- karyawan -----*/
			// id
			// nama_lengkap
			// jabatan_id
			// jenjang_pendidikan_id
			// pihak_ketiga_id
			// jenis_kelamin (Pria, Wanita)
			// alamat_lengkap
			// tgl_lahir
			// no_telepon
			// email
			// dokumen_profil
			// dokumen_pendidikan
			// dokumen_lainnya
			// username
			// password
			// is_user (y, n)

			//----- pihak_ketiga -----
			// id
			// nama_perusahaan
			// npwp
			// alamat_lengkap
			// no_telepon
			// email
			// status ('Cabang', 'Pusat')
			// alamat_pusat
			// no_telepon_pusat
			// email_pusat
			// created_at
			// updated_at
		
			$pihak_ketiga = $this->pihak_ketiga->fetch_data();
			
			foreach($pihak_ketiga as $item):
				$nama_karyawan = str_replace(' ', '', strtolower($item->nama_perusahaan));
				$nama_karyawan = str_replace('.','', $nama_karyawan);

				$email 		= $nama_karyawan.'@mail.com';
				$username = 'admin@'.$nama_karyawan;
				
				$value 		= 'dpusmg123';
				$options 	= ['cost' => 13];
				$encrypt_password = password_hash($value, PASSWORD_BCRYPT, $options);

				$karyawan = array(
						'nama_lengkap'					=> $nama_karyawan,
						'jabatan_id'						=> 4,
						'jenjang_pendidikan_id'	=> 3,
						'pihak_ketiga_id'				=> $item->id,
						'jenis_kelamin'					=> 'Pria',
						'alamat_lengkap'				=> 'Semarang',
						'tgl_lahir'							=> '1990-01-01',
						'no_telepon'						=> '12345',
						'email'									=> $email,
						'dokumen_profil' 				=> '',
						'dokumen_pendidikan'		=> '',
						'dokumen_lainnya'				=> '',
						'username'							=> $username,
						'password'							=> $encrypt_password,
						'is_user'								=> 'y'
					);
				$insert = $this->generic->insert('karyawan', $karyawan);
				if($insert=='ok') {
					print('ok - '.$item->nama_perusahaan.'<br/>');
				}
			endforeach;
			
		}

		public function surat_kontrak()
		{
			$mpdf = new \Mpdf\Mpdf(['format'=>'legal']);
		
			$filenumber = 1;
			// $data['nama_pekerjaan'] = 'Peningkatan Jalan Woltermonginsidi';
			// $views = $this->load->view('ngoprek/template_surat', $data, TRUE);
			
			// $mpdf->SetImportUse();
			// $mpdf->WriteHTML($views);
			// $mpdf->output();

			$template = array('bahpl', 
											'berita_acara_buka_penawaran',
											'berita_acara_nego',
											'cover',
											'daftar_hadir_nego',
											'daftar_hadir_pembukaan_penawaran',
											'ldp',
											'pakta_integritas',
											'penetapan_pemenang',
											'penyampaian_pemenang',
											'sk_hps',
											'spmk',
											'surat_penunjukan_penyedia',
											'surat_pesanan',
											'undangan_pengadaan'									
										);
			
			$mpdf->SetImportUse();
			$views = $this->load->view('ngoprek/template_surat', [], TRUE);
			$mpdf->AddPage();
			$mpdf->WriteHTML($views);

			// $data['template_surat'] = $template
			// foreach($template as $item):
			// 	$location="template_surat_penawaran/$item";
			// 	$views = $this->load->view($location, [], TRUE);
			// 	$mpdf->AddPage();
			// 	$mpdf->WriteHTML($views);
			// 	// print($item.'<br/>');
			// endforeach;

			
				// $views = $this->load->view('template_surat_penawaran/bahpl', [], TRUE);
				// $mpdf->AddPage();
				// $mpdf->WriteHTML($views);

				// $views = $this->load->view('template_surat_penawaran/berita_acara_buka_penawaran', [], TRUE);
				// $mpdf->AddPage();
				// $mpdf->WriteHTML($views);

				// $views = $this->load->view('template_surat_penawaran/cover', $data, TRUE);
				// $mpdf->AddPage();
				// $mpdf->WriteHTML($views);

				// $views = $this->load->view('template_surat_penawaran/daftar_hadir_nego', $data, TRUE);
				// $mpdf->AddPage();
				// $mpdf->WriteHTML($views);

				// $views = $this->load->view('template_surat_penawaran/daftar_hadir_pembukaan_penawaran', $data, TRUE);
				// $mpdf->AddPage();
				// $mpdf->WriteHTML($views);

				// $views = $this->load->view('template_surat_penawaran/ldp', $data, TRUE);
				// $mpdf->AddPage();
				// $mpdf->WriteHTML($views);

				// ================================================================================

				// $views = $this->load->view('template_surat_penawaran/bahpl', [], TRUE);
				// $mpdf->AddPage();
				// $mpdf->WriteHTML($views);

				// $views = $this->load->view('template_surat_penawaran/berita_acara_buka_penawaran', $data, TRUE);
				// $mpdf->AddPage();
				// $mpdf->WriteHTML($views);

				// $views = $this->load->view('template_surat_penawaran/cover', $data, TRUE);
				// $mpdf->AddPage();
				// $mpdf->WriteHTML($views);

				// $views = $this->load->view('template_surat_penawaran/daftar_hadir_nego', $data, TRUE);
				// $mpdf->AddPage();
				// $mpdf->WriteHTML($views);

				// $views = $this->load->view('template_surat_penawaran/daftar_hadir_pembukaan_penawaran', $data, TRUE);
				// $mpdf->AddPage();
				// $mpdf->WriteHTML($views);

				// $views = $this->load->view('template_surat_penawaran/ldp', $data, TRUE);
				// $mpdf->AddPage();
				// $mpdf->WriteHTML($views);

				// $views = $this->load->view('template_surat_penawaran/pakta_integritas', $data, TRUE);
				// $mpdf->AddPage();
				// $mpdf->WriteHTML($views);

				// $views = $this->load->view('template_surat_penawaran/penetapan_pemenang', $data, TRUE);
				// $mpdf->AddPage();
				// $mpdf->WriteHTML($views);

				// $views = $this->load->view('template_surat_penawaran/penyampaian_pemenang', $data, TRUE);
				// $mpdf->AddPage();
				// $mpdf->WriteHTML($views);

				// $views = $this->load->view('template_surat_penawaran/sk_hps', $data, TRUE);
				// $mpdf->AddPage();
				// $mpdf->WriteHTML($views);

				// $views = $this->load->view('template_surat_penawaran/spmk', $data, TRUE);
				// $mpdf->AddPage();
				// $mpdf->WriteHTML($views);
				
				// $views = $this->load->view('template_surat_penawaran/surat_penunjukan_penyedia', $data, TRUE);
				// $mpdf->AddPage();
				// $mpdf->WriteHTML($views);

				// $views = $this->load->view('template_surat_penawaran/surat_pesanan', $data, TRUE);
				// $mpdf->AddPage();
				// $mpdf->WriteHTML($views);

				// $views = $this->load->view('template_surat_penawaran/undangan_pengadaan', $data, TRUE);
				// $mpdf->AddPage();
				// $mpdf->WriteHTML($views);
				
				// $mpdf->output();
			
			$mpdf->output();
			unset($mpdf);
			// $this->load->view('ngoprek/template_surat');
		}

		public function editor_form()
		{
			$this->load->view('ngoprek/editor_form');
			
		}

		public function dayname()
		{
			$this->load->helper('generic');
			
			$tgl = 'Kosong';
			print(dayname(date('l', strtotime($tgl))));

			// Short if condition
			print('<br/>');
			print(($tgl!='Kosong')?tgl_indo($tgl):$tgl);
			// print(($item->durasi_kontrak!=NULL)?$item->durasi_kontrak.' hari':'Kosong');
			$data = '';
			print('<br/>');
			print('isnull : '.is_null($data));
		}
	
	public function datatable_client()
	{
		$this->load->model('Aktivitas_model', 'aktivitas');

		$sess_data = $this->session->userdata('session_data');
		$user_id 	= $sess_data['id'];
		$user_role 	= $sess_data['role'];

		$data['aktivitas_kontrak'] = $this->aktivitas->aktivitas_kontrak($user_id, $user_role);
		// print($this->db->last_query().'<br/>');
		// die;
		$this->load->view('ngoprek/datatable_client', $data);
		
	}

	public function last_id()
	{
		$this->load->model('Generic_model', 'generic');
		
		$last_id = $this->generic->get_last_id('kontrak_pekerjaan');
		print_r($last_id);
	}

	public function send_email()
	{
		$this->load->library('email');
		
		$config = array(
			'useragent'	=> 'CodeIgniter',
			'protocol' 	=> 'smtp',
			'smtp_crypto'	=> 'tls',
			'smtp_host'	=> 'smtp.gmail.com',
			'smtp_port'	=> 465,
			'smtp_timeout'	=> 300,
			'smtp_user'	=> 'maulanaikhsan1995@gmail.com',
			'smtp_pass'	=> '21j4nuary1995',
			'mailtype'	=> 'html',
			'charset'	=> 'utf-8',
			'smtp_wordwrap'	=> TRUE,
			'validate'	=> FALSE,
			'priority'	=> 3,
			'crlf'		=> '\r\n',
			'newline'	=> '\r\n',
			'bcc_batch_mode'	=> FALSE,
			'bcc_batch_size'	=> 200
			
		);
		$this->email->initialize($config);

		$this->email->from('maulanaikhsan1995@gmail.com');
		$this->email->to('maulanaikhsan0195@gmail.com');

		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');

		// var_dump($this->email->send());

		if($this->email->send()) {
			print('success sent');
		}
		else {
			print('fail sent');
		}
	}

	public function csv_import()
	{
		if(!empty($this->input->post('upload'))) {
			$this->load->helper('generic');
			
			$csvData = csv_reader($_FILES['csv_file']['tmp_name']);

			/*--- Struktur column data aktivitas ---*/
			// pekerjaan_kode
			// pekerjaan_nama
			// pekerjaan_tahun
			// aktivitas_nama
			// anggaran_awal
			// perubahan_anggaran
			// hps
			// nilai_kontrak
			// tgl_pengajuan_pencairan
			// tgl_pencairan
			// keterangan
			// pptk_nama
			// pptk_unit_kerja

			if(!empty($csvData)) {
				// foreach($csvData as $data):
				// 	for($item=0; $item<count($data); $item++){
				// 		print($data[$item]);
				// 		print('<br/>');
				// 	}
				// 	print('------------------------------<br/>');
				// endforeach;

				// $month = 'JUN';
				// $filtering = array_filter($csvData, function($var){
				// 	return ($var['kode_pekerjaan']==$month);
				// });

				// print_r($filtering);
			}

			$this->load->model('Aktivitas_model', 'aktivitas');
			
			$this->aktivitas->tahun_pekerjaan = date('Y');
			$aktivitas 		 = object_to_array($this->aktivitas->fetch_data());

			// print_r($aktivitas);
			$kode_pekerjaan = '1.1.03.1.1.03.01.06.006';
			$filtering = array_filter($aktivitas, function($var) use ($kode_pekerjaan) {
				return ($var['kode_pekerjaan']==$kode_pekerjaan);
			});

			print_r($filtering);
		}
		else {
			$this->load->view('ngoprek/import_data');
			
		}
	}
}

/* End of file Research.php */
