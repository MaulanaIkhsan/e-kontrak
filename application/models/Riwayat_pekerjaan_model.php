<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Riwayat_pekerjaan_model extends CI_Model {

    /*----- riwayat_pekerjaan_pihak_ketiga -----*/
    // id
    // pihak_ketiga_id
    // nama_pekerjaan
    // bidang_pekerjaan
    // lokasi_kerja
    // pemberi_tugass
    // no_kontrak
    // tgl_kontrak
    // nilai_kontrak
    // tgl_progress_terakhir
    // presentase_terakhir

    var $column_order = array(null, 'riwayat_pekerjaan_pihak_ketiga.id', 
                    'pihak_ketiga.id',
                    'pihak_ketiga.nama_perusahaan', 
                    'riwayat_pekerjaan_pihak_ketiga.nama_pekerjaan', 
                    'riwayat_pekerjaan_pihak_ketiga.bidang_pekerjaan', 
                    'riwayat_pekerjaan_pihak_ketiga.lokasi_kerja',
                    'riwayat_pekerjaan_pihak_ketiga.pemberi_tugas',
                    'riwayat_pekerjaan_pihak_ketiga.no_kontrak',
                    'riwayat_pekerjaan_pihak_ketiga.tgl_kontrak',
                    'riwayat_pekerjaan_pihak_ketiga.nilai_kontrak',
                    'riwayat_pekerjaan_pihak_ketiga.tgl_progress_terakhir',
                    'riwayat_pekerjaan_pihak_ketiga.presentase_terakhir'); 

    var $column_search = array('riwayat_pekerjaan_pihak_ketiga.id', 
                    'pihak_ketiga.id',
                    'pihak_ketiga.nama_perusahaan', 
                    'riwayat_pekerjaan_pihak_ketiga.nama_pekerjaan', 
                    'riwayat_pekerjaan_pihak_ketiga.bidang_pekerjaan', 
                    'riwayat_pekerjaan_pihak_ketiga.lokasi_kerja',
                    'riwayat_pekerjaan_pihak_ketiga.pemberi_tugas',
                    'riwayat_pekerjaan_pihak_ketiga.no_kontrak',
                    'riwayat_pekerjaan_pihak_ketiga.tgl_kontrak',
                    'riwayat_pekerjaan_pihak_ketiga.nilai_kontrak',
                    'riwayat_pekerjaan_pihak_ketiga.tgl_progress_terakhir',
                    'riwayat_pekerjaan_pihak_ketiga.presentase_terakhir');

    // default order 
    var $order = array('riwayat_pekerjaan_pihak_ketiga.id' => 'asc');

    public function fetch_data($id=NULL)
    {
        $sql = $this->db->select('riwayat_pekerjaan_pihak_ketiga.id as id, 
                            pihak_ketiga.id as pihak_ketiga_id,
                            pihak_ketiga.nama_perusahaan as nama_perusahaan, 
                            riwayat_pekerjaan_pihak_ketiga.nama_pekerjaan as nama_pekerjaan, 
                            riwayat_pekerjaan_pihak_ketiga.bidang_pekerjaan as bidang_pekerjaann, 
                            riwayat_pekerjaan_pihak_ketiga.lokasi_kerja as lokasi_kerja,
                            riwayat_pekerjaan_pihak_ketiga.pemberi_tugas as pemberi_tugas,
                            riwayat_pekerjaan_pihak_ketiga.no_kontrak as no_kontrak,
                            riwayat_pekerjaan_pihak_ketiga.tgl_kontrak as tgl_kontrak,
                            riwayat_pekerjaan_pihak_ketiga.nilai_kontrak as nilai_kontrak,
                            riwayat_pekerjaan_pihak_ketiga.tgl_progress_terakhir as tgl_progress_terakhir,
                            riwayat_pekerjaan_pihak_ketiga.presentase_terakhir as presentase_terakhir')
                        ->join('pihak_ketiga', 'riwayat_pekerjaan_pihak_ketiga.pihak_ketiga_id=pihak_ketiga.id', 'left');

        if($id!=NULL){
            return $sql->where('riwayat_pekerjaan_pihak_ketiga.id', $id)
                        ->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }
    
    /*====== Serverside Datatable ======*/
	private function _get_datatables_query($pihak_ketiga_id)
	{
        $this->db->select('riwayat_pekerjaan_pihak_ketiga.id as id, 
                        pihak_ketiga.id as pihak_ketiga_id,
                        pihak_ketiga.nama_perusahaan as nama_perusahaan, 
                        riwayat_pekerjaan_pihak_ketiga.nama_pekerjaan as nama_pekerjaan, 
                        riwayat_pekerjaan_pihak_ketiga.bidang_pekerjaan as bidang_pekerjaann, 
                        riwayat_pekerjaan_pihak_ketiga.lokasi_kerja as lokasi_kerja,
                        riwayat_pekerjaan_pihak_ketiga.pemberi_tugas as pemberi_tugas,
                        riwayat_pekerjaan_pihak_ketiga.no_kontrak as no_kontrak,
                        riwayat_pekerjaan_pihak_ketiga.tgl_kontrak as tgl_kontrak,
                        riwayat_pekerjaan_pihak_ketiga.nilai_kontrak as nilai_kontrak,
                        riwayat_pekerjaan_pihak_ketiga.tgl_progress_terakhir as tgl_progress_terakhir,
                        riwayat_pekerjaan_pihak_ketiga.presentase_terakhir as presentase_terakhir');

        $this->db->from('riwayat_pekerjaan_pihak_ketiga');
        $this->db->join('pihak_ketiga', 'riwayat_pekerjaan_pihak_ketiga.pihak_ketiga_id=pihak_ketiga.id', 'left');
        $this->db->where('riwayat_pekerjaan_pihak_ketiga.pihak_ketiga_id', $pihak_ketiga_id);

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($pihak_ketiga_id)
	{
		$this->_get_datatables_query($pihak_ketiga_id);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($pihak_ketiga_id)
	{
		$this->_get_datatables_query($pihak_ketiga_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($pihak_ketiga_id)
	{
        $this->db->select('riwayat_pekerjaan_pihak_ketiga.id as id, 
        pihak_ketiga.id as pihak_ketiga_id,
        pihak_ketiga.nama_perusahaan as nama_perusahaan, 
        riwayat_pekerjaan_pihak_ketiga.nama_pekerjaan as nama_pekerjaan, 
        riwayat_pekerjaan_pihak_ketiga.bidang_pekerjaan as bidang_pekerjaann, 
        riwayat_pekerjaan_pihak_ketiga.lokasi_kerja as lokasi_kerja,
        riwayat_pekerjaan_pihak_ketiga.pemberi_tugas as pemberi_tugas,
        riwayat_pekerjaan_pihak_ketiga.no_kontrak as no_kontrak,
        riwayat_pekerjaan_pihak_ketiga.tgl_kontrak as tgl_kontrak,
        riwayat_pekerjaan_pihak_ketiga.nilai_kontrak as nilai_kontrak,
        riwayat_pekerjaan_pihak_ketiga.tgl_progress_terakhir as tgl_progress_terakhir,
        riwayat_pekerjaan_pihak_ketiga.presentase_terakhir as presentase_terakhir');

        $this->db->from('riwayat_pekerjaan_pihak_ketiga');
        $this->db->join('pihak_ketiga', 'riwayat_pekerjaan_pihak_ketiga.pihak_ketiga_id=pihak_ketiga.id', 'left');
        $this->db->where('riwayat_pekerjaan_pihak_ketiga.pihak_ketiga_id', $pihak_ketiga_id);

		return $this->db->count_all_results();
	}
	/*====== /Serverside Datatable ======*/
}

/* End of file Riwayat_pekerjaan_model.php */
