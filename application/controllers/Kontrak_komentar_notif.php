<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kontrak_komentar_notif extends CI_Controller {

    // ----- kontrak_komentar -----
    // id
    // file_id
    // pegawai_id
    // karyawan_id
    // komentar
    // created_at
    // updated_at
    // notif_status

    // ----- kontrak_komentar_notif -----
    // id
    // file_id
    // pegawai_id
    // pihak_ketiga_id
    // notif_status('new', 'read')

    
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $sess_data = $this->session->userdata('session_data');
        if($sess_data===NULL) {
            $this->session->set_flashdata('warning', '<div class="alert alert-warning fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a> 
                Harap login terlebih dahulu untuk mengakses sistem</div>');
			redirect('auth');
        }
    }
    
    
    public function read_notif($file_id)
    {
        $this->load->model('Generic_model', 'generic');
        
        $sess_data  = $this->session->userdata('session_data');
        $user_id    = $sess_data['id'];
        $role       = $sess_data['role'];
        $as         = $sess_data['as'];

        $data['notif_status'] = 'read';
        $key = array('file_id' => $file_id);
        if($as=='pihak_ketiga') {
            $key['pihak_ketiga_id'] = $sess_data['pihak_ketiga_id'];
        } elseif($as=='pegawai') {
            $key['pegawai_id is not NULL'] = NULL;
        } else {
            print('<h1>Unknown Condition</h1>');
            die;
        }

        $update = $this->generic->update('kontrak_komentar_notif', $data, $key);

        if($update=='ok') {
            redirect('kontrak_pihak_ketiga/kontrak_pihak_ketiga_komentar/'.$file_id);
            
        }
    }

}

/* End of file Kontrak_komentar_notif.php */
