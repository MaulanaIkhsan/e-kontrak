<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kontrak_pekerjaan extends CI_Controller {

    /*----- kontrak_pekerjaan -----*/
    // id
	// aktivitas_id
	// pejabat_pembuat_komitmen_id 
	// pejabat_pengadaan_id 
	// tgl_awal_kontrak
	// tgl_akhir_kontrak
	// hps
	// harga_negosiasi
	// status (Pembukaan, Penawaran, Penetapan, Penunjukan)
	// created_at
    // updated_at
    
    var $status_kontrak = ['Pembukaan', 'Penawaran', 
						'Penetapan', 'Penunjukan'];

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $sess_data = $this->session->userdata('session_data');
        if($sess_data===NULL) {
            $this->session->set_flashdata('warning', '<div class="alert alert-warning fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a> 
                Harap login terlebih dahulu untuk mengakses sistem</div>');
			redirect('auth');
        }

        $this->load->model('Kontrak_pekerjaan_model', 'kontrak_pekerjaan');
        $this->load->model('Generic_model', 'generic');
    }
	
	public function index()
	{
		$this->session->unset_userdata('id_aktivitas');
		$this->session->unset_userdata('kontrak_pekerjaan_id');
		$this->session->unset_userdata('resume_kontrak_pihak_ketiga');
		
		$data['flag'] = 'index';

		$this->load->view('kontrak_pekerjaan/show', $data);
	}

	public function resume()
	{
		$this->load->model('Karyawan_model', 'karyawan');
		
		$sess_data = $this->session->userdata('session_data');
		$user_id = $sess_data['id'];
		$user_as = $sess_data['as'];
		$user_role = $sess_data['role'];
		
		
		if($user_as=='pihak_ketiga') {
			$karyawan = $this->karyawan->fetch_data($user_id);
			$pihak_ketiga_id = NULL;

			if(!empty($karyawan)) {
				$pihak_ketiga_id = $karyawan->pihak_ketiga_id;
			}

			$this->session->set_userdata('resume_kontrak_pihak_ketiga', TRUE);
			$this->session->set_userdata('pihak_ketiga_id', $pihak_ketiga_id);
		}

		$this->session->unset_userdata('id_aktivitas');
		$this->session->unset_userdata('kontrak_pekerjaan_id');
		
		$array = array(
			'key' => 'value'
		);
		
		$data['flag'] = 'resume';

		$this->load->view('kontrak_pekerjaan/show', $data);
	}
    
    public function ajax_add($aktivitas_id)
	{
		$this->_validate($aktivitas_id);
		$this->load->helper('generic');

		$this->load->model('Aktivitas_model', 'aktivitas');
		
		$data = array(
				'aktivitas_id'                  => $aktivitas_id,
				// 'pejabat_pembuat_komitmen_id' 	=> $this->input->post('pejabat_pembuat_komitmen'),
				'pejabat_pengadaan_id' 	        => $this->input->post('pejabat_pengadaan'),
				'tgl_awal_kontrak' 	            => format_date($this->input->post('tgl_awal_kontrak'), 1),
				'tgl_akhir_kontrak' 		    => format_date($this->input->post('tgl_akhir_kontrak'), 1),
				'hps' 	                        => $this->input->post('hps'),
				// 'harga_negosiasi'				=> $this->input->post('harga_negosiasi'),
                'status' 	                    => $this->input->post('status'),
			);

		//Update data aktivitas
		$aktivitas 		= $this->aktivitas->detail($aktivitas_id);
		$key_aktivitas 	= ['id'=>$aktivitas_id];
		$data_aktivitas	= array(
			'hps' 			=> $this->input->post('hps'),
			'nilai_kontrak' => $this->input->post('harga_negosiasi')
		);
		$update_aktivitas = $this->generic->update('aktivitas', $data_aktivitas, $key_aktivitas);

		$insert = $this->generic->insert('kontrak_pekerjaan',$data);
        
        if($insert=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => FALSE));
        }
	}

    public function ajax_edit($id)
	{
		$data = $this->kontrak_pekerjaan->fetch_data($id);
		echo json_encode($data);
	}

	public function ajax_update()
	{
		$id = $this->input->post('id');
		$kontrak_pekerjaan = $this->kontrak_pekerjaan->fetch_data($id);
		$this->_validate($kontrak_pekerjaan->aktivitas_id);

		$data = array(
                // 'pejabat_pembuat_komitmen_id' 	=> $this->input->post('pejabat_pembuat_komitmen'),
                'pejabat_pengadaan_id' 	        => $this->input->post('pejabat_pengadaan'),
                'tgl_awal_kontrak' 	            => $this->input->post('tgl_awal_kontrak'),
                'tgl_akhir_kontrak' 		    => $this->input->post('tgl_akhir_kontrak'),
				'hps' 	                        => $this->input->post('hps'),
                'status' 	                    => $this->input->post('status'),
			);
		
		if(!empty($this->input->post('harga_negosiasi'))) {
			$data['harga_negosiasi'] = $this->input->post('harga_negosiasi');
		}
		
		//Update data aktivitas
		$aktivitas 		= $this->aktivitas->detail($kontrak_pekerjaan->aktivitas_id);
		$key_aktivitas 	= ['id'=>$kontrak_pekerjaan->aktivitas_id];
		$data_aktivitas	= array(
			'hps' 			=> $this->input->post('hps'),
			'nilai_kontrak' => $this->input->post('harga_negosiasi')
		);
		$update_aktivitas = $this->generic->update('aktivitas', $data_aktivitas, $key_aktivitas);	

        $key = array('id'=>$id);
		$update = $this->generic->update('kontrak_pekerjaan', $data, $key);
        
        if($update=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => FALSE));
        }
    }
    
    public function ajax_delete($id)
	{
        $key = array('id'=>$id);
		$delete = $this->generic->delete('kontrak_pekerjaan', $key);
        
        if($delete=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => FALSE));
        }
    }
    
    private function _validate($aktivitas_id=NULL)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		// if($this->input->post('pejabat_pembuat_komitmen') == '')
		// {
		// 	$data['inputerror'][] = 'pejabat_pembuat_komitmen';
		// 	$data['error_string'][] = 'Pejabat Pembuat Komitmen is required';
		// 	$data['status'] = FALSE;
		// }

		if($this->input->post('pejabat_pengadaan') == '')
		{
			$data['inputerror'][] = 'pejabat_pengadaan';
			$data['error_string'][] = 'Pejabat Pengadaan is required';
			$data['status'] = FALSE;
		}

		// if($this->input->post('tgl_awal_kontrak') == '')
		// {
		// 	$data['inputerror'][] = 'tgl_awal_kontrak';
		// 	$data['error_string'][] = 'Tgl Awal Kontrak is required';
		// 	$data['status'] = FALSE;
		// }

		// if($this->input->post('tgl_akhir_kontrak') == '')
		// {
		// 	$data['inputerror'][] = 'tgl_akhir_kontrak';
		// 	$data['error_string'][] = 'Tanggal Akhir Kontrak is required';
		// 	$data['status'] = FALSE;
        // }
        
        if($this->input->post('hps') == '')
		{
			$data['inputerror'][] = 'hps';
			$data['error_string'][] = 'HPS is required';
			$data['status'] = FALSE;
		}

		if($this->input->post('harga_negosiasi') != '' and $this->input->post('harga_negosiasi') > $this->input->post('hps'))
		{
			$data['inputerror'][] = 'harga_negosiasi';
			$data['error_string'][] = 'Harga negosiasi harus kurang dari atau sama dengan HPS';
			$data['status'] = FALSE;
		}
		
		if($aktivitas_id!=NULL and $this->input->post('hps')!='NULL')
		{
			$this->load->model('Aktivitas_model', 'aktivitas');
			$aktivitas = $this->aktivitas->detail($aktivitas_id);

			if(!empty($aktivitas_id)){
				$anggaran_awal 		= $aktivitas->anggaran_awal;
				$perubahan_anggaran = $aktivitas->perubahan_anggaran;
				$hps 				= $this->input->post('hps');

				if($perubahan_anggaran>=1 and $hps>$perubahan_anggaran) {
					$data['inputerror'][] = 'hps';
					$data['error_string'][] = 'nilai HPS harus lebih kecil  dari Perubahan Anggaran';
					$data['status'] = FALSE;
				}
				elseif($perubahan_anggaran<=0 and $hps>$anggaran_awal) {
					$data['inputerror'][] = 'hps';
					$data['error_string'][] = 'nilai HPS harus lebih kecil  dari Anggaran Awal';
					$data['status'] = FALSE;
				}
			}
			
		}
        
        if($this->input->post('status') == '')
		{
			$data['inputerror'][] = 'status';
			$data['error_string'][] = 'Status is required';
			$data['status'] = FALSE;
		}
        
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	public function detail($kontrak_id)
	{
		$this->load->model('Pegawai_model', 'pegawai');
		$this->load->model('Pejabat_pengadaan_model', 'pejabat_pengadaan');
		$this->load->model('Jenis_surat_model', 'jenis_surat');
		$this->load->model('Pihak_ketiga_model', 'pihak_ketiga');
		$this->load->model('Karyawan_model', 'karyawan');
		$this->load->model('Bank_model', 'bank');
		$this->load->model('Tempat_rapat_model', 'tempat_rapat');
		$this->load->model('Kontrak_pihak_ketiga_model', 'kontrak_pihak_ketiga');
		$this->load->model('Pejabat_pembuat_komitmen_model', 'pejabat_pembuat_komitmen');	
		$this->load->model('Kontrak_surat_penawaran_model', 'kontrak_surat_penawaran');
		$this->load->model('Kontrak_pihak_ketiga_model', 'kontrak_pihak_ketiga');	
		$this->load->model('Kontrak_dokumen_pihak_ketiga_model', 'kontrak_dokumen_pihak_ketiga');
		
		$this->load->helper('generic');
		
		$sess_data 	= $this->session->userdata('session_data');
		$user_id    = $sess_data['id'];
        $user_role  = $sess_data['role'];
        $user_as    = $sess_data['as'];
		
		$data['detail']	= $this->kontrak_pekerjaan->fetch_data($kontrak_id);
		$curr_year 		= $data['detail']->pekerjaan_tahun;

		//set session kontrak_pekerjaan_id		
		$this->session->set_userdata('kontrak_pekerjaan_id', $kontrak_id);
		
		$data['id'] 						= $kontrak_id;
		$data['user'] 						= $this->pegawai->get_nama_pegawai();
		$data['pejabat_pengadaan']			= $this->pejabat_pengadaan->fetch_data(NULL, $curr_year, NULL);
		$data['pejabat_pembuat_komitmen']	= $this->pejabat_pembuat_komitmen->fetch_data(NULL, $curr_year, NULL);
		$data['status_kontrak'] 			= $this->status_kontrak;
		$data['jenis_surat']				= $this->jenis_surat->fetch_data();
		$data['pihak_ketiga']				= $this->pihak_ketiga->fetch_data();
		$data['bank']						= $this->bank->fetch_data();
		$data['tempat_rapat']				= $this->tempat_rapat->fetch_data();
		$data['kontrak_pihak_ketiga']		= $this->kontrak_pihak_ketiga->fetch_data(NULL, $kontrak_id, NULL);
		$data['contained']					= FALSE;
		$data['is_ppkom']					= FALSE;
		$data['jml_kontrak_surat'] 			= FALSE;
		$data['status_dokumen']				= FALSE;

		if($user_as=='pihak_ketiga') {
			
			$karyawan = $this->karyawan->fetch_data($user_id);
			if(!empty($karyawan)) {
				$data['pihak_ketiga_id'] = $karyawan->pihak_ketiga_id;
				$data['pihak_ketiga_nama'] = $karyawan->pihak_ketiga_nama;				
			}
						
		}

		/*--- Check PPKOM and filter jenis_surat ---*/
		// PPKOM
		$list_jenis_surat = [];
		if($user_role!='Super Admin' and $user_as=='pegawai' and $data['detail']->pejabat_pembuat_komitmen_pegawai_id==$user_id) {
			$data['is_ppkom'] = TRUE;
			$list_jenis_surat = ['SPK', 'SPMK', 'SPVendor'];
			
			$data['jenis_surat'] = $this->jenis_surat->fetch_data(NULL, $list_jenis_surat);
		}
		//Pejabat Pengadaan
		elseif($user_role!='Super Admin' and $user_as=='pegawai' and $data['detail']->pejabat_pengadaan_pegawai_id==$user_id) {
			$list_jenis_surat = ['cover', 'UPengadaan', 'PI', 'BABP', 'DHN', 'BAENK', 'BHPL', 'DPPemenang', 'DSampaiP', 'LDP', 'SKHPS', 'SPesanan', 'SPK', 'SPMK', 'SPVendor'];
			
			$data['jenis_surat'] = $this->jenis_surat->fetch_data(NULL, $list_jenis_surat);
		}

		$kontrak_pihak_ketiga = $this->kontrak_pihak_ketiga->fetch_data(NULL, $kontrak_id, NULL);
		if(count($kontrak_pihak_ketiga)>=1) {
			$data['contained'] = TRUE;

			// Check qty of dokumen_kontrak_pihak_ketiga which terkoreksi is 'n'
			$jml_dokumen = 0;
			foreach($kontrak_pihak_ketiga as $item):
				$kontrak_dokumen = $this->kontrak_dokumen_pihak_ketiga->fetch_data(NULL, $item->id);
				if(!empty($kontrak_dokumen)) {
					foreach($kontrak_dokumen as $value):
						if($value->terkoreksi=='n') {
							$jml_dokumen++;
						}
					endforeach;
				}
			endforeach;

		}

		$kontrak_surat_penawaran = $this->kontrak_surat_penawaran->fetch_data(NULL, $kontrak_id);
		if(count($kontrak_surat_penawaran)>=1) {
			$data['jml_kontrak_surat'] = TRUE;
		}

		$this->load->view('kontrak_pekerjaan/detail', $data);
	}

	public function data_kontrak_pihak_ketiga($kontrak_pekerjaan_id)
	{
		$this->load->model('Kontrak_pihak_ketiga_model', 'kontrak_pihak_ketiga');
		return $this->kontrak_pihak_ketiga->fetch_data(NULL, $kontrak_pekerjaan_id, NULL);
	}

	public function surat_kontrak($kontrak_pekerjaan_id, $kode_surat)
	{
		$this->load->model('Kontrak_surat_penawaran_model', 'kontrak_surat_penawaran');
		return $this->kontrak_surat_penawaran->fetch_data(NULL, $kontrak_pekerjaan_id, $kode_surat);
	}

	public function rekap_data_kontrak()
	{
		$tahun = $this->input->post('tahun');
		// $tahun = date("Y");
		$group_by = $this->input->post('group_by');

		if($tahun!=NULL) {
			$this->load->helper('generic');
			$this->load->model('Kontrak_pekerjaan_model');
			
			$sess_data 	= $this->session->userdata('session_data');
			$user_id 	= $sess_data['id'];
			$user_as	= $sess_data['as'];
			$user_role	= $sess_data['role'];

			// group by Pejabat Pengadaan
			if(!empty($group_by) and $group_by=='pejabat_pengadaan') {
				// print('group by data pejabat pengadaan');
				$this->kontrak_pekerjaan->group_by = 'pejabat_pengadaan';
				
				$data['kontrak_pekerjaan'] = $this->kontrak_pekerjaan->rekap_data_kontrak($user_id, $user_role, $tahun);
				$data['controller'] = $this;
				$data['tahun']		= $tahun;

				// print_r($this->db->last_query());
				$this->load->view('kontrak_pekerjaan/rekap_data_group', $data);
				
			}
			else {
				$data_kontrak_pekerjaan=$this->kontrak_pekerjaan->rekap_data_kontrak($user_id, $user_role, $tahun);

				$data['kontrak_pekerjaan'] = $data_kontrak_pekerjaan;
				$data['controller'] = $this;
				$data['tahun']		= $tahun;

				$this->load->view('kontrak_pekerjaan/rekap_data', $data);
			}

			
		}
		else {
			print('<h1>Access Denied</h1>');
		}
	}

	public function get_data($aktivitas_id=NULL)
	{
		$this->load->helper('generic');
		$this->load->model('Kontrak_pihak_ketiga_model', 'kontrak_pihak_ketiga');
		
		$sess_data  = $this->session->userdata('session_data');
		$user_id    = $sess_data['id'];
		$role       = $sess_data['role'];
		$user_as	= $sess_data['as'];

		$list_kontrak_id = [''];
		
		if($user_as=='pegawai'){
			$list = $this->kontrak_pekerjaan->get_datatables($aktivitas_id, $user_id, $role);
		}
		elseif ($user_as=='pihak_ketiga') {
			$is_resume 		 = $this->session->userdata('resume_kontrak_pihak_ketiga');
			$pihak_ketiga_id = $this->session->userdata('pihak_ketiga_id');

			if($is_resume!=NULL and $pihak_ketiga_id!=NULL) {
				$this->load->model('Kontrak_pihak_ketiga_model', 'kontrak_pihak_ketiga');
				
				$list_kontrak_pekerjaan = [''];
				$kontrak_pihak_ketiga = $this->kontrak_pihak_ketiga->fetch_data(NULL, NULL, $pihak_ketiga_id);
				if(!empty($kontrak_pihak_ketiga)) {
					foreach($kontrak_pihak_ketiga as $item):
						array_push($list_kontrak_pekerjaan, $item->kontrak_pekerjaan_id);
					endforeach;
				}
				
				$list 			= $this->kontrak_pekerjaan->get_datatables(NULL, NULL, NULL, $list_kontrak_pekerjaan);
				$aktivitas_id		= NULL;
				$user_id 			= NULL;
				$role 				= NULL;
				$list_kontrak_id 	= $list_kontrak_pekerjaan;
				// print('running resume<br/>');
			}
			else {
				$list = $this->kontrak_pekerjaan->get_datatables();
				$aktivitas_id		= NULL;
				$user_id 			= NULL;
				$role 				= NULL;
				$list_kontrak_id 	= NULL;
			}
			
		}

		// print($this->db->last_query());
		// die;
		
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->nama_aktivitas;
			$row[] = $field->pekerjaan_tahun;
			$row[] = $field->pegawai_nama_lengkap;
			$row[] = format_date($field->tgl_awal_kontrak, 2);
            $row[] = format_date($field->tgl_akhir_kontrak, 2);
            $row[] = format_money($field->hps);
			$row[] = $field->status;
			
			if($user_as=='pegawai') {
				$row[] = "<a class='btn btn-primary' href=".base_url('kontrak_pekerjaan/detail/'.$field->id).">Detail</a>";
			}
			elseif($user_as='pihak_ketiga') {
				$kontrak_pihak_ketiga = $this->kontrak_pihak_ketiga->fetch_data(NULL, $field->id, NULL);

				if(!empty($kontrak_pihak_ketiga)) {
					$row[] = "<a class='btn btn-primary' href=".base_url('kontrak_pihak_ketiga/detail/'.$kontrak_pihak_ketiga[0]->id).">Detail</a>";
				}
			}
			
			$row[] = "";

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->kontrak_pekerjaan->count_all($aktivitas_id, $user_id, $role, $list_kontrak_id),
			"recordsFiltered" => $this->kontrak_pekerjaan->count_filtered($aktivitas_id, $user_id, $role, $list_kontrak_id),
			"data" => $data,
		);
		// output dalam format JSON
		echo json_encode($output);
	}

	public function download_dokumen_kontrak($kontrak_pekerjaan_id)
	{
		ob_start();

		$this->load->model('Kontrak_dokumen_lainnya_model', 'kontrak_dokumen_lainnya');
		$this->load->model('Kontrak_pihak_ketiga_model', 'kontrak_pihak_ketiga');
		$this->load->model('Kontrak_dokumen_pihak_ketiga_model', 'kontrak_dokumen_pihak_ketiga');
		$this->load->model('Kontrak_dokumen_lainnya_pihak_ketiga_model', 'kontrak_dokumen_lainnya_pihak_ketiga');
        $this->load->model('Kontrak_karyawan_pihak_ketiga_model', 'kontrak_karyawan_pihak_ketiga');
		$this->load->model('Kontrak_surat_penawaran_model', 'kontrak_surat_penawaran');
		$this->load->model('Pejabat_pengadaan_model', 'pejabat_pengadaan');
		$this->load->model('Pejabat_pembuat_komitmen_model', 'pejabat_pembuat_komitmen');
		$this->load->model('Karyawan_model', 'karyawan');
		$this->load->model('Jenis_surat_model', 'jenis_surat');
		
		
		$this->load->helper('generic');

		$mpdf = new \Mpdf\Mpdf(['format'=>'legal', 'debug' => true, 'allow_output_buffering' => true]);
		$mpdf->SetCompression(TRUE);
		$mpdf->SetImportUse();

        $kontrak_pekerjaan = $this->kontrak_pekerjaan->fetch_data($kontrak_pekerjaan_id);
		$filename = 'Kontrak Pekerjaan - '.$kontrak_pekerjaan->aktivitas_nama.' .pdf';
		
		$mpdf->SetTitle($filename);
		
		$data['filename'] = $filename;

		$path_lampiran_kontrak_pekerjaan 	= './assets/uploads/documents_kontrak_lainnya/';
		$path_lampiran_kontrak_pihak_ketiga = './assets/uploads/documents_kontrak_pihak_ketiga/';
		$path_dokumen_pihak_ketiga			= './assets/uploads/documents/';
		$path_penugasan_karyawan 			= './assets/uploads/documents_karyawan/';
		$path_dokumen_lainnya_pihak_ketiga	= './assets/uploads/documents_kontrak_lainnya_pihak_ketiga/';
		

		$data['kontrak_pekerjaan'] 			= $this->kontrak_pekerjaan->fetch_data($kontrak_pekerjaan_id);
		$data['kontrak_pihak_ketiga'] 		= $this->kontrak_pihak_ketiga->fetch_data(NULL, $kontrak_pekerjaan_id, NULL);
		$data['pejabat_pengadaan']          = $this->pejabat_pengadaan->fetch_data($data['kontrak_pekerjaan']->pejabat_pengadaan_id);
        $data['pejabat_pembuat_komitmen']   = $this->pejabat_pembuat_komitmen->fetch_data($data['kontrak_pekerjaan']->pejabat_pembuat_komitmen_id);
		$data['surat_penawaran']			= $this->kontrak_surat_penawaran->fetch_data(NULL, $kontrak_pekerjaan_id);
		$data['controller']					= $this;
		$data['direktur_perusahaan'] 		= NULL;

		$karyawan = $this->karyawan->fetch_data(NULL, $data['kontrak_pihak_ketiga'][0]->pihak_ketiga_id);
		if(!empty($karyawan)) {                    
			foreach($karyawan as $item):
				if($item->jabatan_nama=='Direktur'){
					$data['direktur_perusahaan'] = $item->nama_lengkap;
					break;
				}
			endforeach;
		}
		
		
		//Cover
		$data['spk'] = $this->kontrak_surat_penawaran->fetch_data(NULL, $kontrak_pekerjaan_id, 'SPK');

        $views = $this->load->view('template_surat_penawaran/cover', $data, TRUE);
        $mpdf->AddPage();
	    $mpdf->WriteHTML($views);
                
		/* ----- Kontrak Surat Penawaran ----- */

		// Check data kontrak_surat_penawaran
		if(!empty($data['surat_penawaran'])) {
			$jml_daftar_hadir 	= 0;
			$list_jenis_surat 	= [];
			$jenis_surat 		= $this->jenis_surat->fetch_data();
			$except_jenis_surat = ['DHN', 'SPesanan', 'cover'];

			if(!empty($jenis_surat)) {
				foreach($jenis_surat as $item):
					if(!in_array($item->kode, $except_jenis_surat)) {
						array_push($list_jenis_surat, $item->kode);
					}
				endforeach;
			}
			$list_jenis_surat = array_unique($list_jenis_surat);

			$list_kontrak_surat = [];
			foreach($data['surat_penawaran'] as $item):
				if($item->jenis_surat_kode=='DHN') {
					$jml_daftar_hadir++;
				}
				else {
					array_push($list_kontrak_surat, $item->jenis_surat_kode);
				}
			endforeach;
			$list_kontrak_surat = array_unique($list_kontrak_surat);

			$intersect = array_intersect($list_jenis_surat, $list_kontrak_surat);

			if(count($list_jenis_surat)==count($intersect) and $jml_daftar_hadir>=2) {
				/* ----- Kontrak Surat Penawaran ----- */
				$views = $this->load->view('kontrak_pekerjaan/download_dokumen_first', $data, TRUE);
				
				$mpdf->AddPage();
				$mpdf->WriteHTML($views);

				//Lampiran SPK
				$kontrak_surat = $this->get_kontrak_surat_penawaran(NULL, $kontrak_pekerjaan_id, 'SPK');
				$kontrak_surat = $kontrak_surat[0];
				
				$document_surat_kontrak = './assets/uploads/documents_lampiran_surat/';

				$file_location = $document_surat_kontrak.$kontrak_surat->lampiran;
				if(file_exists($file_location) and !empty($kontrak_surat->lampiran)) {
					// Get all pages in current file
					$pageInFile = $mpdf->SetSourceFile($file_location);

					for ($flag=1; $flag<=$pageInFile ; $flag++) { 
						$pageId = $mpdf->ImportPage($flag);

						$page = $mpdf->getTemplateSize($pageId);

						if($flag==1) {
							$mpdf->state=0;
							$mpdf->UseTemplate($pageId);
						}
						else {
							$mpdf->state = 1;
							$mpdf->AddPage($page['w']>$page['h']?'L':'P');
							$mpdf->UseTemplate($pageId);
						}
					}
				}
				else {
                    $view = "<h1 style='color: #000; font-family: arial, sans-serif; font-size: 24px; font-weight: bold; margin-top: 0px;
                    margin-bottom: 1px;text-decoration: none; text-align:left; margin-left:0'>Data lampiran perincian harga masih kosong / dokumen laampiran tidak ditemukan</h1>";
                    $mpdf->AddPage();
                    $mpdf->WriteHTML($view);
				}
				
				$views = $this->load->view('kontrak_pekerjaan/download_dokumen_second', $data, TRUE);
				
				$mpdf->AddPage();
				$mpdf->WriteHTML($views);
				/* ----- /Kontrak Surat Penawaran ----- */
			}
			else {
				$view = "<h1 style='color: #000; font-family: arial, sans-serif; font-size: 24px; font-weight: bold; margin-top: 0px;
			margin-bottom: 1px;text-decoration: none; text-align:left; margin-left:0'>Data dokumen pengadaan masih kurang lengkap. Harap lengkapi terlebih dahulu</h1>";
			    $mpdf->AddPage();
			    $mpdf->WriteHTML($view);
			}
		}
		else {
			$view = "<h1 style='color: #000; font-family: arial, sans-serif; font-size: 24px; font-weight: bold; margin-top: 0px;
			margin-bottom: 1px;text-decoration: none; text-align:left; margin-left:0'>Data dokumen pengadaan masih kurang lengkap. Harap lengkapi terlebih dahulu</h1>";
			$mpdf->AddPage();
			$mpdf->WriteHTML($view);
		}	
		/* ----- /Kontrak Surat Penawaran ----- */
		
		/* ----- Dokumen Kontrak Pihak Ketiga ----- */
		$kontrak_pihak_ketiga 			= $data['kontrak_pihak_ketiga'];
		$dokumen_kontrak_pihak_ketiga 	= $this->kontrak_dokumen_pihak_ketiga->fetch_data(NULL, $kontrak_pihak_ketiga[0]->id, NULL);
		$penugasan_karyawan 			= $this->kontrak_karyawan_pihak_ketiga->fetch_data(NULL, $kontrak_pihak_ketiga[0]->id);
		$dokumen_lainnya    			= $this->kontrak_dokumen_lainnya_pihak_ketiga->fetch_data(NULL, $kontrak_pihak_ketiga[0]->id);

		// Dokumen Profil
		if(!empty($dokumen_kontrak_pihak_ketiga)) {
			$data_dokumen_pihak_ketiga = [];

			foreach($dokumen_kontrak_pihak_ketiga as $item):
				array_push($data_dokumen_pihak_ketiga, $path_dokumen_pihak_ketiga.$item->dokumen_pihak_ketiga_file);
			endforeach;

			if(!empty($data_dokumen_pihak_ketiga)) {
				foreach($data_dokumen_pihak_ketiga as $item):
					$pageInFile = $mpdf->SetSourceFile($item);

					for ($flag=1; $flag<=$pageInFile; $flag++) { 
						$pageId = $mpdf->ImportPage($flag);

						$page = $mpdf->getTemplateSize($pageId);

						if($flag==1) {
							$mpdf->state=0;
							$mpdf->UseTemplate($pageId);
						}
						else {
							$mpdf->state = 1;
							$mpdf->AddPage($page['w']>$page['h']?'L':'P');
							$mpdf->UseTemplate($pageId);
						}
					}
				endforeach;
			}

			$mpdf->SetImportUse();
		}
		else {
			$view = "<h1 style='color: #000; font-family: arial, sans-serif; font-size: 24px; font-weight: bold; margin-top: 0px;
			margin-bottom: 1px;text-decoration: none; text-align:left; margin-left:0'>Data Dokumen Profil Pihak Ketiga Masih Kosong</h1>";
			$mpdf->AddPage();
			$mpdf->WriteHTML($view);
		}

		// Penugasan Karyawan
		$list_file = [];
		if(!empty($penugasan_karyawan)) {
			foreach($penugasan_karyawan as $item):
				if(!empty($item->karyawan_dokumen_profil)) {
					array_push($list_file, $path_penugasan_karyawan.$item->karyawan_dokumen_profil);                          
				}

				if(!empty($item->karyawan_dokumen_pendidikan)) {
					array_push($list_file, $path_penugasan_karyawan.$item->karyawan_dokumen_pendidikan);
				}

				if(!empty($item->karyawan_dokumen_lainnya)) {
					array_push($list_file, $path_penugasan_karyawan.$item->karyawan_dokumen_lainnya);
				}
			endforeach;
		}

		if(!empty($list_file)) {
			foreach($list_file as $item):
				if(file_exists($item)) {
					// Get all pages in current file
					$pageInFile = $mpdf->SetSourceFile($item);

					for ($flag=1; $flag<=$pageInFile ; $flag++) { 
						$pageId = $mpdf->ImportPage($flag);

						$page = $mpdf->getTemplateSize($pageId);

						if($flag==1) {
							$mpdf->state=0;
							$mpdf->UseTemplate($pageId);
						}
						else {
							$mpdf->state = 1;
							$mpdf->AddPage($page['w']>$page['h']?'L':'P');
							$mpdf->UseTemplate($pageId);
						}
					}
					// print($item.'<br/>');
				}
			endforeach;
			// die;
		}
		else {
			$view = "<h1 style='color: #000; font-family: arial, sans-serif; font-size: 24px; font-weight: bold; margin-top: 0px;
			margin-bottom: 1px;text-decoration: none; text-align:left; margin-left:0'>Data Dokumen Penugasan Karyawan(Bagian Profil) Masih Kosong</h1>";
			$mpdf->AddPage();
			$mpdf->WriteHTML($view);
		}

		//Dokumen Lainnya Kontrak Pihak Ketiga
		$list_file = [];
		if(!empty($dokumen_lainnya)) {
			foreach($dokumen_lainnya as $item):
				if(!empty($item->file)) {
					array_push($list_file, $path_dokumen_lainnya_pihak_ketiga.$item->file);
				}
			endforeach;
		}

		if(!empty($list_file)) {
			foreach($list_file as $item):
				if(file_exists($item)) {
					// Get all pages in current file
					$pageInFile = $mpdf->SetSourceFile($item);

					for ($flag=1; $flag<=$pageInFile ; $flag++) { 
						$pageId = $mpdf->ImportPage($flag);

						$page = $mpdf->getTemplateSize($pageId);

						if($flag==1) {
							$mpdf->state=0;
							$mpdf->UseTemplate($pageId);
						}
						else {
							$mpdf->state = 1;
							$mpdf->AddPage($page['w']>$page['h']?'L':'P');
							$mpdf->UseTemplate($pageId);
						}
					}
				}
			endforeach;
		}
		else {
			$view = "<h1 style='color: #000; font-family: arial, sans-serif; font-size: 24px; font-weight: bold; margin-top: 0px;
			margin-bottom: 1px;text-decoration: none; text-align:left; margin-left:0'>Data Dokumen Pendukung Lainnya Masih Kosong</h1>";
			$mpdf->AddPage();
			$mpdf->WriteHTML($view);
		}
		/* ----- /Kontrak Pekerjaan Pihak Ketiga ----- */


		/* ----- Kontrak Pekerjaan Dokumen Lainnya ----- */
		$kontrak_dokumen_lainnya= $this->kontrak_dokumen_lainnya->fetch_data(NULL, $kontrak_pekerjaan_id);
	
		if(!empty($kontrak_dokumen_lainnya)) {
			$data_lampiran = [];
			foreach($kontrak_dokumen_lainnya as $item):
				$data['file'] = $path_lampiran_kontrak_pekerjaan.$item->file;
				array_push($data_lampiran, $data);
			endforeach;

			$mpdf->SetImportUse();

			if(!empty($data_lampiran)) {
				foreach($data_lampiran as $item):
					if(file_exists($item['file'])){
						// Get all pages in current file
						$pageInFile = $mpdf->SetSourceFile($item['file']);
	
						for ($flag=1; $flag<=$pageInFile; $flag++) { 
							$pageId = $mpdf->ImportPage($flag);
	
							$page = $mpdf->getTemplateSize($pageId);
	
							if($flag==1) {
								$mpdf->state=0;
								$mpdf->UseTemplate($pageId);
							}
							else {
								$mpdf->state = 1;
								$mpdf->AddPage($page['w']>$page['h']?'L':'P');
								$mpdf->UseTemplate($pageId);
							}
						}
					}
				endforeach;
			}
			
		}
		else {
            $view = "<h1 style='color: #000; font-family: arial, sans-serif; font-size: 24px; font-weight: bold; margin-top: 0px;
			margin-bottom: 1px;text-decoration: none; text-align:left; margin-left:0'>Data Dokumen Pendukung Kontrak Masih Kosong</h1>";
            $mpdf->AddPage();
            $mpdf->WriteHTML($view);
        }
		/* ----- /Kontrak Pekerjaan Dokumen Lainnya ----- */
		ob_clean();
		ob_end_flush(); 

		$mpdf->output($filename,"I");
		unset($mpdf);
// 		ob_end_flush();
        // ob_clean();
		
	}

	public function get_kontrak_surat_penawaran($kontrak_surat_id, $kontrak_pekerjaan_id, $kode_surat)
	{
		$this->load->model('Kontrak_surat_penawaran_model', 'kontrak_surat_penawaran');
		
		return $this->kontrak_surat_penawaran->fetch_data($kontrak_surat_id, $kontrak_pekerjaan_id, $kode_surat);
	}

	public function get_jadwal_kegiatan_kontrak($kontrak_surat_id, $kode=NULL)
	{
		$this->load->model('Jadwal_kegiatan_kontrak_model', 'jadwal_kegiatan_kontrak');
		
		return $this->jadwal_kegiatan_kontrak->fetch_data(NULL, $kode, $kontrak_surat_id);
	}

	public function get_direktur_pihak_ketiga($pihak_ketiga_id)
	{
		$this->load->model('Karyawan_model', 'karyawan');
                
		$karyawan = $this->karyawan->fetch_data(NULL, $pihak_ketiga_id);
		if(!empty($karyawan) and !empty($pihak_ketiga_id) ) {                    
			foreach($karyawan as $item):
				if($item->jabatan_nama=='Direktur'){
					return $item->nama_lengkap;
					break;
				}
			endforeach;
		}
	}
}

/* End of file Kontrak_pekerjaan.php */
