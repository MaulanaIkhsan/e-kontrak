<!DOCTYPE html>
<HtmL>

<HeAd>
    <MeTa HTTP-EquIV="CONTENT-TYPE" CONTent="text/html; charset=utf-8">
    <TItlE>Semarang, 29 Maret 2004</tITLE>
    <meta name=GENERATOR coNTeNT="LibreOffice 4.1.6.2 (Linux)">
    <Meta nAMe="AUTHOR" CONTeNt="DPU">
    <MEtA NaMe=CREATED cONtENt="Tahun_Anggaran0817;20400000000000">
    <MeTA NAmE="CHANGEDBY" CoNteNt="isan">
    <META NAme="CHANGED" coNTent="20190320;163019000000000">
    <MEtA nAmE=KSOProductBuildVer CONTEnt=1033-10.1.0.6757>
    <sTyLE>
        @page {
            size: 8.47in 13.98in;
            margin-right: 0.88in;
            margin-top: 0.59in;
            margin-bottom: 0.69in
        }
        P {
            margin-bottom: 0.08in;
            direction: ltr;
            color: #000000
        }
        P.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
        H1 {
            margin-left: 3.5in;
            margin-top: 0in;
            margin-bottom: 0in;
            direction: ltr;
            color: #000000;
            text-align: justify;
            text-decoration: underline
        }
        H1.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
        .text {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
        }
    </sTYLE>
</heAd>

<bOdY LANg="en-US" tExT=#000000 dIR="LTR">
    <?php
        $dayname    = dayname(date('l', strtotime($kontrak_surat->tgl_surat)));
        $tglindo    = tgl_indo($kontrak_surat->tgl_surat);
        $split_tgl  = explode(' ', $tglindo);       
    ?>

    <p clAss="western" ALIGN=CENTER stYLe="margin-bottom: 0in; page-break-before: none">
        <u><b>PENETAPAN PEMENANG</b></U><br/>
        Nomor : <?php print($kontrak_surat->no_surat);?>
    </P>
    <p></p>

    <p cLaSs=western ALIGN=JUSTIFY style="margin-bottom: 0in">
        <?php
        $no_bahpl = NULL;
        $tgl_bahpl = 'Kosong';
        if(!empty($bahpl)) {
            $no_bahpl = $bahpl[0]->no_surat;
            $tgl_bahpl = $bahpl[0]->tgl_surat;
        }
        ?>

        Berdasarkan BAHPL Nomor <?php print($no_bahpl); ?> tanggal <?php print($tgl_bahpl!='Kosong'?tgl_indo($tgl_bahpl):$tgl_bahpl); ?>, Pejabat Pengadaan pada Dinas Pekerjaan Umum Kota Semarang Tahun Anggaran <?php print(date("Y")); ?> Keputusan Kepala Dinas Pekerjaan Umum Kota Semarang Selaku Pengguna Anggaran Nomor <?php print($pejabat_pengadaan->sk_no); ?> tanggal <?php print(tgl_indo($pejabat_pengadaan->sk_tgl_penetapan)); ?> tentang Penunjukan Pejabat Pengadaan Pada Dinas Pekerjaan Umum Kota Semarang Tahun Anggaran <?php print($pejabat_pengadaan->sk_tahun); ?> untuk :
    </p>
    <p></p>
    <table align="center" cellpadding="2">
        <tr>
            <td class="text" style="font-size:12pt">Program</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pekerjaan->program_nama); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Kegiatan</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pekerjaan->pekerjaan_nama); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Pekerjaan</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pekerjaan->aktivitas_nama); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Sumber Dana</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pekerjaan->aktivitas_sumber_dana); ?></td>
        </tr>
    </table>
    <p></p>
    <p cLaSs=western ALIGN="center" style="margin-bottom: 0in">
        <strong>Menetapkan :</strong>
    </p>
    <p></p>
    <table align="center" cellpadding="2">
        <tr>
            <td class="text" style="font-size:12pt">Nama Perusahaan</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Alamat</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_alamat); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">NPWP</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_npwp); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Harga Penawaran Terkoreksi</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pekerjaan->harga_negosiasi); ?></td>
        </tr>
    </table>

    <p cLaSs=western ALIGN=JUSTIFY style="margin-bottom: 0in">
        Demikian penetapan ini untuk diketahui dan dipergunakan sebagaimana mestinya.
    </p>
    <p></p>
    <p></p>
    <table>
        <tr>
            <td width="250"></td>
            <td width="250"></td>
            <td width="500" align="center" style="font-size:16pt">
                <?php print($pejabat_pengadaan->jenis_pejabat_pengadaan_nama); ?> <?php print($pejabat_pengadaan->seri_pejabat_pengadaan_nama); ?><br/>
                Pada Dinas Pekerjaan Umum Kota Semarang<br/>
                Tahun Anggaran <?php print(date("Y"));?><br/>
                <br/><br/><br/><br/><br/>
                <u><?php print($pejabat_pengadaan->pegawai_nama); ?></u><br/>
                NIP. <?php print($pejabat_pengadaan->pegawai_nip); ?>
            </td>
        </tr>
    </table>
    
    <p></p>
    <p></p>
    <p ClaSs=western StYLe="margin-bottom: 0in">
        Tembusan : <br/>
        <ol type="1">
            <li>Kuasa Pengguna Anggaran selaku Pejabat Pembuat Komitmen</li>
            <li>Arsip</li>
        </ol>
    </p>
    
</BODY>

</HTML>