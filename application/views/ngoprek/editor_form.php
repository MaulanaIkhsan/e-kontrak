<?php
$this->load->view('template/header');?>

<?php $this->load->view('template/asset_header');?>
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/jquery-ui/themes/dot-luv/jquery-ui.min.css')); ?>">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet" href="<?php print(base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')); ?>" />
<style>
    ul.wysihtml5-toolbar li a[title="Insert image"] { display: none; }
    ul.wysihtml5-toolbar li a[title="Insert link"] { display: none; }
    ul.wysihtml5-toolbar li a[title="Indent"] { display: none; }
    ul.wysihtml5-toolbar li a[title="Outdent"] { display: none; }
    ul.wysihtml5-toolbar li.dropdown { display: none; }
</style>
<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        	<div class="col-md-12">
            <!-- Horizontal Form -->
	          <div class="box box-primary">
	              <div class="box-header with-border">
	                <h3 class="box-title">Autocomplete</h3>
	                
	              </div>
	              <!-- /.box-header -->
	              
	              <!-- form start -->
	              <div class="box-body">
                    <form action="" method="post" id="editor-form">
	              	    <textarea name="dasar_durat" class="textarea form-control">
                        </textarea>
                    </form>
	              </div>
	          	</div>
	      	</div>

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php print(base_url('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')); ?>"></script>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5();
    // $('#editor-form').contents().find('.textarea').html('<ul><li>unordered list content</li></ul>');

    $('.textarea').val("<?php print('<ul><li>First Content</li></ul>'); ?>");
  });
</script>
<?php $this->load->view('template/footer');?>
<!-- https://stackoverflow.com/questions/18328216/wysihtml5-setting-a-value-wont-work-because-sandbox-iframe-isnt-loaded-yet -->

