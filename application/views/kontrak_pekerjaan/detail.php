<?php
$this->load->view('template/header');?>
<!-- Select2 -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/select2/dist/css/select2.min.css')); ?>">
<?php $this->load->view('template/asset_header');?>
<link rel="stylesheet" type="text/css" href="<?php print(base_url('assets/bower_components/datatables.net-bs/css/custom_button_export.css'))?>" />
<link href="<?php echo base_url('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css')?>" rel="stylesheet">
<style>
.select2-selection--multiple {
  height: 2rem;
  max-height: 2rem;
  overflow: auto;
}
</style>
<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <!-- Box Form Pengunjung -->
        <?php 
            print($this->session->flashdata('alert')); 
            print($this->session->flashdata('success')); 

            $sess_aktivitas_id  = $this->session->userdata('id_aktivitas');
            $sess_data          = $this->session->userdata('session_data');
            $user_as            = $sess_data['as'];
            $user_role          = $sess_data['role'];
            $user_id            = $sess_data['id'];
        ?>
        <div class="col-md-12">

            <?php if($jml_kontrak_surat==FALSE and $contained==TRUE) { ?>
                <div class="alert alert-warning fade in">
                    <a href="#" class="close" data-dismiss="alert">&times;</a> 
                    Silahkan lengkapi dokumen pengadaan sesuai peraturan yang berlaku
                </div>
            <?php } ?>
          <!-- Horizontal Form -->
          <div class="box box-warning" id="form-pengunjung">
              <div class="box-header with-border">
                <h3 class="box-title">Detail Kontrak Kegiatan</h3>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <form class="form-horizontal" method="post">
                <div class="box-body">
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Nama Program</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_nama_program"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Nama Pekerjaan</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_nama_pekerjaan"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Nama Kegiatan</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_nama_kegiatan"></div>
                    </div>
                  </div>  
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Pejabat Pembuat Komitmen</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_pejabat_pembuat_komitmen"></div>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Pejabat Pengadaan</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_pejabat_pengadaan"></div>
                    </div>
                  </div>  
                  <div class="form-group data-pusat">
                    <label for="namalengkap" class="col-sm-2 control-label">Tgl Awal Kontrak</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_tgl_awal_kontrak"></div>
                    </div>
                  </div>
                  <div class="form-group data-pusat">
                    <label for="namalengkap" class="col-sm-2 control-label">Tgl Akhir Kontrak</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_tgl_akhir_kontrak"></div>
                    </div>
                  </div>
                  <div class="form-group data-pusat">
                    <label for="namalengkap" class="col-sm-2 control-label">HPS</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_hps" class="money"></div>
                    </div>
                  </div>
                  <div class="form-group data-pusat">
                    <label for="namalengkap" class="col-sm-2 control-label">Harga Negosiasi / Nilai Kontrak</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_harga_negosiasi" class="money"></div>
                    </div>
                  </div>
                  <div class="form-group data-pusat">
                    <label for="namalengkap" class="col-sm-2 control-label">Status</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="detail_status"></div>
                    </div>
                  </div>                  
                </div>
                <div class="box-footer">
                    <?php 
                    if($sess_aktivitas_id!==NULL) { ?>
                        <a href="<?php print(base_url('aktivitas/detail/'.$sess_aktivitas_id));?>" class="btn btn-primary">Kembali</a>
                    <?php } elseif($this->session->userdata('resume_kontrak_pihak_ketiga')!=NULL) { ?>
                        <a href="<?php print(base_url('kontrak_pekerjaan/resume/'));?>" class="btn btn-primary">Kembali</a>
                    <?php } else { ?>
                        <a href="<?php print(base_url('kontrak_pekerjaan'));?>" class="btn btn-primary">Kembali</a>
                    <?php }
                    switch ($user_as) {
                        case 'pegawai':
                            // User superadmin
                            if($user_role=='Super Admin') { ?>
                                <a href="javascript:void(0)" class="btn btn-primary" onclick="edit_kontrak_pekerjaan()">Edit</a>
                                <a href="javascript:void(0)" class="btn btn-danger" onclick="delete_data()">Delete</a>
                                
                                <div class="pull-right">
                                    <a href="<?php print(base_url('kontrak_pekerjaan/download_dokumen_kontrak/'.$id));?>" class="btn btn-primary"data-toggle="tooltip" target="_blank" title="Download semua dokumen kontrak">Download Dokumen Kontrak</a>
                                </div>
                            <?php }
                            // while current user is pejabat_pengadaan
                            elseif($user_role!='Super Admin' and $detail->pejabat_pengadaan_pegawai_id==$user_id) { ?>
                                <a href="javascript:void(0)" class="btn btn-primary" onclick="edit_kontrak_pekerjaan()">Edit</a>
                                <a href="javascript:void(0)" class="btn btn-danger" onclick="delete_data()">Delete</a>
                                
                                <div class="pull-right">
                                    <a href="<?php print(base_url('kontrak_pekerjaan/download_dokumen_kontrak/'.$id));?>" class="btn btn-primary"data-toggle="tooltip" title="Download semua dokumen kontrak" target="_blank">Download Dokumen Kontrak</a>
                                </div>
                            <?php }
                            break;
                        case 'pihak_ketiga':
                            ?>
                            <?php if($contained==FALSE) { ?>
                                <a href="javascript:void(0)" class="btn btn-primary" onclick="add_pihak_ketiga()">Ajukan Penawaran</a>
                            <?php } 
                            break;
                        default:
                            ?>
                            <a href="javascript:void(0)" class="btn btn-primary">Not Access</a>
                            <?php
                            break;
                    }
                    ?>
                    
                </div>
                <!-- /.box-footer -->
              </form>

              <!-- Tabs Content -->
              <div class="nav-tabs-custom">
                    <!-- Tabs within a box -->
                    <ul class="nav nav-tabs pull-left">
                        <li class="active"><a href="#data-surat-penawaran" data-toggle="tab">Dokumen Pengadaan</a></li>
                        <li>
                            <a href="#data-pihak-ketiga" data-toggle="tab">
                                Penyedia Barang/Jasa 
                            </a>
                        </li>
                        <li><a href="#data-dokumen-lainnya" data-toggle="tab">Dokumen Pendukung</a></li>
                    </ul>
                    <div class="tab-content no-padding">
                        <div class="chart tab-pane active" id="data-surat-penawaran" style="position: relative;">
                            <?php if($user_as=='pegawai' and 
                            ($detail->pejabat_pengadaan_pegawai_id==$user_id or $is_ppkom==TRUE or
                            $user_role=='Super Admin') and $contained==TRUE) { ?>
                                <p>
                                    <a href="javascript:void(0)" class="btn btn-primary" onclick="add_surat_penawaran()">Insert Surat/Dokumen</a>
                                </p>
                            <?php } else {?>
                                <div class="callout callout-danger">
                                    <h4>Perhatian!</h4>
                                    <p>Harap pilih <strong>Pihak Penyedia Barang/Jasa</strong> di terlebih dahulu</p>
                                </div>
                            <?php } ?>
                            <table id="tabel_surat_penawaran" class="table table-bordered table-striped table-responsive">
                                <thead>
                                    <tr class="headings" align="center">
                                        <th class="column-title" align="center">No</th>
                                        <th class="column-title" align="center">Nama Surat/Dokumen</th>
                                        <th class="column-title" align="center">Tgl Surat</th>
                                        <th class="column-title" align="center">No Surat</th>
                                        <th class="column-title" align="center">Lampiran</th>
                                        <th class="column-title" align="center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <div class="chart tab-pane" id="data-pihak-ketiga" style="position: relative;">
                            <?php 
                            if($user_as=='pegawai' and $contained==FALSE and
                            ($detail->pejabat_pengadaan_pegawai_id==$user_id or $is_ppkom==TRUE or
                            $user_role=='Super Admin')) { ?>
                                <p>
                                    <a href="javascript:void(0)" class="btn btn-primary" onclick="add_pihak_ketiga()">Insert Data</a>
                                </p>
                            <?php } ?>
                            <table id="tabel_kontrak_pihak_ketiga" class="table table-bordered table-striped table-responsive">
                                <thead>
                                    <tr class="headings" align="center">
                                        <th class="column-title" align="center">No</th>
                                        <th class="column-title" align="center">Nama Perusahaan</th>
                                        <th class="column-title" align="center">Nilai Penawaran</th>
                                        <th class="column-title" align="center">Lampiran</th>
                                        <th class="column-title" align="center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <div class="chart tab-pane" id="data-dokumen-lainnya" style="position: relative;">
                            <?php if($user_as=='pegawai' and 
                            ($detail->pejabat_pengadaan_pegawai_id==$user_id or $is_ppkom==TRUE or
                            $user_role=='Super Admin') and $contained==TRUE) { ?>
                                <p>
                                    <a href="javascript:void(0)" class="btn btn-primary" onclick="add_dokumen_lainnya()">Insert Data</a>
                                </p>
                            <?php } else {?>
                                <div class="callout callout-danger">
                                    <h4>Perhatian!</h4>
                                    <p>Harap pilih <strong>Pihak Penyedia Barang/Jasa</strong> di terlebih dahulu</p>
                                </div>
                            <?php } ?>
                            <table id="tabel_kontrak_dokumen_lainnya" class="table table-bordered table-striped table-responsive">
                                <thead>
                                    <tr class="headings" align="center">
                                        <th class="column-title" align="center">No</th>
                                        <th class="column-title" align="center">Nama Dokumen</th>
                                        <th class="column-title" align="center">Dokumen</th>
                                        <th class="column-title" align="center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                       
                    </div>
                </div>
                <!-- /Tabs Content -->

            </div>
          </div>
          <!-- /Box Form Pengunjung -->

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/dataTables.buttons.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.flash.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/jszip.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/pdfmake.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/vfs_fonts.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.html5.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.print.min.js'))?>"></script>
<script src="<?php echo base_url('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')?>"></script>
<!-- Select2 -->
<script src="<?php print(base_url('assets/bower_components/select2/dist/js/select2.full.min.js'));?>"></script>
<!-- Number divider -->
<script src="<?php print(base_url('assets/my_custom/js/number-divider.min.js')); ?>"></script>

<script>
    var save_method_kontrak_pekerjaan,
        table_surat_penawaran,
        save_method_surat_penawaran,
        url_surat_penawaran,
        table_pihak_ketiga,
        save_method_pihak_ketiga,
        url_pihak_ketiga,
        table_pihak_ketiga,
        url_dokumen_lainnya,
        save_method_dokumen_lainnya;

    $(function(){
        $.fn.dataTable.ext.errMode = 'none';
        detail_data();

        $('.data-akun').hide();

        $('.select2').select2();

        $('#used').change(function(){
            if(this.checked) {
               $('#lampiran').attr('required', 'required');
            }
            else {
                $('#lampiran').removeAttr('required');
            }
        });

        //Just number input
        $(".money").on("keypress keyup",function (event) {    
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });

        $("input").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $("textarea").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $("select").change(function(){
            $(this).parent().parent().removeClass('has-error');
            $(this).next().empty();
        });

        $('.datepicker').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd",
            todayHighlight: true,
            todayBtn: true,
            todayHighlight: true,  
        });

        //Just number input
        $(".number").on("keypress keyup",function (event) {    
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });      

        table_surat_penawaran = $('#tabel_surat_penawaran').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
            "ajax": {
                "url": "<?php echo site_url('kontrak_surat_penawaran/get_data/'.$id);?>",
                "type": "POST"
            },
            "autoWidth": false,
            dom: 'lBfrtip',
            button:[{
              extend:'pdf',
              oriented:'potrait',
              pageSize: 'A4',
              title : 'Data Kontrak Surat Penawaran',
              download : 'open'
            }, 'copy', 'csv', 'excel', 'pdf', 'print'],
            "columnDefs": [{ 
                "targets": [ 0 ], 
                "orderable": false, 
            }]
        });

        table_pihak_ketiga = $('#tabel_kontrak_pihak_ketiga').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
            "ajax": {
                "url": "<?php echo site_url('kontrak_pihak_ketiga/get_data/'.$id);?>",
                "type": "POST"
            },
            "autoWidth": false,
            dom: 'lBfrtip',
            button:[{
              extend:'pdf',
              oriented:'potrait',
              pageSize: 'A4',
              title : 'Data Kontrak Pihak Ketiga',
              download : 'open'
            }, 'copy', 'csv', 'excel', 'pdf', 'print'],
            "columnDefs": [{ 
                "targets": [ 0 ], 
                "orderable": false, 
            }]
        });

        table_dokumen_lainnya = $('#tabel_kontrak_dokumen_lainnya').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
            "ajax": {
                "url": "<?php echo site_url('kontrak_dokumen_lainnya/get_data/'.$id);?>",
                "type": "POST"
            },
            "autoWidth": false,
            dom: 'lBfrtip',
            button:[{
              extend:'pdf',
              oriented:'potrait',
              pageSize: 'A4',
              title : 'Data Kontrak Dokumen Lainnya',
              download : 'open'
            }, 'copy', 'csv', 'excel', 'pdf', 'print'],
            "columnDefs": [{ 
                "targets": [ 0 ], 
                "orderable": false, 
            }]
        });

        // Save and Update surat penawawan
        $('#form-surat-penawaran').submit(function(obj){
            obj.preventDefault();
            if(save_method_surat_penawaran=='add') {
                url_surat_penawaran = "<?php echo site_url('kontrak_surat_penawaran/ajax_add/'.$id)?>";
            } else {
                url_surat_penawaran = "<?php echo site_url('kontrak_surat_penawaran/ajax_update/')?>";
            }
            $.ajax({
                url: url_surat_penawaran,
                type: 'POST',
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(datas){
                    console.log(datas);  
                    
                    // Parsing to object
                    datas=JSON.parse(datas);                  
                    
                    //if success close modal and reload ajax table dokumen
                    if(datas.hasOwnProperty('status') && datas.status) 
                    {
                        alert('Data berhasil tersimpan');
                        $('#modal_form_surat_penawaran').modal('hide');
                        $('#form-surat-penawaran')[0].reset(); // reset form on modals
                        reload_data_surat_penawaran();
                    }
                    else
                    {
                        if(datas.hasOwnProperty('inputerror')) {
                            for (var i = 0; i < datas.inputerror.length; i++) 
                            {
                                $('[name="'+datas.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                $('[name="'+datas.inputerror[i]+'"]').next().text(datas.error_string[i]); //select span help-block class set text error string
                            }
                        }
                    }
                    $('#btnSaveSuratPenawaran').text('save'); //change button text
                    $('#btnSaveSuratPenawaran').attr('disabled',false); //set button enable 
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error calling save data. Please check again');
                    $('#modal_form_surat_penawaran').modal('hide');
                }
            });
        });
       
        // Save and Update kontrak pihak ketiga
        $('#form-kontrak-pihak-ketiga').submit(function(obj){
            obj.preventDefault();
            if(save_method_pihak_ketiga=='add') {
                url_pihak_ketiga = "<?php echo site_url('kontrak_pihak_ketiga/ajax_add/'.$id)?>";
            } else {
                url_pihak_ketiga = "<?php echo site_url('kontrak_pihak_ketiga/ajax_update/')?>";
            }
            $.ajax({
                url: url_pihak_ketiga,
                type: 'POST',
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(datas){                  
                    // Parsing to object
                    console.log(datas);
                    
                    datas=JSON.parse(datas);                  
                    
                    //if success close modal and reload ajax table dokumen
                    if(datas.hasOwnProperty('status') && datas.status) 
                    {
                        alert('Data berhasil tersimpan');
                        $('#modal_form_kontrak_pihak_ketiga').modal('hide');
                        $('#form-kontrak-pihak-ketiga')[0].reset(); // reset form on modals
                        reload_data_pihak_ketiga();
                        window.location.reload();
                    }
                    else
                    {
                        if(datas.hasOwnProperty('inputerror')) {
                            for (var i = 0; i < datas.inputerror.length; i++) 
                            {
                                $('[name="'+datas.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                $('[name="'+datas.inputerror[i]+'"]').next().text(datas.error_string[i]); //select span help-block class set text error string
                            }
                        }
                    }
                    $('#btnSaveKontrakPihakKetiga').text('save'); //change button text
                    $('#btnSaveKontrakPihakKetiga').attr('disabled',false); //set button enable 
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error calling save data. Please check again');
                    $('#modal_form_kontrak_pihak_ketiga').modal('hide');
                }
            });
        });

        // Save and Update kontrak dokumen lainnya
        $('#form-dokumen-lainnya').submit(function(obj){
            obj.preventDefault();
            if(save_method_dokumen_lainnya=='add') {
                url_dokumen_lainnya = "<?php echo site_url('kontrak_dokumen_lainnya/ajax_add/'.$id)?>";
            } else {
                url_dokumen_lainnya = "<?php echo site_url('kontrak_dokumen_lainnya/ajax_update/')?>";
            }
            $.ajax({
                url: url_dokumen_lainnya,
                type: 'POST',
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function(datas){                  
                    // Parsing to object
                    datas=JSON.parse(datas);                  
                    
                    //if success close modal and reload ajax table dokumen
                    if(datas.hasOwnProperty('status') && datas.status) 
                    {
                        alert('Data berhasil tersimpan');
                        $('#modal_form_dokumen_lainnya').modal('hide');
                        $('#form-dokumen-lainnya')[0].reset(); // reset form on modals
                        reload_data_dokumen_lainnya();
                    }
                    else
                    {
                        if(datas.hasOwnProperty('inputerror')) {
                            for (var i = 0; i < datas.inputerror.length; i++) 
                            {
                                $('[name="'+datas.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                $('[name="'+datas.inputerror[i]+'"]').next().text(datas.error_string[i]); //select span help-block class set text error string
                            }
                        }
                    }
                    $('#btnSaveDokumenLainnya').text('save'); //change button text
                    $('#btnSaveDokumenLainnya').attr('disabled',false); //set button enable 
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error calling save data. Please check again');
                    $('#modal_form_kontrak_pihak_ketiga').modal('hide');
                }
            });
        });

    });

    function detail_data() {
        $.ajax({
            url: "<?php echo site_url('kontrak_pekerjaan/ajax_edit/'.$id)?>",
            method: 'GET',
            type: 'JSON',
            success: function(data) {
                // Parsing to object
                data=JSON.parse(data);
                
                $('#detail_nama_program').html(data.program_nama);
                $('#detail_nama_pekerjaan').text(data.pekerjaan_nama);
                $('#detail_nama_kegiatan').text(data.aktivitas_nama);
                $('#detail_pejabat_pembuat_komitmen').text(data.pejabat_pembuat_komitmen_nama);
                $('#detail_pejabat_pengadaan').text(data.pejabat_pengadaan_nama);
                $('#detail_tgl_awal_kontrak').text(data.tgl_awal_kontrak);
                $('#detail_tgl_akhir_kontrak').text(data.tgl_akhir_kontrak);
                $('#detail_hps').text(data.hps);
                $('#detail_harga_negosiasi').text(data.harga_negosiasi);
                $('#detail_status').text(data.status);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error while load detail data detail kontrak kegiatan !');
            }
        });
    }

    function delete_data() {
        if(confirm('Apakah anda yakin akan menghapus data ini ?')){
            $.ajax({
                url: "<?php echo site_url('kontrak_pekerjaan/ajax_delete/'.$id)?>",
                method: 'POST',
                type: 'JSON',
                success: function(){
                  alert('Data berhasil dihapus');
                  <?php if($sess_aktivitas_id!=NULL) {?>
                    window.location.replace("<?php print(base_url('aktivitas/detail/'.$sess_aktivitas_id)); ?>");
                  <?php } else { ?>
                    window.location.replace("<?php print(base_url('kontrak_pekerjaan')); ?>");
                  <?php }?>
                    
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error while delete data data pihak ketiga !');
                }
            });
        }
    }

    function edit_kontrak_pekerjaan() {
        save_method_kontrak_pekerjaan = 'update';
        $('#form-penawaran')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('kontrak_pekerjaan/ajax_edit/'.$id)?>",
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id);
                $('[name="pejabat_pembuat_komitmen"]').val(data.pejabat_pembuat_komitmen_id);
                $('[name="pejabat_pengadaan"]').val(data.pejabat_pengadaan_id);
                $('[name="pejabat_pengadaan_nama"]').val(data.pejabat_pengadaan_nama);
                $('[name="tgl_awal_kontrak"]').datepicker('update',data.tgl_awal_kontrak);
                $('[name="tgl_akhir_kontrak"]').datepicker('update', data.tgl_akhir_kontrak);
                $('[name="hps"]').val(data.hps);
                $('[name="status"]').val(data.status);

                $('[for="harga_negosiasi"]').remove();
                $('.harga_negosiasi').remove();

                <?php if($contained==TRUE) { ?> 
                    $('#harga-negosiasi').append('<label for="harga_negosiasi" class="col-sm-4 control-label">Harga Negosiasi / Nilai Kontrak</label><div class="col-sm-8 harga_negosiasi"><input type="text" class="form-control money" name="harga_negosiasi" /><span class="help-block"></span></div>');
                <?php } ?>
                
                $('#modal-create-penawaran').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Update Data'); // Set title to Bootstrap modal title

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    function save_kontrak() 
    {
      $('#btnSave').text('saving...'); //change button text
      $('#btnSave').attr('disabled',true); //set button disable 
      var url;
      
      url = "<?php echo site_url('kontrak_pekerjaan/ajax_update')?>";
      // ajax adding data to database
      $.ajax({
          url : url,
          type: "POST",
          data: $('#form-penawaran').serialize(),
          dataType: "JSON",
          success: function(data)
          {
              if(data.status)
              {
                  $('#modal-create-penawaran').modal('hide');
                  alert('Data berhasil tersimpan');
                  detail_data();
              }
              else
              {
                  for (var i = 0; i < data.inputerror.length; i++) 
                  {
                      $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                      $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                  }

                  $('.select2').select2({
                    closeOnSelect: true
                  });
              }
              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable 


          },
          error: function (jqXHR, textStatus, errorThrown)
          {
              alert('Error adding / update data');
              console.log('Error adding / update data');
              $('#btnSave').text('save'); //change button text
              $('#btnSave').attr('disabled',false); //set button enable 

          }
      });
    }

    // Manage surat penawaran
    function reload_data_surat_penawaran()
    {
      table_surat_penawaran.ajax.reload();
    }

    function add_surat_penawaran() {
        save_method_surat_penawaran = 'add';
        $('#form-surat-penawaran')[0].reset(); // reset form on modals
        <?php if(!empty($kontrak_pihak_ketiga)) {?>
            $('[name="kontrak_pihak_ketiga_id"]').val('<?php print($kontrak_pihak_ketiga[0]->id); ?>');
            $('[name="pihak_ketiga_nama"]').val('<?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama); ?>');
        <?php } ?>
        
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form_surat_penawaran').modal('show'); // show bootstrap modal
        $('.modal-title').text('Insert Dokumen Pengadaan'); // Set Title to Bootstrap modal title
    }

    //Kontrak Pihak Ketiga
    function reload_data_pihak_ketiga()
    {
      table_pihak_ketiga.ajax.reload();
    }

    function add_pihak_ketiga() {
        save_method_pihak_ketiga = 'add';
        $('#form-kontrak-pihak-ketiga')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form_kontrak_pihak_ketiga').modal('show'); // show bootstrap modal
        $('.modal-title').text('Insert Pihak Ketiga'); // Set Title to Bootstrap modal title
        <?php if($user_as=='pihak_ketiga' and $pihak_ketiga_id!=NULL and $pihak_ketiga_nama!==NULL) { ?>
            $('[name="pihak_ketiga_id"]').val('<?php print($pihak_ketiga_id); ?>');
            $('[name="pihak_ketiga_nama"]').val('<?php print($pihak_ketiga_nama); ?>');
        <?php } ?>

        // $('[name="pihak_ketiga_id"]').select2().val('').trigger("change");
    }

    //Kontrak document lainnya
    function reload_data_dokumen_lainnya()
    {
      table_dokumen_lainnya.ajax.reload();
    }

    function add_dokumen_lainnya() {
        save_method_dokumen_lainnya = 'add';
        $('#form-dokumen-lainnya')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form_dokumen_lainnya').modal('show'); // show bootstrap modal
        $('.modal-title').text('Insert Dokumen Lainnya'); // Set Title to Bootstrap modal title
    }

    function edit_dokumen_lainnya(id) {
        save_method_dokumen_lainnya = 'update';
        $('#form-dokumen-lainnya')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo site_url('kontrak_dokumen_lainnya/ajax_edit/')?>/"+id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id);
                $('[name="nama"]').val(data.nama);
                
                $('#modal_form_dokumen_lainnya').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Update Data'); // Set title to Bootstrap modal title

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }

    function delete_dokumen_lainnya(id)
    {
        if(confirm('Are you sure delete this data?'))
        {
            
            // ajax delete data to database
            $.ajax({
                url : "<?php echo site_url('kontrak_dokumen_lainnya/ajax_delete')?>/"+id,
                method: 'POST',
                type: 'JSON',
                success: function(datas)
                {
                    alert('Data berhasil dihapus');
                    reload_data_dokumen_lainnya();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });

        }
    }
</script>

<?php 
    // Load Modal Form
    $this->load->view('kontrak_pekerjaan/modal_form');
    $this->load->view('kontrak_surat_penawaran/modal_form');
    $this->load->view('kontrak_pihak_ketiga/modal_form');
    $this->load->view('kontrak_dokumen_lainnya/modal_form');
?>

<?php $this->load->view('template/footer');?>