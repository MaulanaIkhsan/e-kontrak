<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_kegiatan_kontrak_model extends CI_Model {

    /*--- jadwal_kegiatan_kontrak ---*/
    // id
    // jenis_kegiatan_id
    // kontrak_surat_penawaran_id
    // tgl
    // jam
    // created_at
    // updated_at

    var $column_order = array(null, 'jadwal_kegiatan_kontrak.id', 
            'jenis_kegiatan_pengadaan.nama',
            'jadwal_kegiatan_kontrak.tgl', 
            'jadwal_kegiatan_kontrak.jam'); 

    var $column_search = array('jadwal_kegiatan_kontrak.id', 
            'jenis_kegiatan_pengadaan.nama',
            'jadwal_kegiatan_kontrak.tgl', 
            'jadwal_kegiatan_kontrak.jam');

    // default order 
    var $order = array('jadwal_kegiatan_kontrak.id' => 'asc');

    public function fetch_data($id=NULL, $kode=NULL, $kontrak_surat_penawaran_id=NULL)
    {
        $sql = $this->db->select('jadwal_kegiatan_kontrak.id as id,
                                jenis_kegiatan_pengadaan.id as jenis_kegiatan_id,
                                jenis_kegiatan_pengadaan.nama as jenis_kegiatan_nama,
                                kontrak_surat_penawaran.id as kontrak_surat_penawaran_id,
                                jadwal_kegiatan_kontrak.tgl as tgl,
                                jadwal_kegiatan_kontrak.jam as jam')
                        ->from('jadwal_kegiatan_kontrak')
                        ->join('jenis_kegiatan_pengadaan', 'jadwal_kegiatan_kontrak.jenis_kegiatan_id=jenis_kegiatan_pengadaan.id', 'left')
                        ->join('kontrak_surat_penawaran', 'jadwal_kegiatan_kontrak.kontrak_surat_penawaran_id=kontrak_surat_penawaran.id', 'left')
                        ->order_by('jenis_kegiatan_pengadaan.nama', 'asc');

        if($id!=NULL) {
            $sql->where('jadwal_kegiatan_kontrak.id', $id);
        }

        if($kode!=NULL) {
            $sql->where('jenis_kegiatan_pengadaan.kode', $kode);
        }

        if($kontrak_surat_penawaran_id!=NULL) {
            $sql->where('jadwal_kegiatan_kontrak.kontrak_surat_penawaran_id', $kontrak_surat_penawaran_id);
        }

        if($id!=NULL or $kode!=NULL){
            return $sql->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }

    /*====== Serverside Datatable ======*/
	private function _get_datatables_query($kontrak_surat_penawaran_id)
	{
        $this->db->select('jadwal_kegiatan_kontrak.id as id, 
                        jenis_kegiatan_pengadaan.nama as jenis_kegiatan_nama,
                        jadwal_kegiatan_kontrak.tgl as tgl, 
                        jadwal_kegiatan_kontrak.jam as jam');

        $this->db->from('jadwal_kegiatan_kontrak');
        $this->db->join('jenis_kegiatan_pengadaan', 'jadwal_kegiatan_kontrak.jenis_kegiatan_id=jenis_kegiatan_pengadaan.id', 'left');
        $this->db->join('kontrak_surat_penawaran', 'jadwal_kegiatan_kontrak.kontrak_surat_penawaran_id=kontrak_surat_penawaran.id', 'left');
        
        $this->db->where('jadwal_kegiatan_kontrak.kontrak_surat_penawaran_id', $kontrak_surat_penawaran_id);
        
        $i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($kontrak_surat_penawaran_id)
	{
		$this->_get_datatables_query($kontrak_surat_penawaran_id);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($kontrak_surat_penawaran_id)
	{
		$this->_get_datatables_query($kontrak_surat_penawaran_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($kontrak_surat_penawaran_id)
	{
		$this->db->select('jadwal_kegiatan_kontrak.id as id, 
                        jenis_kegiatan_pengadaan.nama as jenis_kegiatan_nama,
                        jadwal_kegiatan_kontrak.tgl as tgl, 
                        jadwal_kegiatan_kontrak.jam as jam');

        $this->db->from('jadwal_kegiatan_kontrak');
        $this->db->join('jenis_kegiatan_pengadaan', 'jadwal_kegiatan_kontrak.jenis_kegiatan_id=jenis_kegiatan_pengadaan.id', 'left');
        $this->db->join('kontrak_surat_penawaran', 'jadwal_kegiatan_kontrak.kontrak_surat_penawaran_id=kontrak_surat_penawaran.id', 'left');
        
        $this->db->where('jadwal_kegiatan_kontrak.kontrak_surat_penawaran_id', $kontrak_surat_penawaran_id);

		return $this->db->count_all_results();
	}
	/*====== /Serverside Datatable ======*/
}

/* End of file Jadwal_kegiatan_kontrak_model.php */
