<!DOCTYPE html>
<HtmL>

<HeAd>
    <MeTa HTTP-EquIV="CONTENT-TYPE" CONTent="text/html; charset=utf-8">
    <TItlE>Semarang, 29 Maret 2004</tITLE>
    <meta name=GENERATOR coNTeNT="LibreOffice 4.1.6.2 (Linux)">
    <Meta nAMe="AUTHOR" CONTeNt="DPU">
    <MEtA NaMe=CREATED cONtENt="Tahun_Anggaran0817;20400000000000">
    <MeTA NAmE="CHANGEDBY" CoNteNt="isan">
    <META NAme="CHANGED" coNTent="20190320;163019000000000">
    <MEtA nAmE=KSOProductBuildVer CONTEnt=1033-10.1.0.6757>
    <sTyLE>
        @page {
            size: 8.47in 13.98in;
            margin-right: 0.88in;
            margin-top: 0.59in;
            margin-bottom: 0.69in
        }
        P {
            margin-bottom: 0.08in;
            direction: ltr;
            color: #000000
        }
        P.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        P.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
        H1 {
            margin-left: 3.5in;
            margin-top: 0in;
            margin-bottom: 0in;
            direction: ltr;
            color: #000000;
            text-align: justify;
            text-decoration: underline
        }
        H1.western {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.cjk {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: en-US
        }
        H1.ctl {
            font-family: "Times New Roman", serif;
            font-size: 12pt;
            so-language: ar-SA
        }
    </sTYLE>
</heAd>

<bOdY LANg="en-US" tExT=#000000 dIR="LTR">
    <p>
        <Img SRc="<?php print(base_url('assets/template_surat/img/kop_surat_dpu_monokrom.png'));?>" nAme="Picture 8" width="100%" ALIGN=center BORDER=0>
    </p>
    <p clAsS="western" ALIGN="center" StyLE="margin-bottom: 0in">
        <u><strong>SURAT PERINTAH MULAI KERJA (SPMK)</strong></u><br/>
        Nomor : <?php print($kontrak_surat->no_surat); ?>
    </p>
    
    <p></p>
    
    <p clAsS="western" ALIGN="justify" StyLE="margin-bottom: 0in">
    Berdasarkan Surat Perintah Kerja Nomor <?php print((!empty($spk[0]->no_surat))?$spk[0]->no_surat:''); ?>, Tanggal 
    <?php
    if(!empty($spk[0]->tgl_surat)) {
        print(tgl_indo($spk[0]->tgl_surat));
    }
    else {
        print('');
    }
    ?>, dengan ini Kuasa Pengguna Anggaran Selaku Pejabat Pembuat Komitmen Judul_Program Kegiatan Judul_Kegiatan , memerintahkan kepada
    </p>
    <p></p>
    <table cellpadding="2">
        <tr>
            <td class="text" style="font-size:12pt">Nama</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($direktur_perusahaan);?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Jabatan</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt">Direktur <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt">Alamat</td>
            <td class="text" style="font-size:12pt">:</td>
            <td class="text" style="font-size:12pt"><?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_alamat); ?></td>
        </tr>
        <tr>
            <td class="text" style="font-size:12pt" valign="top">NPWP</td>
            <td class="text" style="font-size:12pt" valign="top">:</td>
            <td class="text" style="font-size:12pt">
                <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_npwp); ?>
            </td>
        </tr>
    </table>
    <p></p>
    <p clAsS="western" ALIGN="justify" StyLE="margin-bottom: 0in">
        Untuk melaksanakan :
    </p>
    <p></p>
    <table cellpadding="2">
        <tr>
            <td class="text" valign="top" style="font-size:12pt">Program</td>
            <td class="text" valign="top" style="font-size:12pt">:</td>
            <td class="text" valign="top" style="font-size:12pt"><?php print($kontrak_pekerjaan->program_nama); ?></td>
        </tr>
        <tr>
            <td class="text" valign="top" style="font-size:12pt">Kegiatan</td>
            <td class="text" valign="top" style="font-size:12pt">:</td>
            <td class="text" valign="top" style="font-size:12pt"><?php print($kontrak_pekerjaan->pekerjaan_nama); ?></td>
        </tr>
        <tr>
            <td class="text" valign="top" style="font-size:12pt">Pekerjaan</td>
            <td class="text" valign="top" style="font-size:12pt">:</td>
            <td class="text" valign="top" style="font-size:12pt"><?php print($kontrak_pekerjaan->aktivitas_nama); ?></td>
        </tr>
        <tr>
            <td class="text" valign="top" style="font-size:12pt">No. Rekening Belanja</td>
            <td class="text" valign="top" style="font-size:12pt">:</td>
            <td class="text" valign="top" style="font-size:12pt"><?php print($kontrak_pekerjaan->aktivitas_no_rekening); ?></td>
        </tr>
        <tr>
            <td class="text" valign="top" style="font-size:12pt">Sumber Dana</td>
            <td class="text" valign="top" style="font-size:12pt">:</td>
            <td class="text" valign="top" style="font-size:12pt"><?php print($kontrak_pekerjaan->aktivitas_sumber_dana); ?></td>
        </tr>
        <tr>
            <td class="text" valign="top" style="font-size:12pt">Harga Borongan</td>
            <td class="text" valign="top" style="font-size:12pt">:</td>
            <td class="text" valign="top" style="font-size:12pt">
                <?php print('Rp '.format_money($kontrak_pekerjaan->harga_negosiasi).',-'); ?><br/>
                <?php print(ucwords(strtolower(terbilang($kontrak_pekerjaan->harga_negosiasi))).' Rupiah'); ?>
            </td>
        </tr>
        <tr>
            <td class="text" valign="top" style="font-size:12pt">Waktu Pelaksanaan</td>
            <td class="text" valign="top" style="font-size:12pt">:</td>
            <td class="text" valign="top" style="font-size:12pt">
                Selama <?php print($kontrak_pekerjaan->durasi_kontrak); ?> 
                (<?php print(terbilang($kontrak_pekerjaan->durasi_kontrak)); ?>) hari kalender<br/>
                <ul>
                    <li>Mulai pelaksanaan tanggal <?php print(tgl_indo($kontrak_pekerjaan->tgl_awal_kontrak)); ?></li>
                    <li>Selesai Pelaksanaan tanggal <?php print(tgl_indo($kontrak_pekerjaan->tgl_akhir_kontrak)); ?></li>
                </ul>
            </td>
        </tr>
    </table>
    <p></p>
    <p clAsS="western" ALIGN="justify" StyLE="margin-bottom: 0in">
        Demikian Surat Perintah Mulai Kerja (SPMK) ini disampaikan untuk dilaksanakan sebaik – baiknya dan penuh tanggung jawab.
    </p>
    <p></p>
    <p></p>
    <table>
        <tr>
            <td width="300" align="center" style="font-size:16pt">
                Setuju:<br/>
                <?php print($kontrak_pihak_ketiga[0]->pihak_ketiga_nama);?><br/>
                <br/><br/><br/><br/><br/><br/><br/><br/><br/>
                <u><?php print($direktur_perusahaan);?></u><br/>
                Direktur
            </td>
            <td width="300"></td>
            <td width="400" align="center" style="font-size:16pt">
                A.n. Kepala Dinas Pekerjaan Umum<br/>
                Kota Semarang<br/>
                Kuasa Pengguna Anggaran<br/>
                Selaku Pejabat Pembuat Komitmen<br/>
                Tahun Anggaran <?php print((!empty($pejabat_pembuat_komitmen->tahun))?$pejabat_pembuat_komitmen->tahun:''); ?><br/>
                <br/><br/><br/><br/><br/>
                <u><?php print((!empty($pejabat_pembuat_komitmen->pegawai_nama))?$pejabat_pembuat_komitmen->pegawai_nama:''); ?></u><br/>
                NIP. <?php print((!empty($pejabat_pembuat_komitmen->pegawai_nip))?$pejabat_pembuat_komitmen->pegawai_nip:''); ?>
            </td>
        </tr>
    </table>

</BODY>

</HTML>