<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kontrak_pihak_ketiga_model extends CI_Model {

    /*----- kontrak_pihak_ketiga -----*/
    // id
	// kontrak_pekerjaan_id
	// pihak_ketiga_id
	// bank_id
	// no_rekening
	// nilai_kontrak
	// lampiran
	// created_at
    // updated_at
    
    var $column_order = array(null, 'kontrak_pihak_ketiga.id',
                    'pihak_ketiga.nama_perusahaan',
                    'kontrak_pihak_ketiga.nilai_kontrak', 
                    'kontrak_pihak_ketiga.lampiran'); 

    var $column_search = array('kontrak_pihak_ketiga.id',
                    'pihak_ketiga.nama_perusahaan',
                    'kontrak_pihak_ketiga.nilai_kontrak', 
                    'kontrak_pihak_ketiga.lampiran');

    // default order 
    var $order = array('kontrak_pihak_ketiga.id' => 'asc');

    public function fetch_data($id=NULL, $kontrak_pekerjaan_id=NULL, $pihak_ketiga_id=NULL)
    {
        $sql = $this->db->select('kontrak_pihak_ketiga.id as id, 
								kontrak_pihak_ketiga.kontrak_pekerjaan_id as kontrak_pekerjaan_id,
								kontrak_pekerjaan.tgl_awal_kontrak as tgl_awal_kontrak,
								kontrak_pekerjaan.tgl_akhir_kontrak as tgl_akhir_kontrak,
								datediff(kontrak_pekerjaan.tgl_akhir_kontrak, kontrak_pekerjaan.tgl_awal_kontrak) as durasi_kontrak,
								kontrak_pekerjaan.hps as hps,
								pihak_ketiga.id as pihak_ketiga_id,
								pihak_ketiga.nama_perusahaan as pihak_ketiga_nama,
								pihak_ketiga.alamat_lengkap as pihak_ketiga_alamat,
								pihak_ketiga.no_telepon as pihak_ketiga_no_telepon,
								pihak_ketiga.npwp as pihak_ketiga_npwp,
								bank.id as bank_id,
								bank.nama as bank_nama,
								kontrak_pihak_ketiga.no_rekening as no_rekening,
                                kontrak_pihak_ketiga.nilai_kontrak as nilai_kontrak,
								kontrak_pihak_ketiga.lampiran as lampiran,
								aktivitas.nama_aktivitas as nama_aktivitas,
								pejabat_pembuat_komitmen_view.pegawai_id as pejabat_pembuat_komitmen_id,
								pejabat_pembuat_komitmen_view.pegawai_nama_lengkap as pejabat_pembuat_komitmen_nama,
								pejabat_pengadaan.id as pejabat_pengadaan_id,
                                pejabat_pengadaan_view.pegawai_nama_lengkap as pejabat_pengadaan_nama')
                        ->from('kontrak_pihak_ketiga')
						->join('pihak_ketiga', 'kontrak_pihak_ketiga.pihak_ketiga_id=pihak_ketiga.id', 'left')
						->join('kontrak_pekerjaan', 'kontrak_pekerjaan.id=kontrak_pihak_ketiga.kontrak_pekerjaan_id', 'left')
						->join('aktivitas', 'kontrak_pekerjaan.aktivitas_id=aktivitas.id', 'left')
						// ->join('pegawai', 'pegawai.id=kontrak_pekerjaan.pejabat_pembuat_komitmen_id', 'left')
						->join('pejabat_pembuat_komitmen', 'aktivitas.ppkom=pejabat_pembuat_komitmen.id', 'left')
						->join('view_data_pegawai as pejabat_pembuat_komitmen_view', 'pejabat_pembuat_komitmen_view.pegawai_id=pejabat_pembuat_komitmen.pegawai_id', 'left')
						->join('pejabat_pengadaan', 'kontrak_pekerjaan.pejabat_pengadaan_id=pejabat_pengadaan.id', 'left')
						->join('view_data_pegawai as pejabat_pengadaan_view', 'pejabat_pengadaan_view.pegawai_id=pejabat_pengadaan.pegawai_id','right')
						->join('bank', 'kontrak_pihak_ketiga.bank_id=bank.id', 'left');

        if($id!=NULL){
            $sql->where('kontrak_pihak_ketiga.id', $id);
        }

        if($kontrak_pekerjaan_id!=NULL) {
            $sql->where('kontrak_pihak_ketiga.kontrak_pekerjaan_id', $kontrak_pekerjaan_id);
		}
		
		if ($pihak_ketiga_id!=NULL) {
			$sql->where('kontrak_pihak_ketiga.pihak_ketiga_id', $pihak_ketiga_id);
		}

        if($id!=NULL) {
            return $sql->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }

    /*====== Serverside Datatable ======*/
	private function _get_datatables_query($kontrak_pekerjaan_id)
	{
        $this->db->select('kontrak_pihak_ketiga.id as id,
                        kontrak_pihak_ketiga.kontrak_pekerjaan_id as kontrak_pekerjaan_id,
                        pihak_ketiga.id as pihak_ketiga_id,
                        pihak_ketiga.nama_perusahaan as pihak_ketiga_nama,
                        kontrak_pihak_ketiga.nilai_kontrak as nilai_kontrak,
						kontrak_pihak_ketiga.lampiran as lampiran,
						');

        $this->db->from('kontrak_pihak_ketiga');
        $this->db->join('kontrak_pekerjaan', 'kontrak_pihak_ketiga.kontrak_pekerjaan_id=kontrak_pekerjaan.id', 'left');
        $this->db->join('pihak_ketiga', 'kontrak_pihak_ketiga.pihak_ketiga_id=pihak_ketiga.id', 'left');
        
        $this->db->where('kontrak_pihak_ketiga.kontrak_pekerjaan_id', $kontrak_pekerjaan_id);
        
        $i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($kontrak_pekerjaan_id)
	{
		$this->_get_datatables_query($kontrak_pekerjaan_id);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($kontrak_pekerjaan_id)
	{
		$this->_get_datatables_query($kontrak_pekerjaan_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($kontrak_pekerjaan_id)
	{
		$this->db->select('kontrak_pihak_ketiga.id as id,
                        kontrak_pihak_ketiga.kontrak_pekerjaan_id as kontrak_pekerjaan_id,
                        pihak_ketiga.id as pihak_ketiga_id,
                        pihak_ketiga.nama_perusahaan as pihak_ketiga_nama,
                        kontrak_pihak_ketiga.nilai_kontrak as nilai_kontrak,
                        kontrak_pihak_ketiga.lampiran as lampiran');

        $this->db->from('kontrak_pihak_ketiga');
        $this->db->join('kontrak_pekerjaan', 'kontrak_pihak_ketiga.kontrak_pekerjaan_id=kontrak_pekerjaan.id', 'left');
        $this->db->join('pihak_ketiga', 'kontrak_pihak_ketiga.pihak_ketiga_id=pihak_ketiga.id', 'left');
        
        $this->db->where('kontrak_pihak_ketiga.kontrak_pekerjaan_id', $kontrak_pekerjaan_id);

		return $this->db->count_all_results();
	}
	/*====== /Serverside Datatable ======*/

}

/* End of file Kontrak_pihak_ketiga_model.php */
