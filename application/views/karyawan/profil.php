<?php
$this->load->view('template/header');?>
<!-- Select2 -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/select2/dist/css/select2.min.css')); ?>">
<?php $this->load->view('template/asset_header');?>
<link rel="stylesheet" type="text/css" href="<?php print(base_url('assets/bower_components/datatables.net-bs/css/custom_button_export.css'))?>" />
<link href="<?php echo base_url('assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css')?>" rel="stylesheet">

<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <!-- Box Form Pengunjung -->
        <?php 
            print($this->session->flashdata('alert')); 
            print($this->session->flashdata('success')); 
            $sess_data = $this->session->userdata('session_data');
            if($karyawan_id!=NULL){
              $id = $karyawan_id;
            }
            else {
              $id = $sess_data['id'];
            }
            $pihak_ketiga_id = $this->session->userdata('pihak_ketiga_id');
        ?>
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-warning" id="form-pengunjung">
              <div class="box-header with-border">
                <h3 class="box-title">Detail Profil Karyawan</h3>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <form class="form-horizontal" >
                <div class="box-body">  
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Perusahaan</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="perusahaan"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Nama Lengkap</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="nama_lengkap"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Jabatan</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="jabatan"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Jenjang Pendidikan</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="jenjang_pendidikan"></div>
                    </div>
                  </div>  
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Jenis Kelamin</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="jenis_kelamin"></div>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Alamat Lengkap</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="alamat_lengkap"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Tgl Lahir</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="tgl_lahir"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">No Telepon</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="no_telepon"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="email"></div>
                    </div>
                  </div> 
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Dokumen Profil</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="dokumen_profil"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Dokumen Pendidikan</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="dokumen_pendidikan"></div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Dokumen Lainnya</label>
                    <div class="col-sm-9">
                      <div class="form-control" id="dokumen_lainnya"></div>
                    </div>
                  </div>                                 
                </div>
                <div class="box-footer">
                  <?php if($karyawan_id!=NULL) {?>
                    <a href="<?php print(base_url('pihak_ketiga/detail/'.$this->session->userdata('pihak_ketiga_id')));?>" class="btn btn-primary pull-left">Kembali</a>
                    <div class="pull-right">
                      <a href="#" class="btn btn-primary" onclick="edit_profil()">Edit</a> | 
                      <a href="#" class="btn btn-danger" onclick="delete_karyawan(<?php print($id); ?>)">Delete</a>
                    </div>
                  <?php } else {?>
                    <a href="#" class="btn btn-primary" onclick="edit_profil()">Edit Profil</a>
                  <?php } ?>
                </div>
                <!-- /.box-footer -->
              </form>

               

            </div>
          </div>
          <!-- /Box Form Pengunjung -->

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<script src="<?php echo base_url('assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')?>"></script>
<script>
var url_file = "<?php print(base_url('assets/uploads/documents_karyawan/'))?>";

$(function(){
  detail_data();

  $('#form-karyawan').submit(function(obj){
      obj.preventDefault();

      url_dokumen = "<?php echo site_url('karyawan/ajax_update/')?>";

      $.ajax({
          url: url_dokumen,
          type: 'POST',
          data: new FormData(this),
          processData: false,
          contentType: false,
          success: function(datas){
              console.log(datas);

              // Parsing to object
              datas=JSON.parse(datas);                    
              
              //if success close modal and reload ajax table dokumen
              if(datas.hasOwnProperty('status') && datas.status) 
              {
                  alert('Data berhasil tersimpan');
                  $('#modal_form_karyawan').modal('hide');
                  detail_data();
                  $('#form-karyawan')[0].reset(); // reset form on modals
              }
              else
              {
                  if(datas.hasOwnProperty('inputerror')) {
                      for (var i = 0; i < datas.inputerror.length; i++) 
                      {
                          $('[name="'+datas.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                          $('[name="'+datas.inputerror[i]+'"]').next().text(datas.error_string[i]); //select span help-block class set text error string
                      }
                  }
              }
              $('#btnSaveDokumen').text('save'); //change button text
              $('#btnSaveDokumen').attr('disabled',false); //set button enable 
          },
          error: function(jqXHR, textStatus, errorThrown) {
              alert('Error calling save data. Please check again');
              $('#modal_form_karyawan').modal('hide');
          }
      });
  });

  $('.datepicker').datepicker({
      autoclose: true,
      format: "yyyy-mm-dd",
      todayHighlight: true,
      todayBtn: true,
      todayHighlight: true,  
  });

  $("select").change(function(){
      if($('[name="is_user"]').val()=='y') {
          $('.data-akun').show();
      }
      else {
          $('.data-akun').hide();
          $('[name="username"]').val('');
          $('[name="password"]').val('');
      }
  });

  //Just number input
  $(".number").on("keypress keyup",function (event) {    
      $(this).val($(this).val().replace(/[^\d].+/, ""));
      if ((event.which < 48 || event.which > 57)) {
          event.preventDefault();
      }
  });
});

function detail_data(){
  $.ajax({
    url: "<?php echo site_url('karyawan/ajax_edit/'.$id)?>",
    method: 'GET',
    type: 'JSON',
    success: function(data) {
        console.log(url_file);
        // Parsing to object
        data=JSON.parse(data);

        $('#perusahaan').text(data.pihak_ketiga_nama);
        $('#nama_lengkap').text(data.nama_lengkap);
        $('#jabatan').text(data.jabatan_nama);
        $('#jenjang_pendidikan').text(data.pendidikan_nama);
        $('#jenis_kelamin').text(data.jenis_kelamin);
        $('#alamat_lengkap').text(data.alamat_lengkap);
        $('#tgl_lahir').text(data.tgl_lahir);
        $('#no_telepon').text(data.no_telepon);
        $('#email').text(data.email);
        if(data.dokumen_profil) {
          $('#dokumen_profil').html("<a href='"+url_file+data.dokumen_profil+"' target='_blank'>Dokumen Profil</a>");
        }
        else {
          $('#dokumen_profil').text('Kosong');
        }

        if(data.dokumen_pendidikan) {
          $('#dokumen_pendidikan').html("<a href='"+url_file+data.dokumen_pendidikan+"' target='_blank'>Dokumen Pendidikan</a>");
        }
        else {
          $('#dokumen_pendidikan').text('Kosong');
        }

        if(data.dokumen_lainnya) {
          $('#dokumen_lainnya').html("<a href='"+url_file+data.dokumen_lainnya+"' target='_blank'>Dokumen Lainnya</a>");
        }
        else {
          $('#dokumen_lainnya').text('Kosong');
        }
    },
    error: function(jqXHR, textStatus, errorThrown) {
        alert('Error while load detail profil pegawai !');
    }
  });
}

function edit_profil() {
    save_method_karyawan = 'update';
    $('#form-karyawan')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('karyawan/ajax_edit/'.$id)?>",
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="nama_lengkap"]').val(data.nama_lengkap);
            $('[name="jabatan_id"]').val(data.jabatan_id);
            $('[name="jenjang_pendidikan_id"]').val(data.pendidikan_id);
            $('[name="jenis_kelamin"]').val(data.jenis_kelamin);
            $('[name="alamat_lengkap"]').val(data.alamat_lengkap);
            $('[name="tgl_lahir"]').datepicker('update', data.tgl_lahir);
            $('[name="no_telepon"]').val(data.no_telepon);
            $('[name="email"]').val(data.email);
            $('[name="is_user"]').val(data.is_user);

            if(data.is_user=='y') {
                $('.data-akun').show();
                $('[name="username"]').val(data.username);
            }
            
            $('#modal_form_karyawan').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Update Data'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}


function save_profil()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    
    // ajax adding data to database
    $.ajax({
        url : "<?php echo site_url('pegawai/ajax_update')?>",
        type: "POST",
        data: $('#form-profil').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            // console.log(data);
            // data=JSON.parse(data);

            if(data.status) //if success close modal and reload ajax table
            {
                alert('Data profil berhasil diupdate !');
                $('#modal_form_profil').modal('hide');
                detail_data();
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(jqXHR+' | '+textStatus+' | '+errorThrown);
            alert('Error update profil !');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

// Delete karyawan
function delete_karyawan(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('karyawan/ajax_delete')?>/"+id,
            method: 'POST',
            type: 'JSON',
            success: function(datas)
            {
                alert('Data berhasil dihapus');
                window.location.replace("<?php print(base_url('pihak_ketiga/detail/'.$pihak_ketiga_id)); ?>");
                
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}
</script>
<?php 
  $this->load->view('karyawan/modal_form');
  $this->load->view('template/footer');
?>