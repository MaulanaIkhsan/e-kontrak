<?php
$this->load->view('template/header');?>
<!-- Select2 -->
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/select2/dist/css/select2.min.css')); ?>">
<?php $this->load->view('template/asset_header');?>
<!-- Star Rating -->
<link rel="stylesheet" href="<?php print(base_url('assets/plugins/rating/dist/starrr.css')); ?>">


<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <!-- Box Form Pengunjung -->
        <?php print($this->session->flashdata('alert')); ?>
        <?php print($this->session->flashdata('success')); ?>
        <div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-primary" id="form-pengunjung">
              <div class="box-header with-border">
                <h3 class="box-title">Formulir Kunjungan</h3>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <form class="form-horizontal" method="post" action="<?php print(base_url('kunjungan/update/'.$kunjungan->id))?>">
                <div class="box-body">
                  <?php 
                  // Condition to make show data pengunjung that has been previously selected
                  if($this->session->userdata('pengunjung_id')==TRUE){ ?>
                    <div class="form-group">
                      <label for="namalengkap" class="col-sm-2 control-label">Nama Lengkap</label>
                      <div class="col-sm-9">
                        <input type="text" class="form-control" name="namalengkap" maxlength="20" value="<?php print($pengunjung->nama);?>" readonly="readonly">
                        <span class="text-danger col-md-8"><?php echo form_error('NamaLengkap'); ?></span>
                        <input type="hidden" class="form-control" name="idpengunjung" maxlength="20" value="<?php print($pengunjung->id);?>" readonly="readonly">
                        <span class="text-danger col-md-8"><?php echo form_error('IdPengunjung'); ?></span>
                      </div>
                    </div>
                  <?php } else { ?>
                     <div class="form-group">
                      <label for="namalengkap" class="col-sm-2 control-label">Nama Lengkap</label>
                      <div class="col-sm-9">
                        <select class="form-control" name="namalengkap" id="namalengkap" required="required">
                          <?php foreach($pengunjung as $item):
                          if($item->id == $kunjungan->tamu_id) {?>
                            <option value="<?php print($item->id); ?>" selected="selected"><?php print($item->nama.' | '.$item->nik); ?></option>
                          <?php } else { ?>
                            <option value="<?php print($item->id); ?>"><?php print($item->nama.' | '.$item->nik); ?></option>
                          <?php } endforeach;?>
                        </select>
                      </div>
                    </div>
                  <?php } ?>
                 
                  <div class="form-group">
                    <label for="keperluan" class="col-sm-2 control-label">Keperluan</label>
                    <div class="col-sm-9">
                    <textarea class="form-control" rows="3" placeholder="Cantumkan keperluan anda" maxlength="500" name="keperluan" required="required"><?php print($kunjungan->keperluan);?></textarea>
                    <span class="text-danger col-md-8"><?php echo form_error('Keperluan'); ?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">Tujuan</label>
                    <div class="col-sm-9">                        
                        <select class="form-control" name="tujuan" id="tujuan" required="required">
                          <?php foreach($unit_kerja as $item):
                          if($item->id == $kunjungan->tujuan) {?>
                          <option value="<?php print($item->id); ?>" selected="selected"><?php print($item->nama); ?></option>
                          <?php } else { ?>
                          <option value="<?php print($item->id); ?>"><?php print($item->nama); ?></option>
                          <?php } endforeach;?>
                        </select>
                        <span class="text-danger col-md-8"><?php echo form_error('Tujuan'); ?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Penilaian</label>
                    <div class="col-sm-9">
                      <div class='starrr' id='star'></div>
                      <br />
                      <input type='hidden' name='rating' id='star_input' value="<?php print($kunjungan->rating); ?>" />
                      <span class="text-danger col-md-8"><?php echo form_error('Rating'); ?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="namalengkap" class="col-sm-2 control-label">Kritik dan Saran</label>
                    <div class="col-sm-9">
                     <textarea rows="3" class="form-control" placeholder="Masukan untuk DPU Kota Semarang" maxlength="500" name="masukan" required="required" maxlength="500"><?php print($kunjungan->masukan); ?></textarea>
                      <span class="text-danger col-md-8"><?php echo form_error('Masukan'); ?></span>
                    </div>
                  </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <a href="<?php print(base_url('kunjungan/detail/'.$kunjungan->id)); ?>" class="btn btn-primary">Kembali</a> | 
                  <button type="submit" name="save" class="btn btn-success" value="simpan">Simpan</button>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
          </div>
          <!-- /Box Form Pengunjung -->

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<!-- Select2 -->
<script src="<?php print(base_url('assets/bower_components/select2/dist/js/select2.full.min.js'));?>"></script>
<!-- Star Rating -->
<script type="text/javascript" src="<?php print(base_url('assets/plugins/rating/dist/starrr.js')); ?>"></script>
<script>
  $(function(){
    $('#nohp').bind('keyup paste', function(){
      this.value = this.value.replace(/[^0-9]/g, '');
    });

    //Initialize Select2 Elements
     $('#tujuan, #namalengkap').select2();

    var $s2input = $('#star_input');
    $('#star').starrr({
      max: 10,
      rating: $s2input.val(),
      change: function(e, value){
        $s2input.val(value).trigger('input');
      }
    });
  });
</script>
<?php $this->load->view('template/footer');?>