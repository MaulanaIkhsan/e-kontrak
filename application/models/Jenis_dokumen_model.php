<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_dokumen_model extends CI_Model {

    // ----- jenis_dokumen_pihak_ketiga -----
    // id
    // nama
    // is_series
    // created_at
    // updated_at
        
    public function fetch_data($id=NULL, $is_series=NULL)
    {
        $sql = $this->db->select('jenis_dokumen_pihak_ketiga.id as id, 
                                jenis_dokumen_pihak_ketiga.nama as nama, 
                                jenis_dokumen_pihak_ketiga.is_series as is_series')
                        ->from('jenis_dokumen_pihak_ketiga');
        
        if($id!=NULL and $is_series!=NULL) {
            return $sql->where('jenis_dokumen_pihak_ketiga.id', $id)
                        ->where('jenis_dokumen_pihak_ketiga.is_series', 'y')
                        ->get()->row();
        }
        elseif ($id==NULL and $is_series!=NULL) {
            return $sql->where('jenis_dokumen_pihak_ketiga.is_series', 'y')
                        ->order_by('jenis_dokumen_pihak_ketiga.nama', 'asc')
                        ->get()->result();
        }
        elseif ($id!=NULL and $is_series==NULL) {
            return $sql->where('jenis_dokumen_pihak_ketiga.id', $id)
                        ->get()->row();
        }
        
        return $sql->order_by('jenis_dokumen_pihak_ketiga.nama', 'asc')
                    ->get()->result();
    }

}

/* End of file Jenis_dokumen_model.php */
