<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Consume_monev extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $this->load->library('curl');
		$this->load->helper('generic');
    }
    

    public function index()
	{
		$this->load->model('Unit_kerja_model', 'unit_kerja');
		$this->load->model('Jenis_pekerjaan_model', 'jenis_pekerjaan');
		$this->load->model('Jenis_pengadaan_model', 'jenis_pengadaan');
		$this->load->model('Pegawai_model', 'pegawai');
		$this->load->model('Aktivitas_model', 'aktivitas');
				
		$tahun = date("Y");
		
		/*--- Struktur data API monev get_aktivitas ---*/
		// pekerjaan_kode
		// pekerjaan_nama
		// pekerjaan_tahun
		// aktivitas_nama
		// anggaran_awal
		// perubahan_anggaran
		// hps
		// nilai_kontrak
		// tgl_pengajuan_pencairan
		// tgl_pencairan
		// keterangan
		// pptk_nama
		// pptk_unit_kerja

		// ----- aktivitas -----
		// id
		// nama_aktivitas
		// no_rekening
		// sumber_dana
		// anggaran_awal
		// perubahan_anggaran
		// hps
		// nilai_kontrak
		// tgl_pengajuan_pencairan
		// tgl_pencairan
		// keterangan
		// pptk
		// ppkom
		// bendahara
		// pekerjaan_id
		// pengadaan_id
		// jenis_pekerjaan_id
		// created_at
		// updated_at
		// created_by
		// updated_by

		// ----- unit_kerja -----
        // id
        // nama
        // created_at
		// updated_at

		// ----- jenis_pekerjaan -----
        // id
		// nama
		// nickname
        // created_at
		// updated_at

		// ----- jenis_pengadaan -----
        // id
		// nama
		// nickname
        // created_at
		// updated_at

		// ----- Pegawai -----
        // id
        // nip
        // nama_lengkap
        // jenis_kelamin ('Pria', 'Wanita')
        // alamat
        // no_telepon
        // is_pegawai ('y','n')
        // unit_id
        // jabatan_id
        // role_id
        // username
        // password
        // created_at
        // updated_at

		$link = 'http://monevpu.semarangkota.go.id/api/';
		$params = array('tahun'=>$tahun);
		
		$data_monev = json_decode($this->curl->simple_get($link, $params));

		print_r($data_monev);
		die;
		
		$jenis_pengadaan 	= (!empty($this->jenis_pengadaan->show()))?object_to_array($this->jenis_pengadaan->show()):NULL;
		$jenis_pekerjaan 	= (!empty($this->jenis_pekerjaan->show()))?object_to_array($this->jenis_pekerjaan->show()):NULL;
		$unit_kerja 		= (!empty($this->unit_kerja->fetch_data()))?object_to_array($this->unit_kerja->fetch_data()):NULL;
		$pegawai			= (!empty($this->pegawai->fetch_data()))?object_to_array($this->pegawai->fetch_data()):NULL;
		$aktivitas			= '';

		$sess_jenis_pengadaan = NULL;
		if(!empty($this->session->userdata('sess_jenis_pengadaan'))) {
			$sess_jenis_pengadaan = $this->session->userdata('sess_jenis_pengadaan');
		}
		else {
			$this->session->set_userdata('sess_jenis_pengadaan', $jenis_pengadaan);
			$sess_jenis_pengadaan = $jenis_pengadaan;
		}

		$sess_jenis_pekerjaan = NULL;
		if(!empty($this->session->userdata('sess_jenis_pekerjaan'))) {
			$sess_jenis_pekerjaan = $this->session->userdata('sess_jenis_pekerjaan');
		}
		else {
			$this->session->set_userdata('sess_jenis_pekerjaan', $jenis_pekerjaan);
			$sess_jenis_pekerjaan = $jenis_pekerjaan;
		}

		$sess_unit_kerja = NULL;
		if(!empty($this->session->userdata('sess_unit_kerja'))) {
			$sess_unit_kerja = $this->session->userdata('sess_unit_kerja');
		}
		else {
			$this->session->set_userdata('sess_unit_kerja', $unit_kerja);
			$sess_unit_kerja = $unit_kerja;
		}

		$sess_pegawai = NULL;
		if(!empty($this->session->userdata('sess_pegawai'))) {
			$sess_pegawai = $this->session->userdata('sess_pegawai');
		}
		else {
			$this->session->set_userdata('sess_pegawai', $pegawai);
			$sess_pegawai = $pegawai;
		}
		
		// print_r($this->session->userdata('sess_jenis_pekerjaan'));
		// print('<br/>');
		// print_r($jenis_pekerjaan);
		// print('<br/>');
		// print('----------------------------------------<br/>');
		// print_r($this->session->userdata('sess_jenis_pengadaan'));
		// print('<br/>');
		// print('----------------------------------------<br/>');
		// print_r($this->session->userdata('sess_unit_kerja'));
		// print('<br/>');
		// print('----------------------------------------<br/>');
		// print_r($this->session->userdata('sess_pegawai'));
		// print('<br/>');
		// print('----------------------------------------<br/>');
		if(!empty($data_monev) and $data_monev) {
			foreach($data_monev as $item):
				// Check unit kerja
				print($item->pptk_unit_kerja.'<br/>');
			endforeach;
		}

	}

}

/* End of file Consume_monev.php */
