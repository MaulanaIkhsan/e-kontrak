<?php
$this->load->view('template/header');?>

<?php $this->load->view('template/asset_header');?>
<link rel="stylesheet" type="text/css" href="<?php print(base_url('assets/bower_components/datatables.net-bs/css/custom_button_export.css'))?>" />

<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        <?php print($this->session->flashdata('alert')); ?>
        <?php print($this->session->flashdata('success')); ?>
        <div class="col-md-12">
            <!-- Horizontal Form -->
          <div class="box box-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Data Penyedia Barang/Jasa</h3>
              </div>
              <!-- /.box-header -->
              
              <!-- form start -->
              <div class="box-body">
                <p>
                    <button class="btn btn-primary" onclick="add_pihak_ketiga()">Insert Data</button>
                </p>
                <table id="pihak_ketiga" class="table table-bordered table-striped table-responsive">
                    <thead>
                        <tr class="headings" align="center">
                            <th class="column-title" align="center">No</th>
                            <th class="column-title" align="center">Nama Perusahaan</th>
                            <th class="column-title" align="center">Alamat Lengkap</th>
                            <th class="column-title" align="center">No. Telepon</th>
                            <th class="column-title" align="center">Email</th>
                            <th class="column-title" align="center">Status</th>
                            <th class="column-title" align="center">Aksi</th>
                        </tr>
                    </thead>
                  <tbody></tbody>
                </table>
              </div> 

            </div>
          </div>

        </div>
    </section>
</div>
<?php $this->load->view('template/asset_footer');?>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/dataTables.buttons.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.flash.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/jszip.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/pdfmake.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/vfs_fonts.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.html5.min.js'))?>"></script>
<script type="text/javascript" src="<?php print(base_url('assets/bower_components/datatables.net-bs/js/buttons.print.min.js'))?>"></script>

<script>
    var save_method; //for save method string
    var table;

  $(function(){
    $.fn.dataTable.ext.errMode = 'none';
    
    table = $('#pihak_ketiga').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
            "ajax": {
                "url": "<?php echo site_url('pihak_ketiga/get_data');?>",
                "type": "POST"
            },
            dom: 'lBfrtip',
            button:[{
              extend:'pdf',
              oriented:'potrait',
              pageSize: 'A4',
              title : 'Data Pihak Ketiga',
              download : 'open'
            }, 'copy', 'csv', 'excel', 'pdf', 'print'],
            "columnDefs": [{ 
                "targets": [ 0 ], 
                "orderable": false, 
            }]
        });
    
    $('.data-pusat').hide();

    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();

        if($('[name="status"]').val()=='Cabang') {
            $('.data-pusat').show();
        }
        else {
            $('.data-pusat').hide();
            $('[name="alamat_pusat"]').val('');
            $('[name="no_telepon_pusat"]').val('');
            $('[name="email_pusat"]').val('');
        }
    });

    //Just number input
    $(".number").on("keypress keyup",function (event) {    
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

  });

    function add_pihak_ketiga() {
        save_method = 'add';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form_pihak_ketiga').modal('show'); // show bootstrap modal
        $('.modal-title').text('Insert Data'); // Set Title to Bootstrap modal title
        $('.admin-user').append('<input type="checkbox" name="admin-user" value="admin-user"> Create admin user');
    }

    function reload_table()
    {
        table.ajax.reload(); //reload datatable ajax 
    }

    function save_pihak_ketiga()
    {
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable 
        var url;

        if(save_method == 'add') {
            url = "<?php echo site_url('pihak_ketiga/ajax_add')?>";
        } else {
            url = "<?php echo site_url('pihak_ketiga/ajax_update')?>";
        }

        // ajax adding data to database
        $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {

                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form_pihak_ketiga').modal('hide');
                    alert('Data berhasil tersimpan');
                    reload_table();
                }
                else
                {
                    for (var i = 0; i < data.inputerror.length; i++) 
                    {
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    }
                }
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 


            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 

            }
        });
    }

</script>

<?php 
$this->load->view('pihak_ketiga/modal_form');
?>


<?php $this->load->view('template/footer');?>