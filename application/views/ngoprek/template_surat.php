<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>

<HEAD>
	<META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
	<TITLE>Semarang, 29 Maret 2004</TITLE>
	<META NAME="GENERATOR" CONTENT="LibreOffice 4.1.6.2 (Linux)">
	<META NAME="AUTHOR" CONTENT="DPU">
	<META NAME="CREATED" CONTENT="Tahun_Anggaran0817;20400000000000">
	<META NAME="CHANGEDBY" CONTENT="isan">
	<META NAME="CHANGED" CONTENT="20190320;163019000000000">
	<META NAME="KSOProductBuildVer" CONTENT="1033-10.1.0.6757">
	<STYLE TYPE="text/css">
		< !-- @page {
			size: 8.47in 13.98in;
			margin-right: 0.88in;
			margin-top: 0.59in;
			margin-bottom: 0.69in
		}

		P {
			margin-bottom: 0.08in;
			direction: ltr;
			color: #000000
		}

		P.western {
			font-family: "Times New Roman", serif;
			font-size: 12pt;
			so-language: en-US
		}

		P.cjk {
			font-family: "Times New Roman", serif;
			font-size: 12pt;
			so-language: en-US
		}

		P.ctl {
			font-family: "Times New Roman", serif;
			font-size: 12pt;
			so-language: ar-SA
		}

		H1 {
			margin-left: 3.5in;
			margin-top: 0in;
			margin-bottom: 0in;
			direction: ltr;
			color: #000000;
			text-align: justify;
			text-decoration: underline
		}

		H1.western {
			font-family: "Times New Roman", serif;
			font-size: 12pt;
			so-language: en-US
		}

		H1.cjk {
			font-family: "Times New Roman", serif;
			font-size: 12pt;
			so-language: en-US
		}

		H1.ctl {
			font-family: "Times New Roman", serif;
			font-size: 12pt;
			so-language: ar-SA
		}

		-->
	</STYLE>
</HEAD>

<BODY LANG="en-US" TEXT="#000000" DIR="LTR">
	<P LANG="id-ID" CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<BR>
	</P>
	<P LANG="id-ID" CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<BR>
	</P>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<IMG SRC="SPK_mulai_1__html_c71e3fec.png" NAME="Picture 8" ALIGN=BOTTOM WIDTH=111 HEIGHT=130 BORDER=0></P>
	<P LANG="sv-SE" CLASS="western" STYLE="margin-bottom: 0in">
		<FONT SIZE=5><B> </B></FONT>
	</P>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4 STYLE="font-size: 16pt">PEMERINTAH
				KOTA SEMARANG</FONT>
		</FONT>
	</P>
	<BR>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=5><B>SURAT
					PERINTAH KERJA </B></FONT>
		</FONT>
	</P>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=5><B>(SPK)</B></FONT>
		</FONT>
	</P>
	<BR>
	<P CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4><SPAN LANG="sv-SE"><B>Nomor
						: no_kontrak Tanggal : tgl_kontrak </B></SPAN></FONT>
		</FONT>
	</P>
	<BR>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER>
		<FONT FACE="Tahoma, Ubuntu, sans-serif">PROGRAM Judul_Program </FONT>
	</P>
	<BR>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER>
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4><B>Kegiatan </B></FONT>
		</FONT>
	</P>
	<P LANG="id-ID" CLASS="western" ALIGN=CENTER>
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4>Judul_Kegiatan</FONT>
		</FONT>
	</P>
	<BR>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER>
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4><B>Pekerjaan </B></FONT>
		</FONT>
	</P>
	<P LANG="id-ID" CLASS="western" ALIGN=CENTER>
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4>Judul_Pekerjaan</FONT>
		</FONT>
	</P>
	<BR>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4><B>Antara</B></FONT>
		</FONT>
	</P>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER>
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4>Kuasa Pengguna
				Anggaran</FONT>
		</FONT>
	</P>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER>
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4>Selaku Pejabat
				Pembuat Komitmen</FONT>
		</FONT>
	</P>
	<P CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4><SPAN LANG="sv-SE">Pekerjaan -replace-</SPAN></FONT>
		</FONT>
	</P>
	<BR>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER>
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4><B>Dengan</B></FONT>
		</FONT>
	</P>
	<div LANG="sv-SE" CLASS="western" ALIGN=CENTER>
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4>nama_perusahaan</FONT>
		</FONT>
	</div>
	<div CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=2 STYLE="font-size: 11pt"><SPAN LANG="sv-SE"> alamat_perusahaan</SPAN></FONT>
		</FONT>
	</div><BR />
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4 STYLE="font-size: 16pt"><B>Nilai
					Kontrak</B></FONT>
		</FONT>
	</P>
	<P LANG="sv-SE" CLASS="western" ALIGN=CENTER>
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4>Nilai_Kontrak</FONT>
		</FONT>
	</P>
	<P CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=2 STYLE="font-size: 11pt"><SPAN LANG="sv-SE">(nilai_kontrak_terbilang)</SPAN></FONT>
		</FONT>
	</P>
	<BR>
	<P CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif"><SPAN LANG="sv-SE">Kode
				Rekening</SPAN></FONT>
		<FONT FACE="Tahoma, Ubuntu, sans-serif"><SPAN LANG="sv-SE"><B>
					: </B></SPAN></FONT>
		<FONT FACE="Tahoma, Ubuntu, sans-serif"><SPAN LANG="sv-SE"><B>NPWP_Perusahaan</FONT>
	</P>
	<P CLASS="western" ALIGN=CENTER STYLE="margin-bottom: 0in">
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4 STYLE="font-size: 16pt"><SPAN LANG="sv-SE">Sumber
					Dana : </SPAN></FONT>
		</FONT>
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4 STYLE="font-size: 16pt"><SPAN LANG="sv-SE"><B>APBD</B></SPAN></FONT>
		</FONT>
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4 STYLE="font-size: 16pt"><SPAN LANG="id-ID"><B>
					</B></SPAN></FONT>
		</FONT>
		<FONT FACE="Tahoma, Ubuntu, sans-serif">
			<FONT SIZE=4 STYLE="font-size: 16pt"><SPAN LANG="sv-SE"><B>Kota
						Semarang T.A. Tahun_Anggaran</FONT>
	</P>

	<p ClASs=western ALIGN=CENTER sTylE="margin-bottom: 0in; page-break-before: none">
        <B>BERITA ACARA HASIL PENGADAAN LANGSUNG</b>
    </P>
    <p ClaSs=western ALIGN=CENTER stYlE="margin-bottom: 0in"><b>(B A H P L</B>
        <fOnt SIZE=2 STyLE="font-size: 11pt"><B>)</B></fOnt>
    </P>
    <P claSs=western sTYLe="margin-bottom: 0in">
        <fOnt SIZE=2 stylE="font-size: 11pt"><b>------------------------------------------------------------------------------------------------------------</B></FOnt>
        <FOnT SIZE=2 STyLe="font-size: 11pt"><SpAn LANg=id-ID><b>--------------------</b></Span></FONt>
    </P>
    <P ClASs=western ALIGN=CENTER STYle="margin-bottom: 0in">
        <FOnT SIZE=2 StYLE="font-size: 11pt">Nomor
: No_BAHPL</FOnT>
    </P>
    <p cLAss=western ALIGN=CENTER stylE="margin-bottom: 0in">
        <BR>
    </P>
    <p CLaSs=western ALIGN=JUSTIFY sTYle="margin-bottom: 0in">
        <FONt SIZE=2 Style="font-size: 11pt">Pada
hari ini, </fOnT>
        <Font SIZE=2 style="font-size: 11pt"><B>Hari_Nego,
</b></foNt>
        <FOnt SIZE=2 STYLE="font-size: 11pt">tanggal </fOnT>
        <fOnt SIZE=2 StyLE="font-size: 11pt"><b>tgl_trblgnego</B></FOnT><font SIZE=2 STYLE="font-size: 11pt">
bulan </Font>
        <FONT SIZE=2 StylE="font-size: 11pt"><B>bln_trblgnego </B></fOnT>
        <FOnT SIZE=2 StYle="font-size: 11pt">tahun
</fOnt>
        <FONT SIZE=2 stYle="font-size: 11pt"><B>thn_trblgnego</B>
</B></span></fONT>
        <FonT SIZE=2 stYlE="font-size: 11pt"><B>(Tanggal_Nego)</b></fONt>
        <FONt SIZE=2 STYLE="font-size: 11pt">,
bertempat di Kantor Dinas </FoNt>
        <fOnT SIZE=2 sTYLe="font-size: 11pt"><SPaN LaNG="id-ID">Pekerjaan
Umum</sPAn></FonT>
        <fOnT SIZE=2 sTYLe="font-size: 11pt"> Kota
Semarang, yang bertanda tangan dibawah ini :</fOnt>
    </P>
    <p ClaSS="western" ALIGN=JUSTIFY stYle="margin-bottom: 0in">
    </P>
    <P CLaSS="western" ALIGN=JUSTIFY stYle="margin-bottom: 0in">
        <fONT SIZE=2 StYLE="font-size: 11pt"><spAn lanG=pt-BR><B>	NamaPejabat_Pengadaan</B></SPan></fONT>
    </P>
    <p LanG="pt-BR" CLaSs="western" ALIGN=JUSTIFY stYLE="text-indent: 0.49in; margin-bottom: 0in">
    </p>
    <p clAss="western" ALIGN=JUSTIFY STyLe="margin-bottom: 0in">
        <FonT SIZE=2 STyLe="font-size: 11pt"><SPan LaNG="pt-BR">Selaku
Pejabat Pengadaan pada Dinas </SpAN></FONT>
        <FonT SIZE=2 stYLE="font-size: 11pt"><spAN laNg=id-ID>Pekerjaan
Umum</sPaN></FONT>
        <FoNT SIZE=2 StylE="font-size: 11pt"><SPaN LAng="pt-BR">
Kota Semarang Tahun Anggaran Tahun_Anggaran<FONT SIZE=2 stYLe="font-size: 11pt"><spAn lang="pt-BR">,
yang dibentuk berdasarkan Keputusan Kepala Dinas </sPAN></FonT><font SIZE=2 STYLe="font-size: 11pt"><sPaN lANG=id-ID>Pekerjaan
Umum</sPan></fONt>
        <FoNt SIZE=2 sTYle="font-size: 11pt"><SPaN LANg=pt-BR>
Kota Semarang Selaku Pengguna Anggaran Nomor </SPaN></Font>
        <FonT SIZE=2 styLe="font-size: 11pt"><spAN Lang="id-ID">NoSK_PjbtPengadaan</sPAN></fOnt>
        <fonT SIZE=2 stYLe="font-size: 11pt"><SPan lANG="pt-BR">
tanggal </spaN></FOnT><font SIZE=2 stYle="font-size: 11pt"><sPAN lAnG=id-ID>tglSK_Pjbtpengadaan</span></FoNt>
        <fONT SIZE=2 stYlE="font-size: 11pt"><SPaN laNg="pt-BR">
</SpaN></foNT>
        <FoNt SIZE=2 StYlE="font-size: 11pt"><span lAnG=id-ID>blnSK_pjbtpengadaan
</sPAn></fONT>
        <fONT SIZE=2 sTyle="font-size: 11pt"><spaN lAnG=pt-BR>Tahun_Anggaran<fOnT SIZE=2 StYLE="font-size: 11pt"><spaN LAnG=pt-BR>
tentang Penunjukan Pejabat Pengadaan Pada Dinas </SPAN></FonT>
        <fonT SIZE=2 stylE="font-size: 11pt"><SPAn lANg="id-ID">Pekerjaan
Umum</sPan></Font>
        <fOnt SIZE=2 sTylE="font-size: 11pt"><sPAN LANG=pt-BR>
kota Semarang </spAn></foNT>
        <FOnT SIZE=2 sTyLE="font-size: 11pt"><SpAN LANG=id-ID>APBD
</SpAn></fonT>
        <fonT SIZE=2 StyLe="font-size: 11pt"><sPan laNg=pt-BR>Tahun
Anggaran Tahun_Anggaran<fONt SIZE=2 STylE="font-size: 11pt"><SpAN Lang="pt-BR">,
telah melakukan Penyusunan Hasil Pengadaan</span></FONt>
        <FONt SIZE=2 sTYle="font-size: 11pt"><SPan Lang=id-ID>
Langsung (BAHPL)</spAN></Font>
        <fonT SIZE=2 StylE="font-size: 11pt"><SpAn Lang=pt-BR>,
untuk</SPAn></foNt>
        <FONt SIZE=2 StyLe="font-size: 11pt"><SPaN lAng="id-ID">:</sPAn></FOnT>
    </p>

    <BR>
    <fonT SIZE=2 Style="font-size: 11pt">Program		:
	Judul_Program</fONT>
    <br>
    <FoNt SIZE=2 sTyLE="font-size: 11pt">Kegiatan		:
	Judul_Kegiatan</FonT>
    <Br>
    <FOnT SIZE=2 StYle="font-size: 11pt">Pekerjaan 		:</FONt>
    <foNt SIZE=2 STyLe="font-size: 11pt"><B>Judul_Pekerjaan</b></FoNT>
    <BR>
    <fONt SIZE=2 sTyle="font-size: 11pt">Sumber
Dana			:  APBD</FONT>
    <foNT SIZE=2 styLe="font-size: 11pt">Kota
Semarang T.A. Tahun_Anggaran</FOnt>

    <p cLass=western ALIGN=JUSTIFY stYLe="margin-bottom: 0in"><font SIZE=2 style="font-size: 11pt">Adapun
hal-hal yang disepakati adalah sebagai berikut :</FONT>
    </p>
    <P CLASS=western ALIGN=JUSTIFY Style="margin-bottom: 0in">
        <FOnt SIZE=2 styLe="font-size: 11pt">Pelaksanaan
pengadaan barang/jasa telah dilaksanakan sesuai Peraturan Presiden
Republik Indonesia Nomor </FoNt>
        <FOnt SIZE=2 sTyLe="font-size: 11pt"><sPaN LaNg=id-ID>4</spAn></fOnt>
        <FONT SIZE=2 STYLE="font-size: 11pt">
Tahun 201</fonT>
        <Font SIZE=2 sTylE="font-size: 11pt"><SPan lANg=id-ID>5</sPaN></FOnT>
        <foNT SIZE=2 StYLe="font-size: 11pt">
tentang Perubahan </FonT>
        <FonT SIZE=2 STYLE="font-size: 11pt"><SpaN LANG="id-ID">keempat</SpAN></FOnt>
        <fONt SIZE=2 sTYLE="font-size: 11pt">
atas Peraturan Presiden Nomor 54 Tahun 2010 tentang Pengadaan
Barang/Jasa Pemerintah dan telah melalui tahapan-tahapan kegiatan.</Font>
    </P>
    <p cLAss="western" ALIGN=JUSTIFY StYle="margin-bottom: 0in">
    </p>
    <p CLaSs=western ALIGN=CENTER sTYLE="margin-bottom: 0in">
        <FOnT SIZE=2 STYLe="font-size: 11pt"><b>TAHAP
PROSES PENGADAAN LANGSUNG</b></fOnt>
    </p>
    <ol>
        <LI>
            <P cLAsS=western ALIGN=JUSTIFY StYLe="margin-bottom: 0in">
                <fONT SIZE=2 stYLe="font-size: 11pt">Perusahaan
	yang diundang sebanyak 1 (satu) dan memasukkan dokumen sebanyak 1
	(satu) Perusahaan</fOnt>
            </P>
    </oL>
    <Ol START=2>
        <Li>
            <P cLASs="western" ALIGN=JUSTIFY stylE="margin-bottom: 0in">
                <Font SIZE=2 sTYLe="font-size: 11pt">Pemasukkan
	dan Pembukaan Dokumen Penawaran</FOnt>
            </p>
    </oL>
    <ul>
        <Li>
            <P cLASs="western" ALIGN=JUSTIFY stYlE="margin-bottom: 0in">
                <fOnT SIZE=2 StyLE="font-size: 11pt">Dokumen
	penawaran yang disampaikan oleh penyedia barang/jasa, setelah dibuka
	dan diteliti dinyatakan memenuhi syarat sehingga dapat dilanjutkan
	evaluasi.</Font>
            </P>
            <li>
                <p ClASs=western ALIGN=JUSTIFY stylE="margin-bottom: 0in">
                    <foNt SIZE=2 style="font-size: 11pt">Hasil
	Pembukaan Dokumen Penawaran adalah sebagai berikut :</FONt>
                </p>
    </uL>
    <P cLAsS="western" ALIGN=JUSTIFY style="margin-left: 0.5in; margin-bottom: 0in">
        <fOnT SIZE=2 StyLe="font-size: 11pt">HPS/OE	:	kontrak_HPS</fOnt>
    </P>
    <dl>
        <Dd>
            <taBle WIDTH=649 CELLPADDING=7 CELLSPACING=0>
                <Col WIDTH=85>
                    <COl WIDTH=297>
                        <COL WIDTH=189>
                            <tr>
                                <tD WIDTH=85 sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P CLAsS="western" ALIGN=CENTER>
                                        <fOnT SIZE=2 stYLE="font-size: 11pt"><b>No.
				URUT</b></fOnt>
                                    </p>
                                </td>
                                <td WIDTH=297 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <P cLass=western ALIGN=CENTER>
                                        <FONT SIZE=2 StYle="font-size: 11pt"><b>NAMA
				PENYEDIA JASA</b></fOnT>
                                    </P>
                                </Td>
                                <td WIDTH=156 StYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <P cLAss="western" ALIGN=CENTER stYLE="margin-bottom: 0in">
                                        <fONT SIZE=2 sTyLE="font-size: 11pt"><b>NILAI
				PENAWARAN (Rp) </B></FoNT>
                                    </p>
                                </Td>
                            </tr>
                            <tR VALIGN=TOP>
                                <TD WIDTH=85 sTyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p Class=western ALIGN=CENTER>
                                        <fONT SIZE=2 stYlE="font-size: 11pt">1</fONT>
                                    </P>
                                </Td>
                                <Td WIDTH=297 sTYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                    <p ClasS="western">nama_perusahaan
                                        <fONt SIZE=2 stYlE="font-size: 11pt"><SPaN Lang="pt-BR">	</spaN></FONT>
                                    </p>
                                </tD>
                                <td WIDTH=156 StYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                    <P cLass=western ALIGN=CENTER>nilai_penawaran</P>
                                </tD>
                            </Tr>
            </taBle>
    </dl>
    <p ClASs=western ALIGN=CENTER sTylE="margin-bottom: 0in; page-break-before: always">
        <ol START=3>
            <LI>
                <P claSS=western ALIGN=JUSTIFY stYle="margin-bottom: 0in">
                    <fOnt SIZE=2 sTyLE="font-size: 11pt">Unsur-unsur
	yang dievaluasi</fOnt>
                </P>
        </OL>
        <oL TYPE="a" START=1>
            <li>
                <p ClaSs="western" ALIGN=JUSTIFY sTyle="margin-bottom: 0in">
                    <FoNT SIZE=2 STylE="font-size: 11pt">Evaluasi
	Administrasi</FONT>
                </p>
        </oL>
        <dl>
            <DD>
                <taBLe WIDTH=649 CELLPADDING=7 CELLSPACING=0>
                    <COL WIDTH=23>
                        <COL WIDTH=246>
                            <CoL WIDTH=46>
                                <coL WIDTH=72>
                                    <cOL WIDTH=56>
                                        <cOl WIDTH=86>
                                            <tR>
                                                <tD ROWSPAN=3 WIDTH=23 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p LAnG=pt-BR CLasS="western" ALIGN=CENTER>
                                                        <fONt SIZE=2 STyle="font-size: 11pt">NO</Font>
                                                    </p>
                                                </Td>
                                                <Td ROWSPAN=3 WIDTH=196 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p LaNG=pt-BR clasS=western ALIGN=CENTER>
                                                        <FoNt SIZE=2 STYle="font-size: 11pt">NAMA
				DOKUMEN</FonT>
                                                    </P>
                                                </TD>
                                                <TD COLSPAN=2 WIDTH=132 VALIGN=TOP sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P lanG=pt-BR clASs=western ALIGN=CENTER>
                                                        <FONT SIZE=2 STyLe="font-size: 11pt">KELENGKAPAN</foNt>
                                                    </p>
                                                </td>
                                                <tD ROWSPAN=3 WIDTH=56 STYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P LanG=pt-BR CLAsS=western ALIGN=CENTER>
                                                        <fonT SIZE=2 sTyLe="font-size: 11pt">Tdk
				ada</FoNT>
                                                    </P>
                                                </TD>
                                                <TD ROWSPAN=3 WIDTH=136 StyLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p laNg="pt-BR" cLASs="western" ALIGN=CENTER>
                                                        <FoNT SIZE=2 STYle="font-size: 11pt">KETERANGAN</FONt>
                                                    </p>
                                                </Td>
                                            </Tr>
                                            <tR VALIGN=TOP>
                                                <tD COLSPAN=2 WIDTH=132 sTylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P Lang=pt-BR cLass=western ALIGN=CENTER>
                                                        <FoNt SIZE=2 STYlE="font-size: 11pt">Ada
				(+)</Font>
                                                    </P>
                                                </Td>
                                            </Tr>
                                            <Tr VALIGN=TOP>
                                                <td WIDTH=46 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p LAnG="pt-BR" CLasS=western ALIGN=JUSTIFY>
                                                        <FONT SIZE=2 sTYLE="font-size: 11pt">Sesuai
				</font>
                                                    </p>
                                                </TD>
                                                <Td WIDTH=72 StyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p lang="pt-BR" ClAss="western" ALIGN=JUSTIFY>
                                                        <fOnT SIZE=2 STyLE="font-size: 11pt">Tdk
				Sesuai</fONt>
                                                    </P>
                                                </TD>
                                            </Tr>
                                            <tr VALIGN=TOP>
                                                <td WIDTH=23 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P laNg="pt-BR" cLASS="western" ALIGN=CENTER>
                                                        <foNt SIZE=2 STyle="font-size: 11pt">1</font>
                                                    </P>
                                                </tD>
                                                <tD WIDTH=196 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P lAng=pt-BR class=western ALIGN=JUSTIFY>
                                                        <foNT SIZE=2 sTylE="font-size: 11pt">Ditandatangai
				oleh pihak sebagaimana dalam ketentuan</foNt>
                                                    </P>
                                                </td>
                                                <Td WIDTH=46 stYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P lAng="pt-BR" ClaSs=western ALIGN=CENTER>
                                                        <fonT SIZE=2 StyLE="font-size: 11pt">+</foNT>
                                                    </p>
                                                </TD>
                                                <TD WIDTH=72 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P LaNg=pt-BR clasS="western" ALIGN=JUSTIFY>
                                                        <bR>
                                                    </P>
                                                </td>
                                                <tD WIDTH=56 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P LANG=pt-BR ClAsS=western ALIGN=JUSTIFY>
                                                        <bR>
                                                    </P>
                                                </tD>
                                                <tD WIDTH=136 STylE="border: 1px solid #000000; padding: 0in 0.08in">
                                                </tD>
                                            </TR>
                                            <Tr VALIGN=TOP>
                                                <Td WIDTH=23 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p LanG="pt-BR" ClASS=western ALIGN=CENTER>
                                                        <fOnT SIZE=2 sTYlE="font-size: 11pt">2</FonT>
                                                    </p>
                                                </tD>
                                                <TD WIDTH=196 style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p LaNG=pt-BR claSS="western" ALIGN=JUSTIFY>
                                                        <FOnt SIZE=2 styLE="font-size: 11pt">Mencantumkan
				penawaran harga</Font>
                                                    </p>
                                                </Td>
                                                <td WIDTH=46 style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P lANG="pt-BR" Class=western ALIGN=CENTER>
                                                        <fONt SIZE=2 StyLe="font-size: 11pt">+</fOnT>
                                                    </p>
                                                </Td>
                                                <tD WIDTH=72 STyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p Lang=pt-BR CLaSs="western" ALIGN=JUSTIFY>
                                                        <bR>
                                                    </P>
                                                </Td>
                                                <td WIDTH=56 StYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p laNG=pt-BR cLasS="western" ALIGN=JUSTIFY>
                                                        <BR>
                                                    </p>
                                                </td>
                                                <td WIDTH=136 sTYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P lANG="pt-BR" ClAsS=western ALIGN=JUSTIFY>
                                                        <Br>
                                                    </P>
                                                </tD>
                                            </Tr>
                                            <tr VALIGN=TOP>
                                                <tD WIDTH=23 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P LAnG=pt-BR cLASs="western" ALIGN=CENTER>
                                                        <FoNT SIZE=2 sTYle="font-size: 11pt">3</font>
                                                    </p>
                                                </TD>
                                                <TD WIDTH=196 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P lanG="pt-BR" ClaSS=western ALIGN=JUSTIFY>
                                                        <fONt SIZE=2 stYLE="font-size: 11pt">Jangka
				waktu surat penawaran</FoNT>
                                                    </P>
                                                </td>
                                                <tD WIDTH=46 STyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p lanG="pt-BR" clAsS="western" ALIGN=CENTER>
                                                        <fonT SIZE=2 styLe="font-size: 11pt">+</fONT>
                                                    </P>
                                                </tD>
                                                <td WIDTH=72 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P LANG=pt-BR clasS=western ALIGN=JUSTIFY>
                                                        <Br>
                                                    </P>
                                                </Td>
                                                <TD WIDTH=56 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p LAng=pt-BR ClasS="western" ALIGN=JUSTIFY>
                                                        <Br>
                                                    </p>
                                                </TD>
                                                <Td WIDTH=136 STyLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p LaNG=pt-BR clAsS="western" ALIGN=JUSTIFY>
                                                        <br>
                                                    </P>
                                                </Td>
                                            </tR>
                                            <TR VALIGN=TOP>
                                                <tD WIDTH=23 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P Lang="pt-BR" ClAss="western" ALIGN=CENTER>
                                                        <FoNT SIZE=2 sTYlE="font-size: 11pt">4</Font>
                                                    </P>
                                                </td>
                                                <tD WIDTH=196 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P LANg="pt-BR" clasS=western ALIGN=JUSTIFY>
                                                        <FonT SIZE=2 Style="font-size: 11pt">Jangka
				waktu pelaksanaan</FONt>
                                                    </P>
                                                </TD>
                                                <Td WIDTH=46 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P lANG=pt-BR cLASS=western ALIGN=CENTER>
                                                        <Font SIZE=2 stYLE="font-size: 11pt">+</fONT>
                                                    </P>
                                                </Td>
                                                <td WIDTH=72 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P LanG=pt-BR ClaSS="western" ALIGN=JUSTIFY>
                                                        <Br>
                                                    </P>
                                                </Td>
                                                <TD WIDTH=56 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P LAnG=pt-BR CLASS=western ALIGN=JUSTIFY>
                                                        <Br>
                                                    </p>
                                                </tD>
                                                <Td WIDTH=136 style="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p laNG=pt-BR CLASs="western" ALIGN=JUSTIFY>
                                                        <br>
                                                    </p>
                                                </td>
                                            </TR>
                                            <tR VALIGN=TOP>
                                                <td WIDTH=23 StYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P lAnG="pt-BR" ClaSS=western ALIGN=CENTER>
                                                        <fONT SIZE=2 stYLe="font-size: 11pt">5</fOnT>
                                                    </p>
                                                </tD>
                                                <td WIDTH=196 STyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p laNg=pt-BR cLAss=western ALIGN=JUSTIFY>
                                                        <foNT SIZE=2 STyLe="font-size: 11pt">Tanggal
				</FonT>
                                                    </P>
                                                </Td>
                                                <td WIDTH=46 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P lANG=pt-BR clASs=western ALIGN=CENTER>
                                                        <FOnt SIZE=2 sTYlE="font-size: 11pt">+</fOnt>
                                                    </P>
                                                </TD>
                                                <TD WIDTH=72 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P laNg=pt-BR CLAsS=western ALIGN=JUSTIFY>
                                                        <bR>
                                                    </p>
                                                </td>
                                                <Td WIDTH=56 sTyle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P LaNg=pt-BR clASS="western" ALIGN=JUSTIFY>
                                                        <bR>
                                                    </p>
                                                </Td>
                                                <TD WIDTH=136 styLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p Lang=pt-BR ClAsS=western ALIGN=JUSTIFY>
                                                        <bR>
                                                    </P>
                                                </td>
                                            </tR>
                                            <TR VALIGN=TOP>
                                                <td WIDTH=23 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p lANg="pt-BR" CLAsS="western" ALIGN=JUSTIFY>
                                                        <bR>
                                                    </p>
                                                </TD>
                                                <Td WIDTH=196 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P lANG=pt-BR cLasS=western ALIGN=JUSTIFY>
                                                        <fONt SIZE=2 stylE="font-size: 11pt">HASIL
				AKHIR</FonT>
                                                    </P>
                                                </TD>
                                                <tD WIDTH=46 StYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P lang="pt-BR" ClASs=western ALIGN=JUSTIFY>
                                                        <bR>
                                                    </p>
                                                </tD>
                                                <tD WIDTH=72 styLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P LanG=pt-BR clasS="western" ALIGN=JUSTIFY>
                                                        <BR>
                                                    </P>
                                                </td>
                                                <td WIDTH=56 StYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p LANG=pt-BR cLASS="western" ALIGN=JUSTIFY>
                                                        <Br>
                                                    </p>
                                                </tD>
                                                <TD WIDTH=136 stYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P cLASs="western" ALIGN=JUSTIFY>
                                                        <FoNt SIZE=2 StYLe="font-size: 11pt"><Span LanG="pt-BR">LULUS/</SPaN></FOnt>
                                                        <sTRIKe><FoNt SIZE=2 styLe="font-size: 11pt"><SPAN LaNg=pt-BR>TDK
				LULUS</SpAn></fonT></StrIKe>
                                                    </P>
                                                </Td>
                                            </Tr>
                </TAblE>
        </dl>
        <p lANG=id-ID cLasS=western ALIGN=JUSTIFY STyle="margin-left: 0.5in; margin-bottom: 0in">
            <br>
        </p>
        <OL TYPE="a" START=2>
            <LI>
                <p clASS=western ALIGN=JUSTIFY stYLe="margin-bottom: 0in">
                    <foNT SIZE=2 style="font-size: 11pt">Evaluasi
	Teknis</FONT>
                </p>
        </ol>
        <DL>
            <dd>
                <TAblE WIDTH=649 CELLPADDING=7 CELLSPACING=0>
                    <Col WIDTH=23>
                        <COL WIDTH=196>
                            <COL WIDTH=46>
                                <COL WIDTH=72>
                                    <COl WIDTH=56>
                                        <COL WIDTH=136>
                                            <tR>
                                                <td ROWSPAN=3 WIDTH=23 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P lanG="pt-BR" cLaSS=western ALIGN=CENTER><font SIZE=2 sTyLE="font-size: 11pt"><B>NO</B></FoNT>
                                                    </p>
                                                </Td>
                                                <td ROWSPAN=3 WIDTH=196 Style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P LANg=pt-BR cLass="western" ALIGN=CENTER>
                                                        <FOnt SIZE=2 style="font-size: 11pt"><B>NAMA
				DOKUMEN</B></font>
                                                    </P>
                                                </tD>
                                                <Td COLSPAN=2 WIDTH=132 VALIGN=TOP STylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P lANg=pt-BR ClaSs=western ALIGN=CENTER>
                                                        <foNt SIZE=2 stYlE="font-size: 11pt"><B>KELENGKAPAN</B></FOnt>
                                                    </p>
                                                </td>
                                                <TD ROWSPAN=3 WIDTH=56 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P lANg="pt-BR" cLasS=western ALIGN=CENTER>
                                                        <FoNt SIZE=2 STyle="font-size: 11pt"><B>Tdk
				ada</B></fONt>
                                                    </P>
                                                </tD>
                                                <TD ROWSPAN=3 WIDTH=136 StYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P LAng="pt-BR" CLaSs=western ALIGN=CENTER>
                                                        <Font SIZE=2 STyle="font-size: 11pt"><B>KETERANGAN</B></FoNT>
                                                    </p>
                                                </td>
                                            </Tr>
                                            <tR VALIGN=TOP>
                                                <Td COLSPAN=2 WIDTH=132 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p lANg="pt-BR" clasS=western ALIGN=CENTER>
                                                        <fOnt SIZE=2 sTYLE="font-size: 11pt"><b>Ada
				(+)</B></foNt>
                                                    </P>
                                                </tD>
                                            </tr>
                                            <tR VALIGN=TOP>
                                                <TD WIDTH=46 stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p LaNG=pt-BR clASs=western ALIGN=JUSTIFY>
                                                        <FONT SIZE=2 sTYlE="font-size: 11pt"><B>Sesuai
				</b></Font>
                                                    </p>
                                                </TD>
                                                <tD WIDTH=72 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p lang=pt-BR cLAss=western ALIGN=JUSTIFY>
                                                        <fOnT SIZE=2 stYLE="font-size: 11pt"><b>Tdk
				Sesuai</B></fONt>
                                                    </p>
                                                </tD>
                                            </Tr>
                                            <tR VALIGN=TOP>
                                                <tD WIDTH=23 STyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p LANg="pt-BR" Class=western ALIGN=CENTER>
                                                        <FONt SIZE=2 StylE="font-size: 11pt">1</FOnt>
                                                    </p>
                                                </tD>
                                                <td WIDTH=196 STylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P laNg=pt-BR cLASs=western ALIGN=JUSTIFY>
                                                        <Font SIZE=2 sTYlE="font-size: 11pt">Time
				Schedule</fOnt>
                                                    </P>
                                                </tD>
                                                <td WIDTH=46 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P LANg="pt-BR" ClAsS="western" ALIGN=CENTER><font SIZE=2 StyLE="font-size: 11pt">+</foNt>
                                                    </p>
                                                </Td>
                                                <tD WIDTH=72 sTYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p lAnG="pt-BR" cLASs=western ALIGN=JUSTIFY>
                                                        <br>
                                                    </p>
                                                </Td>
                                                <Td WIDTH=56 StyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p lANG=pt-BR ClAsS=western ALIGN=JUSTIFY>
                                                        <Br>
                                                    </p>
                                                </td>
                                                <TD WIDTH=136 sTyLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P lAng=pt-BR cLaSs="western" ALIGN=JUSTIFY>
                                                        <bR>
                                                    </P>
                                                </tD>
                                            </tR>
                                            <tR VALIGN=TOP>
                                                <TD WIDTH=23 STyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p lanG="pt-BR" CLAss=western ALIGN=CENTER>
                                                        <fonT SIZE=2 sTYlE="font-size: 11pt">2</Font>
                                                    </p>
                                                </td>
                                                <Td WIDTH=196 style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p LAnG=pt-BR CLass=western ALIGN=JUSTIFY>
                                                        <fONT SIZE=2 stYlE="font-size: 11pt">Jangka
				Waktu pelaksanaan</fonT>
                                                    </p>
                                                </Td>
                                                <tD WIDTH=46 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p LaNg=pt-BR CLasS=western ALIGN=CENTER>
                                                        <foNT SIZE=2 STylE="font-size: 11pt">+</FONt>
                                                    </p>
                                                </TD>
                                                <td WIDTH=72 styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p LaNG=pt-BR ClAss=western ALIGN=JUSTIFY>
                                                        <bR>
                                                    </p>
                                                </tD>
                                                <td WIDTH=56 stYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p LANG="pt-BR" ClaSs=western ALIGN=JUSTIFY>
                                                        <bR>
                                                    </P>
                                                </TD>
                                                <TD WIDTH=136 StYLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p LaNG=pt-BR cLASS=western ALIGN=JUSTIFY>
                                                        <BR>
                                                    </p>
                                                </TD>
                                            </TR>
                                            <tR VALIGN=TOP>
                                                <td WIDTH=23 stYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p lANG="pt-BR" clASS="western" ALIGN=JUSTIFY>
                                                        <BR>
                                                    </p>
                                                </TD>
                                                <TD WIDTH=196 stYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p lang=pt-BR clAsS=western ALIGN=JUSTIFY>
                                                        <FOnt SIZE=2 stYLe="font-size: 11pt">HASIL
				AKHIR</font>
                                                    </p>
                                                </tD>
                                                <Td WIDTH=46 StyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p lanG=pt-BR clASs="western" ALIGN=JUSTIFY>
                                                        <bR>
                                                    </P>
                                                </tD>
                                                <td WIDTH=72 StYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P lAnG="pt-BR" ClaSs=western ALIGN=JUSTIFY>
                                                        <BR>
                                                    </p>
                                                </Td>
                                                <td WIDTH=56 StylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p LAng=pt-BR clASS=western ALIGN=JUSTIFY>
                                                        <BR>
                                                    </P>
                                                </Td>
                                                <tD WIDTH=136 styLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P cLASS=western ALIGN=JUSTIFY>
                                                        <FonT SIZE=2 styLE="font-size: 11pt"><SPaN Lang="pt-BR">LULUS/</SpAn></fONT>
                                                        <sTrIKe><foNT SIZE=2 StYLe="font-size: 11pt"><Span LaNg=pt-BR>TDK
				LULUS</SPan></FoNT></STRike>
                                                    </p>
                                                </tD>
                                            </Tr>
                </tAble>
        </Dl>
        <P CLass=western ALIGN=JUSTIFY sTyle="margin-left: 0.5in; margin-bottom: 0in">
            <BR>
        </p>
        <Ol TYPE="a" START=3>
            <lI>
                <p cLasS="western" ALIGN=JUSTIFY stylE="margin-bottom: 0in">
                    <Font SIZE=2 stYLe="font-size: 11pt">Evaluasi
	Harga</FOnt>
                </p>
        </oL>
        <dL>
            <dD>
                <TABLe WIDTH=649 CELLPADDING=7 CELLSPACING=0>
                    <col WIDTH=23>
                        <col WIDTH=196>
                            <COL WIDTH=46>
                                <COl WIDTH=72>
                                    <COL WIDTH=56>
                                        <coL WIDTH=136>
                                            <Tr>
                                                <Td ROWSPAN=3 WIDTH=23 stYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P LAng=pt-BR ClASS=western ALIGN=CENTER>
                                                        <FOnt SIZE=2 StYle="font-size: 11pt"><B>NO</b></fonT>
                                                    </p>
                                                </tD>
                                                <td ROWSPAN=3 WIDTH=196 STyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P lang=pt-BR clASs="western" ALIGN=CENTER>
                                                        <FOnt SIZE=2 STyLe="font-size: 11pt"><B>NAMA
				DOKUMEN</b></FoNt>
                                                    </P>
                                                </tD>
                                                <Td COLSPAN=2 WIDTH=132 VALIGN=TOP stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P LaNG=pt-BR ClASs=western ALIGN=CENTER>
                                                        <FoNT SIZE=2 sTyLE="font-size: 11pt"><B>KELENGKAPAN</B></foNT>
                                                    </p>
                                                </TD>
                                                <tD ROWSPAN=3 WIDTH=56 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p lANG=pt-BR clASS="western" ALIGN=CENTER>
                                                        <FONt SIZE=2 sTylE="font-size: 11pt"><b>Tdk
				ada</B></FonT>
                                                    </p>
                                                </tD>
                                                <tD ROWSPAN=3 WIDTH=136 Style="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <P LaNG=pt-BR CLASs=western ALIGN=CENTER>
                                                        <FONT SIZE=2 Style="font-size: 11pt"><b>KETERANGAN</B></FONt>
                                                    </P>
                                                </Td>
                                            </tR>
                                            <Tr VALIGN=TOP>
                                                <td COLSPAN=2 WIDTH=132 sTyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p LaNG="pt-BR" CLAsS=western ALIGN=CENTER>
                                                        <fONt SIZE=2 styLe="font-size: 11pt"><b>Ada
				(+)</b></FONt>
                                                    </p>
                                                </Td>
                                            </tR>
                                            <TR VALIGN=TOP>
                                                <TD WIDTH=46 sTylE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P LanG="pt-BR" ClASs="western" ALIGN=JUSTIFY>
                                                        <fOnT SIZE=2 StYle="font-size: 11pt"><B>Sesuai
				</b></FoNT>
                                                    </P>
                                                </td>
                                                <tD WIDTH=72 StYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p Lang=pt-BR CLaSs=western ALIGN=JUSTIFY>
                                                        <FonT SIZE=2 StYLe="font-size: 11pt"><B>Tdk
				Sesuai</b></FOnt>
                                                    </P>
                                                </Td>
                                            </tr>
                                            <tR VALIGN=TOP>
                                                <Td WIDTH=23 StYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p LanG=pt-BR CLasS="western" ALIGN=CENTER>
                                                        <FoNt SIZE=2 sTyLe="font-size: 11pt">1</FoNT>
                                                    </P>
                                                </Td>
                                                <Td WIDTH=196 STYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P LANg=pt-BR ClaSS=western ALIGN=JUSTIFY>
                                                        <FoNT SIZE=2 StyLe="font-size: 11pt">Daftar
				Kuantitas dan Harga</FoNT>
                                                    </P>
                                                </td>
                                                <td WIDTH=46 STYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P LANg="pt-BR" cLASs="western" ALIGN=CENTER>
                                                        <FONT SIZE=2 styLe="font-size: 11pt">+</fONT>
                                                    </P>
                                                </tD>
                                                <TD WIDTH=72 STYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p LaNg="pt-BR" cLAsS="western" ALIGN=JUSTIFY>
                                                        <Br>
                                                    </p>
                                                </Td>
                                                <td WIDTH=56 stYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P lang="pt-BR" ClASs="western" ALIGN=JUSTIFY>
                                                        <BR>
                                                    </P>
                                                </Td>
                                                <Td WIDTH=136 stYLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p lAnG=pt-BR ClasS=western ALIGN=JUSTIFY>
                                                        <bR>
                                                    </P>
                                                </tD>
                                            </tr>
                                            <tr VALIGN=TOP>
                                                <Td WIDTH=23 STYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P LanG=pt-BR CLASS="western" ALIGN=JUSTIFY>
                                                        <Br>
                                                    </P>
                                                </Td>
                                                <tD WIDTH=196 STyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p LANg=pt-BR clAsS="western" ALIGN=JUSTIFY>
                                                        <Font SIZE=2 sTYlE="font-size: 11pt">HASIL
				AKHIR</FonT>
                                                    </p>
                                                </td>
                                                <TD WIDTH=46 STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p LAnG=pt-BR class=western ALIGN=JUSTIFY>
                                                        <Br>
                                                    </p>
                                                </TD>
                                                <Td WIDTH=72 sTYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P lANG=pt-BR ClASS=western ALIGN=JUSTIFY>
                                                        <bR>
                                                    </P>
                                                </td>
                                                <td WIDTH=56 STYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p Lang="pt-BR" cLasS=western ALIGN=JUSTIFY>
                                                        <BR>
                                                    </P>
                                                </TD>
                                                <td WIDTH=136 sTyLE="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p claSS=western ALIGN=JUSTIFY>
                                                        <Font SIZE=2 Style="font-size: 11pt"><SpaN LanG="pt-BR">LULUS/</SPAN></FonT>
                                                        <StrikE><FOnt SIZE=2 stYLE="font-size: 11pt"><SPan LAng=pt-BR>TDK
				LULUS</sPAn></fOnt></STriKE>
                                                    </p>
                                                </td>
                                            </tR>
                </TABLe>
        </Dl>
        <p LanG=id-ID clAsS=western ALIGN=JUSTIFY StylE="margin-left: 0.25in; margin-bottom: 0in">
            <Br>
        </p>
        <p ClASs=western ALIGN=CENTER sTylE="margin-bottom: 0in; page-break-before: always">
            <oL TYPE="a" START=4>
                <Li>
                    <P CLaSS=western ALIGN=JUSTIFY stylE="margin-bottom: 0in">
                        <FonT SIZE=2 sTYle="font-size: 11pt">Berdasarkan
	Berita Acara</FonT>
                        <FoNT SIZE=2 Style="font-size: 11pt"><Span lANg=id-ID>
	Evaluasi, </SpaN></fOnt>
                        <FoNT SIZE=2 StyLE="font-size: 11pt">Klarifikasi
	dan Negosiasi Nomor : </FonT>
                        <FOnT SIZE=2 sTyle="font-size: 11pt">29</FoNt>
                        <foNt SIZE=2 sTyle="font-size: 11pt"><SPaN lang="id-ID">
	</SpaN></fONt>
                        <fonT SIZE=2 StYLE="font-size: 11pt">tanggal Tanggal_Nego,
	</font>
                        <FOnT SIZE=2 StYlE="font-size: 11pt">Klarifikasi dilakukan
	terhadap 1 (satu) penawar yang responsive, dengan hasil sebagai
	berikut </font>
                    </p>
            </ol>
            <dL>
                <Dd>
                    <TABLe WIDTH=649 CELLPADDING=7 CELLSPACING=0>
                        <cOL WIDTH=34>
                            <Col WIDTH=186>
                                <COl WIDTH=129>
                                    <Col WIDTH=129>
                                        <CoL WIDTH=99>
                                            <Tr>
                                                <tD WIDTH=34 sTYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p ClASs="western" ALIGN=CENTER>
                                                        <fonT SIZE=2 STyle="font-size: 11pt"><b>No</b></fONt>
                                                    </p>
                                                </td>
                                                <Td WIDTH=186 VALIGN=TOP stYlE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p cLASS=western ALIGN=CENTER>
                                                        <FoNT SIZE=2 style="font-size: 11pt"><b>NAMA
				PERUSAHAAN</B></fOnt>
                                                    </P>
                                                </TD>
                                                <td WIDTH=129 VALIGN=TOP styLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p CLASs="western" ALIGN=CENTER>
                                                        <fonT SIZE=2 STYle="font-size: 11pt"><b>NILAI
				PENAWARAN</B></fONT>
                                                    </P>
                                                </td>
                                                <TD WIDTH=129 VALIGN=TOP sTyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P clasS=western ALIGN=CENTER>
                                                        <FoNT SIZE=2 sTyLE="font-size: 11pt"><b>NILAI
				NEGOSIASI</B></FOnt>
                                                    </P>
                                                </tD>
                                                <Td WIDTH=99 VALIGN=TOP styLe="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p ClasS="western" ALIGN=CENTER>
                                                        <fONT SIZE=2 StYle="font-size: 11pt"><B>HASIL
				KUALIFIKASI</B></fONT>
                                                    </p>
                                                </Td>
                                            </Tr>
                                            <Tr VALIGN=TOP>
                                                <TD WIDTH=34 sTyLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p cLAss=western ALIGN=CENTER><font SIZE=2 StYlE="font-size: 11pt">1</FOnT>
                                                    </p>
                                                </tD>
                                                <td WIDTH=186 StyLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p laNG="pt-BR" ClasS=western>nama_perusahaan</p>
                                                </tD>
                                                <td WIDTH=129 STYLe="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <p LAnG=pt-BR clasS=western ALIGN=CENTER>nilai_penawaran</p>
                                                </td>
                                                <tD WIDTH=129 STYle="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in">
                                                    <P LaNG=pt-BR cLass=western ALIGN=CENTER>68173000</p>
                                                </TD>
                                                <td WIDTH=99 stYle="border: 1px solid #000000; padding: 0in 0.08in">
                                                    <p clASS=western ALIGN=CENTER>
                                                        <FoNt SIZE=2 sTyLe="font-size: 11pt">LULUS</fONT>
                                                    </P>
                                                </Td>
                                            </tr>
                    </TABlE>
            </dL>
            <P CLASs="western" ALIGN=JUSTIFY sTYlE="margin-left: 0.25in; margin-bottom: 0in">

            </p>
            <P Class=western ALIGN=JUSTIFY STyLe="margin-bottom: 0in">
                <FonT SIZE=2 StYlE="font-size: 11pt"><SPAN laNG="fi-FI">Demikian
Berita Acara Penyusunan Hasil Pengadaan (BAHPL) Pekerjaan Judul_Pekerjaan
ini dibuat dengan sebenarnya, d</span></FoNT>
                <fONT SIZE=2 sTYlE="font-size: 11pt"><sPan lANG="pt-BR">itandatangani
oleh Pejabat Pengadaan, untuk digunakan sebagaimana mestinya.</sPan></FOnT>
            </P>
            <P LAng=fi-FI ClAsS=western ALIGN=JUSTIFY sTylE="margin-bottom: 0in">
                <bR>
            </p>
            <P clASS="western" ALIGN=JUSTIFY sTYLE="margin-bottom: 0in">
                <fOnt SIZE=2 style="font-size: 11pt"><sPAN LanG=pt-BR>				Pejabat
Pejabat_Pengadaan</sPaN></fONT>
            </P>
            <P CLAsS="western" ALIGN=JUSTIFY styLe="margin-bottom: 0in">
                <fOnT SIZE=2 STYLE="font-size: 11pt"><SpaN LanG=pt-BR>				DINAS
</spaN></FoNt>
                <fOnT SIZE=2 style="font-size: 11pt"><spAn laNg="id-ID">PEKERJAAN
UMUM</sPAN></fOnt>
                <foNt SIZE=2 sTyle="font-size: 11pt"><sPan lang="pt-BR">
KOTA SEMARANG</Span></fONT>
            </P>
            <p clasS=western ALIGN=JUSTIFY stylE="margin-bottom: 0in">
                <FONT SIZE=2 sTYle="font-size: 11pt"><spAN lanG=pt-BR>				APBD
TAHUN ANGGARAN Tahun_Anggaran</p>
<P LaNg=pt-BR claSS=western ALIGN=JUSTIFY stYLE="margin-bottom: 0in">
<Br>
</P>
<P lanG="pt-BR" ClASS=western ALIGN=JUSTIFY stylE="margin-bottom: 0in">
<Br>
</P>
<P clASS="western" ALIGN=JUSTIFY STYLe="margin-bottom: 0in"><FONT SIZE=2 sTYLe="font-size: 11pt"><spAn Lang="pt-BR">				</spAn></FoNt>
                <fOnt SIZE=2 style="font-size: 11pt"><SPAN lang="pt-BR"><U><b>NamaPejabat_Pengadaan</b></u></spAN></FoNT>
            </P>
            <P class=western ALIGN=JUSTIFY sTylE="margin-bottom: 0in">
                <fONt SIZE=2 styLe="font-size: 11pt"><SPAN LANG="pt-BR">				NIP.
NipPejabat_Pengadaan</spaN></FoNt>
            </P>
</BODY>

</HTML>