<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {

    /*----- karyawan -----*/
    // id
    // nama_lengkap
    // jabatan_id
    // jenjang_pendidikan_id
    // pihak_ketiga_id
    // jenis_kelamin (Pria, Wanita)
    // alamat_lengkap
    // tgl_lahir
    // no_telepon
    // email
    // dokumen_profil
    // dokumen_pendidikan
    // dokumen_lainnya
    // username
    // password
    // is_user (y, n)

    // Configuration upload file 
    var $conf_upload = array(
        'upload_path'   => "./assets/uploads/documents_karyawan/",
        'allowed_types' => "pdf",
        'max_size'      => '2000',
        'encrypt_name'  => FALSE
    );

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $sess_data = $this->session->userdata('session_data');
        if($sess_data===NULL) {
            $this->session->set_flashdata('warning', '<div class="alert alert-warning fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a> 
                Harap login terlebih dahulu untuk mengakses sistem</div>');
			redirect('auth');
        }

        $this->load->model('Karyawan_model', 'karyawan');
        $this->load->model('Generic_model', 'generic');
        
    }

    public function index()
    {
        return 'Hello World';
    }

    private function _validate($is_update=NULL, $id=NULL)
	{
        $this->load->helper('generic');
        
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('nama_lengkap') == '')
		{
			$data['inputerror'][] = 'nama_lengkap';
			$data['error_string'][] = 'Nama Lengkap is required';
			$data['status'] = FALSE;
        }
        
        if($this->input->post('jabatan_id') == '')
		{
			$data['inputerror'][] = 'jabatan_id';
			$data['error_string'][] = 'Jabatan is required';
			$data['status'] = FALSE;
		}

        if($this->input->post('jenjang_pendidikan_id') == '')
		{
			$data['inputerror'][] = 'jenjang_pendidikan_id';
			$data['error_string'][] = 'Jenjang Pendidikan is required';
			$data['status'] = FALSE;
        }
        
        if($this->input->post('jenis_kelamin') == '')
		{
			$data['inputerror'][] = 'jenis_kelamin';
			$data['error_string'][] = 'Jabatan is required';
			$data['status'] = FALSE;
        }
        
        if($this->input->post('alamat_lengkap') == '')
		{
			$data['inputerror'][] = 'alamat_lengkap';
			$data['error_string'][] = 'Alamat is required';
			$data['status'] = FALSE;
        }
        
        if($this->input->post('tgl_lahir') == '')
		{
			$data['inputerror'][] = 'tgl_lahir';
			$data['error_string'][] = 'Tanggal Lahir is required';
			$data['status'] = FALSE;
        }

        if($this->input->post('no_telepon') == '')
		{
			$data['inputerror'][] = 'no_telepon';
			$data['error_string'][] = 'Nomor Telepon is required';
			$data['status'] = FALSE;
        }
        
        if($this->input->post('email') == '')
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Email is required';
			$data['status'] = FALSE;
        }

        if($this->input->post('email')!='')
		{
			$email = $this->input->post('email');
			$check_email = filter_var(filter_var($email, FILTER_SANITIZE_EMAIL), FILTER_VALIDATE_EMAIL);

			if(!$check_email) {
				$data['inputerror'][] = 'email';
				$data['error_string'][] = 'Invalid email address';
				$data['status'] = FALSE;
			}
		}
        
        if($this->input->post('is_user') == '')
		{
			$data['inputerror'][] = 'is_user';
			$data['error_string'][] = 'Jabatan is required';
			$data['status'] = FALSE;
        }

        if($this->input->post('is_user') == 'y')
		{
			if($this->input->post('username') == '')
            {
                $data['inputerror'][] = 'username';
                $data['error_string'][] = 'Username is required';
                $data['status'] = FALSE;
            }
            else if($this->input->post('username') != '')
            {
                $username = $this->input->post('username');
                $is_unique = $this->karyawan->check_username($username);
                
                // get 1 data include $id value
                $include = $this->karyawan->check_username($username, $id);
                // With data exclude $id value
                $exclude = (array)$this->karyawan->check_username($username, $id, 'y');
                $index_exclude = index_by_column($exclude, 'username', $username);

                if($is_update=='update'){

                    if(count((array)$include)>0) {
                        if( ($include->username!=$username) and (($index_exclude!==FALSE) or ($index_exclude!="0")) ) {
                            $data['inputerror'][] = 'username';
                            $data['error_string'][] = 'Username sudah digunakan, harap cari yang lain';
                            $data['status'] = FALSE;
                        }
                    } 
                    else {
                        if (($index_exclude!==FALSE) or ($index_exclude!="0")) {
                            $data['inputerror'][] = 'username';
                            $data['error_string'][] = 'Username sudah digunakan, harap cari yang lain';
                            $data['status'] = FALSE;
                        }
                    }
                    
                    
                }

                
            }

            if($is_update!='update') {
                if($this->input->post('password') == '')
                {
                    $data['inputerror'][] = 'password';
                    $data['error_string'][] = 'Password Awal Aktif is required';
                    $data['status'] = FALSE;
                }
            }

            
        }
        
        if(empty($_FILES))
        {
            // Re-initialize data
            $data = array();
            $data['error_string'] = array();
            $data['inputerror'] = array();
            
            $data['inputerror'][] = 'dokumen_profil';
            $data['error_string'][] = 'Dokumen Harap berformat PDF*';

            $data['inputerror'][] = 'dokumen_pendidikan';
            $data['error_string'][] = 'Dokumen Harap berformat PDF*';

            $data['inputerror'][] = 'dokumen_lainnya';
            $data['error_string'][] = 'Dokumen Harap berformat PDF*';

            $data['status'] = FALSE;
        }
		elseif($_FILES['dokumen_profil']['name']!='')
		{
            $nama_dok = $_FILES['dokumen_profil']['name'];
            $split = explode('.', $nama_dok);
            $ekstensi = strtolower($split[1]);

            //'2000000' as same as 2MB
            if($_FILES['dokumen_profil']['size']>2000000) {
                $data['inputerror'][] = 'files';
                $data['error_string'][] = 'Size dokumen melebihi 2MB';
                $data['status'] = FALSE;
            }
            else if(strpos($this->conf_upload['allowed_types'], $ekstensi)===FALSE) {
                $data['inputerror'][] = 'dokumen_profil';
                $data['error_string'][] = 'Format dokumen harus PDF';
                $data['status'] = FALSE;
            }
        }
        
        elseif($_FILES['dokumen_pendidikan']['name']!='')
		{
            $nama_dok = $_FILES['dokumen_pendidikan']['name'];
            $split = explode('.', $nama_dok);
            $ekstensi = strtolower($split[1]);

            //'2000000' as same as 2MB
            if($_FILES['dokumen_pendidikan']['size']>2000000) {
                $data['inputerror'][] = 'files';
                $data['error_string'][] = 'Size dokumen melebihi 2MB';
                $data['status'] = FALSE;
            }
            else if(strpos($this->conf_upload['allowed_types'], $ekstensi)===FALSE) {
                $data['inputerror'][] = 'dokumen_pendidikan';
                $data['error_string'][] = 'Format dokumen harus PDF';
                $data['status'] = FALSE;
            }
        }
        
        elseif($_FILES['dokumen_lainnya']['name']!='')
		{
            $nama_dok = $_FILES['dokumen_lainnya']['name'];
            $split = explode('.', $nama_dok);
            $ekstensi = strtolower($split[1]);

            //'2000000' as same as 2MB
            if($_FILES['dokumen_lainnya']['size']>2000000) {
                $data['inputerror'][] = 'files';
                $data['error_string'][] = 'Size dokumen melebihi 2MB';
                $data['status'] = FALSE;
            }
            else if(strpos($this->conf_upload['allowed_types'], $ekstensi)===FALSE) {
                $data['inputerror'][] = 'dokumen_lainnya';
                $data['error_string'][] = 'Format dokumen harus PDF';
                $data['status'] = FALSE;
            }
		}

        if($data['status'] === FALSE)
		{
			echo json_encode((object)$data);
			exit();
		}
	}

    public function get_data($pihak_ketiga_id)
	{        
		$list = $this->karyawan->get_datatables($pihak_ketiga_id);

		$data = array();
        $no = $_POST['start'];
        
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->nama_lengkap;
			$row[] = $field->jabatan_nama;
            $row[] = $field->alamat_lengkap;
            $row[] = $field->no_telepon;
            $row[] = $field->is_user;
            if(strlen($field->dokumen_profil)>0){
                $row[] = "<a href='".base_url('assets/uploads/documents_karyawan/'.$field->dokumen_profil)."' target='_blank'>Dokumen Profil</a>";
            } else {
                $row[] = "Kosong";
            }

            if(strlen($field->dokumen_pendidikan)>0){
                $row[] = "<a href='".base_url('assets/uploads/documents_karyawan/'.$field->dokumen_pendidikan)."' target='_blank'>Dokumen Pendidikan</a>";
            } else {
                $row[] = "Kosong";
            }

            if(strlen($field->dokumen_lainnya)>0){
                $row[] = "<a href='".base_url('assets/uploads/documents_karyawan/'.$field->dokumen_lainnya)."' target='_blank'>Dokumen Lainnya</a>";;
            } else {
                $row[] = "Kosong";
            }
            
            
			// $row[] = '<a href="javascript:void(0)" title="Edit" onclick="edit_karyawan('."'".$field->id."'".')"><i class="fa fa-pencil"></i></a> | 
			// <a href="javascript:void(0)" title="Hapus" onclick="delete_karyawan('."'".$field->id."'".')"><i class="fa fa-trash"></i></a>';
			$row[] = "<a class='btn btn-primary' href=".base_url('karyawan/profil/'.$field->id).">Detail</a>";
			$row[] = "";

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->karyawan->count_all($pihak_ketiga_id),
			"recordsFiltered" => $this->karyawan->count_filtered($pihak_ketiga_id),
			"data" => $data,
		);
		// output dalam format JSON
		echo json_encode($output);
	}

    public function ajax_add()
	{
        $this->_validate();
        $this->load->helper('generic');
        
        $options = ['cost' => 13];

		$data = array(
                'nama_lengkap'          => $this->input->post('nama_lengkap'),
                'jabatan_id'            => $this->input->post('jabatan_id'),
				'jenjang_pendidikan_id' => $this->input->post('jenjang_pendidikan_id'),
				'pihak_ketiga_id'       => $this->session->userdata('pihak_ketiga_id'),
                'jenis_kelamin' 	    => $this->input->post('jenis_kelamin'),
                'alamat_lengkap'        => $this->input->post('alamat_lengkap'),
                'tgl_lahir'             => $this->input->post('tgl_lahir'),
                'no_telepon'            => $this->input->post('no_telepon'),
                'email'                 => $this->input->post('email'),
                'username'              => $this->input->post('username'),
                'password'              => password_hash($this->input->post('password'), PASSWORD_BCRYPT, $options),
                'is_user'               => $this->input->post('is_user'),
            );
        
        /* Upload dokumen_profil */
        $random_name             = rename_file();
        $new_name_dokumen_profil = $random_name.'.pdf';
        $this->conf_upload['file_name'] = $new_name_dokumen_profil;
        $this->load->library('upload', $this->conf_upload);
        $this->upload->initialize($this->conf_upload);

        $filename = str_replace(' ','', $_FILES['dokumen_profil']['name']);
        if(strlen($filename)>0) {
            $uploading = $this->upload->do_upload("dokumen_profil");
            if($uploading==TRUE){
                $data['dokumen_profil'] = $new_name_dokumen_profil;
            }
            else {
                echo json_encode(array('status' => 'Error upload new dokumen profil'));
                exit;
            }
        }

        /* Upload dokumen_pendidikan */
        $random_name             = rename_file();
        $new_name_dokumen_pendidikan = $random_name.'.pdf';
        $this->conf_upload['file_name'] = $new_name_dokumen_pendidikan;
        $this->load->library('upload', $this->conf_upload);
        $this->upload->initialize($this->conf_upload);

        $filename = str_replace(' ','', $_FILES['dokumen_pendidikan']['name']);
        if(strlen($filename)>0) {
            $uploading = $this->upload->do_upload("dokumen_pendidikan");
            if($uploading==TRUE){
                $data['dokumen_pendidikan'] = $new_name_dokumen_pendidikan;
            }
            else {
                echo json_encode(array('status' => 'Error upload new dokumen pendidikan'));
                exit;
            }
        }

        /* Upload dokumen_lainnya */
        $random_name             = rename_file();
        $new_name_dokumen_lainnya = $random_name.'.pdf';
        $this->conf_upload['file_name'] = $new_name_dokumen_lainnya;
        $this->load->library('upload', $this->conf_upload);
        $this->upload->initialize($this->conf_upload);

        $filename = str_replace(' ','', $_FILES['dokumen_lainnya']['name']);
        if(strlen($filename)>0) {
            $uploading = $this->upload->do_upload("dokumen_lainnya");
            if($uploading==TRUE){
                $data['dokumen_lainnya'] = $new_name_dokumen_lainnya;
            }
            else {
                echo json_encode(array('status' => 'Error upload new dokumen'));
                exit;
            }
        }

        $insert = $this->generic->insert('karyawan',$data);
        if($insert=='ok') {
            echo json_encode(array("status" => TRUE));
            exit;
        }
        else {
            echo json_encode(array("status" => 'Fail insert data'));
            exit;
        }
        
	}

    public function ajax_edit($id)
	{
		$data = $this->karyawan->fetch_data($id);
		echo json_encode($data);
	}

	public function ajax_update()
	{
        $id = $this->input->post('id');
        $this->_validate('update', $id);
        
        $this->load->helper('generic');
        $options = ['cost' => 13];

		$data = array(
            'nama_lengkap'          => $this->input->post('nama_lengkap'),
            'jabatan_id'            => $this->input->post('jabatan_id'),
            'jenjang_pendidikan_id' => $this->input->post('jenjang_pendidikan_id'),
            'pihak_ketiga_id'       => $this->session->userdata('pihak_ketiga_id'),
            'jenis_kelamin' 	    => $this->input->post('jenis_kelamin'),
            'alamat_lengkap'        => $this->input->post('alamat_lengkap'),
            'tgl_lahir'             => $this->input->post('tgl_lahir'),
            'no_telepon'            => $this->input->post('no_telepon'),
            'email'                 => $this->input->post('email'),
            'username'              => $this->input->post('username'),
            'is_user'               => $this->input->post('is_user'),
        );
        if(strlen($this->input->post('password'))>0) {
            $data['password'] = password_hash($this->input->post('password'), PASSWORD_BCRYPT, $options);
        }
        $dokumen = $this->karyawan->fetch_data($id);

        $file_dokumen_profil = str_replace(' ','', $_FILES['dokumen_profil']['name']);
        $file_dokumen_pendidikan = str_replace(' ','', $_FILES['dokumen_pendidikan']['name']);
        $file_dokumen_lainnya = str_replace(' ','', $_FILES['dokumen_lainnya']['name']);
        
        /* dokumen_profil */
        if(strlen($file_dokumen_profil)>0) {
            $random_name             = rename_file();
            $new_name_dokumen_profil = $random_name.'.pdf';
            $this->conf_upload['file_name'] = $new_name_dokumen_profil;

            $this->load->library('upload', $this->conf_upload);
            $this->upload->initialize($this->conf_upload);

            $uploading = $this->upload->do_upload("dokumen_profil");

            //Deleting old file
            if(!empty($dokumen->dokumen_profil)) {
                $path_dokumen   = $this->conf_upload['upload_path'].$dokumen->dokumen_profil;
                if($path_dokumen!=NULL and file_exists($path_dokumen)){
                    unlink($path_dokumen);
                }
            }
            
            if($uploading==TRUE){
                $data['dokumen_profil'] = $new_name_dokumen_profil;
            }
            else {
                echo json_encode(array('status' => 'Error upload new dokumen profil'));
                exit;
            }
        }

        /* dokumen_pendidikan */
        if(strlen($file_dokumen_pendidikan)>0) {
            $random_name                    = rename_file();
            $new_name_dokumen_pendidikan    = $random_name.'.pdf';
            $this->conf_upload['file_name'] = $new_name_dokumen_pendidikan;

            $this->load->library('upload', $this->conf_upload);
            $this->upload->initialize($this->conf_upload);

            $uploading = $this->upload->do_upload("dokumen_pendidikan");

            //Deleting old file
            if(!empty($dokumen->dokumen_pendidikan)) {
                $path_dokumen   = $this->conf_upload['upload_path'].$dokumen->dokumen_pendidikan;
                if($path_dokumen!=NULL and file_exists($path_dokumen)){
                    unlink($path_dokumen);
                }
            }
            
            if($uploading==TRUE){
                $data['dokumen_pendidikan'] = $new_name_dokumen_pendidikan;
            }
            else {
                echo json_encode(array('status' => 'Error upload new dokumen pendidikan'));
                exit;
            }
        }

        /* dokumen_lainnya */
        if(strlen($file_dokumen_lainnya)>0) {
            $random_name                = rename_file();
            $new_name_dokumen_lainnya   = $random_name.'.pdf';
            $this->conf_upload['file_name'] = $new_name_dokumen_lainnya;

            $this->load->library('upload', $this->conf_upload);
            $this->upload->initialize($this->conf_upload);

            $uploading = $this->upload->do_upload("dokumen_lainnya");

            //Deleting old file
            if(!empty($dokumen->dokumen_lainnya)) {
                $path_dokumen   = $this->conf_upload['upload_path'].$dokumen->dokumen_lainnya;
                if($path_dokumen!=NULL and file_exists($path_dokumen)){
                    unlink($path_dokumen);
                }
            }
            
            if($uploading==TRUE){
                $data['dokumen_lainnya'] = $new_name_dokumen_lainnya;
            }
            else {
                echo json_encode(array('status' => 'Error upload new dokumen lainnya'));
                exit;
            }
        }

        $key    = array('id'=>$id);
        $update = $this->generic->update('karyawan', $data, $key);
        
        if($update=='ok') {
            echo json_encode(array("status" => TRUE));
            exit;
        }
        else {
            echo json_encode(array("status" => 'Error edit data'));
            exit;
        }
        
    }
    
    public function ajax_delete($id)
	{
        $key = array('id'=>$id);

        //Deleting file
        $dokumen = $this->karyawan->fetch_data($id);
        
        $path_dokumen_profil     = $this->conf_upload['upload_path'].$dokumen->dokumen_profil;
        $path_dokumen_pendidikan = $this->conf_upload['upload_path'].$dokumen->dokumen_pendidikan;
        $path_dokumen_lainnya    = $this->conf_upload['upload_path'].$dokumen->dokumen_lainnya;
        
        if($path_dokumen_profil!=NULL and file_exists($path_dokumen_profil)){
            unlink($path_dokumen_profil);
        }
        if($path_dokumen_pendidikan!=NULL and file_exists($path_dokumen_pendidikan)){
            unlink($path_dokumen_pendidikan);
        }
        if($path_dokumen_lainnya!=NULL and file_exists($path_dokumen_lainnya)){
            unlink($path_dokumen_lainnya);
        }

		$delete = $this->generic->delete('karyawan', $key);
        
        if($delete=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => FALSE));
        }
    }

    public function profil($karyawan_id=NULL)
    {
        $this->load->model('Jenis_dokumen_model', 'jenis_dokumen');
		$this->load->model('Jenjang_pendidikan_model', 'jenjang_pendidikan');
		$this->load->model('Jabatan_perusahaan_model', 'jabatan');

		$data['jenis_dokumen'] 		= $this->jenis_dokumen->fetch_data();
		$data['jenjang_pendidikan'] = $this->jenjang_pendidikan->fetch_data();
        $data['jabatan_perusahaan'] = $this->jabatan->fetch_data();
        $data['karyawan_id']        = NULL;
        if($karyawan_id!=NULL) {
            $data['karyawan_id'] = $karyawan_id;
        }

        $sess_data = $this->session->userdata('session_data');
        if($sess_data['as']=='pihak_ketiga') {
            $this->session->set_userdata('is_profil', TRUE);
        }
		// $this->session->set_userdata('pihak_ketiga_id', $id);
        
        $this->load->view('karyawan/profil', $data);
        
    }
    
}

/* End of file Karyawan.php */
