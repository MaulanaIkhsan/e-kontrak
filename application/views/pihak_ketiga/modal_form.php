<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_pihak_ketiga" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Penyedia Barang/Jasa Form</h4>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Perusahaan</label>
                            <div class="col-md-9">
                                <input name="nama_perusahaan" placeholder="Nama Perusahaan" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">NPWP</label>
                            <div class="col-md-9">
                                <input name="npwp" placeholder="Nomor NPWP" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Alamat Lengkap</label>
                            <div class="col-md-9">
                                <textarea name="alamat_lengkap" placeholder="Alamat Lengkap Perusahaan" class="form-control"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Nomor Telepon</label>
                            <div class="col-md-9">
                                <input name="no_telepon" placeholder="Nomor Telepon Perusahaan" class="form-control number" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Email</label>
                            <div class="col-md-9">
                                <input name="email" placeholder="Alamat Email Perusahaan" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Status</label>
                            <div class="col-md-9">
                                <select name="status" class="form-control">
                                    <option value="">--Select Status--</option>
                                    <option value="Cabang">Cabang</option>
                                    <option value="Pusat">Pusat</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group data-pusat">
                            <label class="control-label col-md-3">Alamat Pusat</label>
                            <div class="col-md-9">
                                <textarea name="alamat_pusat" placeholder="Alamat Kantor Pusat" class="form-control"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group data-pusat">
                            <label class="control-label col-md-3">Nomor Telepon Pusat</label>
                            <div class="col-md-9">
                                <input name="no_telepon_pusat" placeholder="Nomor Telepon Kantor Pusat" class="form-control number" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group data-pusat">
                            <label class="control-label col-md-3">Email Pusat</label>
                            <div class="col-md-9">
                                <input name="email_pusat" placeholder="Alamat Email Kantor Pusat" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="admin-user">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save_pihak_ketiga()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->