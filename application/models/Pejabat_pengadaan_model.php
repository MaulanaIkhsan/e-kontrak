<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pejabat_pengadaan_model extends CI_Model {

    /*----- pejabat_pengadaan -----*/
    // id
    // pegawai_id
    // jenis_pejabat_pengadaan_id
    // tingkat_id
    // sk_id
    // tahun
    // created_at
    // updated_at

    public function fetch_data($id=NULL, $tahun=NULL, $pegawai_id=NULL)
    {
        $sql = $this->db->select('pejabat_pengadaan.id as pejabat_pengadaan_id,
                                pegawai.id as pegawai_id,
                                pegawai.nama_lengkap as pegawai_nama,
                                pegawai.nip as pegawai_nip,
                                jenis_pejabat_pengadaan.id as jenis_pejabat_pengadaan_id,
                                jenis_pejabat_pengadaan.nama as jenis_pejabat_pengadaan_nama,
                                seri_pejabat_pengadaan.id as seri_pejabat_pengadaan_id,
                                seri_pejabat_pengadaan.nama as seri_pejabat_pengadaan_nama,
                                pejabat_pengadaan.tahun as tahun,
                                sk_pejabat_pengadaan.no_sk as sk_no,
                                sk_pejabat_pengadaan.tahun as sk_tahun,
                                sk_pejabat_pengadaan.tgl_penetapan as sk_tgl_penetapan')
                        ->from('pejabat_pengadaan')
                        ->join('pegawai', 'pejabat_pengadaan.pegawai_id=pegawai.id', 'left')
                        ->join('jenis_pejabat_pengadaan', 'jenis_pejabat_pengadaan.id=pejabat_pengadaan.jenis_pejabat_pengadaan_id', 'left')
                        ->join('seri_pejabat_pengadaan', 'seri_pejabat_pengadaan.id=pejabat_pengadaan.tingkat_id', 'left')
                        ->join('sk_pejabat_pengadaan', 'pejabat_pengadaan.sk_id=sk_pejabat_pengadaan.id', 'left');

        if($tahun!=NULL) {
            $sql->where('pejabat_pengadaan.tahun', $tahun);
        }

        if($pegawai_id!=NULL) {
            $sql->where('pejabat_pengadaan.pegawai_id', $pegawai_id);
        }
        
        if($id!=NULL) {
            return $sql->where('pejabat_pengadaan.id', $id)
                        ->get()->row();
        }
        elseif($pegawai_id!=NULL and $tahun!=NULL) {
            return $sql->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }

}

/* End of file Pejabat_pengadaan_model.php */
