<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skpd_model extends CI_Model {

	var $main_table = 'skpd',
		$skpd = 'DINAS PEKERJAAN UMUM';

	public function show()
	{
		return $this->db->select('id, nama')
						->from($this->main_table)
						->get()->result();
	}

	public function current_skpd()
	{
		return $this->db->select('kode_skpd, nama')
						->from($this->main_table)
						->where('nama', $this->skpd)
						->get()->row();
	}

}

/* End of file Skpd_model.php */
/* Location: ./application/models/Skpd_model.php */