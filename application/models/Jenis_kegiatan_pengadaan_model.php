<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis_kegiatan_pengadaan_model extends CI_Model {

    /*--- jenis_kegiatan_pengadaan ---*/
    // id
    // nama
    // kode
    // created_at
    // updated_at

    public function fetch_data($id=NULL, $kode=NULL)
    {
        $sql = $this->db->select('jenis_kegiatan_pengadaan.id as id,
                                jenis_kegiatan_pengadaan.nama as nama,
                                jenis_kegiatan_pengadaan.kode as kode')
                        ->from('jenis_kegiatan_pengadaan')
                        ->order_by('jenis_kegiatan_pengadaan.nama', 'asc');

        if($id!=NULL) {
            $sql->where('jenis_kegiatan_pengadaan.id', $id);
        }

        if($kode!=NULL) {
            $sql->where('jenis_kegiatan_pengadaan.kode', $kode);
        }

        if($id!=NULL or $kode!=NULL){
            return $sql->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }

}

/* End of file ModelName.php */
