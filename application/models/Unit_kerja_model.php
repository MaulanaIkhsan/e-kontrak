<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Unit_kerja_model extends CI_Model {
    // ----- unit_kerja -----
    // id
    // nama
    // created_at
    // updated_at
        
    public function fetch_data($id=NULL)
    {
        $sql = $this->db->select('unit_kerja.id as id,
                                unit_kerja.nama as nama')
                        ->from('unit_kerja');
        
        if($id!=NULL) {
            return $sql->where('unit_kerja.id', $id)
                        ->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }

}

/* End of file Unit_kerja_model.php */
