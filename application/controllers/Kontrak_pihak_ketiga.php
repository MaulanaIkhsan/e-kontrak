<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kontrak_pihak_ketiga extends CI_Controller {

    /*----- kontrak_pihak_ketiga -----*/
    // id
	// kontrak_pekerjaan_id
	// pihak_ketiga_id
	// nilai_kontrak
	// lampiran
	// created_at
    // updated_at

    // Configuration upload file
    var $conf_upload = array(
        'upload_path'   => "./assets/uploads/documents_kontrak_pihak_ketiga/",
        'allowed_types' => "pdf",
        'max_size'      => '2000',
        'encrypt_name'  => FALSE
    );

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $sess_data = $this->session->userdata('session_data');
        if($sess_data===NULL) {
            $this->session->set_flashdata('warning', '<div class="alert alert-warning fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                Harap login terlebih dahulu untuk mengakses sistem</div>');
			redirect('auth');
        }

        $this->load->model('Kontrak_pihak_ketiga_model', 'kontrak_pihak_ketiga');
        $this->load->model('Generic_model', 'generic');
    }

    private function _validate($kontrak_pekerjaan_id=NULL)
	{
        $sess_data  = $this->session->userdata('session_data');
        $user_id    = $sess_data['id'];
        $user_role  = $sess_data['role'];
        $user_as    = $sess_data['as'];

        $data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('pihak_ketiga_id') == '')
		{
			$data['inputerror'][] = 'pihak_ketiga_id';
			$data['error_string'][] = 'Pihak Ketiga is required';
			$data['status'] = FALSE;
        }

        if(($user_as=='pegawai' and $user_role=='Super Admin') or $user_as=='pihak_ketiga')
        {
            if($this->input->post('nilai_kontrak') == '')
            {
                $data['inputerror'][] = 'nilai_kontrak';
                $data['error_string'][] = 'Nilai Penawaran is required';
                $data['status'] = FALSE;
            }

            if($kontrak_pekerjaan_id!=NULL and $this->input->post('nilai_kontrak')!='')
            {
                $this->load->model('Kontrak_pekerjaan_model', 'kontrak_pekerjaan');
                $kontrak_pekerjaan = $this->kontrak_pekerjaan->fetch_data($kontrak_pekerjaan_id);
                $nilai_kontrak = $this->input->post('nilai_kontrak');

                if(!empty($kontrak_pekerjaan) and $nilai_kontrak>$kontrak_pekerjaan->hps){
                    $data['inputerror'][] = 'nilai_kontrak';
                    $data['error_string'][] = 'Nilai Penawaran harus lebih kecil atau sama dengan HPS';
                    $data['status'] = FALSE;
                }
            }

            if($this->input->post('nama_bank') == '')
            {
                $data['inputerror'][] = 'nama_bank';
                $data['error_string'][] = 'Nama Bank is required';
                $data['status'] = FALSE;
            }

            if($this->input->post('no_rekening') == '')
            {
                $data['inputerror'][] = 'no_rekening';
                $data['error_string'][] = 'Nomor Rekening is required';
                $data['status'] = FALSE;
            }

            // if(empty($_FILES))
            // {
            //     // Re-initialize data
            //     $data = array();
            //     $data['error_string'] = array();
            //     $data['inputerror'] = array();

            //     $data['inputerror'][] = 'lampiran';
            //     $data['error_string'][] = 'Lampiran Harap berformat PDF*';
            //     $data['status'] = FALSE;
            // }
            // elseif($_FILES['lampiran']['name']!='') {
            //     $nama_dok = $_FILES['lampiran']['name'];
            //     $split = explode('.', $nama_dok);
            //     $ekstensi = strtolower($split[1]);

            //     //'2000000' as same as 2MB
            //     if($_FILES['lampiran']['size']>2000000 or $_FILES['lampiran']['size']<=0) {
            //         $data['inputerror'][] = 'lampiran';
            //         $data['error_string'][] = 'Size dokumen melebihi 2MB';
            //         $data['status'] = FALSE;
            //     }
            //     else if(strpos($this->conf_upload['allowed_types'], $ekstensi)===FALSE) {
            //         $data['inputerror'][] = 'lampiran';
            //         $data['error_string'][] = 'Format dokumen harus PDF';
            //         $data['status'] = FALSE;
            //     }
            // }
        }

		if($data['status'] === FALSE)
		{
			echo json_encode((object)$data);
			exit();
		}

    }


    public function get_data($kontrak_pekerjaan_id)
	{
        $this->load->model('Kontrak_pekerjaan_model', 'kontrak_pekerjaan');
        $this->load->model('Kontrak_dokumen_pihak_ketiga_model', 'kontrak_dokumen_pihak_ketiga');
        
        $kontrak_pekerjaan = $this->kontrak_pekerjaan->fetch_data($kontrak_pekerjaan_id);

        $sess_data  = $this->session->userdata('session_data');
        $user_id    = $sess_data['id'];
        $user_role  = $sess_data['role'];
        $user_as    = $sess_data['as'];

        $list = $this->kontrak_pihak_ketiga->get_datatables($kontrak_pekerjaan_id);
        $this->load->helper('generic');

		$data = array();
        $no = $_POST['start'];

		foreach ($list as $field) {
            $no++;
            
            $row = array();
            
            // Count qty data kontrak_dokumen_pihak_ketiga which terkoreksi value is 'n'
            $jml_dokumen = 0;
            $status      = FALSE;
            $dokumen_profil = $this->kontrak_dokumen_pihak_ketiga->fetch_data(NULL, $field->id);
            if(!empty($dokumen_profil)) {
                foreach($dokumen_profil as $item):
                    if($item->terkoreksi=='n') {
                        $jml_dokumen++;
                    }
                endforeach;
            }
            else {
                $jml_dokumen = 'kosong';
            }
            
            if($jml_dokumen==0) {
                $status = FALSE;
            }
            elseif($jml_dokumen=='kosong') {
                $status = 'kosong';
            } 
            else if($jml_dokumen>0){
                $status = $jml_dokumen;
            }
            else {
                $status = FALSE;
            }

            $row[] = $no;
            $nama_perusahaan = $field->pihak_ketiga_nama.'<span class="badge bg-red pull-right">'.$status.'</span>';
            $row[] = $nama_perusahaan;
			$row[] = format_money($field->nilai_kontrak);
            if(strlen($field->lampiran)>0) {
                $row[] = "<a href=".base_url('assets/uploads/documents_kontrak_pihak_ketiga/'.$field->lampiran)." target='_blank'>Lampiran</a>";
            }
            else {
                $row[] = 'Kosong';
            }

            // Access right for Pegawai
            if($user_as=='pegawai' and
            ($kontrak_pekerjaan->pejabat_pengadaan_pegawai_id==$user_id or $kontrak_pekerjaan->ppkom==$user_id or
            $user_role=='Super Admin')) {
                $row[] = "<a class='btn btn-primary' href=".base_url('kontrak_pihak_ketiga/detail/'.$field->id).">Detail</a>";
            }
            // Access right for Pihak Ketiga
            elseif ($user_as=='pihak_ketiga') {
                $this->load->model('Karyawan_model', 'karyawan');

                $karyawan = NULL;
                $karyawan = $this->karyawan->fetch_data($user_id);

                if($karyawan!=NULL and $karyawan->pihak_ketiga_id==$field->pihak_ketiga_id) {
                    $row[] = "<a class='btn btn-primary' href=".base_url('kontrak_pihak_ketiga/detail/'.$field->id).">Detail</a>";
                }
            }

			$row[] = "";

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->kontrak_pihak_ketiga->count_all($kontrak_pekerjaan_id),
			"recordsFiltered" => $this->kontrak_pihak_ketiga->count_filtered($kontrak_pekerjaan_id),
			"data" => $data,
		);
		// output dalam format JSON
		echo json_encode($output);
    }

    public function detail($kontrak_pihak_ketiga_id)
	{
        $this->load->model('Pihak_ketiga_model', 'pihak_ketiga');
        $this->load->model('Karyawan_model', 'karyawan');
        $this->load->model('Dokumen_pihak_ketiga_model', 'dokumen_pihak_ketiga');
        $this->load->model('Kontrak_pekerjaan_model', 'kontrak_pekerjaan');
        $this->load->model('Bank_model', 'bank');
        $this->load->model('Kontrak_karyawan_pihak_ketiga_model', 'kontrak_karyawan_pihak_ketiga');
        $this->load->model('Kontrak_dokumen_pihak_ketiga_model', 'kontrak_dokumen_pihak_ketiga');

        //get detail data kontrak pihak ketiga based on ID
        $kontrak_pihak_ketiga = $this->kontrak_pihak_ketiga->fetch_data($kontrak_pihak_ketiga_id);
       
        $data['id']                     = $kontrak_pihak_ketiga_id;
        $data['kontrak_pihak_ketiga']   = $kontrak_pihak_ketiga;
        $data['pihak_ketiga']           = $this->pihak_ketiga->fetch_data();
        $data['dokumen']                = $this->dokumen_pihak_ketiga->fetch_data(NULL, $kontrak_pihak_ketiga->pihak_ketiga_id, 'y');
        $data['karyawan']               = $this->karyawan->fetch_data(NULL, $kontrak_pihak_ketiga->pihak_ketiga_id);
        $data['bank']		            = $this->bank->fetch_data();
        $data['is_opened']              = TRUE;
        $data['is_penetapan']           = FALSE;
        $data['is_pejabat_pengadaan']   = FALSE;
        $data['belum_terkoreksi']       = 'kosong';

        $kontrak_pekerjaan = $this->kontrak_pekerjaan->fetch_data($kontrak_pihak_ketiga->kontrak_pekerjaan_id);
        if($kontrak_pekerjaan=='Penetapan') {
            $data['is_penetapan'] = TRUE;
        }

        $sess_data  = $this->session->userdata('session_data');
        $user_id    = $sess_data['id'];
        $user_role  = $sess_data['role'];
        $user_as    = $sess_data['as'];

        if($user_as=='pihak_ketiga') {
            $data['is_pihak_ketiga'] = TRUE;
        }

        $karyawan = NULL;
        $karyawan = $this->karyawan->fetch_data($user_id);

        if(!empty($karyawan)) {
            $data['pihak_ketiga_id'] = $karyawan->pihak_ketiga_id;
            $data['pihak_ketiga_nama'] = $karyawan->pihak_ketiga_nama;
        }

        // Check status kontrak_pekerjaan
        $kontrak_pekerjaan = $this->kontrak_pekerjaan->fetch_data($kontrak_pihak_ketiga->kontrak_pekerjaan_id);
        if(!empty($kontrak_pekerjaan) and ($kontrak_pekerjaan->status=='Penetapan' or $kontrak_pekerjaan->status=='Penunjukan')) {
            $data['is_opened'] = FALSE;
        }

        // Check pejabat pengadaan
        $pejabat_pengadaan = $kontrak_pekerjaan->pejabat_pengadaan_pegawai_id;
        $ppkom = $kontrak_pekerjaan->pejabat_pembuat_komitmen_pegawai_id;
        if($pejabat_pengadaan==$user_id or $ppkom==$user_id) {
            $data['is_pejabat_pengadaan'] = TRUE;
        }

        // Message Box
        $dokumen_kontrak    = $this->kontrak_dokumen_pihak_ketiga->fetch_data(NULL, $kontrak_pihak_ketiga_id);
        $penugasan_karyawan = $this->kontrak_karyawan_pihak_ketiga->fetch_data(NULL, $kontrak_pihak_ketiga_id);;

        $data['is_dokumen_kontrak']     = FALSE;
        $data['is_penugasan_karyawan']  = FALSE;

        $data['belum_terkoreksi']   = 0;

        if(!empty($dokumen_kontrak)) {
            $data['is_dokumen_kontrak'] = TRUE;

            foreach($dokumen_kontrak as $item):
                if($item->terkoreksi=='n') {
                    $data['belum_terkoreksi']++;
                }
            endforeach;
        }
        else {
            $data['belum_terkoreksi'] = 'kosong';
        }

        if($data['belum_terkoreksi']==0) {
            $data['belum_terkoreksi'] = FALSE;
        }
        elseif($data['belum_terkoreksi']=='kosong') {
            $data['belum_terkoreksi'] = 'kosong';
        } 
        else if($data['belum_terkoreksi']>0){
            $data['belum_terkoreksi'] = $data['belum_terkoreksi'];
        }
        else {
            $status = FALSE;
        }

        if(!empty($penugasan_karyawan)) {
            $data['is_penugasan_karyawan'] = TRUE;
        }

		$this->load->view('kontrak_pihak_ketiga/detail', $data);
	}

    public function ajax_add($kontrak_pekerjaan_id)
	{
        $this->_validate($kontrak_pekerjaan_id);

        $this->load->helper('generic');

        // $random_name = rename_file();
        // $new_name = $random_name.'.pdf';
        // $this->conf_upload['file_name'] = $new_name;
        // $this->load->library('upload', $this->conf_upload);
        // $this->upload->initialize($this->conf_upload);

		$data = array(
                'kontrak_pekerjaan_id'  => $kontrak_pekerjaan_id,
                'pihak_ketiga_id'       => $this->input->post('pihak_ketiga_id'),
                'nilai_kontrak'         => $this->input->post('nilai_kontrak'),
                'bank_id'               => $this->input->post('nama_bank'),
                'no_rekening'           => $this->input->post('no_rekening')
            );

        //'file' is name of textbox_upload in view
        // if(!empty($_FILES['lampiran'])) {
        //     $filename = str_replace(' ','', $_FILES['lampiran']['name']);
        //     if(strlen($filename)>0) {
        //         $uploading = $this->upload->do_upload("lampiran");
        //         if($uploading==TRUE){
        //             $data['lampiran'] = $new_name;
        //         }
        //         else {
        //             echo json_encode(array('status' => FALSE));
        //             die;
        //         }
        //     }
        // }

        $this->db->query('set FOREIGN_KEY_CHECKS = 0;');
        $insert = $this->generic->insert('kontrak_pihak_ketiga',$data);
        if($insert=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => FALSE));
        }
        $this->db->query('set FOREIGN_KEY_CHECKS = 1;');
    }

    public function ajax_edit($id)
	{
		$data = $this->kontrak_pihak_ketiga->fetch_data($id);
		echo json_encode($data);
	}

    public function ajax_update()
	{
        $id = $this->input->post('id');
        $kontrak_pihak_ketiga = $this->kontrak_pihak_ketiga->fetch_data($id);
        $this->_validate($kontrak_pihak_ketiga->kontrak_pekerjaan_id);

        $this->load->helper('generic');

        // $random_name = rename_file();
        // $new_name = $random_name.'.pdf';
        // $this->conf_upload['file_name'] = $new_name;
        // $this->load->library('upload', $this->conf_upload);
        // $this->upload->initialize($this->conf_upload);

		$data = array(
            'pihak_ketiga_id'   => $this->input->post('pihak_ketiga_id'),
            'nilai_kontrak'     => $this->input->post('nilai_kontrak'),
            'bank_id'           => $this->input->post('nama_bank'),
            'no_rekening'       => $this->input->post('no_rekening')
        );

        //'file' is name of textbox_upload in view
        // $filename = str_replace(' ','', $_FILES['lampiran']['name']);

        // if(!empty($_FILES['lampiran'])) {
        //     if(strlen($filename)>0) {
        //         $uploading = $this->upload->do_upload("lampiran");

        //         //Deleting old file
        //         $dokumen        = $this->kontrak_pihak_ketiga->fetch_data($id);
        //         if(!empty($dokumen->lampiran)) {
        //             $path_dokumen   = $this->conf_upload['upload_path'].$dokumen->lampiran;
        //             if($path_dokumen!=NULL and file_exists($path_dokumen)){
        //                 unlink($path_dokumen);
        //             }
        //         }

        //         if($uploading==TRUE){
        //             $data['lampiran'] = $new_name;
        //         }
        //         else {
        //             echo json_encode(array('status' => 'Error upload new lampiran'));
        //         }
        //     }
        // }

        $this->db->query('set FOREIGN_KEY_CHECKS = 0;');

        $key    = array('id'=>$id);
        $update = $this->generic->update('kontrak_pihak_ketiga', $data, $key);

        if($update=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => 'Error edit data'));
        }
        $this->db->query('set FOREIGN_KEY_CHECKS = 1;');
    }

    public function ajax_delete($id)
	{
        $key = array('id'=>$id);

        //Deleting file
        $dokumen = $this->kontrak_pihak_ketiga->fetch_data($id);
        $path_dokumen = $this->conf_upload['upload_path'].$dokumen->lampiran;
        if($path_dokumen!=NULL and file_exists($path_dokumen)){
            unlink($path_dokumen);
        }

		$delete = $this->generic->delete('kontrak_pihak_ketiga', $key);

        if($delete=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => FALSE));
        }
    }

    public function download_dokumen($kontrak_pihak_ketiga_id)
    {
        ob_start();
        
        $this->load->model('Kontrak_dokumen_pihak_ketiga_model', 'kontrak_dokumen_pihak_ketiga');
        $this->load->model('Kontrak_karyawan_pihak_ketiga_model', 'kontrak_karyawan_pihak_ketiga');
        $this->load->model('Kontrak_dokumen_lainnya_pihak_ketiga_model', 'kontrak_dokumen_lainnya_pihak_ketiga');

        $dokumen_profil     = $this->kontrak_dokumen_pihak_ketiga->fetch_data(NULL, $kontrak_pihak_ketiga_id);
        $penugasan_karyawan = $this->kontrak_karyawan_pihak_ketiga->fetch_data(NULL, $kontrak_pihak_ketiga_id);
        $dokumen_lainnya    = $this->kontrak_dokumen_lainnya_pihak_ketiga->fetch_data(NULL, $kontrak_pihak_ketiga_id);
        $detail             = $this->kontrak_pihak_ketiga->fetch_data($kontrak_pihak_ketiga_id);

        $path_dokumen_profil     = './assets/uploads/documents/';
        $path_penugasan_karyawan = './assets/uploads/documents_karyawan/';
        $path_dokumen_lainnya    = './assets/uploads/documents_kontrak_lainnya_pihak_ketiga/';

        $mpdf = new \Mpdf\Mpdf(['format'=>'legal']);
        $mpdf->SetImportUse();

        $filename = 'Dokumen Kontrak - '.$detail->pihak_ketiga_nama;
        
        
        
        // Dokumen Profil
        $list_file = [];
        if(!empty($dokumen_profil)) {
            foreach($dokumen_profil as $item):
                if(!empty($item->dokumen_pihak_ketiga_file)) {
                    array_push($list_file, $path_dokumen_profil.$item->dokumen_pihak_ketiga_file);
                }
            endforeach;
        }
        
        if(!empty($list_file)) {
            foreach($list_file as $item):
                if(file_exists($item)) {
                     // Get all pages in current file
					$pageInFile = $mpdf->SetSourceFile($item);

					for ($flag=1; $flag<=$pageInFile ; $flag++) {
						$pageId = $mpdf->ImportPage($flag);

						$page = $mpdf->getTemplateSize($pageId);

						if($flag==1) {
							$mpdf->state=0;
							$mpdf->UseTemplate($pageId);
						}
						else {
							$mpdf->state = 1;
							$mpdf->AddPage($page['w']>$page['h']?'L':'P');
							$mpdf->UseTemplate($pageId);
						}
					}
                }
            endforeach;
        }
        else {
            $view = "<h1>Data Dokumen Profil Masih Kosong</h1>";
            $mpdf->AddPage();
            $mpdf->WriteHTML($view);
        }

        // Dokumen Penugasan Karyawan
        $list_file = [];
        if(!empty($penugasan_karyawan)) {
            foreach($penugasan_karyawan as $item):
                if(!empty($item->karyawan_dokumen_profil)) {
                    array_push($list_file, $path_penugasan_karyawan.$item->karyawan_dokumen_profil);
                }

                if(!empty($item->karyawan_dokumen_pendidikan)) {
                    array_push($list_file, $path_penugasan_karyawan.$item->karyawan_dokumen_pendidikan);
                }

                if(!empty($item->karyawan_dokumen_lainnya)) {
                    array_push($list_file, $path_penugasan_karyawan.$item->karyawan_dokumen_lainnya);
                }
            endforeach;
        }

        if(!empty($list_file)) {
            foreach($list_file as $item):
                if(file_exists($item)) {
                     // Get all pages in current file
                    $pageInFile = $mpdf->SetSourceFile($item);

                    for ($flag=1; $flag<=$pageInFile ; $flag++) {
                        $pageId = $mpdf->ImportPage($flag);

                        $page = $mpdf->getTemplateSize($pageId);

                        if($flag==1) {
                            $mpdf->state=0;
                            $mpdf->UseTemplate($pageId);
                        }
                        else {
                            $mpdf->state = 1;
                            $mpdf->AddPage($page['w']>$page['h']?'L':'P');
                            $mpdf->UseTemplate($pageId);
                        }
                    }
                }
            endforeach;
        }
        else {
            $view = "<h1>Data Dokumen Penugasan Karyawan(Bagian Profil) Masih Kosong</h1>";
            $mpdf->AddPage();
            $mpdf->WriteHTML($view);
        }

         // Dokumen Lainnya
         $list_file = [];
         if(!empty($dokumen_lainnya)) {
             foreach($dokumen_lainnya as $item):
                 if(!empty($item->file)) {
                     array_push($list_file, $path_dokumen_lainnya.$item->file);
                 }
             endforeach;
         }

         if(!empty($list_file)) {
             foreach($list_file as $item):
                 if(file_exists($item)) {
                      // Get all pages in current file
                     $pageInFile = $mpdf->SetSourceFile($item);

                     for ($flag=1; $flag<=$pageInFile ; $flag++) {
                         $pageId = $mpdf->ImportPage($flag);

                         $page = $mpdf->getTemplateSize($pageId);

                         if($flag==1) {
                             $mpdf->state=0;
                             $mpdf->UseTemplate($pageId);
                         }
                         else {
                             $mpdf->state = 1;
                             $mpdf->AddPage($page['w']>$page['h']?'L':'P');
                             $mpdf->UseTemplate($pageId);
                         }
                     }
                 }
             endforeach;
         }
         else {
             $view = "<h1>Data Dokumen Pendukung Lainnya Masih Kosong</h1>";
             $mpdf->AddPage();
             $mpdf->WriteHTML($view);
         }

        ob_clean();
		ob_end_flush();

        $mpdf->output($filename.'.pdf', "D");
		unset($mpdf);
    }

    function kontrak_pihak_ketiga_komentar($dokumen_id){
        //code by faleddo
        //see my simplicity
        $this->load->model('Kontrak_dokumen_pihak_ketiga_model', 'kontrak_dokumen_pihak_ketiga');
        
        $data['detail']     = $this->kontrak_dokumen_pihak_ketiga->fetch_data($dokumen_id);
        $data['dokumen_id'] = $dokumen_id;

        $this->load->view('kontrak_pihak_ketiga/komentar', $data);
    }

}

/* End of file Kontrak_pihak_ketiga.php */
