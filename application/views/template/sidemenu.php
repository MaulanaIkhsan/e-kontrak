</head>
<body class="hold-transition skin-yellow fixed sidebar-mini">
  <div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

      <!-- Logo -->
      <a href="#l" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><i class="fa fa-th"></i></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Sisuper</b></span>
      </a>

      <!-- Header Navbar -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
        <?php
        $sess_data  = $this->session->userdata('session_data');
        $user_id    = $sess_data['id'];
        $role       = $sess_data['role'];
        $as         = $sess_data['as'];

        $notifs = $this->db->select('count(kontrak_komentar_notif.id) as total,
                              aktivitas.nama_aktivitas as nama_aktivitas,
                              dokumen_pihak_ketiga.nama_dokumen as dokumen_pihak_ketiga,
                              kontrak_dokumen_pihak_ketiga.id as kontrak_dokumen_pihak_ketiga_id')
                          ->from('kontrak_komentar_notif')
                          ->join('kontrak_dokumen_pihak_ketiga', 'kontrak_komentar_notif.file_id=kontrak_dokumen_pihak_ketiga.id', 'inner')
                          ->join('dokumen_pihak_ketiga', 'kontrak_dokumen_pihak_ketiga.dokumen_pihak_ketiga_id=dokumen_pihak_ketiga.id', 'inner')
                          ->join('kontrak_pihak_ketiga', 'kontrak_pihak_ketiga.id=kontrak_dokumen_pihak_ketiga.kontrak_pihak_ketiga_id', 'inner')
                          ->join('pihak_ketiga', 'kontrak_pihak_ketiga.pihak_ketiga_id=pihak_ketiga.id', 'inner')
                          ->join('kontrak_pekerjaan', 'kontrak_pihak_ketiga.kontrak_pekerjaan_id=kontrak_pekerjaan.id', 'inner')
                          ->join('aktivitas', 'kontrak_pekerjaan.aktivitas_id=aktivitas.id', 'inner')
                          ->join('pejabat_pengadaan', 'kontrak_pekerjaan.pejabat_pengadaan_id=pejabat_pengadaan.id', 'inner')
                          ->join('pegawai', 'pejabat_pengadaan.pegawai_id=pegawai.id', 'inner');
        
        if($as=='pegawai') {
          $notifs->where('pegawai.id', $user_id);
        }
        else {
          $notifs->where('pihak_ketiga.id', $sess_data['pihak_ketiga_id']);
        }

        $notifs = $notifs->where('kontrak_komentar_notif.notif_status', 'new')
                        ->group_by('aktivitas.nama_aktivitas, dokumen_pihak_ketiga.nama_dokumen, kontrak_dokumen_pihak_ketiga.id')
                        ->get()->result_array();
        $total = 0;
        
        if(!empty($notifs)) {
          foreach($notifs as $item):
            $total = $total+$item['total'];
          endforeach;
        }
        ?>
        <ul class="nav navbar-nav pull-left">
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
              <i class="fa fa-bell-o"></i>
              <span class="label label-danger"><?php print(($total>0)?$total:''); ?></span>
            </a>
            <ul class="dropdown-menu">

              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <?php 
                    if(!empty($notifs)) {
                      foreach($notifs as $item):
                        print('<li><a href="'.base_url('kontrak_komentar_notif/read_notif/'.$item['kontrak_dokumen_pihak_ketiga_id']).'">');
                        print('<div class="label label-danger">'.$item['total'].'</div> - '.$item['dokumen_pihak_ketiga'].' - '.$item['nama_aktivitas']);
                        print('</a></li>');
                      endforeach;
                    }
                  ?>
                </ul>
              </li>
            </ul>
          </li>
        </ul>

      </div>
      </nav>

    </header>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

    <?php $sess_user = $this->session->userdata('session_data'); ?>

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
          <?php if($sess_user['as']=='pegawai') { ?>
            <img src="<?php print(base_url('assets/dist/img/avatar5.png')); ?>" class="img-circle" alt="User Image">
          <?php } else { ?>
            <img src="<?php print(base_url('assets/dist/img/avatar04.png')); ?>" class="img-circle" alt="User Image">
          <?php }?>

        </div>
        <div class="pull-left info">
          <br/>
          <p>
            <?php
            print($sess_user['nama']);
            if($sess_user['as']=='pihak_ketiga') {
              print('<br/><small>Penyedia barang/jasa</small>');
            }
            ?>
          </p>
          <br/>Penyedia barang/jasa
          <br/>Pihak Ketiga
        </div>
      </div>

       <!-- Sidebar user panel (optional) -->

      <?php
      $sess_user = $this->session->userdata('session_data');
      if($sess_user['as']=='pegawai') {
        switch ($sess_user['role']) {
          case 'Super Admin':
            print('menu : superadmin');
            $this->load->view('template/menu_superadmin');
            break;
          case 'Admin':
            print('menu : admin');
            $this->load->view('template/menu_admin');
            break;
          case 'Staff':
            print('menu : staff');
            $this->load->view('template/menu_staff');
            break;
          default:
            $this->session->set_flashdata('alert', '<div class="alert alert-danger fade in">
                          <a href="#" class="close" data-dismiss="alert">&times;</a>
                          Data sidemenu tidak ditemukan !</div>');
            redirect('auth/logout');
            break;
        }
      }
      elseif($sess_user['as']=='pihak_ketiga') {
        $this->load->view('template/menu_pihak_ketiga');
      }
      else {
        $this->session->set_flashdata('alert', '<div class="alert alert-danger fade in">
                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                        Data sidemenu tidak ditemukan !</div>');
        redirect('auth/logout');
      }


      ?>
    </section>
    <!-- /.sidebar -->
    </aside>

    <!-- Modal Logout System -->
    <div class="modal fade" id="modal-logout">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button> -->
          </div>
          <div class="modal-body">
            <p>
              <strong>Logout dari sistem ?</strong>
            </p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
            <a href="<?php print(base_url('auth/logout')); ?>" class="btn btn-primary">Ya, Logout</a>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal Consume Simanggaran-->
    <div class="modal fade" id="modal-consume-simanggaran">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><i class="fa fa-warning"></i> Konfirmasi Consume Simanggaran</h4>
          </div>
          <div class="modal-body">
            <p>Consume data urusan, program dan pekerjaan dari sistem Simanggaran</p>
            <form class="form-horizontal" method="post" action="<?php print(base_url('consume_api/consume_simanggaran'))?>">
              <div class="form-group">
                <label for="namalengkap" class="col-sm-2 control-label">Tahun</label>
                <div class="col-sm-9">
                  <select class="form-control" name="tahun" id="namalengkap" required="required">
                    <option value="">--Pilih Tahun --</option>
                    <?php
                    $curr_year = (int)date("Y");
                    $limit_year = (int)4;
                    $item=$curr_year-$limit_year;

                    while ($curr_year>=$item) {
                        if($curr_year==date("Y")) { ?>
                          <option value="<?php print($curr_year); ?>" selected="selected"><?php print($curr_year); ?></option>
                        <?php } else { ?>
                          <option value="<?php print($curr_year); ?>"><?php print($curr_year); ?></option>
                        <?php }
                        $curr_year--;
                    }
                    ?>
                  </select>
                </div>
              </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
            <button type="submit" name="consume" class="btn btn-primary">Consume Data</button>
            </form>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal Rekap Data -->
    <div class="modal fade" id="modal-rekap-kontrak-pekerjaan">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title-rekap">Rekap Data Kontrak Pekerjaan</h4>
          </div>
          <div class="modal-body">

            <form class="form-horizontal" method="post" action="<?php print(base_url('kontrak_pekerjaan/rekap_data_kontrak'))?>">
              <div class="form-group">
                <label for="namalengkap" class="col-sm-2 control-label">Tahun</label>
                <div class="col-sm-9">
                  <select class="form-control" name="tahun" required="required">
                    <option value="">--Pilih Tahun --</option>
                    <?php
                    $curr_year = (int)date("Y");
                    $limit_year = (int)4;
                    $item=$curr_year-$limit_year;

                    while ($curr_year>=$item) {
                        if($curr_year==date("Y")) { ?>
                          <option value="<?php print($curr_year); ?>" selected="selected"><?php print($curr_year); ?></option>
                        <?php } else { ?>
                          <option value="<?php print($curr_year); ?>"><?php print($curr_year); ?></option>
                        <?php }
                        $curr_year--;
                    }
                    ?>
                  </select>
                </div>
              </div>

              <?php
              $sess_data          = $this->session->userdata('session_data');
              $user_as            = $sess_data['as'];
              $user_role          = $sess_data['role'];
              $user_id            = $sess_data['id'];
              if($user_role=='Super Admin' or $user_role=='Admin') { ?>
                <div class="form-group">
                  <label for="namalengkap" class="col-sm-2 control-label">Group By</label>
                  <div class="col-sm-9">
                    <input type="radio" name="group_by" value="pekerjaan" checked="checked" /> Data Pekerjaan <br/>
                    <input type="radio" name="group_by" value="pejabat_pengadaan" /> Data Pejabat Pengadaan
                  </div>
                </div>
              <?php } ?>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
            <button type="submit" name="consume" class="btn btn-primary">Generate</button>
            </form>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
