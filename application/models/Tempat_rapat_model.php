<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tempat_rapat_model extends CI_Model {

    /*--- tempat_rapat ---*/
    // id
    // nama
    // alamat
    // created_at
    // updated_at
    
    public function fetch_data($id=NULL)
    {
        $sql = $this->db->select('tempat_rapat.id as id,
                                tempat_rapat.nama as nama,
                                tempat_rapat.alamat as alamat')
                        ->from('tempat_rapat')
                        ->order_by('tempat_rapat.nama', 'asc');

        if($id!=NULL){
            return $sql->where('tempat_rapat.id', $id)
                        ->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }

}

/* End of file Tempat_rapat_model.php */
