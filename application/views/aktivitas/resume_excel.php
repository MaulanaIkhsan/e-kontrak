<!DOCTYPE html>
<html>
<head>
	<title>Resume Data Pekerjaan</title>
</head>
<body>
	<?php 
	header('Content-type: application/vnd-ms-excel');
	header('Content-Disposition: attachment; filename=resume-data-pekerjaan.xls');
	?>
	<table>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <th colspan="3" align="center">RESUME DATA PEKERJAAN</th>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><strong>Nama Program</strong></td>
            <td><?php print($detail->nama_program); ?></td>
        </tr>
        <tr>
            <td><strong>Tahun Program</strong></td>
            <td align="left"><?php print($detail->program_tahun); ?></td>
        </tr>
        <tr>
            <td><strong>Nama Kegiatan</strong></td>
            <td><?php print($detail->nama_pekerjaan); ?></td>
        </tr>
        <tr>
            <td><strong>Tahun Kegiatan</strong></td>
            <td align="left"><?php print($detail->pekerjaan_tahun); ?></td>
        </tr>
		<tr>
            <td><strong>Nama Pekerjaan</strong></td>
            <td><?php print($detail->nama_aktivitas); ?></td>
        </tr>
        <tr>
            <td><strong>PPTK</strong></td>
            <td><?php print($detail->pptk); ?></td>
        </tr>
        <tr>
            <td><strong>Metode Pengadaan</strong></td>
            <td><?php print($detail->jenis_pengadaan); ?></td>
        </tr>
        <tr>
            <td><strong>Jenis Pekerjaan</strong></td>
            <td><?php print($detail->jenis_pekerjaan); ?></td>
        </tr>
         <tr>
            <td><strong>Anggaran Awal</strong></td>
            <td><?php print('Rp '.number_format($detail->anggaran_awal, 0, '.','.').',-'); ?></td>
        </tr>
        <tr>
            <td><strong>Perubahan Anggaran</strong></td>
            <td><?php print('Rp '.number_format($detail->perubahan_anggaran, 0, '.','.').',-'); ?></td>
        </tr>
        <tr>
            <td><strong>Tgl Pengajuan Pencairan</strong></td>
            <td align="left">
                <?php echo $detail->tgl_pengajuan_pencairan=='Belum'?$detail->tgl_pengajuan_pencairan:$controller->format_date($detail->tgl_pengajuan_pencairan, 2);?>       
            </td>
        </tr>
        <tr>
            <td><strong>Tgl Pencairan</strong></td>
            <td align="left">
                <?php echo $detail->tgl_pencairan=='Belum'?$detail->tgl_pencairan:$controller->format_date($detail->tgl_pencairan, 2);?>
            </td>
        </tr>
        <tr>
            <td><strong>Nilai Kontrak</strong></td>
            <td><?php print('Rp '.number_format($detail->nilai_kontrak, 0, '.','.').',-'); ?></td>
        </tr>
        <tr>
            <td><strong>HPS</strong></td>
            <td><?php print('Rp '.number_format($detail->hps, 0, '.','.').',-'); ?></td>
        </tr>
        <tr>
            <td><strong>Serapan</strong></td>
            <td><?php print('Rp '.number_format($detail->serapan, 0, '.','.').',-'); ?></td>
        </tr>
        <tr>
            <td><strong>Keterangan</strong></td>
            <td><?php print($detail->keterangan); ?></td>
        </tr>
	</table>
	<p></p>
    
    <p></p>
    
        <?php 
        $no=1;
        if(count($kontrak)>0) { ?>
            <table>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <th colspan="3" align="center">DATA PIHAK KETIGA DAN PROGRESS PEKERJAAN</th>
                </tr>
            </table>
            <?php foreach($kontrak as $item): ?>
            <table>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr align="left">
                    <th colspan="3">Data Pihak Ketiga</th>
                </tr>
                <tr align="left">
                    <td>&nbsp;</td>
                </tr>
                <tr align="left">
                    <td><strong>Nomor</strong></td>
                    <td><?php print($no); ?></td>
                </tr>
                <tr align="left">
                    <td><strong>Pihak Ketiga</strong></td>
                    <td><?php print($item->nama_vendor); ?></td>
                </tr>
                <tr align="left">
                    <td><strong>Nomor Kontrak</strong></td>
                    <td><?php print($item->no_kontrak); ?></td>
                </tr>
                <tr align="left">
                    <td><strong>Penawaran Harga</strong></td>
                    <td><?php print('Rp '.number_format($item->hps, 0, '.',',').',-'); ?></td>
                </tr>
                <tr align="left">
                    <td><strong>Tanggal Awal</strong></td>
                    <td><?php print(format_date($item->awal_kontrak, 2)); ?></td>
                </tr>
                <tr align="left">
                    <td><strong>Tanggal Akhir</strong></td>
                    <td><?php print(format_date($item->akhir_kontrak, 2)); ?></td>
                </tr>
                <tr align="left">
                    <td><strong>Status</strong></td>
                    <td><?php print($item->status); ?></td>
                </tr>
                <tr align="left">
                    <td><strong>Keterangan</strong></td>
                    <td><?php print($item->keterangan); ?></td>
                </tr>
            </table>

            <?php
            $flag_progress = 1; 
            $progress = $controller->get_realisasi($item->kontrak_id);
            $jml = count($progress);
            if($jml>0) { ?>
                <table border="1">
                <tr align="center" style="border: 1px solid black;">
                    <td>NO</td>
                    <td>Tanggal</td>
                    <td>Serapan Anggaran</td>
                    <td>Rencana Fisik</td>
                    <td>Realisasi Fisik</td>
                    <td>Deviasi</td>
                    <td>Status Teknis</td>
                    <td>Status Pencairan</td>
                    <td>Keterangan</td>
                </tr>
            <?php foreach($progress as $value): ?>
                <tr>
                    <td align="center"><?php print($no.'.'.$flag_progress); ?></td>
                    <td align="center"><?php print($controller->format_date($value->tanggal, 2)); ?></td>
                    <td align="right"><?php print('Rp '.number_format($value->harga,0,'.',',').',-'); ?></td>
                    <td align="center"><?php print($value->rencana_fisik.'%'); ?></td>
                    <td align="center"><?php print($value->realisasi_fisik.'%'); ?></td>
                    <td align="center"><?php print($value->deviasi.'%'); ?></td>
                    <td align="center"><?php print($value->status_teknis); ?></td>
                    <td align="center"><?php print($value->status_pencairan); ?></td>
                    <td align="left"><?php print($value->keterangan); ?></td>
                </tr>

            <?php 
            $flag_progress++;
            endforeach; //endforeach progress
            ?></table><?php
        } // end jml progress 
        $no++; 
    endforeach; 
} ?>
</body>
</html>