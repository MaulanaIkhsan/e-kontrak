<?php
$this->load->view('template/header');?>

<?php $this->load->view('template/asset_header');?>
<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/jquery-ui/themes/dot-luv/jquery-ui.min.css')); ?>">

<?php $this->load->view('template/sidemenu');?>

<div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
        	<div class="col-md-12">
            <!-- Horizontal Form -->
	          <div class="box box-primary">
	              <div class="box-header with-border">
	                <h3 class="box-title">Autocomplete</h3>
	                
	              </div>
	              <!-- /.box-header -->
	              
	              <!-- form start -->
	              <div class="box-body">
	              	<input type="text" class="form-control" name="unit_kerja" id="unit_kerja" placeholder="Unit Kerja Name" />
	              </div>
	          	</div>
	      	</div>

        </div>
    </section>
</div>

<?php $this->load->view('template/asset_footer');?>
<!-- JQueryUI -->
<script src="<?php print(base_url('assets/bower_components/jquery-ui/jquery-ui.min.js')); ?>"></script>
<script type="text/javascript">
	$(function(){
		$( "#unit_kerja" ).autocomplete({
          source: "<?php echo site_url('pengunjung/get_autocomplete?getData=false'); ?>"
        });
	});
</script>
<?php $this->load->view('template/footer');?>


