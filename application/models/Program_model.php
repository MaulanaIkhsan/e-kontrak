<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Program_model extends CI_Model {

	var $main_table = 'program';
	var $column_order = array(null, 'program.tahun', 'program.nama_program','program.keterangan'); //field yang ada di table user
	var $column_search = array('program.nama_program','program.keterangan', 'program.tahun'); //field yang diizin untuk pencarian 
	var $order = array('program.tahun, program.id' => 'asc'); // default order 

	public function check_data_program($kode_program, $tahun_program)
	{
		$sql = $this->db->select('kode_program, nama_program, tahun')
						->from($this->main_table)
						->where('kode_program', $kode_program)
						->where('tahun', $tahun_program)
						->get();
		if($sql->num_rows()==0){
			return FALSE;
		}
		else {
			return $sql->row();
		}
	}

	public function check_nama_program($kode_program, $nama_program, $tahun_program)
	{
		$sql = $this->check_data_program($kode_program, $tahun_program);

		if($sql->nama_program !== $nama_program){
			return FALSE;
		}
		else {
			return TRUE;
		}
	}

	public function update($data, $key)
	{
		$sql = $this->db->where($key)
						->update($this->main_table, $data);
		if($sql){
			return 'ok';
		}
		else {
			return 'cancel';
		}
	}

	public function insert($data, $linked_urusan)
	{
		$this->load->helper('generic');
		
		$nama_program = $data['nama_program'];
		$kode_program = $data['kode_program'];
		$tahun_program = $data['tahun'];

		$check_program = $this->check_data_program($kode_program, $tahun_program);
		if($check_program!==FALSE) {

			$check_nama_program = $this->check_nama_program($kode_program, $nama_program, $tahun_program);
			if($check_nama_program==FALSE) {
				//update nama urusan
				$data = [];
				$data['nama_program'] = $nama_program;
				$key['kode_program'] = $kode_program;
				$update = $this->update($data, $key);
				if ($update=='ok') {
					return 'ok';
				}
				else {
					return 'false';
				}
			}//check nama_program
			else {
				return 'exist';
			}
		} //check_program
		else {
			$this->load->model('Urusan_model', 'urusan');

			// Get linked data urusan
			$kode_urusan = $linked_urusan['kode_urusan'];
			$nama_urusan = $linked_urusan['nama_urusan'];
			$tahun_urusan = $linked_urusan['tahun'];
			$get_linked = $this->urusan->get_urusan_by_uniq_code($kode_urusan, $nama_urusan, $tahun_urusan);
			$data['kode_urusan'] = $get_linked->uniq_code;

			$data['uniq_code'] = random_unique_code(12, 'P');

			//input data program baru
			$sql = $this->db->insert($this->main_table, $data);
			if($sql){
				return 'ok';
			}
			else {
				return 'fail';
			}
		}

	}

	public function get_program_by_uniq_code($kode_program, $nama_program, $tahun)
	{
		return $this->db->select('uniq_code')
						->from($this->main_table)
						->where('kode_program', $kode_program)
						->where('nama_program', $nama_program)
						->where('tahun', $tahun)
						->get()->row();
	}

	public function insert_common($data)
	{
		$sql = $this->db->insert($this->main_table, $data);
		if($sql){
			return 'ok';
		}
		else {
			return 'fail';
		}
	}

	public function show($tahun=null)
	{
		$sql = $this->db->select('id, kode_program, nama_program, tahun, uniq_code, kode_urusan')
						->from($this->main_table);
		
		if($tahun!=null){
			$sql->where('tahun', $tahun);
			return $sql->get()->result();
		}
		else {
			return $sql->get()->result();
		}
	}

	public function show_update($key)
	{
		return $this->db->select('id, kode_program, nama_program, ppkom, keterangan, kode_urusan')
						->from($this->main_table)
						->where('id', $key)
						->get()->row();
	}

	public function delete($key)
	{
		$sql = $this->db->delete($this->main_table, $key);
		if ($sql) {
			return 'ok';
		}
		else{
			return 'fail';
		}
	}

	public function detail($program_id)
	{
		return $this->db->select('program.id as program_id, 
						program.nama_program as nama_program, 
						program.keterangan as keterangan, 
						program.kode_program as kode_program,
						program.uniq_code as uniq_code,
						program.tahun as tahun')
						->from('program')
						->where('program.id', $program_id)
						->get()->row();
	}

	public function get_data_program($program_id)
	{
		return $this->db->select('program.id as program_id, program.kode_program as kode_program, program.nama_program as nama_program, users.nama as ppkom')
						->from($this->main_table)
						->where('program.id', $program_id)
						->get()->row();
	}

	public function get_tahun()
	{
		return $this->db->select('tahun')
						->from($this->main_table)
						->group_by('tahun')
						->get()->result();
	}

	public function resume_pekerjaan_by_program($program_id)
	{		
		return $this->db->select('pekerjaan.id as pekerjaan_id,
							pekerjaan.nama_pekerjaan as nama_pekerjaan,
							pekerjaan.tahun as tahun,
							coalesce(pekerjaan.anggaran_awal, 0) as anggaran_pekerjaan,
							coalesce(sum(aktivitas.total_realisasi), 0) as serapan')
						->from('pekerjaan')
						->join('program', 'program.uniq_code=pekerjaan.kode_program', 'left')
						->join('aktivitas', 'aktivitas.pekerjaan_id=pekerjaan.id', 'left')
						->where('program.id', $program_id)
						->group_by('pekerjaan.id, 
								pekerjaan.nama_pekerjaan, 
								pekerjaan.tahun, 
								pekerjaan.anggaran_awal')
						->get()->result();
	}
	/*====== Serverside Datatable ======*/
	private function _get_datatables_query()
	{
		$this->db->select('program.id as program_id, 
					program.nama_program as nama_program, 
					program.keterangan as keterangan, 
					program.tahun as tahun');
		$this->db->from($this->main_table);

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->select('program.id as program_id, 
					program.nama_program as nama_program, 
					users.nama as ppkom, program.keterangan as keterangan, 
					program.tahun as tahun');
		$this->db->from($this->main_table);

		// if($role!=='admin') {			
		// 	$sub_query = "(select 
		// 			distinct(pekerjaan.kode_program) as kode_program
		// 		from
		// 			pekerjaan
		// 				inner join
		// 			aktivitas on (aktivitas.pekerjaan_id=pekerjaan.id)
		// 				inner join
		// 			users on (aktivitas.pptk=users.id)
		// 		where
		// 			users.id = ".$user_id.")";

		// 	$this->db->join($sub_query.' as filter_pekerjaan', 'filter_pekerjaan.kode_program=program.kode_program', 'inner');
		// }

		return $this->db->count_all_results();
	}
	/*====== /Serverside Datatable ======*/
	
}

/* End of file Program_model.php */
/* Location: ./application/models/Program_model.php */