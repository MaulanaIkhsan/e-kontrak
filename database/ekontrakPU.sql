-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 15, 2019 at 04:41 AM
-- Server version: 5.7.25-0ubuntu0.18.04.2
-- PHP Version: 7.2.15-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ekontrakPU`
--
DROP DATABASE IF EXISTS `ekontrakPU`;
CREATE DATABASE IF NOT EXISTS `ekontrakPU` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ekontrakPU`;

-- --------------------------------------------------------

--
-- Table structure for table `aktivitas`
--

DROP TABLE IF EXISTS `aktivitas`;
CREATE TABLE `aktivitas` (
  `id` int(11) NOT NULL,
  `nama_aktivitas` varchar(100) NOT NULL,
  `anggaran_awal` bigint(20) NOT NULL,
  `perubahan_anggaran` bigint(20) DEFAULT NULL,
  `hps` bigint(20) NOT NULL,
  `nilai_kontrak` bigint(20) NOT NULL,
  `tgl_pengajuan_pencairan` date DEFAULT NULL,
  `tgl_pencairan` date DEFAULT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `pptk` int(11) DEFAULT NULL,
  `ppkom` int(11) DEFAULT NULL,
  `bendahara` int(11) DEFAULT NULL,
  `pekerjaan_id` int(11) DEFAULT NULL,
  `pengadaan_id` int(11) DEFAULT NULL,
  `jenis_pekerjaan_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aktivitas`
--

INSERT INTO `aktivitas` (`id`, `nama_aktivitas`, `anggaran_awal`, `perubahan_anggaran`, `hps`, `nilai_kontrak`, `tgl_pengajuan_pencairan`, `tgl_pencairan`, `keterangan`, `pptk`, `ppkom`, `bendahara`, `pekerjaan_id`, `pengadaan_id`, `jenis_pekerjaan_id`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Pengadaan Amplop Coklat', 250000000, 250000000, 0, 0, '2019-02-01', '2019-02-05', '', 12, 14, 13, 1, 3, 3, '2019-02-04 09:17:37', '2019-02-28 09:28:20', 1, NULL),
(2, 'Pelebaran irigasi daerah Gunung Pati', 200000000, 200000000, 0, 0, NULL, NULL, '', 19, 13, 12, 1, 1, 1, '2019-02-14 07:35:37', '2019-02-28 09:27:53', 19, NULL);

--
-- Triggers `aktivitas`
--
DROP TRIGGER IF EXISTS `aktivitas_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `aktivitas_BEFORE_INSERT` BEFORE INSERT ON `aktivitas` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `aktivitas_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `aktivitas_BEFORE_UPDATE` BEFORE UPDATE ON `aktivitas` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `api`
--

DROP TABLE IF EXISTS `api`;
CREATE TABLE `api` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `url` char(80) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `api`
--

INSERT INTO `api` (`id`, `nama`, `url`, `created_at`, `updated_at`) VALUES
(1, 'Master data SKPD', 'https://simanggaran.semarangkota.go.id/api/v2/skpd', '2018-07-09 08:14:05', '2018-07-09 08:14:05'),
(2, 'Master data kode rekening', 'https://simanggaran.semarangkota.go.id/api/v2/kode_rekening', '2018-07-09 08:14:05', '2018-07-09 08:14:05'),
(3, 'Master data kegiatan', 'https://simanggaran.semarangkota.go.id/api/v2/kegiatan', '2018-07-09 08:14:05', '2018-07-09 08:14:05'),
(4, 'Master data program', 'https://simanggaran.semarangkota.go.id/api/v2/program', '2018-07-09 08:14:05', '2018-07-09 08:14:05'),
(5, 'Get anggaran', 'https://simanggaran.semarangkota.go.id/api/v2/anggaran', '2018-07-09 08:14:05', '2018-07-09 08:14:05'),
(6, 'Pencairan anggaran kegiatan SKPD', 'https://simanggaran.semarangkota.go.id/api/v2/cair', '2018-07-09 08:14:05', '2018-07-09 08:14:05'),
(7, 'Indikator kegiatan SKPD', 'https://simanggaran.semarangkota.go.id/api/v2/cair', '2018-07-09 08:14:05', '2018-07-09 08:14:05');

--
-- Triggers `api`
--
DROP TRIGGER IF EXISTS `api_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `api_BEFORE_INSERT` BEFORE INSERT ON `api` FOR EACH ROW BEGIN
	set new.created_at = (select now());
    set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `api_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `api_BEFORE_UPDATE` BEFORE UPDATE ON `api` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `dokumen_pihak_ketiga`
--

DROP TABLE IF EXISTS `dokumen_pihak_ketiga`;
CREATE TABLE `dokumen_pihak_ketiga` (
  `id` int(11) NOT NULL,
  `nama_dokumen` varchar(100) DEFAULT NULL,
  `pihak_ketiga_id` int(11) DEFAULT NULL,
  `jenis_dokumen_id` int(11) DEFAULT NULL,
  `file` varchar(250) DEFAULT NULL,
  `tgl_awal_aktif` date DEFAULT NULL,
  `tgl_akhir_aktif` date NOT NULL,
  `is_aktif` enum('y','n') NOT NULL DEFAULT 'n',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dokumen_pihak_ketiga`
--

INSERT INTO `dokumen_pihak_ketiga` (`id`, `nama_dokumen`, `pihak_ketiga_id`, `jenis_dokumen_id`, `file`, `tgl_awal_aktif`, `tgl_akhir_aktif`, `is_aktif`, `created_at`, `updated_at`) VALUES
(19, 'Dokumen Pajak', 1, 1, 'file20190212aIHpMuZtUh094553.pdf', '2019-02-12', '2019-02-13', 'y', '2019-02-12 09:45:53', '2019-02-12 09:45:53');

--
-- Triggers `dokumen_pihak_ketiga`
--
DROP TRIGGER IF EXISTS `dokumen_pihak_ketiga_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `dokumen_pihak_ketiga_BEFORE_INSERT` BEFORE INSERT ON `dokumen_pihak_ketiga` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `dokumen_pihak_ketiga_BEFORE_UDPATE`;
DELIMITER $$
CREATE TRIGGER `dokumen_pihak_ketiga_BEFORE_UDPATE` BEFORE UPDATE ON `dokumen_pihak_ketiga` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `dokumen_template`
--

DROP TABLE IF EXISTS `dokumen_template`;
CREATE TABLE `dokumen_template` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `file` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dokumen_template`
--

INSERT INTO `dokumen_template` (`id`, `nama`, `file`, `created_at`, `updated_at`) VALUES
(1, 'Cover', 'acddf-template-cover.pdf', '2019-03-04 12:41:31', '2019-03-04 12:41:31');

--
-- Triggers `dokumen_template`
--
DROP TRIGGER IF EXISTS `dokumen_template_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `dokumen_template_BEFORE_INSERT` BEFORE INSERT ON `dokumen_template` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `dokumen_template_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `dokumen_template_BEFORE_UPDATE` BEFORE UPDATE ON `dokumen_template` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `jabatan_perusahaan`
--

DROP TABLE IF EXISTS `jabatan_perusahaan`;
CREATE TABLE `jabatan_perusahaan` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan_perusahaan`
--

INSERT INTO `jabatan_perusahaan` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'Direktur', '2019-01-28 07:57:44', '2019-01-28 07:57:44'),
(3, 'Wakil Direktur', '2019-01-28 07:58:02', '2019-01-28 07:58:02'),
(4, 'Sekretaris', '2019-01-28 07:58:13', '2019-01-28 07:58:13'),
(5, 'Tenaga Ahli', '2019-01-28 07:58:22', '2019-01-28 07:58:22');

--
-- Triggers `jabatan_perusahaan`
--
DROP TRIGGER IF EXISTS `jabatan_perusahaan_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `jabatan_perusahaan_BEFORE_INSERT` BEFORE INSERT ON `jabatan_perusahaan` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `jabatan_perusahaan_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `jabatan_perusahaan_BEFORE_UPDATE` BEFORE UPDATE ON `jabatan_perusahaan` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `jenis_dokumen_pihak_ketiga`
--

DROP TABLE IF EXISTS `jenis_dokumen_pihak_ketiga`;
CREATE TABLE `jenis_dokumen_pihak_ketiga` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `is_series` enum('y','n') DEFAULT 'n',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_dokumen_pihak_ketiga`
--

INSERT INTO `jenis_dokumen_pihak_ketiga` (`id`, `nama`, `is_series`, `created_at`, `updated_at`) VALUES
(1, 'Pelaporan Pajak', 'y', '2019-02-06 09:22:35', '2019-02-06 09:22:35'),
(2, 'SIUP', 'n', '2019-02-06 09:22:56', '2019-02-06 09:22:56'),
(3, 'Pengalaman Pekerjaan', 'n', '2019-02-23 10:00:32', '2019-02-23 10:00:32');

--
-- Triggers `jenis_dokumen_pihak_ketiga`
--
DROP TRIGGER IF EXISTS `jenis_dokumen_pihak_ketiga`;
DELIMITER $$
CREATE TRIGGER `jenis_dokumen_pihak_ketiga` BEFORE UPDATE ON `jenis_dokumen_pihak_ketiga` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `jenis_dokumen_pihak_ketiga_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `jenis_dokumen_pihak_ketiga_BEFORE_INSERT` BEFORE INSERT ON `jenis_dokumen_pihak_ketiga` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `jenis_pejabat_pengadaan`
--

DROP TABLE IF EXISTS `jenis_pejabat_pengadaan`;
CREATE TABLE `jenis_pejabat_pengadaan` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_pejabat_pengadaan`
--

INSERT INTO `jenis_pejabat_pengadaan` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'Pejabat Pengadaan Konstruksi', '2019-01-29 15:19:07', '2019-01-29 15:19:07'),
(2, 'Pejabat Pengadaan Barang/Jasa', '2019-01-29 15:19:27', '2019-01-29 15:19:27'),
(3, 'Pejabat Pengadaan Konsultan', '2019-01-29 15:19:41', '2019-01-29 15:19:41');

--
-- Triggers `jenis_pejabat_pengadaan`
--
DROP TRIGGER IF EXISTS `jenis_pejabat_pengadaan_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `jenis_pejabat_pengadaan_BEFORE_INSERT` BEFORE INSERT ON `jenis_pejabat_pengadaan` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `jenis_pejabat_pengadaan_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `jenis_pejabat_pengadaan_BEFORE_UPDATE` BEFORE UPDATE ON `jenis_pejabat_pengadaan` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `jenis_pekerjaan`
--

DROP TABLE IF EXISTS `jenis_pekerjaan`;
CREATE TABLE `jenis_pekerjaan` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `nickname` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_pekerjaan`
--

INSERT INTO `jenis_pekerjaan` (`id`, `nama`, `nickname`, `created_at`, `updated_at`) VALUES
(1, 'Fisik', 'fisik', '2018-07-25 20:48:53', '2018-07-25 20:48:53'),
(3, 'Pengadaan Barang', 'pengadaan barang', '2018-07-25 20:49:22', '2018-07-25 20:49:22'),
(23, 'Jasa Konsultan', 'konsultan', '2018-11-28 12:37:33', '2019-01-28 10:12:48'),
(24, 'Jasa Lainnya', 'lainnya', '2018-12-04 12:01:48', '2019-01-28 10:12:55');

--
-- Triggers `jenis_pekerjaan`
--
DROP TRIGGER IF EXISTS `jenis_pekerjaan_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `jenis_pekerjaan_BEFORE_INSERT` BEFORE INSERT ON `jenis_pekerjaan` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `jenis_pekerjaan_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `jenis_pekerjaan_BEFORE_UPDATE` BEFORE UPDATE ON `jenis_pekerjaan` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `jenis_pengadaan`
--

DROP TABLE IF EXISTS `jenis_pengadaan`;
CREATE TABLE `jenis_pengadaan` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `nickname` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_pengadaan`
--

INSERT INTO `jenis_pengadaan` (`id`, `nama`, `nickname`, `created_at`, `updated_at`) VALUES
(1, 'Lelang', 'L', '2018-07-25 20:49:51', '2019-01-28 10:09:00'),
(2, 'Pengadaan Langsung', 'PL', '2018-07-25 20:50:00', '2018-11-29 19:54:22'),
(3, 'E-Catalog', 'e-catalog', '2018-07-25 20:50:06', '2018-12-04 12:04:29'),
(4, 'Swakelola', 'swa', '2018-07-25 20:50:14', '2019-01-28 10:09:16'),
(5, 'Tender', 'tender', '2018-12-04 12:02:24', '2018-12-04 12:02:24'),
(6, 'Tender Cepat', 'tender cepat', '2018-12-04 12:02:55', '2018-12-04 12:02:55'),
(7, 'Seleksi', 'seleksi', '2018-12-04 12:03:22', '2018-12-04 12:03:22'),
(8, 'Penunjukan Langsung', 'penunjukan langsung', '2018-12-04 12:03:50', '2018-12-04 12:03:50'),
(9, 'E-purchasing', 'e-purchasing', '2018-12-04 12:04:06', '2018-12-04 12:04:06');

--
-- Triggers `jenis_pengadaan`
--
DROP TRIGGER IF EXISTS `jenis_pengadaan_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `jenis_pengadaan_BEFORE_INSERT` BEFORE INSERT ON `jenis_pengadaan` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `jenis_pengadaan_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `jenis_pengadaan_BEFORE_UPDATE` BEFORE UPDATE ON `jenis_pengadaan` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `jenjang_pendidikan`
--

DROP TABLE IF EXISTS `jenjang_pendidikan`;
CREATE TABLE `jenjang_pendidikan` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenjang_pendidikan`
--

INSERT INTO `jenjang_pendidikan` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'SMK', '2019-01-28 09:26:48', '2019-01-28 09:26:48'),
(2, 'SMA', '2019-01-28 09:30:04', '2019-01-28 09:30:04'),
(3, 'S1', '2019-01-28 09:30:17', '2019-01-28 09:30:17'),
(4, 'S2', '2019-01-28 09:30:22', '2019-01-28 09:30:22');

--
-- Triggers `jenjang_pendidikan`
--
DROP TRIGGER IF EXISTS `jenjang_pendidikan_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `jenjang_pendidikan_BEFORE_INSERT` BEFORE INSERT ON `jenjang_pendidikan` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `jenjang_pendidikan_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `jenjang_pendidikan_BEFORE_UPDATE` BEFORE UPDATE ON `jenjang_pendidikan` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

DROP TABLE IF EXISTS `karyawan`;
CREATE TABLE `karyawan` (
  `id` int(11) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `jabatan_id` int(11) DEFAULT NULL,
  `jenjang_pendidikan_id` int(11) DEFAULT NULL,
  `pihak_ketiga_id` int(11) DEFAULT NULL,
  `jenis_kelamin` enum('Pria','Wanita') NOT NULL DEFAULT 'Pria',
  `alamat_lengkap` varchar(250) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `no_telepon` varchar(15) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `dokumen_profil` varchar(250) DEFAULT NULL,
  `dokumen_pendidikan` varchar(250) DEFAULT NULL,
  `dokumen_lainnya` varchar(250) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `is_user` enum('y','n') NOT NULL DEFAULT 'n',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`id`, `nama_lengkap`, `jabatan_id`, `jenjang_pendidikan_id`, `pihak_ketiga_id`, `jenis_kelamin`, `alamat_lengkap`, `tgl_lahir`, `no_telepon`, `email`, `dokumen_profil`, `dokumen_pendidikan`, `dokumen_lainnya`, `username`, `password`, `is_user`, `created_at`, `updated_at`) VALUES
(3, 'isan', 4, 3, NULL, 'Pria', 'ab', '1994-01-20', '123', 'a@mail.com', 'file20190216ytABUXKGPb165710.pdf', NULL, 'file20190309VFPQfvJKnv220223.pdf', 'ikhsan', 'ZHB1c21nMTIz', 'y', '2019-02-12 14:04:37', '2019-03-09 22:02:23'),
(5, 'b', 0, 0, 3, '', '', '0000-00-00', '2', 'bba', '', '', '', 'cige90le', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(6, 'cacaca', 0, 0, 4, '', '', '0000-00-00', '123', 'c', '', '', '', 'hjmjljl6', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(7, 'PT. Parkit Merah', 0, 0, 5, '', '', '0000-00-00', '(234) 1234567', 'parkitmerah@mail.com', '', '', '', '4sqqqt2g', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(8, 'PT. Kuda Terbang', 0, 0, 6, '', '', '0000-00-00', '(024) 1234567', '', '', '', '', '319mhg68', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(9, 'PT. SUMBER ALAM BERKARYA', 0, 0, 7, '', '', '0000-00-00', '(024) 6585220', '', '', '', '', '185fb3a7', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(10, 'CV. KARYA USAHA JAYA', 0, 0, 8, '', '', '0000-00-00', '(024) 6702364', '', '', '', '', 'ck8g94i7', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(11, 'CV. DIAN RAHMA', 0, 0, 9, '', '', '0000-00-00', '', '', '', '', '', '4e346e1g', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(12, 'CV. DANA REJA', 0, 0, 10, '', '', '0000-00-00', '(024) 7663539', '', '', '', '', '1ic9anh1', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(13, 'CV. NADIA UTAMA', 0, 0, 11, '', '', '0000-00-00', '(024) 7473436', '', '', '', '', 'lde58oi0', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(14, 'CV. ANUGRAH PERSADA', 0, 0, 12, '', '', '0000-00-00', '', '', '', '', '', '4gjea5b4', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(15, 'CV. NUR ABADI SEJAHTERA', 0, 0, 13, '', '', '0000-00-00', '', 'cvnurabadisejahtera@yahoo', '', '', '', '72n1p43g', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(16, 'CV. KARYA NAFA PERKASA', 0, 0, 14, '', '', '0000-00-00', '(024) 6717782', '', '', '', '', '4cidiwci', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(17, 'PT.AYA SOPHIA PRIMATAMA', 0, 0, 15, '', '', '0000-00-00', '(024) 7479589', 'pt.aya_sophia_primatama@yahoo.', '', '', '', 'h79ajm61', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(18, 'CV. MAURIS CIPTA MANDIRI', 0, 0, 16, '', '', '0000-00-00', '', '', '', '', '', 'i1b3p8ml', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(19, 'CV. SINAR TIGA MITRA', 0, 0, 17, '', '', '0000-00-00', '', '', '', '', '', 'j19m9nor', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(20, 'PT. FIKANOVA SRI MANUNGAL', 0, 0, 18, '', '', '0000-00-00', '(024) 7615290', '', '', '', '', '7be15f0i', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(21, 'PT.ARMADA HADA GRAHA', 0, 0, 19, '', '', '0000-00-00', '(029) 3366175', 'office@armadahadagraha.co', '', '', '', '66gesilw', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(22, 'PT. PURI SAKTI PERKASA', 0, 0, 20, '', '', '0000-00-00', '(024) 8414147', 'purisakti_perkasa_smg@yah', '', '', '', 'ft0i403x', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(23, 'PT. BUMI PANEN MAKMUR', 0, 0, 21, '', '', '0000-00-00', '(024) 7087853', 'ptbumipanenmakmur@yahoo.c', '', '', '', 'a3ii8d0e', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(24, 'CV.AJI KARYA MEGAH', 0, 0, 22, '', '', '0000-00-00', '(024) 3565942', '', '', '', '', '1pcq9pss', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(25, 'CV. DINARMAS JAYA LAKONT', 0, 0, 23, '', '', '0000-00-00', '', 'cv.dimasjala@yahoo.co.id', '', '', '', '6539d443', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(26, 'PT. REZEKI BERKAH SENTOSA', 0, 0, 24, '', '', '0000-00-00', '(024) 7674181', 'rezekiberkahpt99@gmail.co', '', '', '', '60nc99gb', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(27, 'PT. TIGA PUTRA CIPTA SARANA', 0, 0, 25, '', '', '0000-00-00', '(071) 1369136', 'ciptasarana56@yahoo.co.id', '', '', '', '621ahc4e', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(28, 'PT.CIPTA ARTHA SENTOSA', 0, 0, 26, '', '', '0000-00-00', '(027) 3321904', 'ciptaarthasentosa.pt@gmai', '', '', '', 'epgkq70s', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(29, 'CV.DANU SAKTI', 0, 0, 27, '', '', '0000-00-00', '', 'jatisupri@gmail.com', '', '', '', '1llkbi7a', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(30, 'PT. SEMARANG MULTI CONS', 0, 0, 28, '', '', '0000-00-00', '(024) 3513109', '', '', '', '', '3j5ijj2e', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(31, 'PT.SATRIAMAS KARYATAMA ', 0, 0, 29, '', '', '0000-00-00', '(024) 3563122', 'satriamas.karyatama@yahoo', '', '', '', '1il5j1em', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(32, 'PT. FANIDITA SARANA', 0, 0, 30, '', '', '0000-00-00', '(024) 7674555', '', '', '', '', '2qq3k5hp', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(33, 'CV. MITRA MANDIRI', 0, 0, 31, '', '', '0000-00-00', '', 'mitramandiri259@gmail.com', '', '', '', '5a55d70i', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(34, 'CV. SATRIO PRINGGODANI', 0, 0, 32, '', '', '0000-00-00', '(024) 7625410', 'cv.satriopringgodani@gmai', '', '', '', '1rovo66f', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(35, 'PT. MINA FAJAR ABADI', 0, 0, 33, '', '', '0000-00-00', '', '', '', '', '', '10d96eb6', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(36, 'PT. WRINGINDARI NJAGO', 0, 0, 34, '', '', '0000-00-00', '', '', '', '', '', 'i072d0jf', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(37, 'PT. ARSHY CITRA KAMATO', 0, 0, 35, '', '', '0000-00-00', '', 'acikamatosmrg@gmail.com', '', '', '', '28cccifc', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(38, 'cv. bergairah', 0, 0, 37, '', '', '0000-00-00', '(024) 7403558', 'bukasitikjos@gmail.com', '', '', '', 'hn5oglod', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(39, 'CV. DAPOER NGEBOEL', 0, 0, 38, '', '', '0000-00-00', '(024) 8754475', 'wirosableng@gmail.com', '', '', '', '1rd3j960', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(40, 'CV Ngantuk', 0, 0, 39, '', '', '0000-00-00', '(121) 2212121', 'adith_pratidina@yahoo.co.', '', '', '', '2gfg3ij7', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(41, 'PT. NONGKO', 0, 0, 40, '', '', '0000-00-00', '(024) 123123', 'nongko@gmai.com', '', '', '', '2971bbe0', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(42, 'PT DUA DUA', 0, 0, 41, '', '', '0000-00-00', '(024) 1231234', 'NANGKA@GMAIL.COM', '', '', '', '1bd5808c', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(43, 'PT OPO CV PODO WAE', 0, 0, 42, '', '', '0000-00-00', '(010) 5678912', 'EMAILPT@dah.kom', '', '', '', '10kfcm9j', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(44, 'CV. NADIFHA BAGUS PRASTYO', 0, 0, 43, '', '', '0000-00-00', '(024) 6767676', 'Nadifhagaramjaya@gmail.co', '', '', '', '26cdbee5', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(45, 'PT cantik cantiknya', 0, 0, 44, '', '', '0000-00-00', '(024) 2411989', 'kecebadaiii@gmail.com', '', '', '', 'd4a5ee03', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(46, 'PT NONGKO', 0, 0, 45, '', '', '0000-00-00', '(024) 234234', 'nongko@gmail.com', '', '', '', 'cae8051c', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(47, 'PT.NONGKO', 0, 0, 46, '', '', '0000-00-00', '(024) 123123', 'nongko@gmail.com', '', '', '', '3js65e8r', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(48, 'CV. MENGGODA', 0, 0, 47, '', '', '0000-00-00', '(024) 7777777', 'BUKAWAE@GMAIL.COM', '', '', '', '1l7397rp', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(49, 'PT DURIAN', 0, 0, 48, '', '', '0000-00-00', '(024) 9867858', 'durian@gmail.com', '', '', '', '1577d457', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(50, 'CV. NADIFHA BAGUS PRASTYO', 0, 0, 49, '', '', '0000-00-00', '(024) 6767676', 'Nadifhagaramjaya@gmail.co', '', '', '', 'b9d4807a', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(51, 'PT. NIKU', 0, 0, 50, '', '', '0000-00-00', '(024) 8311506', 'niku@gmail.com', '', '', '', '153gd53d', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(52, 'PT NONGKO', 0, 0, 51, '', '', '0000-00-00', '(024) 123123', 'nongko@gmail.com', '', '', '', '20oxrw6f', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(53, 'PT.NANGKA', 0, 0, 52, '', '', '0000-00-00', '(024) 123123', 'nongko@gmail.com', '', '', '', '1jj744g9', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(54, 'PT. MARGOROTO', 0, 0, 53, '', '', '0000-00-00', '(098) 3374447', '', '', '', '', '7f0dghii', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(55, 'PT. NONGGO', 0, 0, 54, '', '', '0000-00-00', '(024) 6922266', '', '', '', '', '148f2024', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(56, 'PT.DUA ANGKA', 0, 0, 55, '', '', '0000-00-00', '(024) 52518', 'dua_angka@gmail.com', '', '', '', 'gdkkf3dk', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(57, 'maju mundur', 0, 0, 56, '', '', '0000-00-00', '(024) 7666666', 'mudasi@gmail.com', '', '', '', 'fmkdnf1e', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(58, 'CV. MUL JAYA', 0, 0, 57, '', '', '0000-00-00', '(024) 123123', 'muljaya@gimail.com', '', '', '', '257ie1hd', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(59, 'maju mundur', 0, 0, 58, '', '', '0000-00-00', '(024) 7666666', 'mudasi@gmail.com', '', '', '', 'ja85a8ae', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(60, 'maju mundur', 0, 0, 59, '', '', '0000-00-00', '(024) 7666666', 'mudasi@gmail.com', '', '', '', '7p8pi1qq', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(61, 'pt. mudasir', 0, 0, 60, '', '', '0000-00-00', '(024) 5777156', 'zarkonibm2@gmail.com', '', '', '', '7qlfpgqc', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(62, 'pt jj', 0, 0, 61, '', '', '0000-00-00', '(098) 456123', 'botak@gmail.com', '', '', '', 'ipj6ko4m', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(63, 'PT. MANGGA', 0, 0, 62, '', '', '0000-00-00', '(024) 8441254', 'MANGGA@GMAIL.COM', '', '', '', '103i200i', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(64, 'pt. mudasir', 0, 0, 63, '', '', '0000-00-00', '(024) 5777156', 'zarkonibm2@gmail.com', '', '', '', '7c1d04c8', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(65, 'pt noko', 0, 0, 64, '', '', '0000-00-00', '(024) 8665678', 'nongko67@gmail.co', '', '', '', '148f215d', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(66, 'nongko', 0, 0, 65, '', '', '0000-00-00', '(024) 7898', 'nongko@gmail.com', '', '', '', '99k6e702', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(67, 'CV. Kesemek', 0, 0, 66, '', '', '0000-00-00', '(024) 6494396', 'kesemek@gmail.com', '', '', '', '1hsqjdq6', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(68, 'PT SUKA SUKA', 0, 0, 67, '', '', '0000-00-00', '(024) 7625767', 'niriswati@gmail.com', '', '', '', '2f3bhu7l', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(69, 'PT NONGGO', 0, 0, 68, '', '', '0000-00-00', '(021) 123123', 'NAGKA@yAHOO.COM', '', '', '', '5s8c27j3', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(70, 'PT. DUREN MONTONG', 0, 0, 69, '', '', '0000-00-00', '(081) 0810817', 'duren@gmail.com', '', '', '', 'a0bbf962', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(71, 'PT HAWUG2', 0, 0, 70, '', '', '0000-00-00', '(024) 55667', 'cyinnnn@gmail.com', '', '', '', '4ja8ef6e', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(72, 'CV. FAJAR UTAMA', 0, 0, 71, '', '', '0000-00-00', '', '', '', '', '', '6c59a89e', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(73, 'CV. MMR', 0, 0, 72, '', '', '0000-00-00', '(0) ', '', '', '', '', 'ac8993d1', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(74, 'PT. FANIDITA', 0, 0, 73, '', '', '0000-00-00', '(024) 555555', 'fanidita@yahoo.com', '', '', '', '4br1t4d2', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(75, 'PT. MUHANDAS', 0, 0, 74, '', '', '0000-00-00', '(024) 1111144', 'muhandas@gmail.com', '', '', '', '69o97o79', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(76, 'PT. HRV PLANNER CONSULTANT', 0, 0, 75, '', '', '0000-00-00', '(024) ', 'hrv@gmail.com', '', '', '', '4dwjh62a', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(77, 'CV. YUNUS PRAKASA RAKHMANU', 0, 0, 76, '', '', '0000-00-00', '(024) 7643396', 'yunus_rakhmanu@gmail.com', '', '', '', '3h9k15kb', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(78, 'cv,adem ayem', 0, 0, 77, '', '', '0000-00-00', '(024) 1978721', 'ayem123@gmail.com', '', '', '', '10ea9e4k', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15'),
(79, 'CV MAJU MULIA', 0, 0, 78, '', '', '0000-00-00', '(024) 7093963', 'cv_majumulia@yahoo.com', '', '', '', '3c34eb3h', 'ZHB1c21nMTIz', 'y', '2019-02-13 09:46:15', '2019-02-13 09:46:15');

--
-- Triggers `karyawan`
--
DROP TRIGGER IF EXISTS `karyawan_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `karyawan_BEFORE_INSERT` BEFORE INSERT ON `karyawan` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `keryawan_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `keryawan_BEFORE_UPDATE` BEFORE UPDATE ON `karyawan` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_dokumen_lainnya`
--

DROP TABLE IF EXISTS `kontrak_dokumen_lainnya`;
CREATE TABLE `kontrak_dokumen_lainnya` (
  `id` int(11) NOT NULL,
  `kontrak_pekerjaan_id` int(11) DEFAULT NULL,
  `nama` varchar(150) NOT NULL,
  `file` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `kontrak_dokumen_lainnya`
--
DROP TRIGGER IF EXISTS `kontrak_dokumen_lainnnya_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `kontrak_dokumen_lainnnya_BEFORE_UPDATE` BEFORE UPDATE ON `kontrak_dokumen_lainnya` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `kontrak_dokumen_lainnya_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `kontrak_dokumen_lainnya_BEFORE_INSERT` BEFORE INSERT ON `kontrak_dokumen_lainnya` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_dokumen_pihak_ketiga`
--

DROP TABLE IF EXISTS `kontrak_dokumen_pihak_ketiga`;
CREATE TABLE `kontrak_dokumen_pihak_ketiga` (
  `id` int(11) NOT NULL,
  `kontrak_pihak_ketiga_id` int(11) DEFAULT NULL,
  `dokumen_pihak_ketiga_id` int(11) DEFAULT NULL,
  `urutan` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `kontrak_dokumen_pihak_ketiga`
--
DROP TRIGGER IF EXISTS `kontrak_dokumen_pihak_ketiga_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `kontrak_dokumen_pihak_ketiga_BEFORE_INSERT` BEFORE INSERT ON `kontrak_dokumen_pihak_ketiga` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `kontrak_dokumen_pihak_ketiga_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `kontrak_dokumen_pihak_ketiga_BEFORE_UPDATE` BEFORE UPDATE ON `kontrak_dokumen_pihak_ketiga` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_karyawan_pihak_ketiga`
--

DROP TABLE IF EXISTS `kontrak_karyawan_pihak_ketiga`;
CREATE TABLE `kontrak_karyawan_pihak_ketiga` (
  `id` int(11) NOT NULL,
  `kontrak_pihak_ketiga_id` int(11) DEFAULT NULL,
  `karyawan_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_komentar`
--

DROP TABLE IF EXISTS `kontrak_komentar`;
CREATE TABLE `kontrak_komentar` (
  `id` int(11) NOT NULL,
  `kontrak_pekerjaan_id` int(11) DEFAULT NULL,
  `pegawai_id` int(11) DEFAULT NULL,
  `karyawan_id` int(11) DEFAULT NULL,
  `komentar` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `kontrak_komentar`
--
DROP TRIGGER IF EXISTS `kontrak_komentar_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `kontrak_komentar_BEFORE_INSERT` BEFORE INSERT ON `kontrak_komentar` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `kontrak_komentar_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `kontrak_komentar_BEFORE_UPDATE` BEFORE UPDATE ON `kontrak_komentar` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_pekerjaan`
--

DROP TABLE IF EXISTS `kontrak_pekerjaan`;
CREATE TABLE `kontrak_pekerjaan` (
  `id` int(11) NOT NULL,
  `aktivitas_id` int(11) DEFAULT NULL,
  `pejabat_pembuat_komitmen_id` int(11) DEFAULT NULL,
  `pejabat_pengadaan_id` int(11) DEFAULT NULL,
  `tgl_awal_kontrak` date DEFAULT NULL,
  `tgl_akhir_kontrak` date DEFAULT NULL,
  `hps` int(11) NOT NULL,
  `status` enum('Pembukaan','Penawaran','Penetapan','Penunjukan') NOT NULL DEFAULT 'Pembukaan',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kontrak_pekerjaan`
--

INSERT INTO `kontrak_pekerjaan` (`id`, `aktivitas_id`, `pejabat_pembuat_komitmen_id`, `pejabat_pengadaan_id`, `tgl_awal_kontrak`, `tgl_akhir_kontrak`, `hps`, `status`, `created_at`, `updated_at`) VALUES
(3, 2, 11, 1, '2019-04-01', '2019-04-30', 200000000, 'Pembukaan', '2019-03-03 20:25:49', '2019-03-12 08:08:06');

--
-- Triggers `kontrak_pekerjaan`
--
DROP TRIGGER IF EXISTS `kontrak_pekerjaan_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `kontrak_pekerjaan_BEFORE_INSERT` BEFORE INSERT ON `kontrak_pekerjaan` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `kontrak_pekerjaan_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `kontrak_pekerjaan_BEFORE_UPDATE` BEFORE UPDATE ON `kontrak_pekerjaan` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_petugas_pihak_ketiga`
--

DROP TABLE IF EXISTS `kontrak_petugas_pihak_ketiga`;
CREATE TABLE `kontrak_petugas_pihak_ketiga` (
  `id` int(11) NOT NULL,
  `kontrak_pihak_ketiga_id` int(11) DEFAULT NULL,
  `karyawan_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `kontrak_petugas_pihak_ketiga`
--
DROP TRIGGER IF EXISTS `kontnrak_petugas_pihak_ketiga_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `kontnrak_petugas_pihak_ketiga_BEFORE_UPDATE` BEFORE UPDATE ON `kontrak_petugas_pihak_ketiga` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `kontrak_petugas_pihak_ketiga_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `kontrak_petugas_pihak_ketiga_BEFORE_INSERT` BEFORE INSERT ON `kontrak_petugas_pihak_ketiga` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_pihak_ketiga`
--

DROP TABLE IF EXISTS `kontrak_pihak_ketiga`;
CREATE TABLE `kontrak_pihak_ketiga` (
  `id` int(11) NOT NULL,
  `kontrak_pekerjaan_id` int(11) DEFAULT NULL,
  `pihak_ketiga_id` int(11) DEFAULT NULL,
  `nilai_kontrak` bigint(20) DEFAULT NULL,
  `lampiran` varchar(200) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `kontrak_pihak_ketiga`
--
DROP TRIGGER IF EXISTS `kontrak_pihak_ketiga_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `kontrak_pihak_ketiga_BEFORE_INSERT` BEFORE INSERT ON `kontrak_pihak_ketiga` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `kontrak_pihak_ketiga_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `kontrak_pihak_ketiga_BEFORE_UPDATE` BEFORE UPDATE ON `kontrak_pihak_ketiga` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `kontrak_surat_penawaran`
--

DROP TABLE IF EXISTS `kontrak_surat_penawaran`;
CREATE TABLE `kontrak_surat_penawaran` (
  `id` int(11) NOT NULL,
  `kontrak_pekerjaan_id` int(11) DEFAULT NULL,
  `template_surat_id` int(11) DEFAULT NULL,
  `tgl_surat` date NOT NULL,
  `no_surat` varchar(100) NOT NULL,
  `lampiran` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `kontrak_surat_penawaran`
--
DROP TRIGGER IF EXISTS `kontrak_surat_penawaran_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `kontrak_surat_penawaran_BEFORE_INSERT` BEFORE INSERT ON `kontrak_surat_penawaran` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `kontrak_surat_penawaran_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `kontrak_surat_penawaran_BEFORE_UPDATE` BEFORE UPDATE ON `kontrak_surat_penawaran` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

DROP TABLE IF EXISTS `pegawai`;
CREATE TABLE `pegawai` (
  `id` int(11) NOT NULL,
  `nip` varchar(30) DEFAULT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `jenis_kelamin` enum('Pria','Wanita') NOT NULL DEFAULT 'Pria',
  `alamat` varchar(150) DEFAULT NULL,
  `no_telepon` varchar(13) DEFAULT NULL,
  `is_pegawai` enum('y','n') NOT NULL DEFAULT 'y',
  `unit_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `username` varchar(25) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id`, `nip`, `nama_lengkap`, `jenis_kelamin`, `alamat`, `no_telepon`, `is_pegawai`, `unit_id`, `role_id`, `username`, `password`, `created_at`, `updated_at`) VALUES
(1, '123456789', 'Super Admin Pegawai', 'Wanita', 'kosong', '123456789', 'n', 4, 6, 'superadmin', 'ZHB1c21nMTIz', '2019-01-29 09:57:30', '2019-02-28 13:31:41'),
(11, NULL, ' Dede Bambang Hartono, SE, MM ', 'Pria', NULL, NULL, 'y', 1, NULL, 'dede', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(12, '19770516 201001 1 015', ' Arief Eko Wibowo,ST ', 'Pria', NULL, NULL, 'y', 1, NULL, 'arief', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(13, '', ' Maileni, ST. Msi ', 'Pria', NULL, NULL, 'y', 35, NULL, 'maileni', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(14, '123', 'Titus Tonny W, ST', 'Pria', 'Jl. Semarang 2', NULL, 'y', 4, 7, 'titus', 'ZHB1c21nMTIz', '2019-01-30 07:25:54', '2019-03-03 20:18:14'),
(15, '123', ' Danang Hadicara, ST ', 'Pria', 'Jl. Semarang I', NULL, 'y', 4, 5, 'danang', 'ZHB1c21nMTIz', '2019-01-30 07:25:54', '2019-02-13 15:07:40'),
(16, '', ' Irwan Candra Purnama, ST ', 'Pria', NULL, NULL, 'y', 4, NULL, 'irwan', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(17, '', ' Kirana Prasetya Azizah, ST ', 'Pria', NULL, NULL, 'y', 4, NULL, 'kirana', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(18, NULL, ' Wahyu Anggoro, ST ', 'Pria', NULL, NULL, 'y', 5, NULL, 'wahyu', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(19, '123', 'Indri Bariyanti Hapsari, ST', 'Wanita', 'Jl. Simpang Lima No 10', '123', 'y', 4, 7, 'indri', 'WkhCMWMyMW5NVEl6', '2019-01-30 07:25:54', '2019-03-03 20:19:49'),
(20, NULL, ' Sutomo ', 'Pria', NULL, NULL, 'y', 5, NULL, 'sutomo', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(21, NULL, ' Mulyarto, ST ', 'Pria', NULL, NULL, 'y', 5, NULL, 'mulyarto', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(22, '', ' Yoyok Wiratmoko,ST ', 'Pria', NULL, NULL, 'y', 4, NULL, 'yoyok', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(23, '', ' Dwi Supriyadi, ST ', 'Pria', NULL, NULL, 'y', 4, NULL, 'dwi', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(24, '', ' M. Teqi Wijaya,ST ', 'Pria', NULL, NULL, 'y', 4, NULL, 'teqi', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(25, '', ' Mujiyono ', 'Pria', NULL, NULL, 'y', 5, NULL, 'mujiyono', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(26, '19680203 199703 1 0006', 'Ir. Iswar Aminuddin, MT.', 'Pria', NULL, NULL, 'y', 32, NULL, 'iswar', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(27, '196812101988011002', 'SAELAN, ST', 'Pria', NULL, NULL, 'y', 8, NULL, 'SAELAN', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(28, '19850720 201001 1 018', 'Endri Dwi Purwanto, ST', 'Pria', NULL, NULL, 'y', 33, NULL, 'endri', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(29, '19740517 200901 1 008', 'Ulya Adiwibawa, ST', 'Pria', NULL, NULL, 'y', 8, NULL, 'ulya', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(30, '19680325 199203 1 010', 'SIH RIANUNG. ST. MT', 'Pria', NULL, NULL, 'y', 1, NULL, 'sih', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(31, '19710119 200604 1 009', 'KUSTADI, ST', 'Pria', NULL, NULL, 'y', 34, NULL, 'kustadi', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(32, '19650608 198903 1 008', 'C. AREF DWI HARYONO. ST. MT', 'Pria', NULL, NULL, 'y', 12, NULL, 'aref', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(33, '19770420 200501 1 005', 'VICTOR TRI KARYANTO NUGROHO, ST, MT', 'Pria', NULL, NULL, 'y', 12, NULL, 'victor', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(34, '19600914 198703 1 009', 'KUMBINO, ST, MM', 'Pria', NULL, NULL, 'y', 12, NULL, 'KUMBINO', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(35, '19630723 198903 1 006', 'Ir. SUPARJIYATNO', 'Pria', NULL, NULL, 'y', 4, NULL, 'SUP', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(36, '19660120 200604 2 006', 'Ir. YENI RISAWATI', 'Pria', NULL, NULL, 'y', 7, NULL, 'YENI', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(37, '19601107 199203 1 004', 'WIWI WIDJANARKO, S.ST', 'Pria', NULL, NULL, 'y', 38, NULL, 'WIWI', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(38, '19730617 199803 1 006', 'FARIDIAN BAKHTIAR, ST', 'Pria', NULL, NULL, 'y', 4, NULL, 'faridian', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(39, '19670309 199103 1 008', 'ZARKONI. ST', 'Pria', NULL, NULL, 'y', 8, NULL, 'ZARKONI', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(40, '19690706 199003 1 005', 'DANI DWI TJAHYONO, ST', 'Pria', NULL, NULL, 'y', 8, NULL, 'DANI', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(41, '19710419 199803 1 008', 'GUNTUR RACHMANTO. ST', 'Pria', NULL, NULL, 'y', 39, NULL, 'GUNTUR', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(42, '19890518 201101 1 004', 'UMAR ABDULLAH, ST', 'Pria', NULL, NULL, 'y', 8, NULL, 'umar', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(43, '19741119 200901 1 001', 'SUKO NUGROHO, ST', 'Pria', NULL, NULL, 'y', 34, NULL, 'suko', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(44, '19850713 201001 1 018', 'ANUGRAH WITJAKSONO, ST,M.SI', 'Pria', NULL, NULL, 'y', 8, NULL, 'anugrah', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(45, '19870818 201001 1 007', 'CHOIRUL IMAN, ST', 'Pria', NULL, NULL, 'y', 8, NULL, 'choirul', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(46, '19771021 200604 2 001', 'SURIYATY. ST. MM', 'Pria', NULL, NULL, 'y', 8, NULL, 'suriyaty', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(47, '19860307 201001 2 021', 'PRANANING NUSADHANI, ST', 'Pria', NULL, NULL, 'y', 8, NULL, 'prananing', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(48, '19610115 198901 1 002', 'TRI MARDOKO, ST', 'Pria', NULL, NULL, 'y', 8, NULL, 'tri', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(49, '19640322 198603 1 005', ' M. JUMIAN. S.ST', 'Pria', NULL, NULL, 'y', 36, NULL, 'jumian', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(50, '1983032 201001 1 024', 'ADITYO GINEUNG PRATIDINA,SS', 'Pria', NULL, NULL, 'y', 1, NULL, 'adityo', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(51, '19861029 201101 1 010', 'MUHAMAD YASIN', 'Pria', NULL, NULL, 'y', 1, NULL, 'yasin', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(52, '19760105 200901 1 002', 'TUNGGUL HAPSORO ADHI, ST', 'Pria', NULL, NULL, 'y', 16, NULL, 'tunggul', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(53, '19691113 200901 1 001', 'M. DHONI SURYO PRAMONO AMD', 'Pria', NULL, NULL, 'y', 12, NULL, 'dhoni', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(54, '', 'admin', 'Pria', NULL, NULL, 'y', 4, NULL, 'admin', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(55, '', 'Isworo Syamsul Hadi, ST', 'Pria', NULL, NULL, 'y', 4, NULL, 'Isworo', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(56, '', 'Petrus Siswahyudi,ST', 'Pria', NULL, NULL, 'y', 36, NULL, 'Petrus', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(57, '', 'Mudasir,ST', 'Pria', NULL, NULL, 'y', 33, NULL, 'Mudasir', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(58, '', 'Tiara Setiawan, ST', 'Pria', NULL, NULL, 'y', 8, NULL, 'Tiara', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(59, '', 'Riyadi Lilik  Priyono,ST', 'Pria', NULL, NULL, 'y', 12, NULL, 'Riyadi', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(60, '', 'Budhi Tri Nasril,ST', 'Pria', NULL, NULL, 'y', 12, NULL, 'Budhi', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(61, '', 'Edi Purwanto,ST', 'Pria', NULL, NULL, 'y', 12, NULL, 'Edi', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(62, '', 'Surono,ST', 'Pria', NULL, NULL, 'y', 4, NULL, 'Surono', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(63, '', 'Yunus Rakhmanung,ST', 'Pria', NULL, NULL, 'y', 1, NULL, 'Yunus', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(64, '', 'Abdul Rochman,ST', 'Pria', NULL, NULL, 'y', 4, NULL, 'Abdul', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(65, '', 'ALIS DHARMA OKTAVARIN, SE', 'Pria', NULL, NULL, 'y', 4, NULL, 'Alis', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(66, '', 'AGUS TRI PRASETYA, SE', 'Pria', NULL, NULL, 'y', 4, NULL, 'Agus', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(67, '', 'PRIYONO, SE', 'Pria', NULL, NULL, 'y', 4, NULL, 'Priyono', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(68, '', 'DARYANTO, SST', 'Pria', NULL, NULL, 'y', 4, NULL, 'Daryanto', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(69, '', 'FUAD KHOIRON', 'Pria', NULL, NULL, 'y', 4, NULL, 'Fuad', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(70, '', 'SIGIT DWI SULISTYORINI', 'Pria', NULL, NULL, 'y', 4, NULL, 'Sigit', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(71, '', 'MUJI BAGUS PRASETYO', 'Pria', NULL, NULL, 'y', 4, NULL, 'Muji', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(72, '', 'HER MARGONO,SE', 'Pria', NULL, NULL, 'y', 4, NULL, 'Her', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(73, '', 'WIDYARINI, SE', 'Pria', NULL, NULL, 'y', 4, NULL, 'Widyarini', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(74, '', 'VINCENTIA PUTRI KIRANA L., SE', 'Pria', NULL, NULL, 'y', 4, NULL, 'Vincentia', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(75, '', 'Rr. ATIK DIANA PRIYANTI, SE', 'Pria', NULL, NULL, 'y', 4, NULL, 'Atik', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(76, '', 'ENDANG YULIARTI, A.Md', 'Pria', NULL, NULL, 'y', 4, NULL, 'Endang', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(77, '', 'SITI FATHONAH, SE', 'Pria', NULL, NULL, 'y', 4, NULL, 'Siti', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(78, '', 'OKTI HERMANTO', 'Pria', NULL, NULL, 'y', 4, NULL, 'Okti', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(79, '', 'KUNDORI, SE', 'Pria', NULL, NULL, 'y', 4, NULL, 'Kundori', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(80, '', 'DANANG TRI PRASETYO', 'Pria', NULL, NULL, 'y', 4, NULL, 'Danang_Tri', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(81, '0', ' Anto Toto W, SH ', 'Pria', NULL, NULL, 'y', 1, NULL, 'anto', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(82, '0', 'M. HISAM ASHARI, ST', 'Pria', NULL, NULL, 'y', 12, NULL, 'hisam', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(83, '197821072010011019', 'WIRA ARJUANDA, AMD', 'Pria', NULL, NULL, 'y', 36, NULL, 'wira', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(84, '123', 'ASMUNI', 'Pria', NULL, NULL, 'y', 8, NULL, 'asmuni', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(85, '123', 'JUMADI', 'Pria', NULL, NULL, 'y', 36, NULL, 'jumadi', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(86, '123', 'Endah Pramudiastuti, SH', 'Pria', NULL, NULL, 'y', 4, NULL, 'endah', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(87, '123', 'Wiwi Nugrahwati, SE', 'Pria', NULL, NULL, 'y', 4, NULL, 'wiwi_nugrahwati', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(88, '123', 'Siti Fatonah', 'Pria', NULL, NULL, 'y', 4, NULL, 'siti_fatonah', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(89, '123', 'Wikanda Tarnanto, SE', 'Pria', NULL, NULL, 'y', 4, NULL, 'wikanda', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(90, '', 'Nanang Tri Sanyoto, ST', 'Pria', NULL, NULL, 'y', 12, NULL, 'nanang_tri', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(91, '', 'Theresia Suyatmi', 'Pria', NULL, NULL, 'y', 8, NULL, 'theresia', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(92, '', 'Kartikaningsih, ST', 'Pria', NULL, NULL, 'y', 8, NULL, 'kartikaningsih', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(93, '', 'Marsidi', 'Pria', NULL, NULL, 'y', 39, NULL, 'marsidi', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(94, '', 'Tony Budiyanto', 'Pria', NULL, NULL, 'y', 39, NULL, 'tony_budiyanto', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(95, '', 'Fendy', 'Pria', NULL, NULL, 'y', 16, NULL, 'fendy', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(96, '', 'muslimin', 'Pria', NULL, NULL, 'y', 16, NULL, 'muslimin', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(97, '', 'Sudarsono, A. Md', 'Pria', NULL, NULL, 'y', 16, NULL, 'sudarsono', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(98, '123', 'John', 'Pria', NULL, NULL, 'y', 1, NULL, 'john', NULL, '2019-01-30 07:25:54', '2019-01-30 07:25:54'),
(99, '123', 'ikhsan', 'Pria', 'a', '123', 'y', 8, 5, 'ikhsan', 'ZHB1c21nMTIz', '2019-02-13 13:00:44', '2019-02-19 11:47:19');

--
-- Triggers `pegawai`
--
DROP TRIGGER IF EXISTS `pegawai_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `pegawai_BEFORE_INSERT` BEFORE INSERT ON `pegawai` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `pegawai_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `pegawai_BEFORE_UPDATE` BEFORE UPDATE ON `pegawai` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pejabat_pengadaan`
--

DROP TABLE IF EXISTS `pejabat_pengadaan`;
CREATE TABLE `pejabat_pengadaan` (
  `id` int(11) NOT NULL,
  `pegawai_id` int(11) DEFAULT NULL,
  `jenis_pejabat_pengadaan_id` int(11) DEFAULT NULL,
  `tingkat_id` int(11) DEFAULT NULL,
  `tahun` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pejabat_pengadaan`
--

INSERT INTO `pejabat_pengadaan` (`id`, `pegawai_id`, `jenis_pejabat_pengadaan_id`, `tingkat_id`, `tahun`, `created_at`, `updated_at`) VALUES
(1, 19, 2, 1, 2019, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pekerjaan`
--

DROP TABLE IF EXISTS `pekerjaan`;
CREATE TABLE `pekerjaan` (
  `id` int(11) NOT NULL,
  `nama_pekerjaan` varchar(200) DEFAULT NULL,
  `kode_rekening` varchar(45) NOT NULL,
  `anggaran_awal` bigint(20) DEFAULT NULL,
  `tahun` int(4) DEFAULT NULL,
  `kode_pekerjaan` varchar(45) DEFAULT NULL,
  `kode_program` varchar(45) DEFAULT NULL,
  `tahap_pekerjaan` enum('Murni','Pergeseran','Perubahan') DEFAULT NULL,
  `uniq_code` varchar(15) DEFAULT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pekerjaan`
--

INSERT INTO `pekerjaan` (`id`, `nama_pekerjaan`, `kode_rekening`, `anggaran_awal`, `tahun`, `kode_pekerjaan`, `kode_program`, `tahap_pekerjaan`, `uniq_code`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'PENYEDIAAN JASA SURAT MENYURAT', '', 20000000, 2019, '1.1.03.1.1.03.01.01.001', 'Pardm77r3r180', 'Pergeseran', 'p35b1b26350c1', NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:51'),
(2, 'PENYEDIAAN JASA KOMUNIKASI, SUMBER DAYA AIR DAN LISTRIK', '', 3800000000, 2019, '1.1.03.1.1.03.01.01.002', 'Pardm77r3r180', 'Pergeseran', 'p5iw45fy05esk', NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:51'),
(3, 'PENYEDIAAN JASA KEBERSIHAN KANTOR', '', 2730000000, 2019, '1.1.03.1.1.03.01.01.008', 'Pardm77r3r180', 'Pergeseran', 'pfzrxma2bpvs4', NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:51'),
(4, 'PENYEDIAAN ALAT TULIS KANTOR', '', 247448000, 2019, '1.1.03.1.1.03.01.01.010', 'Pardm77r3r180', 'Pergeseran', 'p6d7adbad2552', NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:52'),
(5, 'PENYEDIAAN BARANG CETAKAN DAN PENGGANDAAN', '', 200000000, 2019, '1.1.03.1.1.03.01.01.011', 'Pardm77r3r180', 'Pergeseran', 'pd6g5f28j9aa1', NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:52'),
(6, 'PENYEDIAAN KOMPONEN INSTALASI LISTRIK / PENERANGAN BANGUNAN KANTOR', '', 290000000, 2019, '1.1.03.1.1.03.01.01.012', 'Pardm77r3r180', 'Pergeseran', 'ppnmhjmwl2wpj', NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:52'),
(7, 'PENYEDIAAN MAKANAN DAN MINUMAN', '', 414369000, 2019, '1.1.03.1.1.03.01.01.017', 'Pardm77r3r180', 'Pergeseran', 'p1nb8fbfjd1ef', NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:52'),
(8, 'RAPAT-RAPAT KOORDINASI DAN KONSULTASI KE LUAR DAERAH', '', 650000000, 2019, '1.1.03.1.1.03.01.01.018', 'Pardm77r3r180', 'Pergeseran', 'p68djb4aai4e1', NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:52'),
(9, 'RAPAT-RAPAT KOORDINASI DAN KONSULTASI DALAM DAERAH', '', 40000000, 2019, '1.1.03.1.1.03.01.01.028', 'Pardm77r3r180', 'Pergeseran', 'pd3a7d2b9e31i', NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:52'),
(10, 'Penyediaan Peralatan Kebersihan dan Bahan Pembersih', '', 20000000, 2019, '1.1.03.1.1.03.01.01.143', 'Pardm77r3r180', 'Pergeseran', 'p9jjdh7bffhh1', NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:52'),
(11, 'Belanja Jasa Penunjang Administrasi Perkantoran', '', 5000000000, 2019, '1.1.03.1.1.03.01.01.154', 'Pardm77r3r180', 'Pergeseran', 'pdb9030b77a24', NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:52'),
(12, 'PEMBANGUNAN GEDUNG KANTOR', '', 12750000000, 2019, '1.1.03.1.1.03.01.02.003', 'P179a4rbj7ol6', 'Pergeseran', 'p1aki7efhhib1', NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:52'),
(13, 'PENYUSUNAN PELAPORAN KEUANGAN SEMESTERAN', '', 5000000, 2019, '1.1.03.1.1.03.01.06.022', 'Ptv3tt4xjq0gs', 'Pergeseran', 'p1lvf7xtbfjnf', NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:52'),
(14, 'REHABILITASI / PEMELIHARAAN JARINGAN IRIGASI', '', 3007705000, 2019, '1.1.03.1.1.03.01.21.010', 'P112d22cb47f9', 'Pergeseran', 'p130ea1hij5gg', NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:52'),
(15, 'PENINGKATAN PEMBANGUNAN PUSAT-PUSAT PENGENDALI BANJIR', '', 3000000000, 2019, '1.1.03.1.1.03.01.24.008', 'P1a1s38cf39ca', 'Pergeseran', 'p1p0ysjufe37j', NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:52'),
(16, 'PEMBEBASAN LAHAN DALAM RANGKA PEMBANGUNAN INFRASTRUKTUR', '', 39000000000, 2019, '1.1.03.1.1.03.01.25.026', 'P3298f9g12h31', 'Pergeseran', 'p88789ebj4fee', NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:52'),
(17, 'Pengembangan Inner Ring Road', '', 3000000000, 2019, '1.1.03.1.1.03.01.41.001', 'P3gf97cndoj8g', 'Pergeseran', 'pbl751ga53fao', NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:52'),
(18, 'PERBAIKAN SALURAN DRAINASE/GORONG-GORONG SISTEM WILAYAH TIMUR', '', 14000000000, 2019, '1.1.03.1.1.03.01.38.001', 'P6sqpmmuvabl0', 'Pergeseran', 'p25l82io17ra8', NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:52'),
(19, 'SURVEY DAN PENGUKURAN', '', 300000000, 2019, '1.1.03.1.1.03.01.42.001', 'P1okell495me1', 'Pergeseran', 'p2blshibis3un', NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:52'),
(20, 'PENGADAAN ALAT BERAT SDA DAN DRAINASE', '', 5500000000, 2019, '1.1.03.1.1.03.01.39.002', 'P8n3co17c5qc0', 'Pergeseran', 'p285fpqm21lbq', NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:53'),
(21, 'Pemeliharaan Jalan dan Drainase Wilayah I', '', 2000000000, 2019, '1.1.03.1.1.03.01.43.011', 'Pb4cye78qssax', 'Pergeseran', 'p6bj9acafl00k', NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:53');

--
-- Triggers `pekerjaan`
--
DROP TRIGGER IF EXISTS `pekerjaan_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `pekerjaan_BEFORE_UPDATE` BEFORE UPDATE ON `pekerjaan` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `pekerjaan_before_insert`;
DELIMITER $$
CREATE TRIGGER `pekerjaan_before_insert` BEFORE INSERT ON `pekerjaan` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pihak_ketiga`
--

DROP TABLE IF EXISTS `pihak_ketiga`;
CREATE TABLE `pihak_ketiga` (
  `id` int(11) NOT NULL,
  `nama_perusahaan` varchar(100) DEFAULT NULL,
  `alamat_lengkap` varchar(250) DEFAULT NULL,
  `no_telepon` varchar(15) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `status` enum('Cabang','Pusat') DEFAULT 'Pusat',
  `alamat_pusat` varchar(250) DEFAULT NULL,
  `no_telepon_pusat` varchar(15) DEFAULT NULL,
  `email_pusat` varchar(30) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pihak_ketiga`
--

INSERT INTO `pihak_ketiga` (`id`, `nama_perusahaan`, `alamat_lengkap`, `no_telepon`, `email`, `status`, `alamat_pusat`, `no_telepon_pusat`, `email_pusat`, `created_at`, `updated_at`) VALUES
(1, 'a', 'a', '123456789', 'a@mail.com', 'Pusat', '', '', '', '2019-02-06 18:56:50', '2019-02-25 13:05:42'),
(3, 'b', 'b', '2', 'bba', 'Pusat', '', '', '', '2019-02-06 21:06:33', '2019-02-06 21:25:41'),
(5, 'PT. Parkit Merah', 'Jl. Beo II, No 9', '(234) 1234567', 'parkitmerah@mail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(6, 'PT. Kuda Terbang', 'Jl. Kuda No. 98', '(024) 1234567', '', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(7, 'PT. SUMBER ALAM BERKARYA', 'Jl. Tambak Dalam Raya No. 19 - Semarang (Kota', '(024) 6585220', '', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(8, 'CV. KARYA USAHA JAYA', 'SIDOLUHUR RAYA NO.12 RT 3 RW 5 - Semarang (Ko', '(024) 6702364', '', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(9, 'CV. DIAN RAHMA', 'Surodadi RT 02 RW II Sayung Demak', '', '', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(10, 'CV. DANA REJA', 'JL. Srinindito I No 6 Semarang', '(024) 7663539', '', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(11, 'CV. NADIA UTAMA', 'JL.Diponegoro II No.16 Banyumanik Semarang\r\n', '(024) 7473436', '', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(12, 'CV. ANUGRAH PERSADA', 'JL.Bukit Bougenvil Raya Blok B No.26 Semarang', '', '', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(13, 'CV. NUR ABADI SEJAHTERA', 'JL.Ngemplak No.7 RT.03 RW.09 Kel.Tandang Kec.', '', 'cvnurabadisejahtera@yahoo', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(14, 'CV. KARYA NAFA PERKASA', 'JL. Sendangguwo Raya No 50 RT 06 RW 10 Semara', '(024) 6717782', '', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(15, 'PT.AYA SOPHIA PRIMATAMA', 'JL.Kanfer Raya Q-14 Semarang 50267', '(024) 7479589', 'pt.aya_sophia_primatama@yahoo.', 'Pusat', '', '', '', '2019-02-13 08:53:45', '2019-02-13 08:57:03'),
(16, 'CV. MAURIS CIPTA MANDIRI', 'JL. Pedurungan Tengah 29 No.29 RT.02 RW.10 Pe', '', '', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(17, 'CV. SINAR TIGA MITRA', 'JL. Surtikanti VII No.11 Semarang\r\n', '', '', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(18, 'PT. FIKANOVA SRI MANUNGAL', 'JL. Dworowati Raya No 24, Semarang\r\n', '(024) 7615290', '', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(19, 'PT.ARMADA HADA GRAHA', 'JL.Delima No. 41-43 Armada Estate, Magelang -', '(029) 3366175', 'office@armadahadagraha.co', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(20, 'PT. PURI SAKTI PERKASA', 'JL. Teuku Umar No 18 A Semarang - 50234', '(024) 8414147', 'purisakti_perkasa_smg@yah', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(21, 'PT. BUMI PANEN MAKMUR', 'JL. Gondang Timur IV D.3 No.01 Perum Grand Te', '(024) 7087853', 'ptbumipanenmakmur@yahoo.c', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(22, 'CV.AJI KARYA MEGAH', 'JL.Layur No87 A Semarang\r\n', '(024) 3565942', '', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(23, 'CV. DINARMAS JAYA LAKONT', 'Perum Dinar Indah 1B No.07, RT.01 RW.26 Kec.T', '', 'cv.dimasjala@yahoo.co.id', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(24, 'PT. REZEKI BERKAH SENTOSA', 'JL.Bukit Bougenvil Raya Blok B No.26 Semarang', '(024) 7674181', 'rezekiberkahpt99@gmail.co', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(25, 'PT. TIGA PUTRA CIPTA SARANA', 'JL.Proklamasi Blok J No.12 A Lt.1 RT.32 RW.09', '(071) 1369136', 'ciptasarana56@yahoo.co.id', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(26, 'PT.CIPTA ARTHA SENTOSA', 'Jl. Gatot Subroto No 41 Rt 02 Rw 05 Wonokarto', '(027) 3321904', 'ciptaarthasentosa.pt@gmai', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(27, 'CV.DANU SAKTI', 'Jl. CANDISARI No. 16 SEMARANG\r\n', '', 'jatisupri@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(28, 'PT. SEMARANG MULTI CONS', 'JL. NAKULA RAYA NO. 51 SEMARANG - Semarang (K', '(024) 3513109', '', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(29, 'PT.SATRIAMAS KARYATAMA ', 'JL. SINDORO NO. 1 KEL. LEMPONG SARI KEC. GAJA', '(024) 3563122', 'satriamas.karyatama@yahoo', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(30, 'PT. FANIDITA SARANA', 'Jl.Sapta Prasetya Utara X no. 74, Pedurungan,', '(024) 7674555', '', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(31, 'CV. MITRA MANDIRI', 'Jl. Kedungwinong RT 02 RW III Kel. Meteseh Ke', '', 'mitramandiri259@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(32, 'CV. SATRIO PRINGGODANI', 'Jln. Pringgodani I no. 26 Semarang\r\n', '(024) 7625410', 'cv.satriopringgodani@gmai', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(33, 'PT. MINA FAJAR ABADI', 'Jl. Kuala Kagok Dusun Mesjid Gampong Keude Ka', '', '', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(34, 'PT. WRINGINDARI NJAGO', 'JL. MUGAS DALAM XIII NO. 05 RT 008 RW 004 SEM', '', '', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(35, 'PT. ARSHY CITRA KAMATO', 'Jl.Seranti No 5 Air Tawar Timur Padang 25132 ', '', 'acikamatosmrg@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(37, 'cv. bergairah', 'Jl. Tembalang Sidomukti Selatan ', '(024) 7403558', 'bukasitikjos@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(38, 'CV. DAPOER NGEBOEL', 'Jl. Sambiroto Baru D 14 Kedungmundu Semarang', '(024) 8754475', 'wirosableng@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(39, 'CV Ngantuk', 'Jl Ngantuk', '(121) 2212121', 'adith_pratidina@yahoo.co.', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(40, 'PT. NONGKO', 'JL. Nangka No. 5 Semarang', '(024) 123123', 'nongko@gmai.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(41, 'PT DUA DUA', 'JL.NANGKA NO.5', '(024) 1231234', 'NANGKA@GMAIL.COM', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(42, 'PT OPO CV PODO WAE', 'JL. KESASAR', '(010) 5678912', 'EMAILPT@dah.kom', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(43, 'CV. NADIFHA BAGUS PRASTYO', 'JL. KANFER UTARA IV/128', '(024) 6767676', 'Nadifhagaramjaya@gmail.co', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(44, 'PT cantik cantiknya', 'jalan mawar indah raya no.123', '(024) 2411989', 'kecebadaiii@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(45, 'PT NONGKO', 'JALAN NANGKA NO 5', '(024) 234234', 'nongko@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(46, 'PT.NONGKO', 'JL. NANGKA NO 5', '(024) 123123', 'nongko@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(47, 'CV. MENGGODA', 'JL. ASMARA TINGKUNGAN BAHAYA NO. 1', '(024) 7777777', 'BUKAWAE@GMAIL.COM', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(48, 'PT DURIAN', 'Jl DURIAN 1/10', '(024) 9867858', 'durian@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(49, 'CV. NADIFHA BAGUS PRASTYO', 'JL. KANFER UTARA IV/128', '(024) 6767676', 'Nadifhagaramjaya@gmail.co', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(50, 'PT. NIKU', 'Jl. Goyang No.5', '(024) 8311506', 'niku@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(51, 'PT NONGKO', 'JL NANGKA NO 5', '(024) 123123', 'nongko@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(52, 'PT.NANGKA', 'Jl.Nangka', '(024) 123123', 'nongko@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(53, 'PT. MARGOROTO', 'JL. BLABLABLA', '(098) 3374447', '', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(54, 'PT. NONGGO', 'JEDUNG', '(024) 6922266', '', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(55, 'PT.DUA ANGKA', 'JL.Nangka No.5', '(024) 52518', 'dua_angka@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(56, 'maju mundur', 'sendangguwo', '(024) 7666666', 'mudasi@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(57, 'CV. MUL JAYA', 'Jl. Nangka Semarang', '(024) 123123', 'muljaya@gimail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(58, 'maju mundur', 'sendangguwo', '(024) 7666666', 'mudasi@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(59, 'maju mundur', 'sendangguwo', '(024) 7666666', 'mudasi@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(60, 'pt. mudasir', 'Jl. madukoro', '(024) 5777156', 'zarkonibm2@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(61, 'pt jj', 'jl. gug', '(098) 456123', 'botak@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(62, 'PT. MANGGA', 'JL. MANGGA', '(024) 8441254', 'MANGGA@GMAIL.COM', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(63, 'pt. mudasir', 'Jl. madukoro', '(024) 5777156', 'zarkonibm2@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(64, 'pt noko', 'jl.nongko no 5 ', '(024) 8665678', 'nongko67@gmail.co', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(65, 'nongko', 'jalan nongko', '(024) 7898', 'nongko@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(66, 'CV. Kesemek', 'jl buah no 2', '(024) 6494396', 'kesemek@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(67, 'PT SUKA SUKA', 'jl mendut utara 4', '(024) 7625767', 'niriswati@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(68, 'PT NONGGO', 'JL NANKA NO.5', '(021) 123123', 'NAGKA@yAHOO.COM', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(69, 'PT. DUREN MONTONG', 'Jl. Manis pait III', '(081) 0810817', 'duren@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(70, 'PT HAWUG2', 'JL CYIIIN', '(024) 55667', 'cyinnnn@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(71, 'CV. FAJAR UTAMA', 'Jl. K.Zaenudin Dalam No. 9 Karangroto RT. 1 R', '', '', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(72, 'CV. MMR', 'MADUKORO', '(0) ', '', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(73, 'PT. FANIDITA', 'JL. TLOGOSARI', '(024) 555555', 'fanidita@yahoo.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(74, 'PT. MUHANDAS', 'JL. PORWOGONDO NO3', '(024) 1111144', 'muhandas@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(75, 'PT. HRV PLANNER CONSULTANT', 'Jl. Ngesrep', '(024) ', 'hrv@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(76, 'CV. YUNUS PRAKASA RAKHMANU', 'Jl. Madokoro Ekonom', '(024) 7643396', 'yunus_rakhmanu@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(77, 'cv,adem ayem', 'jl.madukoro no 78 semarang', '(024) 1978721', 'ayem123@gmail.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(78, 'CV MAJU MULIA', 'Jl. Krajan II No. 8 Salamsari-Boja \r\nkendal', '(024) 7093963', 'cv_majumulia@yahoo.com', 'Pusat', NULL, NULL, NULL, '2019-02-13 08:53:45', '2019-02-13 08:53:45'),
(80, 'c', 'c', '123', 'MaulanaIkhsan', 'Pusat', '', '', '', '2019-02-16 22:37:28', '2019-02-16 22:37:28');

--
-- Triggers `pihak_ketiga`
--
DROP TRIGGER IF EXISTS `pihak_ketiga_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `pihak_ketiga_BEFORE_INSERT` BEFORE INSERT ON `pihak_ketiga` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `pihak_ketiga_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `pihak_ketiga_BEFORE_UPDATE` BEFORE UPDATE ON `pihak_ketiga` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

DROP TABLE IF EXISTS `program`;
CREATE TABLE `program` (
  `id` int(11) NOT NULL,
  `kode_program` varchar(45) DEFAULT NULL,
  `nama_program` varchar(200) DEFAULT NULL,
  `tahun` int(4) DEFAULT NULL,
  `uniq_code` varchar(15) DEFAULT NULL,
  `keterangan` varchar(200) DEFAULT NULL,
  `kode_urusan` varchar(10) DEFAULT NULL,
  `ppkom` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`id`, `kode_program`, `nama_program`, `tahun`, `uniq_code`, `keterangan`, `kode_urusan`, `ppkom`, `created_at`, `updated_at`) VALUES
(1, '1.1.03.1.1.03.01.01', 'PROGRAM PELAYANAN ADMINISTRASI PERKANTORAN', 2019, 'Pardm77r3r180', NULL, 'Utn4jk2blj', NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:48'),
(2, '1.1.03.1.1.03.01.02', 'PROGRAM PENINGKATAN SARANA DAN PRASARANA APARATUR', 2019, 'P179a4rbj7ol6', NULL, 'Utn4jk2blj', NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:52'),
(3, '1.1.03.1.1.03.01.06', 'PROGRAM PENINGKATAN PENGEMBANGAN SISTEM PELAPORAN CAPAIAN KINERJA DAN KEUANGAN', 2019, 'Ptv3tt4xjq0gs', NULL, 'Utn4jk2blj', NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:52'),
(4, '1.1.03.1.1.03.01.21', 'PROGRAM PENGEMBANGAN DAN PENGELOLAAN JARINGAN IRIGASI, RAWA DAN JARINGAN PENGAIRAN LAINNYA', 2019, 'P112d22cb47f9', NULL, 'Utn4jk2blj', NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:52'),
(5, '1.1.03.1.1.03.01.24', 'PROGRAM PENGENDALIAN BANJIR', 2019, 'P1a1s38cf39ca', NULL, 'Utn4jk2blj', NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:52'),
(6, '1.1.03.1.1.03.01.25', 'PROGRAM PENGEMBANGAN WILAYAH STRATEGIS DAN CEPAT TUMBUH', 2019, 'P3298f9g12h31', NULL, 'Utn4jk2blj', NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:52'),
(7, '1.1.03.1.1.03.01.41', 'PROGRAM PEMBANGUNAN DAN PEMELIHARAAN JALAN DAN JEMBATAN', 2019, 'P3gf97cndoj8g', NULL, 'Utn4jk2blj', NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:52'),
(8, '1.1.03.1.1.03.01.38', 'PROGRAM PEMBANGUNAN DAN PEMELIHARAAN SUMBER DAYA AIR', 2019, 'P6sqpmmuvabl0', NULL, 'Utn4jk2blj', NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:52'),
(9, '1.1.03.1.1.03.01.42', 'PROGRAM PERENCANAAN DAN PENGEMBANGAN INFRASTRUKTUR', 2019, 'P1okell495me1', NULL, 'Utn4jk2blj', NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:53'),
(10, '1.1.03.1.1.03.01.39', 'PROGRAM PENGADAAN DAN PENINGKATAN SARANA DAN PRASARANA SDA', 2019, 'P8n3co17c5qc0', NULL, 'Utn4jk2blj', NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:53'),
(11, '1.1.03.1.1.03.01.43', 'PROGRAM REHABILITASI INFRASTRUKTUR WILAYAH', 2019, 'Pb4cye78qssax', NULL, 'Utn4jk2blj', NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:53');

--
-- Triggers `program`
--
DROP TRIGGER IF EXISTS `program_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `program_BEFORE_INSERT` BEFORE INSERT ON `program` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `program_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `program_BEFORE_UPDATE` BEFORE UPDATE ON `program` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `riwayat_anggaran`
--

DROP TABLE IF EXISTS `riwayat_anggaran`;
CREATE TABLE `riwayat_anggaran` (
  `id` int(11) NOT NULL,
  `kode_pekerjaan` varchar(45) DEFAULT NULL,
  `tahap_pekerjaan` enum('Murni','Pergeseran','Perubahan') DEFAULT NULL,
  `anggaran` bigint(45) DEFAULT NULL,
  `sp2d` int(30) DEFAULT NULL,
  `spj` int(30) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `riwayat_anggaran`
--

INSERT INTO `riwayat_anggaran` (`id`, `kode_pekerjaan`, `tahap_pekerjaan`, `anggaran`, `sp2d`, `spj`, `created_at`, `updated_at`) VALUES
(1, 'p35b1b26350c1', 'Murni', 23500000000, NULL, NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:49'),
(2, 'p5iw45fy05esk', 'Murni', 3800000000, NULL, NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:48'),
(3, 'pfzrxma2bpvs4', 'Murni', 2730000000, NULL, NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:48'),
(4, 'p6d7adbad2552', 'Murni', 247448000, NULL, NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:48'),
(5, 'pd6g5f28j9aa1', 'Murni', 200000000, NULL, NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:48'),
(6, 'ppnmhjmwl2wpj', 'Murni', 290000000, NULL, NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:48'),
(7, 'p1nb8fbfjd1ef', 'Murni', 414369000, NULL, NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:48'),
(8, 'p68djb4aai4e1', 'Murni', 650000000, NULL, NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:48'),
(9, 'pd3a7d2b9e31i', 'Murni', 40000000, NULL, NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:48'),
(10, 'p9jjdh7bffhh1', 'Murni', 20000000, NULL, NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:48'),
(11, 'pdb9030b77a24', 'Murni', 5000000000, NULL, NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:48'),
(12, 'p1aki7efhhib1', 'Murni', 12750000000, NULL, NULL, '2019-01-31 13:29:48', '2019-01-31 13:29:48'),
(13, 'p1lvf7xtbfjnf', 'Murni', 5000000, NULL, NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:49'),
(14, 'p130ea1hij5gg', 'Murni', 3007705000, NULL, NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:49'),
(15, 'p1p0ysjufe37j', 'Murni', 3000000000, NULL, NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:49'),
(16, 'p88789ebj4fee', 'Murni', 39000000000, NULL, NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:49'),
(17, 'pbl751ga53fao', 'Murni', 3000000000, NULL, NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:49'),
(18, 'p25l82io17ra8', 'Murni', 14000000000, NULL, NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:49'),
(19, 'p2blshibis3un', 'Murni', 300000000, NULL, NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:49'),
(20, 'p285fpqm21lbq', 'Murni', 5500000000, NULL, NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:49'),
(21, 'p6bj9acafl00k', 'Murni', 2000000000, NULL, NULL, '2019-01-31 13:29:49', '2019-01-31 13:29:49'),
(22, 'p35b1b26350c1', 'Pergeseran', 23500000000, NULL, NULL, '2019-01-31 13:29:51', '2019-01-31 13:29:53'),
(23, 'p5iw45fy05esk', 'Pergeseran', 3800000000, NULL, NULL, '2019-01-31 13:29:51', '2019-01-31 13:29:51'),
(24, 'pfzrxma2bpvs4', 'Pergeseran', 2730000000, NULL, NULL, '2019-01-31 13:29:52', '2019-01-31 13:29:52'),
(25, 'p6d7adbad2552', 'Pergeseran', 247448000, NULL, NULL, '2019-01-31 13:29:52', '2019-01-31 13:29:52'),
(26, 'pd6g5f28j9aa1', 'Pergeseran', 200000000, NULL, NULL, '2019-01-31 13:29:52', '2019-01-31 13:29:52'),
(27, 'ppnmhjmwl2wpj', 'Pergeseran', 290000000, NULL, NULL, '2019-01-31 13:29:52', '2019-01-31 13:29:52'),
(28, 'p1nb8fbfjd1ef', 'Pergeseran', 414369000, NULL, NULL, '2019-01-31 13:29:52', '2019-01-31 13:29:52'),
(29, 'p68djb4aai4e1', 'Pergeseran', 650000000, NULL, NULL, '2019-01-31 13:29:52', '2019-01-31 13:29:52'),
(30, 'pd3a7d2b9e31i', 'Pergeseran', 40000000, NULL, NULL, '2019-01-31 13:29:52', '2019-01-31 13:29:52'),
(31, 'p9jjdh7bffhh1', 'Pergeseran', 20000000, NULL, NULL, '2019-01-31 13:29:52', '2019-01-31 13:29:52'),
(32, 'pdb9030b77a24', 'Pergeseran', 5000000000, NULL, NULL, '2019-01-31 13:29:52', '2019-01-31 13:29:52'),
(33, 'p1aki7efhhib1', 'Pergeseran', 12750000000, NULL, NULL, '2019-01-31 13:29:52', '2019-01-31 13:29:52'),
(34, 'p1lvf7xtbfjnf', 'Pergeseran', 5000000, NULL, NULL, '2019-01-31 13:29:52', '2019-01-31 13:29:52'),
(35, 'p130ea1hij5gg', 'Pergeseran', 3007705000, NULL, NULL, '2019-01-31 13:29:52', '2019-01-31 13:29:52'),
(36, 'p1p0ysjufe37j', 'Pergeseran', 3000000000, NULL, NULL, '2019-01-31 13:29:52', '2019-01-31 13:29:52'),
(37, 'p88789ebj4fee', 'Pergeseran', 39000000000, NULL, NULL, '2019-01-31 13:29:52', '2019-01-31 13:29:52'),
(38, 'pbl751ga53fao', 'Pergeseran', 3000000000, NULL, NULL, '2019-01-31 13:29:52', '2019-01-31 13:29:52'),
(39, 'p25l82io17ra8', 'Pergeseran', 14000000000, NULL, NULL, '2019-01-31 13:29:52', '2019-01-31 13:29:52'),
(40, 'p2blshibis3un', 'Pergeseran', 300000000, NULL, NULL, '2019-01-31 13:29:52', '2019-01-31 13:29:52'),
(41, 'p285fpqm21lbq', 'Pergeseran', 5500000000, NULL, NULL, '2019-01-31 13:29:53', '2019-01-31 13:29:53'),
(42, 'p6bj9acafl00k', 'Pergeseran', 2000000000, NULL, NULL, '2019-01-31 13:29:53', '2019-01-31 13:29:53');

--
-- Triggers `riwayat_anggaran`
--
DROP TRIGGER IF EXISTS `riwayat_anggaran_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `riwayat_anggaran_BEFORE_INSERT` BEFORE INSERT ON `riwayat_anggaran` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `riwayat_anggaran_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `riwayat_anggaran_BEFORE_UPDATE` BEFORE UPDATE ON `riwayat_anggaran` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `riwayat_pekerjaan_pihak_ketiga`
--

DROP TABLE IF EXISTS `riwayat_pekerjaan_pihak_ketiga`;
CREATE TABLE `riwayat_pekerjaan_pihak_ketiga` (
  `id` int(11) NOT NULL,
  `pihak_ketiga_id` int(11) DEFAULT NULL,
  `nama_pekerjaan` varchar(250) NOT NULL,
  `bidang_pekerjaan` varchar(100) NOT NULL,
  `lokasi_kerja` varchar(100) NOT NULL,
  `pemberi_tugas` varchar(100) NOT NULL,
  `no_kontrak` varchar(100) NOT NULL,
  `tgl_kontrak` date NOT NULL,
  `nilai_kontrak` bigint(45) NOT NULL,
  `tgl_progress_terakhir` date NOT NULL,
  `presentase_terakhir` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `riwayat_pekerjaan_pihak_ketiga`
--
DROP TRIGGER IF EXISTS `riwayat_pekerjaan_pihak_ketiga_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `riwayat_pekerjaan_pihak_ketiga_BEFORE_INSERT` BEFORE INSERT ON `riwayat_pekerjaan_pihak_ketiga` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `riwayat_pekerjaan_pihak_ketiga_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `riwayat_pekerjaan_pihak_ketiga_BEFORE_UPDATE` BEFORE UPDATE ON `riwayat_pekerjaan_pihak_ketiga` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `riwayat_pendidikan _karyawan`
--

DROP TABLE IF EXISTS `riwayat_pendidikan _karyawan`;
CREATE TABLE `riwayat_pendidikan _karyawan` (
  `id` int(11) NOT NULL,
  `karyawan_id` int(11) DEFAULT NULL,
  `jenjang_pendidikan_id` int(11) DEFAULT NULL,
  `file` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `riwayat_pendidikan _karyawan`
--
DROP TRIGGER IF EXISTS `riwayat_pendidikan_karyawan_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `riwayat_pendidikan_karyawan_BEFORE_INSERT` BEFORE INSERT ON `riwayat_pendidikan _karyawan` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `riwayat_pendidikan_karyawan_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `riwayat_pendidikan_karyawan_BEFORE_UPDATE` BEFORE UPDATE ON `riwayat_pendidikan _karyawan` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(5, 'Admin', '2019-01-24 18:44:00', '2019-01-27 13:27:57'),
(6, 'Super Admin', '2019-01-24 18:44:07', '2019-01-28 09:28:46'),
(7, 'Staff', '2019-01-28 10:46:21', '2019-01-28 10:46:21');

--
-- Triggers `role`
--
DROP TRIGGER IF EXISTS `role_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `role_BEFORE_INSERT` BEFORE INSERT ON `role` FOR EACH ROW BEGIN
	set new.created_at = (select now());
    set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `role_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `role_BEFORE_UPDATE` BEFORE UPDATE ON `role` FOR EACH ROW BEGIN
    set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `seri_pejabat_pengadaan`
--

DROP TABLE IF EXISTS `seri_pejabat_pengadaan`;
CREATE TABLE `seri_pejabat_pengadaan` (
  `id` int(11) NOT NULL,
  `nama` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seri_pejabat_pengadaan`
--

INSERT INTO `seri_pejabat_pengadaan` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'I', '2019-01-29 15:32:49', '2019-01-29 15:32:49'),
(2, 'II', '2019-01-29 15:32:53', '2019-01-29 15:32:53'),
(3, 'III', '2019-01-29 15:32:58', '2019-01-29 15:32:58'),
(4, 'IV', '2019-01-29 15:33:02', '2019-01-29 15:33:02'),
(5, 'V', '2019-01-29 15:33:06', '2019-01-29 15:33:06'),
(6, 'VI', '2019-01-29 15:33:12', '2019-01-29 15:33:12'),
(7, 'VII', '2019-01-29 15:33:16', '2019-01-29 15:33:16'),
(8, 'VIII', '2019-01-29 15:33:21', '2019-01-29 15:33:21'),
(9, 'IX', '2019-01-29 15:33:29', '2019-01-29 15:33:29'),
(10, 'X', '2019-01-29 15:33:46', '2019-01-29 15:33:46');

--
-- Triggers `seri_pejabat_pengadaan`
--
DROP TRIGGER IF EXISTS `seri_pejabat_pengadaan_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `seri_pejabat_pengadaan_BEFORE_INSERT` BEFORE INSERT ON `seri_pejabat_pengadaan` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `seri_pejabat_pengadaan_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `seri_pejabat_pengadaan_BEFORE_UPDATE` BEFORE UPDATE ON `seri_pejabat_pengadaan` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `skpd`
--

DROP TABLE IF EXISTS `skpd`;
CREATE TABLE `skpd` (
  `id` int(11) NOT NULL,
  `kode_skpd` varchar(45) DEFAULT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skpd`
--

INSERT INTO `skpd` (`id`, `kode_skpd`, `nama`, `created_at`, `updated_at`) VALUES
(1, '1.1.03.01', 'DINAS PEKERJAAN UMUM', '2018-07-09 08:14:05', '2018-07-09 08:14:05');

--
-- Triggers `skpd`
--
DROP TRIGGER IF EXISTS `skpd_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `skpd_BEFORE_INSERT` BEFORE INSERT ON `skpd` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `skpd_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `skpd_BEFORE_UPDATE` BEFORE UPDATE ON `skpd` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `surat_template`
--

DROP TABLE IF EXISTS `surat_template`;
CREATE TABLE `surat_template` (
  `id` int(11) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `file` varchar(150) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `surat_template`
--

INSERT INTO `surat_template` (`id`, `nama`, `file`, `created_at`, `updated_at`) VALUES
(1, 'Surat Pengumuman Penawaran', 'ea560-cara-mudah-closing-di-instagram.pdf', '2019-02-28 10:30:33', '2019-02-28 10:30:33');

--
-- Triggers `surat_template`
--
DROP TRIGGER IF EXISTS `surat_template_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `surat_template_BEFORE_INSERT` BEFORE INSERT ON `surat_template` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `surat_template_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `surat_template_BEFORE_UPDATE` BEFORE UPDATE ON `surat_template` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `unit_kerja`
--

DROP TABLE IF EXISTS `unit_kerja`;
CREATE TABLE `unit_kerja` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit_kerja`
--

INSERT INTO `unit_kerja` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'SEKRETARIAT', '2018-07-24 13:22:30', '2018-07-24 13:22:30'),
(2, 'SUB BAGIAN PERENCANAAN EVALUASI DAN KEUANGAN', '2018-07-24 13:22:30', '2018-07-24 13:22:30'),
(3, 'SUB BAGIAN UMUM DAN KEPEGAWAIAN', '2018-07-24 13:22:30', '2018-07-24 13:22:30'),
(4, 'BIDANG REKAYASA TEKNIS', '2018-07-24 13:22:30', '2018-07-24 13:22:30'),
(5, 'SEKSIE SURVEY DAN PENGUKURAN', '2018-07-24 13:22:30', '2018-07-24 13:22:30'),
(6, 'SEKSIE PERANCANGAN TEKNIS', '2018-07-24 13:22:30', '2018-07-24 13:22:30'),
(7, 'SEKSIE PENGEMBANGAN TEKNOLOGI', '2018-07-24 13:22:30', '2018-07-24 13:22:30'),
(8, 'BIDANG BINAMARGA', '2018-07-24 13:22:30', '2019-01-28 09:45:17'),
(9, 'SEKSIE PEMBANGUNAN PENGEMBANGAN JALAN DAN JEMBATAN', '2018-07-24 13:22:30', '2018-11-29 09:41:45'),
(10, 'SEKSIE PRESERVASI JALAN DAN JEMBATAN', '2018-07-24 13:22:30', '2018-07-24 13:22:30'),
(11, 'SEKSIE PENDAYAGUNAAN RUANG MILIK JALAN', '2018-07-24 13:22:30', '2018-07-24 13:22:30'),
(12, 'BIDANG SUMBERDAYA AIR DAN DRAINASE', '2018-07-24 13:22:30', '2018-07-24 13:22:30'),
(13, 'SEKSIE PENGELOLAAN DAN PENGEMBANGAN DRAINASE', '2018-07-24 13:22:30', '2018-07-24 13:22:30'),
(14, 'SEKSIE PENGELOLAAN SUNGAI, IRIGASI DAN PANTAI', '2018-07-24 13:22:30', '2018-07-24 13:22:30'),
(15, 'SEKSIE PENGENDALIAAN DAN PEMANFAATAN KONSERVASI', '2018-07-24 13:22:30', '2018-11-29 09:43:24'),
(16, 'UPTD PERALATAN DAN PERBEKALAN BINAMARGA', '2018-07-24 13:22:30', '2018-07-24 13:22:30'),
(17, 'UPTD PERALATAN DAN PERBEKALAN SUMBER DAYA AIR', '2018-07-24 13:22:30', '2018-07-24 13:22:30'),
(18, 'UPTD LABORATORIUM DAN PENGUJIAN', '2018-07-24 13:22:30', '2018-07-24 13:22:30'),
(32, 'KEPALA DINAS', '2018-07-24 15:03:15', '2018-07-24 15:03:15'),
(33, 'UPT DPU WILAYAH TIMUR', '2018-08-03 10:17:00', '2018-08-03 10:17:00'),
(34, 'UPT DPU WILAYAH SELATAN', '2018-08-03 10:17:24', '2018-08-03 10:17:24'),
(35, 'UPT DPU WILAYAH UTARA', '2018-08-03 10:17:37', '2018-08-03 10:17:37'),
(36, 'UPT DPU WILAYAH BARAT', '2018-08-03 10:17:57', '2018-08-03 10:17:57'),
(38, 'UPTD POMPA BANJIR', '2018-08-06 21:10:23', '2018-08-06 21:11:08'),
(39, 'UPTD Wilayah Tengah II', '2019-01-05 09:27:48', '2019-01-05 09:27:48');

--
-- Triggers `unit_kerja`
--
DROP TRIGGER IF EXISTS `unit_kerja_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `unit_kerja_BEFORE_INSERT` BEFORE INSERT ON `unit_kerja` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `unit_kerja_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `unit_kerja_BEFORE_UPDATE` BEFORE UPDATE ON `unit_kerja` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `urusan`
--

DROP TABLE IF EXISTS `urusan`;
CREATE TABLE `urusan` (
  `id` int(11) NOT NULL,
  `kode_urusan` varchar(45) DEFAULT NULL,
  `nama_urusan` varchar(200) DEFAULT NULL,
  `tahun` int(4) DEFAULT NULL,
  `uniq_code` varchar(15) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `urusan`
--

INSERT INTO `urusan` (`id`, `kode_urusan`, `nama_urusan`, `tahun`, `uniq_code`, `created_at`, `updated_at`) VALUES
(1, '1.1.03', 'URUSAN WAJIB PELAYANAN DASAR PEKERJAAN UMUM DAN PENATAAN RUANG', 2019, 'Utn4jk2bljj40', '2019-01-31 13:29:48', '2019-01-31 13:29:48');

--
-- Triggers `urusan`
--
DROP TRIGGER IF EXISTS `urusan_BEFORE_INSERT`;
DELIMITER $$
CREATE TRIGGER `urusan_BEFORE_INSERT` BEFORE INSERT ON `urusan` FOR EACH ROW BEGIN
	set new.created_at = (select now());
	set new.updated_at = (select now());
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `urusan_BEFORE_UPDATE`;
DELIMITER $$
CREATE TRIGGER `urusan_BEFORE_UPDATE` BEFORE UPDATE ON `urusan` FOR EACH ROW BEGIN
	set new.updated_at = (select now());
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_data_pegawai`
-- (See below for the actual view)
--
DROP VIEW IF EXISTS `view_data_pegawai`;
CREATE TABLE `view_data_pegawai` (
`pegawai_id` int(11)
,`pegawai_nama_lengkap` varchar(100)
,`unit_kerja` varchar(50)
,`is_pegawai` enum('y','n')
,`role` varchar(20)
);

-- --------------------------------------------------------

--
-- Structure for view `view_data_pegawai`
--
DROP TABLE IF EXISTS `view_data_pegawai`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_data_pegawai`  AS  select `pegawai`.`id` AS `pegawai_id`,`pegawai`.`nama_lengkap` AS `pegawai_nama_lengkap`,`unit_kerja`.`nama` AS `unit_kerja`,`pegawai`.`is_pegawai` AS `is_pegawai`,`role`.`nama` AS `role` from ((`pegawai` left join `unit_kerja` on((`pegawai`.`unit_id` = `unit_kerja`.`id`))) left join `role` on((`pegawai`.`role_id` = `role`.`id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aktivitas`
--
ALTER TABLE `aktivitas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pptk` (`pptk`),
  ADD KEY `ppkom` (`ppkom`),
  ADD KEY `bendahara` (`bendahara`),
  ADD KEY `pekerjaan_id` (`pekerjaan_id`),
  ADD KEY `pengadaan_id` (`pengadaan_id`),
  ADD KEY `jenis_pekerjaan_id` (`jenis_pekerjaan_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `api`
--
ALTER TABLE `api`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dokumen_pihak_ketiga`
--
ALTER TABLE `dokumen_pihak_ketiga`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pihak_ketiga_id` (`pihak_ketiga_id`),
  ADD KEY `jenis_dokumen_id` (`jenis_dokumen_id`);

--
-- Indexes for table `dokumen_template`
--
ALTER TABLE `dokumen_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jabatan_perusahaan`
--
ALTER TABLE `jabatan_perusahaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_dokumen_pihak_ketiga`
--
ALTER TABLE `jenis_dokumen_pihak_ketiga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_pejabat_pengadaan`
--
ALTER TABLE `jenis_pejabat_pengadaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_pekerjaan`
--
ALTER TABLE `jenis_pekerjaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_pengadaan`
--
ALTER TABLE `jenis_pengadaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenjang_pendidikan`
--
ALTER TABLE `jenjang_pendidikan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jabatan_id` (`jabatan_id`),
  ADD KEY `jenjang_pendidikan_id` (`jenjang_pendidikan_id`),
  ADD KEY `pihak_ketiga_id` (`pihak_ketiga_id`);

--
-- Indexes for table `kontrak_dokumen_lainnya`
--
ALTER TABLE `kontrak_dokumen_lainnya`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kontrak_pekerjaan_id` (`kontrak_pekerjaan_id`);

--
-- Indexes for table `kontrak_dokumen_pihak_ketiga`
--
ALTER TABLE `kontrak_dokumen_pihak_ketiga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kontrak_karyawan_pihak_ketiga`
--
ALTER TABLE `kontrak_karyawan_pihak_ketiga`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kontrak_pihak_ketiga_id` (`kontrak_pihak_ketiga_id`),
  ADD KEY `karyawan_id` (`karyawan_id`);

--
-- Indexes for table `kontrak_komentar`
--
ALTER TABLE `kontrak_komentar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kontrak_pekerjaan_id` (`kontrak_pekerjaan_id`),
  ADD KEY `pegawai_id` (`pegawai_id`),
  ADD KEY `karyawan_id` (`karyawan_id`);

--
-- Indexes for table `kontrak_pekerjaan`
--
ALTER TABLE `kontrak_pekerjaan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `aktivitas_id` (`aktivitas_id`),
  ADD KEY `pejabat_pembuat_komitmen_id` (`pejabat_pembuat_komitmen_id`),
  ADD KEY `pejabat_pengadaan_id` (`pejabat_pengadaan_id`);

--
-- Indexes for table `kontrak_petugas_pihak_ketiga`
--
ALTER TABLE `kontrak_petugas_pihak_ketiga`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kontrak_pihak_ketiga_id` (`kontrak_pihak_ketiga_id`),
  ADD KEY `karyawan_id` (`karyawan_id`);

--
-- Indexes for table `kontrak_pihak_ketiga`
--
ALTER TABLE `kontrak_pihak_ketiga`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kontrak_pekerjaan_id` (`kontrak_pekerjaan_id`),
  ADD KEY `pihak_ketiga_id` (`pihak_ketiga_id`);

--
-- Indexes for table `kontrak_surat_penawaran`
--
ALTER TABLE `kontrak_surat_penawaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kontrak_pekerjaan_id` (`kontrak_pekerjaan_id`),
  ADD KEY `template_surat_id` (`template_surat_id`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `unit_id` (`unit_id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `pejabat_pengadaan`
--
ALTER TABLE `pejabat_pengadaan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pegawai_id` (`pegawai_id`),
  ADD KEY `jenis_pengadaann_id` (`jenis_pejabat_pengadaan_id`),
  ADD KEY `tingkat_id` (`tingkat_id`);

--
-- Indexes for table `pekerjaan`
--
ALTER TABLE `pekerjaan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode_pekerjaan` (`kode_pekerjaan`),
  ADD KEY `kode_program` (`kode_program`),
  ADD KEY `kode_program_2` (`kode_program`),
  ADD KEY `uniq_code` (`uniq_code`);

--
-- Indexes for table `pihak_ketiga`
--
ALTER TABLE `pihak_ketiga`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_program_urusan_id_idx` (`kode_urusan`),
  ADD KEY `kode_program` (`kode_program`),
  ADD KEY `ppkom` (`ppkom`),
  ADD KEY `uniq_code` (`uniq_code`);

--
-- Indexes for table `riwayat_anggaran`
--
ALTER TABLE `riwayat_anggaran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode_pekerjaan` (`kode_pekerjaan`);

--
-- Indexes for table `riwayat_pekerjaan_pihak_ketiga`
--
ALTER TABLE `riwayat_pekerjaan_pihak_ketiga`
  ADD KEY `riwayat_pekerjaan_pihak_ketiga_pihak_ketiga_id` (`pihak_ketiga_id`);

--
-- Indexes for table `riwayat_pendidikan _karyawan`
--
ALTER TABLE `riwayat_pendidikan _karyawan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `karyawan_id` (`karyawan_id`),
  ADD KEY `jenjang_pendidikan_id` (`jenjang_pendidikan_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seri_pejabat_pengadaan`
--
ALTER TABLE `seri_pejabat_pengadaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skpd`
--
ALTER TABLE `skpd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surat_template`
--
ALTER TABLE `surat_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit_kerja`
--
ALTER TABLE `unit_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `urusan`
--
ALTER TABLE `urusan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode_urusan` (`kode_urusan`),
  ADD KEY `uniq_code` (`uniq_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aktivitas`
--
ALTER TABLE `aktivitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `api`
--
ALTER TABLE `api`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `dokumen_pihak_ketiga`
--
ALTER TABLE `dokumen_pihak_ketiga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `dokumen_template`
--
ALTER TABLE `dokumen_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `jabatan_perusahaan`
--
ALTER TABLE `jabatan_perusahaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `jenis_dokumen_pihak_ketiga`
--
ALTER TABLE `jenis_dokumen_pihak_ketiga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `jenis_pejabat_pengadaan`
--
ALTER TABLE `jenis_pejabat_pengadaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `jenis_pekerjaan`
--
ALTER TABLE `jenis_pekerjaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `jenis_pengadaan`
--
ALTER TABLE `jenis_pengadaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `jenjang_pendidikan`
--
ALTER TABLE `jenjang_pendidikan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `karyawan`
--
ALTER TABLE `karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `kontrak_dokumen_lainnya`
--
ALTER TABLE `kontrak_dokumen_lainnya`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kontrak_dokumen_pihak_ketiga`
--
ALTER TABLE `kontrak_dokumen_pihak_ketiga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kontrak_karyawan_pihak_ketiga`
--
ALTER TABLE `kontrak_karyawan_pihak_ketiga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kontrak_komentar`
--
ALTER TABLE `kontrak_komentar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kontrak_pekerjaan`
--
ALTER TABLE `kontrak_pekerjaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `kontrak_petugas_pihak_ketiga`
--
ALTER TABLE `kontrak_petugas_pihak_ketiga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kontrak_pihak_ketiga`
--
ALTER TABLE `kontrak_pihak_ketiga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kontrak_surat_penawaran`
--
ALTER TABLE `kontrak_surat_penawaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `pejabat_pengadaan`
--
ALTER TABLE `pejabat_pengadaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pekerjaan`
--
ALTER TABLE `pekerjaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `pihak_ketiga`
--
ALTER TABLE `pihak_ketiga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `riwayat_anggaran`
--
ALTER TABLE `riwayat_anggaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `riwayat_pendidikan _karyawan`
--
ALTER TABLE `riwayat_pendidikan _karyawan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `seri_pejabat_pengadaan`
--
ALTER TABLE `seri_pejabat_pengadaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `skpd`
--
ALTER TABLE `skpd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `surat_template`
--
ALTER TABLE `surat_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `unit_kerja`
--
ALTER TABLE `unit_kerja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `urusan`
--
ALTER TABLE `urusan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `aktivitas`
--
ALTER TABLE `aktivitas`
  ADD CONSTRAINT `fk_aktivitas_bendahara` FOREIGN KEY (`bendahara`) REFERENCES `pegawai` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_aktivitas_created_by` FOREIGN KEY (`created_by`) REFERENCES `pegawai` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_aktivitas_jenis_pekerjaan_id` FOREIGN KEY (`jenis_pekerjaan_id`) REFERENCES `jenis_pekerjaan` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_aktivitas_pekerjaan_id` FOREIGN KEY (`pekerjaan_id`) REFERENCES `pekerjaan` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_aktivitas_pengadaan_id` FOREIGN KEY (`pengadaan_id`) REFERENCES `jenis_pengadaan` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_aktivitas_ppkom` FOREIGN KEY (`ppkom`) REFERENCES `pegawai` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_aktivitas_pptk` FOREIGN KEY (`pptk`) REFERENCES `pegawai` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_aktivitas_updated_by` FOREIGN KEY (`updated_by`) REFERENCES `pegawai` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `dokumen_pihak_ketiga`
--
ALTER TABLE `dokumen_pihak_ketiga`
  ADD CONSTRAINT `fk_dokumen_pihak_ketiga_jenis_dokumen_id` FOREIGN KEY (`jenis_dokumen_id`) REFERENCES `jenis_dokumen_pihak_ketiga` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_dokumen_pihak_ketiga_pihak_ketiga_id` FOREIGN KEY (`pihak_ketiga_id`) REFERENCES `pihak_ketiga` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD CONSTRAINT `fk_karyawan_jabatan_id` FOREIGN KEY (`jabatan_id`) REFERENCES `jabatan_perusahaan` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_karyawan_jenjang_pendidikan_id` FOREIGN KEY (`jenjang_pendidikan_id`) REFERENCES `jenjang_pendidikan` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_karyawan_pihak_ketiga_id` FOREIGN KEY (`pihak_ketiga_id`) REFERENCES `pihak_ketiga` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `kontrak_dokumen_lainnya`
--
ALTER TABLE `kontrak_dokumen_lainnya`
  ADD CONSTRAINT `fk_kontrak_dokumen_lainnya_kontrak_pekerjaan_id` FOREIGN KEY (`kontrak_pekerjaan_id`) REFERENCES `kontrak_pekerjaan` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `kontrak_karyawan_pihak_ketiga`
--
ALTER TABLE `kontrak_karyawan_pihak_ketiga`
  ADD CONSTRAINT `fk_kontrak_karyawan_pihak_ketiga_karyawan_id` FOREIGN KEY (`karyawan_id`) REFERENCES `karyawan` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kontrak_karyawan_pihak_ketiga_kontrak_pihak_ketiga_id` FOREIGN KEY (`kontrak_pihak_ketiga_id`) REFERENCES `kontrak_pihak_ketiga` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `kontrak_komentar`
--
ALTER TABLE `kontrak_komentar`
  ADD CONSTRAINT `kontrak_komentar_karyawan_id` FOREIGN KEY (`karyawan_id`) REFERENCES `karyawan` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `kontrak_komentar_kontrak_pekerjaan_id` FOREIGN KEY (`kontrak_pekerjaan_id`) REFERENCES `kontrak_pekerjaan` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `kontrak_komentar_pegawai_id` FOREIGN KEY (`pegawai_id`) REFERENCES `pegawai` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `kontrak_pekerjaan`
--
ALTER TABLE `kontrak_pekerjaan`
  ADD CONSTRAINT `fk_kontrak_pekerjaan_aktivitas_id` FOREIGN KEY (`aktivitas_id`) REFERENCES `aktivitas` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kontrak_pekerjaan_pejabat_pembuat_komitmen_id` FOREIGN KEY (`pejabat_pembuat_komitmen_id`) REFERENCES `pegawai` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kontrak_pekerjaan_pejabat_pengadaan_id` FOREIGN KEY (`pejabat_pengadaan_id`) REFERENCES `pejabat_pengadaan` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `kontrak_petugas_pihak_ketiga`
--
ALTER TABLE `kontrak_petugas_pihak_ketiga`
  ADD CONSTRAINT `fk_kontrak_petugas_pihak_ketiga_karyawan_id` FOREIGN KEY (`kontrak_pihak_ketiga_id`) REFERENCES `karyawan` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kontrak_petugas_pihak_ketiga_kontrak_pihak_ketiga_id` FOREIGN KEY (`kontrak_pihak_ketiga_id`) REFERENCES `kontrak_pihak_ketiga` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `kontrak_pihak_ketiga`
--
ALTER TABLE `kontrak_pihak_ketiga`
  ADD CONSTRAINT `fk_kontrak_pihak_ketiga_id` FOREIGN KEY (`pihak_ketiga_id`) REFERENCES `pihak_ketiga` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kontrak_pihak_ketiga_kontrak_pekerjaan_id` FOREIGN KEY (`kontrak_pekerjaan_id`) REFERENCES `kontrak_pekerjaan` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `kontrak_surat_penawaran`
--
ALTER TABLE `kontrak_surat_penawaran`
  ADD CONSTRAINT `fk_kontrak_surat_penawaran_kontrak_pekerjaan_id` FOREIGN KEY (`kontrak_pekerjaan_id`) REFERENCES `kontrak_pekerjaan` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kontrak_surat_penawaran_template_surat_id` FOREIGN KEY (`template_surat_id`) REFERENCES `surat_template` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD CONSTRAINT `fk_pegawai_role_id` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pegawai_unit_id` FOREIGN KEY (`unit_id`) REFERENCES `unit_kerja` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `pejabat_pengadaan`
--
ALTER TABLE `pejabat_pengadaan`
  ADD CONSTRAINT `fk_pejabat_pengadaan_jenis_pejabat_pengadaan_id` FOREIGN KEY (`jenis_pejabat_pengadaan_id`) REFERENCES `jenis_pejabat_pengadaan` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pejabat_pengadaan_pegawai_id` FOREIGN KEY (`pegawai_id`) REFERENCES `pegawai` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pejabat_pengadaan_tingkat_id` FOREIGN KEY (`tingkat_id`) REFERENCES `seri_pejabat_pengadaan` (`id`);

--
-- Constraints for table `pekerjaan`
--
ALTER TABLE `pekerjaan`
  ADD CONSTRAINT `fk_pekerjaan_kode_program` FOREIGN KEY (`kode_program`) REFERENCES `program` (`uniq_code`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `program`
--
ALTER TABLE `program`
  ADD CONSTRAINT `fk_program_kode_urusan` FOREIGN KEY (`kode_urusan`) REFERENCES `urusan` (`uniq_code`),
  ADD CONSTRAINT `fk_program_ppkom` FOREIGN KEY (`ppkom`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `riwayat_anggaran`
--
ALTER TABLE `riwayat_anggaran`
  ADD CONSTRAINT `fk_riwayat_anggaran_kode_pekerjaan` FOREIGN KEY (`kode_pekerjaan`) REFERENCES `pekerjaan` (`uniq_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `riwayat_pekerjaan_pihak_ketiga`
--
ALTER TABLE `riwayat_pekerjaan_pihak_ketiga`
  ADD CONSTRAINT `riwayat_pekerjaan_pihak_ketiga_pihak_ketiga_id` FOREIGN KEY (`pihak_ketiga_id`) REFERENCES `pihak_ketiga` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `riwayat_pendidikan _karyawan`
--
ALTER TABLE `riwayat_pendidikan _karyawan`
  ADD CONSTRAINT `riwayat_pendidikan_karyawan_jenjang_pendidikan_id` FOREIGN KEY (`jenjang_pendidikan_id`) REFERENCES `jenjang_pendidikan` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `riwayat_pendidikan_karyawan_karyawan_id` FOREIGN KEY (`karyawan_id`) REFERENCES `karyawan` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
