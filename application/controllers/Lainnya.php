<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Lainnya extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$sess_data = $this->session->userdata('session_data');
        if($sess_data===NULL) {
            $this->session->set_flashdata('warning', '<div class="alert alert-warning fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a> 
                Harap login terlebih dahulu untuk mengakses sistem</div>');
			redirect('auth');
        }

		$this->load->library('grocery_CRUD');
	}
	
    public function role()
	{
		// ----- role -----
        // id
        // nama
        // created_at
		// updated_at
		
		$title = 'Role Pegawai';
		$table = 'role';

		$crud = new grocery_CRUD();

		$crud->set_table($table);
		$crud->columns('nama');

		$crud->set_subject($title);
		$crud->display_as('nama', 'Role');
		
        // Set columns that show, edit and insert operation
        $crud->fields('nama');
        
		// Disable button clone
		$crud->unset_clone();

		// Required field
		$crud->required_fields('nama');
		// Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
	}

	public function unit_kerja()
	{
		// ----- unit_kerja -----
        // id
        // nama
        // created_at
		// updated_at

		$title = 'Unit Kerja';
		$table = 'unit_kerja';

		$crud = new grocery_CRUD();

		$crud->set_table($table);
		$crud->columns('nama');

		$crud->set_subject($title);
		$crud->display_as('nama', 'Unit Kerja');
		
        // Set columns that show, edit and insert operation
        $crud->fields('nama');
        
		// Disable button clone
		$crud->unset_clone();

		// Required field
		$crud->required_fields('nama');
		// Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
	}
	
	public function jabatan_perusahaan()
	{
		// ----- jabatan_perusahaan -----
        // id
        // nama
        // created_at
		// updated_at

		$title = 'Jabatan Perusahaan';
		$table = 'jabatan_perusahaan';

		$crud = new grocery_CRUD();

		$crud->set_table($table);
		$crud->columns('nama');

		$crud->set_subject($title);
		$crud->display_as('nama', 'Jabatan');
		
        // Set columns that show, edit and insert operation
        $crud->fields('nama');
        
		// Disable button clone
		$crud->unset_clone();

		// Required field
		$crud->required_fields('nama');
		// Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
	}

	public function jenjang_pendidikan()
	{
		// ----- jenjang_pendidikan -----
        // id
        // nama
        // created_at
		// updated_at

		$title = 'Jenjang Pendidikan';
		$table = 'jenjang_pendidikan';

		$crud = new grocery_CRUD();

		$crud->set_table($table);
		$crud->columns('nama');

		$crud->set_subject($title);
		$crud->display_as('nama', 'Nama');
		
        // Set columns that show, edit and insert operation
        $crud->fields('nama');

		// Disable button clone
		$crud->unset_clone();

		// Required field
		$crud->required_fields('nama');
		// Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);		
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
	}
	
	public function jenis_pekerjaan()
	{
		// ----- jenis_pekerjaan -----
        // id
		// nama
		// nickname
        // created_at
		// updated_at

		$title = 'Jenis Pekerjaan';
		$table = 'jenis_pekerjaan';

		$crud = new grocery_CRUD();

		$crud->set_table($table);
		$crud->columns('nama', 'nickname');

		$crud->set_subject($title);
		$crud->display_as('nama', 'Jenis');
		$crud->display_as('nickname', 'Singkatan');
		
        // Set columns that show, edit and insert operation
		$crud->fields('nama', 'nickname');

		// Disable button clone
		$crud->unset_clone();

		// Required field
		$crud->required_fields('nama', 'nickname');

		// Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);		
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
	}

	public function jenis_pengadaan()
	{
		// ----- jenis_pengadaan -----
        // id
		// nama
		// nickname
        // created_at
		// updated_at

		$title = 'Metode Pengadaan';
		$table = 'jenis_pengadaan';

		$crud = new grocery_CRUD();

		$crud->set_table($table);
		$crud->columns('nama', 'nickname');

		$crud->set_subject($title);
		$crud->display_as('nama', 'Jenis');
		$crud->display_as('nickname', 'Singkatan');
		
        // Set columns that show, edit and insert operation
		$crud->fields('nama', 'nickname');

		// Disable button clone
		$crud->unset_clone();

		// Required field
		$crud->required_fields('nama', 'nickname');

		// Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);		
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
	}

	public function list_api()
	{
		// ----- api -----
        // id
		// nama
		// url
        // created_at
		// updated_at

		$title = 'Daftar API';
		$table = 'api';

		$crud = new grocery_CRUD();

		$crud->set_table($table);
		$crud->columns('nama', 'url');

		$crud->set_subject($title);
		$crud->display_as('nama', 'Deskripsi');
		$crud->display_as('url', 'Link');
		
        // Set columns that show, edit and insert operation
		$crud->fields('nama', 'url');

		// Disable button clone
		$crud->unset_clone();

		// Required field
		$crud->required_fields('nama', 'url');

		// Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);		
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
	}

	public function list_skpd()
	{
		// ----- skpd -----
        // id
		// kode_skpd
		// nama
        // created_at
		// updated_at

		$title = 'Daftar SKPD';
		$table = 'skpd';

		$crud = new grocery_CRUD();

		$crud->set_table($table);
		$crud->columns('kode_skpd', 'nama');

		$crud->set_subject($title);
		$crud->display_as('kode_skpd', 'Kode SKPD');
		$crud->display_as('nama', 'Nama SKPD');
		
        // Set columns that show, edit and insert operation
		$crud->fields('kode_skpd', 'nama');

		// Disable button clone
		$crud->unset_clone();

		// Required field
		$crud->required_fields('kode_skpd', 'nama');

		// Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);		
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
	}

	public function jenis_pejabat_pengadaan()
	{
		// ----- jenis_pejabat_pengadaan -----
        // id
		// nama
        // created_at
		// updated_at

		$title = 'Jenis Pejabat Pengadaan';
		$table = 'jenis_pejabat_pengadaan';

		$crud = new grocery_CRUD();

		$crud->set_table($table);
		$crud->columns('nama');

		$crud->set_subject($title);
		$crud->display_as('nama', 'Jenis');
		
        // Set columns that show, edit and insert operation
		$crud->fields('nama');

		// Disable button clone
		$crud->unset_clone();

		// Required field
		$crud->required_fields('nama');

		// Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);		
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
	}

	public function seri_pejabat_pengadaan()
	{
		// ----- seri_pejabat_pengadaan -----
        // id
		// nama
        // created_at
		// updated_at

		$title = 'Urutan Pejabat Pengadaan';
		$table = 'seri_pejabat_pengadaan';

		$crud = new grocery_CRUD();

		$crud->set_table($table);
		$crud->columns('nama');

		$crud->set_subject($title);
		$crud->display_as('nama', 'Urutan');
		
        // Set columns that show, edit and insert operation
		$crud->fields('nama');

		// Disable button clone
		$crud->unset_clone();

		// Required field
		$crud->required_fields('nama');

		// Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);		
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
	}

	public function jenis_dokumen()
	{
		// ----- jenis_dokumen_pihak_ketiga -----
        // id
		// nama
		// is_series
        // created_at
		// updated_at
		
		$title = 'Jenis Dokumen Pihak Ketiga';
		$table = 'jenis_dokumen_pihak_ketiga';

		$crud = new grocery_CRUD();

		$crud->set_table($table);
		$crud->columns('nama', 'is_series');

		$crud->set_subject($title);
		$crud->display_as('nama', 'Nama Dokumen');
		$crud->display_as('is_series', 'Series ?');

        // Set columns that show, edit and insert operation
		$crud->fields('nama', 'is_series');

		// Disable button clone
		$crud->unset_clone();

		$series_value = array('y'=>'Series', 'n'=>'Tidak Series');
		// Field type beside textbox
        $crud->field_type('is_series', 'dropdown', $series_value);

		// Required field
		$crud->required_fields('nama', 'is_series');

		// Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);		
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
	}

	public function dokumen_template()
	{
		// ----- dokumen_template -----
        // id
		// nama
		// file
        // created_at
		// updated_at

		$title = 'Dokumen Template';
		$table = 'dokumen_template';

		$crud = new grocery_CRUD();

		$crud->set_table($table);
		$crud->columns('nama', 'file');

		$crud->set_subject($title);
		$crud->display_as('nama', 'Nama Dokumen');
		$crud->display_as('file', 'File');

        // Set columns that show, edit and insert operation
		$crud->fields('nama', 'file');

		// Upload file
		$crud->set_field_upload('file', 'assets/uploads/documents_template');

		// Disable button clone
		$crud->unset_clone();

		// Required field
		$crud->required_fields('nama', 'file');

		// Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);		
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
	}

	public function jenis_surat()
	{
		// ----- jenis_surat -----
        // id
		// nama
		// kode
        // created_at
		// updated_at

		$title = 'Jenis Surat / Dokumen Pengadaan';
		$table = 'jenis_surat';

		$crud = new grocery_CRUD();

		$crud->set_table($table);
		$crud->columns('nama', 'kode');

		$crud->set_subject($title);
		$crud->display_as('nama', 'Nama Surat / Dokumen');
		$crud->display_as('kode', 'Kode');

        // Set columns that show, edit and insert operation
		$crud->fields('nama', 'kode');

		// Disable button clone
		$crud->unset_clone();

		// Required field
		$crud->required_fields('nama', 'kode');

		// Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);		
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
	}

	public function bank()
	{
		// ----- bank -----
        // id
		// nama
        // created_at
		// updated_at

		$title = 'Nama Bank';
		$table = 'bank';

		$crud = new grocery_CRUD();

		$crud->set_table($table);
		$crud->columns('nama');

		$crud->set_subject($title);
		$crud->display_as('nama', 'Nama Bank');

        // Set columns that show, edit and insert operation
		$crud->fields('nama');

		// Disable button clone
		$crud->unset_clone();

		// Required field
		$crud->required_fields('nama');

		// Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);		
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
	}

	public function tempat_rapat()
	{
		/*--- tempat_rapat ---*/
		// id
		// nama
		// alamat
		// created_at
		// updated_at

		$title = 'Tempat Rapat';
		$table = 'tempat_rapat';

		$crud = new grocery_CRUD();

		$crud->set_table($table);
		$crud->columns('nama', 'alamat');

		$crud->set_subject($title);
		$crud->display_as('nama', 'Nama Tempat Rapat');
		$crud->display_as('alamat', 'Alamat');

        // Set columns that show, edit and insert operation
		$crud->fields('nama', 'alamat');

		// Disable button clone
		$crud->unset_clone();

		// Required field
		$crud->required_fields('nama', 'alamat');

		// Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);		
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
	}

	public function jenis_kegiatan_pengadaan()
	{
		/*--- jenis_kegiatan_pengadaan ---*/
		// id
		// nama
		// kode
		// created_at
		// updated_at

		$title = 'Jenis Kegiatan Pengadaan';
		$table = 'jenis_kegiatan_pengadaan';

		$crud = new grocery_CRUD();

		$crud->set_table($table);
		$crud->columns('nama', 'kode');

		$crud->set_subject($title);
		$crud->display_as('nama', 'Nama Kegiatan Pengadaan');
		$crud->display_as('kode', 'Kode');

        // Set columns that show, edit and insert operation
		$crud->fields('nama', 'kode');

		// Disable button clone
		$crud->unset_clone();

		// Required field
		$crud->required_fields('nama', 'kode');

		// Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);		
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
	}

	public function jabatan_fungsional()
	{
		/*--- jabatan_fungsional ---*/
		// id
		// nama
		// created_at
		// updated_at

		$title = 'Jabatan Struktural';
		$table = 'jabatan_fungsional';

		$crud = new grocery_CRUD();

		$crud->set_table($table);
		$crud->columns('nama');

		$crud->set_subject($title);
		$crud->display_as('nama', 'Nama Jabatan');

        // Set columns that show, edit and insert operation
		$crud->fields('nama');

		// Disable button clone
		$crud->unset_clone();

		// Required field
		$crud->required_fields('nama');

		// Event autoclose dialog after insert and update
		$crud->set_lang_string('insert_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );
		 $crud->set_lang_string('update_success_message',
				 'Your data has been successfully stored into the database.<br/>Please wait while you are redirecting to the list page.
				 <script type="text/javascript">
				  window.location = "'.site_url(strtolower(__CLASS__).'/'.strtolower(__FUNCTION__)).'";
				 </script>
				 <div style="display:none">
				 '
		   );

		$this->config->set_item('grocery_crud_dialog_forms', true);
		$this->config->set_item('grocery_crud_default_per_page', 25);		
		
		$output = (array)$crud->render();
		$output['title_content'] = $title;

		$this->load->view('data_lainnya/content', $output);
	}
}

/* End of file Lainnya.php */
