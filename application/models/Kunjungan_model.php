<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kunjungan_model extends CI_Model {

	// Column : 
	// id
	// tamu_id
	// kode_kunjungan
	// tanggal
	// waktu
	// keperluan
	// tujuan
	// masukan
	// rating
	var $column_order = array(null, 'kunjungan.id', 'tamu.id', 'tamu.nama', 'tamu.asal', 'kunjungan.kode_kunjungan', 'kunjungan.tanggal', 'kunjungan.waktu', 'kunjungan.keperluan', 'unit_kerja.id', 'unit_kerja.nama', 'kunjungan.masukan', 'kunjungan.rating'); 

	var $column_search = array('kunjungan.id', 'tamu.id', 'tamu.nama', 'tamu.asal', 'kunjungan.kode_kunjungan', 'kunjungan.tanggal', 'kunjungan.waktu', 'kunjungan.keperluan', 'unit_kerja.id', 'unit_kerja.nama', 'kunjungan.masukan', 'kunjungan.rating');

	var $order = array('kunjungan.id' => 'asc'); // default order 

	public function detail_kunjungan($kunjungan_id)
	{
		return $this->db->select("kunjungan.id as id, 
			tamu.id as tamu_id,
			tamu.nama as tamu_nama, 
			tamu.asal as tamu_asal,
			kunjungan.kode_kunjungan as kode_kunjungan, 
			kunjungan.tanggal as tanggal, 
			kunjungan.waktu as waktu, 
			kunjungan.keperluan as keperluan, 
			unit_kerja.id as tujuan,
			unit_kerja.nama as tujuan_nama, 
			kunjungan.masukan as masukan, 
			kunjungan.rating as rating,
			(CASE
				WHEN kunjungan.rating>=1 AND kunjungan.rating<=2 THEN 'Kurang Memuaskan'
				WHEN kunjungan.rating>=3 AND kunjungan.rating<=4 THEN 'Cukup Memuaskan'
				WHEN kunjungan.rating>=5 AND kunjungan.rating<=7 THEN 'Memuaskan'
				WHEN kunjungan.rating>=8 AND kunjungan.rating<=10 THEN 'Sangat Memuaskan'
			END) AS penilaian")
					->from('kunjungan')
					->join('tamu', 'kunjungan.tamu_id=tamu.id', 'left')
					->join('unit_kerja', 'unit_kerja.id=kunjungan.tujuan', 'left')
					->where('kunjungan.id', $kunjungan_id)
					->get()->row();
	}

	// Function to check kode pengunjung
	public function check_kode($kode_kunjungan)
	{
		$check = $this->db->select('id')
						->from('kunjungan')
						->where('kode_kunjungan', $kode_kunjungan)
						->get()->row();

		if(count($check)==1) {
			return $check->id;
		}
		else {
			return FALSE;
		}
	}

	public function statistik($type='month')
	{
		$sql = $this->db->select("(case
								when kunjungan.rating=0 then 'Kosong'
								when kunjungan.rating>=1 and kunjungan.rating<=2 then 'Kurang Memuaskan'
								when kunjungan.rating>=3 and kunjungan.rating<=4 then 'Cukup Memuaskan'
								when kunjungan.rating>=5 and kunjungan.rating<=7 then 'Memuaskan'
								when kunjungan.rating>=8 and kunjungan.rating<=10 then 'Sangat Memuaskan'
							end) as respon,
							round((count(kunjungan.id) / (select count(id) from kunjungan)  * 100), 2) as presentase")
						->from('kunjungan');
		switch ($type) {
			case 'month':
				$sql->where("month(kunjungan.tanggal)=month(curdate())");
				break;
			case 'year':
				$sql->where("year(kunjungan.tanggal)=year(curdate())");
				break;
			default:
				$sql->group_by('respon')
								->get()->result();
				return $sql;
				break;
		}

		$sql = $this->db->group_by('respon')
				->get()->result();
		
		return $sql;
	}

	/*====== Serverside Datatable ======*/
	private function _get_datatables_query($pengunjung_id=NULL)
	{
		$this->db->select("kunjungan.id as id, 
			tamu.id as tamu_id,
			tamu.nama as tamu_nama, 
			tamu.asal as tamu_asal,
			kunjungan.kode_kunjungan as kode_kunjungan, 
			kunjungan.tanggal as tanggal, 
			kunjungan.waktu as waktu, 
			kunjungan.keperluan as keperluan, 
			unit_kerja.id as tujuan,
			unit_kerja.nama as tujuan_nama, 
			kunjungan.masukan as masukan,  
			(CASE
				WHEN kunjungan.rating>=1 AND kunjungan.rating<=2 THEN 'Kurang Memuaskan'
				WHEN kunjungan.rating>=3 AND kunjungan.rating<=4 THEN 'Cukup Memuaskan'
				WHEN kunjungan.rating>=5 AND kunjungan.rating<=7 THEN 'Memuaskan'
				WHEN kunjungan.rating>=8 AND kunjungan.rating<=10 THEN 'Sangat Memuaskan'
			END) AS rating ");
		$this->db->from('kunjungan');
		$this->db->join('tamu', 'kunjungan.tamu_id=tamu.id', 'left');
		$this->db->join('unit_kerja', 'unit_kerja.id=kunjungan.tujuan', 'left');

		if($pengunjung_id!=NULL) {
			$this->db->where('tamu.id', $pengunjung_id);
		}

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($pengunjung_id=NULL)
	{	
		$this->_get_datatables_query($pengunjung_id);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($pengunjung_id=NULL)
	{
		$this->_get_datatables_query($pengunjung_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($pengunjung_id=NULL)
	{
		$this->db->select("kunjungan.id as id, 
			tamu.id as tamu_id,
			tamu.nama as tamu_nama, 
			tamu.asal as tamu_asal,
			kunjungan.kode_kunjungan as kode_kunjungan, 
			kunjungan.tanggal as tanggal, 
			kunjungan.waktu as waktu, 
			kunjungan.keperluan as keperluan, 
			unit_kerja.id as tujuan,
			unit_kerja.nama as tujuan_nama, 
			kunjungan.masukan as masukan,  
			(CASE
				WHEN kunjungan.rating>=1 AND kunjungan.rating<=2 THEN 'Kurang Memuaskan'
				WHEN kunjungan.rating>=3 AND kunjungan.rating<=4 THEN 'Cukup Memuaskan'
				WHEN kunjungan.rating>=5 AND kunjungan.rating<=7 THEN 'Memuaskan'
				WHEN kunjungan.rating>=8 AND kunjungan.rating<=10 THEN 'Sangat Memuaskan'
			END) AS rating ");
		$this->db->from('kunjungan');
		$this->db->join('tamu', 'kunjungan.tamu_id=tamu.id', 'left');
		$this->db->join('unit_kerja', 'unit_kerja.id=kunjungan.tujuan', 'left');

		if($pengunjung_id!=NULL) {
			$this->db->where('tamu.id', $pengunjung_id);
		}

		return $this->db->count_all_results();
	}
	/*====== /Serverside Datatable ======*/

}

/* End of file Kunjungan_model.php */
/* Location: ./application/models/Kunjungan_model.php */