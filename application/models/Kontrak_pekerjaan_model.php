<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kontrak_pekerjaan_model extends CI_Model {

    /*----- kontrak_pekerjaan -----*/
    // id
	// aktivitas_id
	// pejabat_pembuat_komitmen_id 
	// pejabat_pengadaan_id 
	// tgl_awal_kontrak
	// tgl_akhir_kontrak
    // hps
    // harga_negosiasi
	// status (Pembukaan, Penawaran, Penetapan, Penunjukan)
	// created_at
    // updated_at

    var $role_pegawai = array('Admin', 'Staff', 'Super Admin');
    var $main_table = 'kontrak_pekerjaan';

    public $group_by = NULL;
    
    //field yang ada di table
    var $column_order = array(NULL, 'aktivitas.nama_aktivitas',
                        'pekerjaan.tahun',
                        'pejabat_pengadaan_view.pegawai_nama_lengkap', 
                        'kontrak_pekerjaan.tgl_awal_kontrak',
                        'kontrak_pekerjaan.tgl_akhir_kontrak',
                        'kontrak_pekerjaan.hps',
                        'kontrak_pekerjaan.status', NULL);
    
    //field yang diizin untuk pencarian 
    var $column_search = array('aktivitas.nama_aktivitas',
                        'pekerjaan.tahun',
                        'pejabat_pengadaan_view.pegawai_nama_lengkap', 
                        'kontrak_pekerjaan.tgl_awal_kontrak',
                        'kontrak_pekerjaan.tgl_akhir_kontrak',
                        'kontrak_pekerjaan.hps',
                        'kontrak_pekerjaan.status'); 
	
	// default order 
    var $order = array('aktivitas.nama_aktivitas, pejabat_pengadaan_view.pegawai_nama_lengkap' => 'asc'); 

    public function fetch_data($id=NULL, $aktivitas_id=NULL, $tahun=NULL)
    {
        $sql = $this->db->select("kontrak_pekerjaan.id as id,
                                program.id as program_id,
                                program.nama_program as program_nama,
                                pekerjaan.id as pekerjaan_id,
                                pekerjaan.nama_pekerjaan as pekerjaan_nama,
                                pekerjaan.tahun as pekerjaan_tahun,
                                aktivitas.id as aktivitas_id,
                                aktivitas.nama_aktivitas as aktivitas_nama,
                                aktivitas.ppkom as ppkom,
                                pegawai_ppkom.pegawai_nama_lengkap as ppkom_nama,
                                aktivitas.no_rekening as aktivitas_no_rekening,
                                aktivitas.sumber_dana as aktivitas_sumber_dana,
                                aktivitas.anggaran_awal as aktivitas_anggaran_awal,                                
                                aktivitas.ppkom as pejabat_pembuat_komitmen_id,
                                pegawai_ppkom.pegawai_nama_lengkap as pejabat_pembuat_komitmen_nama,
                                pegawai_ppkom.pegawai_id as pejabat_pembuat_komitmen_pegawai_id,
                                pejabat_pengadaan.id as pejabat_pengadaan_id,
                                pejabat_pengadaan.pegawai_id as pejabat_pengadaan_pegawai_id,
                                pejabat_pengadaan_view.pegawai_nama_lengkap as pejabat_pengadaan_nama,
                                sk_pejabat_pengadaan.no_sk as sk_pejabat_pengadaan_no,
                                sk_pejabat_pengadaan.tgl_penetapan as sk_pejabat_pengadaan_tgl_penetapan,
                                sk_pejabat_pengadaan.tahun as sk_pejabat_pengadaan_tahun,
                                jenis_pejabat_pengadaan.nama as jenis_pejabat_pengadaan_nama,
                                seri_pejabat_pengadaan.nama as seri_pejabat_pengadaan_nama,
                                coalesce(kontrak_pekerjaan.tgl_awal_kontrak, 'Kosong') as tgl_awal_kontrak,
                                coalesce(kontrak_pekerjaan.tgl_akhir_kontrak, 'Kosong') as tgl_akhir_kontrak,
                                datediff(kontrak_pekerjaan.tgl_akhir_kontrak, kontrak_pekerjaan.tgl_awal_kontrak) as durasi_kontrak,
                                kontrak_pekerjaan.hps as hps,
                                kontrak_pekerjaan.harga_negosiasi as harga_negosiasi,
                                kontrak_pekerjaan.status as status")
                        ->from('kontrak_pekerjaan')
                        ->join('pejabat_pengadaan', 'kontrak_pekerjaan.pejabat_pengadaan_id=pejabat_pengadaan.id', 'left')
                        // ->join('pejabat_pembuat_komitmen', 'kontrak_pekerjaan.pejabat_pembuat_komitmen_id=pejabat_pembuat_komitmen.id', 'left')
                        ->join('view_data_pegawai as pejabat_pengadaan_view', 'pejabat_pengadaan_view.pegawai_id=pejabat_pengadaan.pegawai_id','left')
                        // ->join('view_data_pegawai as pejabat_pembuat_komitmen_view', 'pejabat_pembuat_komitmen_view.pegawai_id=pejabat_pembuat_komitmen.pegawai_id', 'left')
                        ->join('aktivitas', 'kontrak_pekerjaan.aktivitas_id=aktivitas.id', 'left')
                        ->join('pejabat_pembuat_komitmen', 'aktivitas.ppkom=pejabat_pembuat_komitmen.id', 'left')
                        ->join('view_data_pegawai as pegawai_ppkom', 'pejabat_pembuat_komitmen.pegawai_id=pegawai_ppkom.pegawai_id', 'left')
                        ->join('pekerjaan', 'aktivitas.pekerjaan_id=pekerjaan.id', 'inner')
                        ->join('program', 'pekerjaan.kode_program=program.uniq_code', 'inner')
                        ->join('jenis_pejabat_pengadaan', 'pejabat_pengadaan.jenis_pejabat_pengadaan_id=jenis_pejabat_pengadaan.id', 'left')
                        ->join('seri_pejabat_pengadaan', 'pejabat_pengadaan.tingkat_id=seri_pejabat_pengadaan.id', 'left')
                        ->join('sk_pejabat_pengadaan', 'pejabat_pengadaan.sk_id=sk_pejabat_pengadaan.id', 'left');

        if($id!=NULL) {
            $sql->where('kontrak_pekerjaan.id', $id);
        }
        if($aktivitas_id!=NULL){
            $sql->where('aktivitas.id', $aktivitas_id);
        }
        if($tahun!=NULL){
            $sql->where('pekerjaan.tahun', $tahun);
        }

        if($id!=NULL) {
            return $sql->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }

    public function rekap_data_kontrak($user_id=NULL, $user_role=NULL, $tahun=NULL)
    {
        $sql = $this->db->select("program.id as program_id,
                                program.nama_program as program_nama,
                                pekerjaan.id as pekerjaan_id,
                                pekerjaan.nama_pekerjaan as pekerjaan_nama,
                                pekerjaan.tahun as pekerjaan_tahun,
                                aktivitas.id as aktivitas_id,
                                aktivitas.nama_aktivitas as aktivitas_nama,
                                aktivitas.no_rekening as aktivitas_no_rekening,
                                pptk.pegawai_id as pptk_id,
                                pptk.pegawai_nama_lengkap as pptk_nama,
                                ppkom.pegawai_id as ppkom_id,
                                ppkom.pegawai_nama_lengkap as ppkom_nama,
                                bendahara.pegawai_id as bendahara_id,
                                bendahara.pegawai_nama_lengkap as bendahara_nama,
                                aktivitas.anggaran_awal as anggaran_awal,
                                aktivitas.perubahan_anggaran as perubahan_anggaran,
                                aktivitas.sumber_dana as aktivitas_sumber_dana,
                                kontrak_pekerjaan.id as kontrak_pekerjaan_id,
                                pejabat_pengadaan.id as pejabat_pengadaan_id,
                                pejabat_pengadaan.pegawai_id as pejabat_pengadaan_pegawai_id,
                                pejabat_pengadaan_view.pegawai_nama_lengkap as pejabat_pengadaan_nama,
                                coalesce(kontrak_pekerjaan.tgl_awal_kontrak, 'Kosong') as tgl_awal_kontrak,
                                coalesce(kontrak_pekerjaan.tgl_akhir_kontrak, 'Kosong') as tgl_akhir_kontrak,
                                datediff(kontrak_pekerjaan.tgl_akhir_kontrak, kontrak_pekerjaan.tgl_awal_kontrak) as durasi_kontrak,
                                kontrak_pekerjaan.hps as hps,
                                kontrak_pekerjaan.status as status,
                                kontrak_pekerjaan.harga_negosiasi as harga_negosiasi")
                        ->from('aktivitas')
                        ->join('kontrak_pekerjaan', 'aktivitas.id=kontrak_pekerjaan.aktivitas_id', 'left')
                        ->join('pekerjaan', 'aktivitas.pekerjaan_id=pekerjaan.id', 'left')
                        ->join('program', 'pekerjaan.kode_program=program.uniq_code', 'left')
                        ->join('pejabat_pengadaan', 'kontrak_pekerjaan.pejabat_pengadaan_id=pejabat_pengadaan.id', 'left')
                        ->join('pejabat_pembuat_komitmen', 'aktivitas.ppkom=pejabat_pembuat_komitmen.id', 'left')
                        ->join('view_data_pegawai as pejabat_pengadaan_view', 'pejabat_pengadaan_view.pegawai_id=pejabat_pengadaan.pegawai_id','left')
                        ->join('view_data_pegawai as pejabat_pembuat_komitmen_view', 'pejabat_pembuat_komitmen_view.pegawai_id=pejabat_pembuat_komitmen.pegawai_id', 'left')
                        ->join('view_data_pegawai as pptk', 'aktivitas.pptk=pptk.pegawai_id', 'left')
                        ->join('view_data_pegawai as ppkom', 'aktivitas.ppkom=ppkom.pegawai_id', 'left')
                        ->join('view_data_pegawai as bendahara', 'aktivitas.bendahara=bendahara.pegawai_id', 'left')
                        ->join('jenis_pengadaan', 'aktivitas.pengadaan_id=jenis_pengadaan.id', 'left')
                        ->join('jenis_pekerjaan', 'aktivitas.jenis_pekerjaan_id=jenis_pekerjaan.id', 'left')
                        ->where_in('jenis_pengadaan.nama', ['Penunjukan Langsung', 'Pengadaan Langsung'])
                        ->where_in('jenis_pekerjaan.nama', ['Fisik', 'Pengadaan Barang', 'Jasa Konsultan', 'Pengadaan Langsung'])
                        ->where('aktivitas.anggaran_awal >=', 50000000)
                        ->where('aktivitas.anggaran_awal <=', 200000000)
                        ->or_where('aktivitas.perubahan_anggaran >=', 50000000)
                        ->where('aktivitas.perubahan_anggaran <=', 200000000)
                        ->where('pekerjaan.tahun', $tahun);
			
        if($user_role!='Super Admin' or $user_role!='Admin') {
            $sql->where('pejabat_pengadaan_view.pegawai_id', $user_id)
                ->or_where('ppkom.pegawai_id', $user_id);		
        }
        // print($this->group_by);
        if($this->group_by=='pejabat_pengadaan') {
            $sql->order_by('pejabat_pengadaan_view.pegawai_nama_lengkap');
        }

        $sql->order_by('pekerjaan.nama_pekerjaan, aktivitas.nama_aktivitas');

		return $sql->get()->result();
    }

    // Statistik jml data Awal Kontrak pada tahun ini
	public function stat_awal_kontrak($user_id, $role='Super Admin')
	{
		$sql = $this->db->select('monthname(kontrak_pekerjaan.tgl_awal_kontrak) as bulan, 
							count(monthname(kontrak_pekerjaan.tgl_awal_kontrak)) as jml_awal_kontrak')
						->from('kontrak_pekerjaan')
						->join('aktivitas', 'kontrak_pekerjaan.aktivitas_id=aktivitas.id', 'left')
                        ->join('pekerjaan', 'aktivitas.pekerjaan_id=pekerjaan.id', 'left')
                        ->join('pegawai', 'kontrak_pekerjaan.pejabat_pembuat_komitmen_id=pegawai.id', 'left')
                        ->join('pejabat_pengadaan', 'kontrak_pekerjaan.pejabat_pengadaan_id=pejabat_pengadaan.id', 'left')
                        ->join('view_data_pegawai as pejabat_pengadaan_view', 'pejabat_pengadaan_view.pegawai_id=pejabat_pengadaan.pegawai_id','left');
		
		if($role!=='Super Admin'){
			$sql->where('pejabat_pengadaan_view.pegawai_id', $user_id)
                ->or_where('pegawai.id', $user_id);
		}
		
		return $sql->where('pekerjaan.tahun', date("Y"))
                    ->group_by('monthname(kontrak_pekerjaan.tgl_awal_kontrak)')
                    ->order_by('monthname(kontrak_pekerjaan.tgl_awal_kontrak)', 'asc')
					->get()->result();
	}

    // Statistik jml data Akhir Kontrak pada tahun ini
	public function stat_akhir_kontrak($user_id, $role='Super Admin')
	{
		$sql = $this->db->select('monthname(kontrak_pekerjaan.tgl_akhir_kontrak) as bulan, 
							count(monthname(kontrak_pekerjaan.tgl_akhir_kontrak)) as jml_akhir_kontrak')
                        ->from('kontrak_pekerjaan')
						->join('aktivitas', 'kontrak_pekerjaan.aktivitas_id=aktivitas.id', 'left')
                        ->join('pekerjaan', 'aktivitas.pekerjaan_id=pekerjaan.id', 'left')
                        ->join('pegawai', 'kontrak_pekerjaan.pejabat_pembuat_komitmen_id=pegawai.id', 'left')
                        ->join('pejabat_pengadaan', 'kontrak_pekerjaan.pejabat_pengadaan_id=pejabat_pengadaan.id', 'left')
                        ->join('view_data_pegawai as pejabat_pengadaan_view', 'pejabat_pengadaan_view.pegawai_id=pejabat_pengadaan.pegawai_id','left');
		
		if($role!=='Super Admin'){
			$sql->where('pejabat_pengadaan_view.pegawai_id', $user_id)
                ->or_where('pegawai.id', $user_id);
		}
		
		return $sql->where('pekerjaan.tahun', date("Y"))
                    ->group_by('monthname(kontrak_pekerjaan.tgl_akhir_kontrak)')
                    ->order_by('monthname(kontrak_pekerjaan.tgl_akhir_kontrak)', 'asc')
					->get()->result();
    }
    
    // Statistik jml data Kontrak Pekerjaan pada tahun ini
    public function jml_kontrak_pekerjaan($user_id, $role='Super Admin')
	{
		$sql = $this->db->select('count(kontrak_pekerjaan.id) as jml_kontrak')
                        ->from('kontrak_pekerjaan')
						->join('aktivitas', 'kontrak_pekerjaan.aktivitas_id=aktivitas.id', 'left')
                        ->join('pekerjaan', 'aktivitas.pekerjaan_id=pekerjaan.id', 'left')
                        ->join('pegawai', 'kontrak_pekerjaan.pejabat_pembuat_komitmen_id=pegawai.id', 'left')
                        ->join('pejabat_pengadaan', 'kontrak_pekerjaan.pejabat_pengadaan_id=pejabat_pengadaan.id', 'left')
                        ->join('view_data_pegawai as pejabat_pengadaan_view', 'pejabat_pengadaan_view.pegawai_id=pejabat_pengadaan.pegawai_id','left');
		
		if($role!=='Super Admin'){
			$sql->where('pejabat_pengadaan_view.pegawai_id', $user_id)
                ->or_where('pegawai.id', $user_id);
		}
		
		return $sql->where('pekerjaan.tahun', date("Y"))
					->get()->row();
    }

    // Statistik jml data status Kontrak Pekerjaan pada tahun ini
    public function jml_status_kontrak_pekerjaan($user_id, $role='Super Admin')
	{
        $sql = $this->db->select('kontrak_pekerjaan.status as status,
                            count(kontrak_pekerjaan.id) as jml_kontrak')
                        ->from('kontrak_pekerjaan')
						->join('aktivitas', 'kontrak_pekerjaan.aktivitas_id=aktivitas.id', 'left')
                        ->join('pekerjaan', 'aktivitas.pekerjaan_id=pekerjaan.id', 'left')
                        ->join('pegawai', 'kontrak_pekerjaan.pejabat_pembuat_komitmen_id=pegawai.id', 'left')
                        ->join('pejabat_pengadaan', 'kontrak_pekerjaan.pejabat_pengadaan_id=pejabat_pengadaan.id', 'left')
                        ->join('view_data_pegawai as pejabat_pengadaan_view', 'pejabat_pengadaan_view.pegawai_id=pejabat_pengadaan.pegawai_id','left');
		
		if($role!=='Super Admin'){                
            $sql->where('pejabat_pengadaan_view.pegawai_id', $user_id)
                ->or_where('pegawai.id', $user_id);
		}
		
        return $sql->where('pekerjaan.tahun', date("Y"))
                    ->group_by('kontrak_pekerjaan.status')
					->get()->result();
    }

    /*====== Serverside Datatable ======*/
	private function _get_datatables_query($aktivitas_id=NULL, $user_id=NULL, $role='Super Admin', $list_kontrak_pekerjaan_id=NULL)
	{
        $this->db->select('kontrak_pekerjaan.id as id,
                        aktivitas.nama_aktivitas as nama_aktivitas,
                        pekerjaan.tahun as pekerjaan_tahun,
                        pejabat_pengadaan_view.pegawai_nama_lengkap, 
                        kontrak_pekerjaan.tgl_awal_kontrak,
                        kontrak_pekerjaan.tgl_akhir_kontrak,
                        kontrak_pekerjaan.hps,
                        kontrak_pekerjaan.status');

        $this->db->from('kontrak_pekerjaan');
        $this->db->join('pejabat_pengadaan', 'kontrak_pekerjaan.pejabat_pengadaan_id=pejabat_pengadaan.id', 'left');
        // ->join('pejabat_pembuat_komitmen', 'kontrak_pekerjaan.pejabat_pembuat_komitmen_id=pejabat_pembuat_komitmen.id', 'left')
        $this->db->join('view_data_pegawai as pejabat_pengadaan_view', 'pejabat_pengadaan_view.pegawai_id=pejabat_pengadaan.pegawai_id','left');
        // ->join('view_data_pegawai as pejabat_pembuat_komitmen_view', 'pejabat_pembuat_komitmen_view.pegawai_id=pejabat_pembuat_komitmen.pegawai_id', 'left')
        $this->db->join('aktivitas', 'kontrak_pekerjaan.aktivitas_id=aktivitas.id', 'left');
        $this->db->join('pejabat_pembuat_komitmen', 'aktivitas.ppkom=pejabat_pembuat_komitmen.id', 'left');
        $this->db->join('view_data_pegawai as pegawai_ppkom', 'pejabat_pembuat_komitmen.pegawai_id=pegawai_ppkom.pegawai_id', 'left');
        $this->db->join('pekerjaan', 'aktivitas.pekerjaan_id=pekerjaan.id', 'inner');
        $this->db->join('program', 'pekerjaan.kode_program=program.uniq_code', 'inner');
        $this->db->join('jenis_pejabat_pengadaan', 'pejabat_pengadaan.jenis_pejabat_pengadaan_id=jenis_pejabat_pengadaan.id', 'left');
        $this->db->join('seri_pejabat_pengadaan', 'pejabat_pengadaan.tingkat_id=seri_pejabat_pengadaan.id', 'left');
        $this->db->join('sk_pejabat_pengadaan', 'pejabat_pengadaan.sk_id=sk_pejabat_pengadaan.id', 'left');

        if($aktivitas_id!=NULL and $role!='Super Admin' and $list_kontrak_pekerjaan_id==NULL){
            $this->db->where('aktivitas.id', $aktivitas_id);
            $this->db->where('pejabat_pengadaan_view.pegawai_id', $user_id);
            $this->db->or_where('pegawai_ppkom.pegawai_id', $user_id);
        }
        elseif($aktivitas_id==NULL and $role!='Super Admin' and $list_kontrak_pekerjaan_id==NULL) {
            $this->db->where('pejabat_pengadaan_view.pegawai_id', $user_id);
            $this->db->or_where('pegawai_ppkom.pegawai_id', $user_id);
        }
        elseif($aktivitas_id!=NULL and $role=='Super Admin' and $list_kontrak_pekerjaan_id==NULL) {
            $this->db->where('kontrak_pekerjaan.aktivitas_id', $aktivitas_id);
        }
        elseif($aktivitas_id==NULL and $role!='Super Admin' and $list_kontrak_pekerjaan_id!=NULL and is_array($list_kontrak_pekerjaan_id)) {
            $this->db->where_in('kontrak_pekerjaan.id', $list_kontrak_pekerjaan_id);
        }

        $this->db->order_by('pekerjaan.tahun', 'desc');

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($aktivitas_id=NULL, $user_id=NULL, $role='Super Admin', $list_kontrak_pekerjaan_id=NULL)
	{
		$this->_get_datatables_query($aktivitas_id, $user_id, $role, $list_kontrak_pekerjaan_id);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($aktivitas_id=NULL, $user_id=NULL, $role='Super Admin', $list_kontrak_pekerjaan_id=NULL)
	{
        $this->_get_datatables_query($aktivitas_id, $user_id, $role, $list_kontrak_pekerjaan_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($aktivitas_id=NULL, $user_id=NULL, $role='Super Admin', $list_kontrak_pekerjaan_id=NULL)
	{
		$this->db->select('kontrak_pekerjaan.id as id,
                        aktivitas.nama_aktivitas as nama_aktivitas,
                        pekerjaan.tahun as pekerjaan_tahun,
                        pejabat_pengadaan_view.pegawai_nama_lengkap, 
                        kontrak_pekerjaan.tgl_awal_kontrak,
                        kontrak_pekerjaan.tgl_akhir_kontrak,
                        kontrak_pekerjaan.hps,
                        kontrak_pekerjaan.status');

        $this->db->from('kontrak_pekerjaan');
        $this->db->join('pejabat_pengadaan', 'kontrak_pekerjaan.pejabat_pengadaan_id=pejabat_pengadaan.id', 'left');
        // ->join('pejabat_pembuat_komitmen', 'kontrak_pekerjaan.pejabat_pembuat_komitmen_id=pejabat_pembuat_komitmen.id', 'left')
        $this->db->join('view_data_pegawai as pejabat_pengadaan_view', 'pejabat_pengadaan_view.pegawai_id=pejabat_pengadaan.pegawai_id','left');
        // ->join('view_data_pegawai as pejabat_pembuat_komitmen_view', 'pejabat_pembuat_komitmen_view.pegawai_id=pejabat_pembuat_komitmen.pegawai_id', 'left')
        $this->db->join('aktivitas', 'kontrak_pekerjaan.aktivitas_id=aktivitas.id', 'left');
        $this->db->join('pejabat_pembuat_komitmen', 'aktivitas.ppkom=pejabat_pembuat_komitmen.id', 'left');
        $this->db->join('view_data_pegawai as pegawai_ppkom', 'pejabat_pembuat_komitmen.pegawai_id=pegawai_ppkom.pegawai_id', 'left');
        $this->db->join('pekerjaan', 'aktivitas.pekerjaan_id=pekerjaan.id', 'inner');
        $this->db->join('program', 'pekerjaan.kode_program=program.uniq_code', 'inner');
        $this->db->join('jenis_pejabat_pengadaan', 'pejabat_pengadaan.jenis_pejabat_pengadaan_id=jenis_pejabat_pengadaan.id', 'left');
        $this->db->join('seri_pejabat_pengadaan', 'pejabat_pengadaan.tingkat_id=seri_pejabat_pengadaan.id', 'left');
        $this->db->join('sk_pejabat_pengadaan', 'pejabat_pengadaan.sk_id=sk_pejabat_pengadaan.id', 'left');

        if($aktivitas_id!=NULL and $role!='Super Admin' and $list_kontrak_pekerjaan_id==NULL){
            $this->db->where('aktivitas.id', $aktivitas_id);
            $this->db->where('pejabat_pengadaan_view.pegawai_id', $user_id);
            $this->db->or_where('pegawai_ppkom.pegawai_id', $user_id);
        }
        elseif($aktivitas_id==NULL and $role!='Super Admin' and $list_kontrak_pekerjaan_id==NULL) {
            $this->db->where('pejabat_pengadaan_view.pegawai_id', $user_id);
            $this->db->or_where('pegawai_ppkom.pegawai_id', $user_id);
        }
        elseif($aktivitas_id!=NULL and $role=='Super Admin' and $list_kontrak_pekerjaan_id==NULL) {
            $this->db->where('kontrak_pekerjaan.aktivitas_id', $aktivitas_id);
        }
        elseif($aktivitas_id==NULL and $role!='Super Admin' and $list_kontrak_pekerjaan_id!=NULL and is_array($list_kontrak_pekerjaan_id)) {
            $this->db->where_in('kontrak_pekerjaan.id', $list_kontrak_pekerjaan_id);
        }

        $this->db->order_by('pekerjaan.tahun', 'desc');

		return $this->db->count_all_results();
	}
	/*====== /Serverside Datatable ======*/

}

/* End of file Kontrak_pekerjaan_model.php */
