<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Jenjang_pendidikan_model extends CI_Model {

    public function fetch_data($id=NULL)
    {
        $sql = $this->db->select('jenjang_pendidikan.id as id,
                            jenjang_pendidikan.nama as nama')
                        ->from('jenjang_pendidikan');
        
        if($id!=NULL) {
            return $sql->where('id', $id)
                        ->get()->row();
        }
        else {
            return $sql->get()->result();
        }
    }

}

/* End of file Jenjang_pendidikan_model.php */
