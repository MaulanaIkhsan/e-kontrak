<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumen_pihak_ketiga extends CI_Controller {

    /*----- dokumen_pihak_ketiga -----*/
    // id
    // nama_dokumen
    // pihak_ketiga_id
    // jenis_dokumen_id
    // file
    // tgl_awal_aktif
    // tgl_akhir_aktif
    // is_aktif

    // Configuration upload file 
    var $conf_upload = array(
        'upload_path'   => "./assets/uploads/documents/",
        'allowed_types' => "pdf",
        'max_size'      => '5000',
        'encrypt_name'  => FALSE
    );

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
        $sess_data = $this->session->userdata('session_data');
        if($sess_data===NULL) {
            $this->session->set_flashdata('warning', '<div class="alert alert-warning fade in">
                <a href="#" class="close" data-dismiss="alert">&times;</a> 
                Harap login terlebih dahulu untuk mengakses sistem</div>');
			redirect('auth');
        }

        $this->load->model('Dokumen_pihak_ketiga_model', 'dokumen_pihak_ketiga');
        $this->load->model('Generic_model', 'generic');
        
    }
    
    private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('nama_dokumen') == '')
		{
			$data['inputerror'][] = 'nama_dokumen';
			$data['error_string'][] = 'Nama Dokumen is required';
			$data['status'] = FALSE;
        }
        
        if($this->input->post('jenis_dokumen') == '')
		{
			$data['inputerror'][] = 'jenis_dokumen';
			$data['error_string'][] = 'Jenis Dokumen is required';
			$data['status'] = FALSE;
        }
        
        if($this->input->post('jenis_dokumen') != '')
        {
            $this->load->model('Jenis_dokumen_model', 'jenis_dokumen');
            
            $jenis_dokumen = $this->jenis_dokumen->fetch_data($this->input->post('jenis_dokumen'));
            if(!empty($jenis_dokumen) and $jenis_dokumen->is_series=='y') 
            {
                if($this->input->post('tgl_awal_aktif') == '')
                {
                    $data['inputerror'][] = 'tgl_awal_aktif';
                    $data['error_string'][] = 'Tanggal Awal Aktif is required';
                    $data['status'] = FALSE;
                }

                if($this->input->post('tgl_akhir_aktif') == '')
                {
                    $data['inputerror'][] = 'tgl_akhir_aktif';
                    $data['error_string'][] = 'Tanggal Akhir Aktif is required';
                    $data['status'] = FALSE;
                }
            }
        }

        if(empty($_FILES))
        {
            // Re-initialize data
            $data = array();
            $data['error_string'] = array();
            $data['inputerror'] = array();
            
            $data['inputerror'][] = 'files';
            $data['error_string'][] = 'Dokumen Harap berformat PDF*';
            $data['status'] = FALSE;
        }
		elseif($_FILES['files']['name']!='')
		{
            $nama_dok = $_FILES['files']['name'];
            $split = explode('.', $nama_dok);
            $ekstensi = strtolower($split[1]);

            //'2000000' as same as 2MB
            if($_FILES['files']['size']>5000000) {
                $data['inputerror'][] = 'files';
                $data['error_string'][] = 'Size dokumen melebihi 2MB';
                $data['status'] = FALSE;
            }
            else if(strpos($this->conf_upload['allowed_types'], $ekstensi)===FALSE) {
                $data['inputerror'][] = 'files';
                $data['error_string'][] = 'Format dokumen harus PDF';
                $data['status'] = FALSE;
            }
			
		}

		if($this->input->post('is_aktif') == '')
		{
			$data['inputerror'][] = 'is_aktif';
			$data['error_string'][] = 'Keaktifan is required';
			$data['status'] = FALSE;
		}		
        
		if($data['status'] === FALSE)
		{
			echo json_encode((object)$data);
			exit();
		}
        
	}

    public function get_data($id)
	{        
		$list = $this->dokumen_pihak_ketiga->get_datatables($id);

		$data = array();
        $no = $_POST['start'];
        
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->nama_dokumen;
			$row[] = $field->jenis_dokumen;
            $row[] = $field->tgl_awal_aktif;
            $row[] = $field->tgl_akhir_aktif;
            if(strlen($field->file)>0) {
                $row[] = "<a href=".base_url('assets/uploads/documents/'.$field->file)." target='_blank'> File-".$field->jenis_dokumen."</a>";
            }
            else {
                $row[] = "Kosong";
            }
            
            $row[] = $field->is_aktif;
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_dokumen('."'".$field->id."'".')"> Edit</a>
			<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_dokumen('."'".$field->id."'".')">Delete</a>';
			// $row[] = "<a class='btn btn-primary' href=".base_url('pihak_ketiga/detail/'.$field->id).">Detail</a>";
			$row[] = "";

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->dokumen_pihak_ketiga->count_all($id),
			"recordsFiltered" => $this->dokumen_pihak_ketiga->count_filtered($id),
			"data" => $data,
		);
		// output dalam format JSON
		echo json_encode($output);
	}

    public function ajax_add()
	{
        $this->_validate();
        
        $this->load->helper('generic');

        $random_name = rename_file();
        $new_name = $random_name.'.pdf';
        $this->conf_upload['file_name'] = $new_name;
        $this->load->library('upload', $this->conf_upload);
        $this->upload->initialize($this->conf_upload);

		$data = array(
                'nama_dokumen'      => $this->input->post('nama_dokumen'),
                'jenis_dokumen_id'  => $this->input->post('jenis_dokumen'),
				'tgl_awal_aktif'    => $this->input->post('tgl_awal_aktif'),
				'tgl_akhir_aktif'   => $this->input->post('tgl_akhir_aktif'),
                'is_aktif' 	        => $this->input->post('is_aktif'),
                'pihak_ketiga_id'   => $this->session->userdata('pihak_ketiga_id')
            );
        
        //'file' is name of textbox_upload in view
        $filename = str_replace(' ','', $_FILES['files']['name']);
        if(strlen($filename)>0) {
            $uploading = $this->upload->do_upload("files");
            if($uploading==TRUE){
                $data['file'] = $new_name;
            }
            else {
                echo json_encode(array('status' => 'Error upload new dokumen'));
            }
        }

        $insert = $this->generic->insert('dokumen_pihak_ketiga',$data);
        if($insert=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => 'Fail insert data'));
        }
        
	}

    public function ajax_edit($id)
	{
		$data = $this->dokumen_pihak_ketiga->fetch_data($id);
		echo json_encode($data);
	}

	public function ajax_update()
	{
        $this->_validate();
        
        $this->load->helper('generic');

        $random_name = rename_file();
        $new_name = $random_name.'.pdf';
        $this->conf_upload['file_name'] = $new_name;
        $this->load->library('upload', $this->conf_upload);
        $this->upload->initialize($this->conf_upload);

        $id = $this->input->post('id');

		$data = array(
            'nama_dokumen'      => $this->input->post('nama_dokumen'),
            'jenis_dokumen_id'  => $this->input->post('jenis_dokumen'),
            'tgl_awal_aktif'    => $this->input->post('tgl_awal_aktif'),
            'tgl_akhir_aktif'   => $this->input->post('tgl_akhir_aktif'),
            'is_aktif' 	        => $this->input->post('is_aktif'),
            'pihak_ketiga_id'   => $this->session->userdata('pihak_ketiga_id')
        );
        
        //'file' is name of textbox_upload in view
        $filename = str_replace(' ','', $_FILES['files']['name']);

        if(strlen($filename)>0) {
            $uploading = $this->upload->do_upload("files");

            //Deleting old file
            $dokumen        = $this->dokumen_pihak_ketiga->fetch_data($id);
            if(!empty($dokumen->file)) {
                $path_dokumen   = $this->conf_upload['upload_path'].$dokumen->file;
                if($path_dokumen!=NULL and file_exists($path_dokumen)){
                    unlink($path_dokumen);
                }
            }
            
            if($uploading==TRUE){
                $data['file'] = $new_name;
            }
            else {
                echo json_encode(array('status' => 'Error upload new dokumen'));
            }
        }

        $key    = array('id'=>$id);
        $update = $this->generic->update('dokumen_pihak_ketiga', $data, $key);
        
        if($update=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => 'Error edit data'));
        }
        
    }
    
    public function ajax_delete($id)
	{
        $key = array('id'=>$id);

        //Deleting file
        $dokumen = $this->dokumen_pihak_ketiga->fetch_data($id);
        $path_dokumen = $this->conf_upload['upload_path'].$dokumen->file;
        if($path_dokumen!=NULL and file_exists($path_dokumen)){
            unlink($path_dokumen);
        }

		$delete = $this->generic->delete('dokumen_pihak_ketiga', $key);
        
        if($delete=='ok') {
            echo json_encode(array("status" => TRUE));
        }
        else {
            echo json_encode(array("status" => FALSE));
        }
	}

    

    public function sample()
    {
        $this->load->helper('generic');
        
        print(rename_file());
    }
}

/* End of file Dokumen_pihak_ketiga.php */
