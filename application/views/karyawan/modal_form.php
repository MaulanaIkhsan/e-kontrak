<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_karyawan" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Upload</h4>
            </div>
            <div class="modal-body form">
                <div class="callout callout-danger">
                    <h4>Perhatian!</h4>

                    <p>Harap upload dokumen dengan format <strong><u>PDF</u></strong> dan size <strong><u>kurang dari 2MB</u></strong>  (atau compress terlebih dahulu)</p>
                </div>
                <form id="form-karyawan" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Nama Karyawan</label>
                            <div class="col-md-9">
                                <input name="nama_lengkap" placeholder="Nama Karyawan" class="form-control" type="text" required="required">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Jabatan</label>
                            <div class="col-md-9">
                                <select name="jabatan_id" class="form-control" required="required">
                                    <option value="">--Select Jabatan--</option>
                                    <?php foreach($jabatan_perusahaan as $item):?>
                                        <option value="<?php print($item->id); ?>"><?php print($item->nama); ?></option>
                                    <?php endforeach;?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Pendidikan Terkakhir</label>
                            <div class="col-md-9">
                                <select name="jenjang_pendidikan_id" class="form-control " required="required">
                                    <option value="">--Select Pendidikan--</option>
                                    <?php foreach($jenjang_pendidikan as $item):?>
                                        <option value="<?php print($item->id); ?>"><?php print($item->nama); ?></option>
                                    <?php endforeach;?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div><div class="form-group">
                            <label class="control-label col-md-3">Dokumen Pendidikan</label>
                            <div class="col-md-9">
                                <input type="file" name="dokumen_pendidikan" />
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Jenis Kelamin</label>
                            <div class="col-md-9">
                                <select name="jenis_kelamin" class="form-control " required="required">
                                    <option value="">--Select Jenis Kelamin--</option>
                                    <option value="Pria">Pria</option>
                                    <option value="Wanita">Wanita</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Alamat Lengkap</label>
                            <div class="col-md-9">
                                <textarea name="alamat_lengkap" placeholder="Alamat Lengkap" class="form-control"   required="required"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Tanggal Lahir</label>
                            <div class="col-md-9">
                                <input name="tgl_lahir" placeholder="yyyy-mm-dd" class="form-control datepicker" type="text" required="required">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">No Telepon</label>
                            <div class="col-md-9">
                                <input name="no_telepon" placeholder="Nomor Telepon" class="form-control number" type="text" required="required">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Email</label>
                            <div class="col-md-9">
                                <input name="email" placeholder="Alamat Email" class="form-control" type="text" required="required">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Dokumen Profil</label>
                            <div class="col-md-9">
                                <input type="file" name="dokumen_profil" />
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Dokumen Lainnya</label>
                            <div class="col-md-9">
                                <input type="file" name="dokumen_lainnya" />
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">User ?</label>
                            <div class="col-md-9">
                                <select name="is_user" class="form-control" required="required">
                                    <option value="">--Select Keaktifan--</option>
                                    <option value="y">Ya</option>
                                    <option value="n">Tidak</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group data-akun">
                            <label class="control-label col-md-3">Username</label>
                            <div class="col-md-9">
                                <input name="username" placeholder="Username" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group data-akun">
                            <label class="control-label col-md-3">Password</label>
                            <div class="col-md-9">
                                <input name="password" placeholder="Password" class="form-control" type="password">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                
            </div>
            <div class="modal-footer">
                    <button type="submit" id="btnSaveDokumen" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->