<!DOCTYPE html>
<html>
<head>
	<title>Print Content</title>
	<script type="text/javascript">
		window.print();
	</script>
	<link rel="stylesheet" href="<?php print(base_url('assets/bower_components/bootstrap/dist/css/bootstrap.min.css')); ?>">
	<link rel="stylesheet" type="text/css" href="<?php print(base_url('assets/plugins/customize/css/card.css'))?>">
</head>
<body>
	<div class="face face-back">
		<div class="row">
			<div class="col-md-2 col-md-offset-5" id="kode">
				<a href="#" onclick="history.back()"><?php print($kode_kunjungan) ?></a>
			</div>
			<hr align="center" />
			<div class="col-md-12"><?php print($nama) ?></div>
			<div class="col-md-12"><?php print($tujuan) ?></div>
			<hr align="center" />
			<div class="col-md-12"><?php print(format_date($tanggal,2)) ?> | <?php print($waktu) ?></div>	
					<!-- <div class="col-md-2">Waktu</div><div class="col-md-10">00:00:00</div>
			<div class="col-md-2">Tanggal</div><div class="col-md-10">00-Agust-0000</div> -->
		</div>
	</div>
	
</body>
</html>