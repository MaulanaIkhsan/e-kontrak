<?php $this->load->view('templates/header'); ?>       
<!-- Form input and update data karyawan -->
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="x_panel">
		    <div class="x_title">
		        <h2>Data Pekerjaan</h2>
		        <div class="clearfix"></div>
		    </div>
		    <div class="x_content">
        		<div class="table-responsive">        			
        			<?php print($this->session->flashdata('success')); ?>
        			<?php print($this->session->flashdata('alert')); ?>
        			<table class="table table-striped jambo_table bulk_action" id="aktivitas">
                        <thead>
                            <th>No</th>
                            <th>Nama Pekerjaan</th>
                            <th>PPTK</th>
                            <th>Jenis Pekerjaan</th>
                            <th>Metode Pengadaan</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody></tbody>
                    </table>
	        		
        		</div>
        	</div>
		</div>

	</div>
</div>
<script src="<?php echo base_url('assets/jquery/jquery-2.2.3.min.js')?>"></script>
<script type="text/javascript">
    $(document).ready(function(){


        var program = $('#aktivitas').DataTable({ 

            "processing": true, 
            "serverSide": true, 
            "order": [], 
            
            "ajax": {
                "url": "<?php echo site_url('aktivitas/get_data');?>",
                "type": "POST"
            },            
            "columnDefs": [{ 
                "targets": [ 0 ], 
                "orderable": false, 
            }],
            
        });
    });


</script>
<?php $this->load->view('templates/footer'); ?>  